--Report Name            : Invoice Order Detail Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXWC_CUSTOMER_ORDERS_V
set scan off define off
prompt Creating View XXEIS.XXWC_CUSTOMER_ORDERS_V
Create or replace View XXEIS.XXWC_CUSTOMER_ORDERS_V
(CREATION_DATE,CREATION_LESS_7,SOURCE,ORDER_TYPE,INVOICE_NUM,TRX_DATE,BRANCH,BILL_TO_CUST_NUM,BILL_TO_CUST_NAME,RAC_SHIP_TO_CUSTOMER_NUM,RAC_SHIP_TO_CUSTOMER_NAME,SU_SHIP_TO_LOCATION,CTT_TYPE_NAME,SALES_REP_NAME,SALES_REP_ID,ORACLE_SALESREP_NUM,SHIP_VIA,SOURCE_TYPE,USER_NAME,USER_NAME_DESC,SALES_ORDER,SALES_ORDER_LINE,ITEM,ITEM_DESC,ITEM_CATEGORY,ORDER_QTY,UOM,LIST_PRICE,SELL_PRICE,EXT_AMOUNT,NEW_COST_2,NEW_EXT_COST_2) AS 
SELECT
TRUNC(c.creation_date) AS CREATION_DATE,
c.creation_date-7 as CREATION_LESS_7,
c.interface_line_context as Source,
c.interface_line_attribute2 as Order_Type,
a.trx_number Invoice_Num,
a.trx_date,
substr(c.warehouse_name,1,3) as Branch,
a.rac_bill_to_customer_num as Bill_To_Cust_Num,
a.rac_bill_to_customer_name as Bill_To_Cust_Name,
a.rac_ship_to_customer_num,
a.rac_ship_to_customer_name,
a.su_ship_to_location,
a.ctt_type_name,
a.ras_primary_salesrep_name as Sales_Rep_Name,
l.salesrep_id as Sales_Rep_ID,
(SELECT JRS.salesrep_number from APPS.JTF_RS_SALESREPS JRS WHERE JRS.salesrep_id = l.salesrep_id and rownum=1) as Oracle_SalesRep_Num,   (SELECT WCSMV.ship_method_code_meaning FROM APPS.WSH_CARRIER_SHIP_METHODS_V WCSMV
    WHERE WCSMV.ship_method_code= l.shipping_method_code and rownum=1) as Ship_Via,
    l.source_type_code as Source_Type,
     u.user_name,
      u.description as User_Name_Desc,
  c.sales_order,
  c.sales_order_line,
   msi.segment1 as Item,
   c.description as Item_Desc,
   cat.category_concat_segs item_category,
    nvl(c.quantity_invoiced,c.quantity_credited) as Order_Qty,
    l.Pricing_Quantity_UOM as UOM,
    l.unit_list_price as List_Price,
    c.net_unit_selling_price as Sell_Price,
    c.net_extended_amount as Ext_Amount,
   -- apps.xxwc_mv_routines_pkg.get_order_line_cost (l.line_id) new_unit_cost,
   --(NVL (c.quantity_invoiced, c.quantity_credited) * apps.xxwc_mv_routines_pkg.get_order_line_cost (l.line_id)) AS new_ext_cost
ROUND
               (
                  (NVL
                   (APPS.xxwc_mv_routines_pkg.get_vendor_quote_cost
                                                         (l.line_id),
                    NVL
                       (APPS.xxwc_mv_routines_pkg.get_order_line_cost
                                                         (l.line_id),
                        0
                       )
                   )
                 ),
                2
               ) new_cost_2,
            (NVL (c.quantity_invoiced, c.quantity_credited))*((ROUND
                                                                   (
                                                                    (NVL
                                                                       (APPS.xxwc_mv_routines_pkg.get_vendor_quote_cost
                                                                                                             (l.line_id),
                                                                        NVL
                                                                           (APPS.xxwc_mv_routines_pkg.get_order_line_cost
                                                                                                             (l.line_id),
                                                                            0
                                                                           )
                                                                       )
                                                                     ),
                                                                    2
                                                                   ))) new_ext_cost_2


 FROM APPS.ra_customer_trx_v a, APPS.ra_customer_trx_lines_v c, APPS.mtl_system_items msi, APPS.oe_order_lines_all l,   APPS.mtl_item_categories_v cat, APPLSYS.fnd_user u

WHERE a.interface_header_context = 'ORDER ENTRY'
   and c.interface_line_context = 'ORDER ENTRY'
   and a.customer_trx_id=c.customer_trx_id
   and nvl(a.of_organization_id,222)=222
   and c.line_type='LINE'
   and msi.inventory_item_id=c.inventory_item_id
   and msi.organization_id=222
   and c.oe_header_id=l.header_id
   and c.interface_line_attribute6=to_char(l.line_id)
   and l.inventory_item_id=cat.inventory_item_id
   and cat.organization_id=222
   and cat.category_set_id=1100000062
   and l.created_by=u.user_id
 -- and c.creation_date>trunc(sysdate-1)+.25

ORDER BY c.warehouse_name, a.trx_date,c.sales_order, c.sales_order_line
/
set scan on define on
prompt Creating View Data for Invoice Order Detail Report
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_CUSTOMER_ORDERS_V
xxeis.eis_rs_ins.v( 'XXWC_CUSTOMER_ORDERS_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Xxwc Customer Orders V','XCOV','','');
--Delete View Columns for XXWC_CUSTOMER_ORDERS_V
xxeis.eis_rs_utility.delete_view_rows('XXWC_CUSTOMER_ORDERS_V',222,FALSE);
--Inserting View Columns for XXWC_CUSTOMER_ORDERS_V
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','EXT_COST',222,'Ext Cost','EXT_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Ext Cost','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','UNIT_COST',222,'Unit Cost','UNIT_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','EXT_AMOUNT',222,'Ext Amount','EXT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Ext Amount','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','SELL_PRICE',222,'Sell Price','SELL_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Sell Price','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','LIST_PRICE',222,'List Price','LIST_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','List Price','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','UOM',222,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','ORDER_QTY',222,'Order Qty','ORDER_QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Qty','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','ITEM_CATEGORY',222,'Item Category','ITEM_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Category','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','ITEM_DESC',222,'Item Desc','ITEM_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Desc','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','ITEM',222,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','SALES_ORDER_LINE',222,'Sales Order Line','SALES_ORDER_LINE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order Line','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','SALES_ORDER',222,'Sales Order','SALES_ORDER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','USER_NAME_DESC',222,'User Name Desc','USER_NAME_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','User Name Desc','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','USER_NAME',222,'User Name','USER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','User Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','SOURCE_TYPE',222,'Source Type','SOURCE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','SHIP_VIA',222,'Ship Via','SHIP_VIA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Via','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','ORACLE_SALESREP_NUM',222,'Oracle Salesrep Num','ORACLE_SALESREP_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Salesrep Num','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','SALES_REP_ID',222,'Sales Rep Id','SALES_REP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Rep Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','SALES_REP_NAME',222,'Sales Rep Name','SALES_REP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Rep Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','CTT_TYPE_NAME',222,'Ctt Type Name','CTT_TYPE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ctt Type Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','SU_SHIP_TO_LOCATION',222,'Su Ship To Location','SU_SHIP_TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Su Ship To Location','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','RAC_SHIP_TO_CUSTOMER_NAME',222,'Rac Ship To Customer Name','RAC_SHIP_TO_CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rac Ship To Customer Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','RAC_SHIP_TO_CUSTOMER_NUM',222,'Rac Ship To Customer Num','RAC_SHIP_TO_CUSTOMER_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rac Ship To Customer Num','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','BILL_TO_CUST_NAME',222,'Bill To Cust Name','BILL_TO_CUST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Cust Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','BILL_TO_CUST_NUM',222,'Bill To Cust Num','BILL_TO_CUST_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Cust Num','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','BRANCH',222,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Trx Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','INVOICE_NUM',222,'Invoice Num','INVOICE_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Num','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','ORDER_TYPE',222,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','SOURCE',222,'Source','SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','CREATION_DATE',222,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','NEW_EXT_COSTCOST',222,'New Ext Costcost','NEW_EXT_COSTCOST','','','','XXEIS_RS_ADMIN','NUMBER','','','New Ext Costcost','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','NEW_UNIT_COST',222,'New Unit Cost','NEW_UNIT_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','New Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','NEW_EXT_COST',222,'New Ext Cost','NEW_EXT_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','New Ext Cost','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','CREATION_LESS_7',222,'Creation Less 7','CREATION_LESS_7','','','','XXEIS_RS_ADMIN','DATE','','','Creation Less 7','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','NEW_COST_2',222,'New Cost 2','NEW_COST_2','','','','XXEIS_RS_ADMIN','NUMBER','','','New Cost 2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTOMER_ORDERS_V','NEW_EXT_COST_2',222,'New Ext Cost 2','NEW_EXT_COST_2','','','','XXEIS_RS_ADMIN','NUMBER','','','New Ext Cost 2','','','');
--Inserting View Components for XXWC_CUSTOMER_ORDERS_V
--Inserting View Component Joins for XXWC_CUSTOMER_ORDERS_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Invoice Order Detail Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Invoice Order Detail Report
xxeis.eis_rs_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct jrse.source_name salesrepname,jrs.salesrep_number salesrepnumber
  from     jtf_rs_salesreps jrs,    jtf_rs_resource_extns jrse
  where JRS.RESOURCE_ID            =JRSE.RESOURCE_ID
','','AR Sales Person LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Invoice Order Detail Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Invoice Order Detail Report
xxeis.eis_rs_utility.delete_report_rows( 'Invoice Order Detail Report' );
--Inserting Report - Invoice Order Detail Report
xxeis.eis_rs_ins.r( 222,'Invoice Order Detail Report','','This report gives the information of all the invoie order detail by branch or by invoice date or by sales rep names.

*****Attention*****
Due to the large amount of data, please limit the date range to within 7 days.
','','','','XXEIS_RS_ADMIN','XXWC_CUSTOMER_ORDERS_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Invoice Order Detail Report
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'BILL_TO_CUST_NAME','Bill To Cust Name','Bill To Cust Name','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'BILL_TO_CUST_NUM','Bill To Cust Num','Bill To Cust Num','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'BRANCH','Branch','Branch','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'CTT_TYPE_NAME','Ctt Type Name','Ctt Type Name','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'EXT_AMOUNT','Ext Amount','Ext Amount','','~T~D~2','default','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'INVOICE_NUM','Invoice Num','Invoice Num','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'ITEM','Item','Item','','','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'ITEM_CATEGORY','Item Category','Item Category','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'ITEM_DESC','Item Desc','Item Desc','','','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'LIST_PRICE','List Price','List Price','','~~~','default','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'ORACLE_SALESREP_NUM','Oracle Salesrep Num','Oracle Salesrep Num','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'ORDER_QTY','Order Qty','Order Qty','','~~~','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'ORDER_TYPE','Order Type','Order Type','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'RAC_SHIP_TO_CUSTOMER_NAME','Rac Ship To Customer Name','Rac Ship To Customer Name','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'RAC_SHIP_TO_CUSTOMER_NUM','Rac Ship To Customer Num','Rac Ship To Customer Num','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'SALES_ORDER','Sales Order','Sales Order','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'SALES_ORDER_LINE','Sales Order Line','Sales Order Line','','','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'SALES_REP_ID','Sales Rep Id','Sales Rep Id','','~~~','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'SALES_REP_NAME','Sales Rep Name','Sales Rep Name','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'SELL_PRICE','Sell Price','Sell Price','','~~~','default','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'SHIP_VIA','Ship Via','Ship Via','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'SOURCE','Source','Source','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'SOURCE_TYPE','Source Type','Source Type','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'SU_SHIP_TO_LOCATION','Su Ship To Location','Su Ship To Location','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'TRX_DATE','Trx Date','Trx Date','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'UOM','Uom','Uom','','','default','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'USER_NAME','User Name','User Name','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'USER_NAME_DESC','User Name Desc','User Name Desc','','','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'CREATION_DATE','Creation Date','Creation Date','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'NEW_COST_2','New Cost 2','New Cost 2','','~~~','default','','32','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Order Detail Report',222,'NEW_EXT_COST_2','New Ext Cost 2','New Ext Cost 2','','~~~','default','','33','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTOMER_ORDERS_V','','');
--Inserting Report Parameters - Invoice Order Detail Report
xxeis.eis_rs_ins.rp( 'Invoice Order Detail Report',222,'Branch','Branch','BRANCH','IN','AR Organizaion Code LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Order Detail Report',222,'Sales_Rep_Name','Sales Rep Name','SALES_REP_NAME','IN','AR Sales Person LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Order Detail Report',222,'Start_date','From Date','','=','','','DATE','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Invoice Order Detail Report
xxeis.eis_rs_ins.rcn( 'Invoice Order Detail Report',222,'BRANCH','IN',':Branch','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Order Detail Report',222,'SALES_REP_NAME','IN',':Sales_Rep_Name','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Order Detail Report',222,'XCOV.CREATION_DATE','=',':Start_date','','','Y','3','N','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Invoice Order Detail Report
--Inserting Report Triggers - Invoice Order Detail Report
--Inserting Report Templates - Invoice Order Detail Report
--Inserting Report Portals - Invoice Order Detail Report
--Inserting Report Dashboards - Invoice Order Detail Report
--Inserting Report Security - Invoice Order Detail Report
xxeis.eis_rs_ins.rsec( 'Invoice Order Detail Report','20005','','50843',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Order Detail Report','20005','','50900',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Order Detail Report','222','','50894',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Invoice Order Detail Report','660','','50886',222,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Invoice Order Detail Report
END;
/
set scan on define on
