--Report Name            : AR Aging Summary by Customer - MonthEnd - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for AR Aging Summary by Customer - MonthEnd - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_AGING_CUST_MONTH_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_AGING_CUST_MONTH_V',222,'','','','','SA059956','XXEIS','Eix Xxwc Ar Customer Summary V','EXAACMV','','');
--Delete View Columns for EIS_XXWC_AR_AGING_CUST_MONTH_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_AGING_CUST_MONTH_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_AGING_CUST_MONTH_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','BUCKET_361_DAYS_AND_ABOVE',222,'Bucket 361 Days And Above','BUCKET_361_DAYS_AND_ABOVE','','','','SA059956','NUMBER','','','Bucket 361 Days And Above','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','BUCKET_181_TO_360',222,'Bucket 181 To 360','BUCKET_181_TO_360','','','','SA059956','NUMBER','','','Bucket 181 To 360','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','BUCKET_91_TO_180',222,'Bucket 91 To 180','BUCKET_91_TO_180','','','','SA059956','NUMBER','','','Bucket 91 To 180','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','BUCKET_61_TO_90',222,'Bucket 61 To 90','BUCKET_61_TO_90','','','','SA059956','NUMBER','','','Bucket 61 To 90','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','BUCKET_31_TO_60',222,'Bucket 31 To 60','BUCKET_31_TO_60','','','','SA059956','NUMBER','','','Bucket 31 To 60','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','BUCKET_1_TO_30',222,'Bucket 1 To 30','BUCKET_1_TO_30','','','','SA059956','NUMBER','','','Bucket 1 To 30','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','BUCKET_CURRENT',222,'Bucket Current','BUCKET_CURRENT','','','','SA059956','NUMBER','','','Bucket Current','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','OUTSTANDING_AMOUNT',222,'Outstanding Amount','OUTSTANDING_AMOUNT','','','','SA059956','NUMBER','','','Outstanding Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','BILL_TO_CUSTOMER_NUMBER',222,'Bill To Customer Number','BILL_TO_CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Bill To Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','BILL_TO_CUSTOMER_NAME',222,'Bill To Customer Name','BILL_TO_CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Bill To Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','CREDIT_HOLD',222,'Credit Hold','CREDIT_HOLD','','','','SA059956','VARCHAR2','','','Credit Hold','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','CUSTOMER_ACCOUNT_STATUS',222,'Customer Account Status','CUSTOMER_ACCOUNT_STATUS','','','','SA059956','VARCHAR2','','','Customer Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','PROFILE_CLASS',222,'Profile Class','PROFILE_CLASS','','','','SA059956','VARCHAR2','','','Profile Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','COLLECTOR',222,'Collector','COLLECTOR','','','','SA059956','VARCHAR2','','','Collector','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','CREDIT_ANALYST_NAME',222,'Credit Analyst Name','CREDIT_ANALYST_NAME','','','','SA059956','VARCHAR2','','','Credit Analyst Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','SALESREP_NUMBER',222,'Salesrep Number','SALESREP_NUMBER','','','','SA059956','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_CUST_MONTH_V','ACCOUNT_MANAGER',222,'Account Manager','ACCOUNT_MANAGER','','','','SA059956','VARCHAR2','','','Account Manager','','','');
--Inserting View Components for EIS_XXWC_AR_AGING_CUST_MONTH_V
--Inserting View Component Joins for EIS_XXWC_AR_AGING_CUST_MONTH_V
END;
/
set scan on define on
prompt Creating Report LOV Data for AR Aging Summary by Customer - MonthEnd - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - AR Aging Summary by Customer - MonthEnd - WC
xxeis.eis_rs_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
    where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select name from ar_collectors','null','Collector','Displays list of values for Collector','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct name from hz_cust_profile_classes','null','PROFILE CLASS','This LOV lists all the profile classes of the customers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  resource_name resource_name
 FROM jtf_rs_role_relations a,
  jtf_rs_roles_vl b,
  jtf_rs_resource_extns_vl c
WHERE a.role_resource_type  = ''RS_INDIVIDUAL''
AND a.role_resource_id      = c.resource_id
AND a.role_id               = b.role_id
AND b.role_code             = ''CREDIT_ANALYST''
AND c.category              = ''EMPLOYEE''
AND NVL(a.delete_flag,''N'') <> ''Y''
Order BY resource_name','','Credit Analyst','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct credit_hold from hz_customer_profiles','','Credit Holds','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select  customer_status.meaning Status
from  fnd_lookup_values_vl customer_status
where customer_status.lookup_type= ''ACCOUNT_STATUS''
 and customer_status.view_application_id=222','','XXWC Customer Account Status','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct account_manager
from XXEIS.EIS_XXWC_AR_AGING_MONTH_END_V
order by account_manager','','XXWC AR Aging SalesRep Name','Sales Rep Name from AR Aging View','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct salesrep_number
from XXEIS.EIS_XXWC_AR_AGING_MONTH_END_V
order by salesrep_number','','XXWC AR Aging SalesRep Num','Sales Rep number from Aging View','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for AR Aging Summary by Customer - MonthEnd - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - AR Aging Summary by Customer - MonthEnd - WC
xxeis.eis_rs_utility.delete_report_rows( 'AR Aging Summary by Customer - MonthEnd - WC' );
--Inserting Report - AR Aging Summary by Customer - MonthEnd - WC
xxeis.eis_rs_ins.r( 222,'AR Aging Summary by Customer - MonthEnd - WC','','AR Aging Summary by Customer - MonthEnd - WC','','','','SA059956','EIS_XXWC_AR_AGING_CUST_MONTH_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - AR Aging Summary by Customer - MonthEnd - WC
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BILL_TO_CUSTOMER_NAME','Customer Name','Bill To Customer Name','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BILL_TO_CUSTOMER_NUMBER','Customer Number','Bill To Customer Number','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BUCKET_181_TO_360','Bucket 181 To 360','Bucket 181 To 360','','~~~','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BUCKET_1_TO_30','Bucket 1 To 30','Bucket 1 To 30','','~~~','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BUCKET_31_TO_60','Bucket 31 To 60','Bucket 31 To 60','','~~~','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BUCKET_361_DAYS_AND_ABOVE','Bucket 361 Days And Above','Bucket 361 Days And Above','','~~~','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BUCKET_61_TO_90','Bucket 61 To 90','Bucket 61 To 90','','~~~','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BUCKET_91_TO_180','Bucket 91 To 180','Bucket 91 To 180','','~~~','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BUCKET_CURRENT','Bucket Current','Bucket Current','','~~~','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'OUTSTANDING_AMOUNT','Customer Account Balance','Outstanding Amount','','~~~','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'COLLECTOR','Collector','Collector','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'CREDIT_HOLD','Credit Hold','Credit Hold','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'CUSTOMER_ACCOUNT_STATUS','Customer Account Status','Customer Account Status','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'PROFILE_CLASS','Profile Class','Profile Class','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'ACCOUNT_MANAGER','Salesrep Name','Account Manager','','','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary by Customer - MonthEnd - WC',222,'SALESREP_NUMBER','Salesrep Number','Salesrep Number','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_CUST_MONTH_V','','');
--Inserting Report Parameters - AR Aging Summary by Customer - MonthEnd - WC
xxeis.eis_rs_ins.rp( 'AR Aging Summary by Customer - MonthEnd - WC',222,'Customer Name','Customer Name','BILL_TO_CUSTOMER_NAME','IN','Customer Name','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary by Customer - MonthEnd - WC',222,'Customer Account Status','Customer Account Status','CUSTOMER_ACCOUNT_STATUS','IN','XXWC Customer Account Status','','VARCHAR2','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary by Customer - MonthEnd - WC',222,'Collector','Collector','COLLECTOR','IN','Collector','','VARCHAR2','N','Y','3','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary by Customer - MonthEnd - WC',222,'Credit Analyst','Credit Analyst','CREDIT_ANALYST_NAME','IN','Credit Analyst','','VARCHAR2','N','Y','4','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary by Customer - MonthEnd - WC',222,'Credit Hold','Credit Hold','CREDIT_HOLD','IN','Credit Holds','','VARCHAR2','N','Y','5','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary by Customer - MonthEnd - WC',222,'Profile Class','Profile Class','PROFILE_CLASS','IN','PROFILE CLASS','','VARCHAR2','N','Y','6','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary by Customer - MonthEnd - WC',222,'Sales Rep Name','Sales Rep Name','ACCOUNT_MANAGER','IN','XXWC AR Aging SalesRep Name','','VARCHAR2','N','Y','7','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary by Customer - MonthEnd - WC',222,'Sales Rep Number','Sales Rep Number','SALESREP_NUMBER','IN','XXWC AR Aging SalesRep Num','','VARCHAR2','N','Y','8','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - AR Aging Summary by Customer - MonthEnd - WC
xxeis.eis_rs_ins.rcn( 'AR Aging Summary by Customer - MonthEnd - WC',222,'BILL_TO_CUSTOMER_NAME','IN',':Customer Name','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary by Customer - MonthEnd - WC',222,'CUSTOMER_ACCOUNT_STATUS','IN',':Customer Account Status','','','Y','2','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary by Customer - MonthEnd - WC',222,'COLLECTOR','IN',':Collector','','','Y','3','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary by Customer - MonthEnd - WC',222,'CREDIT_ANALYST_NAME','IN',':Credit Analyst','','','Y','4','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary by Customer - MonthEnd - WC',222,'CREDIT_HOLD','IN',':Credit Hold','','','Y','5','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary by Customer - MonthEnd - WC',222,'PROFILE_CLASS','IN',':Profile Class','','','Y','6','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary by Customer - MonthEnd - WC',222,'ACCOUNT_MANAGER','IN',':Sales Rep','','','Y','7','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary by Customer - MonthEnd - WC',222,'ACCOUNT_MANAGER','IN',':Sales Rep Name','','','Y','7','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary by Customer - MonthEnd - WC',222,'SALESREP_NUMBER','IN',':Sales Rep Number','','','Y','8','Y','SA059956');
--Inserting Report Sorts - AR Aging Summary by Customer - MonthEnd - WC
--Inserting Report Triggers - AR Aging Summary by Customer - MonthEnd - WC
--Inserting Report Templates - AR Aging Summary by Customer - MonthEnd - WC
--Inserting Report Portals - AR Aging Summary by Customer - MonthEnd - WC
--Inserting Report Dashboards - AR Aging Summary by Customer - MonthEnd - WC
--Inserting Report Security - AR Aging Summary by Customer - MonthEnd - WC
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','10012196','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','LB048272','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','MM050208','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','SO004816','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','10010432','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','LC053655','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','RB054040','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','RV003897','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','SS084202','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','','10011289','',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50878',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50877',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50879',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50853',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50880',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50854',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50873',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50638',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50622',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','1','','50962',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','20678',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50844',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50845',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50846',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50865',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50862',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50863',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50864',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50847',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50866',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50848',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50868',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50849',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50867',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50870',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50871',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50869',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','201','','50850',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','201','','50872',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50851',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','702','','50875',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','260','','50758',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','260','','50770',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','702','','50852',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50876',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','512','','50881',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','175','','50922',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','175','','50941',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50942',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','140','','50728',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','140','','50789',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','140','','50874',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','101','','50982',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50882',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50883',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50981',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50855',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50884',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','20005','','50861',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','20005','','50843',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','704','','50885',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50857',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50858',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50859',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50886',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50860',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','50901',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','200','','50904',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','200','','50905',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','200','','50902',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','200','','50890',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','200','','50903',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','200','','50887',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','200','','50888',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','200','','50889',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','661','','50891',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','201','','50921',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','201','','50892',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','201','','50910',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','201','','50893',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50894',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50895',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','20005','','50897',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','20005','','50900',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','1','','50961',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','706','','50909',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','50871',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50872',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','222','','21404',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50619',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','401','','50821',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','20005','','50880',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary by Customer - MonthEnd - WC','660','','51044',222,'SA059956','','');
--Inserting Report Pivots - AR Aging Summary by Customer - MonthEnd - WC
xxeis.eis_rs_ins.rpivot( 'AR Aging Summary by Customer - MonthEnd - WC',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
