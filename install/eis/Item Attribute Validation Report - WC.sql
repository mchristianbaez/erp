--Report Name            : Item Attribute Validation Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Item Attribute Validation Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_NEW_PO_ISR_RPT_V1
xxeis.eis_rs_ins.v( 'EIS_XXWC_NEW_PO_ISR_RPT_V1',201,'','','','','SA059956','XXEIS','Eis Xxwc Po Isr V','EXPIV','','');
--Delete View Columns for EIS_XXWC_NEW_PO_ISR_RPT_V1
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_NEW_PO_ISR_RPT_V1',201,FALSE);
--Inserting View Columns for EIS_XXWC_NEW_PO_ISR_RPT_V1
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','BPA',201,'Bpa','BPA','','','','SA059956','VARCHAR2','','','Bpa','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ITEM_COST',201,'Item Cost','ITEM_COST','','','','SA059956','NUMBER','','','Item Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','AVER_COST',201,'Aver Cost','AVER_COST','','','','SA059956','NUMBER','','','Aver Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','AMU',201,'Amu','AMU','','','','SA059956','VARCHAR2','','','Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','PM',201,'Pm','PM','','','','SA059956','VARCHAR2','','','Pm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','STK_FLAG',201,'Stk Flag','STK_FLAG','','','','SA059956','VARCHAR2','','','Stk Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','CL',201,'Cl','CL','','','','SA059956','VARCHAR2','','','Cl','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','UOM',201,'Uom','UOM','','','','SA059956','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','CAT',201,'Cat','CAT','','','','SA059956','VARCHAR2','','','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','DESCRIPTION',201,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ST',201,'St','ST','','','','SA059956','VARCHAR2','','','St','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','SOURCE',201,'Source','SOURCE','','','','SA059956','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','VENDOR_NUM',201,'Vendor Num','VENDOR_NUM','','','','SA059956','VARCHAR2','','','Vendor Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ITEM_NUMBER',201,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','PRE',201,'Pre','PRE','','','','SA059956','VARCHAR2','','','Pre','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ORG',201,'Org','ORG','','','','SA059956','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','BUYER',201,'Buyer','BUYER','','','','SA059956','VARCHAR2','','','Buyer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','TURNS',201,'Turns','TURNS','','','','SA059956','NUMBER','','','Turns','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','THIRTEEN_WK_AN_COGS',201,'Thirteen Wk An Cogs','THIRTEEN_WK_AN_COGS','','','','SA059956','NUMBER','','','Thirteen Wk An Cogs','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','THIRTEEN_WK_AVG_INV',201,'Thirteen Wk Avg Inv','THIRTEEN_WK_AVG_INV','','','','SA059956','NUMBER','','','Thirteen Wk Avg Inv','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','RES',201,'Res','RES','','','','SA059956','VARCHAR2','','','Res','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','FREEZE_DATE',201,'Freeze Date','FREEZE_DATE','','','','SA059956','DATE','','','Freeze Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','FI_FLAG',201,'Fi Flag','FI_FLAG','','','','SA059956','VARCHAR2','','','Fi Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MC',201,'Mc','MC','','','','SA059956','VARCHAR2','','','Mc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','BIN_LOC',201,'Bin Loc','BIN_LOC','','','','SA059956','VARCHAR2','','','Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','AVAILABLEDOLLAR',201,'Availabledollar','AVAILABLEDOLLAR','','','','SA059956','NUMBER','','','Availabledollar','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','AVAILABLE',201,'Available','AVAILABLE','','','','SA059956','NUMBER','','','Available','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','QOH',201,'Qoh','QOH','','','','SA059956','NUMBER','','','Qoh','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','APR_SALES',201,'Apr Sales','APR_SALES','','','','SA059956','NUMBER','','','Apr Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','AUG_SALES',201,'Aug Sales','AUG_SALES','','','','SA059956','NUMBER','','','Aug Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','DEC_SALES',201,'Dec Sales','DEC_SALES','','','','SA059956','NUMBER','','','Dec Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','FEB_SALES',201,'Feb Sales','FEB_SALES','','','','SA059956','NUMBER','','','Feb Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','HIT4_SALES',201,'Hit4 Sales','HIT4_SALES','','','','SA059956','NUMBER','','','Hit4 Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','HIT6_SALES',201,'Hit6 Sales','HIT6_SALES','','','','SA059956','NUMBER','','','Hit6 Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','JAN_SALES',201,'Jan Sales','JAN_SALES','','','','SA059956','NUMBER','','','Jan Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','JUL_SALES',201,'Jul Sales','JUL_SALES','','','','SA059956','NUMBER','','','Jul Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','JUNE_SALES',201,'June Sales','JUNE_SALES','','','','SA059956','NUMBER','','','June Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MAR_SALES',201,'Mar Sales','MAR_SALES','','','','SA059956','NUMBER','','','Mar Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MAY_SALES',201,'May Sales','MAY_SALES','','','','SA059956','NUMBER','','','May Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','NOV_SALES',201,'Nov Sales','NOV_SALES','','','','SA059956','NUMBER','','','Nov Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','OCT_SALES',201,'Oct Sales','OCT_SALES','','','','SA059956','NUMBER','','','Oct Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','SEP_SALES',201,'Sep Sales','SEP_SALES','','','','SA059956','NUMBER','','','Sep Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ONE_SALES',201,'One Sales','ONE_SALES','','','','SA059956','NUMBER','','','One Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','SIX_SALES',201,'Six Sales','SIX_SALES','','','','SA059956','NUMBER','','','Six Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','TWELVE_SALES',201,'Twelve Sales','TWELVE_SALES','','','','SA059956','NUMBER','','','Twelve Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','DISTRICT',201,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','REGION',201,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MAXN',201,'Maxn','MAXN','','','','SA059956','VARCHAR2','','','Maxn','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MINN',201,'Minn','MINN','','','','SA059956','VARCHAR2','','','Minn','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','TS',201,'Ts','TS','','','','SA059956','VARCHAR2','','','Ts','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','BPA_COST',201,'Bpa Cost','BPA_COST','','','','SA059956','NUMBER','','','Bpa Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ON_ORD',201,'On Ord','ON_ORD','','','','SA059956','NUMBER','','','On Ord','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','FML',201,'Fml','FML','','','','SA059956','NUMBER','','','Fml','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','OPEN_REQ',201,'Open Req','OPEN_REQ','','','','SA059956','NUMBER','','','Open Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','WT',201,'Wt','WT','','','','SA059956','NUMBER','','','Wt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','SO',201,'So','SO','','','','SA059956','NUMBER','','','So','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','SOURCING_RULE',201,'Sourcing Rule','SOURCING_RULE','','','','SA059956','VARCHAR2','','','Sourcing Rule','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','SS',201,'Ss','SS','','','','SA059956','NUMBER','','','Ss','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','CLT',201,'Clt','CLT','','','','SA059956','NUMBER','','','Clt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','AVAIL2',201,'Avail2','AVAIL2','','','','SA059956','NUMBER','','','Avail2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','INT_REQ',201,'Int Req','INT_REQ','','','','SA059956','NUMBER','','','Int Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','DIR_REQ',201,'Dir Req','DIR_REQ','','','','SA059956','NUMBER','','','Dir Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','SITE_VENDOR_NUM',201,'Site Vendor Num','SITE_VENDOR_NUM','','','','SA059956','VARCHAR2','','','Site Vendor Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','VENDOR_SITE',201,'Vendor Site','VENDOR_SITE','','','','SA059956','VARCHAR2','','','Vendor Site','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','PLT',201,'Plt','PLT','','','','SA059956','NUMBER','','','Plt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','PPLT',201,'Pplt','PPLT','','','','SA059956','NUMBER','','','Pplt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ORG_NAME',201,'Org Name','ORG_NAME','','','','SA059956','VARCHAR2','','','Org Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MF_FLAG',201,'Mf Flag','MF_FLAG','','','','SA059956','VARCHAR2','','','Mf Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','SET_OF_BOOKS_ID',201,'Set Of Books Id','SET_OF_BOOKS_ID','','','','SA059956','NUMBER','','','Set Of Books Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ORGANIZATION_ID',201,'Organization Id','ORGANIZATION_ID','','','','SA059956','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','CAT_SBA_OWNER',201,'Cat Sba Owner','CAT_SBA_OWNER','','','','SA059956','VARCHAR2','','','Cat Sba Owner','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','CORE',201,'Core','CORE','','','','SA059956','VARCHAR2','','','Core','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','DEM',201,'Dem','DEM','','','','SA059956','NUMBER','','','Dem','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','FLIP_DATE',201,'Flip Date','FLIP_DATE','','','','SA059956','DATE','','','Flip Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','LAST_RECEIPT_DATE',201,'Last Receipt Date','LAST_RECEIPT_DATE','','','','SA059956','DATE','','','Last Receipt Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','LIST_PRICE',201,'List Price','LIST_PRICE','','','','SA059956','NUMBER','','','List Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MAKE_BUY',201,'Make Buy','MAKE_BUY','','','','SA059956','VARCHAR2','','','Make Buy','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MFG_PART_NUMBER',201,'Mfg Part Number','MFG_PART_NUMBER','','','','SA059956','VARCHAR2','','','Mfg Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MST_ITEM_STATUS',201,'Mst Item Status','MST_ITEM_STATUS','','','','SA059956','VARCHAR2','','','Mst Item Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MST_USER_ITEM_TYPE',201,'Mst User Item Type','MST_USER_ITEM_TYPE','','','','SA059956','VARCHAR2','','','Mst User Item Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','MST_VENDOR',201,'Mst Vendor','MST_VENDOR','','','','SA059956','VARCHAR2','','','Mst Vendor','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ONHAND_GT_270',201,'Onhand Gt 270','ONHAND_GT_270','','','','SA059956','NUMBER','','','Onhand Gt 270','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ORG_ITEM_STATUS',201,'Org Item Status','ORG_ITEM_STATUS','','','','SA059956','VARCHAR2','','','Org Item Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','ORG_USER_ITEM_TYPE',201,'Org User Item Type','ORG_USER_ITEM_TYPE','','','','SA059956','VARCHAR2','','','Org User Item Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','SUPERSEDE_ITEM',201,'Supersede Item','SUPERSEDE_ITEM','','','','SA059956','VARCHAR2','','','Supersede Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','TIER',201,'Tier','TIER','','','','SA059956','VARCHAR2','','','Tier','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','COMMON_OUTPUT_ID',201,'Common Output Id','COMMON_OUTPUT_ID','','','','SA059956','NUMBER','','','Common Output Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','DEMAND',201,'Demand','DEMAND','','','','SA059956','NUMBER','','','Demand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_PO_ISR_RPT_V1','OPERATING_UNIT',201,'Operating Unit','OPERATING_UNIT','','','','SA059956','VARCHAR2','','','Operating Unit','','','');
--Inserting View Components for EIS_XXWC_NEW_PO_ISR_RPT_V1
--Inserting View Component Joins for EIS_XXWC_NEW_PO_ISR_RPT_V1
END;
/
set scan on define on
prompt Creating Report LOV Data for Item Attribute Validation Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'    SELECT distinct Mcv.SEGMENT2 cat_class
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            MTL_ITEM_CATEGORIES MIC,
            mtl_system_items_kfv msi
            Where   Mcs.Category_Set_Name      = ''Inventory Category''
          AND MCS.STRUCTURE_ID                 = MCV.STRUCTURE_ID
          AND MIC.INVENTORY_ITEM_ID            = MSI.INVENTORY_ITEM_ID
          And Mic.Organization_Id              = msi.Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id
order by Mcv.SEGMENT2','','Catclass Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Exclude,Include,Tool Repair Only','Tool Repair','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','  ,Vendor Number(%),3 Digit Prefix,Item number,Source,2 Digit Cat,4 Digit Cat Class,Default Buyer(%)','Report Criteria','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Active small,All items,All items on hand, Stock items only,Non stock only,Active large, Non-stock on hand,Stock items with 0/0 min/max','EIS PO XXWC ISR REPORT COND','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Include,Time Sensitive Only','EIS PO XXWC Time Sensitive','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Yes,No','EIS PO XXWC DC Mode','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'SELECT DISTINCT segment1 Item
FROM mtl_system_items_b msi
WHERE NOT EXISTS
  (SELECT 1
  FROM mtl_parameters mp
  WHERE organization_code IN (''CAN'',''HDS'',''US1'',''CN1'')
  AND msi.organization_id  = mp.organization_id
  )
order by segment1 ASC','','XXWC Item','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct pov.segment1 vendor_number, pov.vendor_name from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC Vendors','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,
  ood.organization_name organization_name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE organization_code NOT IN(''CAN'',''HDS'',''US1'',''CN1'')
ORDER BY organization_code','','XXWC Org Lov','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Item Attribute Validation Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Item Attribute Validation Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'Item Attribute Validation Report - WC' );
--Inserting Report - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.r( 201,'Item Attribute Validation Report - WC','','This report displays info needed to make inventory replenishment decisions ( by vendor, part number) at selected locations.','','','','SA059956','EIS_XXWC_NEW_PO_ISR_RPT_V1','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AVER_COST','Aver Cost','Aver Cost','','~T~D~2','default','','27','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'BIN_LOC','Bin Loc','Bin Loc','','','default','','43','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'BPA','Bpa#','Bpa','','','default','','30','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'BUYER','Buyer','Buyer','','','default','','51','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'CAT','Cat/Cl','Cat','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'CL','Cl','Cl','','','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'DEC_SALES','Dec','Dec Sales','','~T~D~2','default','','64','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'DESCRIPTION','Description','Description','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'FEB_SALES','Feb','Feb Sales','','~T~D~2','default','','54','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'FI_FLAG','FI','Fi Flag','','','default','','45','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'HIT4_SALES','Hit4','Hit4 Sales','','~T~D~2','default','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'HIT6_SALES','Hit6','Hit6 Sales','','~T~D~2','default','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ITEM_NUMBER','Item Number','Item Number','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'JAN_SALES','Jan','Jan Sales','','~T~D~2','default','','53','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AUG_SALES','Aug','Aug Sales','','~T~D~2','default','','60','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AVAILABLE','Avail1','Available','','~T~D~2','default','','37','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AVAILABLEDOLLAR','Ext$','Availabledollar','','~T~D~2','default','','39','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'JUL_SALES','Jul','Jul Sales','','~T~D~2','default','','59','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'JUNE_SALES','June','June Sales','','~T~D~2','default','','58','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MAR_SALES','Mar','Mar Sales','','~T~D~2','default','','55','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MAXN','Max','Maxn','NUMBER','~T~D~0','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MAY_SALES','May','May Sales','','~T~D~2','default','','57','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'QOH','QOH','Qoh','','~T~D~2','default','','31','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'RES','Res','Res','NUMBER','~T~D~0','default','','47','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SEP_SALES','Sep','Sep Sales','','~T~D~2','default','','61','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SIX_SALES','6','Six Sales','','~T~D~2','default','','41','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SOURCE','Source','Source','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ST','ST','St','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'STK_FLAG','Stk','Stk Flag','','','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'THIRTEEN_WK_AN_COGS','13 Wk An COGS $','Thirteen Wk An Cogs','','~T~D~2','default','','49','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'THIRTEEN_WK_AVG_INV','13 Wk Av Inv $','Thirteen Wk Avg Inv','','~T~D~2','default','','48','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'TS','TS','Ts','NUMBER','~T~D~0','default','','52','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'TURNS','Turns','Turns','','~T~D~2','default','','50','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'TWELVE_SALES','12','Twelve Sales','','~T~D~2','default','','42','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'UOM','UOM','Uom','','','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'VENDOR_NAME','Vendor','Vendor Name','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MINN','Min','Minn','NUMBER','~T~D~0','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'VENDOR_NUM','Vend #','Vendor Num','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AMU','AMU','Amu','NUMBER','~T~D~0','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'FML','FLM','Fml','','~T~D~2','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'OPEN_REQ','E Req','Open Req','','~T~D~2','default','','33','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'WT','Wt','Wt','','~T~D~2','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SO','So','So','','~T~D~2','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SOURCING_RULE','Sourcing Rule','Sourcing Rule','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SS','Ss','Ss','','~T~D~2','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'CLT','CLT','Clt','','~T~D~2','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'AVAIL2','Avail2','Avail2','','~T~D~2','default','','38','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'BPA_COST','Bpa Cost','Bpa Cost','','~T~D~2','default','','29','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ON_ORD','On Ord','On Ord','','~T~D~2','default','','36','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'APR_SALES','Apr','Apr Sales','','~T~D~2','default','','56','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'INT_REQ','Int Req','Int Req','','~T~D~2','default','','34','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'DIR_REQ','Dir Req','Dir Req','','~T~D~2','default','','35','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'NOV_SALES','Nov','Nov Sales','','~T~D~2','default','','63','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'OCT_SALES','Oct','Oct Sales','','~T~D~2','default','','62','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ONE_SALES','1','One Sales','','~T~D~2','default','','40','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ORG','Org','Org','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'PM','PM','Pm','','','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'PRE','Pre','Pre','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'CAT_SBA_OWNER','Cat','Cat Sba Owner','','','default','','67','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'CORE','Core','Core','','','default','','65','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'DEM','Dem','Dem','','~T~D~2','default','','32','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'FLIP_DATE','Flip Date','Flip Date','','','default','','44','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'LAST_RECEIPT_DATE','Last Receipt Date','Last Receipt Date','','','default','','75','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'LIST_PRICE','List Price','List Price','','~T~D~2','default','','28','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MAKE_BUY','Make Buy','Make Buy','','','default','','70','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MFG_PART_NUMBER','Mfg Part Number','Mfg Part Number','','','default','','68','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MST_ITEM_STATUS','Mst Item Status','Mst Item Status','','','default','','73','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MST_USER_ITEM_TYPE','Mst User Item Type','Mst User Item Type','','','default','','74','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'MST_VENDOR','Mst Vendor','Mst Vendor','','','default','','69','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ONHAND_GT_270','Onhand Gt 270','Onhand Gt 270','','~T~D~2','default','','76','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ORG_ITEM_STATUS','Org Item Status','Org Item Status','','','default','','71','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'ORG_USER_ITEM_TYPE','Org User Item Type','Org User Item Type','','','default','','72','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'SUPERSEDE_ITEM','Supersede Item','Supersede Item','','','default','','77','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'TIER','Tier','Tier','','','default','','66','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'PLT','PLT','Plt','','~T~D~2','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'PPLT','PPLT','Pplt','','~T~D~2','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
xxeis.eis_rs_ins.rc( 'Item Attribute Validation Report - WC',201,'FREEZE_DATE','Res$','Freeze Date','','','default','','46','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_PO_ISR_RPT_V1','','');
--Inserting Report Parameters - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Items w/ 4 mon sales hits','Stock items w/ 4 mon sales hits > x','','IN','','','NUMERIC','N','Y','9','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Report Criteria','Report Criteria','','IN','Report Criteria','','VARCHAR2','N','Y','10','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Report Criteria Value','Report Criteria Value','','IN','','','VARCHAR2','N','Y','11','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'DC Mode','DC Mode','','IN','EIS PO XXWC DC Mode','''No''','VARCHAR2','N','Y','5','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Starting Bin','Starting Bin','BIN_LOC','>=','Bin Location Lov','','VARCHAR2','N','Y','12','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Ending Bin','Ending Bin','BIN_LOC','<=','Bin Location Lov','','VARCHAR2','N','Y','13','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Tool Repair','Tool Repair','','IN','Tool Repair','''Include''','VARCHAR2','N','Y','6','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Time Sensitive','Time Sensitive','','IN','EIS PO XXWC Time Sensitive','','VARCHAR2','N','Y','7','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Vendor','Vendor','VENDOR_NUM','IN','XXWC Vendors','','VARCHAR2','N','Y','14','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Item','Item','ITEM_NUMBER','IN','XXWC Item','','VARCHAR2','N','Y','15','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Cat Class','Cat Class','CAT','IN','Catclass Lov','','VARCHAR2','N','Y','16','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Report Filter','Report Condition','','IN','EIS PO XXWC ISR REPORT COND','','VARCHAR2','N','Y','8','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','17','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','18','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Cat Class List','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','19','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Source List','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','20','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Organization List','Organization List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Organization','Organization','ORG','IN','XXWC Org Lov','','VARCHAR2','N','Y','3','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Item Attribute Validation Report - WC',201,'Intangible Items','Intangible Items','','IN','','''Exclude''','VARCHAR2','N','Y','21','','N','CONSTANT','SA059956','N','N','','','');
--Inserting Report Conditions - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'BIN_LOC','>=',':Starting Bin','','','Y','12','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'PROCESS_ID','IN',':SYSTEM.PROCESS_ID','','','Y','3','N','SA059956');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'BIN_LOC','<=',':Ending Bin','','','Y','13','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'VENDOR_NUM','IN',':Vendor','','','Y','14','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'ITEM_NUMBER','IN',':Item','','','Y','15','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'CAT','IN',':Cat Class','','','Y','16','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'ORG','IN',':Organization','','','Y','3','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'REGION','IN',':Region','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Item Attribute Validation Report - WC',201,'DISTRICT','IN',':District','','','Y','2','Y','SA059956');
--Inserting Report Sorts - Item Attribute Validation Report - WC
--Inserting Report Triggers - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rt( 'Item Attribute Validation Report - WC',201,'begin
xxeis.eis_po_xxwc_isr_util_pkg.G_isr_rpt_dc_mod_sub:=:DC Mode;
xxeis.eis_new_po_xxwc_isr_pkg.Isr_live_Rpt_Proc(
P_process_id => :SYSTEM.PROCESS_ID,
P_Region =>:Region,
P_District =>:District,
P_Location =>:Organization,
P_Dc_Mode =>:DC Mode,
P_Tool_Repair =>:Tool Repair,
P_Time_Sensitive =>:Time Sensitive,
P_Stk_Items_With_Hit4 =>:Items w/ 4 mon sales hits,
p_Report_Condition =>:Report Filter,
P_Report_Criteria            =>:Report Criteria,
P_Report_Criteria_val            =>:Report Criteria Value,
p_start_bin_loc =>:Starting Bin,
p_end_bin_loc =>:Ending Bin,
p_vendor =>:Vendor,
p_item =>:Item,
p_cat_class =>:Cat Class,
p_org_list =>:Organization List,
P_ITEM_LIST =>:Item List,
P_SUPPLIER_LIST =>:Vendor List,
P_CAT_CLASS_LIST =>:Cat Class List,
P_SOURCE_LIST =>:Source List
);
xxwc_isr_datamart_pkg.report_isr_live(
p_process_id => :SYSTEM.PROCESS_ID,
p_region =>:Region,
p_district =>:District,
p_location =>:Organization,
p_dc_mode =>:DC Mode,
p_tool_repair =>:Tool Repair,
p_time_sensitive =>:Time Sensitive,
p_stk_items_with_hit4 =>:Items w/ 4 mon sales hits,
p_report_condition =>:Report Filter,
p_report_criteria            =>:Report Criteria,
p_report_criteria_val            =>:Report Criteria Value,
p_start_bin_loc =>:Starting Bin,
p_end_bin_loc =>:Ending Bin,
p_vendor =>:Vendor,
p_item =>:Item,
p_cat_class =>:Cat Class,
p_org_list =>:Organization List,
p_item_list =>:Item List,
p_supplier_list =>:Vendor List,
p_cat_class_list =>:Cat Class List,
p_source_list =>:Source List,
p_intangibles => :Intangible Items
);
end;','B','Y','SA059956');
--Inserting Report Templates - Item Attribute Validation Report - WC
--Inserting Report Portals - Item Attribute Validation Report - WC
--Inserting Report Dashboards - Item Attribute Validation Report - WC
--Inserting Report Security - Item Attribute Validation Report - WC
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','201','','50983',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','20005','','50900',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','201','','50893',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50862',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50848',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50868',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50849',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50867',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50990',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','51004',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50883',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50882',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','201','','50621',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','201','','51369',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','201','','50910',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','201','','50892',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','201','','50921',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50884',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50855',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','401','','50981',201,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Item Attribute Validation Report - WC','','10010494','',201,'SA059956','','');
--Inserting Report Pivots - Item Attribute Validation Report - WC
END;
/
set scan on define on
