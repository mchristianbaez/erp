--Report Name            : Cash Drawer Reconciliation
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_CASH_DRWR_RECON_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_CASH_DRWR_RECON_V
Create or replace View XXEIS.EIS_XXWC_AR_CASH_DRWR_RECON_V
(ORDER_NUMBER,ORG_ID,PAYMENT_TYPE_CODE,PAYMENT_TYPE_CODE_NEW,TAKEN_BY,CASH_TYPE,CHK_CARDNO,CARD_NUMBER,CARD_ISSUER_CODE,CARD_ISSUER_NAME,CASH_AMOUNT,CUSTOMER_NUMBER,CUSTOMER_NAME,BRANCH_NUMBER,CHECK_NUMBER,CASH_DATE,PAYMENT_CHANNEL_NAME,NAME,PAYMENT_NUMBER,PAYMENT_AMOUNT,ORDER_AMOUNT,PARTY_ID,CUST_ACCOUNT_ID,INVOICE_NUMBER,INVOICE_DATE,SEGMENT_NUMBER,HEADER_ID,CASH_RECEIPT_ID) AS 
SELECT OH.ORDER_NUMBER,
    oh.org_id,
    --olkp.meaning PAYMENT_TYPE_CODE,
    CASE
      WHEN OE.PAYMENT_TYPE_CODE IN( 'CHECK','CREDIT_CARD')
      AND upper(ARC.NAME) LIKE UPPER('%Card%')
      THEN 'Credit Card'
      WHEN OE.PAYMENT_TYPE_CODE IN ('CHECK')
      AND UPPER(ARC.NAME) NOT LIKE UPPER('%Card%')
      THEN 'Check'
      ELSE olkp.meaning
    END PAYMENT_TYPE_CODE,
    CASE
      WHEN OE.PAYMENT_TYPE_CODE IN( 'CHECK','CREDIT_CARD')
      AND upper(ARC.NAME) LIKE UPPER('%Card%')
      THEN 'Credit Card'
      WHEN OE.PAYMENT_TYPE_CODE IN ('CHECK')
      AND UPPER(ARC.NAME) NOT LIKE UPPER('%Card%')
      THEN 'Check'
      ELSE olkp.meaning
    END PAYMENT_TYPE_CODE_NEW,
    PPF.LAST_NAME TAKEN_BY,
    CASE
      WHEN OE.PAYMENT_TYPE_CODE IN ('CASH','CHECK','CREDIT_CARD')
      AND NOT EXISTS
        (SELECT ORDERED_ITEM
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        AND ORDERED_ITEM LIKE '%DEPOSIT%'
        )
      AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )
      THEN 'CASH SALES'
      WHEN OE.PAYMENT_TYPE_CODE IN ('CASH','CHECK','CREDIT_CARD')
      AND EXISTS
        (SELECT ORDERED_ITEM
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        AND ORDERED_ITEM LIKE '%DEPOSIT%'
        )
      AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )
      THEN 'DEPOSIT'
    END CASH_TYPE,
    /*CASE
    WHEN OE.PAYMENT_TYPE_CODE IN( 'CHECK','CREDIT_CARD') AND ARC.NAME LIKE '%Credit%Card%'
    THEN SUBSTR(OE.CHECK_NUMBER,-4)
    ELSE OE.CHECK_NUMBER
    END CHK_CARDNO,*/
    DECODE(OE.PAYMENT_TYPE_CODE,'CHECK',OE.CHECK_NUMBER,'CREDIT_CARD',SUBSTR(ITE.CARD_NUMBER,-4)) CHK_CARDNO,
    SUBSTR(ITE.CARD_NUMBER,                                                                  -4) CARD_NUMBER,
    ITE.CARD_ISSUER_CODE,
    ITE.CARD_ISSUER_NAME,
    OE.PAYMENT_AMOUNT CASH_AMOUNT,
    hca.account_number customer_number,
    HZP.PARTY_NAME CUSTOMER_NAME,
    OOD.ORGANIZATION_CODE BRANCH_NUMBER,
    OE.CHECK_NUMBER,
    TRUNC(OH.ORDERED_DATE) CASH_DATE,
    ITE.PAYMENT_CHANNEL_NAME,
    ARC.name,
    PAYMENT_NUMBER,
    (SELECT SUM( OE1.PAYMENT_AMOUNT)
    FROM OE_PAYMENTS OE1
    WHERE HEADER_ID=OH.HEADER_ID
    ) PAYMENT_AMOUNT,
    CASE
      WHEN Payment_Number=1
      THEN
        (SELECT ROUND(SUM((OL.ORDERED_QUANTITY*OL.UNIT_SELLING_PRICE)+ OL.TAX_VALUE),2) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        )
      ELSE 0
    END ORDER_AMOUNT,
    HZP.PARTY_ID,
    HCA.CUST_ACCOUNT_ID,
    (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT
    WHERE TO_CHAR(OH.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    )Invoice_Number,
    --   NULL CREDIT_NUMBER,
    (
    SELECT MAX(RCT.TRX_DATE)
    FROM RA_CUSTOMER_TRX RCT
    WHERE TO_CHAR(OH.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    )INVOICE_DATE,
    (SELECT MAX(GCC.SEGMENT2)
    FROM OE_ORDER_LINES L,
      RA_CUSTOMER_TRX_LINES RCTL,
      RA_CUSTOMER_TRX RCT,
      RA_CUST_TRX_LINE_GL_DIST RCTG,
      GL_CODE_COMBINATIONS_KFV GCC
    WHERE OH.HEADER_ID           =L.HEADER_ID
    AND TO_CHAR(OH.ORDER_NUMBER) =RCTL.INTERFACE_LINE_ATTRIBUTE1
    AND TO_CHAR(L.LINE_ID)       =RCTL.INTERFACE_LINE_ATTRIBUTE6
    AND RCT.CUSTOMER_TRX_ID      =RCTL.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_ID     =RCTG.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_LINE_ID=RCTG.CUSTOMER_TRX_LINE_ID
    AND rctg.code_combination_id =gcc.code_combination_id
    )Segment_Number,
    Oh.Header_Id Header_Id,
    0 cash_receipt_id
  FROM OE_ORDER_HEADERS OH,
    oe_payments oe,
    oe_lookups olkp,
    hz_parties hzp,
    HZ_CUST_ACCOUNTS HCA,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    IBY_TRXN_EXTENSIONS_V ITE,
    PER_ALL_PEOPLE_F PPF,
    FND_USER FU,
    AR_RECEIPT_METHODS arc
  WHERE OH.SOLD_TO_ORG_ID =HCA.CUST_ACCOUNT_ID
  AND OE.HEADER_ID        =OH.HEADER_ID
  AND HZP.PARTY_ID        =HCA.PARTY_ID
  AND OH.SHIP_FROM_ORG_ID =OOD.ORGANIZATION_ID
  AND FU.USER_ID          =OH.CREATED_BY
  AND fu.employee_id      =PPF.PERSON_ID(+)
  AND OH.ORDERED_DATE BETWEEN NVL(PPF.EFFECTIVE_START_DATE,OH.ORDERED_DATE) AND NVL(PPF.EFFECTIVE_END_DATE,OH.ORDERED_DATE)
  AND OE.TRXN_EXTENSION_ID =ITE.TRXN_EXTENSION_ID(+)
  AND oe.receipt_method_id =arc.receipt_method_id(+)
  AND oe.payment_type_code = olkp.lookup_code
  AND oh.flow_status_code IN('BOOKED','CLOSED')
  AND Olkp.Lookup_Type     = 'PAYMENT TYPE'
    -- And Oh.Header_Id=26587
  AND NOT EXISTS
    (SELECT 1
    FROM XXWC_OM_CASH_REFUND_TBL XOC
    WHERE XOC.RETURN_HEADER_ID=OH.HEADER_ID
    )
  /* AND EXISTS
  (SELECT 1
  FROM RA_CUSTOMER_TRX RCT
  WHERE TO_CHAR(Oh.Order_Number) =Rct.Interface_Header_Attribute1
  )*/
  UNION
  SELECT OH.ORDER_NUMBER,
    oh.org_id,
    olkp.meaning PAYMENT_TYPE_CODE,
    olkp.meaning PAYMENT_TYPE_CODE_NEW,
    PPF.LAST_NAME TAKEN_BY,
    'CASH RETURN' CASH_TYPE,
    DECODE(XMCR.PAYMENT_TYPE_CODE,'CHECK',OE.CHECK_NUMBER,'CREDIT_CARD',SUBSTR(ITE.CARD_NUMBER,-4)) CHK_CARDNO,
    SUBSTR(ITE.CARD_NUMBER,                                                                    -4) CARD_NUMBER,
    ITE.CARD_ISSUER_CODE,
    Ite.Card_Issuer_Name,
    xmcr.refund_amount*-1 cash_amount,
    hca.account_number customer_number,
    HZP.PARTY_NAME CUSTOMER_NAME,
    OOD.ORGANIZATION_CODE BRANCH_NUMBER,
    OE.CHECK_NUMBER,
    TRUNC(OH.ORDERED_DATE) CASH_DATE,
    ITE.PAYMENT_CHANNEL_NAME,
    Arc.Name,
    NULL PAYMENT_NUMBER,
    (SELECT SUM(XX.REFUND_AMOUNT)
    FROM XXWC_OM_CASH_REFUND_TBL XX
    WHERE XX.RETURN_HEADER_ID =XMCR.RETURN_HEADER_ID
    AND XX.PAYMENT_TYPE_CODE <> 'CHECK'
    )Payment_Amount,
    xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_cash_drawer_amt(oh.header_id) ORDER_AMOUNT,
    HZP.PARTY_ID,
    HCA.CUST_ACCOUNT_ID,
    (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT,
      OE_ORDER_HEADERS H
    WHERE H.HEADER_ID           =XMCR.HEADER_ID
    AND TO_CHAR(H.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    )Invoice_Number,
    /*   (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT,
    Oe_Order_Headers H
    WHERE H.HEADER_ID           =XMCR.RETURN_HEADER_ID
    And To_Char(H.Order_Number) =Rct.Interface_Header_Attribute1
    )CREDIT_NUMBER,*/
    (
    SELECT MAX(RCT.TRX_DATE)
    FROM RA_CUSTOMER_TRX RCT,
      OE_ORDER_HEADERS H1
    WHERE TO_CHAR(H1.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    AND H1.HEADER_ID               =XMCR.HEADER_ID
    )INVOICE_DATE,
    (SELECT MAX(GCC.SEGMENT2)
    FROM OE_ORDER_LINES L,
      OE_ORDER_HEADERS H2,
      RA_CUSTOMER_TRX_LINES RCTL,
      RA_CUSTOMER_TRX RCT,
      RA_CUST_TRX_LINE_GL_DIST RCTG,
      GL_CODE_COMBINATIONS_KFV GCC
    WHERE H2.HEADER_ID           =XMCR.HEADER_ID
    AND H2.HEADER_ID             =L.HEADER_ID
    AND TO_CHAR(H2.order_number) =rct.interface_HEADER_attribute1
    AND TO_CHAR(L.LINE_ID)       =RCTL.INTERFACE_LINE_ATTRIBUTE6
    AND RCT.CUSTOMER_TRX_ID      =RCTL.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_ID     =RCTG.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_LINE_ID=RCTG.CUSTOMER_TRX_LINE_ID
    AND RCTG.CODE_COMBINATION_ID =GCC.CODE_COMBINATION_ID
    )Segment_Number,
    Oh.Header_Id Header_Id,
    XMCR.cash_receipt_id cash_receipt_id
  FROM OE_ORDER_HEADERS OH,
    OE_PAYMENTS OE,
    OE_LOOKUPS OLKP,
    HZ_PARTIES HZP,
    HZ_CUST_ACCOUNTS HCA,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    XXWC_OM_CASH_REFUND_TBL XMCR,
    IBY_TRXN_EXTENSIONS_V ITE,
    PER_ALL_PEOPLE_F PPF,
    FND_USER FU,
    AR_RECEIPT_METHODS arc
  WHERE OE.HEADER_ID(+)     =OH.HEADER_ID
  AND oH.sold_to_org_id     =hca.cust_account_id
  AND HZP.PARTY_ID          =HCA.PARTY_ID
  AND OH.SHIP_FROM_ORG_ID   =OOD.ORGANIZATION_ID
  AND xmcr.RETURN_header_id =oh.header_id
  AND FU.USER_ID            =OH.CREATED_BY
  AND fu.employee_id        =PPF.PERSON_ID(+)
  AND OH.ORDERED_DATE BETWEEN NVL(PPF.EFFECTIVE_START_DATE,OH.ORDERED_DATE) AND NVL(PPF.EFFECTIVE_END_DATE,OH.ORDERED_DATE)
  AND Oe.Trxn_Extension_Id =Ite.Trxn_Extension_Id(+)
    -- And xmcr.RETURN_header_id=26587
  AND XMCR.PAYMENT_TYPE_CODE <> 'CHECK'
  AND oe.receipt_method_id    =arc.receipt_method_id(+)
  AND xmcr.payment_type_code  = olkp.lookup_code
  AND Olkp.Lookup_Type        = 'PAYMENT TYPE'
  ORDER BY Header_Id,
    Order_Amount DESC
/
set scan on define on
prompt Creating View Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_CASH_DRWR_RECON_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Cash Drwr Recon V','EXACDRV');
--Delete View Columns for EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_CASH_DRWR_RECON_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_DATE',222,'Cash Date','CASH_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Cash Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','BRANCH_NUMBER',222,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_AMOUNT',222,'Cash Amount','CASH_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Cash Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_ISSUER_NAME',222,'Card Issuer Name','CARD_ISSUER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_TYPE',222,'Cash Type','CASH_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cash Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_TYPE_CODE',222,'Payment Type Code','PAYMENT_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','TAKEN_BY',222,'Taken By','TAKEN_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Taken By');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','SEGMENT_NUMBER',222,'Segment Number','SEGMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CHK_CARDNO',222,'Chk Cardno','CHK_CARDNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Chk Cardno');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_AMOUNT',222,'Order Amount','ORDER_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','NAME',222,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_CHANNEL_NAME',222,'Payment Channel Name','PAYMENT_CHANNEL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Channel Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CHECK_NUMBER',222,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_ISSUER_CODE',222,'Card Issuer Code','CARD_ISSUER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_NUMBER',222,'Card Number','CARD_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORG_ID',222,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_NUMBER',222,'Payment Number','PAYMENT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_RECEIPT_ID',222,'Cash Receipt Id','CASH_RECEIPT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cash Receipt Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','HEADER_ID',222,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_AMOUNT',222,'Payment Amount','PAYMENT_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_TYPE_CODE_NEW',222,'Payment Type Code New','PAYMENT_TYPE_CODE_NEW','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code New');
--Inserting View Components for EIS_XXWC_AR_CASH_DRWR_RECON_V
--Inserting View Component Joins for EIS_XXWC_AR_CASH_DRWR_RECON_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Cash Drawer Reconciliation
xxeis.eis_rs_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Cash Drawer Reconciliation
xxeis.eis_rs_utility.delete_report_rows( 'Cash Drawer Reconciliation' );
--Inserting Report - Cash Drawer Reconciliation
xxeis.eis_rs_ins.r( 222,'Cash Drawer Reconciliation','','This report provides a total listing of all Cash, Check and Credit Card Sales by Branch (Customer, Taken By, Order Number, Cash Date, Cash Amount, Cash Type, Invoice Number, Invoice Date, Payment Type, Check/Credit Card No.)','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_CASH_DRWR_RECON_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'BRANCH_NUMBER','Branch Number','Branch Number','','','','','1','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CASH_AMOUNT','Cash Amount','Cash Amount','','','','','9','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CASH_DATE','Cash Date','Cash Date','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CASH_TYPE','Cash Type','Cash Type','','','','','15','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CHK_CARDNO','Check/Card No','Chk Cardno','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','3','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'NAME','Receipt Method','Name','','','','','14','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'ORDER_AMOUNT','Order Amount','Order Amount','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'ORDER_NUMBER','Order Number','Order Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'PAYMENT_TYPE_CODE','Payment Type','Payment Type Code','','','','','11','N','','ROW_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'SEGMENT_NUMBER','Segment Number','Segment Number','','','','','17','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'TAKEN_BY','Created By','Taken By','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','4','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CARD_ISSUER_NAME','Card Type','Card Issuer Name','','','','','13','N','','ROW_FIELD','','','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'DIFFERENCE','DIFFERENCE','Card Issuer Name','NUMBER','~~2','','','10','Y','','','','','','','case when  EXACDRV.order_amount <> 0  then (EXACDRV.order_amount-EXACDRV.PAYMENT_AMOUNT) else 0 end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
--Inserting Report Parameters - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rp( 'Cash Drawer Reconciliation',222,'Location','Branch Number','BRANCH_NUMBER','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cash Drawer Reconciliation',222,'From Date','From Date','CASH_DATE','>=','','','DATE','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cash Drawer Reconciliation',222,'To Date','To Date','CASH_DATE','<=','','','DATE','Y','Y','3','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rcn( 'Cash Drawer Reconciliation',222,'BRANCH_NUMBER','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cash Drawer Reconciliation',222,'TRUNC(CASH_DATE)','>=',':From Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cash Drawer Reconciliation',222,'TRUNC(CASH_DATE)','<=',':To Date','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Cash Drawer Reconciliation
--Inserting Report Triggers - Cash Drawer Reconciliation
--Inserting Report Templates - Cash Drawer Reconciliation
--Inserting Report Portals - Cash Drawer Reconciliation
--Inserting Report Dashboards - Cash Drawer Reconciliation
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 751','Dynamic 751','pie','large','Cash Amount','Cash Amount','Customer Name','Customer Name','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 752','Dynamic 752','pie','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 753','Dynamic 753','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 754','Dynamic 754','absolute line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 755','Dynamic 755','point','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 756','Dynamic 756','stacked line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 757','Dynamic 757','scatter','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 758','Dynamic 758','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 759','Dynamic 759','horizontal stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
--Inserting Report Security - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','401','','50835',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','50838',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','401','','50840',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50721',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50662',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50720',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50723',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50801',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50665',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50722',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50780',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50667',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50668',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','20434',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','20475',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','701','','50546',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','21623',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','','LB048272','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','20005','','50880',222,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on

--Report Name            : Inventory Sales and Reorder Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_PO_ISR_RPT_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_PO_ISR_RPT_V
Create or replace View XXEIS.EIS_XXWC_PO_ISR_RPT_V
(ORG,PRE,ITEM_NUMBER,VENDOR_NUM,VENDOR_NAME,SOURCE,ST,DESCRIPTION,CAT,PPLT,PLT,UOM,CL,STK_FLAG,PM,MINN,MAXN,AMU,MF_FLAG,HIT6_SALES,AVER_COST,ITEM_COST,BPA_COST,BPA,QOH,AVAILABLE,AVAILABLEDOLLAR,JAN_SALES,FEB_SALES,MAR_SALES,APR_SALES,MAY_SALES,JUNE_SALES,JUL_SALES,AUG_SALES,SEP_SALES,OCT_SALES,NOV_SALES,DEC_SALES,HIT4_SALES,ONE_SALES,SIX_SALES,TWELVE_SALES,BIN_LOC,MC,FI_FLAG,FREEZE_DATE,RES,THIRTEEN_WK_AVG_INV,THIRTEEN_WK_AN_COGS,TURNS,BUYER,TS,SO,INVENTORY_ITEM_ID,ORGANIZATION_ID,SET_OF_BOOKS_ID,ON_ORD,WT,SS,FML,OPEN_REQ,ORG_NAME,DISTRICT,REGION,SOURCING_RULE,CLT,COMMON_OUTPUT_ID,PROCESS_ID) AS 
SELECT Varchar2_Col1 "ORG",
    Varchar2_Col2 "PRE",
    Varchar2_Col3 "ITEM_NUMBER",
    Varchar2_Col4 "VENDOR_NUM",
    Varchar2_Col5 "VENDOR_NAME",
    Varchar2_Col6 "SOURCE",
    Varchar2_Col7 "ST",
    Varchar2_Col8 "DESCRIPTION",
    Varchar2_Col9 "CAT",
    Varchar2_Col10 "PPLT",
    Varchar2_Col11 "PLT",
    Varchar2_Col12 "UOM",
    Varchar2_Col13 "CL",
    Varchar2_Col14 "STK_FLAG",
    Varchar2_Col15 "PM",
    Varchar2_Col16 "MINN",
    Varchar2_Col17 "MAXN",
    Varchar2_Col18 "AMU",
    Varchar2_Col19 "MF_FLAG",
    Number_Col1 Hit6_Sales,
    Number_Col2 "AVER_COST",
    Number_Col3 "ITEM_COST",
    Number_Col4 "BPA_COST",
    Varchar2_Col20 "BPA",
    Number_Col5 "QOH",
    Number_Col6 "AVAILABLE",
    Number_Col7 "AVAILABLEDOLLAR",
    Number_Col8 Jan_Sales,
    Number_Col9 Feb_Sales,
    Number_Col10 Mar_Sales,
    Number_Col11 Apr_Sales,
    Number_Col12 May_Sales,
    Number_Col13 June_Sales,
    Number_Col14 Jul_Sales,
    Number_Col15 Aug_Sales,
    Number_Col16 Sep_Sales,
    Number_Col17 Oct_Sales,
    Number_Col18 Nov_Sales,
    Number_Col19 Dec_Sales,
    Number_Col20 Hit4_Sales,
    Number_Col21 One_Sales,
    Number_Col22 Six_Sales,
    Number_Col23 Twelve_Sales,
    Varchar2_Col21 "BIN_LOC",
    Varchar2_Col22 "MC",
    Varchar2_Col23 "FI_FLAG",
    Date_Col1 "FREEZE_DATE",
    Varchar2_Col24 "RES",
    Number_Col24 "THIRTEEN_WK_AVG_INV",
    Number_Col25 "THIRTEEN_WK_AN_COGS",
    Number_Col26 "TURNS",
    Varchar2_Col25 "BUYER",
    Varchar2_Col26 "TS",
    Number_Col27 "SO",
    Number_Col28 "INVENTORY_ITEM_ID",
    Number_Col29 "ORGANIZATION_ID",
    Number_Col30 "SET_OF_BOOKS_ID",
    number_col31 "ON_ORD",
    Number_Col32 "WT",
    Number_Col33 "SS",
    Number_Col34 "FML",
    Number_Col35 "OPEN_REQ",
    Varchar2_Col27 "ORG_NAME",
    Varchar2_Col28 "DISTRICT",
    varchar2_col29 "REGION",
    Varchar2_Col30 "SOURCING_RULE",
    Number_Col36 "CLT",
    common_output_id,
    process_id
  FROM XXEIS.EIS_RS_COMMON_OUTPUTS
/
set scan on define on
prompt Creating View Data for Inventory Sales and Reorder Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_ISR_RPT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_ISR_RPT_V',201,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Po Isr V','EXPIV');
--Delete View Columns for EIS_XXWC_PO_ISR_RPT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_ISR_RPT_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_ISR_RPT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BPA',201,'Bpa','BPA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bpa');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ITEM_COST',201,'Item Cost','ITEM_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Item Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVER_COST',201,'Aver Cost','AVER_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Aver Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AMU',201,'Amu','AMU','','','','XXEIS_RS_ADMIN','NUMBER','','','Amu');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PM',201,'Pm','PM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pm');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','STK_FLAG',201,'Stk Flag','STK_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Stk Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','CL',201,'Cl','CL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cl');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','UOM',201,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','CAT',201,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DESCRIPTION',201,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ST',201,'St','ST','','','','XXEIS_RS_ADMIN','VARCHAR2','','','St');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SOURCE',201,'Source','SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','VENDOR_NUM',201,'Vendor Num','VENDOR_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Num');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ITEM_NUMBER',201,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PRE',201,'Pre','PRE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pre');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ORG',201,'Org','ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BUYER',201,'Buyer','BUYER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TURNS',201,'Turns','TURNS','','','','XXEIS_RS_ADMIN','NUMBER','','','Turns');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','THIRTEEN_WK_AN_COGS',201,'Thirteen Wk An Cogs','THIRTEEN_WK_AN_COGS','','','','XXEIS_RS_ADMIN','NUMBER','','','Thirteen Wk An Cogs');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','THIRTEEN_WK_AVG_INV',201,'Thirteen Wk Avg Inv','THIRTEEN_WK_AVG_INV','','','','XXEIS_RS_ADMIN','NUMBER','','','Thirteen Wk Avg Inv');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','RES',201,'Res','RES','','','','XXEIS_RS_ADMIN','NUMBER','','','Res');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FREEZE_DATE',201,'Freeze Date','FREEZE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Freeze Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FI_FLAG',201,'Fi Flag','FI_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fi Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MC',201,'Mc','MC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BIN_LOC',201,'Bin Loc','BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin Loc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVAILABLEDOLLAR',201,'Availabledollar','AVAILABLEDOLLAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Availabledollar');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AVAILABLE',201,'Available','AVAILABLE','','','','XXEIS_RS_ADMIN','NUMBER','','','Available');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','QOH',201,'Qoh','QOH','','','','XXEIS_RS_ADMIN','NUMBER','','','Qoh');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ORG_NAME',201,'Org Name','ORG_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','APR_SALES',201,'Apr Sales','APR_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Apr Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','AUG_SALES',201,'Aug Sales','AUG_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Aug Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DEC_SALES',201,'Dec Sales','DEC_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Dec Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FEB_SALES',201,'Feb Sales','FEB_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Feb Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','HIT4_SALES',201,'Hit4 Sales','HIT4_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Hit4 Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','HIT6_SALES',201,'Hit6 Sales','HIT6_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Hit6 Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','JAN_SALES',201,'Jan Sales','JAN_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Jan Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','JUL_SALES',201,'Jul Sales','JUL_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Jul Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','JUNE_SALES',201,'June Sales','JUNE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','June Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAR_SALES',201,'Mar Sales','MAR_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Mar Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAY_SALES',201,'May Sales','MAY_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','May Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','NOV_SALES',201,'Nov Sales','NOV_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Nov Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','OCT_SALES',201,'Oct Sales','OCT_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Oct Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SEP_SALES',201,'Sep Sales','SEP_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sep Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ONE_SALES',201,'One Sales','ONE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','One Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SIX_SALES',201,'Six Sales','SIX_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Six Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TWELVE_SALES',201,'Twelve Sales','TWELVE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Twelve Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','DISTRICT',201,'District','DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','District');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','REGION',201,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MAXN',201,'Maxn','MAXN','','','','XXEIS_RS_ADMIN','NUMBER','','','Maxn');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MINN',201,'Minn','MINN','','','','XXEIS_RS_ADMIN','NUMBER','','','Minn');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PLT',201,'Plt','PLT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Plt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PPLT',201,'Pplt','PPLT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pplt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','TS',201,'Ts','TS','','','','XXEIS_RS_ADMIN','NUMBER','','','Ts');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','BPA_COST',201,'Bpa Cost','BPA_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Bpa Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ON_ORD',201,'On Ord','ON_ORD','','','','XXEIS_RS_ADMIN','NUMBER','','','On Ord');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','FML',201,'Fml','FML','','','','XXEIS_RS_ADMIN','NUMBER','','','Fml');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','OPEN_REQ',201,'Open Req','OPEN_REQ','','','','XXEIS_RS_ADMIN','NUMBER','','','Open Req');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','WT',201,'Wt','WT','','','','XXEIS_RS_ADMIN','NUMBER','','','Wt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SO',201,'So','SO','','','','XXEIS_RS_ADMIN','NUMBER','','','So');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SOURCING_RULE',201,'Sourcing Rule','SOURCING_RULE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sourcing Rule');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SS',201,'Ss','SS','','','','XXEIS_RS_ADMIN','NUMBER','','','Ss');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','CLT',201,'Clt','CLT','','','','XXEIS_RS_ADMIN','NUMBER','','','Clt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','MF_FLAG',201,'Mf Flag','MF_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mf Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','SET_OF_BOOKS_ID',201,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','ORGANIZATION_ID',201,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','COMMON_OUTPUT_ID',201,'Common Output Id','COMMON_OUTPUT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Common Output Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_V','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id');
--Inserting View Components for EIS_XXWC_PO_ISR_RPT_V
--Inserting View Component Joins for EIS_XXWC_PO_ISR_RPT_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Inventory Sales and Reorder Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,ood.organization_name organization_name  FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE EXISTS(SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V WHERE organization_id = ood.organization_id) ORDER BY organization_code','','PO Organization Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'    SELECT distinct Mcv.SEGMENT2 cat_class
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            MTL_ITEM_CATEGORIES MIC,
            mtl_system_items_kfv msi
            Where   Mcs.Category_Set_Name      = ''Inventory Category''
          AND MCS.STRUCTURE_ID                 = MCV.STRUCTURE_ID
          AND MIC.INVENTORY_ITEM_ID            = MSI.INVENTORY_ITEM_ID
          And Mic.Organization_Id              = msi.Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id
order by Mcv.SEGMENT2','','Catclass Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Exclude,Include,Tool Repair Only','Tool Repair','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','  ,Vendor Number,First 3 digits of Item,Item number,Source,2 Digit Cat,4 Digit Cat Class,Default buyer','Report Criteria','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Active small,All items,All items on hand, Stock items only,Non stock only,Active large, Non-stock on hand,Stock items with 0/0 min/max','EIS PO XXWC ISR REPORT COND','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Include,Time Sensitive Only','EIS PO XXWC Time Sensitive','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','No,Yes','EIS PO XXWC DC Mode','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'Select distinct segment1 from mtl_system_items_b','','XXWC Item','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Inventory Sales and Reorder Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory Sales and Reorder Report
xxeis.eis_rs_utility.delete_report_rows( 'Inventory Sales and Reorder Report' );
--Inserting Report - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.r( 201,'Inventory Sales and Reorder Report','','This report displays info needed to make inventory replenishment decisions ( by vendor, part number) at selected locations.','','','','XXEIS_RS_ADMIN','eis_xxwc_po_isr_rpt_v','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'BPA_COST','Bpa Cost','Bpa Cost','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ON_ORD','On Ord','On Ord','','','','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'APR_SALES','Apr','Apr Sales','','','','','52','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AUG_SALES','Aug','Aug Sales','','','','','56','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AVAILABLE','Avail','Available','','','','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AVAILABLEDOLLAR','Ext$','Availabledollar','','~,~2','','','35','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AVER_COST','Aver Cost','Aver Cost','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'BIN_LOC','Bin Loc','Bin Loc','','','','','39','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'BPA','Bpa#','Bpa','','','','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'BUYER','Buyer','Buyer','','','','','47','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'CAT','Cat/Cl','Cat','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'CL','Cl','Cl','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'DEC_SALES','Dec','Dec Sales','','','','','60','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'DESCRIPTION','Description','Description','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'FEB_SALES','Feb','Feb Sales','','','','','50','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'FI_FLAG','FI','Fi Flag','','','','','41','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'FREEZE_DATE','Freeze Date','Freeze Date','','','','','42','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'HIT4_SALES','Hit4','Hit4 Sales','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'HIT6_SALES','Hit6','Hit6 Sales','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ITEM_COST','Item Cost','Item Cost','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ITEM_NUMBER','Item Number','Item Number','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'JAN_SALES','Jan','Jan Sales','','','','','49','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'JUL_SALES','Jul','Jul Sales','','','','','55','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'JUNE_SALES','June','June Sales','','','','','54','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MAR_SALES','Mar','Mar Sales','','','','','51','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MAXN','Max','Maxn','NUMBER','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MAY_SALES','May','May Sales','','','','','53','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MC','MC','Mc','','','','','40','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'MINN','Min','Minn','NUMBER','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'NOV_SALES','Nov','Nov Sales','','','','','59','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'OCT_SALES','Oct','Oct Sales','','','','','58','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ONE_SALES','1','One Sales','','','','','36','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ORG','Org','Org','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'PLT','PLT','Plt','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'PM','PM','Pm','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'PPLT','PPLT','Pplt','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'PRE','Pre','Pre','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'QOH','QOH','Qoh','','','','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'RES','Res','Res','NUMBER','','','','43','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SEP_SALES','Sep','Sep Sales','','','','','57','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SIX_SALES','6','Six Sales','','','','','37','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SOURCE','Source','Source','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'ST','ST','St','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'STK_FLAG','Stk','Stk Flag','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'THIRTEEN_WK_AN_COGS','13 Wk An COGS $','Thirteen Wk An Cogs','','','','','45','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'THIRTEEN_WK_AVG_INV','13 Wk Av Inv $','Thirteen Wk Avg Inv','','','','','44','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'TS','TS','Ts','NUMBER','','','','48','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'TURNS','Turns','Turns','','','','','46','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'TWELVE_SALES','12','Twelve Sales','','','','','38','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'UOM','UOM','Uom','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'VENDOR_NAME','Vendor','Vendor Name','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'VENDOR_NUM','Vend #','Vendor Num','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'AMU','AMU','Amu','NUMBER','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'FML','FLM','Fml','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'OPEN_REQ','Req','Open Req','','','','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'WT','Wt','Wt','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SO','So','So','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SOURCING_RULE','Sourcing Rule','Sourcing Rule','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'SS','Ss','Ss','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report',201,'CLT','CLT','Clt','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_ISR_RPT_V','','');
--Inserting Report Parameters - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Organization','Organization','ORG_NAME','IN','PO Organization Lov','','VARCHAR2','N','Y','3','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'DC Mode','DC Mode','','IN','EIS PO XXWC DC Mode','''No''','VARCHAR2','Y','Y','5','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Starting Bin','Starting Bin','BIN_LOC','>=','Bin Location Lov','','VARCHAR2','N','Y','12','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Ending Bin','Ending Bin','BIN_LOC','<=','Bin Location Lov','','VARCHAR2','N','Y','13','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Tool Repair','Tool Repair','','IN','Tool Repair','''Include''','VARCHAR2','Y','Y','6','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Time Sensitive','Time Sensitive','','IN','EIS PO XXWC Time Sensitive','','VARCHAR2','N','Y','7','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Vendor','Vendor','VENDOR_NUM','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','14','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Item','Item','ITEM_NUMBER','IN','XXWC Item','','VARCHAR2','N','Y','15','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Cat Class','Cat Class','CAT','IN','Catclass Lov','','VARCHAR2','N','Y','16','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Items w/ 4 mon sales hits','Stock items w/ 4 mon sales hits > x','','IN','','','NUMERIC','N','Y','9','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Report Criteria','Report Criteria','','IN','Report Criteria','','VARCHAR2','N','Y','10','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Report Criteria Value','Report Criteria Value','','IN','','','VARCHAR2','N','Y','11','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Report Filter','Report Condition','','IN','EIS PO XXWC ISR REPORT COND','','VARCHAR2','N','Y','8','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','17','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','18','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Cat Class List','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','19','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Source List','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','20','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report',201,'Location List','Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report',201,'PROCESS_ID','IN',':SYSTEM.PROCESS_ID','','','Y','1','N','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Inventory Sales and Reorder Report
--Inserting Report Triggers - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rt( 'Inventory Sales and Reorder Report',201,'begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.G_isr_rpt_dc_mod_sub:=:DC Mode;
xxeis.eis_po_xxwc_isr_pkg.Isr_Rpt_Proc(
P_process_id => :SYSTEM.PROCESS_ID,
P_Region =>:Region,
P_District =>:District,
P_Location =>:Organization,
P_Dc_Mode =>:DC Mode,
P_Tool_Repair =>:Tool Repair,
P_Time_Sensitive =>:Time Sensitive,                            
P_Stk_Items_With_Hit4 =>:Items w/ 4 mon sales hits,
p_Report_Condition =>:Report Filter,
P_Report_Criteria            =>:Report Criteria,
P_Report_Criteria_val            =>:Report Criteria Value,
p_start_bin_loc =>:Starting Bin,
p_end_bin_loc =>:Ending Bin,
p_vendor =>:Vendor,
p_item =>:Item,
p_cat_class =>:Cat Class,
p_org_list =>:Location List,
P_ITEM_LIST =>:Item List,
P_SUPPLIER_LIST =>:Vendor List,
P_CAT_CLASS_LIST =>:Cat Class List,
P_SOURCE_LIST =>:Source List
);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Inventory Sales and Reorder Report
--Inserting Report Portals - Inventory Sales and Reorder Report
--Inserting Report Dashboards - Inventory Sales and Reorder Report
--Inserting Report Security - Inventory Sales and Reorder Report
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50868',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50903',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50869',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50621',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50873',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','20005','','50880',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50984',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report','201','','50985',201,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Inventory Sales and Reorder(ISR) Datamart
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_835_LWVGQS_V
--Connecting to schema APPS
set scan off define off
prompt Creating View APPS.XXEIS_835_LWVGQS_V
Create or replace View APPS.XXEIS_835_LWVGQS_V
(CNT) AS 
select count(*) cnt from xxeis.eis_xxwc_po_isr_tab
/
set scan on define on
prompt Creating View Data for Inventory Sales and Reorder(ISR) Datamart
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_835_LWVGQS_V
xxeis.eis_rs_ins.v( 'XXEIS_835_LWVGQS_V',201,'Paste SQL View for Inventory Sales and Reorder Datamart','1.0','','','XXEIS_RS_ADMIN','APPS','Inventory Sales and Reorder Datamart View','X8LV');
--Delete View Columns for XXEIS_835_LWVGQS_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_835_LWVGQS_V',201,FALSE);
--Inserting View Columns for XXEIS_835_LWVGQS_V
xxeis.eis_rs_ins.vc( 'XXEIS_835_LWVGQS_V','CNT',201,'','','','','','XXEIS_RS_ADMIN','NUMBER','','','Cnt');
--Inserting View Components for XXEIS_835_LWVGQS_V
--Inserting View Component Joins for XXEIS_835_LWVGQS_V
END;
/
set scan on define on
prompt Creating Report Data for Inventory Sales and Reorder(ISR) Datamart
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_utility.delete_report_rows( 'Inventory Sales and Reorder(ISR) Datamart' );
--Inserting Report - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_ins.r( 201,'Inventory Sales and Reorder(ISR) Datamart','','Datamart for Inventory Sales and Reorder Report','','','','XXEIS_RS_ADMIN','XXEIS_835_LWVGQS_V','Y','','select count(*) cnt from xxeis.eis_xxwc_po_isr_tab
','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder(ISR) Datamart',201,'CNT','Cnt','','','','','','','','Y','','','','','','','XXEIS_RS_ADMIN','','','','XXEIS_835_LWVGQS_V','','');
--Inserting Report Parameters - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Conditions - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Sorts - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Triggers - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_ins.rt( 'Inventory Sales and Reorder(ISR) Datamart',201,'Begin
xxeis.eis_po_xxwc_isr_pkg.main();
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Portals - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Dashboards - Inventory Sales and Reorder(ISR) Datamart
--Inserting Report Security - Inventory Sales and Reorder(ISR) Datamart
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder(ISR) Datamart','201','','50621',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder(ISR) Datamart','201','','50869',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder(ISR) Datamart','201','','50903',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder(ISR) Datamart','20005','','50880',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder(ISR) Datamart','201','','50984',201,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder(ISR) Datamart','201','','50985',201,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on

