--Report Name            : Inventory - COS, Shipped Prior Period
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Inventory - COS, Shipped Prior Period
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_COS_SHIPPERIOD_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_COS_SHIPPERIOD_V',401,'','','','','SA059956','XXEIS','Eis Xxwc Inv Cos Shipperiod V','EXICSV','','');
--Delete View Columns for EIS_XXWC_INV_COS_SHIPPERIOD_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_COS_SHIPPERIOD_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_COS_SHIPPERIOD_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','MEANING',401,'Meaning','MEANING','','','','SA059956','VARCHAR2','','','Meaning','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SEGMENT7',401,'Segment7','SEGMENT7','','','','SA059956','VARCHAR2','','','Segment7','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SEGMENT6',401,'Segment6','SEGMENT6','','','','SA059956','VARCHAR2','','','Segment6','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SEGMENT5',401,'Segment5','SEGMENT5','','','','SA059956','VARCHAR2','','','Segment5','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SEGMENT4',401,'Segment4','SEGMENT4','','','','SA059956','VARCHAR2','','','Segment4','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SEGMENT3',401,'Segment3','SEGMENT3','','','','SA059956','VARCHAR2','','','Segment3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SEGMENT2',401,'Segment2','SEGMENT2','','','','SA059956','VARCHAR2','','','Segment2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SEGMENT1',401,'Segment1','SEGMENT1','','','','SA059956','VARCHAR2','','','Segment1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','CONCATENATED_SEGMENTS',401,'Concatenated Segments','CONCATENATED_SEGMENTS','','','','SA059956','VARCHAR2','','','Concatenated Segments','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SUM_TRANSACTION_VALUE',401,'Sum Transaction Value','SUM_TRANSACTION_VALUE','','','','SA059956','NUMBER','','','Sum Transaction Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','PRIMARY_QUANTITY',401,'Primary Quantity','PRIMARY_QUANTITY','','','','SA059956','NUMBER','','','Primary Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','ACCOUNTING_LINE_TYPE',401,'Accounting Line Type','ACCOUNTING_LINE_TYPE','','','','SA059956','NUMBER','','','Accounting Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','ORDER_NUMBER',401,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SHIPMENT_NUMBER',401,'Shipment Number','SHIPMENT_NUMBER','','','','SA059956','NUMBER','','','Shipment Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','LINE_NUMBER',401,'Line Number','LINE_NUMBER','','','','SA059956','NUMBER','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','REFERENCE_LINE_ID',401,'Reference Line Id','REFERENCE_LINE_ID','','','','SA059956','NUMBER','','','Reference Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','TRANSACTION_TYPE_NAME',401,'Transaction Type Name','TRANSACTION_TYPE_NAME','','','','SA059956','VARCHAR2','','','Transaction Type Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','SA059956','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','MSIB_SEGMENT1',401,'Msib Segment1','MSIB_SEGMENT1','','','','SA059956','VARCHAR2','','','Msib Segment1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','PERIOD_NAME',401,'Period Name','PERIOD_NAME','','','','SA059956','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','ACCT_PERIOD_ID',401,'Acct Period Id','ACCT_PERIOD_ID','','','','SA059956','NUMBER','','','Acct Period Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','ACTUAL_COST',401,'Actual Cost','ACTUAL_COST','','','','SA059956','NUMBER','','','Actual Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','SUBINVENTORY_CODE',401,'Subinventory Code','SUBINVENTORY_CODE','','','','SA059956','VARCHAR2','','','Subinventory Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_COS_SHIPPERIOD_V','TRANSACTION_TYPE_ID',401,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','SA059956','NUMBER','','','Transaction Type Id','','','');
--Inserting View Components for EIS_XXWC_INV_COS_SHIPPERIOD_V
--Inserting View Component Joins for EIS_XXWC_INV_COS_SHIPPERIOD_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Inventory - COS, Shipped Prior Period
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Inventory - COS, Shipped Prior Period
xxeis.eis_rs_ins.lov( 401,'select distinct organization_code
from apps.mtl_parameters','','XXWC Organization Code','Organization Code from the mtl_parameters table','ID020048',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct period_name
from apps.ORG_ACCT_PERIODS','','XXWC PERIOD_NAME','PERIOD NAME FROM THE ORG_ACCT_PERIODS TABLE','ID020048',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Inventory - COS, Shipped Prior Period
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory - COS, Shipped Prior Period
xxeis.eis_rs_utility.delete_report_rows( 'Inventory - COS, Shipped Prior Period' );
--Inserting Report - Inventory - COS, Shipped Prior Period
xxeis.eis_rs_ins.r( 401,'Inventory - COS, Shipped Prior Period','','','','','','SA059956','EIS_XXWC_INV_COS_SHIPPERIOD_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Inventory - COS, Shipped Prior Period
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'ACCOUNTING_LINE_TYPE','Accounting Line Type','Accounting Line Type','','~~~','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'ACCT_PERIOD_ID','Acct Period Id','Acct Period Id','','~~~','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'ACTUAL_COST','Actual Cost','Actual Cost','','~~~','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'CONCATENATED_SEGMENTS','Concatenated Segments','Concatenated Segments','','','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'CREATION_DATE','Creation Date','Creation Date','','','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'LINE_NUMBER','Line Number','Line Number','','~~~','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'MEANING','Meaning','Meaning','','','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'MSIB_SEGMENT1','Msib Segment1','Msib Segment1','','','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'ORGANIZATION_CODE','Organization Code','Organization Code','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'PERIOD_NAME','Period Name','Period Name','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'PRIMARY_QUANTITY','Primary Quantity','Primary Quantity','','~~~','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'REFERENCE_LINE_ID','Reference Line Id','Reference Line Id','','~~~','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SEGMENT1','Segment1','Segment1','','','default','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SEGMENT2','Segment2','Segment2','','','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SEGMENT3','Segment3','Segment3','','','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SEGMENT4','Segment4','Segment4','','','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SEGMENT5','Segment5','Segment5','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SEGMENT6','Segment6','Segment6','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SEGMENT7','Segment7','Segment7','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SHIPMENT_NUMBER','Shipment Number','Shipment Number','','~~~','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SUBINVENTORY_CODE','Subinventory Code','Subinventory Code','','','default','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'SUM_TRANSACTION_VALUE','Sum Transaction Value','Sum Transaction Value','','~T~D~2','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'TRANSACTION_TYPE_ID','Transaction Type Id','Transaction Type Id','','~~~','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
xxeis.eis_rs_ins.rc( 'Inventory - COS, Shipped Prior Period',401,'TRANSACTION_TYPE_NAME','Transaction Type Name','Transaction Type Name','','','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_INV_COS_SHIPPERIOD_V','','');
--Inserting Report Parameters - Inventory - COS, Shipped Prior Period
xxeis.eis_rs_ins.rp( 'Inventory - COS, Shipped Prior Period',401,'Organization Code','Organization Code','ORGANIZATION_CODE','IN','XXWC Organization Code','','VARCHAR2','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory - COS, Shipped Prior Period',401,'Period Name','Period Name','PERIOD_NAME','IN','XXWC PERIOD_NAME','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Inventory - COS, Shipped Prior Period
xxeis.eis_rs_ins.rcn( 'Inventory - COS, Shipped Prior Period',401,'ORGANIZATION_CODE','IN',':Organization Code','','','Y','2','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Inventory - COS, Shipped Prior Period',401,'PERIOD_NAME','IN',':Period Name','','','Y','1','Y','SA059956');
--Inserting Report Sorts - Inventory - COS, Shipped Prior Period
xxeis.eis_rs_ins.rs( 'Inventory - COS, Shipped Prior Period',401,'SUM_TRANSACTION_VALUE','DESC','SA059956','1','');
--Inserting Report Triggers - Inventory - COS, Shipped Prior Period
--Inserting Report Templates - Inventory - COS, Shipped Prior Period
--Inserting Report Portals - Inventory - COS, Shipped Prior Period
--Inserting Report Dashboards - Inventory - COS, Shipped Prior Period
--Inserting Report Security - Inventory - COS, Shipped Prior Period
xxeis.eis_rs_ins.rsec( 'Inventory - COS, Shipped Prior Period','707','','51104',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Inventory - COS, Shipped Prior Period','20005','','50900',401,'SA059956','','');
--Inserting Report Pivots - Inventory - COS, Shipped Prior Period
END;
/
set scan on define on
