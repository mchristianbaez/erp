--Report Name            : HDS Account Analysis Subledger Detail Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXHDS_EIS_GL_SL_180_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object XXHDS_EIS_GL_SL_180_V
xxeis.eis_rsc_ins.v( 'XXHDS_EIS_GL_SL_180_V',101,'','','','','MM027735','XXEIS','Xxhds Eis Gl Sl 180 V','XEGS1V','','','VIEW','US','','');
--Delete Object Columns for XXHDS_EIS_GL_SL_180_V
xxeis.eis_rsc_utility.delete_view_rows('XXHDS_EIS_GL_SL_180_V',101,FALSE);
--Inserting Object Columns for XXHDS_EIS_GL_SL_180_V
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','~T~D~2','','MM027735','NUMBER','','','Sla Dist Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','MM027735','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','EFFECTIVE_PERIOD_NUM',101,'Effective Period Num','EFFECTIVE_PERIOD_NUM','','','','MM027735','NUMBER','','','Effective Period Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','~T~D~2','','MM027735','NUMBER','','','Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','~T~D~2','','MM027735','NUMBER','','','Sla Line Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','~T~D~2','','MM027735','NUMBER','','','Sla Dist Accounted Net','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','~T~D~2','','MM027735','NUMBER','','','Sla Line Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','~T~D~2','','MM027735','NUMBER','','','Sla Dist Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','~T~D~2','','MM027735','NUMBER','','','Line Acctd Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','MM027735','DATE','','','Acc Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','MM027735','VARCHAR2','','','Je Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','~T~D~2','','MM027735','NUMBER','','','Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','~T~D~2','','MM027735','NUMBER','','','Sla Line Entered Net','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LLINE',101,'Lline','LLINE','','','','MM027735','NUMBER','','','Lline','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','~T~D~2','','MM027735','NUMBER','','','Sla Line Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','TRANSACTION_NUM',101,'Transaction Num','TRANSACTION_NUM','','','','MM027735','VARCHAR2','','','Transaction Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','CURRENCY_CODE',101,'Currency Code','CURRENCY_CODE','','','','MM027735','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','MM027735','NUMBER','','','Je Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','MM027735','VARCHAR2','','','Customer Or Vendor','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','~T~D~2','','MM027735','NUMBER','','','Sla Dist Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LSEQUENCE',101,'Lsequence','LSEQUENCE','','','','MM027735','VARCHAR2','','','Lsequence','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ENTRY',101,'Entry','ENTRY','','','','MM027735','VARCHAR2','','','Entry','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ASSOCIATE_NUM',101,'Associate Num','ASSOCIATE_NUM','','','','MM027735','VARCHAR2','','','Associate Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','MM027735','VARCHAR2','','','Line Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','~T~D~2','','MM027735','NUMBER','','','Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','BATCH',101,'Batch','BATCH','','','','MM027735','NUMBER','','','Batch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','~T~D~2','','MM027735','NUMBER','','','Sla Dist Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','HNUMBER',101,'Hnumber','HNUMBER','','','','MM027735','NUMBER','','','Hnumber','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','~T~D~2','','MM027735','NUMBER','','','Sla Line Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','~T~D~2','','MM027735','NUMBER','','','Sla Line Accounted Net','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','PO_NUMBER',101,'Po Number','PO_NUMBER','','','','MM027735','VARCHAR2','','','Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','MM027735','VARCHAR2','','','Gl Account String','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','~T~D~2','','MM027735','NUMBER','','','Line Ent Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','~T~D~2','','MM027735','NUMBER','','','Sla Dist Entered Net','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','TYPE',101,'Type','TYPE','','','','MM027735','VARCHAR2','','','Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','~T~D~2','','MM027735','NUMBER','','','Line Acctd Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','~T~D~2','','MM027735','NUMBER','','','Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','MM027735','VARCHAR2','','','Batch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','NAME',101,'Name','NAME','','','','MM027735','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SEQ_NUM',101,'Seq Num','SEQ_NUM','','','','MM027735','NUMBER','','','Seq Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','~T~D~2','','MM027735','NUMBER','','','Line Ent Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SOURCE',101,'Source','SOURCE','','','','MM027735','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328ACCOUNT',101,'Gcc50328account','GCC50328ACCOUNT','','','','MM027735','VARCHAR2','','','Gcc50328account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328PRODUCT',101,'Gcc50328product','GCC50328PRODUCT','','','','MM027735','VARCHAR2','','','Gcc50328product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','MM027735','NUMBER','','','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','H_SEQ_ID',101,'H Seq Id','H_SEQ_ID','','','','MM027735','NUMBER','','','H Seq Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','MM027735','NUMBER','','','Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SEQ_ID',101,'Seq Id','SEQ_ID','','','','MM027735','NUMBER','','','Seq Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SLA_EVENT_TYPE',101,'Sla Event Type','SLA_EVENT_TYPE','','','','MM027735','VARCHAR2','','','Sla Event Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','MM027735','NUMBER','','','Gl Sl Link Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328ACCOUNTDESCR',101,'Gcc50328accountdescr','GCC50328ACCOUNTDESCR','','','','MM027735','VARCHAR2','','','Gcc50328accountdescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328COST_CENTER',101,'Gcc50328cost Center','GCC50328COST_CENTER','','','','MM027735','VARCHAR2','','','Gcc50328cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328COST_CENTERDESCR',101,'Gcc50328cost Centerdescr','GCC50328COST_CENTERDESCR','','','','MM027735','VARCHAR2','','','Gcc50328cost Centerdescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328FURTURE_USE',101,'Gcc50328furture Use','GCC50328FURTURE_USE','','','','MM027735','VARCHAR2','','','Gcc50328furture Use','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328FURTURE_USEDESCR',101,'Gcc50328furture Usedescr','GCC50328FURTURE_USEDESCR','','','','MM027735','VARCHAR2','','','Gcc50328furture Usedescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328FUTURE_USE_2',101,'Gcc50328future Use 2','GCC50328FUTURE_USE_2','','','','MM027735','VARCHAR2','','','Gcc50328future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328FUTURE_USE_2DESCR',101,'Gcc50328future Use 2descr','GCC50328FUTURE_USE_2DESCR','','','','MM027735','VARCHAR2','','','Gcc50328future Use 2descr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328LOCATION',101,'Gcc50328location','GCC50328LOCATION','','','','MM027735','VARCHAR2','','','Gcc50328location','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328LOCATIONDESCR',101,'Gcc50328locationdescr','GCC50328LOCATIONDESCR','','','','MM027735','VARCHAR2','','','Gcc50328locationdescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328PRODUCTDESCR',101,'Gcc50328productdescr','GCC50328PRODUCTDESCR','','','','MM027735','VARCHAR2','','','Gcc50328productdescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328PROJECT_CODE',101,'Gcc50328project Code','GCC50328PROJECT_CODE','','','','MM027735','VARCHAR2','','','Gcc50328project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50328PROJECT_CODEDESCR',101,'Gcc50328project Codedescr','GCC50328PROJECT_CODEDESCR','','','','MM027735','VARCHAR2','','','Gcc50328project Codedescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368ACCOUNT',101,'Gcc50368account','GCC50368ACCOUNT','','','','MM027735','VARCHAR2','','','Gcc50368account','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368ACCOUNTDESCR',101,'Gcc50368accountdescr','GCC50368ACCOUNTDESCR','','','','MM027735','VARCHAR2','','','Gcc50368accountdescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368DEPARTMENT',101,'Gcc50368department','GCC50368DEPARTMENT','','','','MM027735','VARCHAR2','','','Gcc50368department','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368DEPARTMENTDESCR',101,'Gcc50368departmentdescr','GCC50368DEPARTMENTDESCR','','','','MM027735','VARCHAR2','','','Gcc50368departmentdescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368DIVISION',101,'Gcc50368division','GCC50368DIVISION','','','','MM027735','VARCHAR2','','','Gcc50368division','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368DIVISIONDESCR',101,'Gcc50368divisiondescr','GCC50368DIVISIONDESCR','','','','MM027735','VARCHAR2','','','Gcc50368divisiondescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368FUTURE_USE',101,'Gcc50368future Use','GCC50368FUTURE_USE','','','','MM027735','VARCHAR2','','','Gcc50368future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368FUTURE_USEDESCR',101,'Gcc50368future Usedescr','GCC50368FUTURE_USEDESCR','','','','MM027735','VARCHAR2','','','Gcc50368future Usedescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368PRODUCT',101,'Gcc50368product','GCC50368PRODUCT','','','','MM027735','VARCHAR2','','','Gcc50368product','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368PRODUCTDESCR',101,'Gcc50368productdescr','GCC50368PRODUCTDESCR','','','','MM027735','VARCHAR2','','','Gcc50368productdescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368SUBACCOUNT',101,'Gcc50368subaccount','GCC50368SUBACCOUNT','','','','MM027735','VARCHAR2','','','Gcc50368subaccount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','GCC50368SUBACCOUNTDESCR',101,'Gcc50368subaccountdescr','GCC50368SUBACCOUNTDESCR','','','','MM027735','VARCHAR2','','','Gcc50368subaccountdescr','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','AP_INV_DATE',101,'Ap Inv Date','AP_INV_DATE','','','','MM027735','DATE','','','Ap Inv Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','AP_INV_SOURCE',101,'Ap Inv Source','AP_INV_SOURCE','','','','MM027735','VARCHAR2','','','Ap Inv Source','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','BOL_NUMBER',101,'Bol Number','BOL_NUMBER','','','','MM027735','VARCHAR2','','','Bol Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','CUSTOMER_OR_VENDOR_NUMBER',101,'Customer Or Vendor Number','CUSTOMER_OR_VENDOR_NUMBER','','','','MM027735','VARCHAR2','','','Customer Or Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ITEM_MATERIAL_COST',101,'Item Material Cost','ITEM_MATERIAL_COST','','~T~D~2','','MM027735','NUMBER','','','Item Material Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ITEM_OVRHD_COST',101,'Item Ovrhd Cost','ITEM_OVRHD_COST','','~T~D~2','','MM027735','NUMBER','','','Item Ovrhd Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ITEM_TXN_QTY',101,'Item Txn Qty','ITEM_TXN_QTY','','~T~D~2','','MM027735','NUMBER','','','Item Txn Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','ITEM_UNIT_WT',101,'Item Unit Wt','ITEM_UNIT_WT','','~T~D~2','','MM027735','NUMBER','','','Item Unit Wt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','PART_DESCRIPTION',101,'Part Description','PART_DESCRIPTION','','','','MM027735','VARCHAR2','','','Part Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','PART_NUMBER',101,'Part Number','PART_NUMBER','','','','MM027735','VARCHAR2','','','Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SALES_ORDER',101,'Sales Order','SALES_ORDER','','','','MM027735','VARCHAR2','','','Sales Order','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','XXCUS_IMAGE_LINK',101,'Xxcus Image Link','XXCUS_IMAGE_LINK','','','','MM027735','VARCHAR2','','','Xxcus Image Link','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','XXCUS_LINE_DESC',101,'Xxcus Line Desc','XXCUS_LINE_DESC','','','','MM027735','VARCHAR2','','','Xxcus Line Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','XXCUS_PO_CREATED_BY',101,'Xxcus Po Created By','XXCUS_PO_CREATED_BY','','','','MM027735','VARCHAR2','','','Xxcus Po Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','APPLIED_TO_SOURCE_ID_NUM_1',101,'Applied To Source Id Num 1','APPLIED_TO_SOURCE_ID_NUM_1','','','','MM027735','NUMBER','','','Applied To Source Id Num 1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','FETCH_SEQ',101,'Fetch Seq','FETCH_SEQ','','','','MM027735','NUMBER','','','Fetch Seq','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SOURCE_DIST_ID_NUM_1',101,'Source Dist Id Num 1','SOURCE_DIST_ID_NUM_1','','','','MM027735','NUMBER','','','Source Dist Id Num 1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','SOURCE_DIST_TYPE',101,'Source Dist Type','SOURCE_DIST_TYPE','','','','MM027735','VARCHAR2','','','Source Dist Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXHDS_EIS_GL_SL_180_V','WAYBILL',101,'Waybill','WAYBILL','','','','MM027735','VARCHAR2','','','Waybill','','','','US');
--Inserting Object Components for XXHDS_EIS_GL_SL_180_V
--Inserting Object Component Joins for XXHDS_EIS_GL_SL_180_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Account Analysis Subledger Detail Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.lov( 101,'SELECT distinct user_je_source_name FROM gl_je_sources','','EIS_GL_JE_SOURCES_LOV','LOV of all Available Journal Sources','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT1'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT2'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_COSTCENTER'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT3'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_COSTCENTER','XXCUS_GL_COSTCENTER','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					apps.fnd_flex_value_sets ffvs , 
					apps.fnd_flex_values ffv, 
					apps.fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
	 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT4'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PROJECT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT5'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_PROJECT','XXCUS_GL_PROJECT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'select distinct period_name
from gl_je_headers','','HDS_GL_PERIOD_NAME','GL Period Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS Account Analysis Subledger Detail Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Account Analysis Subledger Detail Report
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Account Analysis Subledger Detail Report' );
--Inserting Report - HDS Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.r( 101,'HDS Account Analysis Subledger Detail Report','','This report will provide detail from the subledger for all entries posted for the account in general ledger.

Created from ESMS#281615','','','','SA059956','XXHDS_EIS_GL_SL_180_V','Y','','','SA059956','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - HDS Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_DIST_ENT_NET','Sla Dist Ent Net','','NUMBER','~T~D~0','default','','30','Y','','','','','','','XEGS1V.SLA_DIST_ENTERED_NET*-1','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_EVENT_TYPE','Sla Event Type','Sla Event Type','','','default','','31','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ACCOUNTED_CR','Accounted Cr','Accounted Cr','','~T~D~0','default','','13','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ACCOUNTED_DR','Accounted Dr','Accounted Dr','','~T~D~0','default','','12','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ACC_DATE','Acc Date','Acc Date','','','default','','6','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','3','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'CURRENCY_CODE','Currency Code','Currency Code','','','default','','15','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'CUSTOMER_OR_VENDOR','Customer Or Vendor','Customer Or Vendor','','','default','','11','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'EFFECTIVE_PERIOD_NUM','Effective Period Num','Effective Period Num','','~T~D~0','default','','16','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ENTERED_CR','Entered Cr','Entered Cr','','~T~D~0','default','','18','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ENTERED_DR','Entered Dr','Entered Dr','','~T~D~0','default','','17','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'ENTRY','Entry','Entry','','','default','','19','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GL_ACCOUNT_STRING','Gl Account String','Gl Account String','','','default','','2','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'HNUMBER','Hnumber','Hnumber','','~T~D~0','default','','20','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'JE_CATEGORY','Je Category','Je Category','','','default','','4','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'JE_LINE_NUM','Je Line Num','Je Line Num','','~T~D~0','default','','21','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_ACCTD_CR','Line Acctd Cr','Line Acctd Cr','','~T~D~0','default','','23','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_ACCTD_DR','Line Acctd Dr','Line Acctd Dr','','~T~D~0','default','','22','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_DESCR','Line Descr','Line Descr','','','default','','5','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_ENT_CR','Line Ent Cr','Line Ent Cr','','~T~D~0','default','','25','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'LINE_ENT_DR','Line Ent Dr','Line Ent Dr','','~T~D~0','default','','24','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'NAME','Name','Name','','','default','','26','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'PERIOD_NAME','Period Name','Period Name','','','default','','1','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'PO_NUMBER','Po Number','Po Number','','','default','','8','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_CR','Sla Dist Accounted Cr','Sla Dist Accounted Cr','','~T~D~0','default','','28','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_DR','Sla Dist Accounted Dr','Sla Dist Accounted Dr','','~T~D~0','default','','27','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SLA_DIST_ACCOUNTED_NET','Sla Dist Accounted Net','Sla Dist Accounted Net','','~T~D~0','default','','29','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'SOURCE','Source','Source','','','default','','7','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'TRANSACTION_NUM','Transaction Num','Transaction Num','','','default','','9','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'TYPE','Type','Type','','','default','','10','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNT','Gcc50328account','Gcc50328account','','','default','','32','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNTDESCR','Gcc50328accountdescr','Gcc50328accountdescr','','','default','','33','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328LOCATION','Gcc50328location','Gcc50328location','','','default','','34','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328LOCATIONDESCR','Gcc50328locationdescr','Gcc50328locationdescr','','','default','','35','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328PRODUCT','Gcc50328product','Gcc50328product','','','default','','36','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328PROJECT_CODE','Gcc50328project Code','Gcc50328project Code','','','default','','37','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'GL_SL_LINK_ID','Gl Sl Link Id','Gl Sl Link Id','','~T~D~0','default','','38','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'AP_INV_DATE','Ap Inv Date','Ap Inv Date','','','default','','39','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'BOL_NUMBER','Bol Number','Bol Number','','','default','','40','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'CUSTOMER_OR_VENDOR_NUMBER','Customer Or Vendor Number','Customer Or Vendor Number','','','default','','41','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'XXCUS_IMAGE_LINK','Xxcus Image Link','Xxcus Image Link','','','default','','42','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'XXCUS_LINE_DESC','Xxcus Line Desc','Xxcus Line Desc','','','default','','43','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'XXCUS_PO_CREATED_BY','Xxcus Po Created By','Xxcus Po Created By','','','default','','44','N','','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Account Analysis Subledger Detail Report',101,'WAYBILL','Waybill','Waybill','','','default','','45','','Y','','','','','','','SA059956','N','N','','XXHDS_EIS_GL_SL_180_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Location','Gcc50328location','GCC50328LOCATION','IN','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Period Name','Period Name','PERIOD_NAME','IN','HDS_GL_PERIOD_NAME','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Product','Gcc50328product','GCC50328PRODUCT','IN','XXCUS_GL_PRODUCT','','VARCHAR2','Y','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Account Low','Gcc50328account','GCC50328ACCOUNT','>=','XXCUS_GL_ACCOUNT','','VARCHAR2','Y','Y','4','N','Y','CONSTANT','SA059956','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Cost Center','Gcc50328cost Center','GCC50328COST_CENTER','IN','XXCUS_GL_COSTCENTER','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SA059956','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Project Code','Gcc50328project Code','GCC50328PROJECT_CODE','IN','XXCUS_GL_PROJECT','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Source','Source','SOURCE','IN','EIS_GL_JE_SOURCES_LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','SA059956','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Account Analysis Subledger Detail Report',101,'Account High','Gcc50328account','GCC50328ACCOUNT','<=','XXCUS_GL_ACCOUNT','','VARCHAR2','Y','Y','5','N','Y','CONSTANT','SA059956','Y','N','','','','XXHDS_EIS_GL_SL_180_V','','','US','');
--Inserting Dependent Parameters - HDS Account Analysis Subledger Detail Report
--Inserting Report Conditions - HDS Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rcnh( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNT <= :Account High ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328ACCOUNT','','Account High','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',101,'HDS Account Analysis Subledger Detail Report','GCC50328ACCOUNT <= :Account High ');
xxeis.eis_rsc_ins.rcnh( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328ACCOUNT >= :Account Low ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328ACCOUNT','','Account Low','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',101,'HDS Account Analysis Subledger Detail Report','GCC50328ACCOUNT >= :Account Low ');
xxeis.eis_rsc_ins.rcnh( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328COST_CENTER IN :Cost Center ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328COST_CENTER','','Cost Center','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS Account Analysis Subledger Detail Report','GCC50328COST_CENTER IN :Cost Center ');
xxeis.eis_rsc_ins.rcnh( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328LOCATION IN :Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328LOCATION','','Location','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS Account Analysis Subledger Detail Report','GCC50328LOCATION IN :Location ');
xxeis.eis_rsc_ins.rcnh( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328PRODUCT IN :Product ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328PRODUCT','','Product','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS Account Analysis Subledger Detail Report','GCC50328PRODUCT IN :Product ');
xxeis.eis_rsc_ins.rcnh( 'HDS Account Analysis Subledger Detail Report',101,'GCC50328PROJECT_CODE IN :Project Code ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GCC50328PROJECT_CODE','','Project Code','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS Account Analysis Subledger Detail Report','GCC50328PROJECT_CODE IN :Project Code ');
xxeis.eis_rsc_ins.rcnh( 'HDS Account Analysis Subledger Detail Report',101,'PERIOD_NAME IN :Period Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Period Name','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS Account Analysis Subledger Detail Report','PERIOD_NAME IN :Period Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS Account Analysis Subledger Detail Report',101,'SOURCE IN :Source ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SOURCE','','Source','','','','','XXHDS_EIS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS Account Analysis Subledger Detail Report','SOURCE IN :Source ');
--Inserting Report Sorts - HDS Account Analysis Subledger Detail Report
--Inserting Report Triggers - HDS Account Analysis Subledger Detail Report
--inserting report templates - HDS Account Analysis Subledger Detail Report
--Inserting Report Portals - HDS Account Analysis Subledger Detail Report
--inserting report dashboards - HDS Account Analysis Subledger Detail Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Account Analysis Subledger Detail Report','101','XXHDS_EIS_GL_SL_180_V','XXHDS_EIS_GL_SL_180_V','N','');
--inserting report security - HDS Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','HDS_GNRL_LDGR_SPR_USR',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','HDS_GNRL_LDGR_CAD',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','GNRL_LDGR_LTMR_FSS',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','GNRL_LDGR_FSS',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXWC_GL_SETUP',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','HDS_RBTS_MNTH_END_PROCS_MNGR',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','HDS_RBTS_MNTH_END_PROCS',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','HDS_CAD_MNTH_END_PROCS',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_MANAGER_PVF',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_MANAGER_GLOBAL',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_MANAGER',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_INQUIRY_PVF',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','GNRL_LDGR_LTMR_NQR',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','HDS GL INQUIRY',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_INQUIRY',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_ACCOUNTANT_USD',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','GNRL_LDGR_LTMR_ACCNTNT',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Account Analysis Subledger Detail Report','','SS084202','',101,'SA059956','','','');
--Inserting Report Pivots - HDS Account Analysis Subledger Detail Report
--Inserting Report   Version details- HDS Account Analysis Subledger Detail Report
xxeis.eis_rsc_ins.rv( 'HDS Account Analysis Subledger Detail Report','','HDS Account Analysis Subledger Detail Report','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
