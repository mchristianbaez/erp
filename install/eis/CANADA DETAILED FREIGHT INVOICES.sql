--Report Name            : CANADA DETAILED FREIGHT INVOICES
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_716485_KCIZAR_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_716485_KCIZAR_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_716485_KCIZAR_V
 ("INVOICE_ID","ORIGIN","DESTINATION","CARRIER","FISCAL_MONTH","PRORECVD","PAID_DATE","DUE_DATE","SHIP_DATE","PRO_NO","BL_NO","WEIGHT","CUBE","PAID","BILLED","GST","HST","QST","FUNDS","PRODUCT","GL1","GL2","SHIP_TYPE","CHARGE_TO_BRANCH","SHIPPER_NAME","CONSIGNEE_NAME","PIECES","EXCH_RATE","GL_DATE","ORACLE_ACCOUNT","ORG_STATE","DST_STATE","PO_NUMBER","ORG_POSTAL","DST_POSTAL","ACCOUNTED","SOURCE","TARGET_SYS","WEEKLY_ADJUSTMENT","WEEKLY_OTHER_CHARGES","STATUS"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_716485_KCIZAR_V
 ("INVOICE_ID","ORIGIN","DESTINATION","CARRIER","FISCAL_MONTH","PRORECVD","PAID_DATE","DUE_DATE","SHIP_DATE","PRO_NO","BL_NO","WEIGHT","CUBE","PAID","BILLED","GST","HST","QST","FUNDS","PRODUCT","GL1","GL2","SHIP_TYPE","CHARGE_TO_BRANCH","SHIPPER_NAME","CONSIGNEE_NAME","PIECES","EXCH_RATE","GL_DATE","ORACLE_ACCOUNT","ORG_STATE","DST_STATE","PO_NUMBER","ORG_POSTAL","DST_POSTAL","ACCOUNTED","SOURCE","TARGET_SYS","WEEKLY_ADJUSTMENT","WEEKLY_OTHER_CHARGES","STATUS"
) as ');
l_stmt :=  'SELECT api.invoice_id ,
    tmsi.shipper_city
    || '' - ''
    || tmsi.shipper_state Origin ,
    tmsi.consignee_city
    || '' - ''
    || tmsi.consignee_st Destination ,
    aps.vendor_name Carrier ,
    apil.period_name Fiscal_Month ,
    TRUNC(api.creation_date) ProRecvd ,
    --  apc.check_date Paid_Date ,
    (
    SELECT TRUNC(apc1.check_date) check_date
    FROM ap.ap_checks_all apc1,
      ap.ap_invoice_payments_all apip
    WHERE apc1.check_id = apip.check_id
    AND APC1.ORG_ID     = 167
    AND APC1.VOID_DATE IS NULL
    AND APIP.INVOICE_ID =API.INVOICE_ID
    AND rownum          =1
    ) Paid_Date,
    apis.due_date ,
    api.invoice_date Ship_Date ,
    api.invoice_num Pro_No ,
    tmsi.bol BL_No ,
    tmsi.weight ,
    tmsi.weight cube ,
    NVL(api.amount_paid,0) Paid ,
    api.invoice_amount Billed ,
    NVL(IVL.GST, 0) GST ,
    NVL(IVL.HST, 0) HST ,
    NVL(IVL.QST, 0) QST ,
    api.payment_currency_code Funds ,
    GLA.SEGMENT1 PRODUCT ,
    (
    CASE
      WHEN XNAT.Z_NEW_VALUE = ''OVERRIDE''
      AND gla.segment1      = ''18''
      THEN
        (SELECT t.z_new_value
        FROM xxcus.xxcus_natural_acct_tbl t
        WHERE z_old_value = ''P18''
        AND target_sys    = ''SXE-FIN-CAN''
        AND PROJECTDESCR  = ''ORCL-ET DEPT OVERRIDE''
        )
      WHEN XNAT.Z_NEW_VALUE = ''OVERRIDE''
      AND gla.segment1      = ''17''
      THEN
        (SELECT t.z_new_value
        FROM xxcus.xxcus_natural_acct_tbl t
        WHERE z_old_value = ''P17''
        AND target_sys    = ''SXE-FIN-CAN''
        AND PROJECTDESCR  = ''ORCL-ET DEPT OVERRIDE''
        )
      WHEN XNAT.Z_NEW_VALUE = ''OVERRIDE''
      AND gla.segment1      = ''HC''
      THEN
        (SELECT t.z_new_value
        FROM xxcus.xxcus_natural_acct_tbl t
        WHERE z_old_value = ''PHC''
        AND target_sys    = ''SXE-FIN-CAN''
        AND PROJECTDESCR  = ''ORCL-ET DEPT OVERRIDE''
        )
      ELSE XNAT.Z_NEW_VALUE
    END)
    || ''-''
    || xnat.z_new_value2
    || ''-''
    || (
    CASE
      WHEN xnat.z_new_value3 = ''1''
      THEN NULL
      WHEN xnat.z_new_value3 = ''2''
      THEN xlct.lob_branch
      ELSE xnat.z_new_value3
    END) GL1 ,
    xlct.lob_branch
    || ''-''
    || xlct.fru_descr GL2 ,
    (SELECT fvt.description
    FROM applsys.fnd_flex_values fv ,
      applsys.fnd_flex_value_sets fvs ,
      applsys.fnd_flex_values_tl fvt
    WHERE fv.flex_value_id      = fvt.flex_value_id
    AND fvs.flex_value_set_name = ''XXCUS_GL_ACCOUNT''
    AND fv.flex_value_set_id    = fvs.flex_value_set_id
    AND fv.flex_value           = gla.segment4
    ) Ship_Type ,
    xlct.lob_branch charge_to_branch ,
    tmsi.shipper_name ,
    tmsi.consignee_name ,
    tmsi.pieces ,
    api.exchange_rate Exch_Rate ,
    TRUNC(api.gl_date) GL_Date ,
    xnat.z_old_value Oracle_Account ,
    tmsi.shipper_state Org_State ,
    tmsi.consignee_st Dst_State ,
    tmsi.po_number ,
    tmsi.shipper_zip Org_Postal ,
    tmsi.consignee_zip Dst_Postal ,
    apid.posted_flag Accounted ,
    api.source ,
    xnat.target_sys ,
    NVL(IVL.WEEKLY_ADJUSTMENT, 0) WEEKLY_ADJUSTMENT ,
    NVL(IVL.WEEKLY_OTHER_CHARGES, 0) WEEKLY_OTHER_CHARGES ,
    tmsi.status
  FROM ap.ap_invoices_all api ,
    ap.ap_invoice_lines_all apil ,
    ap.ap_invoice_distributions_all apid ,
    ap.ap_suppliers aps ,
    --  ap.ap_supplier_sites_all apsa ,
    ap.ap_payment_schedules_all apis ,
    xxcus.xxcusap_tms_inv_stg_tbl tmsi,
    xxcus.xxcus_location_code_tbl xlct ,
    (SELECT iv.invoice_id,
      SUM(
      CASE
        WHEN IV.DESCRIPTION          = ''GST''
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION          = ''GST''
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) GST,
      SUM(
      CASE
        WHEN IV.DESCRIPTION          = ''HST''
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION          = ''HST''
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) HST,
      SUM(
      CASE
        WHEN IV.DESCRIPTION         IN (''PST'', ''QST'')
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION         IN (''PST'', ''QST'')
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) QST,
      SUM(
      CASE
        WHEN IV.DESCRIPTION          = ''TMS Adjustment''
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION          = ''TMS Adjustment''
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) WEEKLY_ADJUSTMENT,
      SUM(
      CASE
        WHEN IV.DESCRIPTION NOT     IN (''GST'', ''QST'', ''HST'', ''PST'', ''TMS Adjustment'')
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION NOT     IN (''GST'', ''QST'', ''HST'', ''PST'', ''TMS Adjustment'')
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) WEEKLY_OTHER_CHARGES
    FROM ap.ap_invoice_lines_all iv,
      ap.ap_invoices_all ih
    WHERE iv.invoice_id = ih.invoice_id
    AND iv.org_id       = 167
    AND iv.line_number <> 1
    GROUP BY iv.invoice_id
    ) IVL ,
    --  (select
    --    a.invoice_id,
    --    b.po_number,
    --    b.BOL,
    --    b.pieces,
    --    b.weight,
    --    b.shipper_name,
    --    b.shipper_city,
    --    b.shipper_state,
    --    b.shipper_zip,
    --    b.consignee_name,
    --    b.consignee_city,
    --    b.consignee_st,
    --    b.consignee_zip,
    --    b.invoice_num,
    --    b.invoice_date,
    --    b.net,
    --    b.status
    --  FROM ap.ap_invoices_all a,
    --    xxcus.xxcusap_tms_inv_stg_tbl b
    --  WHERE a.invoice_num  = b.invoice_num
    --  AND a.invoice_amount = b.net
    --  AND a.invoice_date   = b.invoice_date
    --  AND a.org_id         = 167
    --  ) TMSI ,
    --  (SELECT apip.invoice_id,
    --    apc1.check_id,
    --    TRUNC(apc1.check_date) check_date
    --  FROM ap.ap_checks_all apc1,
    --    ap.ap_invoice_payments_all apip
    --  WHERE apc1.check_id = apip.check_id
    --  AND apc1.org_id     = 167
    --  AND apc1.void_date IS NULL
    --  ) APC ,
    (
    SELECT DISTINCT gcc.segment1,
      gcc.segment2,
      gcc.segment4,
      gcc.code_combination_id
    FROM gl.gl_code_combinations gcc,
      ap.ap_invoice_lines_all apil
    WHERE gcc.code_combination_id = apil.default_dist_ccid
    AND apil.org_id               = ''167''
    AND line_number               = 1
    ) GLA ,
    (SELECT DISTINCT
      CASE
        WHEN TARGET_SYS = ''GRTPLNS-FIN-CAN''
        THEN ''Great Plains-Brafasco''
        ELSE TARGET_SYS
      END TARGET_SYS,
      Z_OLD_VALUE,
      Z_NEW_VALUE,
      Z_NEW_VALUE2,
      Z_NEW_VALUE3
    FROM xxcus.xxcus_natural_acct_tbl
    WHERE system_cd = ''ORCL-CAN''
    ) XNAT
  WHERE api.invoice_id = apil.invoice_id
  AND APIL.INVOICE_ID  = APID.INVOICE_ID
  AND apil.line_number = apid.INVOICE_LINE_NUMBER --added for 20170801-00151 
  AND api.vendor_id    = aps.vendor_id
    --AND api.vendor_id             = apsa.vendor_id
  AND api.invoice_id = apis.invoice_id -- changed apil to api
  AND api.invoice_id = ivl.invoice_id(+)
    --AND api.invoice_id            = tmsi.invoice_id(+)
  AND API.INVOICE_NUM    = TMSI.INVOICE_NUM(+)
  AND API.INVOICE_AMOUNT = TMSI.NET(+)
  AND api.invoice_date   = tmsi.invoice_date(+)
    --AND api.invoice_id            = apc.invoice_id(+)
    --AND aps.vendor_id             = apsa.vendor_id
  AND apil.default_dist_ccid = gla.code_combination_id(+) --changed outer join for 20170801-00151 
  AND gla.segment4           = xnat.z_old_value(+)
  AND gla.segment2           = xlct.entrp_loc(+)
  AND xlct.system_cd         = xnat.target_sys
  AND api.org_id             = 167
  AND api.source            <> ''SelfService''

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Object Data XXEIS_716485_KCIZAR_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_716485_KCIZAR_V
xxeis.eis_rsc_ins.v( 'XXEIS_716485_KCIZAR_V',101,'Paste SQL View for CANADA DETAILED FREIGHT INVOICES','','','','ID020048','APPS','CANADA DETAILED FREIGHT INVOICES View','X7KV','','Y','VIEW','US','','','');
--Delete Object Columns for XXEIS_716485_KCIZAR_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_716485_KCIZAR_V',101,FALSE);
--Inserting Object Columns for XXEIS_716485_KCIZAR_V
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','DST_POSTAL',101,'','','','','','ID020048','VARCHAR2','','','Dst Postal','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','ACCOUNTED',101,'','','','','','ID020048','VARCHAR2','','','Accounted','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','SOURCE',101,'','','','','','ID020048','VARCHAR2','','','Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','TARGET_SYS',101,'','','','','','ID020048','VARCHAR2','','','Target Sys','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','WEEKLY_ADJUSTMENT',101,'','','','','','ID020048','NUMBER','','','Weekly Adjustment','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','WEEKLY_OTHER_CHARGES',101,'','','','','','ID020048','NUMBER','','','Weekly Other Charges','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','ORIGIN',101,'','','','','','ID020048','VARCHAR2','','','Origin','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','DESTINATION',101,'','','','','','ID020048','VARCHAR2','','','Destination','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','CARRIER',101,'','','','','','ID020048','VARCHAR2','','','Carrier','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','FISCAL_MONTH',101,'','','','','','ID020048','VARCHAR2','','','Fiscal Month','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','PRORECVD',101,'','','','','','ID020048','DATE','','','Prorecvd','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','PAID_DATE',101,'','','','','','ID020048','DATE','','','Paid Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','DUE_DATE',101,'','','','','','ID020048','DATE','','','Due Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','SHIP_DATE',101,'','','','','','ID020048','DATE','','','Ship Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','PRO_NO',101,'','','','','','ID020048','VARCHAR2','','','Pro No','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','BL_NO',101,'','','','','','ID020048','VARCHAR2','','','Bl No','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','WEIGHT',101,'','','','','','ID020048','NUMBER','','','Weight','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','CUBE',101,'','','','','','ID020048','NUMBER','','','Cube','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','PAID',101,'','','','','','ID020048','NUMBER','','','Paid','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','BILLED',101,'','','','~T~D~2','','ID020048','NUMBER','','','Billed','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','GST',101,'','','','','','ID020048','NUMBER','','','Gst','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','HST',101,'','','','','','ID020048','NUMBER','','','Hst','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','QST',101,'','','','','','ID020048','NUMBER','','','Qst','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','FUNDS',101,'','','','','','ID020048','VARCHAR2','','','Funds','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','GL1',101,'','','','','','ID020048','VARCHAR2','','','Gl1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','GL2',101,'','','','','','ID020048','VARCHAR2','','','Gl2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','SHIP_TYPE',101,'','','','','','ID020048','VARCHAR2','','','Ship Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','CHARGE_TO_BRANCH',101,'','','','','','ID020048','VARCHAR2','','','Charge To Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','SHIPPER_NAME',101,'','','','','','ID020048','VARCHAR2','','','Shipper Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','CONSIGNEE_NAME',101,'','','','','','ID020048','VARCHAR2','','','Consignee Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','PIECES',101,'','','','','','ID020048','NUMBER','','','Pieces','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','EXCH_RATE',101,'','','','~T~D~2','','ID020048','NUMBER','','','Exch Rate','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','GL_DATE',101,'','','','','','ID020048','DATE','','','Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','ORACLE_ACCOUNT',101,'','','','','','ID020048','VARCHAR2','','','Oracle Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','ORG_STATE',101,'','','','','','ID020048','VARCHAR2','','','Org State','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','DST_STATE',101,'','','','','','ID020048','VARCHAR2','','','Dst State','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','PO_NUMBER',101,'','','','','','ID020048','VARCHAR2','','','Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','ORG_POSTAL',101,'','','','','','ID020048','VARCHAR2','','','Org Postal','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','STATUS',101,'','','','','','ID020048','VARCHAR2','','','Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','INVOICE_ID',101,'','','','','','ID020048','NUMBER','','','Invoice Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_716485_KCIZAR_V','PRODUCT',101,'','','','','','ID020048','VARCHAR2','','','Product','','','','US','');
--Inserting Object Components for XXEIS_716485_KCIZAR_V
--Inserting Object Component Joins for XXEIS_716485_KCIZAR_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for CANADA DETAILED FREIGHT INVOICES
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - CANADA DETAILED FREIGHT INVOICES
xxeis.eis_rsc_ins.lov( 101,'SELECT  GPS.PERIOD_NAME period_name, LED.NAME LEDGER_NAME, GPS.PERIOD_YEAR , GPS.PERIOD_NUM, GPS.START_DATE, GPS.END_DATE
FROM    GL_PERIOD_STATUSES GPS,
             GL_LEDGERS LED
WHERE  GPS.CLOSING_STATUS IN (''O'',''C'',''P'')
AND       GPS.APPLICATION_ID = 101
AND       LED.LEDGER_ID          = GPS.SET_OF_BOOKS_ID
AND       GL_SECURITY_PKG.VALIDATE_ACCESS(LED.LEDGER_ID) = ''TRUE''
ORDER BY PERIOD_YEAR DESC, PERIOD_NUM DESC','','EIS_GL_PERIOD_NAMES_LOV','Periods with closing_status ''O'',''P'' and ''C'' from gl_period_statuses','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for CANADA DETAILED FREIGHT INVOICES
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - CANADA DETAILED FREIGHT INVOICES
xxeis.eis_rsc_utility.delete_report_rows( 'CANADA DETAILED FREIGHT INVOICES',101 );
--Inserting Report - CANADA DETAILED FREIGHT INVOICES
xxeis.eis_rsc_ins.r( 101,'CANADA DETAILED FREIGHT INVOICES','','This report captures all canada freight invoices paid and unpaid.','','','','XXEIS_RS_ADMIN','XXEIS_716485_KCIZAR_V','Y','',' SELECT api.invoice_id ,
    tmsi.shipper_city
    || '' - ''
    || tmsi.shipper_state Origin ,
    tmsi.consignee_city
    || '' - ''
    || tmsi.consignee_st Destination ,
    aps.vendor_name Carrier ,
    apil.period_name Fiscal_Month ,
    TRUNC(api.creation_date) ProRecvd ,
    --  apc.check_date Paid_Date ,
    (
    SELECT TRUNC(apc1.check_date) check_date
    FROM ap.ap_checks_all apc1,
      ap.ap_invoice_payments_all apip
    WHERE apc1.check_id = apip.check_id
    AND APC1.ORG_ID     = 167
    AND APC1.VOID_DATE IS NULL
    AND APIP.INVOICE_ID =API.INVOICE_ID
    AND rownum          =1
    ) Paid_Date,
    apis.due_date ,
    api.invoice_date Ship_Date ,
    api.invoice_num Pro_No ,
    tmsi.bol BL_No ,
    tmsi.weight ,
    tmsi.weight cube ,
    NVL(api.amount_paid,0) Paid ,
    api.invoice_amount Billed ,
    NVL(IVL.GST, 0) GST ,
    NVL(IVL.HST, 0) HST ,
    NVL(IVL.QST, 0) QST ,
    api.payment_currency_code Funds ,
    GLA.SEGMENT1 PRODUCT ,
    (
    CASE
      WHEN XNAT.Z_NEW_VALUE = ''OVERRIDE''
      AND gla.segment1      = ''18''
      THEN
        (SELECT t.z_new_value
        FROM xxcus.xxcus_natural_acct_tbl t
        WHERE z_old_value = ''P18''
        AND target_sys    = ''SXE-FIN-CAN''
        AND PROJECTDESCR  = ''ORCL-ET DEPT OVERRIDE''
        )
      WHEN XNAT.Z_NEW_VALUE = ''OVERRIDE''
      AND gla.segment1      = ''17''
      THEN
        (SELECT t.z_new_value
        FROM xxcus.xxcus_natural_acct_tbl t
        WHERE z_old_value = ''P17''
        AND target_sys    = ''SXE-FIN-CAN''
        AND PROJECTDESCR  = ''ORCL-ET DEPT OVERRIDE''
        )
      WHEN XNAT.Z_NEW_VALUE = ''OVERRIDE''
      AND gla.segment1      = ''HC''
      THEN
        (SELECT t.z_new_value
        FROM xxcus.xxcus_natural_acct_tbl t
        WHERE z_old_value = ''PHC''
        AND target_sys    = ''SXE-FIN-CAN''
        AND PROJECTDESCR  = ''ORCL-ET DEPT OVERRIDE''
        )
      ELSE XNAT.Z_NEW_VALUE
    END)
    || ''-''
    || xnat.z_new_value2
    || ''-''
    || (
    CASE
      WHEN xnat.z_new_value3 = ''1''
      THEN NULL
      WHEN xnat.z_new_value3 = ''2''
      THEN xlct.lob_branch
      ELSE xnat.z_new_value3
    END) GL1 ,
    xlct.lob_branch
    || ''-''
    || xlct.fru_descr GL2 ,
    (SELECT fvt.description
    FROM applsys.fnd_flex_values fv ,
      applsys.fnd_flex_value_sets fvs ,
      applsys.fnd_flex_values_tl fvt
    WHERE fv.flex_value_id      = fvt.flex_value_id
    AND fvs.flex_value_set_name = ''XXCUS_GL_ACCOUNT''
    AND fv.flex_value_set_id    = fvs.flex_value_set_id
    AND fv.flex_value           = gla.segment4
    ) Ship_Type ,
    xlct.lob_branch charge_to_branch ,
    tmsi.shipper_name ,
    tmsi.consignee_name ,
    tmsi.pieces ,
    api.exchange_rate Exch_Rate ,
    TRUNC(api.gl_date) GL_Date ,
    xnat.z_old_value Oracle_Account ,
    tmsi.shipper_state Org_State ,
    tmsi.consignee_st Dst_State ,
    tmsi.po_number ,
    tmsi.shipper_zip Org_Postal ,
    tmsi.consignee_zip Dst_Postal ,
    apid.posted_flag Accounted ,
    api.source ,
    xnat.target_sys ,
    NVL(IVL.WEEKLY_ADJUSTMENT, 0) WEEKLY_ADJUSTMENT ,
    NVL(IVL.WEEKLY_OTHER_CHARGES, 0) WEEKLY_OTHER_CHARGES ,
    tmsi.status
  FROM ap.ap_invoices_all api ,
    ap.ap_invoice_lines_all apil ,
    ap.ap_invoice_distributions_all apid ,
    ap.ap_suppliers aps ,
    --  ap.ap_supplier_sites_all apsa ,
    ap.ap_payment_schedules_all apis ,
    xxcus.xxcusap_tms_inv_stg_tbl tmsi,
    xxcus.xxcus_location_code_tbl xlct ,
    (SELECT iv.invoice_id,
      SUM(
      CASE
        WHEN IV.DESCRIPTION          = ''GST''
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION          = ''GST''
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) GST,
      SUM(
      CASE
        WHEN IV.DESCRIPTION          = ''HST''
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION          = ''HST''
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) HST,
      SUM(
      CASE
        WHEN IV.DESCRIPTION         IN (''PST'', ''QST'')
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION         IN (''PST'', ''QST'')
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) QST,
      SUM(
      CASE
        WHEN IV.DESCRIPTION          = ''TMS Adjustment''
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION          = ''TMS Adjustment''
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) WEEKLY_ADJUSTMENT,
      SUM(
      CASE
        WHEN IV.DESCRIPTION NOT     IN (''GST'', ''QST'', ''HST'', ''PST'', ''TMS Adjustment'')
        AND IH.PAYMENT_CURRENCY_CODE = ''USD''
        THEN IV.BASE_AMOUNT
        WHEN IV.DESCRIPTION NOT     IN (''GST'', ''QST'', ''HST'', ''PST'', ''TMS Adjustment'')
        AND IH.PAYMENT_CURRENCY_CODE = ''CAD''
        THEN IV.AMOUNT
      END) WEEKLY_OTHER_CHARGES
    FROM ap.ap_invoice_lines_all iv,
      ap.ap_invoices_all ih
    WHERE iv.invoice_id = ih.invoice_id
    AND iv.org_id       = 167
    AND iv.line_number <> 1
    GROUP BY iv.invoice_id
    ) IVL ,
    --  (select
    --    a.invoice_id,
    --    b.po_number,
    --    b.BOL,
    --    b.pieces,
    --    b.weight,
    --    b.shipper_name,
    --    b.shipper_city,
    --    b.shipper_state,
    --    b.shipper_zip,
    --    b.consignee_name,
    --    b.consignee_city,
    --    b.consignee_st,
    --    b.consignee_zip,
    --    b.invoice_num,
    --    b.invoice_date,
    --    b.net,
    --    b.status
    --  FROM ap.ap_invoices_all a,
    --    xxcus.xxcusap_tms_inv_stg_tbl b
    --  WHERE a.invoice_num  = b.invoice_num
    --  AND a.invoice_amount = b.net
    --  AND a.invoice_date   = b.invoice_date
    --  AND a.org_id         = 167
    --  ) TMSI ,
    --  (SELECT apip.invoice_id,
    --    apc1.check_id,
    --    TRUNC(apc1.check_date) check_date
    --  FROM ap.ap_checks_all apc1,
    --    ap.ap_invoice_payments_all apip
    --  WHERE apc1.check_id = apip.check_id
    --  AND apc1.org_id     = 167
    --  AND apc1.void_date IS NULL
    --  ) APC ,
    (
    SELECT DISTINCT gcc.segment1,
      gcc.segment2,
      gcc.segment4,
      gcc.code_combination_id
    FROM gl.gl_code_combinations gcc,
      ap.ap_invoice_lines_all apil
    WHERE gcc.code_combination_id = apil.default_dist_ccid
    AND apil.org_id               = ''167''
    AND line_number               = 1
    ) GLA ,
    (SELECT DISTINCT
      CASE
        WHEN TARGET_SYS = ''GRTPLNS-FIN-CAN''
        THEN ''Great Plains-Brafasco''
        ELSE TARGET_SYS
      END TARGET_SYS,
      Z_OLD_VALUE,
      Z_NEW_VALUE,
      Z_NEW_VALUE2,
      Z_NEW_VALUE3
    FROM xxcus.xxcus_natural_acct_tbl
    WHERE system_cd = ''ORCL-CAN''
    ) XNAT
  WHERE api.invoice_id = apil.invoice_id
  AND APIL.INVOICE_ID  = APID.INVOICE_ID
  AND apil.line_number = apid.INVOICE_LINE_NUMBER --added for 20170801-00151 
  AND api.vendor_id    = aps.vendor_id
    --AND api.vendor_id             = apsa.vendor_id
  AND api.invoice_id = apis.invoice_id -- changed apil to api
  AND api.invoice_id = ivl.invoice_id(+)
    --AND api.invoice_id            = tmsi.invoice_id(+)
  AND API.INVOICE_NUM    = TMSI.INVOICE_NUM(+)
  AND API.INVOICE_AMOUNT = TMSI.NET(+)
  AND api.invoice_date   = tmsi.invoice_date(+)
    --AND api.invoice_id            = apc.invoice_id(+)
    --AND aps.vendor_id             = apsa.vendor_id
  AND apil.default_dist_ccid = gla.code_combination_id(+) --changed outer join for 20170801-00151 
  AND gla.segment4           = xnat.z_old_value(+)
  AND gla.segment2           = xlct.entrp_loc(+)
  AND xlct.system_cd         = xnat.target_sys
  AND api.org_id             = 167
  AND api.source            <> ''SelfService''
','XXEIS_RS_ADMIN','','N','Canada Report','','CSV,Pivot Excel,EXCEL,','N','','','','','','N','APPS','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - CANADA DETAILED FREIGHT INVOICES
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'ACCOUNTED','Accounted','','','','','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'CUBE','Cube','','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'PAID_DATE','Paid Date','','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'BILLED','Billed','','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'CHARGE_TO_BRANCH','Charge To Branch','','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'TARGET_SYS','Target Sys','','','','','','36','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'DUE_DATE','Due Date','','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'PIECES','Pieces','','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'GL_DATE','Gl Date','','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'PAID','Paid','','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'FISCAL_MONTH','Fiscal Month','','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'PRO_NO','Pro No','','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'FUNDS','Funds','','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'HST','Hst','','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'BL_NO','Bl No','','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'SOURCE','Source','','','','','','35','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'PRORECVD','Prorecvd','','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'SHIPPER_NAME','Shipper Name','','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'WEIGHT','Weight','','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'EXCH_RATE','Exch Rate','','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'DST_STATE','Dst State','','','','','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'ORACLE_ACCOUNT','Oracle Account','','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'CONSIGNEE_NAME','Consignee Name','','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'WEEKLY_OTHER_CHARGES','Weekly Other Charges','','','','','','38','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'ORG_POSTAL','Org Postal','','','','','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'CARRIER','Carrier','','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'WEEKLY_ADJUSTMENT','Weekly Adjustment','','','','','','37','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'DESTINATION','Destination','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'PO_NUMBER','Po Number','','','','','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'GL2','Gl2','','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'QST','Qst','','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'SHIP_TYPE','Ship Type','','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'GL1','Gl1','','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'SHIP_DATE','Ship Date','','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'ORIGIN','Origin','','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'GST','Gst','','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'DST_POSTAL','Dst Postal','','','','','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'CANADA DETAILED FREIGHT INVOICES',101,'ORG_STATE','Org State','','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_716485_KCIZAR_V','','','GROUP_BY','US','','');
--Inserting Report Parameters - CANADA DETAILED FREIGHT INVOICES
xxeis.eis_rsc_ins.rp( 'CANADA DETAILED FREIGHT INVOICES',101,'Period Name','Period Name','FISCAL_MONTH','IN','EIS_GL_PERIOD_NAMES_LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_716485_KCIZAR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CANADA DETAILED FREIGHT INVOICES',101,'Gl Date From','Gl Date From','GL_DATE','>=','','','DATE','N','Y','2','N','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','XXEIS_716485_KCIZAR_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CANADA DETAILED FREIGHT INVOICES',101,'Gl Date To','Gl Date To','GL_DATE','<=','','','DATE','N','Y','3','N','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','XXEIS_716485_KCIZAR_V','','','US','');
--Inserting Dependent Parameters - CANADA DETAILED FREIGHT INVOICES
--Inserting Report Conditions - CANADA DETAILED FREIGHT INVOICES
xxeis.eis_rsc_ins.rcnh( 'CANADA DETAILED FREIGHT INVOICES',101,'X7KV.FISCAL_MONTH IN Period Name','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','FISCAL_MONTH','','Period Name','','','','','XXEIS_716485_KCIZAR_V','','','','','','IN','Y','Y','','','','','1',101,'CANADA DETAILED FREIGHT INVOICES','X7KV.FISCAL_MONTH IN Period Name');
xxeis.eis_rsc_ins.rcnh( 'CANADA DETAILED FREIGHT INVOICES',101,'X7KV.GL_DATE >= Gl Date From','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','Gl Date From','','','','','XXEIS_716485_KCIZAR_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',101,'CANADA DETAILED FREIGHT INVOICES','X7KV.GL_DATE >= Gl Date From');
xxeis.eis_rsc_ins.rcnh( 'CANADA DETAILED FREIGHT INVOICES',101,'X7KV.GL_DATE <= Gl Date To','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','Gl Date To','','','','','XXEIS_716485_KCIZAR_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',101,'CANADA DETAILED FREIGHT INVOICES','X7KV.GL_DATE <= Gl Date To');
--Inserting Report Sorts - CANADA DETAILED FREIGHT INVOICES
--Inserting Report Triggers - CANADA DETAILED FREIGHT INVOICES
--inserting report templates - CANADA DETAILED FREIGHT INVOICES
--Inserting Report Portals - CANADA DETAILED FREIGHT INVOICES
--inserting report dashboards - CANADA DETAILED FREIGHT INVOICES
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'CANADA DETAILED FREIGHT INVOICES','101','XXEIS_716485_KCIZAR_V','XXEIS_716485_KCIZAR_V','N','');
--inserting report security - CANADA DETAILED FREIGHT INVOICES
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','HDS_GNRL_LDGR_SPR_USR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','HDS_GNRL_LDGR_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','GNRL_LDGR_LTMR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','GNRL_LDGR_FSS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXWC_GL_SETUP',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','HDS_RBTS_MNTH_END_PROCS_MNGR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','HDS_RBTS_MNTH_END_PROCS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','HDS_CAD_MNTH_END_PROCS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_MANAGER_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_MANAGER_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_MANAGER',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_INQUIRY_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','GNRL_LDGR_LTMR_NQR',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','HDS GL INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_INQUIRY',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_ACCOUNTANT_USD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','GNRL_LDGR_LTMR_ACCNTNT',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_MANAGER_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_INQUIRY_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_ACCOUNTANT_USD_PS',101,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'CANADA DETAILED FREIGHT INVOICES','101','','XXCUS_GL_ACCOUNTANT_CAD_PS',101,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - CANADA DETAILED FREIGHT INVOICES
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- CANADA DETAILED FREIGHT INVOICES
xxeis.eis_rsc_ins.rv( 'CANADA DETAILED FREIGHT INVOICES','','CANADA DETAILED FREIGHT INVOICES','SA059956','11-SEP-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
