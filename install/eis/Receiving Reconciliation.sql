--Report Name            : Receiving Reconciliation
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_RECEIVING_RECON
set scan off define off
prompt Creating View XXEIS.XXEIS_RECEIVING_RECON
Create or replace View XXEIS.XXEIS_RECEIVING_RECON
 AS 
SELECT b.organization_code
           ,b.segment1
           ,b.po_unit_price
           ,b.receiving_qty
           ,b.inventory_qty
           ,b.expense_qty
           ,b.balance
           ,b.balance * b.po_unit_price ext_cost
       FROM (  SELECT a.organization_code
                     ,a.segment1
                     ,a.po_unit_price
                     ,SUM (a.receiving_qty) receiving_qty
                     ,SUM (a.inventory_qty) inventory_qty
                     ,SUM (a.expense_qty) expense_qty
                     ,SUM (a.receiving_qty) - SUM (a.inventory_qty) - SUM (a.expense_qty) balance
                 FROM (  SELECT mp.organization_code
                               ,msib.segment1
                               ,SUM (rt.primary_quantity) receiving_qty
                               ,0 inventory_qty
                               ,0 expense_qty
                               ,rt.po_unit_price
                           FROM rcv_transactions rt
                               ,po_lines_all pla
                               ,mtl_system_items_b msib
                               ,mtl_parameters mp
                          WHERE     rt.po_line_id = pla.po_line_id
                                AND pla.item_id = msib.inventory_item_id
                                AND rt.organization_id = msib.organization_id
                                AND msib.organization_id = mp.organization_id
                                /*AND mp.organization_code =
                                       NVL (xxeis.xxeis_fin_com_util_pkg.get_organization_code
                                           ,mp.organization_code)*/ -- Commented for TMS#20140619-00144 by Mahesh on 15/07/14
                                AND msib.segment1 = NVL (xxeis.xxeis_fin_com_util_pkg.get_segment1, msib.segment1)
                                AND rt.destination_type_code = 'RECEIVING'
                                AND EXISTS
                                       (SELECT *
                                          FROM org_acct_periods oap
                                         WHERE     oap.period_name =
                                                      NVL (xxeis.xxeis_fin_com_util_pkg.get_period_name
                                                          ,oap.period_name)
                                               AND oap.organization_id = rt.organization_id
                                               AND rt.transaction_date BETWEEN oap.period_start_date
                                                                           AND oap.period_close_date)
                       GROUP BY mp.organization_code
                               ,msib.segment1
                               ,rt.destination_type_code
                               ,rt.po_unit_price
                       UNION
                         SELECT mp.organization_code
                               ,msib.segment1
                               ,0
                               ,SUM (rt.primary_quantity) inventory_qty
                               ,0 expense_qty
                               ,rt.po_unit_price
                           FROM rcv_transactions rt
                               ,po_lines_all pla
                               ,mtl_system_items_b msib
                               ,mtl_parameters mp
                          WHERE     rt.po_line_id = pla.po_line_id
                                AND pla.item_id = msib.inventory_item_id
                                AND rt.organization_id = msib.organization_id
                                AND msib.organization_id = mp.organization_id
                                /*AND mp.organization_code =
                                       NVL (xxeis.xxeis_fin_com_util_pkg.get_organization_code
                                           ,mp.organization_code)*/  -- Commented for TMS#20140619-00144 by Mahesh on 15/07/14
                                AND msib.segment1 = NVL (xxeis.xxeis_fin_com_util_pkg.get_segment1, msib.segment1)
                                AND rt.destination_type_code = 'INVENTORY'
                                AND EXISTS
                                       (SELECT *
                                          FROM org_acct_periods oap
                                         WHERE     oap.period_name =
                                                      NVL (xxeis.xxeis_fin_com_util_pkg.get_period_name
                                                          ,oap.period_name)
                                               AND oap.organization_id = rt.organization_id
                                               AND rt.transaction_date BETWEEN oap.period_start_date
                                                                           AND oap.period_close_date)
                       GROUP BY mp.organization_code
                               ,msib.segment1
                               ,rt.destination_type_code
                               ,rt.po_unit_price
                       UNION
                         SELECT mp.organization_code
                               ,msib.segment1
                               ,0
                               ,SUM (rt.primary_quantity) inventory_qty
                               ,0 expense_qty
                               ,rt.po_unit_price
                           FROM rcv_transactions rt
                               ,po_lines_all pla
                               ,mtl_system_items_b msib
                               ,mtl_parameters mp
                          WHERE     rt.po_line_id = pla.po_line_id
                                AND pla.item_id = msib.inventory_item_id
                                AND rt.organization_id = msib.organization_id
                                AND msib.organization_id = mp.organization_id
                                /*AND mp.organization_code =
                                       NVL (xxeis.xxeis_fin_com_util_pkg.get_organization_code
                                           ,mp.organization_code)*/ -- Commented for TMS#20140619-00144 by Mahesh on 15/07/14
                                AND msib.segment1 = NVL (xxeis.xxeis_fin_com_util_pkg.get_segment1, msib.segment1)
                                AND rt.destination_type_code = 'EXPENSE'
                                AND EXISTS
                                       (SELECT *
                                          FROM org_acct_periods oap
                                         WHERE     oap.period_name =
                                                      NVL (xxeis.xxeis_fin_com_util_pkg.get_period_name
                                                          ,oap.period_name)
                                               AND oap.organization_id = rt.organization_id
                                               AND rt.transaction_date BETWEEN oap.period_start_date
                                                                           AND oap.period_close_date)
                       GROUP BY mp.organization_code
                               ,msib.segment1
                               ,rt.destination_type_code
                               ,rt.po_unit_price) a
             GROUP BY a.organization_code, a.segment1, a.po_unit_price) b
      WHERE b.balance <> 0
   ORDER BY b.organization_code, b.segment1/
set scan on define on
prompt Creating View Data for Receiving Reconciliation
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_RECEIVING_RECON
xxeis.eis_rs_ins.v( 'XXEIS_RECEIVING_RECON',401,'','','','','MK010916','XXEIS','Xxeis Receiving Recon','XRR','','');
--Delete View Columns for XXEIS_RECEIVING_RECON
xxeis.eis_rs_utility.delete_view_rows('XXEIS_RECEIVING_RECON',401,FALSE);
--Inserting View Columns for XXEIS_RECEIVING_RECON
xxeis.eis_rs_ins.vc( 'XXEIS_RECEIVING_RECON','RECEIVING_QTY',401,'Receiving Qty','RECEIVING_QTY','','','','MK010916','NUMBER','','','Receiving Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_RECEIVING_RECON','PO_UNIT_PRICE',401,'Po Unit Price','PO_UNIT_PRICE','','','','MK010916','NUMBER','','','Po Unit Price','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_RECEIVING_RECON','SEGMENT1',401,'Segment1','SEGMENT1','','','','MK010916','VARCHAR2','','','Segment1','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_RECEIVING_RECON','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','MK010916','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_RECEIVING_RECON','EXT_COST',401,'Ext Cost','EXT_COST','','','','MK010916','NUMBER','','','Ext Cost','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_RECEIVING_RECON','BALANCE',401,'Balance','BALANCE','','','','MK010916','NUMBER','','','Balance','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_RECEIVING_RECON','EXPENSE_QTY',401,'Expense Qty','EXPENSE_QTY','','','','MK010916','NUMBER','','','Expense Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_RECEIVING_RECON','INVENTORY_QTY',401,'Inventory Qty','INVENTORY_QTY','','','','MK010916','NUMBER','','','Inventory Qty','','','');
--Inserting View Components for XXEIS_RECEIVING_RECON
--Inserting View Component Joins for XXEIS_RECEIVING_RECON
END;
/
set scan on define on
prompt Creating Report LOV Data for Receiving Reconciliation
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Receiving Reconciliation
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC INV ORGANIZATIONS LOV','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct period_name
from apps.ORG_ACCT_PERIODS','','XXWC PERIOD_NAME','PERIOD NAME FROM THE ORG_ACCT_PERIODS TABLE','ID020048',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Receiving Reconciliation
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Receiving Reconciliation
xxeis.eis_rs_utility.delete_report_rows( 'Receiving Reconciliation' );
--Inserting Report - Receiving Reconciliation
xxeis.eis_rs_ins.r( 401,'Receiving Reconciliation','','','','','','MK010916','XXEIS_RECEIVING_RECON','Y','','','MK010916','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Receiving Reconciliation
xxeis.eis_rs_ins.rc( 'Receiving Reconciliation',401,'BALANCE','Balance','Balance','','~~~','default','','7','N','','','','','','','','MK010916','N','N','','XXEIS_RECEIVING_RECON','','');
xxeis.eis_rs_ins.rc( 'Receiving Reconciliation',401,'EXPENSE_QTY','Expense Qty','Expense Qty','','~~~','default','','6','N','','','','','','','','MK010916','N','N','','XXEIS_RECEIVING_RECON','','');
xxeis.eis_rs_ins.rc( 'Receiving Reconciliation',401,'EXT_COST','Ext Cost','Ext Cost','','~~~','default','','8','N','','','','','','','','MK010916','N','N','','XXEIS_RECEIVING_RECON','','');
xxeis.eis_rs_ins.rc( 'Receiving Reconciliation',401,'INVENTORY_QTY','Inventory Qty','Inventory Qty','','~~~','default','','5','N','','','','','','','','MK010916','N','N','','XXEIS_RECEIVING_RECON','','');
xxeis.eis_rs_ins.rc( 'Receiving Reconciliation',401,'ORGANIZATION_CODE','Organization Code','Organization Code','','','default','','1','N','','','','','','','','MK010916','N','N','','XXEIS_RECEIVING_RECON','','');
xxeis.eis_rs_ins.rc( 'Receiving Reconciliation',401,'PO_UNIT_PRICE','Po Unit Price','Po Unit Price','','~~~','default','','3','N','','','','','','','','MK010916','N','N','','XXEIS_RECEIVING_RECON','','');
xxeis.eis_rs_ins.rc( 'Receiving Reconciliation',401,'RECEIVING_QTY','Receiving Qty','Receiving Qty','','~~~','default','','4','N','','','','','','','','MK010916','N','N','','XXEIS_RECEIVING_RECON','','');
xxeis.eis_rs_ins.rc( 'Receiving Reconciliation',401,'SEGMENT1','Segment1','Segment1','','','default','','2','N','','','','','','','','MK010916','N','N','','XXEIS_RECEIVING_RECON','','');
--Inserting Report Parameters - Receiving Reconciliation
xxeis.eis_rs_ins.rp( 'Receiving Reconciliation',401,'Organization Code','','ORGANIZATION_CODE','IN','XXWC INV ORGANIZATIONS LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','MK010916','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Receiving Reconciliation',401,'ITEM','','','IN','','','VARCHAR2','N','Y','2','','N','CONSTANT','MK010916','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Receiving Reconciliation',401,'Period Name','','','IN','XXWC PERIOD_NAME','','VARCHAR2','N','Y','3','','N','CONSTANT','MK010916','Y','N','','','');
--Inserting Report Conditions - Receiving Reconciliation
xxeis.eis_rs_ins.rcn( 'Receiving Reconciliation',401,'ORGANIZATION_CODE','IN',':Organization Code','','','Y','1','Y','MK010916');
--Inserting Report Sorts - Receiving Reconciliation
--Inserting Report Triggers - Receiving Reconciliation
xxeis.eis_rs_ins.rt( 'Receiving Reconciliation',401,'begin
xxeis.xxeis_fin_com_util_pkg.g_segment1 := :ITEM;
xxeis.xxeis_fin_com_util_pkg.g_period_name := :Period Name;
end;','B','Y','MK010916');
--Inserting Report Templates - Receiving Reconciliation
--Inserting Report Portals - Receiving Reconciliation
--Inserting Report Dashboards - Receiving Reconciliation
--Inserting Report Security - Receiving Reconciliation
xxeis.eis_rs_ins.rsec( 'Receiving Reconciliation','707','','51104',401,'MK010916','','');
xxeis.eis_rs_ins.rsec( 'Receiving Reconciliation','20005','','50900',401,'MK010916','','');
xxeis.eis_rs_ins.rsec( 'Receiving Reconciliation','201','','50892',401,'MK010916','','');
--Inserting Report Pivots - Receiving Reconciliation
END;
/
set scan on define on
