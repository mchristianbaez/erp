--Report Name            : Customer Contact Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Customer Contact Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_CUST_CONT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_CUST_CONT_V',222,'','','','','MR020532','XXEIS','Eis Xxwc Ar Cust Cont V','EXACCV','','');
--Delete View Columns for EIS_XXWC_AR_CUST_CONT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_CUST_CONT_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_CUST_CONT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','FULL_NAME',222,'Full Name','FULL_NAME','','','','MR020532','VARCHAR2','','','Full Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','ACCOUNT_NAME',222,'Account Name','ACCOUNT_NAME','','','','MR020532','VARCHAR2','','','Account Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','ACCOUNT_NUMBER',222,'Account Number','ACCOUNT_NUMBER','','','','MR020532','VARCHAR2','','','Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','ADDRESS',222,'Address','ADDRESS','','','','MR020532','VARCHAR2','','','Address','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','PHONE_NUMBER',222,'Phone Number','PHONE_NUMBER','','','','MR020532','VARCHAR2','','','Phone Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','ROLE',222,'Role','ROLE','','','','MR020532','VARCHAR2','','','Role','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','CONTACT_NUMBER',222,'Contact Number','CONTACT_NUMBER','','','','MR020532','VARCHAR2','','','Contact Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','FAX',222,'Fax','FAX','','','','MR020532','VARCHAR2','','','Fax','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','SALESPERSON',222,'Salesperson','SALESPERSON','','','','MR020532','VARCHAR2','','','Salesperson','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','STATUS',222,'Status','STATUS','','','','MR020532','VARCHAR2','','','Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','EMAIL_ADDRESS',222,'Email Address','EMAIL_ADDRESS','','','','MR020532','VARCHAR2','','','Email Address','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','SITE_NAME',222,'Site Name','SITE_NAME','','','','MR020532','VARCHAR2','','','Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','SITE_NUMBER',222,'Site Number','SITE_NUMBER','','','','MR020532','VARCHAR2','','','Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','MR020532','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','LOCATION_ID',222,'Location Id','LOCATION_ID','','','','MR020532','NUMBER','','','Location Id','','','');
--Inserting View Components for EIS_XXWC_AR_CUST_CONT_V
--Inserting View Component Joins for EIS_XXWC_AR_CUST_CONT_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Customer Contact Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Customer Contact Report - WC
xxeis.eis_rs_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
    where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( '','SELECT DISTINCT FLV.MEANING,HRR.RESPONSIBILITY_TYPE
FROM FND_LOOKUP_VALUES FLV,
     HZ_ROLE_RESPONSIBILITY HRR
WHERE FLV.LOOKUP_CODE=HRR.RESPONSIBILITY_TYPE
AND   FLV.LOOKUP_TYPE=''SITE_USE_CODE''
UNION  
SELECT ''Contact Only'',''Contact_Only'' FROM DUAL','','EIS XXWC CUSTOMER ROLE','EIS XXWC CUSTOMER ROLE','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT DISTINCT res.resource_name
FROM hz_cust_site_uses_all SU,
  jtf_rs_salesreps SR,
  JTF_RS_RESOURCE_EXTNS_VL RES
WHERE 1                    = 1
AND SU.PRIMARY_SALESREP_ID = SR.SALESREP_ID (+)
AND SR.RESOURCE_ID         = RES.RESOURCE_ID (+)
AND res.resource_name     IS NOT NULL','','EIS XXWC CUSTOMER SALESREPS','EIS XXWC CUSTOMER SALESREPS','MR020532',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT DISTINCT CASE
    WHEN current_role_state =''A'' THEN
    ''ACTIVE''
    WHEN current_role_state =''I'' THEN
    ''INACTIVE''
    ELSE ''ALL''
    END STATUS
    FROM HZ_CUST_ACCOUNT_ROLES
    UNION
    SELECT ''ALL'' FROM DUAL','','XXWC CUST ACC STATUS','XXWC CUST ACC STATUS','MR020532',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Customer Contact Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Customer Contact Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'Customer Contact Report - WC' );
--Inserting Report - Customer Contact Report - WC
xxeis.eis_rs_ins.r( 222,'Customer Contact Report - WC','','EiS report to display all customer contacts','','','','MR020532','EIS_XXWC_AR_CUST_CONT_V','Y','','','MR020532','','N','White Cap Reports','PDF,','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Customer Contact Report - WC
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'ACCOUNT_NAME','Account Name','Account Name','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'ACCOUNT_NUMBER','Account Number','Account Number','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'FULL_NAME','Full Name','Full Name','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'ADDRESS','Address','Address','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'PHONE_NUMBER','Phone Number','Phone Number','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'ROLE','Role','Role','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'CONTACT_NUMBER','Contact Number','Contact Number','','','','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'FAX','Fax','Fax','','','','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'SALESPERSON','Salesperson','Salesperson','','','','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'EMAIL_ADDRESS','Email Address','Email Address','','','','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
xxeis.eis_rs_ins.rc( 'Customer Contact Report - WC',222,'STATUS','Status','Status','','','','','11','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_CUST_CONT_V','','');
--Inserting Report Parameters - Customer Contact Report - WC
xxeis.eis_rs_ins.rp( 'Customer Contact Report - WC',222,'Account Name','Account Name','ACCOUNT_NAME','IN','Customer Name','','VARCHAR2','N','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Contact Report - WC',222,'Account Number','Account Number','ACCOUNT_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Contact Report - WC',222,'Role','Role','ROLE','IN','EIS XXWC CUSTOMER ROLE','Authorized Buyer','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Contact Report - WC',222,'Sales Person','Sales Person','SALESPERSON','IN','EIS XXWC CUSTOMER SALESREPS','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Customer Contact Report - WC',222,'Status','Status','STATUS','IN','XXWC CUST ACC STATUS','ACTIVE','VARCHAR2','Y','Y','5','','N','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Customer Contact Report - WC
xxeis.eis_rs_ins.rcn( 'Customer Contact Report - WC',222,'ROLE','IN',':Role','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Customer Contact Report - WC',222,'ACCOUNT_NAME','IN',':Account Name','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Customer Contact Report - WC',222,'ACCOUNT_NUMBER','IN',':Account Number','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Customer Contact Report - WC',222,'SALESPERSON','IN',':Sales Person','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Customer Contact Report - WC',222,'','','','','AND ( ''ALL'' IN (:Status) OR (STATUS IN (:Status)))','Y','0','','MR020532');
--Inserting Report Sorts - Customer Contact Report - WC
--Inserting Report Triggers - Customer Contact Report - WC
--Inserting Report Templates - Customer Contact Report - WC
--Inserting Report Portals - Customer Contact Report - WC
--Inserting Report Dashboards - Customer Contact Report - WC
--Inserting Report Security - Customer Contact Report - WC
xxeis.eis_rs_ins.rsec( 'Customer Contact Report - WC','20005','','50900',222,'MR020532','','');
--Inserting Report Pivots - Customer Contact Report - WC
END;
/
set scan on define on
