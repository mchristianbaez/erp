--Report Name            : WC - Open Intfc Rejects
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_6468_YRFWJX_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_6468_YRFWJX_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_6468_YRFWJX_V
 ("SEGMENT1","PO_NUMBER","GROUP_ID","VENDOR_NUM","VENDOR_NAME","INVOICE_NUM","INVOICE_DATE","INVOICE_AMOUNT","ATTRIBUTE1","CREATION_DATE","REJECT_LOOKUP_CODE","SOURCE","INV_CREATE_DATE"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_6468_YRFWJX_V
 ("SEGMENT1","PO_NUMBER","GROUP_ID","VENDOR_NUM","VENDOR_NAME","INVOICE_NUM","INVOICE_DATE","INVOICE_AMOUNT","ATTRIBUTE1","CREATION_DATE","REJECT_LOOKUP_CODE","SOURCE","INV_CREATE_DATE"
) as ');
l_stmt :=  'SELECT AP.AP_SUPPLIERS.SEGMENT1
  , AP.AP_INVOICES_INTERFACE.PO_NUMBER
  , AP.AP_INVOICES_INTERFACE.GROUP_ID
  , AP.AP_INVOICES_INTERFACE.VENDOR_NUM
  , AP.AP_SUPPLIERS.VENDOR_NAME
  , AP.AP_INVOICES_INTERFACE.INVOICE_NUM
	, AP.AP_INVOICES_INTERFACE.INVOICE_DATE
	, AP.AP_INVOICES_INTERFACE.INVOICE_AMOUNT
	, AP.AP_INVOICES_INTERFACE.ATTRIBUTE1
	, AP.AP_INTERFACE_REJECTIONS.CREATION_DATE
	, AP.AP_INTERFACE_REJECTIONS.REJECT_LOOKUP_CODE
  , AP.AP_INVOICES_INTERFACE.SOURCE
  , TRUNC(AP.AP_INVOICES_INTERFACE.CREATION_DATE) INV_CREATE_DATE
FROM AP.AP_INTERFACE_REJECTIONS, AP.AP_SUPPLIERS
FULL OUTER JOIN AP.AP_INVOICES_INTERFACE
  ON AP.AP_SUPPLIERS.VENDOR_ID = AP.AP_INVOICES_INTERFACE.VENDOR_ID
  OR AP.AP_INVOICES_INTERFACE.VENDOR_NUM = AP.AP_SUPPLIERS.SEGMENT1
WHERE AP.AP_INVOICES_INTERFACE.INVOICE_ID = AP.AP_INTERFACE_REJECTIONS.PARENT_ID
  AND AP.AP_INVOICES_INTERFACE.ORG_ID = 162

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Object Data XXEIS_6468_YRFWJX_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_6468_YRFWJX_V
xxeis.eis_rsc_ins.v( 'XXEIS_6468_YRFWJX_V',200,'Paste SQL View for WC - Open Intfc Rejects','','','','10012196','APPS','WC - Open Intfc Rejects View','X6YV','','Y','VIEW','US','','','');
--Delete Object Columns for XXEIS_6468_YRFWJX_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_6468_YRFWJX_V',200,FALSE);
--Inserting Object Columns for XXEIS_6468_YRFWJX_V
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','SEGMENT1',200,'','','','','','10012196','VARCHAR2','','','Segment1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','PO_NUMBER',200,'','','','','','10012196','VARCHAR2','','','Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','GROUP_ID',200,'','','','','','10012196','VARCHAR2','','','Group Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','VENDOR_NUM',200,'','','','','','10012196','VARCHAR2','','','Vendor Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','VENDOR_NAME',200,'','','','','','10012196','VARCHAR2','','','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','INVOICE_NUM',200,'','','','','','10012196','VARCHAR2','','','Invoice Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','INVOICE_DATE',200,'','','','','','10012196','DATE','','','Invoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','INVOICE_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Invoice Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','ATTRIBUTE1',200,'','','','','','10012196','VARCHAR2','','','Attribute1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','CREATION_DATE',200,'','','','','','10012196','DATE','','','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','REJECT_LOOKUP_CODE',200,'','','','','','10012196','VARCHAR2','','','Reject Lookup Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','SOURCE',200,'','','','','','10012196','VARCHAR2','','','Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_6468_YRFWJX_V','INV_CREATE_DATE',200,'','','','','','10012196','DATE','','','Inv Create Date','','','','US','');
--Inserting Object Components for XXEIS_6468_YRFWJX_V
--Inserting Object Component Joins for XXEIS_6468_YRFWJX_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC - Open Intfc Rejects
prompt Creating Report Data for WC - Open Intfc Rejects
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC - Open Intfc Rejects
xxeis.eis_rsc_utility.delete_report_rows( 'WC - Open Intfc Rejects',200 );
--Inserting Report - WC - Open Intfc Rejects
xxeis.eis_rsc_ins.r( 200,'WC - Open Intfc Rejects','','WC - Open Intfc Rejects','','','','10012196','XXEIS_6468_YRFWJX_V','Y','','SELECT AP.AP_SUPPLIERS.SEGMENT1
  , AP.AP_INVOICES_INTERFACE.PO_NUMBER
  , AP.AP_INVOICES_INTERFACE.GROUP_ID
  , AP.AP_INVOICES_INTERFACE.VENDOR_NUM
  , AP.AP_SUPPLIERS.VENDOR_NAME
  , AP.AP_INVOICES_INTERFACE.INVOICE_NUM
	, AP.AP_INVOICES_INTERFACE.INVOICE_DATE
	, AP.AP_INVOICES_INTERFACE.INVOICE_AMOUNT
	, AP.AP_INVOICES_INTERFACE.ATTRIBUTE1
	, AP.AP_INTERFACE_REJECTIONS.CREATION_DATE
	, AP.AP_INTERFACE_REJECTIONS.REJECT_LOOKUP_CODE
  , AP.AP_INVOICES_INTERFACE.SOURCE
  , TRUNC(AP.AP_INVOICES_INTERFACE.CREATION_DATE) INV_CREATE_DATE
FROM AP.AP_INTERFACE_REJECTIONS, AP.AP_SUPPLIERS
FULL OUTER JOIN AP.AP_INVOICES_INTERFACE
  ON AP.AP_SUPPLIERS.VENDOR_ID = AP.AP_INVOICES_INTERFACE.VENDOR_ID
  OR AP.AP_INVOICES_INTERFACE.VENDOR_NUM = AP.AP_SUPPLIERS.SEGMENT1
WHERE AP.AP_INVOICES_INTERFACE.INVOICE_ID = AP.AP_INTERFACE_REJECTIONS.PARENT_ID
  AND AP.AP_INVOICES_INTERFACE.ORG_ID = 162
','10012196','','N','Invoices','','CSV,Pivot Excel,EXCEL,','','','','','','','N','APPS','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - WC - Open Intfc Rejects
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'SEGMENT1','Segment1','','','','','','1','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'PO_NUMBER','Po Number','','','','','','12','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'GROUP_ID','Group Id','','','','','','11','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'VENDOR_NUM','Vendor Num','','','','','','2','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'VENDOR_NAME','Vendor Name','','','','','','3','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'INVOICE_NUM','Invoice Num','','','','','','4','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'INVOICE_DATE','Invoice Date','','','','','','5','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'INVOICE_AMOUNT','Invoice Amount','','','','','','6','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'ATTRIBUTE1','Attribute1','','','','','','7','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'CREATION_DATE','Creation Date','','','','','','8','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'REJECT_LOOKUP_CODE','Reject Lookup Code','','','','','','10','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'SOURCE','Source','','','','','','9','N','','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects',200,'INV_CREATE_DATE','Inv Create Date','','','','','','13','','Y','','','','','','','10012196','N','N','','XXEIS_6468_YRFWJX_V','','','GROUP_BY','US','','');
--Inserting Report Parameters - WC - Open Intfc Rejects
xxeis.eis_rsc_ins.rp( 'WC - Open Intfc Rejects',200,'Inv Create Date','','INV_CREATE_DATE','=','','','DATE','N','Y','1','Y','Y','CURRENT_DATE','10012196','Y','N','','Start Date','','XXEIS_6468_YRFWJX_V','','','US','');
--Inserting Dependent Parameters - WC - Open Intfc Rejects
--Inserting Report Conditions - WC - Open Intfc Rejects
xxeis.eis_rsc_ins.rcnh( 'WC - Open Intfc Rejects',200,'X6YV.INV_CREATE_DATE = Inv Create Date','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','INV_CREATE_DATE','','Inv Create Date','','','','','XXEIS_6468_YRFWJX_V','','','','','','EQUALS','Y','Y','','','','','1',200,'WC - Open Intfc Rejects','X6YV.INV_CREATE_DATE = Inv Create Date');
--Inserting Report Sorts - WC - Open Intfc Rejects
--Inserting Report Triggers - WC - Open Intfc Rejects
--inserting report templates - WC - Open Intfc Rejects
--Inserting Report Portals - WC - Open Intfc Rejects
--inserting report dashboards - WC - Open Intfc Rejects
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC - Open Intfc Rejects','200','XXEIS_6468_YRFWJX_V','XXEIS_6468_YRFWJX_V','N','');
--inserting report security - WC - Open Intfc Rejects
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects','200','','XXWC_PAY_VENDOR_MSTR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects','200','','XXWC_PAY_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects','200','','XXWC_PAYABLES_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects','200','','XXWC_PAY_DISBURSE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects','200','','XXWC_PAY_W_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects','200','','XXWC_PAY_NO_CALENDAR',200,'10012196','','','');
--Inserting Report Pivots - WC - Open Intfc Rejects
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- WC - Open Intfc Rejects
xxeis.eis_rsc_ins.rv( 'WC - Open Intfc Rejects','','WC - Open Intfc Rejects','SA059956','08-AUG-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
