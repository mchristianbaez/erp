--Report Name            : Credit Hold Release Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Credit Hold Release Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_ORDER_HOLD_DET_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_ORDER_HOLD_DET_V',222,'','','','','MR020532','XXEIS','Eis Xxwc Ar Order Hold Det V','EXAOHDV','','');
--Delete View Columns for EIS_XXWC_AR_ORDER_HOLD_DET_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_ORDER_HOLD_DET_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_ORDER_HOLD_DET_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','INV_DUE_DATE',222,'Inv Due Date','INV_DUE_DATE','','','','MR020532','VARCHAR2','','','Inv Due Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','INV_AMOUNT_DUE_REMAINING',222,'Inv Amount Due Remaining','INV_AMOUNT_DUE_REMAINING','','','','MR020532','NUMBER','','','Inv Amount Due Remaining','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','INV_AMOUNT_DUE_ORIGINAL',222,'Inv Amount Due Original','INV_AMOUNT_DUE_ORIGINAL','','','','MR020532','NUMBER','','','Inv Amount Due Original','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','MR020532','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','MR020532','VARCHAR2','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','BR_NUMBER',222,'Br Number','BR_NUMBER','','','','MR020532','VARCHAR2','','','Br Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','BR_NAME',222,'Br Name','BR_NAME','','','','MR020532','VARCHAR2','','','Br Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','CASE_FOLDER_NUMBER',222,'Case Folder Number','CASE_FOLDER_NUMBER','','','','MR020532','VARCHAR2','','','Case Folder Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','COLLECTOR',222,'Collector','COLLECTOR','','','','MR020532','VARCHAR2','','','Collector','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','CREDIT_CLASSIFICATION',222,'Credit Classification','CREDIT_CLASSIFICATION','','','','MR020532','VARCHAR2','','','Credit Classification','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','SITE_NUMBER',222,'Site Number','SITE_NUMBER','','','','MR020532','VARCHAR2','','','Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','SITE_NAME',222,'Site Name','SITE_NAME','','','','MR020532','VARCHAR2','','','Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','ACCOUNT_NUMBER',222,'Account Number','ACCOUNT_NUMBER','','','','MR020532','VARCHAR2','','','Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','ACCOUNT_NAME',222,'Account Name','ACCOUNT_NAME','','','','MR020532','VARCHAR2','','','Account Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','ORDER_AMOUNT',222,'Order Amount','ORDER_AMOUNT','','','','MR020532','NUMBER','','','Order Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','BRANCH_NUMBER',222,'Branch Number','BRANCH_NUMBER','','','','MR020532','VARCHAR2','','','Branch Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','RELEASE_COMMENT',222,'Release Comment','RELEASE_COMMENT','','','','MR020532','VARCHAR2','','','Release Comment','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','RELEASE_REASON',222,'Release Reason','RELEASE_REASON','','','','MR020532','VARCHAR2','','','Release Reason','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','RELEASED_BY_USER_NAME',222,'Released By User Name','RELEASED_BY_USER_NAME','','','','MR020532','VARCHAR2','','','Released By User Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','RELEASED_BY_USER_ID',222,'Released By User Id','RELEASED_BY_USER_ID','','','','MR020532','VARCHAR2','','','Released By User Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','APPLIED_BY_NAME',222,'Applied By Name','APPLIED_BY_NAME','','','','MR020532','VARCHAR2','','','Applied By Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','APPLIED_BY_ID',222,'Applied By Id','APPLIED_BY_ID','','','','MR020532','VARCHAR2','','','Applied By Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','RELEASED_FLAG',222,'Released Flag','RELEASED_FLAG','','','','MR020532','VARCHAR2','','','Released Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','HOLD_NAME',222,'Hold Name','HOLD_NAME','','','','MR020532','VARCHAR2','','','Hold Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','TYPE',222,'Type','TYPE','','','','MR020532','VARCHAR2','','','Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','CREATED_BY',222,'Created By','CREATED_BY','','','','MR020532','NUMBER','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','MR020532','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','CASE_FOLDER_RELEASED_DATE',222,'Case Folder Released Date','CASE_FOLDER_RELEASED_DATE','','','','MR020532','VARCHAR2','','','Case Folder Released Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','HOLD_APPLIED_DATE',222,'Hold Applied Date','HOLD_APPLIED_DATE','','','','MR020532','VARCHAR2','','','Hold Applied Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','HOLD_RELEASED_DATE',222,'Hold Released Date','HOLD_RELEASED_DATE','','','','MR020532','VARCHAR2','','','Hold Released Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_ORDER_HOLD_DET_V','ORDER_DATE',222,'Order Date','ORDER_DATE','','','','MR020532','VARCHAR2','','','Order Date','','','');
--Inserting View Components for EIS_XXWC_AR_ORDER_HOLD_DET_V
--Inserting View Component Joins for EIS_XXWC_AR_ORDER_HOLD_DET_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Credit Hold Release Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Credit Hold Release Report - WC
xxeis.eis_rs_ins.lov( 222,'select distinct trx_number
from ra_customer_trx_all','null','INVOICES','List of All AR Invoices','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select name from ar_collectors','Null','EIS OM XXWC Credit hold','Display list of Collectors name','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT DISTINCT z.party_site_number party_site_number
              FROM hz_cust_site_uses_all x, hz_cust_acct_sites_all y, hz_party_sites z
             WHERE     x.cust_acct_site_id = y.cust_acct_site_id
                   AND y.party_site_id = z.party_site_id','null','EIS OM XXWC Site Number','Display list of site number for customers.','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','EIS AR XXWC CUSTOMER NUMBER','This gives the Customer Number','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','Select order_number from oe_order_headers_all order by order_number','','EIS OM XXWC Order Number','EIS OM XXWC Order Number','MR020532',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT DISTINCT fu.description RELEASED_BY FROM FND_USER FU,OE_HOLD_RELEASES OHR
WHERE FU.USER_ID = OHR.CREATED_BY
AND fu.user_name NOT IN (''XXWC_INT_SALESFULFILLMENT'',''SYSADMIN'',''AUTOINSTALL'')','','EIS OM XXWC Released By','EIS OM XXWC Released By','MR020532',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Credit Hold Release Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Credit Hold Release Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'Credit Hold Release Report - WC' );
--Inserting Report - Credit Hold Release Report - WC
xxeis.eis_rs_ins.r( 222,'Credit Hold Release Report - WC','','1.    Data presented should:
a.    be from Order Management and/or Receivables
b.    be for open or closed orders
c.    not be for return orders or rental returns
d.    not be for customers with a credit classification of Cash Sale
e.    not be for orders released by any individual in the following:
i.    XXWC_INT_SALESFULFILLMENT
ii.    SYSADMIN
iii.    AUTOINSTALL','','','','MR020532','EIS_XXWC_AR_ORDER_HOLD_DET_V','Y','','','MR020532','','N','White Cap Reports','','CSV,EXCEL,','');
--Inserting Report Columns - Credit Hold Release Report - WC
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'ACCOUNT_NAME','Account Name','Account Name','','','default','','1','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'ACCOUNT_NUMBER','Account Number','Account Number','','','default','','2','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'APPLIED_BY_ID','Applied By Id','Applied By Id','','','default','','3','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'APPLIED_BY_NAME','Applied By Name','Applied By Name','','','default','','4','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','5','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'BR_NAME','Br Name','Br Name','','','default','','6','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'BR_NUMBER','Br Number','Br Number','','','default','','7','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'CASE_FOLDER_NUMBER','Case Folder Number','Case Folder Number','','','default','','8','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'COLLECTOR','Collector','Collector','','','default','','10','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'CREATED_BY','Created By','Created By','','~~~','default','','11','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'CREDIT_CLASSIFICATION','Credit Classification','Credit Classification','','','default','','12','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'HOLD_APPLIED_DATE','Hold Applied Date','Hold Applied Date','','','default','','13','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'HOLD_NAME','Hold Name','Hold Name','','','default','','14','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','17','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'INV_AMOUNT_DUE_ORIGINAL','Inv Amount Due Original','Inv Amount Due Original','','~~~','default','','18','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'INV_AMOUNT_DUE_REMAINING','Inv Amount Due Remaining','Inv Amount Due Remaining','','~~~','default','','19','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'ORDER_AMOUNT','Order Amount','Order Amount','','~,~.~','default','','21','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'ORDER_DATE','Order Date','Order Date','','','default','','22','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','23','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'RELEASED_BY_USER_ID','Released By User Id','Released By User Id','','','default','','24','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'RELEASED_BY_USER_NAME','Released By User Name','Released By User Name','','','default','','25','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'RELEASED_FLAG','Released Flag','Released Flag','','','default','','26','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'RELEASE_COMMENT','Release Comment','Release Comment','','','default','','27','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'RELEASE_REASON','Release Reason','Release Reason','','','default','','28','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'SITE_NAME','Site Name','Site Name','','','default','','29','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'SITE_NUMBER','Site Number','Site Number','','','default','','30','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'TYPE','Type','Type','','','default','','31','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'CASE_FOLDER_RELEASED_DATE','Case Folder Released Date','Case Folder Released Date','','','default','','32','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'HOLD_RELEASED_DATE','Hold Released Date','Hold Released Date','','','default','','33','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','34','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
xxeis.eis_rs_ins.rc( 'Credit Hold Release Report - WC',222,'INV_DUE_DATE','Inv Due Date','Inv Due Date','','','default','','35','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_AR_ORDER_HOLD_DET_V','','');
--Inserting Report Parameters - Credit Hold Release Report - WC
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'Start Date','Start Date','HOLD_RELEASED_DATE','>=','','SELECT TRUNC (ADD_MONTHS (SYSDATE, -3)) FROM DUAL','DATE','Y','Y','1','','Y','SQL','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'End Date','End Date','HOLD_RELEASED_DATE','<=','','SELECT TRUNC (ADD_MONTHS (SYSDATE, -1)) FROM DUAL','DATE','Y','Y','2','','Y','SQL','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'Collector','Collector','COLLECTOR','IN','EIS OM XXWC Credit hold','','VARCHAR2','Y','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'Branch Name','Branch Name','BRANCH_NUMBER','IN','AR Organizaion Code LOV','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'Customer Account Number','Customer Account Number','ACCOUNT_NUMBER','IN','EIS AR XXWC CUSTOMER NUMBER','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'Customer Site Number','Customer Site Number','SITE_NUMBER','IN','EIS OM XXWC Site Number','','VARCHAR2','N','Y','6','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'Order Total','Order Total','ORDER_AMOUNT','>=','','0','NUMERIC','N','Y','7','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'Order Number','Order Number','ORDER_NUMBER','IN','EIS OM XXWC Order Number','','NUMERIC','N','Y','8','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'Invoice Number','Invoice Number','INVOICE_NUMBER','IN','INVOICES','','VARCHAR2','N','Y','9','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Credit Hold Release Report - WC',222,'Released by Name','Released by Name','RELEASED_BY_USER_NAME','IN','EIS OM XXWC Released By','','VARCHAR2','N','Y','10','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Credit Hold Release Report - WC
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'COLLECTOR','IN',':Collector','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'BRANCH_NUMBER','IN',':Branch Name','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'ACCOUNT_NUMBER','IN',':Customer Account Number','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'SITE_NUMBER','IN',':Customer Site Number','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'ORDER_AMOUNT','>=',':Order Total','','','Y','7','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'ORDER_NUMBER','IN',':Order Number','','','Y','8','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'INVOICE_NUMBER','IN',':Invoice Number','','','Y','9','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'RELEASED_BY_USER_NAME','IN',':Released by Name','','','Y','10','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'HOLD_RELEASED_DATE','>=',':Start Date','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Credit Hold Release Report - WC',222,'HOLD_RELEASED_DATE','<=',':End Date','','','Y','2','Y','MR020532');
--Inserting Report Sorts - Credit Hold Release Report - WC
--Inserting Report Triggers - Credit Hold Release Report - WC
xxeis.eis_rs_ins.rt( 'Credit Hold Release Report - WC',222,'begin 
XXEIS.EIS_WC_ORDER_HOLD_DET_PKG.populate_stage;
end;','B','Y','MR020532');
--Inserting Report Templates - Credit Hold Release Report - WC
--Inserting Report Portals - Credit Hold Release Report - WC
--Inserting Report Dashboards - Credit Hold Release Report - WC
--Inserting Report Security - Credit Hold Release Report - WC
xxeis.eis_rs_ins.rsec( 'Credit Hold Release Report - WC','20005','','50900',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Credit Hold Release Report - WC','222','','50878',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Credit Hold Release Report - WC','222','','50993',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Credit Hold Release Report - WC','222','','50854',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Credit Hold Release Report - WC','222','','50853',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Credit Hold Release Report - WC','222','','50879',222,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Credit Hold Release Report - WC','222','','50877',222,'MR020532','','');
--Inserting Report Pivots - Credit Hold Release Report - WC
END;
/
set scan on define on
