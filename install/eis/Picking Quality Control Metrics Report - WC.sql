--Report Name            : Picking Quality Control Metrics Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_PICKING_QLTY_CNTRL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PICKING_QLTY_CNTRL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PICKING_QLTY_CNTRL_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Picking Qlty Cntrl V','EXPQCV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_PICKING_QLTY_CNTRL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PICKING_QLTY_CNTRL_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_PICKING_QLTY_CNTRL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','REPORT_TYPE',660,'Report Type','REPORT_TYPE','','','','SA059956','VARCHAR2','','','Report Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','BRANCH',660,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','DISTRICT',660,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','REGION',660,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','AVG_TIME_PER_ORDER',660,'Avg Time Per Order','AVG_TIME_PER_ORDER','','','','SA059956','NUMBER','','','Avg Time Per Order','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','BR_TIME_PER_ORDER',660,'Br Time Per Order','BR_TIME_PER_ORDER','','','','SA059956','NUMBER','','','Br Time Per Order','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','AVG_TIME_PER_LINE',660,'Avg Time Per Line','AVG_TIME_PER_LINE','','','','SA059956','NUMBER','','','Avg Time Per Line','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','BR_TIME_PER_LINE',660,'Br Time Per Line','BR_TIME_PER_LINE','','','','SA059956','NUMBER','','','Br Time Per Line','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','PICKING_ACCURACY_PERCENT',660,'Picking Accuracy Percent','PICKING_ACCURACY_PERCENT','','','','SA059956','NUMBER','','','Picking Accuracy Percent','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','BR_PICKING_ACCURACY_PERCENT',660,'Br Picking Accuracy Percent','BR_PICKING_ACCURACY_PERCENT','','','','SA059956','NUMBER','','','Br Picking Accuracy Percent','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','TOTAL_NO_OF_ORDERS',660,'Total No Of Orders','TOTAL_NO_OF_ORDERS','','','','SA059956','NUMBER','','','Total No Of Orders','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','TOTAL_NO_OF_LINES',660,'Total No Of Lines','TOTAL_NO_OF_LINES','','','','SA059956','NUMBER','','','Total No Of Lines','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','OVERALL_BRANCH_RANKING',660,'Overall Branch Ranking','OVERALL_BRANCH_RANKING','','','','SA059956','NUMBER','','','Overall Branch Ranking','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','OVERALL_DISTRICT_RANKING',660,'Overall District Ranking','OVERALL_DISTRICT_RANKING','','','','SA059956','NUMBER','','','Overall District Ranking','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','OVERALL_REGIONAL_RANKING',660,'Overall Regional Ranking','OVERALL_REGIONAL_RANKING','','','','SA059956','NUMBER','','','Overall Regional Ranking','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','ASSOCIATE_NAME',660,'Associate Name','ASSOCIATE_NAME','','','','SA059956','VARCHAR2','','','Associate Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','TOTAL_NO_OF_ORDER_BY_ASSOCIATE',660,'Total No Of Order By Associate','TOTAL_NO_OF_ORDER_BY_ASSOCIATE','','','','SA059956','NUMBER','','','Total No Of Order By Associate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','TOTAL_NO_OF_LINES_BY_ASSOCIATE',660,'Total No Of Lines By Associate','TOTAL_NO_OF_LINES_BY_ASSOCIATE','','','','SA059956','NUMBER','','','Total No Of Lines By Associate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','AVGTIME_PER_ORDER_BY_ASSOCIATE',660,'Avgtime Per Order By Associate','AVGTIME_PER_ORDER_BY_ASSOCIATE','','','','SA059956','NUMBER','','','Avgtime Per Order By Associate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','AVGTIME_PER_LINE_BY_ASSOCIATE',660,'Avgtime Per Line By Associate','AVGTIME_PER_LINE_BY_ASSOCIATE','','','','SA059956','NUMBER','','','Avgtime Per Line By Associate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','PICK_ACCURACY_ASSOCIATE_PERCNT',660,'Pick Accuracy Associate Percnt','PICK_ACCURACY_ASSOCIATE_PERCNT','','','','SA059956','NUMBER','','','Pick Accuracy Associate Percnt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','SPEED_RANKING_PERCENT',660,'Speed Ranking Percent','SPEED_RANKING_PERCENT','','','','SA059956','NUMBER','','','Speed Ranking Percent','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','OVERALL_RANKING_FOR_ASSOCIATE',660,'Overall Ranking For Associate','OVERALL_RANKING_FOR_ASSOCIATE','','','','SA059956','NUMBER','','','Overall Ranking For Associate','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PICKING_QLTY_CNTRL_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','SA059956','NUMBER','','','Organization Id','','','','US','');
--Inserting Object Components for EIS_XXWC_PICKING_QLTY_CNTRL_V
--Inserting Object Component Joins for EIS_XXWC_PICKING_QLTY_CNTRL_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Picking Quality Control Metrics Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Picking Quality Control Metrics Report - WC
xxeis.eis_rsc_ins.lov( 660,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT organization_code branch,
  organization_name
FROM org_organization_definitions ood
WHERE SYSDATE < NVL (ood.disable_date, SYSDATE + 1)
AND EXISTS
  (SELECT 1
  FROM xxeis.eis_org_access_v
  WHERE organization_id = ood.organization_id
  )','','XXWC OM Organization Code','','ANONYMOUS',NULL,'Y','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','','''Summary'',''Detail''','EIS XXWC Summary/Detail LOV','EIS XXWC Summary/Detail LOV','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Picking Quality Control Metrics Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Picking Quality Control Metrics Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Picking Quality Control Metrics Report - WC',660 );
--Inserting Report - Picking Quality Control Metrics Report - WC
xxeis.eis_rsc_ins.r( 660,'Picking Quality Control Metrics Report - WC','','This report provides branch ranking and associate ranking on Picking quality control','','','','SA059956','EIS_XXWC_PICKING_QLTY_CNTRL_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Picking Quality Control Metrics Report - WC
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'ASSOCIATE_NAME','Associate','Associate Name','','','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'AVG_TIME_PER_LINE','AVG Time Per Line','Avg Time Per Line','','~T~D~2','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'AVG_TIME_PER_ORDER','AVG Time Per Order','Avg Time Per Order','','~T~D~2','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'AVGTIME_PER_LINE_BY_ASSOCIATE','AVG Time Per Line By Associate','Avgtime Per Line By Associate','','~T~D~2','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'AVGTIME_PER_ORDER_BY_ASSOCIATE','AVG Time Per Order By Associate','Avgtime Per Order By Associate','','~T~D~2','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'BR_PICKING_ACCURACY_PERCENT','BR Picking Accuracy %','Br Picking Accuracy Percent','','~~~','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'BR_TIME_PER_LINE','BR Time Per Line','Br Time Per Line','','~~~','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'BR_TIME_PER_ORDER','BR Time Per Order','Br Time Per Order','','~~~','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'BRANCH','Branch','Branch','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'DISTRICT','District','District','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'OVERALL_BRANCH_RANKING','Overall Branch Ranking','Overall Branch Ranking','','~~~','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'OVERALL_DISTRICT_RANKING','Overall District Ranking','Overall District Ranking','','~~~','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'OVERALL_RANKING_FOR_ASSOCIATE','Overall Ranking For Associate','Overall Ranking For Associate','','~~~','default','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'OVERALL_REGIONAL_RANKING','Overall Regional Ranking','Overall Regional Ranking','','~~~','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'PICK_ACCURACY_ASSOCIATE_PERCNT','Picking Accuracy Associate %','Pick Accuracy Associate Percnt','','~T~D~2','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'PICKING_ACCURACY_PERCENT','Picking Accuracy %','Picking Accuracy Percent','','~T~D~2','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'REGION','Region','Region','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'SPEED_RANKING_PERCENT','Speed accuracy % Vs Natl. AVG','Speed Ranking Percent','','~T~D~2','default','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'TOTAL_NO_OF_LINES','Total No. Of Lines','Total No Of Lines','','~~~','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'TOTAL_NO_OF_LINES_BY_ASSOCIATE','Total No. Of Lines By Associate','Total No Of Lines By Associate','','~~~','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'TOTAL_NO_OF_ORDER_BY_ASSOCIATE','Total No. Of Order By Associate','Total No Of Order By Associate','','~~~','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Picking Quality Control Metrics Report - WC',660,'TOTAL_NO_OF_ORDERS','Total No. Of Orders','Total No Of Orders','','~~~','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','US','','');
--Inserting Report Parameters - Picking Quality Control Metrics Report - WC
xxeis.eis_rsc_ins.rp( 'Picking Quality Control Metrics Report - WC',660,'Branch','Branch','BRANCH','IN','XXWC OM Organization Code','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Picking Quality Control Metrics Report - WC',660,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Picking Quality Control Metrics Report - WC',660,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Picking Quality Control Metrics Report - WC',660,'From Date','','','IN','','','DATE','Y','Y','4','N','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Picking Quality Control Metrics Report - WC',660,'To Date','','','IN','','','DATE','Y','Y','5','N','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Picking Quality Control Metrics Report - WC',660,'Summary/Detail','','','','EIS XXWC Summary/Detail LOV','Summary','VARCHAR2','Y','Y','6','N','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','US','');
--Inserting Dependent Parameters - Picking Quality Control Metrics Report - WC
--Inserting Report Conditions - Picking Quality Control Metrics Report - WC
xxeis.eis_rsc_ins.rcnh( 'Picking Quality Control Metrics Report - WC',660,'EXPQCV.BRANCH IN Branch','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','BRANCH','','Branch','','','','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','','','IN','Y','Y','','','','','1',660,'Picking Quality Control Metrics Report - WC','EXPQCV.BRANCH IN Branch');
xxeis.eis_rsc_ins.rcnh( 'Picking Quality Control Metrics Report - WC',660,'EXPQCV.DISTRICT IN District','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','','','IN','Y','Y','','','','','1',660,'Picking Quality Control Metrics Report - WC','EXPQCV.DISTRICT IN District');
xxeis.eis_rsc_ins.rcnh( 'Picking Quality Control Metrics Report - WC',660,'EXPQCV.REGION IN Region','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_PICKING_QLTY_CNTRL_V','','','','','','IN','Y','Y','','','','','1',660,'Picking Quality Control Metrics Report - WC','EXPQCV.REGION IN Region');
--Inserting Report Sorts - Picking Quality Control Metrics Report - WC
--Inserting Report Triggers - Picking Quality Control Metrics Report - WC
xxeis.eis_rsc_ins.rt( 'Picking Quality Control Metrics Report - WC',660,'  BEGIN
   XXEIS.XXWC_PICKING_QLTY_CNTRL_PKG.REMOVE_RPT_COLUMNS(
    P_REPORT_TYPE => :Summary/Detail,
    P_PROCESS_ID =>  :SYSTEM.PROCESS_ID
   );

  XXEIS.XXWC_PICKING_QLTY_CNTRL_PKG.GET_PICKING_QLTY_CNTRL_PROC(
    P_REPORT_TYPE => :Summary/Detail,
    P_BRANCH => :Branch,
    P_DISTRICT => :District,
    P_REGION => :Region,
    P_FROM_DATE => :From Date,
    P_TO_DATE => :To Date
  );
  END;','B','Y','SA059956','BQ');
--inserting report templates - Picking Quality Control Metrics Report - WC
--Inserting Report Portals - Picking Quality Control Metrics Report - WC
--inserting report dashboards - Picking Quality Control Metrics Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Picking Quality Control Metrics Report - WC','660','EIS_XXWC_PICKING_QLTY_CNTRL_V','EIS_XXWC_PICKING_QLTY_CNTRL_V','N','');
--inserting report security - Picking Quality Control Metrics Report - WC
xxeis.eis_rsc_ins.rsec( 'Picking Quality Control Metrics Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
--Inserting Report Pivots - Picking Quality Control Metrics Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Picking Quality Control Metrics Report - WC
xxeis.eis_rsc_ins.rv( 'Picking Quality Control Metrics Report - WC','','Picking Quality Control Metrics Report - WC','SA059956','10-APR-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
