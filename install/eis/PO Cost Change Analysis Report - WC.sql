--TMS#20150929-00063 modified by Mahender on 10/27/2015
--TMS#20160425-00036 modified by siva on 05/18/2016
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_PO_COST_CHNG_RPT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PO_COST_CHNG_RPT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PO_COST_CHNG_RPT_V',201,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Po Cost Chng Rpt V','EXPCCRV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_PO_COST_CHNG_RPT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PO_COST_CHNG_RPT_V',201,FALSE);
--Inserting Object Columns for EIS_XXWC_PO_COST_CHNG_RPT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CATCLASS_DESC',201,'Catclass Desc','CATCLASS_DESC','','','','ANONYMOUS','VARCHAR2','','','Catclass Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CATEGORY_DESC',201,'Category Desc','CATEGORY_DESC','','','','ANONYMOUS','VARCHAR2','','','Category Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','DISTRICT',201,'District','DISTRICT','','','','ANONYMOUS','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_QTY',201,'Po Qty','PO_QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','REGION',201,'Region','REGION','','','','ANONYMOUS','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','SUB_CATEGORY',201,'Sub Category','SUB_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Sub Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','SUB_CAT_DESC',201,'Sub Cat Desc','SUB_CAT_DESC','','','','ANONYMOUS','VARCHAR2','','','Sub Cat Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_VENDOR_NAME',201,'Po Vendor Name','PO_VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','','','Po Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_VENDOR_NUMBER',201,'Po Vendor Number','PO_VENDOR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Po Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','TRANSMISSION_METHOD',201,'Transmission Method','TRANSMISSION_METHOD','','','','ANONYMOUS','VARCHAR2','','','Transmission Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_SOURCE_SYSTEM',201,'Po Source System','PO_SOURCE_SYSTEM','','','','ANONYMOUS','VARCHAR2','','','Po Source System','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','INVOICE_COST',201,'Invoice Cost','INVOICE_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Invoice Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','BPA_NUMBER',201,'Bpa Number','BPA_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Bpa Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_COST',201,'Po Cost','PO_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Po Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_LINE_ITEM_NUMBER',201,'Po Line Item Number','PO_LINE_ITEM_NUMBER','','','','ANONYMOUS','NUMBER','','','Po Line Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CAT_CLASS',201,'Cat Class','CAT_CLASS','','','','ANONYMOUS','VARCHAR2','','','Cat Class','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CATEGORYNAME',201,'Categoryname','CATEGORYNAME','','','','ANONYMOUS','VARCHAR2','','','Categoryname','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','WC_ITEM_COST',201,'Wc Item Cost','WC_ITEM_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Wc Item Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','WC_PART_DESC',201,'Wc Part Desc','WC_PART_DESC','','','','ANONYMOUS','VARCHAR2','','','Wc Part Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','WC_PART_#',201,'Wc Part #','WC_PART_#','','','','ANONYMOUS','VARCHAR2','','','Wc Part #','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','FISCAL_YEAR',201,'Fiscal Year','FISCAL_YEAR','','','','ANONYMOUS','VARCHAR2','','','Fiscal Year','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','FISCAL_MONTH',201,'Fiscal Month','FISCAL_MONTH','','','','ANONYMOUS','VARCHAR2','','','Fiscal Month','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','BUYER_NAME',201,'Buyer Name','BUYER_NAME','','','','ANONYMOUS','VARCHAR2','','','Buyer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','SHIP_TO_DESTINATION',201,'Ship To Destination','SHIP_TO_DESTINATION','','','','ANONYMOUS','VARCHAR2','','','Ship To Destination','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_STATUS',201,'Po Status','PO_STATUS','','','','ANONYMOUS','VARCHAR2','','','Po Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_NUMBER',201,'Po Number','PO_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','BPA_PRICE',201,'Bpa Price','BPA_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Bpa Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','DELIVERED_COST_TYPE',201,'Delivered Cost Type','DELIVERED_COST_TYPE','','','','ANONYMOUS','VARCHAR2','','','Delivered Cost Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','LAST_TRANSMISSION_DATE',201,'Last Transmission Date','LAST_TRANSMISSION_DATE','','','','ANONYMOUS','VARCHAR2','','','Last Transmission Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','ORG_LIST_COST',201,'Org List Cost','ORG_LIST_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Org List Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_CREATED_DATE',201,'Po Created Date','PO_CREATED_DATE','','','','ANONYMOUS','DATE','','','Po Created Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','ABS_DEL_VS_INV_IMPACT',201,'Abs Del Vs Inv Impact','ABS_DEL_VS_INV_IMPACT','','','','ANONYMOUS','NUMBER','','','Abs Del Vs Inv Impact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','ABS_DEL_VS_SUB_IMPACT',201,'Abs Del Vs Sub Impact','ABS_DEL_VS_SUB_IMPACT','','','','ANONYMOUS','NUMBER','','','Abs Del Vs Sub Impact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','ABS_SUB_VS_INV_IMPACT',201,'Abs Sub Vs Inv Impact','ABS_SUB_VS_INV_IMPACT','','','','ANONYMOUS','NUMBER','','','Abs Sub Vs Inv Impact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','DEL_VS_INV_IMPACT',201,'Del Vs Inv Impact','DEL_VS_INV_IMPACT','','','','ANONYMOUS','NUMBER','','','Del Vs Inv Impact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','DEL_VS_SUB_IMPACT',201,'Del Vs Sub Impact','DEL_VS_SUB_IMPACT','','','','ANONYMOUS','NUMBER','','','Del Vs Sub Impact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','LINE_DISCOUNT_PERCENT',201,'Line Discount Percent','LINE_DISCOUNT_PERCENT','','','','ANONYMOUS','VARCHAR2','','','Line Discount Percent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PRIMARY_UOM',201,'Primary Uom','PRIMARY_UOM','','','','ANONYMOUS','VARCHAR2','','','Primary Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','SUB_VS_INV_IMPACT',201,'Sub Vs Inv Impact','SUB_VS_INV_IMPACT','','','','ANONYMOUS','NUMBER','','','Sub Vs Inv Impact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','SUPPLIER_ITEM',201,'Supplier Item','SUPPLIER_ITEM','','','','ANONYMOUS','VARCHAR2','','','Supplier Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','VENDOR_CATEGORY',201,'Vendor Category','VENDOR_CATEGORY','','','','ANONYMOUS','VARCHAR2','','','Vendor Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','VENDOR_TIER',201,'Vendor Tier','VENDOR_TIER','','','','ANONYMOUS','VARCHAR2','','','Vendor Tier','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','CATEGORY1D',201,'Category1d','CATEGORY1D','','','','ANONYMOUS','NUMBER','','','Category1d','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','HEADER_ID',201,'Header Id','HEADER_ID','','','','ANONYMOUS','NUMBER','','','Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','IMPACT_OF_COST',201,'Impact Of Cost','IMPACT_OF_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Impact Of Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','PO_APPROVED_DATE',201,'Po Approved Date','PO_APPROVED_DATE','','','','ANONYMOUS','DATE','','','Po Approved Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','ACCOUNTING_DATE',201,'Accounting Date','ACCOUNTING_DATE','','','','ANONYMOUS','DATE','','','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','ORGANIZATION_CODE',201,'Organization Code','ORGANIZATION_CODE','','','','ANONYMOUS','VARCHAR2','','','Organization Code','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','SOURCE',201,'Source','SOURCE','','','','ANONYMOUS','VARCHAR2','','','Source','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','INVENTORY_ITEM_STATUS_CODE',201,'Inventory Item Status Code','INVENTORY_ITEM_STATUS_CODE','','','','ANONYMOUS','VARCHAR2','','','Inventory Item Status Code','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','VENDOR_OWNER_NAME',201,'Vendor Owner Name','VENDOR_OWNER_NAME','','','','ANONYMOUS','VARCHAR2','','','Vendor Owner Name','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','VENDOR_OWNER_NUMBER',201,'Vendor Owner Number','VENDOR_OWNER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Vendor Owner Number','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PO_COST_CHNG_RPT_V','VENDOR_ID',201,'Vendor Id','VENDOR_ID','','','','ANONYMOUS','NUMBER','','','Vendor Id','','','','');
--Inserting Object Components for EIS_XXWC_PO_COST_CHNG_RPT_V
--Inserting Object Component Joins for EIS_XXWC_PO_COST_CHNG_RPT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for PO Cost Change Analysis Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - PO Cost Change Analysis Report - WC
xxeis.eis_rsc_ins.lov( 201,'Select distinct concatenated_segments item_name from mtl_system_items_b_kfv','','EIS_PO_ITEM_LIST_LOV','This is the LOV which shows all the items.','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT ood.organization_code organization_code,ood.organization_name organization_name  FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE EXISTS(SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V WHERE organization_id = ood.organization_id) ORDER BY organization_code','','PO Organization Code LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct source
       from xxeis.eis_xxwc_po_isr_tab expi,
           org_organization_definitions ood
          WHERE expi.organization_id = ood.organization_id
            and ood.operating_unit = fnd_profile.value (''ORG_ID'')','','XXWC PO Source LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT mis.inventory_item_status_code item_status
FROM mtl_item_status mis
ORDER BY 1','','EIS XXWC Item Status LOV','This LOV lists the Inventory item status','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct vendor_name,segment1 vendor_number from po_vendors order by 1','','EIS XXWC Vendor Name LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct segment1 vendor_number,vendor_name from po_vendors order by lpad(segment1,10)','','EIS XXWC Vendor Num LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for PO Cost Change Analysis Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - PO Cost Change Analysis Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'PO Cost Change Analysis Report - WC' );
--Inserting Report - PO Cost Change Analysis Report - WC
xxeis.eis_rsc_ins.r( 201,'PO Cost Change Analysis Report - WC','','It will show when the BPA cost or list price are manually overridden on purchase orders, and help us to understand when and why the changes are made.','','','','SA059956','EIS_XXWC_PO_COST_CHNG_RPT_V','Y','','','SA059956','','Y','White Cap Reports','PDF,','CSV,EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - PO Cost Change Analysis Report - WC
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'CATCLASS_DESC','Cat Class Description','Catclass Desc','','','default','','22','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'CATEGORY_DESC','Category Description','Category Desc','','','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'DISTRICT','District','District','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_QTY','Quantity','Po Qty','','~T~D~0','default','','24','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'REGION','Region','Region','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'SUB_CATEGORY','Sub Category','Sub Category','','','default','','19','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'SUB_CAT_DESC','Sub Category Description','Sub Cat Desc','','','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'BPA_NUMBER','Bpa Number','Bpa Number','','','default','','25','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'BUYER_NAME','Buyer Name','Buyer Name','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'CATEGORYNAME','Category','Categoryname','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'CAT_CLASS','Cat Class','Cat Class','','','default','','21','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'INVOICE_COST','Invoice Cost','Invoice Cost','','$~,~.~2','default','','29','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_COST','Submitted Po Cost','Po Cost','','$~,~.~2','default','','28','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_LINE_ITEM_NUMBER','Po Line Item Number','Po Line Item Number','','~T~D~0','default','','23','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_NUMBER','Po Number','Po Number','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_SOURCE_SYSTEM','Po Source System','Po Source System','','','default','','40','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_STATUS','Po Status','Po Status','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_VENDOR_NAME','Po Vendor Name','Po Vendor Name','','','default','','42','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_VENDOR_NUMBER','Po Vendor Number','Po Vendor Number','','','default','','41','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'SHIP_TO_DESTINATION','Ship To Destination','Ship To Destination','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'TRANSMISSION_METHOD','Transmission Method','Transmission Method','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'WC_ITEM_COST','Wc Item Cost','Wc Item Cost','','$~,~.~2','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'WC_PART_#','Wc Part #','Wc Part #','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'WC_PART_DESC','Wc Part Desc','Wc Part Desc','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'BPA_PRICE','Bpa Price','Bpa Price','','~T~D~0','default','','26','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'DELIVERED_COST_TYPE','Delivered Cost Type','Delivered Cost Type','','','default','','39','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'LAST_TRANSMISSION_DATE','Last Transmission Date','Last Transmission Date','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'ORG_LIST_COST','Org List Cost','Org List Cost','','~T~D~0','default','','27','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PO_CREATED_DATE','Po Created Date','Po Created Date','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'ABS_DEL_VS_INV_IMPACT','Absolute of Delivered Vs Invoice Impact','Abs Del Vs Inv Impact','','$~,~.~2','default','','31','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'ABS_DEL_VS_SUB_IMPACT','Absolute of Delivered Vs Submitted Impact','Abs Del Vs Sub Impact','','$~,~.~2','default','','33','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'ABS_SUB_VS_INV_IMPACT','Absolute  of Submitted Vs Invoice Impact','Abs Sub Vs Inv Impact','','$~,~.~2','default','','35','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'DEL_VS_INV_IMPACT','Delivered Vs Invoice Impact','Del Vs Inv Impact','','$~,~.~2','default','','30','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'DEL_VS_SUB_IMPACT','Delivered Vs Submitted Impact','Del Vs Sub Impact','','$~,~.~2','default','','32','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'LINE_DISCOUNT_PERCENT','Line Discount Percent','Line Discount Percent','','','default','','38','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'PRIMARY_UOM','Primary Uom','Primary Uom','','','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'SUB_VS_INV_IMPACT','Submitted Vs Invoice Impact','Sub Vs Inv Impact','','$~,~.~2','default','','34','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'SUPPLIER_ITEM','Supplier Item #','Supplier Item','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'VENDOR_CATEGORY','Vendor Category','Vendor Category','','','default','','37','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'VENDOR_TIER','Vendor Tier','Vendor Tier','','','default','','36','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'ORGANIZATION_CODE','Organization Code','Organization Code','','','default','','47','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'SOURCE','Source','Source','','','default','','45','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'INVENTORY_ITEM_STATUS_CODE','Item Status','Inventory Item Status Code','','','default','','46','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'VENDOR_OWNER_NAME','Vendor Owner Name','Vendor Owner Name','','','','','44','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PO Cost Change Analysis Report - WC',201,'VENDOR_OWNER_NUMBER','Vendor Owner Number','Vendor Owner Number','','','','','43','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','US','');
--Inserting Report Parameters - PO Cost Change Analysis Report - WC
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'PO Creation Date From','PO Creation Date From','PO_CREATED_DATE','>=','','','DATE','N','Y','1','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'PO Creation Date TO','PO Creation Date To','PO_CREATED_DATE','<=','','','DATE','N','Y','2','Y','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Invoiced Date From','Invoiced Date From','ACCOUNTING_DATE','>=','','','DATE','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Invoiced Date To','Invoiced Date To','ACCOUNTING_DATE','<=','','','DATE','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Organization Code','Organization Code','ORGANIZATION_CODE','IN','PO Organization Code LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Organization List','Organization List','','','XXWC Org List','','VARCHAR2','N','Y','6','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Item Number','Item Number','WC_PART_#','IN','EIS_PO_ITEM_LIST_LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Item List','Item List','','','XXWC Item List','','VARCHAR2','N','Y','8','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Vendor Owner Name','Vendor Owner Name','VENDOR_OWNER_NAME','IN','EIS XXWC Vendor Name LOV','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Vendor Owner Number','Vendor Owner Number','VENDOR_OWNER_NUMBER','IN','EIS XXWC Vendor Num LOV','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Vendor Owner List','Vendor Owner List','','','XXWC Supplier List','','VARCHAR2','N','Y','10','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Status','Status','INVENTORY_ITEM_STATUS_CODE','NOT IN','EIS XXWC Item Status LOV','''Discontinu'',''Inactive''','VARCHAR2','N','Y','13','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PO Cost Change Analysis Report - WC',201,'Source','Source','SOURCE','IN','XXWC PO Source LOV','','VARCHAR2','N','Y','12','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','US','');
--Inserting Dependent Parameters - PO Cost Change Analysis Report - WC
xxeis.eis_rsc_ins.rdp( 'PO Cost Change Analysis Report - WC',201,'VENDOR_NUMBER','Vendor Owner Name','Vendor Owner Number','IN','N','');
--Inserting Report Conditions - PO Cost Change Analysis Report - WC
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'TRUNC(PO_CREATED_DATE) >= :PO Creation Date From ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','PO Creation Date From','','','','','','','','','','','GREATER_THAN_EQUALS','Y','Y','TRUNC(PO_CREATED_DATE)','','','','1',201,'PO Cost Change Analysis Report - WC','TRUNC(PO_CREATED_DATE) >= :PO Creation Date From ');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'TRUNC(PO_CREATED_DATE) <= :PO Creation Date TO ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','PO Creation Date TO','','','','','','','','','','','LESS_THAN_EQUALS','Y','Y','TRUNC(PO_CREATED_DATE)','','','','1',201,'PO Cost Change Analysis Report - WC','TRUNC(PO_CREATED_DATE) <= :PO Creation Date TO ');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'EXPCCRV.ACCOUNTING_DATE >= Invoiced Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNTING_DATE','','Invoiced Date From','','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'PO Cost Change Analysis Report - WC','EXPCCRV.ACCOUNTING_DATE >= Invoiced Date From');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'EXPCCRV.ACCOUNTING_DATE <= Invoiced Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNTING_DATE','','Invoiced Date To','','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'PO Cost Change Analysis Report - WC','EXPCCRV.ACCOUNTING_DATE <= Invoiced Date To');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'EXPCCRV.ORGANIZATION_CODE IN Organization','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORGANIZATION_CODE','','Organization Code','','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'PO Cost Change Analysis Report - WC','EXPCCRV.ORGANIZATION_CODE IN Organization');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'EXPCCRV.WC_PART_# IN Item Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','WC_PART_#','','Item Number','','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'PO Cost Change Analysis Report - WC','EXPCCRV.WC_PART_# IN Item Number');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'FreeText','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','AND (:Organization List is null or EXISTS (SELECT 1  FROM
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME= :Organization List
                            AND LIST_TYPE =''Org''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                         EXPCCRV.ORGANIZATION_CODE, 1, 1)<>0)
     )
AND (:Item List is null or EXISTS (SELECT 1  FROM
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME= :Item List
                            AND LIST_TYPE =''Item''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                          EXPCCRV.WC_PART_#, 1, 1)<>0)
     )
AND (:Vendor Owner List is null or EXISTS (SELECT 1  FROM
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME= :Vendor Owner List
                            AND LIST_TYPE =''Supplier''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                          EXPCCRV.VENDOR_OWNER_NUMBER, 1, 1)<>0)
     )','1',201,'PO Cost Change Analysis Report - WC','FreeText');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'EXPCCRV.SOURCE IN Source','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SOURCE','','Source','','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'PO Cost Change Analysis Report - WC','EXPCCRV.SOURCE IN Source');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'EXPCCRV.INVENTORY_ITEM_STATUS_CODE IN Status','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVENTORY_ITEM_STATUS_CODE','','Status','','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','','','NOTIN','Y','Y','','','','','1',201,'PO Cost Change Analysis Report - WC','EXPCCRV.INVENTORY_ITEM_STATUS_CODE IN Status');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'EXPCCRV.VENDOR_OWNER_NUMBER IN Vendor Owner Number','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_OWNER_NUMBER','','Vendor Owner Number','','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'PO Cost Change Analysis Report - WC','EXPCCRV.VENDOR_OWNER_NUMBER IN Vendor Owner Number');
xxeis.eis_rsc_ins.rcnh( 'PO Cost Change Analysis Report - WC',201,'EXPCCRV.VENDOR_OWNER_NAME IN Vendor Owner Name','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_OWNER_NAME','','Vendor Owner Name','','','','','EIS_XXWC_PO_COST_CHNG_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'PO Cost Change Analysis Report - WC','EXPCCRV.VENDOR_OWNER_NAME IN Vendor Owner Name');
--Inserting Report Sorts - PO Cost Change Analysis Report - WC
--Inserting Report Triggers - PO Cost Change Analysis Report - WC
--inserting report templates - PO Cost Change Analysis Report - WC
--Inserting Report Portals - PO Cost Change Analysis Report - WC
--inserting report dashboards - PO Cost Change Analysis Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'PO Cost Change Analysis Report - WC','201','EIS_XXWC_PO_COST_CHNG_RPT_V','EIS_XXWC_PO_COST_CHNG_RPT_V','N','');
--inserting report security - PO Cost Change Analysis Report - WC
xxeis.eis_rsc_ins.rsec( 'PO Cost Change Analysis Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'PO Cost Change Analysis Report - WC','201','','XXWC_PUR_SUPER_USER',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'PO Cost Change Analysis Report - WC','401','','XXWC_DATA_MNGT_SC',201,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'PO Cost Change Analysis Report - WC','','SS084202','',201,'SA059956','','','');
--Inserting Report Pivots - PO Cost Change Analysis Report - WC
--Inserting Report   Version details- PO Cost Change Analysis Report - WC
xxeis.eis_rsc_ins.rv( 'PO Cost Change Analysis Report - WC','','PO Cost Change Analysis Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
