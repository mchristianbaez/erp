--Report Name            : HDS GSC iPro Requisition Details
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_PO_REQUISITIONS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_PO_REQUISITIONS_V
xxeis.eis_rsc_ins.v( 'EIS_PO_REQUISITIONS_V',201,'This view provides purchase requisition information. This view can be used to generate purchase requisition details and summary for a given date Range, supplier range or purchase requisition range or slice the information according to requisition lines or distributions.','','','','XXEIS_RS_ADMIN','XXEIS','EIS PO Requisitions','EPR','','','VIEW','US','Y','');
--Delete Object Columns for EIS_PO_REQUISITIONS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_PO_REQUISITIONS_V',201,FALSE);
--Inserting Object Columns for EIS_PO_REQUISITIONS_V
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PROJECT_DESCRIPTION',201,'The description of the project','PROJECT_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','PA_PROJECTS_ALL','DESCRIPTION','Project Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PROJECT_TYPE',201,'The project type that classifies the project and defaults project information upon project entry','PROJECT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','PA_PROJECTS_ALL','PROJECT_TYPE','Project Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PROJECT_STATUS_CODE',201,'The status of the project.','PROJECT_STATUS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PA_PROJECTS_ALL','PROJECT_STATUS_CODE','Project Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PROJECT_START_DATE',201,'The date on which the project starts; expenditure items with item dates before the start date cannot be entered for the project','PROJECT_START_DATE','','','','XXEIS_RS_ADMIN','DATE','PA_PROJECTS_ALL','START_DATE','Project Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PROJECT_COMPLETION_DATE',201,'The date on which the project is completed;  expenditure items with item dates after the completion date cannot be entered for the project','PROJECT_COMPLETION_DATE','','','','XXEIS_RS_ADMIN','DATE','PA_PROJECTS_ALL','COMPLETION_DATE','Project Completion Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','TASK_ID',201,'The system-generated number that uniquely identifies the task','TASK_ID','','','','XXEIS_RS_ADMIN','NUMBER','PA_TASKS','TASK_ID','Task Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','TASK_NUMBER',201,'User-defined number that uniquely identifies the task within a project.  It is recommended to number tasks based on the wbs since that is how tasks are ordered in some reports.  However note: task numbers do not record the wbs structure','TASK_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','PA_TASKS','TASK_NUMBER','Task Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','TASK_NAME',201,'User-defined short name of the task','TASK_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PA_TASKS','TASK_NAME','Task Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','TASK_DESCRIPTION',201,'Description of the task','TASK_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','PA_TASKS','DESCRIPTION','Task Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','TYPE_LOOKUP_CODE',201,'Requisition type','TYPE_LOOKUP_CODE','','','PO Requisition Type LOV','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','TYPE_LOOKUP_CODE','Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','TASK_START_DATE',201,'The date on which the task starts; expenditure items with item dates before the start date cannot be entered for the task','TASK_START_DATE','','','','XXEIS_RS_ADMIN','DATE','PA_TASKS','START_DATE','Task Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','TASK_COMPLETION_DATE',201,'The date on which the task is completed;  expenditure items with item dates after the task completion date cannot be entered for the task.  You must enter a start date to enter a completion date.','TASK_COMPLETION_DATE','','','','XXEIS_RS_ADMIN','DATE','PA_TASKS','COMPLETION_DATE','Task Completion Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CHARGEABLE_TASK',201,'Flag that indicates if expenditure items can be charged to the task.  Only lowest level tasks can be chargeable','CHARGEABLE_TASK','','','','XXEIS_RS_ADMIN','VARCHAR2','PA_TASKS','CHARGEABLE_FLAG','Chargeable Task','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','BILLABLE_TASK',201,'Default flag for items charged to the task that indicates if the item can accrue revenue (Y or N).  For capital projects this flag is used as capitalizable_flag.  For indirect projects this flag is set to N and is not used.','BILLABLE_TASK','','','','XXEIS_RS_ADMIN','VARCHAR2','PA_TASKS','BILLABLE_FLAG','Billable Task','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','EXPENDITURE_ORG',201,'Organization code','EXPENDITURE_ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ORGANIZATION_CODE','Expenditure Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CHARGE_ACCOUNT',201,'Charge Account','CHARGE_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Charge Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ACCRUAL_ACCOUNT',201,'Accrual Account','ACCRUAL_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Accrual Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','VARIANCE_ACCOUNT',201,'Variance Account','VARIANCE_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Variance Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','SUGGESTED_BUYER_NAME',201,'Concatenation of last name, title, first name, middle names and the name the person is known by.','SUGGESTED_BUYER_NAME~SUGGESTED_BUYER_NAME','','','PO Buyer Name LOV','XXEIS_RS_ADMIN','VARCHAR2','PER_ALL_PEOPLE_F','FULL_NAME','Suggested Buyer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','SUGGESTED_VENDOR_PRODUCT_CODE',201,'Suggested supplier product number','SUGGESTED_VENDOR_PRODUCT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','SUGGESTED_VENDOR_PRODUCT_CODE','Suggested Vendor Product Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CANCEL_FLAG',201,'Indicates whether the requisition is cancelled or not','CANCEL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','CANCEL_FLAG','Cancel Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CANCEL_DATE',201,'Cancel date','CANCEL_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_REQUISITION_LINES_ALL','CANCEL_DATE','Cancel Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','QUANTITY_CANCELLED',201,'Quantity cancelled','QUANTITY_CANCELLED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','QUANTITY_CANCELLED','Quantity Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','LINE_NUM',201,'Line number','LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','LINE_NUM','Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITEM_NUMBER',201,'Key flexfield segment','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','SEGMENT1','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITEM_DESCRIPTION',201,'Item description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','ITEM_DESCRIPTION','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','UNIT_MEAS_LOOKUP_CODE',201,'Unit of measure','UNIT_MEAS_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','UNIT_MEAS_LOOKUP_CODE','Unit Meas Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','SUGGESTED_VENDOR_LOCATION',201,'Suggested supplier site name','SUGGESTED_VENDOR_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','SUGGESTED_VENDOR_LOCATION','Suggested Vendor Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','SUGGESTED_VENDOR_CONTACT',201,'Suggested supplier contact name','SUGGESTED_VENDOR_CONTACT','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','SUGGESTED_VENDOR_CONTACT','Suggested Vendor Contact','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','OPERATING_UNIT',201,'Translated name of the organization','OPERATING_UNIT~OPERATING_UNIT','','','PO Operating Unit List LOV','XXEIS_RS_ADMIN','VARCHAR2','HR_ALL_ORGANIZATION_UNITS_TL','NAME','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','JUSTIFICATION',201,'Purchase justification','JUSTIFICATION','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','JUSTIFICATION','Justification','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','NOTE_TO_AGENT',201,'Note to buyer','NOTE_TO_AGENT','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','NOTE_TO_AGENT','Note To Agent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','NOTE_TO_RECEIVER',201,'Note to deliverer','NOTE_TO_RECEIVER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','NOTE_TO_RECEIVER','Note To Receiver','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','SUGGESTED_VENDOR_NAME',201,'Suggested supplier name','SUGGESTED_VENDOR_NAME','','','PO Requisition Suppliers LOV','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','SUGGESTED_VENDOR_NAME','Suggested Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PREPARER',201,'Concatenation of last name, title, first name, middle names and the name the person is known by.','PREPARER','','','PO Requisition Preparer name LOV','XXEIS_RS_ADMIN','VARCHAR2','PER_ALL_PEOPLE_F','FULL_NAME','Preparer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','UPDATED_BY',201,'Application username (what a user types in at the Oracle Applications sign-on screen)','UPDATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_USER','USER_NAME','Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','REQ_NUM',201,'Requisition number','REQ_NUM','','','PO Requisition Number LOV','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','SEGMENT1','Req Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CREATION_DATE',201,'Standard Who column','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_REQUISITION_HEADERS_ALL','CREATION_DATE','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','LAST_UPDATE_DATE',201,'Standard Who column','LAST_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_REQUISITION_HEADERS_ALL','LAST_UPDATED_BY','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CLOSED_REASON',201,'Close reason','CLOSED_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','CLOSED_REASON','Closed Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CLOSED_DATE',201,'Close date','CLOSED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_REQUISITION_LINES_ALL','CLOSED_DATE','Closed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','QUANTITY_RECEIVED',201,'Quantity received','QUANTITY_RECEIVED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','QUANTITY_RECEIVED','Quantity Received','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PO_NUMBER',201,'Document number - Combined with type_lookup_code and org_id to form unique key','PO_NUMBER~PO_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','SEGMENT1','Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PROJECT_NUMBER',201,'The project number that uniquely identifies the project.  The project number can be user-entered or system-generated as defined in the implementation options','PROJECT_NUMBER','','','PO Project Names LOV','XXEIS_RS_ADMIN','VARCHAR2','PA_PROJECTS_ALL','SEGMENT1','Project Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PROJECT_NAME',201,'User-defined name that uniquely identifies the project','PROJECT_NAME','','','PO Project Names LOV','XXEIS_RS_ADMIN','VARCHAR2','PA_PROJECTS_ALL','NAME','Project Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DESTINATION_ORG',201,'Organization code','DESTINATION_ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ORGANIZATION_CODE','Destination Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','LINE_TYPE',201,'Document line type','LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINE_TYPES_TL','LINE_TYPE','Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','SOURCE_TYPE_CODE',201,'Requisition source type of item','SOURCE_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','SOURCE_TYPE_CODE','Source Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','QUANTITY_DELIVERED',201,'Quantity delivered to date','QUANTITY_DELIVERED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','QUANTITY_DELIVERED','Quantity Delivered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','RFQ_REQUIRED_FLAG',201,'Indicates whether an RFQ is required prior to placement on a purchase order','RFQ_REQUIRED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','RFQ_REQUIRED_FLAG','Rfq Required Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','NEED_BY_DATE',201,'Date the requisition is needed internally','NEED_BY_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_REQUISITION_LINES_ALL','NEED_BY_DATE','Need By Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','REQ_LINE_QUANTITY',201,'Quantity for the distribution','REQ_LINE_QUANTITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQ_DISTRIBUTIONS_ALL','REQ_LINE_QUANTITY','Req Line Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DISTRIBUTION_NUM',201,'Distribution number','DISTRIBUTION_NUM','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQ_DISTRIBUTIONS_ALL','DISTRIBUTION_NUM','Distribution Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','EXPENDITURE_TYPE',201,'Project accounting expenditure type','EXPENDITURE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQ_DISTRIBUTIONS_ALL','EXPENDITURE_TYPE','Expenditure Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','EXPENDITURE_ITEM_DATE',201,'Project accounting expenditure item date','EXPENDITURE_ITEM_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_REQ_DISTRIBUTIONS_ALL','EXPENDITURE_ITEM_DATE','Expenditure Item Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DELIVER_TO_LOCATION_ID',201,'Deliver-to location unique identifier','DELIVER_TO_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','DELIVER_TO_LOCATION_ID','Deliver To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DELIVERY_LOCATION',201,'Location name.','DELIVERY_LOCATION','','','PO Location Names LOV','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL','LOCATION_CODE','Delivery Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','LINE_CLOSED_CODE',201,'Close status','LINE_CLOSED_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','CLOSED_CODE','Line Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','UNIT_PRICE',201,'Unit price in functional currency','UNIT_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','UNIT_PRICE','Unit Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','QUANTITY',201,'Quantity ordered','QUANTITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','QUANTITY','Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DESCRIPTION',201,'Description for requisition','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','DESCRIPTION','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','AUTHORIZATION_STATUS',201,'Authorization status type','AUTHORIZATION_STATUS','','','PO Requisition Status LOV','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','AUTHORIZATION_STATUS','Authorization Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITEM_CATEGORY',201,'Item Category','ITEM_CATEGORY~ITEM_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Item Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CODE_COMBINATION_ID',201,'Key flexfield combination defining column','CODE_COMBINATION_ID~CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','LOCATION_ID',201,'System-generated primary key column.','LOCATION_ID~LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_LOCATIONS_ALL','LOCATION_ID','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ORGANIZATION_ID',201,'Organization identifier','ORGANIZATION_ID~ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_PARAMETERS','ORGANIZATION_ID','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ORG_ID',201,'Org Id','ORG_ID~ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_ALL_ORGANIZATION_UNITS','ORGANIZATION_ID','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PO_DISTRIBUTION_ID',201,'Document distribution unique identifier. Primary key for this table.','PO_DISTRIBUTION_ID~PO_DISTRIBUTION_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_DISTRIBUTIONS_ALL','PO_DISTRIBUTION_ID','Po Distribution Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PO_HEADER_ID',201,'Document header unique identifier','PO_HEADER_ID~PO_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_HEADERS_ALL','PO_HEADER_ID','Po Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','USER_ID',201,'Application user identifier','USER_ID~USER_ID','','','','XXEIS_RS_ADMIN','NUMBER','FND_USER','USER_ID','User Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CHARGE_CCID',201,'Key flexfield combination defining column','CHARGE_CCID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Charge Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ACCRUAL_CCID',201,'Key flexfield combination defining column','ACCRUAL_CCID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Accrual Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PROJECT_ID',201,'The system-generated number that uniquely identifies the project','PROJECT_ID','','','','XXEIS_RS_ADMIN','NUMBER','PA_PROJECTS_ALL','PROJECT_ID','Project Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','AGENT_ID',201,'Buyer unique identifier','AGENT_ID~AGENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_AGENTS','AGENT_ID','Agent Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','SERVICE_TYPE_CODE',201,'The type of work performed on the task','SERVICE_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PA_TASKS','SERVICE_TYPE_CODE','Service Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','EXP_ORG_ID',201,'Organization identifier','EXP_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_PARAMETERS','ORGANIZATION_ID','Exp Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','LINE_TYPE_DESCRIPTION',201,'Description','LINE_TYPE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINE_TYPES_TL','DESCRIPTION','Line Type Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DESTINATION_TYPE_CODE',201,'Destination type','DESTINATION_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','DESTINATION_TYPE_CODE','Destination Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','REQUISITION_LINE_ID',201,'Requisition line unique identifier','REQUISITION_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','REQUISITION_LINE_ID','Requisition Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','EMPLOYEE_ID',201,'System generated person primary key from PER_PEOPLE_S.','EMPLOYEE_ID~EMPLOYEE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PER_ALL_PEOPLE_F','PERSON_ID','Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','AGENT_RETURN_NOTE',201,'Unused since release 7.0','AGENT_RETURN_NOTE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','AGENT_RETURN_NOTE','Agent Return Note','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CHANGED_AFTER_RESEARCH_FLAG',201,'Unused since release 7.0','CHANGED_AFTER_RESEARCH_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','CHANGED_AFTER_RESEARCH_FLAG','Changed After Research Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ON_LINE_FLAG',201,'Unused since release 7.0','ON_LINE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','ON_LINE_FLAG','On Line Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DESTINATION_CONTEXT',201,'Destination descriptive flexfield context column','DESTINATION_CONTEXT','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','DESTINATION_CONTEXT','Destination Context','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DOCUMENT_TYPE_CODE',201,'Source document type','DOCUMENT_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','DOCUMENT_TYPE_CODE','Document Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','REQUISITION_HEADER_ID',201,'Requisition header unique identifier','REQUISITION_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_HEADERS_ALL','REQUISITION_HEADER_ID','Requisition Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','SUMMARY_FLAG',201,'Key flexfield summary flag','SUMMARY_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','SUMMARY_FLAG','Summary Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ENABLED_FLAG',201,'Key flexfield enabled flag','ENABLED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','ENABLED_FLAG','Enabled Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CREATED_BY',201,'Standard Who column','CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_HEADERS_ALL','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','LAST_UPDATED_BY',201,'Standard Who column','LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_HEADERS_ALL','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','TRANSACTION_REASON_CODE',201,'Transaction reason','TRANSACTION_REASON_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','TRANSACTION_REASON_CODE','Transaction Reason Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CANCEL_REASON',201,'Cancel reason','CANCEL_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','CANCEL_REASON','Cancel Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','VARIANCE_CCID',201,'Key flexfield combination defining column','VARIANCE_CCID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Variance Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITEM_REVISION',201,'Item revision','ITEM_REVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','ITEM_REVISION','Item Revision','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','NOTE_TO_VENDOR',201,'Note to supplier','NOTE_TO_VENDOR','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','NOTE_TO_VENDOR','Note To Vendor','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DISTRIBUTION_ID',201,'Requisition distribution unique identifier','DISTRIBUTION_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQ_DISTRIBUTIONS_ALL','DISTRIBUTION_ID','Distribution Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ENCUMBERED_FLAG',201,'Indicates whether the distribution is encumbered or not','ENCUMBERED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQ_DISTRIBUTIONS_ALL','ENCUMBERED_FLAG','Encumbered Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','GL_ENCUMBERED_DATE',201,'Date the distribution was encumbered','GL_ENCUMBERED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_REQ_DISTRIBUTIONS_ALL','GL_ENCUMBERED_DATE','Gl Encumbered Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','GL_ENCUMBERED_PERIOD_NAME',201,'Period in which the distribution was encumbered','GL_ENCUMBERED_PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQ_DISTRIBUTIONS_ALL','GL_ENCUMBERED_PERIOD_NAME','Gl Encumbered Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','GL_CANCELLED_DATE',201,'Date the distribution was cancelled','GL_CANCELLED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_REQ_DISTRIBUTIONS_ALL','GL_CANCELLED_DATE','Gl Cancelled Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','FAILED_FUNDS_LOOKUP_CODE',201,'Type of approval failure for the distribution','FAILED_FUNDS_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQ_DISTRIBUTIONS_ALL','FAILED_FUNDS_LOOKUP_CODE','Failed Funds Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ENCUMBERED_AMOUNT',201,'Encumbered amount for distribution','ENCUMBERED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQ_DISTRIBUTIONS_ALL','ENCUMBERED_AMOUNT','Encumbered Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PREVENT_ENCUMBRANCE_FLAG',201,'Indicates whether the distribution requires encumbrance or not','PREVENT_ENCUMBRANCE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQ_DISTRIBUTIONS_ALL','PREVENT_ENCUMBRANCE_FLAG','Prevent Encumbrance Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','RECOVERABLE_TAX',201,' Recoverable tax amount','RECOVERABLE_TAX','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQ_DISTRIBUTIONS_ALL','RECOVERABLE_TAX','Recoverable Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','NONRECOVERABLE_TAX',201,'Nonrecoverable tax amount','NONRECOVERABLE_TAX','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQ_DISTRIBUTIONS_ALL','NONRECOVERABLE_TAX','Nonrecoverable Tax','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','RECOVERY_RATE',201,'Percentage of tax that can be recovered','RECOVERY_RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQ_DISTRIBUTIONS_ALL','RECOVERY_RATE','Recovery Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','TAX_RECOVERY_OVERRIDE_FLAG',201,'Indicator of  whether tax recovery should be used','TAX_RECOVERY_OVERRIDE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQ_DISTRIBUTIONS_ALL','TAX_RECOVERY_OVERRIDE_FLAG','Tax Recovery Override Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','DEST_ORG_ID',201,'Organization identifier','DEST_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_PARAMETERS','ORGANIZATION_ID','Dest Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITM_ORGANIZATION_ID',201,'Organization identifier','ITM_ORGANIZATION_ID~ITM_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','ORGANIZATION_ID','Itm Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','MP1_ORGANIZATION_ID',201,'Organization identifier','MP1_ORGANIZATION_ID~MP1_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_PARAMETERS','ORGANIZATION_ID','Mp1 Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','MP_ORGANIZATION_ID',201,'Organization identifier','MP_ORGANIZATION_ID~MP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_PARAMETERS','ORGANIZATION_ID','Mp Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PAP_PROJECT_ID',201,'The system-generated number that uniquely identifies the project','PAP_PROJECT_ID~PAP_PROJECT_ID','','','','XXEIS_RS_ADMIN','NUMBER','PA_PROJECTS_ALL','PROJECT_ID','Pap Project Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','PAT_TASK_ID',201,'The system-generated number that uniquely identifies the task','PAT_TASK_ID~PAT_TASK_ID','','','','XXEIS_RS_ADMIN','NUMBER','PA_TASKS','TASK_ID','Pat Task Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','USR_LAST_UPDATED_BY',201,'Standard Who column','USR_LAST_UPDATED_BY~USR_LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','FND_USER','LAST_UPDATED_BY','Usr Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITM_INVENTORY_ITEM_ID',201,'Inventory item identifier','ITM_INVENTORY_ITEM_ID~ITM_INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','INVENTORY_ITEM_ID','Itm Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CLOSED_CODE',201,'Close status','CLOSED_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','CLOSED_CODE','Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','NOTE_TO_AUTHORIZER',201,'Note to approver (Unused since release 8.0)','NOTE_TO_AUTHORIZER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','NOTE_TO_AUTHORIZER','Note To Authorizer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','CATEGORY_ID',201,'Category Id','CATEGORY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Category Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','HRE_EMPLOYEE_ID',201,'Hre Employee Id','HRE_EMPLOYEE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Hre Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITEM_CATEGORY_SEG1',201,'Item Category Seg1','ITEM_CATEGORY_SEG1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Category Seg1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITEM_CATEGORY_SEG2',201,'Item Category Seg2','ITEM_CATEGORY_SEG2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Category Seg2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITEM_CATEGORY_SEG3',201,'Item Category Seg3','ITEM_CATEGORY_SEG3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Category Seg3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','ITEM_CATEGORY_SEG4',201,'Item Category Seg4','ITEM_CATEGORY_SEG4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Category Seg4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','LINE_TYPE_ID',201,'Line Type Id','LINE_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','COPYRIGHT',201,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','VENDOR_ID',201,'Vendor Id','VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_REQUISITIONS_V','VENDOR_NUMBER',201,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US');
--Inserting Object Components for EIS_PO_REQUISITIONS_V
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','PO_REQ_DISTRIBUTIONS',201,'PO_REQ_DISTRIBUTIONS_ALL','PRD','PRD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Reqisition Distributions','','','','','PRD','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','PO_DISTRIBUTIONS',201,'PO_DISTRIBUTIONS_ALL','POD','POD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchase Order Distributions','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','PO_HEADERS',201,'PO_HEADERS_ALL','POH','POH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchasing Headers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','PO_AGENTS_V',201,'PO_AGENTS','POAV','POAV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Buyers Table','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','MTL_PARAMETERS',201,'MTL_PARAMETERS','MP','MP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Destination Organization Details','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','HR_LOCATIONS',201,'HR_LOCATIONS_ALL','HRL','HRL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Locations','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','GL_CODE_COMBINATIONS',201,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Charge Account','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','PA_PROJECTS',201,'PA_PROJECTS_ALL','PAP','PAP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Projects','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','PA_TASKS',201,'PA_TASKS','PAT','PAT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Project Tasks','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','MTL_PARAMETERS',201,'MTL_PARAMETERS','MP1','MP1','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Expenditure Organization Details','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','MTL_SYSTEM_ITEMS',201,'MTL_SYSTEM_ITEMS_B','ITM','ITM','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Items','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','FND_USER',201,'FND_USER','USR','USR','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Application Users','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','HR_ORGANIZATION_UNITS',201,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Organization Details','N','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','PO_REQUISITION_HEADERS',201,'PO_REQUISITION_HEADERS_ALL','PRH','PRH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Reqisition Headers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_REQUISITIONS_V','PO_REQUISITION_LINES',201,'PO_REQUISITION_LINES_ALL','PRL','PRL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Reqisition Lines','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_PO_REQUISITIONS_V
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','PO_REQ_DISTRIBUTIONS','PRD',201,'EPR.DISTRIBUTION_ID','=','PRD.DISTRIBUTION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','PO_DISTRIBUTIONS','POD',201,'EPR.PO_DISTRIBUTION_ID','=','POD.PO_DISTRIBUTION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','PO_HEADERS','POH',201,'EPR.PO_HEADER_ID','=','POH.PO_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','PO_AGENTS_V','POAV',201,'EPR.AGENT_ID','=','POAV.AGENT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','MTL_PARAMETERS','MP',201,'EPR.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','HR_LOCATIONS','HRL',201,'EPR.LOCATION_ID','=','HRL.LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','GL_CODE_COMBINATIONS','GCC',201,'EPR.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','PA_PROJECTS','PAP',201,'EPR.PAP_PROJECT_ID','=','PAP.PROJECT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','PA_TASKS','PAT',201,'EPR.PAT_TASK_ID','=','PAT.TASK_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','MTL_PARAMETERS','MP1',201,'EPR.MP1_ORGANIZATION_ID','=','MP1.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','MTL_SYSTEM_ITEMS','ITM',201,'EPR.ITM_INVENTORY_ITEM_ID','=','ITM.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','MTL_SYSTEM_ITEMS','ITM',201,'EPR.ITM_ORGANIZATION_ID','=','ITM.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','FND_USER','USR',201,'EPR.USER_ID','=','USR.USER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','HR_ORGANIZATION_UNITS','HOU',201,'EPR.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','PO_REQUISITION_HEADERS','PRH',201,'EPR.REQUISITION_HEADER_ID','=','PRH.REQUISITION_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_REQUISITIONS_V','PO_REQUISITION_LINES','PRL',201,'EPR.REQUISITION_LINE_ID','=','PRL.REQUISITION_LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for HDS GSC iPro Requisition Details
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.lov( 201,'select agent_name buyer_name from po_agents_v','','EIS_PO_BUYER_LOV','List of Values for Buyer','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select PRH.SEGMENT1 Requisition_number,HOU.name OPERATING_UNIT from PO_REQUISITION_HEADERS_ALL PRH,HR_OPERATING_UNITS HOU
where PRH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=PRH.ORG_ID)
order by PRH.SEGMENT1','','EIS_PO_REQ_LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct location_code from hr_locations_all','','EIS_LOCATIONS_LOV','This LOV lists all the locations','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct full_name from hr_employees','','EIS_REQ_PREPARER_LOV','This LOV gives a list of all the requisition preparers','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct PRH.AUTHORIZATION_STATUS,HOU.name OPERATING_UNIT from PO_REQUISITION_HEADERS_ALL PRH,HR_OPERATING_UNITS HOU
where prh.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=prh.ORG_ID)','','EIS_REQ_STATUS_LOV','This LOV lists the requisition statuses','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select distinct PRH.type_lookup_code,HOU.name OPERATING_UNIT from PO_REQUISITION_HEADERS_ALL PRH,HR_OPERATING_UNITS HOU
where prh.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=prh.ORG_ID)','','EIS_REQ_TYPE_LOV','This LOV lists all the requisition types','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT distinct concatenated_segments item_category from mtl_categories_kfv ORDER BY concatenated_segments','','EIS_PO_ITEM_CATEGORIES_LOV','Item categories for Purchasing User','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for HDS GSC iPro Requisition Details
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS GSC iPro Requisition Details
xxeis.eis_rsc_utility.delete_report_rows( 'HDS GSC iPro Requisition Details' );
--Inserting Report - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.r( 201,'HDS GSC iPro Requisition Details','','This report lists all purchase requisitions that have been created within a specified date range, purchase requisition range or supplier range.

Created as part of project #20266','','','','MM027735','EIS_PO_REQUISITIONS_V','Y','','','MM027735','','N','iProcurement','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'CLOSED_REASON','Closed Reason','Close reason','','','','','29','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'LINE_CLOSED_CODE','Line Closed Code','Close status','','','','','30','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'NOTE_TO_AGENT','Note To Agent','Note to buyer','','','','','17','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'NOTE_TO_RECEIVER','Note To Receiver','Note to deliverer','','','','','18','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'SUGGESTED_VENDOR_NAME','Suggested Vendor Name','Suggested supplier name','','','','','3','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'SUGGESTED_VENDOR_LOCATION','Suggested Vendor Location','Suggested supplier site name','','','','','4','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'CANCEL_FLAG','Cancel Flag','Indicates whether the requisition is cancelled or not','','','','','24','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'CANCEL_DATE','Cancel Date','Cancel date','','','','','25','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'ITEM_DESCRIPTION','Item Description','Item description','','','','','21','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'UNIT_MEAS_LOOKUP_CODE','Unit Meas Lookup Code','Unit of measure','','','','','13','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'UNIT_PRICE','Unit Price','Unit price in functional currency','','','','','14','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'QUANTITY','Quantity','Quantity ordered','','~T~D~2','','','12','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'OPERATING_UNIT','Operating Unit','Translated name of the organization','','','','','1','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'ITEM_CATEGORY','Item Category','Item Category','','','','','22','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'PO_NUMBER','Po Number','Document number - Combined with type_lookup_code and org_id to form unique key','','','','','8','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'QUANTITY_DELIVERED','Quantity Delivered','Quantity delivered to date','','','','','19','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'NEED_BY_DATE','Need By Date','Date the requisition is needed internally','','','','','9','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'REQ_LINE_QUANTITY','Req Line Quantity','Quantity for the distribution','','','','','20','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'DELIVERY_LOCATION','Delivery Location','Location name.','','','','','10','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'LINE_TYPE','Line Type','Document line type','','','','','11','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'CHARGE_ACCOUNT','Charge Account','Charge Account','','','','','23','N','','','eisRSSegmentInfo.jsp?codeId="EPR.CHARGE_CCID"','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','U');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'REQ_NUM','Req Num','Requisition number','','','','','6','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'LINE_NUM','Line Num','Line number','','','','','7','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'AUTHORIZATION_STATUS','Authorization Status','Authorization status type','','','','','2','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'TYPE_LOOKUP_CODE','Type Lookup Code','Requisition type','','','','','5','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'PREPARER','Preparer','Concatenation of last name, title, first name, middle names and the name the person is known by.','','','','','16','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'CREATION_DATE','Creation Date','Standard Who column','','','','','26','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'LAST_UPDATE_DATE','Last Update Date','Standard Who column','','','','','27','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'CLOSED_DATE','Closed Date','Close date','','','','','28','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'QUANTITY_CANCELLED','Quantity Cancelled','Quantity cancelled','','','','','31','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'QUANTITY_RECEIVED','Quantity Received','Quantity received','','~T~D~2','','','32','N','','','','','','','','MM027735','N','N','','EIS_PO_REQUISITIONS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Requisition Details',201,'LINE_AMOUNT','Line Amount','','NUMBER','','','0','15','Y','','','','','','','EPR.QUANTITY * EPR.UNIT_PRICE','MM027735','N','N','','','','','SUM','US','');
--Inserting Report Parameters - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'Buyer','Buyer','SUGGESTED_BUYER_NAME','IN','EIS_PO_BUYER_LOV','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'PO Req From','PO Requisition From','REQ_NUM','IN','EIS_PO_REQ_LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'PO Req To','PO Requisition To','REQ_NUM','IN','EIS_PO_REQ_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'Supplier','Supplier','SUGGESTED_VENDOR_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'PO Req Location','PO Requisition Location','DELIVERY_LOCATION','IN','EIS_LOCATIONS_LOV','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'PO Req Preparer','PO Requisition Preparer','PREPARER','IN','EIS_REQ_PREPARER_LOV','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'PO Req Status','PO Requisition Status','AUTHORIZATION_STATUS','IN','EIS_REQ_STATUS_LOV','','VARCHAR2','N','Y','11','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'PO Req Type','PO Requisition Type','TYPE_LOOKUP_CODE','IN','EIS_REQ_TYPE_LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'Operating unit','Operating unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','''HD Supply Corp USD - Org''','VARCHAR2','N','Y','1','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'Date From','From Date (PO Requisition Date)','CREATION_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'Date To','To Date (PO Requisition Date)','CREATION_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Requisition Details',201,'Item Category','Item Category','ITEM_CATEGORY','IN','EIS_PO_ITEM_CATEGORIES_LOV','','VARCHAR2','N','Y','12','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_REQUISITIONS_V','','','US','');
--Inserting Dependent Parameters - HDS GSC iPro Requisition Details
--Inserting Report Conditions - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'AUTHORIZATION_STATUS IN :PO Req Status ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','AUTHORIZATION_STATUS','','PO Req Status','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','AUTHORIZATION_STATUS IN :PO Req Status ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'CREATION_DATE >= :Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Date From','','','','','EIS_PO_REQUISITIONS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','CREATION_DATE >= :Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'CREATION_DATE <= :Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Date To','','','','','EIS_PO_REQUISITIONS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','CREATION_DATE <= :Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'DELIVERY_LOCATION IN :PO Req Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DELIVERY_LOCATION','','PO Req Location','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','DELIVERY_LOCATION IN :PO Req Location ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'ITEM_CATEGORY IN :Item Category ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ITEM_CATEGORY','','Item Category','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','ITEM_CATEGORY IN :Item Category ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'OPERATING_UNIT IN :Operating unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating unit','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','OPERATING_UNIT IN :Operating unit ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'PREPARER IN :PO Req Preparer ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PREPARER','','PO Req Preparer','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','PREPARER IN :PO Req Preparer ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'REQ_NUM IN :PO Req From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REQ_NUM','','PO Req From','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','REQ_NUM IN :PO Req From ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'REQ_NUM IN :PO Req To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REQ_NUM','','PO Req To','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','REQ_NUM IN :PO Req To ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'SUGGESTED_BUYER_NAME IN :Buyer ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUGGESTED_BUYER_NAME','','Buyer','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','SUGGESTED_BUYER_NAME IN :Buyer ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'SUGGESTED_VENDOR_NAME IN :Supplier ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SUGGESTED_VENDOR_NAME','','Supplier','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','SUGGESTED_VENDOR_NAME IN :Supplier ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Requisition Details',201,'TYPE_LOOKUP_CODE IN :PO Req Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','TYPE_LOOKUP_CODE','','PO Req Type','','','','','EIS_PO_REQUISITIONS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Requisition Details','TYPE_LOOKUP_CODE IN :PO Req Type ');
--Inserting Report Sorts - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.rs( 'HDS GSC iPro Requisition Details',201,'REQ_NUM','ASC','MM027735','1','');
xxeis.eis_rsc_ins.rs( 'HDS GSC iPro Requisition Details',201,'LINE_NUM','ASC','MM027735','2','');
--Inserting Report Triggers - HDS GSC iPro Requisition Details
--inserting report templates - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.r_tem( 'HDS GSC iPro Requisition Details','HDS Requisition Details','Seeded template for HDS Requisition Details','','','','','','','','','','','HDS Requisition Details.rtf','MM027735','X','','','Y','Y','','');
--Inserting Report Portals - HDS GSC iPro Requisition Details
--inserting report dashboards - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.R_dash( 'HDS GSC iPro Requisition Details','Purchase Requisition Details','Purchase Requisition Details','vertical clustered bar','large','Authorization Status','Authorization Status','Quantity','Quantity','Sum','MM027735');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS GSC iPro Requisition Details','201','EIS_PO_REQUISITIONS_V','EIS_PO_REQUISITIONS_V','N','');
--inserting report security - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','178','','SELF_SERVICES_CATALOG_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','178','','SLF_SRVC_PRCHSNG_5',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_TRNS_ENTRY_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_TRNS_ENTRY_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_SUPPLIER_MAINT_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_MGR_NOSUP_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_MGR_NOSUP_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_PYBLS_MNGR',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_PYABLS_MNGR_CAN',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_MANAGER',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_MGR_NOSUP_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_INQUIRY_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_INQUIRY_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_INQ_CANADA',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_DISBUREMTS_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_DISBURSEMTS_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_DISBURSEMTS_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_PAYABLES_CLOSE_GLBL',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_ADMIN_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_ADMIN_US_GSCIWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_ADMIN_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','200','','HDS_AP_ADMIN_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','101','','GNRL_LDGR_FSS',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','101','','XXCUS_GL_MANAGER_GLOBAL',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','101','','XXCUS_GL_MANAGER',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','101','','XXCUS_GL_INQUIRY',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','101','','XXCUS_GL_ACCOUNTANT_USD',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Requisition Details','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',201,'MM027735','','','');
--Inserting Report Pivots - HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.rpivot( 'HDS GSC iPro Requisition Details',201,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','NOTE_TO_AGENT','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','SUGGESTED_VENDOR_NAME','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','UNIT_MEAS_LOOKUP_CODE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','QUANTITY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','QUANTITY_DELIVERED','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','NEED_BY_DATE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','REQ_LINE_QUANTITY','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','DELIVERY_LOCATION','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','LINE_TYPE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','CHARGE_ACCOUNT','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','REQ_NUM','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','AUTHORIZATION_STATUS','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','TYPE_LOOKUP_CODE','PAGE_FIELD','','','0','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Requisition Details',201,'Pivot','PREPARER','ROW_FIELD','','','2','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- HDS GSC iPro Requisition Details
xxeis.eis_rsc_ins.rv( 'HDS GSC iPro Requisition Details','','HDS GSC iPro Requisition Details','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
