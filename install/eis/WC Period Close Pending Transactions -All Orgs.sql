--Report Name            : WC Period Close Pending Transactions - All Orgs
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXWC_PENDING_TRANSACTION_V
set scan off define off
prompt Creating View XXEIS.XXWC_PENDING_TRANSACTION_V
Create or replace View XXEIS.XXWC_PENDING_TRANSACTION_V
 AS 
SELECT order_by, period_name
                   ,resolution_type
                   ,transaction_type
                   ,organization_code
                   ,organization_name
                   ,COUNT
      FROM (  --Resolution Required
              --1.Unprocessed Material
              SELECT '1' order_by
                    ,oap.period_name
                    ,'Resolution Required' resolution_type
                    ,'Unprocessed Material' transaction_type
                    ,ood.organization_code
                    ,ood.organization_name
                    ,COUNT (1) COUNT
                FROM apps.mtl_material_transactions_temp mmtt
                    ,apps.org_organization_definitions ood
                    ,apps.org_acct_periods oap
               WHERE     transaction_date <= oap.schedule_close_date
                     AND NVL (transaction_status, 0) <> 2
                     AND mmtt.organization_id = ood.organization_id
                     AND mmtt.organization_id = oap.organization_id
            GROUP BY oap.period_name, ood.organization_code, ood.organization_name
            UNION ALL
              ---2.Uncosted Material
              SELECT '2' order_by
                    ,oap.period_name
                    ,'Resolution Required' resolution_type
                    ,'Uncosted Material' transaction_type
                    ,ood.organization_code
                    ,ood.organization_name
                    ,COUNT (1) COUNT
                FROM apps.mtl_material_transactions mmt, apps.org_organization_definitions ood, apps.org_acct_periods oap
               WHERE     mmt.transaction_date <= oap.schedule_close_date
                     AND mmt.costed_flag IS NOT NULL
                     AND mmt.organization_id = ood.organization_id
                     AND mmt.organization_id = oap.organization_id
            GROUP BY oap.period_name, ood.organization_code, ood.organization_name
            UNION ALL
              --3.Pending WIP
              SELECT '3' order_by
                    ,oap.period_name
                    ,'Resolution Required' resolution_type
                    ,'Pending WIP' transaction_type
                    ,ood.organization_code
                    ,ood.organization_name
                    ,COUNT (1) COUNT
                FROM apps.wip_cost_txn_interface wcti, apps.org_organization_definitions ood, apps.org_acct_periods oap
               WHERE     transaction_date <= oap.schedule_close_date
                     AND wcti.organization_id = ood.organization_id
                     AND wcti.organization_id = oap.organization_id
            GROUP BY oap.period_name, ood.organization_code, ood.organization_name
            UNION ALL
              --4.Uncosted WSM
              SELECT '4' order_by
                    ,oap.period_name
                    ,'Resolution Required' resolution_type
                    ,'Uncosted WSM' transaction_type
                    ,ood.organization_code
                    ,ood.organization_name
                    ,COUNT (1) COUNT
                FROM apps.wsm_split_merge_transactions wsmt
                    ,apps.org_organization_definitions ood
                    ,apps.org_acct_periods oap
               WHERE     costed <> 4
                     AND transaction_date <= oap.schedule_close_date
                     AND wsmt.organization_id = ood.organization_id
                     AND wsmt.organization_id = oap.organization_id
            GROUP BY oap.period_name, ood.organization_code, ood.organization_name
            UNION ALL
              --5.Pending WMS Interface
              SELECT '5' order_by
                    ,oap.period_name
                    ,'Resolution Required' resolution_type
                    ,'Pending WMS Interface' transaction_type
                    ,ood.organization_code
                    ,ood.organization_name
                    ,COUNT (1) COUNT
                FROM apps.wsm_split_merge_txn_interface wsmti
                    ,apps.org_organization_definitions ood
                    ,apps.org_acct_periods oap
               WHERE     process_status <> 4
                     AND transaction_date <= oap.schedule_close_date
                     AND wsmti.organization_id = ood.organization_id
                     AND wsmti.organization_id = oap.organization_id
            GROUP BY oap.period_name, ood.organization_code, ood.organization_name
            UNION ALL
              --Resolution Recommended
              --6.Pending Receiving
              SELECT '6' order_by
                    ,oap.period_name
                    ,'Resolution Recommended' resolution_type
                    ,'Pending Receiving' transaction_type
                    ,ood.organization_code
                    ,ood.organization_name
                    ,COUNT (1) COUNT
                FROM apps.rcv_transactions_interface rti
                    ,apps.org_organization_definitions ood
                    ,apps.org_acct_periods oap
               WHERE     rti.transaction_date <= oap.schedule_close_date
                     AND rti.destination_type_code = 'INVENTORY'
                     AND rti.to_organization_id = ood.organization_id
                     AND rti.to_organization_id = oap.organization_id
            GROUP BY oap.period_name, ood.organization_code, ood.organization_name
            UNION ALL
              --7.Pending Material
              SELECT '7' order_by
                    ,oap.period_name
                    ,'Resolution Recommended' resolution_type
                    ,'Pending Material' transaction_type
                    ,ood.organization_code
                    ,ood.organization_name
                    ,COUNT (1) COUNT
                FROM apps.mtl_transactions_interface mti
                    ,apps.org_organization_definitions ood
                    ,apps.org_acct_periods oap
               WHERE     mti.transaction_date <= oap.schedule_close_date
                     AND process_flag <> 9
                     AND mti.organization_id = ood.organization_id
                     AND mti.organization_id = oap.organization_id
            GROUP BY oap.period_name, ood.organization_code, ood.organization_name
            UNION ALL
              --8.Pending Shop Floor Move
              SELECT '8' order_by
                    ,oap.period_name
                    ,'Resolution Recommended' resolution_type
                    ,'Pending Shop Floor Move' transaction_type
                    ,ood.organization_code
                    ,ood.organization_name
                    ,COUNT (1) COUNT
                FROM apps.wip_move_txn_interface wmti, apps.org_organization_definitions ood, apps.org_acct_periods oap
               WHERE     transaction_date <= oap.schedule_close_date
                     AND wmti.organization_id = ood.organization_id
                     AND wmti.organization_id = oap.organization_id
            GROUP BY oap.period_name, ood.organization_code, ood.organization_name
            UNION ALL
              --Unprocessed Shipping Transactions
              --9.Pending Transactions
              SELECT '9' order_by
                    ,oap.period_name
                    ,'Resolution Recommended' resolution_type
                    ,'Unprocessed Shipping Transactions' transaction_type
                    ,ood.organization_code
                    ,ood.organization_name
                    ,COUNT (1) COUNT
                FROM apps.wsh_delivery_details wdd
                    ,apps.wsh_delivery_assignments wda
                    ,apps.wsh_new_deliveries wnd
                    ,apps.wsh_delivery_legs wdl
                    ,apps.wsh_trip_stops wts
                    ,apps.org_organization_definitions ood
                    ,apps.org_acct_periods oap
               WHERE     wdd.source_code = 'OE'
                     AND wdd.released_status = 'C'
                     AND wdd.inv_interfaced_flag IN ('N', 'P')
                     AND wda.delivery_detail_id = wdd.delivery_detail_id
                     AND wnd.delivery_id = wda.delivery_id
                     AND wnd.status_code IN ('CL', 'IT')
                     AND wdl.delivery_id = wnd.delivery_id
                     AND wts.pending_interface_flag = 'Y'
                     AND TRUNC (wts.actual_departure_date) <= oap.schedule_close_date
                     AND wdl.pick_up_stop_id = wts.stop_id
                     AND wdd.organization_id = ood.organization_id
                     AND wdd.organization_id = oap.organization_id
            GROUP BY oap.period_name, ood.organization_code, ood.organization_name)
/
set scan on define on
prompt Creating View Data for WC Period Close Pending Transactions - All Orgs
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_PENDING_TRANSACTION_V
xxeis.eis_rs_ins.v( 'XXWC_PENDING_TRANSACTION_V',660,'','','','','HT038687','XXEIS','Xxwc Pending Transaction V','XPTV','','');
--Delete View Columns for XXWC_PENDING_TRANSACTION_V
xxeis.eis_rs_utility.delete_view_rows('XXWC_PENDING_TRANSACTION_V',660,FALSE);
--Inserting View Columns for XXWC_PENDING_TRANSACTION_V
xxeis.eis_rs_ins.vc( 'XXWC_PENDING_TRANSACTION_V','COUNT',660,'Count','COUNT','','','','HT038687','NUMBER','','','Count','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PENDING_TRANSACTION_V','ORGANIZATION_NAME',660,'Organization Name','ORGANIZATION_NAME','','','','HT038687','VARCHAR2','','','Organization Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PENDING_TRANSACTION_V','ORGANIZATION_CODE',660,'Organization Code','ORGANIZATION_CODE','','','','HT038687','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PENDING_TRANSACTION_V','TRANSACTION_TYPE',660,'Transaction Type','TRANSACTION_TYPE','','','','HT038687','VARCHAR2','','','Transaction Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PENDING_TRANSACTION_V','RESOLUTION_TYPE',660,'Resolution Type','RESOLUTION_TYPE','','','','HT038687','VARCHAR2','','','Resolution Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_PENDING_TRANSACTION_V','PERIOD_NAME',660,'Period Name','PERIOD_NAME','','','','HT038687','VARCHAR2','','','Period Name','','','');
--Inserting View Components for XXWC_PENDING_TRANSACTION_V
--Inserting View Component Joins for XXWC_PENDING_TRANSACTION_V
END;
/
set scan on define on
prompt Creating Report LOV Data for WC Period Close Pending Transactions - All Orgs
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - WC Period Close Pending Transactions - All Orgs
xxeis.eis_rs_ins.lov( '','SELECT  GPS.PERIOD_NAME period_name
FROM    GL_PERIOD_STATUSES GPS,
             GL_LEDGERS LED
WHERE  GPS.CLOSING_STATUS IN (''O'',''C'',''P'')
AND       GPS.APPLICATION_ID = 101
AND       LED.LEDGER_ID          = GPS.SET_OF_BOOKS_ID
AND       GL_SECURITY_PKG.VALIDATE_ACCESS(LED.LEDGER_ID) = ''TRUE''','','XXCUS_PERIOD_NAME GL','Period Name from the gl_period_statuses table','ID020048',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for WC Period Close Pending Transactions - All Orgs
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC Period Close Pending Transactions - All Orgs
xxeis.eis_rs_utility.delete_report_rows( 'WC Period Close Pending Transactions - All Orgs' );
--Inserting Report - WC Period Close Pending Transactions - All Orgs
xxeis.eis_rs_ins.r( 660,'WC Period Close Pending Transactions - All Orgs','','The purpose of this report is to provide a complete listing of pending transactions for all orgs','','','','HT038687','XXWC_PENDING_TRANSACTION_V','Y','','','HT038687','','N','White Cap Reports','','EXCEL,','N');
--Inserting Report Columns - WC Period Close Pending Transactions - All Orgs
xxeis.eis_rs_ins.rc( 'WC Period Close Pending Transactions - All Orgs',660,'COUNT','Count','Count','','~~~','default','','6','N','','','','','','','','HT038687','N','N','','XXWC_PENDING_TRANSACTION_V','','');
xxeis.eis_rs_ins.rc( 'WC Period Close Pending Transactions - All Orgs',660,'ORGANIZATION_CODE','Organization Code','Organization Code','','','default','','4','N','','','','','','','','HT038687','N','N','','XXWC_PENDING_TRANSACTION_V','','');
xxeis.eis_rs_ins.rc( 'WC Period Close Pending Transactions - All Orgs',660,'ORGANIZATION_NAME','Organization Name','Organization Name','','','default','','5','N','','','','','','','','HT038687','N','N','','XXWC_PENDING_TRANSACTION_V','','');
xxeis.eis_rs_ins.rc( 'WC Period Close Pending Transactions - All Orgs',660,'PERIOD_NAME','Period Name','Period Name','','','default','','1','N','','','','','','','','HT038687','N','N','','XXWC_PENDING_TRANSACTION_V','','');
xxeis.eis_rs_ins.rc( 'WC Period Close Pending Transactions - All Orgs',660,'RESOLUTION_TYPE','Resolution Type','Resolution Type','','','default','','2','N','','','','','','','','HT038687','N','N','','XXWC_PENDING_TRANSACTION_V','','');
xxeis.eis_rs_ins.rc( 'WC Period Close Pending Transactions - All Orgs',660,'TRANSACTION_TYPE','Transaction Type','Transaction Type','','','default','','3','N','','','','','','','','HT038687','N','N','','XXWC_PENDING_TRANSACTION_V','','');
--Inserting Report Parameters - WC Period Close Pending Transactions - All Orgs
xxeis.eis_rs_ins.rp( 'WC Period Close Pending Transactions - All Orgs',660,'Period','Period','PERIOD_NAME','IN','XXCUS_PERIOD_NAME GL','','VARCHAR2','Y','Y','1','','Y','CONSTANT','HT038687','Y','N','','','');
--Inserting Report Conditions - WC Period Close Pending Transactions - All Orgs
xxeis.eis_rs_ins.rcn( 'WC Period Close Pending Transactions - All Orgs',660,'PERIOD_NAME','IN',':Period','','','Y','1','Y','HT038687');
--Inserting Report Sorts - WC Period Close Pending Transactions - All Orgs
xxeis.eis_rs_ins.rs( 'WC Period Close Pending Transactions - All Orgs',660,'PERIOD_NAME','ASC','HT038687','1','');
--Inserting Report Triggers - WC Period Close Pending Transactions - All Orgs
--Inserting Report Templates - WC Period Close Pending Transactions - All Orgs
--Inserting Report Portals - WC Period Close Pending Transactions - All Orgs
--Inserting Report Dashboards - WC Period Close Pending Transactions - All Orgs
--Inserting Report Security - WC Period Close Pending Transactions - All Orgs
xxeis.eis_rs_ins.rsec( 'WC Period Close Pending Transactions - All Orgs','222','','51490',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC Period Close Pending Transactions - All Orgs','401','','51491',660,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC Period Close Pending Transactions - All Orgs','20005','','50900',660,'HT038687','','');
--Inserting Report Pivots - WC Period Close Pending Transactions - All Orgs
END;
/
set scan on define on
