--Report Name            : Return To Vendor (RTV) Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator.
begin
xxeis.eis_rsc_install_uninstall_pkg.rename_report('Pending RTV Report - WC', 'Return To Vendor (RTV) Report - WC', 201);
end;
/
prompt Creating Object Data EIS_XXWC_PENDING_RTV_RPT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_PENDING_RTV_RPT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_PENDING_RTV_RPT_V',201,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Pending Rtv Rpt V','EXPRRV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_PENDING_RTV_RPT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_PENDING_RTV_RPT_V',201,FALSE);
--Inserting Object Columns for EIS_XXWC_PENDING_RTV_RPT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','EMPLOYEE_ID',201,'Employee Id','EMPLOYEE_ID','','','','ANONYMOUS','NUMBER','','','Employee Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','ERROR_MESSAGE',201,'Error Message','ERROR_MESSAGE','','','','ANONYMOUS','VARCHAR2','','','Error Message','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','RETURN_NUMBER',201,'Return Number','RETURN_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Return Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','RETURN_REASON',201,'Return Reason','RETURN_REASON','','','','ANONYMOUS','VARCHAR2','','','Return Reason','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','NOTES',201,'Notes','NOTES','','','','ANONYMOUS','VARCHAR2','','','Notes','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','STATUS',201,'Status','STATUS','','','','ANONYMOUS','VARCHAR2','','','Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','CREATION_DATE',201,'Creation Date','CREATION_DATE','','','','ANONYMOUS','DATE','','','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','RESTOCKING_FEE',201,'Restocking Fee','RESTOCKING_FEE','','','','ANONYMOUS','NUMBER','','','Restocking Fee','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','SUPPLIER',201,'Supplier','SUPPLIER','','','','ANONYMOUS','VARCHAR2','','','Supplier','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','CREATED_BY',201,'Created By','CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','WC_RETURN_NUMBER',201,'Wc Return Number','WC_RETURN_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Wc Return Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','BUYER_ID',201,'Buyer Id','BUYER_ID','','','','ANONYMOUS','NUMBER','','','Buyer Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','BUYER_NAME',201,'Buyer Name','BUYER_NAME','','','','ANONYMOUS','VARCHAR2','','','Buyer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','ORG_CODE',201,'Org Code','ORG_CODE','','','','ANONYMOUS','VARCHAR2','','','Org Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','RTV_AMT',201,'Rtv Amt','RTV_AMT','','~T~D~2','','ANONYMOUS','NUMBER','','','Rtv Amt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','CDR_AMT',201,'Cdr Amt','CDR_AMT','','~T~D~2','','ANONYMOUS','NUMBER','','','Cdr Amt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','DAYS_PAST_CREATION_DATE',201,'Days Past Creation Date','DAYS_PAST_CREATION_DATE','','','','ANONYMOUS','NUMBER','','','Days Past Creation Date','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','LATE_Y_N',201,'Late Y N','LATE_Y_N','','','','ANONYMOUS','VARCHAR2','','','Late Y N','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','VENDOR_NUM',201,'Vendor Num','VENDOR_NUM','','','','ANONYMOUS','VARCHAR2','','','Vendor Num','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_PENDING_RTV_RPT_V','RENTAL_Y_N',201,'Rental Y N','RENTAL_Y_N','','','','ANONYMOUS','VARCHAR2','','','Rental Y N','','','','','');
--Inserting Object Components for EIS_XXWC_PENDING_RTV_RPT_V
--Inserting Object Component Joins for EIS_XXWC_PENDING_RTV_RPT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for Return To Vendor (RTV) Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Return To Vendor (RTV) Report - WC
xxeis.eis_rsc_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select distinct rtv_status_code from xxwc.xxwc_po_rtv_headers_tbl','','XX EIS RTV STATUS LOV','To Display RTV Status code','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select fu.user_id buyer_id, Description Buyer_name
from fnd_user fu, Po_agents pa
where fu.employee_id = pa.agent_id
AND (TRUNC(end_date_active) >= sysdate or end_date_active is null)','','XX EIS BUYER NAME LOV','To Display Buyer name','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT ood.organization_code organization_code,ood.organization_name organization_name  FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE EXISTS(SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V WHERE organization_id = ood.organization_id) ORDER BY organization_code','','PO Organization Code LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for Return To Vendor (RTV) Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Return To Vendor (RTV) Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Return To Vendor (RTV) Report - WC',201 );
--Inserting Report - Return To Vendor (RTV) Report - WC
xxeis.eis_rsc_ins.r( 201,'Return To Vendor (RTV) Report - WC','','The purpose of this report is List RTV�s in the system so follow up can be performed to ensure credit is being received from suppliers.','','','','MR020532','EIS_XXWC_PENDING_RTV_RPT_V','Y','','','MR020532','','N','White Cap Reports','PDF,','CSV,EXCEL,','N','','','','','','N','','US','','','','','','Y','','','','','','','','','','');
--Inserting Report Columns - Return To Vendor (RTV) Report - WC
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'CREATED_BY','Created By','Created By','','','default','','3','N','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'CREATION_DATE','Creation Date','Creation Date','','','default','','7','N','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'NOTES','Notes','Notes','','','default','','12','N','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'RESTOCKING_FEE','Restocking Fee','Restocking Fee','','~,~.~2','default','','6','N','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'RETURN_REASON','Return Reason','Return Reason','','','default','','13','N','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'STATUS','Status','Status','','','default','','11','N','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'SUPPLIER','Supplier','Supplier','','','default','','4','N','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'WC_RETURN_NUMBER','WC Return Number','Wc Return Number','','','default','','2','N','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'CDR_AMT','Cdr Amt','Cdr Amt','','~,~.~2','default','','14','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'RTV_AMT','Rtv Amt','Rtv Amt','','~,~.~2','default','','15','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'ORG_CODE','Ship From Branch','Org Code','','','default','','1','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'RETURN_NUMBER','Supplier RGA #','Return Number','','','default','','5','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'DAYS_PAST_CREATION_DATE','Days Past Creation Date','Days Past Creation Date','','~~~','default','','8','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'LATE_Y_N','Late Y/N','Late Y N','','','default','','9','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Return To Vendor (RTV) Report - WC',201,'RENTAL_Y_N','Rental (Y/N)','Rental Y N','','','default','','10','','Y','','','','','','','MR020532','N','N','','EIS_XXWC_PENDING_RTV_RPT_V','','','','US','','');
--Inserting Report Parameters - Return To Vendor (RTV) Report - WC
xxeis.eis_rsc_ins.rp( 'Return To Vendor (RTV) Report - WC',201,'Date From','Date From','CREATION_DATE','>=','','','DATE','N','Y','1','Y','Y','CONSTANT','MR020532','Y','N','','Start Date','','EIS_XXWC_PENDING_RTV_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return To Vendor (RTV) Report - WC',201,'Date TO','Date To','CREATION_DATE','<=','','','DATE','N','Y','2','Y','Y','CONSTANT','MR020532','Y','N','','End Date','','EIS_XXWC_PENDING_RTV_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return To Vendor (RTV) Report - WC',201,'Org Code','Org Code','ORG_CODE','IN','PO Organization Code LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_PENDING_RTV_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return To Vendor (RTV) Report - WC',201,'Created By','Created By','BUYER_ID','IN','XX EIS BUYER NAME LOV','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_PENDING_RTV_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return To Vendor (RTV) Report - WC',201,'Status','Status','STATUS','IN','XX EIS RTV STATUS LOV','Pending','VARCHAR2','Y','Y','6','Y','Y','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_PENDING_RTV_RPT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Return To Vendor (RTV) Report - WC',201,'Organization List','Organization List','','','XXWC Org List','','VARCHAR2','N','Y','4','N','N','CONSTANT','MR020532','Y','N','','','','EIS_XXWC_PENDING_RTV_RPT_V','','','US','');
--Inserting Dependent Parameters - Return To Vendor (RTV) Report - WC
--Inserting Report Conditions - Return To Vendor (RTV) Report - WC
xxeis.eis_rsc_ins.rcnh( 'Return To Vendor (RTV) Report - WC',201,'Org List FreeText','FREE_TEXT','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','AND (:Organization List is null or EXISTS (SELECT 1  FROM
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME= :Organization List
                            AND LIST_TYPE =''Org''
                         AND DBMS_LOB.INSTR(LIST_VALUES,EXPRRV.ORG_CODE,1,1)<>0))','1',201,'Return To Vendor (RTV) Report - WC','Org List FreeText');
xxeis.eis_rsc_ins.rcnh( 'Return To Vendor (RTV) Report - WC',201,'EXPRRV.CREATION_DATE >= Date From','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Date From','','','','','EIS_XXWC_PENDING_RTV_RPT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'Return To Vendor (RTV) Report - WC','EXPRRV.CREATION_DATE >= Date From');
xxeis.eis_rsc_ins.rcnh( 'Return To Vendor (RTV) Report - WC',201,'EXPRRV.CREATION_DATE <= Date TO','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Date TO','','','','','EIS_XXWC_PENDING_RTV_RPT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'Return To Vendor (RTV) Report - WC','EXPRRV.CREATION_DATE <= Date TO');
xxeis.eis_rsc_ins.rcnh( 'Return To Vendor (RTV) Report - WC',201,'EXPRRV.ORG_CODE IN Org Code','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','ORG_CODE','','Org Code','','','','','EIS_XXWC_PENDING_RTV_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'Return To Vendor (RTV) Report - WC','EXPRRV.ORG_CODE IN Org Code');
xxeis.eis_rsc_ins.rcnh( 'Return To Vendor (RTV) Report - WC',201,'EXPRRV.BUYER_ID IN Created By','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','BUYER_ID','','Created By','','','','','EIS_XXWC_PENDING_RTV_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'Return To Vendor (RTV) Report - WC','EXPRRV.BUYER_ID IN Created By');
xxeis.eis_rsc_ins.rcnh( 'Return To Vendor (RTV) Report - WC',201,'EXPRRV.STATUS IN Status','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','STATUS','','Status','','','','','EIS_XXWC_PENDING_RTV_RPT_V','','','','','','IN','Y','Y','','','','','1',201,'Return To Vendor (RTV) Report - WC','EXPRRV.STATUS IN Status');
--Inserting Report Sorts - Return To Vendor (RTV) Report - WC
xxeis.eis_rsc_ins.rs( 'Return To Vendor (RTV) Report - WC',201,'WC_RETURN_NUMBER','DESC','MR020532','1','');
xxeis.eis_rsc_ins.rs( 'Return To Vendor (RTV) Report - WC',201,'CREATION_DATE','DESC','MR020532','2','');
xxeis.eis_rsc_ins.rs( 'Return To Vendor (RTV) Report - WC',201,'SUPPLIER','DESC','MR020532','3','');
--Inserting Report Triggers - Return To Vendor (RTV) Report - WC
--inserting report templates - Return To Vendor (RTV) Report - WC
--Inserting Report Portals - Return To Vendor (RTV) Report - WC
--inserting report dashboards - Return To Vendor (RTV) Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Return To Vendor (RTV) Report - WC','201','EIS_XXWC_PENDING_RTV_RPT_V','EIS_XXWC_PENDING_RTV_RPT_V','N','');
--inserting report security - Return To Vendor (RTV) Report - WC
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','','10012196','',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','660','','XXWC_ORDER_MGMT_SUPER_USER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','201','','XXWC_PURCHASING_SR_MRG_WC',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','201','','XXWC_PURCHASING_RPM',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','200','','XXWC_PAY_MANAGER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','200','','XXWC_PAY_NO_CALENDAR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','201','','XXWC_PUR_SUPER_USER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','201','','HDS_PRCHSNG_SPR_USR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','201','','XXWC_PURCHASING_MGR',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','201','','XXWC_PURCHASING_INQUIRY',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','201','','XXWC_PURCHASING_BUYER',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',201,'MR020532','','','');
xxeis.eis_rsc_ins.rsec( 'Return To Vendor (RTV) Report - WC','401','','XXWC_RECEIVING_ASSOCIATE',201,'MR020532','','','');
--Inserting Report Pivots - Return To Vendor (RTV) Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Return To Vendor (RTV) Report - WC
xxeis.eis_rsc_ins.rv( 'Return To Vendor (RTV) Report - WC','','Return To Vendor (RTV) Report - WC','SA059956','18-DEC-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
