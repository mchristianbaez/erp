--Report Name            : White Cap National Accounts Report (NonTrend) Internal
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_NATACCT_NON_TRND_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_NATACCT_NON_TRND_V
Create or replace View XXEIS.EIS_XXWC_OM_NATACCT_NON_TRND_V
(PARENT_NAME,INVOICE_DATE_COND,INVOICE_DATE,INVOICE_NUMBER,ACCOUNT_NUMBER,ACCOUNT_NAME,JOB_NUMBER,JOB_NAME,CUSTOMER_JOB,CUSTOMER_PO,LOC,SALESREP_NUMBER,PART_NUMBER,PART_DESCRIPTION,VENDOR_NAME,VENDOR_NUMBER,INV_CAT_CLASS,QTY,SALES,COST,UNIT_SELLING_PRICE,UNIT_COST,ORDER_NUMBER,ORDER_LINE,ORDERED_DATE_COND,ORDERED_DATE,CUSTOMER_CLASS_CODE,HEADER_ID,LINE_ID,ORGANIZATION_ID,PARTY_ID,CUST_ACCOUNT_ID,INVENTORY_ITEM_ID,MSI_ORGANIZATION_ID,CUST_ACCT#PARTY_TYPE,CUST_ACCT#VNDR_CODE_AND_FRUL,CUST_ACCT#BRANCH_DESCRIPTION,CUST_ACCT#CUSTOMER_SOURCE,CUST_ACCT#LEGAL_COLLECTION_I,CUST_ACCT#PRISM_NUMBER,CUST_ACCT#YES#NOTE_LINE_#5,CUST_ACCT#YES#NOTE_LINE_#1,CUST_ACCT#YES#NOTE_LINE_#2,CUST_ACCT#YES#NOTE_LINE_#3,CUST_ACCT#YES#NOTE_LINE_#4,PARTY#101#PARTY_TYPE,PARTY#101#COLLECTOR,SHIP_FROM_ORG#FACTORY_PLANNE,SHIP_FROM_ORG#FRU,SHIP_FROM_ORG#LOCATION_NUMBE,SHIP_FROM_ORG#BRANCH_OPERATI,SHIP_FROM_ORG#DELIVER_CHARGE,SHIP_FROM_ORG#FACTORY_PLANNE1,SHIP_FROM_ORG#FACTORY_PLANNE2,SHIP_FROM_ORG#FACTORY_PLANNE3,SHIP_FROM_ORG#FACTORY_PLANNE4,SHIP_FROM_ORG#PRICING_ZONE,SHIP_FROM_ORG#ORG_TYPE,SHIP_FROM_ORG#DISTRICT,SHIP_FROM_ORG#REGION,MSI#HDS#LOB,MSI#HDS#DROP_SHIPMENT_ELIGAB,MSI#HDS#INVOICE_UOM,MSI#HDS#PRODUCT_ID,MSI#HDS#VENDOR_PART_NUMBER,MSI#HDS#UNSPSC_CODE,MSI#HDS#UPC_PRIMARY,MSI#HDS#SKU_DESCRIPTION,MSI#WC#CA_PROP_65,MSI#WC#COUNTRY_OF_ORIGIN,MSI#WC#ORM_D_FLAG,MSI#WC#STORE_VELOCITY,MSI#WC#DC_VELOCITY,MSI#WC#YEARLY_STORE_VELOCITY,MSI#WC#YEARLY_DC_VELOCITY,MSI#WC#PRISM_PART_NUMBER,MSI#WC#HAZMAT_DESCRIPTION,MSI#WC#HAZMAT_CONTAINER,MSI#WC#GTP_INDICATOR,MSI#WC#LAST_LEAD_TIME,MSI#WC#AMU,MSI#WC#RESERVE_STOCK,MSI#WC#TAXWARE_CODE,MSI#WC#AVERAGE_UNITS,MSI#WC#PRODUCT_CODE,MSI#WC#IMPORT_DUTY_,MSI#WC#KEEP_ITEM_ACTIVE,MSI#WC#PESTICIDE_FLAG,MSI#WC#CALC_LEAD_TIME,MSI#WC#VOC_GL,MSI#WC#PESTICIDE_FLAG_STATE,MSI#WC#VOC_CATEGORY,MSI#WC#VOC_SUB_CATEGORY,MSI#WC#MSDS_#,MSI#WC#HAZMAT_PACKAGING_GROU,OH#RENTAL_TERM,OH#ORDER_CHANNEL,OL#ESTIMATED_RETURN_DATE,OL#RERENT_PO,OL#FORCE_SHIP,OL#RENTAL_DATE,OL#ITEM_ON_BLOWOUT,OL#POF_STD_LINE,OL#RERENTAL_BILLING_TERMS,OL#PRICING_GUARDRAIL,OL#PRINT_EXPIRED_PRODUCT_DIS,OL#RENTAL_CHARGE,OL#VENDOR_QUOTE_COST,OL#PO_COST_FOR_VENDOR_QUOTE,OL#SERIAL_NUMBER,OL#ENGINEERING_COST,OL#APPLICATION_METHOD) AS 
SELECT cust_acct.attribute4 parent_name,
    TRUNC(rct.trx_date) invoice_date_cond ,
    (TO_CHAR(rct.trx_date,'MM/DD/YYYY')) invoice_date ,
    rct.trx_number invoice_number,
    CUST_ACCT.ACCOUNT_NUMBER ,
    NVL(Cust_Acct.Account_Name,party.party_NAME) Account_Name ,
    hzps_ship_to.party_site_number job_number,
    CASE
      WHEN instr(Hzcs_Ship_To.location,'-') > 0
      THEN SUBSTR(Hzcs_Ship_To.location,instr(Hzcs_Ship_To.location,'-')+1)
      ELSE NULL
    END Job_Name,
    --SUBSTR(Hzps_Ship_To.Party_Site_Name,5) Job_Name,
    Hzcs_Ship_To.Location Customer_Job,
    OH.Cust_Po_Number Customer_Po,
    Ship_From_Org.Organization_Code Loc,
    rep.salesrep_number,
    DECODE(msi.segment1,'Rental Charge',xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item(ol.link_to_line_id),msi.segment1) part_number,
    DECODE(msi.segment1,'Rental Charge',xxeis.eis_rs_xxwc_com_util_pkg.get_rental_item_desc(ol.link_to_line_id),msi.description) Part_Description,
    DECODE(msi.segment1,'Rental Charge',xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(ol.link_to_line_id),xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id)) vendor_name,
    DECODE(msi.segment1,'Rental Charge',xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(ol.link_to_line_id),xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id)) vendor_number,
    xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) inv_cat_class,
    NVL (rctl.quantity_invoiced, rctl.quantity_credited)QTY,
    --NVL(RCTL.QUANTITY_ORDERED,0) QTY,
    ROUND((NVL(OL.UNIT_SELLING_PRICE,0)                                       * NVL (rctl.quantity_invoiced, rctl.quantity_credited)),2) SALES,
    ROUND((NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0) * NVL (rctl.quantity_invoiced, rctl.quantity_credited)),2) Cost,
    NVL(Ol.Unit_Selling_Price,0) Unit_Selling_Price,
    NVL(apps.xxwc_mv_routines_pkg.get_order_line_cost (ol.line_id), 0) Unit_Cost,
    --NVL(Ol.Unit_Cost,0) Unit_Cost ,
    oh.order_number ORDER_NUMBER,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER
    ||'.'
    ||ol.option_number)) order_line,
    TRUNC(oh.ordered_date) ordered_date_cond ,
    (TO_CHAR(Oh.Ordered_Date,'MM/DD/YYYY')) Ordered_Date ,
    cust_acct.customer_class_code,
    ---Primary Keys
    oh.header_id ,
    ol.line_id ,
    ship_from_org.organization_id ,
    party.party_id ,
    cust_acct.cust_account_id ,
    msi.inventory_item_id ,
    Msi.Organization_Id Msi_Organization_Id
    --descr#flexfield#start
    ,
    xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',CUST_ACCT.ATTRIBUTE1,'I') CUST_ACCT#Party_Type ,
    CUST_ACCT.ATTRIBUTE2 CUST_ACCT#Vndr_Code_and_FRUL ,
    CUST_ACCT.ATTRIBUTE3 CUST_ACCT#Branch_Description ,
    CUST_ACCT.ATTRIBUTE4 CUST_ACCT#Customer_Source ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_LEGAL_COLLECTION',CUST_ACCT.ATTRIBUTE5,'F') CUST_ACCT#Legal_Collection_I ,
    CUST_ACCT.ATTRIBUTE6 CUST_ACCT#Prism_Number ,
    DECODE(CUST_ACCT.ATTRIBUTE_CATEGORY ,'Yes',CUST_ACCT.ATTRIBUTE16, NULL) CUST_ACCT#Yes#Note_Line_#5 ,
    DECODE(CUST_ACCT.ATTRIBUTE_CATEGORY ,'Yes',CUST_ACCT.ATTRIBUTE17, NULL) CUST_ACCT#Yes#Note_Line_#1 ,
    DECODE(CUST_ACCT.ATTRIBUTE_CATEGORY ,'Yes',CUST_ACCT.ATTRIBUTE18, NULL) CUST_ACCT#Yes#Note_Line_#2 ,
    DECODE(CUST_ACCT.ATTRIBUTE_CATEGORY ,'Yes',CUST_ACCT.ATTRIBUTE19, NULL) CUST_ACCT#Yes#Note_Line_#3 ,
    DECODE(CUST_ACCT.ATTRIBUTE_CATEGORY ,'Yes',CUST_ACCT.ATTRIBUTE20, NULL) CUST_ACCT#Yes#Note_Line_#4 ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',PARTY.ATTRIBUTE1,'I'), NULL) PARTY#101#Party_Type ,
    DECODE(PARTY.ATTRIBUTE_CATEGORY ,'101',xxeis.eis_rs_dff.decode_valueset( 'XXCUSOZF_AR_COLLECTORS',PARTY.ATTRIBUTE3,'F'), NULL) PARTY#101#Collector ,
    SHIP_FROM_ORG.ATTRIBUTE1 SHIP_FROM_ORG#Factory_Planne ,
    SHIP_FROM_ORG.ATTRIBUTE10 SHIP_FROM_ORG#FRU ,
    SHIP_FROM_ORG.ATTRIBUTE11 SHIP_FROM_ORG#Location_Numbe ,
    xxeis.eis_rs_dff.decode_valueset( 'HR_DE_EMPLOYEES',SHIP_FROM_ORG.ATTRIBUTE13,'F') SHIP_FROM_ORG#Branch_Operati ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_DELIVERY_CHARGE_EXEMPT',SHIP_FROM_ORG.ATTRIBUTE14,'I') SHIP_FROM_ORG#Deliver_Charge ,
    SHIP_FROM_ORG.ATTRIBUTE2 SHIP_FROM_ORG#Factory_Planne1 ,
    SHIP_FROM_ORG.ATTRIBUTE3 SHIP_FROM_ORG#Factory_Planne2 ,
    SHIP_FROM_ORG.ATTRIBUTE4 SHIP_FROM_ORG#Factory_Planne3 ,
    SHIP_FROM_ORG.ATTRIBUTE5 SHIP_FROM_ORG#Factory_Planne4 ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_PRICE_ZONES',SHIP_FROM_ORG.ATTRIBUTE6,'F') SHIP_FROM_ORG#Pricing_Zone ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_ORG_TYPE',SHIP_FROM_ORG.ATTRIBUTE7,'I') SHIP_FROM_ORG#Org_Type ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_DISTRICT',SHIP_FROM_ORG.ATTRIBUTE8,'I') SHIP_FROM_ORG#District ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_REGION',SHIP_FROM_ORG.ATTRIBUTE9,'I') SHIP_FROM_ORG#Region ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE1, NULL) MSI#HDS#LOB ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE10,'F'), NULL) MSI#HDS#Drop_Shipment_Eligab ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE15, NULL) MSI#HDS#Invoice_UOM ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE2, NULL) MSI#HDS#Product_ID ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE3, NULL) MSI#HDS#Vendor_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE4, NULL) MSI#HDS#UNSPSC_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE5, NULL) MSI#HDS#UPC_Primary ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'HDS',MSI.ATTRIBUTE6, NULL) MSI#HDS#SKU_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE1, NULL) MSI#WC#CA_Prop_65 ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE10, NULL) MSI#WC#Country_of_Origin ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE11,'F'), NULL) MSI#WC#ORM_D_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE12, NULL) MSI#WC#Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE13, NULL) MSI#WC#DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE14, NULL) MSI#WC#Yearly_Store_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE15, NULL) MSI#WC#Yearly_DC_Velocity ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE16, NULL) MSI#WC#PRISM_Part_Number ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE17, NULL) MSI#WC#Hazmat_Description ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC HAZMAT CONTAINER',MSI.ATTRIBUTE18,'I'), NULL) MSI#WC#Hazmat_Container ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE19, NULL) MSI#WC#GTP_Indicator ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE2, NULL) MSI#WC#Last_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE20, NULL) MSI#WC#AMU ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE21, NULL) MSI#WC#Reserve_Stock ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC TAXWARE CODE',MSI.ATTRIBUTE22,'I'), NULL) MSI#WC#Taxware_Code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE25, NULL) MSI#WC#Average_Units ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE26, NULL) MSI#WC#Product_code ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE27, NULL) MSI#WC#Import_Duty_ ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE29,'F'), NULL) MSI#WC#Keep_Item_Active ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'Yes_No',MSI.ATTRIBUTE3,'F'), NULL) MSI#WC#Pesticide_Flag ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE30, NULL) MSI#WC#Calc_Lead_Time ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE4, NULL) MSI#WC#VOC_GL ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE5, NULL) MSI#WC#Pesticide_Flag_State ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE6, NULL) MSI#WC#VOC_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE7, NULL) MSI#WC#VOC_Sub_Category ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',MSI.ATTRIBUTE8, NULL) MSI#WC#MSDS_# ,
    DECODE(MSI.ATTRIBUTE_CATEGORY ,'WC',xxeis.eis_rs_dff.decode_valueset( 'XXWC_HAZMAT_PACKAGE_GROUP',MSI.ATTRIBUTE9,'I'), NULL) MSI#WC#Hazmat_Packaging_Grou ,
    xxeis.eis_rs_dff.decode_valueset( 'RENTAL TYPE',OH.ATTRIBUTE1,'I') OH#Rental_Term ,
    xxeis.eis_rs_dff.decode_valueset( 'WC_CUS_ORDER_CHANNEL',OH.ATTRIBUTE2,'I') OH#Order_Channel ,
    OL.ATTRIBUTE1 OL#Estimated_Return_Date ,
    OL.ATTRIBUTE10 OL#ReRent_PO ,
    OL.ATTRIBUTE11 OL#Force_Ship ,
    OL.ATTRIBUTE12 OL#Rental_Date ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',OL.ATTRIBUTE15,'F') OL#Item_on_Blowout ,
    OL.ATTRIBUTE19 OL#POF_Std_Line ,
    xxeis.eis_rs_dff.decode_valueset( 'ReRental Billing Type',OL.ATTRIBUTE2,'I') OL#ReRental_Billing_Terms ,
    OL.ATTRIBUTE20 OL#Pricing_Guardrail ,
    xxeis.eis_rs_dff.decode_valueset( 'Yes_No',OL.ATTRIBUTE3,'F') OL#Print_Expired_Product_Dis ,
    OL.ATTRIBUTE4 OL#Rental_Charge ,
    OL.ATTRIBUTE5 OL#Vendor_Quote_Cost ,
    OL.ATTRIBUTE6 OL#PO_Cost_for_Vendor_Quote ,
    OL.ATTRIBUTE7 OL#Serial_Number ,
    OL.ATTRIBUTE8 OL#Engineering_Cost ,
    xxeis.eis_rs_dff.decode_valueset( 'XXWC_CSP_APP_METHOD',OL.ATTRIBUTE9,'I') OL#Application_Method
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org,
    hz_parties party,
    hz_cust_accounts cust_acct,
    oe_order_headers oh,
    oe_order_lines ol,
    Ra_Customer_Trx Rct,
    ra_customer_trx_lines rctl,
    Hr_ALL_ORGANIZATION_UNITS Hou,
    ra_salesreps rep,
    Ra_Cust_Trx_Line_Gl_Dist Gd,
    HZ_CUST_SITE_USES HZCS_SHIP_TO,
    hz_cust_acct_sites hcas_ship_to,
    HZ_PARTY_SITES HZPS_SHIP_TO,
    hz_locations hzl_ship_to,
    mtl_system_items_kfv msi
  WHERE ol.sold_to_org_id            = cust_acct.cust_account_id
  AND cust_acct.party_id             = party.party_id
  AND ol.ship_from_org_id            = ship_from_org.organization_id
 -- AND ol.link_to_line_id             = olr.line_id (+)
  AND OL.HEADER_ID                   = OH.HEADER_ID
  AND TO_CHAR(Oh.Order_Number)       = TO_CHAR(Rct.Interface_Header_Attribute1)
  AND TO_CHAR(OL.LINE_ID)            = TO_CHAR(RCTL.INTERFACE_LINE_ATTRIBUTE6)
  AND rctl.INTERFACE_LINE_CONTEXT    = 'ORDER ENTRY'
  AND rct .INTERFACE_HEADER_CONTEXT  = 'ORDER ENTRY'
  AND rct.customer_trx_id            = rctl.customer_trx_id
  AND msi.organization_id            = hou.organization_id
  AND oh.salesrep_id                 = rep.salesrep_id(+)
  AND oh.org_id                      = rep.org_id(+)
  AND MSI.INVENTORY_ITEM_ID          = OL.INVENTORY_ITEM_ID
  AND msi.organization_id            = ol.ship_from_org_id
  AND rct.customer_trx_id            = gd.customer_trx_id(+)
  AND 'REC'                          = NVL(Gd.Account_Class,'REC')
  AND 'Y'                            = NVL(Gd.Latest_Rec_Flag,'Y')
  AND Oh.Ship_To_Org_Id              = Hzcs_Ship_To.Site_Use_Id(+)
  AND Hzcs_Ship_To.Cust_Acct_Site_Id = Hcas_Ship_To.Cust_Acct_Site_Id(+)
  AND hcas_ship_to.party_site_id     = hzps_ship_to.party_site_id(+)
  AND hzl_ship_to.location_id(+)     = hzps_ship_to.location_id
  AND upper(cust_acct.attribute4) LIKE 'NAT%'
  --AND oh.order_number ='10001010'
/
set scan on define on
prompt Creating View Data for White Cap National Accounts Report (NonTrend) Internal
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_NATACCT_NON_TRND_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_NATACCT_NON_TRND_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Natacct Non Trnd V','EXONNTV','','');
--Delete View Columns for EIS_XXWC_OM_NATACCT_NON_TRND_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_NATACCT_NON_TRND_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_NATACCT_NON_TRND_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SALES',660,'Sales','SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','PART_DESCRIPTION',660,'Part Description','PART_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','LOC',660,'Loc','LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUSTOMER_PO',660,'Customer Po','CUSTOMER_PO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Po','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','ACCOUNT_NAME',660,'Account Name','ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','ACCOUNT_NUMBER',660,'Account Number','ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','PARENT_NAME',660,'Parent Name','PARENT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Parent Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','JOB_NAME',660,'Job Name','JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','JOB_NUMBER',660,'Job Number','JOB_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SALESREP_NUMBER',660,'Salesrep Number','SALESREP_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUSTOMER_CLASS_CODE',660,'Customer Class Code','CUSTOMER_CLASS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Class Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUSTOMER_JOB',660,'Customer Job','CUSTOMER_JOB','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Job','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','INV_CAT_CLASS',660,'Inv Cat Class','INV_CAT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Inv Cat Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','ORDER_LINE',660,'Order Line','ORDER_LINE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Line','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','INVOICE_DATE_COND',660,'Invoice Date Cond','INVOICE_DATE_COND','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date Cond','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','COST',660,'Cost','COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','UNIT_COST',660,'Unit Cost','UNIT_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','ORDERED_DATE_COND',660,'Ordered Date Cond','ORDERED_DATE_COND','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date Cond','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#PARTY_TYPE',660,'Descriptive flexfield: Customer Information Column Name: Party Type','CUST_ACCT#Party_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE1','Cust Acct#Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#VNDR_CODE_AND_FRUL',660,'Descriptive flexfield: Customer Information Column Name: Vndr Code and FRULOC','CUST_ACCT#Vndr_Code_and_FRUL','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE2','Cust Acct#Vndr Code And Fruloc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#BRANCH_DESCRIPTION',660,'Descriptive flexfield: Customer Information Column Name: Branch Description','CUST_ACCT#Branch_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE3','Cust Acct#Branch Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#CUSTOMER_SOURCE',660,'Descriptive flexfield: Customer Information Column Name: Customer Source','CUST_ACCT#Customer_Source','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE4','Cust Acct#Customer Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#LEGAL_COLLECTION_I',660,'Descriptive flexfield: Customer Information Column Name: Legal Collection Indicator','CUST_ACCT#Legal_Collection_I','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE5','Cust Acct#Legal Collection Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#PRISM_NUMBER',660,'Descriptive flexfield: Customer Information Column Name: Prism Number','CUST_ACCT#Prism_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE6','Cust Acct#Prism Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#YES#NOTE_LINE_#5',660,'Descriptive flexfield: Customer Information Column Name: Note Line #5 Context: Yes','CUST_ACCT#Yes#Note_Line_#5','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE16','Cust Acct#Yes#Note Line #5','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#YES#NOTE_LINE_#1',660,'Descriptive flexfield: Customer Information Column Name: Note Line #1 Context: Yes','CUST_ACCT#Yes#Note_Line_#1','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE17','Cust Acct#Yes#Note Line #1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#YES#NOTE_LINE_#2',660,'Descriptive flexfield: Customer Information Column Name: Note Line #2 Context: Yes','CUST_ACCT#Yes#Note_Line_#2','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE18','Cust Acct#Yes#Note Line #2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#YES#NOTE_LINE_#3',660,'Descriptive flexfield: Customer Information Column Name: Note Line #3 Context: Yes','CUST_ACCT#Yes#Note_Line_#3','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE19','Cust Acct#Yes#Note Line #3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','CUST_ACCT#YES#NOTE_LINE_#4',660,'Descriptive flexfield: Customer Information Column Name: Note Line #4 Context: Yes','CUST_ACCT#Yes#Note_Line_#4','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE20','Cust Acct#Yes#Note Line #4','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','PARTY#101#PARTY_TYPE',660,'Descriptive flexfield: Party Information Column Name: Party Type Context: 101','PARTY#101#Party_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Party#101#Party Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','PARTY#101#COLLECTOR',660,'Descriptive flexfield: Party Information Column Name: Collector Context: 101','PARTY#101#Collector','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','ATTRIBUTE3','Party#101#Collector','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#FACTORY_PLANNE',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Data Directory','SHIP_FROM_ORG#Factory_Planne','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE1','Ship From Org#Factory Planner Data Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#FRU',660,'Descriptive flexfield: Organization parameters Column Name: FRU','SHIP_FROM_ORG#FRU','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE10','Ship From Org#Fru','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#LOCATION_NUMBE',660,'Descriptive flexfield: Organization parameters Column Name: Location Number','SHIP_FROM_ORG#Location_Numbe','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE11','Ship From Org#Location Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#BRANCH_OPERATI',660,'Descriptive flexfield: Organization parameters Column Name: Branch Operations Manager','SHIP_FROM_ORG#Branch_Operati','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE13','Ship From Org#Branch Operations Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#DELIVER_CHARGE',660,'Descriptive flexfield: Organization parameters Column Name: Deliver Charge','SHIP_FROM_ORG#Deliver_Charge','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE14','Ship From Org#Deliver Charge','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#FACTORY_PLANNE1',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Executable Directory','SHIP_FROM_ORG#Factory_Planne1','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE2','Ship From Org#Factory Planner Executable Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#FACTORY_PLANNE2',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner User','SHIP_FROM_ORG#Factory_Planne2','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE3','Ship From Org#Factory Planner User','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#FACTORY_PLANNE3',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Host','SHIP_FROM_ORG#Factory_Planne3','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE4','Ship From Org#Factory Planner Host','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#FACTORY_PLANNE4',660,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Port Number','SHIP_FROM_ORG#Factory_Planne4','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE5','Ship From Org#Factory Planner Port Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#PRICING_ZONE',660,'Descriptive flexfield: Organization parameters Column Name: Pricing Zone','SHIP_FROM_ORG#Pricing_Zone','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE6','Ship From Org#Pricing Zone','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#ORG_TYPE',660,'Descriptive flexfield: Organization parameters Column Name: Org Type','SHIP_FROM_ORG#Org_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE7','Ship From Org#Org Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#DISTRICT',660,'Descriptive flexfield: Organization parameters Column Name: District','SHIP_FROM_ORG#District','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE8','Ship From Org#District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','SHIP_FROM_ORG#REGION',660,'Descriptive flexfield: Organization parameters Column Name: Region','SHIP_FROM_ORG#Region','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE9','Ship From Org#Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#HDS#LOB',660,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',660,'Descriptive flexfield: Items Column Name: Drop Shipment Eligable Context: HDS','MSI#HDS#Drop_Shipment_Eligab','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment Eligable','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#HDS#INVOICE_UOM',660,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#HDS#PRODUCT_ID',660,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#HDS#VENDOR_PART_NUMBER',660,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#HDS#UNSPSC_CODE',660,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#HDS#UPC_PRIMARY',660,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#HDS#SKU_DESCRIPTION',660,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#CA_PROP_65',660,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#COUNTRY_OF_ORIGIN',660,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#ORM_D_FLAG',660,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#STORE_VELOCITY',660,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#DC_VELOCITY',660,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#YEARLY_STORE_VELOCITY',660,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#YEARLY_DC_VELOCITY',660,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#PRISM_PART_NUMBER',660,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#HAZMAT_DESCRIPTION',660,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#HAZMAT_CONTAINER',660,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#GTP_INDICATOR',660,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#LAST_LEAD_TIME',660,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#AMU',660,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#RESERVE_STOCK',660,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#TAXWARE_CODE',660,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#AVERAGE_UNITS',660,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#PRODUCT_CODE',660,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#IMPORT_DUTY_',660,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#KEEP_ITEM_ACTIVE',660,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#PESTICIDE_FLAG',660,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#CALC_LEAD_TIME',660,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#VOC_GL',660,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#PESTICIDE_FLAG_STATE',660,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#VOC_CATEGORY',660,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#VOC_SUB_CATEGORY',660,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#MSDS_#',660,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MSI#WC#HAZMAT_PACKAGING_GROU',660,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OH#RENTAL_TERM',660,'Descriptive flexfield: Additional Header Information Column Name: Rental Term','OH#Rental_Term','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE1','Oh#Rental Term','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OH#ORDER_CHANNEL',660,'Descriptive flexfield: Additional Header Information Column Name: Order Channel','OH#Order_Channel','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE2','Oh#Order Channel','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#ESTIMATED_RETURN_DATE',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Estimated Return Date','OL#Estimated_Return_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE1','Ol#Estimated Return Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#RERENT_PO',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: ReRent PO','OL#ReRent_PO','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE10','Ol#Rerent Po','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#FORCE_SHIP',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Force Ship','OL#Force_Ship','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE11','Ol#Force Ship','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#RENTAL_DATE',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Rental Date','OL#Rental_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE12','Ol#Rental Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#ITEM_ON_BLOWOUT',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Item on Blowout?','OL#Item_on_Blowout','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE15','Ol#Item On Blowout?','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#POF_STD_LINE',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: POF Std Line','OL#POF_Std_Line','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE19','Ol#Pof Std Line','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#RERENTAL_BILLING_TERMS',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: ReRental Billing Terms','OL#ReRental_Billing_Terms','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE2','Ol#Rerental Billing Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#PRICING_GUARDRAIL',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Pricing Guardrail','OL#Pricing_Guardrail','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE20','Ol#Pricing Guardrail','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#PRINT_EXPIRED_PRODUCT_DIS',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Print Expired Product Disclaim','OL#Print_Expired_Product_Dis','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE3','Ol#Print Expired Product Disclaim','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#RENTAL_CHARGE',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Rental Charge','OL#Rental_Charge','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE4','Ol#Rental Charge','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#VENDOR_QUOTE_COST',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Vendor Quote Cost','OL#Vendor_Quote_Cost','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE5','Ol#Vendor Quote Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#PO_COST_FOR_VENDOR_QUOTE',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: PO Cost for Vendor Quote','OL#PO_Cost_for_Vendor_Quote','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE6','Ol#Po Cost For Vendor Quote','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#SERIAL_NUMBER',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Serial Number','OL#Serial_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE7','Ol#Serial Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#ENGINEERING_COST',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Engineering Cost','OL#Engineering_Cost','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE8','Ol#Engineering Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OL#APPLICATION_METHOD',660,'Descriptive flexfield: Additional Line Attribute Information Column Name: Application Method','OL#Application_Method','','','','XXEIS_RS_ADMIN','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE9','Ol#Application Method','','','');
--Inserting View Components for EIS_XXWC_OM_NATACCT_NON_TRND_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','SHIP_FROM_ORG','SHIP_FROM_ORG','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','HZ_PARTIES',660,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','CUST_ACCT','CUST_ACCT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_OM_NATACCT_NON_TRND_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MTL_PARAMETERS','SHIP_FROM_ORG',660,'EXONNTV.ORGANIZATION_ID','=','SHIP_FROM_ORG.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','HZ_PARTIES','PARTY',660,'EXONNTV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','HZ_CUST_ACCOUNTS','CUST_ACCT',660,'EXONNTV.CUST_ACCOUNT_ID','=','CUST_ACCT.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OE_ORDER_HEADERS','OH',660,'EXONNTV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','OE_ORDER_LINES','OL',660,'EXONNTV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXONNTV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_NATACCT_NON_TRND_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXONNTV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap National Accounts Report (NonTrend) Internal
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap National Accounts Report (NonTrend) Internal
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct hca.ATTRIBUTE4 Parent_Name from hz_cust_accounts hca','','OM Parent Account Name LOV','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap National Accounts Report (NonTrend) Internal
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap National Accounts Report (NonTrend) Internal
xxeis.eis_rs_utility.delete_report_rows( 'White Cap National Accounts Report (NonTrend) Internal' );
--Inserting Report - White Cap National Accounts Report (NonTrend) Internal
xxeis.eis_rs_ins.r( 660,'White Cap National Accounts Report (NonTrend) Internal','','The purpose of this report is to present the account-invoice line level sales, cost and GM% for each White Cap National Account for specified time period.

This report can be generated for either internal or external purposes.  If prepared for internal use, then all columns / fields will be presented (e.g. Sales, Cost, GM%).  If prepared for external reporting, then only the Sales columns are presented (i.e. the Cost and GM% columns are omitted).

If this report is generated on an ad-hoc basis, the user will have the ability to select the Customer Source, Master Accounts, date range and the Internal/External flag.

This report may be scheduled for distribution to pre-defined distribution list.  Two distribution lists are to be supported; one list for Internal recipients and a second list for External recipients.  Recipients will receive the respective format version (i.e. Internal or External) for the date range specified.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_NATACCT_NON_TRND_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - White Cap National Accounts Report (NonTrend) Internal
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'ACCOUNT_NAME','Account Name','Account Name','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'ACCOUNT_NUMBER','Account Number','Account Number','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'CUSTOMER_PO','Customer Po#','Customer Po','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'INVOICE_NUMBER','Invoice#','Invoice Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'LOC','Loc','Loc','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'PARENT_NAME','Parent Name','Parent Name','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'PART_DESCRIPTION','Part Description','Part Description','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'PART_NUMBER','Part#','Part Number','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'SALES','Sales','Sales','','~T~D~2','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','~T~D~2','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'COST','Cost','Cost','','~T~D~2','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'JOB_NAME','Customer Job#','Job Name','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'JOB_NUMBER','Site Number','Job Number','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'SALESREP_NUMBER','Salesrep Number#','Salesrep Number','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'UNIT_COST','Unit Cost','Unit Cost','','~T~D~2','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'VENDOR_NUMBER','Vendor part#','Vendor Number','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'QTY','Qty','Qty','','~~~','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'GM_PERCENT','Gm%','Qty','NUMBER','~T~D~2','default','','20','Y','','','','','','','CASE WHEN EXONNTV.SALES = 0 THEN NULL ELSE ROUND( (((EXONNTV.SALES - EXONNTV.COST)/EXONNTV.SALES) *100),2) END','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'CUSTOMER_JOB','Job Name','Customer Job','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'INV_CAT_CLASS','Inv Cat Class','Inv Cat Class','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap National Accounts Report (NonTrend) Internal',660,'ORDER_NUM','Order Number','Ordered Date','VARCHAR2','','default','','23','Y','','','','','','','to_char(ORDER_NUMBER)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_NATACCT_NON_TRND_V','','');
--Inserting Report Parameters - White Cap National Accounts Report (NonTrend) Internal
xxeis.eis_rs_ins.rp( 'White Cap National Accounts Report (NonTrend) Internal',660,'Parent Account','Parent Account','PARENT_NAME','IN','OM Parent Account Name LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap National Accounts Report (NonTrend) Internal',660,'Customer Class Code','Customer Class Code','CUSTOMER_CLASS_CODE','IN','','''NATIONAL''','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap National Accounts Report (NonTrend) Internal',660,'Account Name','Account Name','ACCOUNT_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap National Accounts Report (NonTrend) Internal',660,'Account Number','Account Number','ACCOUNT_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap National Accounts Report (NonTrend) Internal',660,'Date From','Date From','INVOICE_DATE_COND','>=','','','DATE','Y','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'White Cap National Accounts Report (NonTrend) Internal',660,'Date To','Date To','INVOICE_DATE_COND','<=','','','DATE','Y','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
--Inserting Report Conditions - White Cap National Accounts Report (NonTrend) Internal
xxeis.eis_rs_ins.rcn( 'White Cap National Accounts Report (NonTrend) Internal',660,'PARENT_NAME','IN',':Parent Account','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap National Accounts Report (NonTrend) Internal',660,'ACCOUNT_NAME','IN',':Account Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap National Accounts Report (NonTrend) Internal',660,'ACCOUNT_NUMBER','IN',':Account Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap National Accounts Report (NonTrend) Internal',660,'CUSTOMER_CLASS_CODE','IN',':Customer Class Code','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap National Accounts Report (NonTrend) Internal',660,'invoice_date_cond','>=',':Date From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap National Accounts Report (NonTrend) Internal',660,'invoice_date_cond','<=',':Date To','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - White Cap National Accounts Report (NonTrend) Internal
xxeis.eis_rs_ins.rs( 'White Cap National Accounts Report (NonTrend) Internal',660,'PARENT_NAME','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'White Cap National Accounts Report (NonTrend) Internal',660,'ACCOUNT_NUMBER','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'White Cap National Accounts Report (NonTrend) Internal',660,'INVOICE_NUMBER','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'White Cap National Accounts Report (NonTrend) Internal',660,'PART_NUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - White Cap National Accounts Report (NonTrend) Internal
--Inserting Report Templates - White Cap National Accounts Report (NonTrend) Internal
--Inserting Report Portals - White Cap National Accounts Report (NonTrend) Internal
--Inserting Report Dashboards - White Cap National Accounts Report (NonTrend) Internal
--Inserting Report Security - White Cap National Accounts Report (NonTrend) Internal
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50926',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50927',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50928',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50929',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50931',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50930',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','','LC053655','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','','10010432','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','','RB054040','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','','RV003897','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','','SS084202','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap National Accounts Report (NonTrend) Internal','','SO004816','',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - White Cap National Accounts Report (NonTrend) Internal
xxeis.eis_rs_ins.rpivot( 'White Cap National Accounts Report (NonTrend) Internal',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap National Accounts Report (NonTrend) Internal',660,'Pivot','ACCOUNT_NAME','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap National Accounts Report (NonTrend) Internal',660,'Pivot','ACCOUNT_NUMBER','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap National Accounts Report (NonTrend) Internal',660,'Pivot','SALES','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap National Accounts Report (NonTrend) Internal',660,'Pivot','COST','DATA_FIELD','SUM','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap National Accounts Report (NonTrend) Internal',660,'Pivot','QTY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'White Cap National Accounts Report (NonTrend) Internal',660,'Pivot','GM_PERCENT','DATA_FIELD','SUM','','4','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
