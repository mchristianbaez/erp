Patch Name: eis_patch_case_10559_hdsupply
Patch Description: This patch provides fix for Performance problem with EIS report Order and Invoice Summary Details on to 70071.
Case Number(s) with description(s): 10559 - I noticed we have an EIS report that sometimes takes a very long time to run, even when using reasonable parameters. This would be a candidate to open a case with EIS to have them fix how they use trunc() with date parameters.
Here�s the info. They are using trunc(INVOICE_CREATION_DATE) >= [some date] which bypasses any index on the invoice_creation_date (unless there is an index on the function itself, not likely). They should use some other method so that they don�t truncate the date before comparing it to the parameter.
This came up based on an OEM alert this afternoon and the bad practice of bypassing indexes by truncating dates is something Rick Minutella showed us. 
Case Created By: Pedro Pagan
Patch Created By: Chandra Yerra
Date: 04-Oct-2013
Build #: 2

Patch Instructions:
-------------------
1. Unzip eis_patch_case_10559_hdsupply.zip file.
2. Run order_and_invoice_summary_details_report.sql file from XXEIS schema in SQL Plus session.
3. Run eis_oneoff_ddl.sql file from XXEIS schema in SQL Plus session.
