--Report Name            : Order and Invoice Summary Details Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_OM_INV_ORDER_DETAIL_V
set scan off define off
prompt Creating View XXEIS.EIS_OM_INV_ORDER_DETAIL_V
Create or replace View XXEIS.EIS_OM_INV_ORDER_DETAIL_V
 AS 
SELECT oh.order_number order_number,
    ol.line_number
    ||'.'
    || ol.shipment_number line_number,
    trunc(oh.ordered_date) Ordered_date,
    rctl.line_number trx_line_number,
    party.party_name customer_name,
    ol.unit_selling_price* ol.ordered_quantity extended_price,
    rct.trx_number credit_invoice_number,
    rctt.NAME transaction_name,
    rctt.description transaction_description,
    rctl.sales_order sales_order,
    oh.open_flag open_flag,
    rctl.reason_code reject_reason_code,
    rctl.quantity_ordered quantity_ordered,
    rctl.quantity_credited quantity_credited,
    rctl.quantity_invoiced receivable_inv_quantity,
    rctl.line_type receivable_line_type,
    Rctl.Interface_Line_Context Interface_Line_Context,
    Rctl.Extended_Amount Invoice_Line_Extended_Amount,
    Rctl.Revenue_Amount Invoice_Line_Revenue_Amount,
    Rct_Tax.Extended_Amount Invoice_Line_Tax_Ext_Amnt,/*added by hari to include tax amount corresponding to each line*/
    Rct_Tax.Revenue_Amount Invoice_Line_Tax_Rev_Amnt,
    Ocl.Charge_Name Order_Line_Charge,/*freight line type present in invoice defaults from charges of the order*/
    ocl.charge_amount order_line_charge_cost,
   -- rctl.tax_rate tax_rate,
   -- rctl.vat_tax_id vat_tax_id,
   -- Rctl.Taxable_Amount Taxable_Amount,
   -- rctl.tax_line_id tax_line_id,
    trunc(rct.creation_date) invoice_creation_date,
    ol.ordered_item ordered_item,
    ol.order_quantity_uom order_quantity_uom,
    ol.cancelled_quantity cancelled_quantity,
    ol.shipped_quantity shipped_quantity,
    ol.ordered_quantity ordered_quantity,
    ol.fulfilled_quantity fulfilled_quantity,
    ol.shipping_quantity shipping_quantity,
    ol.shipping_quantity_uom shipping_quantity_uom,
    ol.sold_to_org_id sold_to_org_id,
    ol.ship_from_org_id ship_from_org_id,
    ol.ship_to_org_id ship_to_org_id,
    ol.deliver_to_org_id deliver_to_org_id,
    ol.source_type_code source_type_code,
    ol.price_list_id price_list_id,
    ol.shipment_number shipment_number,
    ol.agreement_id agreement_id,
    ol.shipment_priority_code shipment_priority_code,
    ol.shipping_method_code shipping_method_code,
    ol.freight_terms_code freight_terms_code,
    ol.freight_carrier_code freight_carrier_code,
    ol.fob_point_code fob_point_code,
    ol.tax_point_code tax_point_code,
    ol.payment_term_id payment_term_id,
    ol.invoicing_rule_id invoicing_rule_id,
    ol.accounting_rule_id accounting_rule_id,
    ol.accounting_rule_duration accounting_rule_duration,
    ol.source_document_type_id source_document_type_id,
    ol.orig_sys_document_ref orig_sys_document_ref,
    ol.source_document_id source_document_id,
    ol.orig_sys_line_ref orig_sys_line_re,
    ol.source_document_line_id source_document_line_id,
    ol.reference_line_id reference_line_id,
    ol.reference_type reference_type,
    ol.reference_header_id reference_header_id,
    ol.item_revision item_revision,
    ol.line_category_code line_category_code,
    ol.customer_trx_line_id customer_trx_line_id,
    ol.reference_customer_trx_line_id reference_customer_trx_line_id,
    ol.unit_selling_price unit_selling_price,
    Ol.Unit_List_Price Unit_List_Price,
    ol.tax_value order_tax_value,
    ol.model_group_number model_group_number,
    ol.option_flag option_flag,
    ol.dep_plan_required_flag dep_plan_required_flag,
    ol.visible_demand_flag visible_demand_flag,
    ol.actual_arrival_date actual_arrival_date,
    ol.actual_shipment_date actual_shipment_date,
    ol.earliest_acceptable_date earliest_acceptable_date,
    ol.latest_acceptable_date latest_acceptable_date,
    ott.NAME line_type,
    qlhvl.NAME price_list,
    qlh.rounding_factor rounding_factor,
    accrule.NAME accounting_rule,
    invrule.NAME invoicing_rule,
    term.NAME terms,
    party.party_name sold_to,
    cust_acct.account_number customer_number,
    ship_from_org.organization_code ship_from,
    ol.subinventory subinventory,
    ship_su.LOCATION ship_to,
    ship_su.LOCATION ship_to_location,
    ship_loc.address1 ship_to_address1,
    ship_loc.address2 ship_to_address2,
    ship_loc.address3 ship_to_address3,
    ship_loc.address4 ship_to_address4,
    DECODE (ship_loc.city, NULL, NULL, ship_loc.city
    || ', ' )
    || DECODE (ship_loc.state, NULL, ship_loc.province
    || ', ', ship_loc.state
    || ', ' )
    || DECODE (ship_loc.postal_code, NULL, NULL, ship_loc.postal_code
    || ', ' )
    || DECODE (ship_loc.country, NULL, NULL, ship_loc.country) ship_to_address5,
    bill_su.LOCATION Bill_to,
    bill_su.LOCATION Bill_to_location,
    bill_loc.address1 Bill_to_address1,
    bill_loc.address2 Bill_to_address2,
    bill_loc.address3 Bill_to_address3,
    bill_loc.address4 Bill_to_address4,
    DECODE (bill_loc.city, NULL, NULL, bill_loc.city
    || ', ' )
    || DECODE (bill_loc.state, NULL, bill_loc.province
    || ', ', bill_loc.state
    || ', ' )
    || DECODE (bill_loc.postal_code, NULL, NULL, bill_loc.postal_code
    || ', ' )
    || DECODE (bill_loc.country, NULL, NULL, bill_loc.country) Bill_to_address5,
    ship_party.person_last_name
    || DECODE (ship_party.person_first_name, NULL, NULL, ', '
    || ship_party.person_first_name )
    || DECODE (ship_arl.meaning, NULL, NULL, ' '
    || ship_arl.meaning) ship_to_contact,
    invoice_party.person_last_name
    || DECODE (invoice_party.person_first_name, NULL, NULL, ', '
    || invoice_party.person_first_name )
    || DECODE (invoice_arl.meaning, NULL, NULL, ' '
    || invoice_arl.meaning ) Bill_to_contact,
    Ol.Invoiced_Quantity Invoiced_Quantity,
    Ol.Flow_Status_Code Order_Line_Status,
    oh.flow_status_code order_header_status,
    ol.fulfillment_date fulfillment_date,
    ol.preferred_grade preferred_grade,
    ol.ordered_quantity2 ordered_quantity2,
    ol.ordered_quantity_uom2 ordered_quantity_uom2,
    ol.shipped_quantity2 shipped_quantity2,
    ol.cancelled_quantity2 cancelled_quantity2,
    ol.shipping_quantity2 shipping_quantity2,
    ol.shipping_quantity_uom2 shipping_quantity_uom2,
    ol.fulfilled_quantity2 fulfilled_quantity2,
    ol.upgraded_flag upgraded_flag,
    ol.unit_selling_price_per_pqty unit_selling_price_per_pqty,
    oh.return_reason_code return_reason_code,
    ol.inventory_item_id inventory_item_id,
    hou.name operating_unit,
    --Primary Keys
    ship_from_org.organization_id shipping_org_id,
    ship_su.site_use_id ship_site_use_id,
    ship_ps.party_site_id ship_ps_party_site_id,
    ship_loc.location_id ship_loc_location_id,
    ship_cas.cust_acct_site_id ship_cas_cust_acct_site_id,
    bill_loc.location_id bill_loc_location_id,
    bill_ps.party_site_id bill_ps_party_site_id,
    bill_cas.cust_acct_site_id bill_cas_cust_acct_site_id,
    ship_party.party_id ship_party_party_id,
    cust_acct.cust_account_id cust_acct_cust_account_id,
    ship_roles.CUST_ACCOUNT_ROLE_ID sroles_CUST_ACCOUNT_ROLE_ID,
    ship_rel.RELATIONSHIP_ID ship_rel_RELATIONSHIP_ID,
    ship_acct.cust_account_id ship_acct_cust_account_id,
    invoice_roles.CUST_ACCOUNT_ROLE_ID iroles_cust_account_role_id,
    invoice_party.party_id invoice_party_party_id,
    invoice_rel.RELATIONSHIP_ID invoice_rel_RELATIONSHIP_ID,
    invoice_acct.cust_account_id invoice_acct_cust_account_id,
    oh.header_id order_header_header_id,
    ol.line_id order_line_line_id,
    qlhvl.LIST_HEADER_ID price_list_LIST_HEADER_ID,
    accrule.rule_id accrule_rule_id,
    invrule.rule_id invrule_rule_id,
    term.term_id term_term_id,
    ott.TRANSACTION_TYPE_ID trx_type_TRANSACTION_TYPE_ID,
    rct.cust_trx_type_id cust_trx_type_id,
    hou.organization_id operating_unit_id,
    -- unique keys added for component joins
    PARTY.PARTY_ID                       PARTY_ID,
    BILL_SU.SITE_USE_ID                  BILL_SU_SITE_USE_ID,
    SHIP_REL.DIRECTIONAL_FLAG            SHIP_REL_DIRECTIONAL_FLAG,
    invoice_rel.directional_flag         invoice_rel_dirctnal_flag,
    qlh.list_header_id                   qlh_list_header_id,
    rctl.customer_trx_line_id            rctl_customer_trx_line_id,
    rct_tax.customer_trx_line_id         rct_tx_cstmr_trx_line_id,
    rct.customer_trx_id                  rct_customer_trx_id,
    RCTT.CUST_TRX_TYPE_ID                RCTT_CUST_TRX_TYPE_ID,
    RCTT.ORG_ID                          RCTT_ORG_ID,
   OCL.CHARGE_ID                        OCL_CHARGE_ID
   --  Rct_Tax.LINE_TYPE
       --descr#flexfield#start
       --descr#flexfield#end
       --gl#accountff#start
       --gl#accountff#end
  FROM mtl_parameters ship_from_org,
    hz_cust_site_uses ship_su,
    hz_party_sites ship_ps,
    hz_locations ship_loc,
    hz_cust_acct_sites ship_cas,
    hz_cust_site_uses bill_su,
    hz_party_sites bill_ps,
    hz_locations bill_loc,
    hz_cust_acct_sites bill_cas,
    hz_parties party,
    hz_cust_accounts cust_acct,
    hz_cust_account_roles ship_roles,
    hz_parties ship_party,
    hz_relationships ship_rel,
    hz_cust_accounts ship_acct,
    ar_lookups ship_arl,
    hz_cust_account_roles invoice_roles,
    hz_parties invoice_party,
    hz_relationships invoice_rel,
    hz_cust_accounts invoice_acct,
    ar_lookups invoice_arl,
    oe_order_headers oh,
    oe_order_lines ol,
    qp_list_headers_vl qlhvl,
    qp_list_headers_b qlh ,
    ra_rules accrule,
    ra_rules invrule,
    ra_terms_vl term,
    oe_transaction_types_vl ott,
    Ra_Customer_Trx_Lines Rctl,
    ra_customer_trx_lines rct_tax/*added by hari TO INCLUDE TAX AMOUNT*/,
    ra_customer_trx rct,
    ra_cust_trx_types rctt,
    Hr_Operating_Units Hou,
    oe_charge_lines_v ocl /*added by hari to include freight cost which defaults from om into receivables*/
  WHERE ol.line_type_id               = ott.transaction_type_id
  AND ol.price_list_id                = qlhvl.list_header_id(+)
  AND qlhvl.list_header_id            = qlh.list_header_id(+)
  AND ol.accounting_rule_id           = accrule.rule_id(+)
  AND ol.invoicing_rule_id            = invrule.rule_id(+)
  AND ol.payment_term_id              = term.term_id(+)
  AND ol.sold_to_org_id               = cust_acct.cust_account_id(+)
  AND cust_acct.party_id              = party.party_id(+)
  AND ol.ship_from_org_id             = ship_from_org.organization_id(+)
  AND ol.ship_to_org_id               = ship_su.site_use_id(+)
  AND ship_su.cust_acct_site_id       = ship_cas.cust_acct_site_id(+)
  AND ship_cas.party_site_id          = ship_ps.party_site_id(+)
  AND ship_loc.location_id(+)         = ship_ps.location_id
  AND ol.invoice_to_org_id            = bill_su.site_use_id(+)
  AND bill_su.cust_acct_site_id       = bill_cas.cust_acct_site_id(+)
  AND bill_cas.party_site_id          = bill_ps.party_site_id(+)
  AND bill_loc.location_id(+)         = bill_ps.location_id
  AND ol.ship_to_contact_id           = ship_roles.cust_account_role_id(+)
  AND ship_roles.party_id             = ship_rel.party_id(+)
  AND ship_roles.role_type(+)         = 'CONTACT'
  AND ship_rel.subject_id             = ship_party.party_id(+)
  AND ship_roles.cust_account_id      = ship_acct.cust_account_id(+)
  AND NVL (ship_rel.object_id, -1)    = NVL (ship_acct.party_id, -1)
  AND ship_arl.lookup_type(+)         = 'CONTACT_TITLE'
  AND ship_arl.lookup_code(+)         = ship_party.person_pre_name_adjunct
  AND ol.invoice_to_contact_id        = invoice_roles.cust_account_role_id(+)
  AND invoice_roles.party_id          = invoice_rel.party_id(+)
  AND invoice_roles.role_type(+)      = 'CONTACT'
  AND invoice_rel.subject_id          = invoice_party.party_id(+)
  AND invoice_roles.cust_account_id   = invoice_acct.cust_account_id(+)
  AND NVL (invoice_rel.object_id, -1) = NVL (invoice_acct.party_id, -1)
  AND invoice_arl.lookup_type(+)      = 'CONTACT_TITLE'
  AND invoice_arl.lookup_code(+)      = invoice_party.person_pre_name_adjunct
  AND ol.header_id                    = oh.header_id
  AND OL.BOOKED_FLAG                  = 'Y'
  AND ol.flow_status_code            ='CLOSED'
  AND OL.LINE_TYPE_ID                 = OTT.TRANSACTION_TYPE_ID
 AND rctl.interface_line_attribute6    =   TO_CHAR(ol.line_id)
 AND RCTl.INTERFACE_line_ATTRIBUTE1   =   oh.order_number
   AND rctl.interface_line_context     =   'ORDER ENTRY'
    -- AND ol.line_category_code IN ('RETURN')
    --  AND ol.invoice_interface_status_code  =   'YES'
    --AND rctl.sales_order_line IS NOT NULL
 -- And To_Char(Oh.Order_Number) = To_Char(Rct.Interface_Header_Attribute1)
 -- And Ol.Line_Number=Rctl.Sales_Order_Line/*added by hari to remove cartesian join*/
 AND RCT_TAX.LINK_TO_CUST_TRX_LINE_ID(+)=RCTL.CUSTOMER_TRX_LINE_ID/*added by hari to include tax amounts for corresponding line*/
-- And Rct_Tax.Customer_Trx_Id=Rct.Customer_Trx_Id
 And Rct_Tax.Line_Type(+)='TAX'
 and ocl.line_id(+)=ol.line_id
  AND rct.customer_trx_id      = rctl.customer_trx_id
  AND rct.cust_trx_type_id     = rctt.cust_trx_type_id
  And Rct.Org_Id               = Rctt.Org_Id
  And Oh.Org_Id                = Hou.Organization_Id
-- And Oh.Order_Number='13148'
-- and ol.line_number='4';;/
set scan on define on
prompt Creating View Data for Order and Invoice Summary Details Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_OM_INV_ORDER_DETAIL_V
xxeis.eis_rs_ins.v( 'EIS_OM_INV_ORDER_DETAIL_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Om Inv Order Detail V','EOIODV','','');
--Delete View Columns for EIS_OM_INV_ORDER_DETAIL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_OM_INV_ORDER_DETAIL_V',660,FALSE);
--Inserting View Columns for EIS_OM_INV_ORDER_DETAIL_V
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','TRANSACTION_DESCRIPTION',660,'Transaction Description','TRANSACTION_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Transaction Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','TRANSACTION_NAME',660,'Transaction Name','TRANSACTION_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Transaction Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','CREDIT_INVOICE_NUMBER',660,'Credit Invoice Number','CREDIT_INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','FULFILLMENT_DATE',660,'Fulfillment Date','FULFILLMENT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Fulfillment Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICED_QUANTITY',660,'Invoiced Quantity','INVOICED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoiced Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_TO_CONTACT',660,'Bill To Contact','BILL_TO_CONTACT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Contact','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS5',660,'Bill To Address5','BILL_TO_ADDRESS5','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Address5','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS4',660,'Bill To Address4','BILL_TO_ADDRESS4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Address4','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS3',660,'Bill To Address3','BILL_TO_ADDRESS3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Address3','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS2',660,'Bill To Address2','BILL_TO_ADDRESS2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Address2','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_TO_ADDRESS1',660,'Bill To Address1','BILL_TO_ADDRESS1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Address1','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_TO_LOCATION',660,'Bill To Location','BILL_TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_TO',660,'Bill To','BILL_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS5',660,'Ship To Address5','SHIP_TO_ADDRESS5','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Address5','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS4',660,'Ship To Address4','SHIP_TO_ADDRESS4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Address4','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS3',660,'Ship To Address3','SHIP_TO_ADDRESS3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Address3','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS2',660,'Ship To Address2','SHIP_TO_ADDRESS2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Address2','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','FOB_POINT_CODE',660,'Fob Point Code','FOB_POINT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fob Point Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','FREIGHT_CARRIER_CODE',660,'Freight Carrier Code','FREIGHT_CARRIER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Freight Carrier Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPMENT_NUMBER',660,'Shipment Number','SHIPMENT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipment Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SOURCE_TYPE_CODE',660,'Source Type Code','SOURCE_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source Type Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPPING_QUANTITY',660,'Shipping Quantity','SHIPPING_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipping Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','FULFILLED_QUANTITY',660,'Fulfilled Quantity','FULFILLED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Fulfilled Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDERED_QUANTITY',660,'Ordered Quantity','ORDERED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Ordered Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPPED_QUANTITY',660,'Shipped Quantity','SHIPPED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipped Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','CANCELLED_QUANTITY',660,'Cancelled Quantity','CANCELLED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Cancelled Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_QUANTITY_UOM',660,'Order Quantity Uom','ORDER_QUANTITY_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Quantity Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDERED_ITEM',660,'Ordered Item','ORDERED_ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ordered Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','RECEIVABLE_INV_QUANTITY',660,'Receivable Inv Quantity','RECEIVABLE_INV_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Receivable Inv Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','QUANTITY_CREDITED',660,'Quantity Credited','QUANTITY_CREDITED','','','','XXEIS_RS_ADMIN','NUMBER','','','Quantity Credited','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','QUANTITY_ORDERED',660,'Quantity Ordered','QUANTITY_ORDERED','','','','XXEIS_RS_ADMIN','NUMBER','','','Quantity Ordered','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','REJECT_REASON_CODE',660,'Reject Reason Code','REJECT_REASON_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reject Reason Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','OPEN_FLAG',660,'Open Flag','OPEN_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Open Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_TO_ADDRESS1',660,'Ship To Address1','SHIP_TO_ADDRESS1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Address1','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_TO',660,'Ship To','SHIP_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SUBINVENTORY',660,'Subinventory','SUBINVENTORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Subinventory','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_FROM',660,'Ship From','SHIP_FROM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship From','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SOLD_TO',660,'Sold To','SOLD_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sold To','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICING_RULE',660,'Invoicing Rule','INVOICING_RULE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoicing Rule','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ROUNDING_FACTOR',660,'Rounding Factor','ROUNDING_FACTOR','','','','XXEIS_RS_ADMIN','NUMBER','','','Rounding Factor','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','PRICE_LIST',660,'Price List','PRICE_LIST','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Price List','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','LINE_TYPE',660,'Line Type','LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ACTUAL_SHIPMENT_DATE',660,'Actual Shipment Date','ACTUAL_SHIPMENT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Actual Shipment Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ACTUAL_ARRIVAL_DATE',660,'Actual Arrival Date','ACTUAL_ARRIVAL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Actual Arrival Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','VISIBLE_DEMAND_FLAG',660,'Visible Demand Flag','VISIBLE_DEMAND_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Visible Demand Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','UNIT_LIST_PRICE',660,'Unit List Price','UNIT_LIST_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit List Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','LINE_CATEGORY_CODE',660,'Line Category Code','LINE_CATEGORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Category Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','REFERENCE_TYPE',660,'Reference Type','REFERENCE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reference Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','OPERATING_UNIT',660,'Operating Unit','OPERATING_UNIT~OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','RETURN_REASON_CODE',660,'Return Reason Code','RETURN_REASON_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Return Reason Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER~LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','TRX_LINE_NUMBER',660,'Trx Line Number','TRX_LINE_NUMBER~TRX_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Trx Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME~CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE~ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_CREATION_DATE',660,'Invoice Creation Date','INVOICE_CREATION_DATE~INVOICE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_LINE_EXTENDED_AMOUNT',660,'Invoice Line Extended Amount','INVOICE_LINE_EXTENDED_AMOUNT~INVOICE_LINE_EXTENDED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Line Extended Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_LINE_REVENUE_AMOUNT',660,'Invoice Line Revenue Amount','INVOICE_LINE_REVENUE_AMOUNT~INVOICE_LINE_REVENUE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Line Revenue Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_LINE_TAX_EXT_AMNT',660,'Invoice Line Tax Ext Amnt','INVOICE_LINE_TAX_EXT_AMNT~INVOICE_LINE_TAX_EXT_AMNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Line Tax Ext Amnt','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_LINE_TAX_REV_AMNT',660,'Invoice Line Tax Rev Amnt','INVOICE_LINE_TAX_REV_AMNT~INVOICE_LINE_TAX_REV_AMNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Line Tax Rev Amnt','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_LINE_CHARGE',660,'Order Line Charge','ORDER_LINE_CHARGE~ORDER_LINE_CHARGE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Line Charge','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_LINE_CHARGE_COST',660,'Order Line Charge Cost','ORDER_LINE_CHARGE_COST~ORDER_LINE_CHARGE_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Line Charge Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_LINE_STATUS',660,'Order Line Status','ORDER_LINE_STATUS~ORDER_LINE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Line Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_TAX_VALUE',660,'Order Tax Value','ORDER_TAX_VALUE~ORDER_TAX_VALUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Tax Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','EXTENDED_PRICE',660,'Extended Price','EXTENDED_PRICE~EXTENDED_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Extended Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SELLING_PRICE',660,'Selling Price','SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INV_ITEM_NUMBER',660,'Inv Item Number','INV_ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Inv Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SALES_REPRESENTATIVE',660,'Sales Representative','SALES_REPRESENTATIVE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Representative','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','CUST_TRX_TYPE_ID',660,'Cust Trx Type Id','CUST_TRX_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Trx Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','TRX_TYPE_TRANSACTION_TYPE_ID',660,'Trx Type Transaction Type Id','TRX_TYPE_TRANSACTION_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Trx Type Transaction Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','TERM_TERM_ID',660,'Term Term Id','TERM_TERM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Term Term Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVRULE_RULE_ID',660,'Invrule Rule Id','INVRULE_RULE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invrule Rule Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ACCRULE_RULE_ID',660,'Accrule Rule Id','ACCRULE_RULE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Accrule Rule Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','PRICE_LIST_LIST_HEADER_ID',660,'Price List List Header Id','PRICE_LIST_LIST_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Price List List Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_LINE_LINE_ID',660,'Order Line Line Id','ORDER_LINE_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Line Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_HEADER_HEADER_ID',660,'Order Header Header Id','ORDER_HEADER_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Header Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_ACCT_CUST_ACCOUNT_ID',660,'Invoice Acct Cust Account Id','INVOICE_ACCT_CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Acct Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_REL_RELATIONSHIP_ID',660,'Invoice Rel Relationship Id','INVOICE_REL_RELATIONSHIP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Rel Relationship Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_PARTY_PARTY_ID',660,'Invoice Party Party Id','INVOICE_PARTY_PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Party Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','IROLES_CUST_ACCOUNT_ROLE_ID',660,'Iroles Cust Account Role Id','IROLES_CUST_ACCOUNT_ROLE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Iroles Cust Account Role Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_ACCT_CUST_ACCOUNT_ID',660,'Ship Acct Cust Account Id','SHIP_ACCT_CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship Acct Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_REL_RELATIONSHIP_ID',660,'Ship Rel Relationship Id','SHIP_REL_RELATIONSHIP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship Rel Relationship Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SROLES_CUST_ACCOUNT_ROLE_ID',660,'Sroles Cust Account Role Id','SROLES_CUST_ACCOUNT_ROLE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Sroles Cust Account Role Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','CUST_ACCT_CUST_ACCOUNT_ID',660,'Cust Acct Cust Account Id','CUST_ACCT_CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Acct Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_PARTY_PARTY_ID',660,'Ship Party Party Id','SHIP_PARTY_PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship Party Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_CAS_CUST_ACCT_SITE_ID',660,'Bill Cas Cust Acct Site Id','BILL_CAS_CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Bill Cas Cust Acct Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_PS_PARTY_SITE_ID',660,'Bill Ps Party Site Id','BILL_PS_PARTY_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Bill Ps Party Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_LOC_LOCATION_ID',660,'Bill Loc Location Id','BILL_LOC_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Bill Loc Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_CAS_CUST_ACCT_SITE_ID',660,'Ship Cas Cust Acct Site Id','SHIP_CAS_CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship Cas Cust Acct Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_LOC_LOCATION_ID',660,'Ship Loc Location Id','SHIP_LOC_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship Loc Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_PS_PARTY_SITE_ID',660,'Ship Ps Party Site Id','SHIP_PS_PARTY_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship Ps Party Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_SITE_USE_ID',660,'Ship Site Use Id','SHIP_SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SOURCE_DOCUMENT_ID',660,'Source Document Id','SOURCE_DOCUMENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Source Document Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORIG_SYS_DOCUMENT_REF',660,'Orig Sys Document Ref','ORIG_SYS_DOCUMENT_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Orig Sys Document Ref','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SOURCE_DOCUMENT_TYPE_ID',660,'Source Document Type Id','SOURCE_DOCUMENT_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Source Document Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ACCOUNTING_RULE_DURATION',660,'Accounting Rule Duration','ACCOUNTING_RULE_DURATION','','','','XXEIS_RS_ADMIN','NUMBER','','','Accounting Rule Duration','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ACCOUNTING_RULE_ID',660,'Accounting Rule Id','ACCOUNTING_RULE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Accounting Rule Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICING_RULE_ID',660,'Invoicing Rule Id','INVOICING_RULE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoicing Rule Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','PAYMENT_TERM_ID',660,'Payment Term Id','PAYMENT_TERM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Term Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','TAX_POINT_CODE',660,'Tax Point Code','TAX_POINT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Point Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','FREIGHT_TERMS_CODE',660,'Freight Terms Code','FREIGHT_TERMS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Freight Terms Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPPING_METHOD_CODE',660,'Shipping Method Code','SHIPPING_METHOD_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Shipping Method Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPMENT_PRIORITY_CODE',660,'Shipment Priority Code','SHIPMENT_PRIORITY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Shipment Priority Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','AGREEMENT_ID',660,'Agreement Id','AGREEMENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Agreement Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','PRICE_LIST_ID',660,'Price List Id','PRICE_LIST_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Price List Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','DELIVER_TO_ORG_ID',660,'Deliver To Org Id','DELIVER_TO_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Deliver To Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_TO_ORG_ID',660,'Ship To Org Id','SHIP_TO_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship To Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_FROM_ORG_ID',660,'Ship From Org Id','SHIP_FROM_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship From Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SOLD_TO_ORG_ID',660,'Sold To Org Id','SOLD_TO_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Sold To Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPPING_QUANTITY_UOM',660,'Shipping Quantity Uom','SHIPPING_QUANTITY_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Shipping Quantity Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INTERFACE_LINE_CONTEXT',660,'Interface Line Context','INTERFACE_LINE_CONTEXT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Interface Line Context','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','RECEIVABLE_LINE_TYPE',660,'Receivable Line Type','RECEIVABLE_LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Receivable Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SALES_ORDER',660,'Sales Order','SALES_ORDER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORIG_SYS_LINE_RE',660,'Orig Sys Line Re','ORIG_SYS_LINE_RE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Orig Sys Line Re','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPPING_QUANTITY2',660,'Shipping Quantity2','SHIPPING_QUANTITY2','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipping Quantity2','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','CANCELLED_QUANTITY2',660,'Cancelled Quantity2','CANCELLED_QUANTITY2','','','','XXEIS_RS_ADMIN','NUMBER','','','Cancelled Quantity2','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPPED_QUANTITY2',660,'Shipped Quantity2','SHIPPED_QUANTITY2','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipped Quantity2','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDERED_QUANTITY_UOM2',660,'Ordered Quantity Uom2','ORDERED_QUANTITY_UOM2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ordered Quantity Uom2','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDERED_QUANTITY2',660,'Ordered Quantity2','ORDERED_QUANTITY2','','','','XXEIS_RS_ADMIN','NUMBER','','','Ordered Quantity2','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','PREFERRED_GRADE',660,'Preferred Grade','PREFERRED_GRADE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Preferred Grade','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_TO_CONTACT',660,'Ship To Contact','SHIP_TO_CONTACT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Contact','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_TO_LOCATION',660,'Ship To Location','SHIP_TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','TERMS',660,'Terms','TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ACCOUNTING_RULE',660,'Accounting Rule','ACCOUNTING_RULE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Accounting Rule','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','LATEST_ACCEPTABLE_DATE',660,'Latest Acceptable Date','LATEST_ACCEPTABLE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Latest Acceptable Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','EARLIEST_ACCEPTABLE_DATE',660,'Earliest Acceptable Date','EARLIEST_ACCEPTABLE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Earliest Acceptable Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','DEP_PLAN_REQUIRED_FLAG',660,'Dep Plan Required Flag','DEP_PLAN_REQUIRED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Dep Plan Required Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','OPTION_FLAG',660,'Option Flag','OPTION_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Option Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','MODEL_GROUP_NUMBER',660,'Model Group Number','MODEL_GROUP_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Model Group Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','REFERENCE_CUSTOMER_TRX_LINE_ID',660,'Reference Customer Trx Line Id','REFERENCE_CUSTOMER_TRX_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Reference Customer Trx Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','CUSTOMER_TRX_LINE_ID',660,'Customer Trx Line Id','CUSTOMER_TRX_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ITEM_REVISION',660,'Item Revision','ITEM_REVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Revision','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','REFERENCE_HEADER_ID',660,'Reference Header Id','REFERENCE_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Reference Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','REFERENCE_LINE_ID',660,'Reference Line Id','REFERENCE_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Reference Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SOURCE_DOCUMENT_LINE_ID',660,'Source Document Line Id','SOURCE_DOCUMENT_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Source Document Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPPING_ORG_ID',660,'Shipping Org Id','SHIPPING_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipping Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','UNIT_SELLING_PRICE_PER_PQTY',660,'Unit Selling Price Per Pqty','UNIT_SELLING_PRICE_PER_PQTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Selling Price Per Pqty','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','UPGRADED_FLAG',660,'Upgraded Flag','UPGRADED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Upgraded Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','FULFILLED_QUANTITY2',660,'Fulfilled Quantity2','FULFILLED_QUANTITY2','','','','XXEIS_RS_ADMIN','NUMBER','','','Fulfilled Quantity2','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIPPING_QUANTITY_UOM2',660,'Shipping Quantity Uom2','SHIPPING_QUANTITY_UOM2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Shipping Quantity Uom2','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','OPERATING_UNIT_ID',660,'Operating Unit Id','OPERATING_UNIT_ID~OPERATING_UNIT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Operating Unit Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS~ORDER_HEADER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Header Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','BILL_SU_SITE_USE_ID',660,'Bill Su Site Use Id','BILL_SU_SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Bill Su Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','INVOICE_REL_DIRCTNAL_FLAG',660,'Invoice Rel Dirctnal Flag','INVOICE_REL_DIRCTNAL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Rel Dirctnal Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','OCL_CHARGE_ID',660,'Ocl Charge Id','OCL_CHARGE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ocl Charge Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','QLH_LIST_HEADER_ID',660,'Qlh List Header Id','QLH_LIST_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Qlh List Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','RCTL_CUSTOMER_TRX_LINE_ID',660,'Rctl Customer Trx Line Id','RCTL_CUSTOMER_TRX_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Rctl Customer Trx Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','RCTT_CUST_TRX_TYPE_ID',660,'Rctt Cust Trx Type Id','RCTT_CUST_TRX_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Rctt Cust Trx Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','RCTT_ORG_ID',660,'Rctt Org Id','RCTT_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Rctt Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','RCT_CUSTOMER_TRX_ID',660,'Rct Customer Trx Id','RCT_CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Rct Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','RCT_TX_CSTMR_TRX_LINE_ID',660,'Rct Tx Cstmr Trx Line Id','RCT_TX_CSTMR_TRX_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Rct Tx Cstmr Trx Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_OM_INV_ORDER_DETAIL_V','SHIP_REL_DIRECTIONAL_FLAG',660,'Ship Rel Directional Flag','SHIP_REL_DIRECTIONAL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Rel Directional Flag','','','');
--Inserting View Components for EIS_OM_INV_ORDER_DETAIL_V
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','SHIP_FROM_ORG','SHIP_FROM_ORG','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_SITE_USES',660,'HZ_CUST_SITE_USES_ALL','SHIP_SU','SHIP_SU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Site Uses Or Business Purposes','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTY_SITES',660,'HZ_PARTY_SITES','SHIP_PS','SHIP_PS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Links party to shipping physical locations','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_LOCATIONS',660,'HZ_LOCATIONS','SHIP_LOC','SHIP_LOC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Physical Addresses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','SHIP_CAS','SHIP_CAS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores all customer shipping account sites across all operating units','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_SITE_USES',660,'HZ_CUST_SITE_USES_ALL','BILL_SU','BILL_SU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Site Uses Or Business Purposes','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTY_SITES',660,'HZ_PARTY_SITES','BILL_PS','BILL_PS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Links party to billing physical locations','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_LOCATIONS',660,'HZ_LOCATIONS','BILL_LOC','BILL_LOC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Physical Addresses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','BILL_CAS','BILL_CAS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores all customer billing account sites across all operating units','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTIES',660,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information about parties such as organizations, people, and groups','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','CUST_ACCT','CUST_ACCT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Customer Account Information','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNT_ROLES',660,'HZ_CUST_ACCOUNT_ROLES','SHIP_ROLES','SHIP_ROLES','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Roles That Parties Perform In Customer Accounts','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTIES',660,'HZ_PARTIES','SHIP_PARTY','SHIP_PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_RELATIONSHIPS',660,'HZ_RELATIONSHIPS','SHIP_REL','SHIP_REL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Relationships Between Entities','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','SHIP_ACCT','SHIP_ACCT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Customer Account Information','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNT_ROLES',660,'HZ_CUST_ACCOUNT_ROLES','INVOICE_ROLES','INVOICE_ROLES','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Roles That Parties Perform In Customer Accounts','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTIES',660,'HZ_PARTIES','INVOICE_PARTY','INVOICE_PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_RELATIONSHIPS',660,'HZ_RELATIONSHIPS','INVOICE_REL','INVOICE_REL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Relationships Between Entities','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','INVOICE_ACCT','INVOICE_ACCT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Customer Account Information','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','QP_LIST_HEADERS_VL',660,'QP_LIST_HEADERS_B','QLHVL','QLHVL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Qp List Headers Tl Stores The Translatable Columns','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','QP_LIST_HEADERS_VL',660,'QP_LIST_HEADERS_B','QLH','QLH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Qp List Headers B Stores The Header Information Fo','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','RA_RULES',660,'RA_RULES','ACCRULE','ACCRULE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Invoicing And Accounting Rules','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','RA_RULES',660,'RA_RULES','INVRULE','INVRULE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Invoicing And Accounting Rules','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','RA_TERMS_VL',660,'RA_TERMS_B','TERM','TERM','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Multi-Lingual Support (Mls) For Payment Terms','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','RA_CUST_TRX_TYPES',660,'RA_CUST_TRX_TYPES_ALL','RCTT','RCTT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Transaction Type For Invoices, Commitments And Cre','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','HR_ORGANIZATION_UNITS',660,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','HR_ALL_ORGANIZATION_UNITS','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','QP_LIST_HEADERS_B',660,'QP_LIST_HEADERS_B','QLH','QLH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','QP_LIST_HEADERS_B stores the header information for all lists. List types can be, for example,  Pric','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','RA_CUSTOMER_TRX_LINES',660,'RA_CUSTOMER_TRX_LINES_ALL','RCTL','RCTL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Invoice, debit memo, chargeback, credit memo and commitment lines','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','RA_CUSTOMER_TRX_LINES',660,'RA_CUSTOMER_TRX_LINES_ALL','RCT_TAX','RCT_TAX','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Invoice, Debit Memo, Chargeback, Credit Memo And C','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','RA_CUSTOMER_TRX',660,'RA_CUSTOMER_TRX_ALL','RCT','RCT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Header-level information about invoices, debit memos, chargebacks, commitments and credit memos','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_OM_INV_ORDER_DETAIL_V','OE_CHARGE_LINES_V',660,'OE_PRICE_ADJUSTMENTS','OCL','OCL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','This Table Is Used To Store Price Adjustments That','','','','');
--Inserting View Component Joins for EIS_OM_INV_ORDER_DETAIL_V
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','MTL_PARAMETERS','SHIP_FROM_ORG',660,'EOIODV.SHIPPING_ORG_ID','=','SHIP_FROM_ORG.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_SITE_USES','SHIP_SU',660,'EOIODV.SHIP_SITE_USE_ID','=','SHIP_SU.SITE_USE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_LOCATIONS','SHIP_LOC',660,'EOIODV.SHIP_LOC_LOCATION_ID','=','SHIP_LOC.LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_SITE_USES','BILL_SU',660,'EOIODV.BILL_SU_SITE_USE_ID','=','BILL_SU.SITE_USE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_LOCATIONS','BILL_LOC',660,'EOIODV.BILL_LOC_LOCATION_ID','=','BILL_LOC.LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNTS','CUST_ACCT',660,'EOIODV.CUST_ACCT_CUST_ACCOUNT_ID','=','CUST_ACCT.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNT_ROLES','SHIP_ROLES',660,'EOIODV.SROLES_CUST_ACCOUNT_ROLE_ID','=','SHIP_ROLES.CUST_ACCOUNT_ROLE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTIES','SHIP_PARTY',660,'EOIODV.SHIP_PARTY_PARTY_ID','=','SHIP_PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_RELATIONSHIPS','SHIP_REL',660,'EOIODV.SHIP_REL_RELATIONSHIP_ID','=','SHIP_REL.RELATIONSHIP_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_RELATIONSHIPS','SHIP_REL',660,'EOIODV.SHIP_REL_DIRECTIONAL_FLAG','=','SHIP_REL.DIRECTIONAL_FLAG(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNTS','SHIP_ACCT',660,'EOIODV.SHIP_ACCT_CUST_ACCOUNT_ID','=','SHIP_ACCT.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNT_ROLES','INVOICE_ROLES',660,'EOIODV.IROLES_CUST_ACCOUNT_ROLE_ID','=','INVOICE_ROLES.CUST_ACCOUNT_ROLE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTIES','INVOICE_PARTY',660,'EOIODV.INVOICE_PARTY_PARTY_ID','=','INVOICE_PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_RELATIONSHIPS','INVOICE_REL',660,'EOIODV.INVOICE_REL_RELATIONSHIP_ID','=','INVOICE_REL.RELATIONSHIP_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_RELATIONSHIPS','INVOICE_REL',660,'EOIODV.INVOICE_REL_DIRCTNAL_FLAG','=','INVOICE_REL.DIRECTIONAL_FLAG(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCOUNTS','INVOICE_ACCT',660,'EOIODV.INVOICE_ACCT_CUST_ACCOUNT_ID','=','INVOICE_ACCT.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','OE_ORDER_HEADERS','OH',660,'EOIODV.ORDER_HEADER_HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','OE_ORDER_LINES','OL',660,'EOIODV.ORDER_LINE_LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','QP_LIST_HEADERS_VL','QLHVL',660,'EOIODV.price_list_LIST_HEADER_ID','=','QLHVL.LIST_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','QP_LIST_HEADERS_VL','QLH',660,'EOIODV.qlh_list_header_id','=','QLH.LIST_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','RA_RULES','ACCRULE',660,'EOIODV.ACCRULE_RULE_ID','=','ACCRULE.RULE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','RA_RULES','INVRULE',660,'EOIODV.INVRULE_RULE_ID','=','INVRULE.RULE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','RA_TERMS_VL','TERM',660,'EOIODV.TERM_TERM_ID','=','TERM.TERM_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','RA_CUST_TRX_TYPES','RCTT',660,'EOIODV.RCTT_CUST_TRX_TYPE_ID','=','RCTT.CUST_TRX_TYPE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','RA_CUST_TRX_TYPES','RCTT',660,'EOIODV.RCTT_ORG_ID','=','RCTT.ORG_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HR_ORGANIZATION_UNITS','HOU',660,'EOIODV.OPERATING_UNIT_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','RA_CUSTOMER_TRX_LINES','RCT_TAX',660,'EOIODV.RCT_TX_CSTMR_TRX_LINE_ID','=','RCT_TAX.CUSTOMER_TRX_LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','OE_CHARGE_LINES_V','OCL',660,'EOIODV.OCL_CHARGE_ID','=','OCL.CHARGE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTY_SITES','SHIP_PS',660,'EOIODV.SHIP_PS_PARTY_SITE_ID','=','SHIP_PS.PARTY_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCT_SITES','SHIP_CAS',660,'EOIODV.SHIP_CAS_CUST_ACCT_SITE_ID','=','SHIP_CAS.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTY_SITES','BILL_PS',660,'EOIODV.BILL_PS_PARTY_SITE_ID','=','BILL_PS.PARTY_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_CUST_ACCT_SITES','BILL_CAS',660,'EOIODV.BILL_CAS_CUST_ACCT_SITE_ID','=','BILL_CAS.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','RA_CUSTOMER_TRX','RCT',660,'EOIODV.RCT_CUSTOMER_TRX_ID','=','RCT.CUSTOMER_TRX_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','RA_CUSTOMER_TRX_LINES','RCTL',660,'EOIODV.RCTL_CUSTOMER_TRX_LINE_ID','=','RCTL.CUSTOMER_TRX_LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','QP_LIST_HEADERS_B','QLH',660,'EOIODV.QLH_LIST_HEADER_ID','=','QLH.LIST_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_OM_INV_ORDER_DETAIL_V','HZ_PARTIES','PARTY',660,'EOIODV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Order and Invoice Summary Details Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Order and Invoice Summary Details Report
xxeis.eis_rs_ins.lov( 660,' SELECT OH.ORDER_NUMBER      ORDER_NUMBER,
        NVL(CUST.ACCOUNT_NUMBER, PARTY.PARTY_NUMBER)     CUSTOMER_NUMBER,
		NVL(CUST.ACCOUNT_NAME, PARTY.PARTY_NAME)     CUSTOMER_NAME,
        OH.FLOW_STATUS_CODE  STATUS,
        OH.ORDERED_DATE      ORDERED_DATE
FROM   OE_ORDER_HEADERS OH,
       HZ_PARTIES        PARTY,
       HZ_CUST_ACCOUNTS  CUST
WHERE PARTY.PARTY_ID=CUST.PARTY_ID
AND   CUST.CUST_ACCOUNT_ID =OH.SOLD_TO_ORG_ID','','OM ORDER NUMBER','This gives the Order Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,' SELECT OH.ORDER_NUMBER      ORDER_NUMBER,
        NVL(CUST.ACCOUNT_NUMBER, PARTY.PARTY_NUMBER)     CUSTOMER_NUMBER,
		NVL(CUST.ACCOUNT_NAME, PARTY.PARTY_NAME)     CUSTOMER_NAME,
        OH.FLOW_STATUS_CODE  STATUS,
        OH.ORDERED_DATE      ORDERED_DATE
FROM   OE_ORDER_HEADERS OH,
       HZ_PARTIES        PARTY,
       HZ_CUST_ACCOUNTS  CUST
WHERE PARTY.PARTY_ID=CUST.PARTY_ID
AND   CUST.CUST_ACCOUNT_ID =OH.SOLD_TO_ORG_ID','','OM ORDER NUMBER','This gives the Order Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select CONCATENATED_SEGMENTS ITEM_NAME,DESCRIPTION ,ood.ORGANIZATION_NAME FROM  MTL_SYSTEM_ITEMS_KFV MSI ,org_organization_definitions ood WHERE  OOD.ORGANIZATION_ID=MSI.ORGANIZATION_ID and  exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = msi.organization_id)
','','OM ITEM NAME','This gives the item name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT C.LOCATION  SHIP_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''SHIP_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID
','','OM SHIP TO LOCATION','This gives ship to locations','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT C.LOCATION  BILL_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''BILL_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID','','OM BILL TO LOCATION','This gives bill to locations','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct  trx_number Invoice_Number  from ra_customer_trx','','OM Invoice Number','This Lov pick Transactions Numer .','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct  trx_number Invoice_Number  from ra_customer_trx','','OM Invoice Number','This Lov pick Transactions Numer .','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Order and Invoice Summary Details Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Order and Invoice Summary Details Report
xxeis.eis_rs_utility.delete_report_rows( 'Order and Invoice Summary Details Report' );
--Inserting Report - Order and Invoice Summary Details Report
xxeis.eis_rs_ins.r( 660,'Order and Invoice Summary Details Report','','The Report reviews summary invoice information about orders that have invoiced, including ordered amount, invoiced amount etc.','','','','XXEIS_RS_ADMIN','EIS_OM_INV_ORDER_DETAIL_V','Y','','','XXEIS_RS_ADMIN','','N','Orders','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Order and Invoice Summary Details Report
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ORDER_QUANTITY_UOM','Order Quantity Uom','Order Quantity Uom','','','','','41','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'PRICE_LIST','Price List','Price List','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'INVOICED_QUANTITY','Invoiced Quantity','Invoiced Quantity','','','','','37','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'INVOICING_RULE','Invoicing Rule','Invoicing Rule','','','','','38','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'LINE_CATEGORY_CODE','Line Category Code','Line Category Code','','','','','39','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'LINE_TYPE','Line Type','Line Type','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'OPEN_FLAG','Open Flag','Open Flag','','','','','40','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ORDERED_ITEM','Ordered Item','Ordered Item','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ORDERED_QUANTITY','Ordered Quantity','Ordered Quantity','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'QUANTITY_CREDITED','Quantity Credited','Quantity Credited','','','','','42','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'QUANTITY_ORDERED','Quantity Ordered','Quantity Ordered','','~T~D~2','','','43','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'RECEIVABLE_INV_QUANTITY','Receivable Inv Quantity','Receivable Inv Quantity','','','','','44','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'REFERENCE_TYPE','Reference Type','Reference Type','','','','','45','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'REJECT_REASON_CODE','Reject Reason Code','Reject Reason Code','','','','','46','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'RETURN_REASON_CODE','Return Reason Code','Return Reason Code','','','','','47','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ROUNDING_FACTOR','Rounding Factor','Rounding Factor','','','','','48','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIPMENT_NUMBER','Shipment Number','Shipment Number','','','','','49','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIPPED_QUANTITY','Shipped Quantity','Shipped Quantity','','','','','50','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIPPING_QUANTITY','Shipping Quantity','Shipping Quantity','','','','','51','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIP_FROM','Ship From','Ship From','','','','','52','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIP_TO','Ship To','Ship To','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIP_TO_ADDRESS1','Ship To Address1','Ship To Address1','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIP_TO_ADDRESS2','Ship To Address2','Ship To Address2','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SUBINVENTORY','Subinventory','Subinventory','','','','','53','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'TRANSACTION_NAME','Transaction Name','Transaction Name','','','','','54','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ACTUAL_ARRIVAL_DATE','Actual Arrival Date','Actual Arrival Date','','','','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ACTUAL_SHIPMENT_DATE','Actual Shipment Date','Actual Shipment Date','','','','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'BILL_TO','Bill To','Bill To','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'BILL_TO_ADDRESS1','Bill To Address1','Bill To Address1','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'BILL_TO_ADDRESS2','Bill To Address2','Bill To Address2','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'BILL_TO_LOCATION','Bill To Location','Bill To Location','','','','','56','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'CANCELLED_QUANTITY','Cancelled Quantity','Cancelled Quantity','','','','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'FOB_POINT_CODE','Fob Point Code','Fob Point Code','','','','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'FREIGHT_CARRIER_CODE','Freight Carrier Code','Freight Carrier Code','','','','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'FULFILLED_QUANTITY','Fulfilled Quantity','Fulfilled Quantity','','','','','35','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'FULFILLMENT_DATE','Fulfillment Date','Fulfillment Date','','','','','36','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SOLD_TO','Sold To','Sold To','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SOURCE_TYPE_CODE','Source Type Code','Source Type Code','','','','','58','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'TRANSACTION_DESCRIPTION','Transaction Description','Transaction Description','','','','','61','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'UNIT_LIST_PRICE','Unit List Price','Unit List Price','','','','','62','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','','','','63','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'VISIBLE_DEMAND_FLAG','Visible Demand Flag','Visible Demand Flag','','','','','65','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'BILL_TO_ADDRESS3','Bill To Address3','Bill To Address3','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'BILL_TO_ADDRESS4','Bill To Address4','Bill To Address4','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'BILL_TO_ADDRESS5','Bill To Address5','Bill To Address5','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'BILL_TO_CONTACT','Bill To Contact','Bill To Contact','','','','','55','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIP_TO_ADDRESS3','Ship To Address3','Ship To Address3','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIP_TO_ADDRESS4','Ship To Address4','Ship To Address4','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'SHIP_TO_ADDRESS5','Ship To Address5','Ship To Address5','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'LINE_NUMBER','Line Number','Line Number','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'TRX_LINE_NUMBER','Trx Line Number','Trx Line Number','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'INVOICE_CREATION_DATE','Invoice Creation Date','Invoice Creation Date','','','','','57','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'INVOICE_LINE_EXTENDED_AMOUNT','Invoice Line Extended Amount','Invoice Line Extended Amount','','','','','66','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'INVOICE_LINE_REVENUE_AMOUNT','Invoice Line Revenue Amount','Invoice Line Revenue Amount','','','','','67','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'INVOICE_LINE_TAX_EXT_AMNT','Invoice Line Tax Ext Amnt','Invoice Line Tax Ext Amnt','','','','','68','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'INVOICE_LINE_TAX_REV_AMNT','Invoice Line Tax Rev Amnt','Invoice Line Tax Rev Amnt','','','','','69','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'OPERATING_UNIT','Operating Unit','Operating Unit','','','','','70','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ORDER_LINE_CHARGE','Order Line Charge','Order Line Charge','','','','','71','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ORDER_LINE_CHARGE_COST','Order Line Charge Cost','Order Line Charge Cost','','','','','72','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
xxeis.eis_rs_ins.rc( 'Order and Invoice Summary Details Report',660,'ORDER_LINE_STATUS','Order Line Status','Order Line Status','','','','','73','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_OM_INV_ORDER_DETAIL_V','','');
--Inserting Report Parameters - Order and Invoice Summary Details Report
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Invoice Number To','Invoice Number To','CREDIT_INVOICE_NUMBER','<=','OM Invoice Number','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Order  Number To','Order Number To','ORDER_NUMBER','<=','OM ORDER NUMBER','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','Start Date','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','End Date','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Order Number from','Order Number from','ORDER_NUMBER','>=','OM ORDER NUMBER','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Ordered Item','Ordered Item','ORDERED_ITEM','IN','OM ITEM NAME','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Invoice Number From','Invoice Number From','CREDIT_INVOICE_NUMBER','>=','OM Invoice Number','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Invoice Date From','Invoice Date From','INVOICE_CREATION_DATE','>=','','','DATE','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','Start Date','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Invoice Date To','Invoice Date To','INVOICE_CREATION_DATE','<=','','','DATE','N','Y','9','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','End Date','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Bill To','Bill To','BILL_TO','IN','OM BILL TO LOCATION','','VARCHAR2','N','Y','11','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Ship To','Ship To','SHIP_TO','IN','OM SHIP TO LOCATION','','VARCHAR2','N','Y','12','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','','');
xxeis.eis_rs_ins.rp( 'Order and Invoice Summary Details Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','10','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','','','','');
--Inserting Report Conditions - Order and Invoice Summary Details Report
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'BILL_TO','IN',':Bill To','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'SHIP_TO','IN',':Ship To','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'ORDERED_DATE','>=',':Ordered Date From','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'ORDERED_DATE','<=',':Ordered Date To','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'ORDERED_ITEM','IN',':Ordered Item','','','Y','12','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'INVOICE_CREATION_DATE','>=',':Invoice Date From','','','Y','7','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'INVOICE_CREATION_DATE','<=',':Invoice Date To','','','Y','8','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'ORDER_NUMBER','>=',':Order Number from','','','Y','9','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'ORDER_NUMBER','<=',':Order  Number To','','','Y','10','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'CREDIT_INVOICE_NUMBER','>=',':Invoice Number From','','','Y','11','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Order and Invoice Summary Details Report',660,'CREDIT_INVOICE_NUMBER','<=',':Invoice Number To','','','Y','6','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Order and Invoice Summary Details Report
--Inserting Report Triggers - Order and Invoice Summary Details Report
--Inserting Report Templates - Order and Invoice Summary Details Report
xxeis.eis_rs_ins.R_Tem( 'Order and Invoice Summary Details Report','Order and Invoice Summary Details Report','Seeded template for Order and Invoice Summary Details Report','','','','','','','','','','','','XXEIS_RS_ADMIN');
--Inserting Report Portals - Order and Invoice Summary Details Report
--Inserting Report Dashboards - Order and Invoice Summary Details Report
--Inserting Report Security - Order and Invoice Summary Details Report
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','50870',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','50871',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','50869',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','20005','','50900',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Order and Invoice Summary Details Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Order and Invoice Summary Details Report
xxeis.eis_rs_ins.rpivot( 'Order and Invoice Summary Details Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Order and Invoice Summary Details Report',660,'Pivot','ORDERED_ITEM','ROW_FIELD','','','5','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Order and Invoice Summary Details Report',660,'Pivot','ORDERED_QUANTITY','ROW_FIELD','','','6','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Order and Invoice Summary Details Report',660,'Pivot','ORDER_NUMBER','PAGE_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Order and Invoice Summary Details Report',660,'Pivot','LINE_NUMBER','ROW_FIELD','','','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Order and Invoice Summary Details Report',660,'Pivot','CUSTOMER_NAME','PAGE_FIELD','','','2','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
