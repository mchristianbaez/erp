--Report Name            : Cash Drawer Reconciliation
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_AR_CASH_DRWR_RECON_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_CASH_DRWR_RECON_V
Create or replace View XXEIS.EIS_XXWC_AR_CASH_DRWR_RECON_V
(ORDER_NUMBER,ORG_ID,PAYMENT_TYPE_CODE,PAYMENT_TYPE_CODE_NEW,TAKEN_BY,CASH_TYPE,CHK_CARDNO,CARD_NUMBER,CARD_ISSUER_CODE,CARD_ISSUER_NAME,CASH_AMOUNT,CUSTOMER_NUMBER,CUSTOMER_NAME,BRANCH_NUMBER,CHECK_NUMBER,ORDER_DATE,CASH_DATE,PAYMENT_CHANNEL_NAME,NAME,PAYMENT_NUMBER,PAYMENT_AMOUNT,ORDER_AMOUNT,TOTAL_ORDER_AMOUNT,PARTY_ID,CUST_ACCOUNT_ID,INVOICE_NUMBER,INVOICE_DATE,SEGMENT_NUMBER,HEADER_ID,CASH_RECEIPT_ID,ORDER_TYPE) AS 
SELECT OH.ORDER_NUMBER,
    oh.org_id,
    --olkp.meaning PAYMENT_TYPE_CODE,
    CASE
      WHEN ((OE.PAYMENT_TYPE_CODE IN('CHECK','CREDIT_CARD')
      AND (upper(ARC.NAME) LIKE UPPER('%Card%')
      OR upper(ARC.NAME) LIKE UPPER('%AMX%')
      OR upper(ARC.NAME) LIKE UPPER('%VMD%')))
      OR OH.ATTRIBUTE8 ='CREDIT_CARD')
      THEN 'Credit Card'
      WHEN ((OE.PAYMENT_TYPE_CODE IN ('CHECK')
      AND UPPER(ARC.NAME) NOT LIKE UPPER('%Card%'))
      OR OH.ATTRIBUTE8 = 'CHECK')
      THEN 'Check'
      WHEN (OE.PAYMENT_TYPE_CODE IN ('CASH')
      OR OH.ATTRIBUTE8            = 'CASH')
      THEN 'Cash'
      ELSE NULL
    END PAYMENT_TYPE_CODE,
    CASE
      WHEN OE.PAYMENT_TYPE_CODE IN( 'CHECK','CREDIT_CARD')
      AND upper(ARC.NAME) LIKE UPPER('%Card%')
      THEN 'Credit Card'
      WHEN OE.PAYMENT_TYPE_CODE IN ('CHECK')
      AND UPPER(ARC.NAME) NOT LIKE UPPER('%Card%')
      THEN 'Check'
      ELSE olkp.meaning
    END PAYMENT_TYPE_CODE_NEW,
    PPF.LAST_NAME TAKEN_BY,
    CASE
      WHEN OTL.name = 'RETURN ORDER'
      THEN 'PRISM RETURN'
      WHEN OE.PAYMENT_TYPE_CODE IN ('CASH','CHECK','CREDIT_CARD')
      AND NOT EXISTS
        (SELECT ORDERED_ITEM
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        AND ORDERED_ITEM LIKE '%DEPOSIT%'
        )
        /*AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )*/
      THEN 'CASH SALES'
      WHEN OE.PAYMENT_TYPE_CODE IN ('CASH','CHECK','CREDIT_CARD')
      AND EXISTS
        (SELECT ORDERED_ITEM
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        AND ORDERED_ITEM LIKE '%DEPOSIT%'
        )
        /*AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )*/
      THEN 'DEPOSIT'
    END CASH_TYPE,
    /*CASE
    WHEN OE.PAYMENT_TYPE_CODE IN( 'CHECK','CREDIT_CARD') AND ARC.NAME LIKE '%Credit%Card%'
    THEN SUBSTR(OE.CHECK_NUMBER,-4)
    ELSE OE.CHECK_NUMBER
    END CHK_CARDNO,*/
    DECODE(OE.PAYMENT_TYPE_CODE,'CHECK',OE.CHECK_NUMBER,'CREDIT_CARD',SUBSTR(ITE.CARD_NUMBER,-4)) CHK_CARDNO,
    SUBSTR(ITE.CARD_NUMBER,                                                                  -4) CARD_NUMBER,
    ITE.CARD_ISSUER_CODE,
    ITE.CARD_ISSUER_NAME,
    --  NVL(OE.prepaid_amount,0) CASH_AMOUNT,
    CASE
      WHEN OTL.name = 'RETURN ORDER'
      THEN
        (SELECT NVL(ROUND(SUM((ROUND(ol.ordered_quantity*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE)*DECODE(otl.order_category_code,'RETURN',-1,1)),2),0) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL,
          oe_Transaction_Types_Vl otl
        WHERE OL.HEADER_ID  =OH.HEADER_ID
        AND OL.LINE_TYPE_ID =OTL.TRANSACTION_TYPE_ID
        )
      WHEN TRUNC(oe.creation_date) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
      THEN NVL(OE.prepaid_amount,0)
      ELSE 0
    END CASH_AMOUNT,
    hca.account_number customer_number,
    HZP.PARTY_NAME CUSTOMER_NAME,
    OOD.ORGANIZATION_CODE BRANCH_NUMBER,
    OE.CHECK_NUMBER,
    TRUNC(OH.ORDERED_DATE) ORDER_DATE,
    --  TRUNC(OH.ORDERED_DATE) CASH_DATE,
    --TRUNC(oe.creation_date) CASH_DATE,
    TRUNC(oe.creation_date) CASH_DATE,
    ITE.PAYMENT_CHANNEL_NAME,
    ARC.name,
    PAYMENT_NUMBER,
    (
    CASE
      WHEN OTL.name = 'RETURN ORDER'
      THEN
        (SELECT NVL(ROUND(SUM((ROUND(ol.ordered_quantity*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE)*DECODE(otl.order_category_code,'RETURN',-1,1)),2),0)
        FROM OE_ORDER_LINES OL,
          oe_Transaction_Types_Vl otl
        WHERE OL.HEADER_ID  = OH.HEADER_ID
        AND OL.LINE_TYPE_ID = OTL.TRANSACTION_TYPE_ID
        )
      WHEN TRUNC(oe.creation_date) <= NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE, NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE,SYSDATE))
      THEN
        (SELECT NVL(ROUND(SUM( OE1.prepaid_amount),2),0)
        FROM OE_PAYMENTS OE1
        WHERE HEADER_ID              =OH.HEADER_ID
        AND TRUNC(oe.creation_date) <= NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE, NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE,SYSDATE))
        )
      ELSE 0
    END)PAYMENT_AMOUNT,
    CASE
      WHEN Payment_Number=1
      AND (OTL.NAME     <> 'STANDARD ORDER'
      OR EXISTS
        (SELECT 1
        FROM OE_ORDER_LINES OL1
        WHERE OL1.header_id = OH.HEADER_ID
        AND OL1.ORDERED_ITEM LIKE '%DEPOSIT%'
        ))
      THEN
        (SELECT NVL(ROUND(SUM((ROUND(ol.ordered_quantity*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE)*DECODE(otl.order_category_code,'RETURN',-1,1)),2),0) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL,
          oe_transaction_types_vl OTL
        WHERE OL.HEADER_ID  =OH.HEADER_ID
        AND OL.LINE_TYPE_ID =OTL.TRANSACTION_TYPE_ID
        )
      WHEN Payment_Number=1
      AND OTL.NAME       ='STANDARD ORDER'
      THEN
        (SELECT NVL (ROUND(SUM(ROUND(NVL(OL.SHIPPED_QUANTITY,0)*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE),2),0) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID           =OH.HEADER_ID
        AND NVL(SHIPPED_QUANTITY,0) <> 0
        AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        AND OL.ORDERED_ITEM NOT LIKE '%DEPOSIT%'
        )
      WHEN Payment_Number IS NULL
      AND (OTL.NAME       <> 'STANDARD ORDER'
      OR EXISTS
        (SELECT 1
        FROM OE_ORDER_LINES OL1
        WHERE OL1.header_id = OH.HEADER_ID
        AND OL1.ORDERED_ITEM LIKE '%DEPOSIT%'
        ))
      THEN
        (SELECT NVL(ROUND(SUM((ROUND(ol.ordered_quantity*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE)*DECODE(otl.order_category_code,'RETURN',-1,1)),2),0) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL,
          OE_TRANSACTION_TYPES_VL OTL
        WHERE OL.HEADER_ID  =OH.HEADER_ID
        AND OL.LINE_TYPE_ID =OTL.TRANSACTION_TYPE_ID
        )
      WHEN Payment_Number IS NULL
      AND OTL.NAME         ='STANDARD ORDER'
      THEN
        (SELECT NVL(ROUND(SUM(ROUND(OL.SHIPPED_QUANTITY*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE),2),0) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        AND OL.ORDERED_ITEM NOT LIKE '%DEPOSIT%'
        )
      ELSE 0
    END ORDER_AMOUNT,
    CASE
      WHEN OTL.NAME ='STANDARD ORDER'
      THEN
        (SELECT NVL (ROUND(SUM(ROUND(NVL(OL.SHIPPED_QUANTITY,0)*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE),2),0) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID                  =OH.HEADER_ID
        AND NVL(SHIPPED_QUANTITY,0)        <> 0
        AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) <= XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
        )
      ELSE
        (SELECT NVL(ROUND(SUM((ROUND(ol.ordered_quantity*OL.UNIT_SELLING_PRICE,2)+ OL.TAX_VALUE)*DECODE(otl.order_category_code,'RETURN',-1,1)),2),0) ORDER_AMOUNT
        FROM OE_ORDER_LINES OL,
          OE_TRANSACTION_TYPES_VL OTL
        WHERE OL.HEADER_ID  =OH.HEADER_ID
        AND OL.LINE_TYPE_ID =OTL.TRANSACTION_TYPE_ID
        )
    END TOTAL_ORDER_AMOUNT,
    HZP.PARTY_ID,
    HCA.CUST_ACCOUNT_ID,
    (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT
    WHERE TO_CHAR(OH.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    )Invoice_Number,
    --   NULL CREDIT_NUMBER,
    (
    SELECT MAX(RCT.TRX_DATE)
    FROM RA_CUSTOMER_TRX RCT
    WHERE TO_CHAR(OH.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    )INVOICE_DATE,
    (SELECT MAX(GCC.SEGMENT2)
    FROM OE_ORDER_LINES L,
      RA_CUSTOMER_TRX_LINES RCTL,
      RA_CUSTOMER_TRX RCT,
      RA_CUST_TRX_LINE_GL_DIST RCTG,
      GL_CODE_COMBINATIONS_KFV GCC
    WHERE OH.HEADER_ID           =L.HEADER_ID
    AND TO_CHAR(OH.ORDER_NUMBER) =RCTL.INTERFACE_LINE_ATTRIBUTE1
    AND TO_CHAR(L.LINE_ID)       =RCTL.INTERFACE_LINE_ATTRIBUTE6
    AND RCT.CUSTOMER_TRX_ID      =RCTL.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_ID     =RCTG.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_LINE_ID=RCTG.CUSTOMER_TRX_LINE_ID
    AND rctg.code_combination_id =gcc.code_combination_id
    )Segment_Number,
    Oh.Header_Id Header_Id,
    0 cash_receipt_id,
    otl.name Order_type
  FROM OE_ORDER_HEADERS OH,
    oe_payments oe,
    oe_lookups olkp,
    hz_parties hzp,
    HZ_CUST_ACCOUNTS HCA,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    IBY_TRXN_EXTENSIONS_V ITE,
    PER_ALL_PEOPLE_F PPF,
    FND_USER FU,
    AR_RECEIPT_METHODS arc,
    oe_transaction_types_VL otl,
    ra_terms ra
  WHERE OH.SOLD_TO_ORG_ID =HCA.CUST_ACCOUNT_ID
  AND OE.HEADER_ID(+)     =OH.HEADER_ID
  AND oh.payment_term_id  = ra.term_id
  AND HZP.PARTY_ID        =HCA.PARTY_ID
  AND OH.SHIP_FROM_ORG_ID =OOD.ORGANIZATION_ID
  AND oh.order_type_id    = otl.transaction_type_id
  AND FU.USER_ID          =OH.CREATED_BY
  AND fu.employee_id      =PPF.PERSON_ID(+)
  AND OH.ORDERED_DATE BETWEEN NVL(PPF.EFFECTIVE_START_DATE,OH.ORDERED_DATE) AND NVL(PPF.EFFECTIVE_END_DATE,OH.ORDERED_DATE)
  AND OE.TRXN_EXTENSION_ID =ITE.TRXN_EXTENSION_ID(+)
  AND oe.receipt_method_id =arc.receipt_method_id(+)
  AND oe.payment_type_code = olkp.lookup_code(+)
  AND (arc.name is null or (upper(arc.name) NOT LIKE upper('%GIFT%')
  OR upper(arc.name) NOT LIKE upper('%REBATE%')))
  AND oe.payment_collection_event(+) ='PREPAY'
  AND oh.flow_status_code           IN('BOOKED','CLOSED')
  AND Olkp.Lookup_Type(+)            = 'PAYMENT TYPE'
  AND otl.name                      <> 'INTERNAL ORDER'
  AND (Payment_Number               IS NOT NULL
  OR ra.name                        IN ('COD','PRFRDCASH','IMMEDIATE'))
  AND NOT EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc
    WHERE hca.party_id       = hcp.party_id
    AND hca.cust_account_id  = hcp.cust_account_id
    AND hcp.site_use_id     IS NULL
    AND hcp.profile_class_id = hcpc.profile_class_id(+)
    AND hcpc.name LIKE 'WC%Branches%'
    )
    -- And Oh.Header_Id=26587
  AND(otl.name IN ('COUNTER ORDER','STANDARD ORDER')
  OR NOT EXISTS
    (SELECT 1
    FROM XXWC_OM_CASH_REFUND_TBL XOC
    WHERE XOC.RETURN_HEADER_ID=OH.HEADER_ID
    ) )
  AND ((otl.name ='STANDARD ORDER'
  AND EXISTS
    (SELECT 1
    FROM OE_ORDER_LINES L
    WHERE OH.HEADER_ID =L.HEADER_ID
    AND ((TRUNC(L.ACTUAL_SHIPMENT_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE )
    OR (TRUNC(oe.creation_date) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE) )
    OR (OH.HEADER_ID =L.HEADER_ID
    AND L.ORDERED_ITEM LIKE '%DEPOSIT%'
    AND (TRUNC(OH.ORDERED_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE))
    ) )
  OR(otl.name <> 'STANDARD ORDER'
  AND((TRUNC(OH.ORDERED_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)
  OR(TRUNC(oe.creation_date) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE) ) ) )
  /* AND EXISTS
  (SELECT 1
  FROM OE_ORDER_LINES L
  WHERE OH.HEADER_ID =L.HEADER_ID
  AND (( otl.name    ='STANDARD ORDER'
  AND (TRUNC(L.ACTUAL_SHIPMENT_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE ))
  OR (L.ORDERED_ITEM LIKE '%DEPOSIT%')
  OR (oe.Payment_Number IS NOT NULL)
  OR (otl.name <> 'STANDARD ORDER' )
  )*/
  /* AND EXISTS
  (SELECT 1
  FROM RA_CUSTOMER_TRX RCT
  WHERE TO_CHAR(Oh.Order_Number) =Rct.Interface_Header_Attribute1
  )*/
  UNION
  SELECT OH.ORDER_NUMBER,
    oh.org_id,
    CASE
      WHEN (upper(ARC.NAME) LIKE UPPER('%AMX%')
      OR upper(ARC.NAME) LIKE UPPER('%VMD%'))
      THEN 'Credit Card'
      ELSE DECODE(olkp.meaning,'Check','Cash',olkp.meaning)
    END PAYMENT_TYPE_CODE,
    olkp.meaning PAYMENT_TYPE_CODE_NEW,
    PPF.LAST_NAME TAKEN_BY,
    'CASH RETURN' CASH_TYPE,
    -- DECODE(XMCR.PAYMENT_TYPE_CODE,'CHECK',OE.CHECK_NUMBER,'CREDIT_CARD',SUBSTR(ITE.CARD_NUMBER,-4)) CHK_CARDNO,
    CASE
        /*WHEN XMCR.PAYMENT_TYPE_CODE ='CREDIT_CARD'
        THEN
        (SELECT MAX(SUBSTR(ITE.CARD_NUMBER,-4))
        FROM IBY_TRXN_EXTENSIONS_V ITE,
        oe_payments oe
        WHERE oe.header_id       = xmcr.header_id
        AND OE.TRXN_EXTENSION_ID =ITE.TRXN_EXTENSION_ID
        )*/
      WHEN (upper(ARC.NAME) LIKE UPPER('%AMX%')
      OR upper(ARC.NAME) LIKE UPPER('%VMD%'))
      THEN
        (SELECT MAX(OE.CHECK_NUMBER)
        FROM oe_payments oe
        WHERE oe.header_id       = xmcr.header_id
        AND OE.PAYMENT_TYPE_CODE ='CHECK'
        )
      ELSE NULL
    END CHK_CARDNO,
    --SUBSTR(ITE.CARD_NUMBER,                                                                    -4) CARD_NUMBER,
    NULL CARD_NUMBER,
    NULL CARD_ISSUER_CODE, --ITE.CARD_ISSUER_CODE,
    CASE
      WHEN XMCR.PAYMENT_TYPE_CODE ='CREDIT_CARD'
      THEN
        (SELECT MAX(ITE.CARD_ISSUER_NAME)
        FROM IBY_TRXN_EXTENSIONS_V ITE,
          oe_payments oe
        WHERE oe.header_id       = xmcr.header_id
        AND OE.TRXN_EXTENSION_ID =ITE.TRXN_EXTENSION_ID
        )
      ELSE NULL
    END CARD_ISSUER_NAME, --ITE.CARD_ISSUER_NAME,
    -- NVL(xmcr.refund_amount*-1,0) cash_amount,
    CASE
      WHEN TRUNC(XMCR.CREATION_DATE) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE
      THEN NVL(XMCR.REFUND_AMOUNT*-1,0)
      ELSE 0
    END cash_amount,
    hca.account_number customer_number,
    HZP.PARTY_NAME CUSTOMER_NAME,
    OOD.ORGANIZATION_CODE BRANCH_NUMBER,
    NULL CHECK_NUMBER, --OE.CHECK_NUMBER,
    -- TRUNC(OH.ORDERED_DATE) CASH_DATE,
    OH.ORDERED_DATE ORDER_DATE,
    NVL(XMCR.refund_date,XMCR.creation_date) cash_date,
    NULL PAYMENT_CHANNEL_NAME, --ITE.PAYMENT_CHANNEL_NAME,
    Arc.Name NAME,             --Arc.Name,
    NULL PAYMENT_NUMBER,
    (SELECT NVL(SUM(XX.REFUND_AMOUNT),0)
    FROM XXWC_OM_CASH_REFUND_TBL XX
    WHERE XX.RETURN_HEADER_ID =XMCR.RETURN_HEADER_ID
      --AND XX.PAYMENT_TYPE_CODE <> 'CHECK'
    )Payment_Amount,
    xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_cash_drawer_amt(oh.header_id) ORDER_AMOUNT,
    xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_cash_drawer_amt(oh.header_id) TOTAL_ORDER_AMOUNT,
    HZP.PARTY_ID,
    HCA.CUST_ACCOUNT_ID,
    (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT,
      OE_ORDER_HEADERS H
    WHERE H.HEADER_ID           =XMCR.HEADER_ID
    AND TO_CHAR(H.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    )Invoice_Number,
    /*   (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT,
    Oe_Order_Headers H
    WHERE H.HEADER_ID           =XMCR.RETURN_HEADER_ID
    And To_Char(H.Order_Number) =Rct.Interface_Header_Attribute1
    )CREDIT_NUMBER,*/
    (
    SELECT MAX(RCT.TRX_DATE)
    FROM RA_CUSTOMER_TRX RCT,
      OE_ORDER_HEADERS H1
    WHERE TO_CHAR(H1.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    AND H1.HEADER_ID               =XMCR.HEADER_ID
    )INVOICE_DATE,
    (SELECT MAX(GCC.SEGMENT2)
    FROM OE_ORDER_LINES L,
      OE_ORDER_HEADERS H2,
      RA_CUSTOMER_TRX_LINES RCTL,
      RA_CUSTOMER_TRX RCT,
      RA_CUST_TRX_LINE_GL_DIST RCTG,
      GL_CODE_COMBINATIONS_KFV GCC
    WHERE H2.HEADER_ID           =XMCR.HEADER_ID
    AND H2.HEADER_ID             =L.HEADER_ID
    AND TO_CHAR(H2.order_number) =rct.interface_HEADER_attribute1
    AND TO_CHAR(L.LINE_ID)       =RCTL.INTERFACE_LINE_ATTRIBUTE6
    AND RCT.CUSTOMER_TRX_ID      =RCTL.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_ID     =RCTG.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_LINE_ID=RCTG.CUSTOMER_TRX_LINE_ID
    AND RCTG.CODE_COMBINATION_ID =GCC.CODE_COMBINATION_ID
    )Segment_Number,
    Oh.Header_Id Header_Id,
    XMCR.cash_receipt_id cash_receipt_id,
    otl.name Order_type
  FROM OE_ORDER_HEADERS OH,
    OE_LOOKUPS OLKP,
    HZ_PARTIES HZP,
    HZ_CUST_ACCOUNTS HCA,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    XXWC_OM_CASH_REFUND_TBL XMCR,
    AR_RECEIPT_METHODS arc,
    ar_cash_receipts acr,
    PER_ALL_PEOPLE_F PPF,
    FND_USER FU,
    oe_transaction_types_VL otl
  WHERE OH.sold_to_org_id   =hca.cust_account_id
  AND HZP.PARTY_ID          =HCA.PARTY_ID
  AND OH.SHIP_FROM_ORG_ID   =OOD.ORGANIZATION_ID
  AND xmcr.RETURN_header_id =oh.header_id
  AND xmcr.cash_receipt_id  = acr.cash_receipt_id(+)
  AND acr.receipt_method_id = arc.receipt_method_id(+)
  AND FU.USER_ID            =OH.CREATED_BY
  AND otl.name NOT         IN ('COUNTER ORDER','STANDARD ORDER')
  AND fu.employee_id        =PPF.PERSON_ID(+)
  AND TRUNC(OH.ORDERED_DATE) BETWEEN NVL(TRUNC(PPF.EFFECTIVE_START_DATE),TRUNC(OH.ORDERED_DATE)) AND NVL(TRUNC(PPF.EFFECTIVE_END_DATE),TRUNC(OH.ORDERED_DATE))
  AND oh.order_type_id = otl.transaction_type_id
  AND(NVL(TRUNC(XMCR.refund_date),TRUNC(XMCR.creation_date)) BETWEEN XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)
    -- AND XMCR.PAYMENT_TYPE_CODE <> 'CHECK'
  AND NVL(XMCR.REFUND_AMOUNT,0) <> 0
  AND xmcr.payment_type_code     = olkp.lookup_code
  AND Olkp.Lookup_Type           = 'PAYMENT TYPE'
  AND otl.name                  <> 'INTERNAL ORDER'
  ORDER BY Header_Id,
    ORDER_AMOUNT DESC/
set scan on define on
prompt Creating View Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_CASH_DRWR_RECON_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Cash Drwr Recon V','EXACDRV','','');
--Delete View Columns for EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_CASH_DRWR_RECON_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_DATE',222,'Cash Date','CASH_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Cash Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','BRANCH_NUMBER',222,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_AMOUNT',222,'Cash Amount','CASH_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Cash Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_ISSUER_NAME',222,'Card Issuer Name','CARD_ISSUER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_TYPE',222,'Cash Type','CASH_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cash Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_TYPE_CODE',222,'Payment Type Code','PAYMENT_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','TAKEN_BY',222,'Taken By','TAKEN_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Taken By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','SEGMENT_NUMBER',222,'Segment Number','SEGMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CHK_CARDNO',222,'Chk Cardno','CHK_CARDNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Chk Cardno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_AMOUNT',222,'Order Amount','ORDER_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','NAME',222,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_DATE',222,'Order Date','ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_CHANNEL_NAME',222,'Payment Channel Name','PAYMENT_CHANNEL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Channel Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CHECK_NUMBER',222,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_ISSUER_CODE',222,'Card Issuer Code','CARD_ISSUER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_NUMBER',222,'Card Number','CARD_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORG_ID',222,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_NUMBER',222,'Payment Number','PAYMENT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_RECEIPT_ID',222,'Cash Receipt Id','CASH_RECEIPT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cash Receipt Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','HEADER_ID',222,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_AMOUNT',222,'Payment Amount','PAYMENT_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_TYPE_CODE_NEW',222,'Payment Type Code New','PAYMENT_TYPE_CODE_NEW','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code New','','','');
--Inserting View Components for EIS_XXWC_AR_CASH_DRWR_RECON_V
--Inserting View Component Joins for EIS_XXWC_AR_CASH_DRWR_RECON_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Cash Drawer Reconciliation
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Cash Drawer Reconciliation
xxeis.eis_rs_utility.delete_report_rows( 'Cash Drawer Reconciliation' );
--Inserting Report - Cash Drawer Reconciliation
xxeis.eis_rs_ins.r( 660,'Cash Drawer Reconciliation','','This report provides a total listing of all Cash, Check and Credit Card Sales by Branch (Customer, Taken By, Order Number, Cash Date, Cash Amount, Cash Type, Invoice Number, Invoice Date, Payment Type, Check/Credit Card No.)','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_CASH_DRWR_RECON_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'CASH_AMOUNT','Cash Amount','Cash Amount','','~T~D~2','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'CASH_DATE','Cash Date','Cash Date','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'CASH_TYPE','Cash Type','Cash Type','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'CHK_CARDNO','Check Card No','Chk Cardno','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'NAME','Receipt Method','Name','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'PAYMENT_TYPE_CODE','Payment Type','Payment Type Code','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'SEGMENT_NUMBER','Segment Number','Segment Number','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'TAKEN_BY','Created By','Taken By','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'CARD_ISSUER_NAME','Card Type','Card Issuer Name','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'DIFFERENCE','DIFFERENCE','Card Issuer Name','NUMBER','~T~D~2','default','','10','Y','','','','','','','CASE WHEN EXACDRV.PAYMENT_TYPE_CODE = ''Prism Return'' THEN  0 WHEN EXACDRV.CASH_TYPE = ''CASH RETURN'' THEN  0 WHEN upper(EXACDRV.NAME) LIKE ''%POA%'' THEN   0 WHEN EXACDRV.Order_type = ''STANDARD ORDER'' AND NVL(EXACDRV.PAYMENT_NUMBER,1) = 1 THEN  (EXACDRV.PAYMENT_AMOUNT-(EXACDRV.TOTAL_ORDER_AMOUNT + NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_order_freight(EXACDRV.header_id),0))) WHEN NVL(EXACDRV.PAYMENT_NUMBER,1) = 1 THEN   (EXACDRV.PAYMENT_AMOUNT- (EXACDRV.order_amount + NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_order_freight(EXACDRV.header_id),0))) ELSE  0 END','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'ORDER_DATE','Order Date','Order Date','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',660,'ORDER_AMOUNT','Order Amount','Order Amount','NUMBER','~T~D~2','default','','8','Y','','','','','','','CASE WHEN nvl(EXACDRV.order_amount,0) != 0 THEN (NVL(EXACDRV.order_amount,0) + NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_order_freight(EXACDRV.header_id),0)) ELSE 0 END','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
--Inserting Report Parameters - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rp( 'Cash Drawer Reconciliation',660,'Location','Branch Number','BRANCH_NUMBER','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cash Drawer Reconciliation',660,'From Date','From Date','','>=','','','DATE','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cash Drawer Reconciliation',660,'To Date','To Date','','<=','','','DATE','Y','Y','3','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rcn( 'Cash Drawer Reconciliation',660,'BRANCH_NUMBER','IN',':Location','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cash Drawer Reconciliation',660,'','','','','AND (:From Date IS NULL OR TRUNC(ORDER_DATE) >= :From Date OR TRUNC(CASH_DATE)   >= :From Date
 or ( EXACDRV.Order_type = ''STANDARD ORDER'' AND ((nvl(EXACDRV.TOTAL_ORDER_AMOUNT,0) + NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_order_freight(EXACDRV.header_id),0)) - nvl(EXACDRV.payment_amount,0)) >0  AND Exists (SELECT 1  FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID  =EXACDRV.HEADER_ID
        AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) BETWEEN :From Date and :To Date ))) 
AND (:To Date IS NULL OR TRUNC(ORDER_DATE) <= :To Date OR TRUNC(CASH_DATE) <= :To Date
 or ( EXACDRV.Order_type = ''STANDARD ORDER'' AND ((nvl(EXACDRV.TOTAL_ORDER_AMOUNT,0) + NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_order_freight(EXACDRV.header_id),0)) - nvl(EXACDRV.payment_amount,0)) >0  AND Exists (SELECT 1  FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID  =EXACDRV.HEADER_ID
        AND TRUNC(OL.ACTUAL_SHIPMENT_DATE) BETWEEN :From Date and :To Date ))) ','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Cash Drawer Reconciliation
--Inserting Report Triggers - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rt( 'Cash Drawer Reconciliation',660,'declare
begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.G_CASH_FROM_DATE:= :From Date;
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.G_CASH_TO_DATE:=:To Date;
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Cash Drawer Reconciliation
xxeis.eis_rs_ins.R_Tem( 'Cash Drawer Reconciliation','Cash Drawer Reconciliation','Cash Drawer Reconciliation','','','','','','','','','','','','XXEIS_RS_ADMIN');
--Inserting Report Portals - Cash Drawer Reconciliation
--Inserting Report Dashboards - Cash Drawer Reconciliation
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Reconciliation','Dynamic 758','Dynamic 758','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Reconciliation','Dynamic 759','Dynamic 759','horizontal stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Reconciliation','Dynamic 751','Dynamic 751','pie','large','Cash Amount','Cash Amount','Customer Name','Customer Name','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Reconciliation','Dynamic 754','Dynamic 754','absolute line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Reconciliation','Dynamic 752','Dynamic 752','pie','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Reconciliation','Dynamic 753','Dynamic 753','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Reconciliation','Dynamic 756','Dynamic 756','stacked line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Reconciliation','Dynamic 757','Dynamic 757','scatter','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Reconciliation','Dynamic 755','Dynamic 755','point','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
--Inserting Report Security - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','','LB048272','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','20434',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','20475',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50721',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50662',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50720',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50723',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50801',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50665',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50722',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50780',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50667',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50668',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','20005','','50900',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','401','','50835',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','50838',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','401','','50840',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','51044',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rpivot( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Cash By Payment Type
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','CASH_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','CUSTOMER_NUMBER','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','CUSTOMER_NAME','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','NAME','PAGE_FIELD','','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','SEGMENT_NUMBER','PAGE_FIELD','','','5','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','CASH_AMOUNT','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','BRANCH_NUMBER','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment Type','PAYMENT_TYPE_CODE','ROW_FIELD','','','2','','');
--Inserting Report Summary Calculation Columns For Pivot- Cash By Payment Type
xxeis.eis_rs_ins.rpivot( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','2','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Cash By Payment and Card Type
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CASH_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CUSTOMER_NUMBER','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CUSTOMER_NAME','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','SEGMENT_NUMBER','PAGE_FIELD','','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CASH_AMOUNT','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','BRANCH_NUMBER','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','PAYMENT_TYPE_CODE','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','CARD_ISSUER_NAME','PAGE_FIELD','','','5','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Reconciliation',660,'Cash By Payment and Card Type','NAME','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Cash By Payment and Card Type
END;
/
set scan on define on
