--Report Name            : XXWC AR Escheatment Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_AGING_BAD_EXP_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_AGING_BAD_EXP_V
Create or replace View XXEIS.EIS_XXWC_AR_AGING_BAD_EXP_V
 AS 
SELECT "CUST_ACCOUNT_ID",
    "CUSTOMER_ACCOUNT_NUMBER",
    "PRISM_CUSTOMER_NUMBER",
    "CUSTOMER_NAME",
    "BILL_TO_SITE_USE_ID",
    "BILL_TO_PARTY_SITE_NUMBER",
    "BILL_TO_PRISM_SITE_NUMBER",
    "BILL_TO_PARTY_SITE_NAME",
    "BILL_TO_SITE_NAME",
    "TRX_BILL_TO_SITE_NAME",
    "TRX_PARTY_SITE_NUMBER",
    "TRX_PARTY_SITE_NAME",
    "BILL_TO_ADDRESS1",
    "BILL_TO_ADDRESS2",
    "BILL_TO_ADDRESS3",
    "BILL_TO_ADDRESS4",
    "BILL_TO_CITY",
    "BILL_TO_CITY_PROVINCE",
    "BILL_TO_ZIP_CODE",
    "BILL_TO_COUNTRY",
    "PAYMENT_SCHEDULE_ID",
    "CUSTOMER_TRX_ID",
    "RCTA_CCID",
    "CASH_RECEIPT_ID",
    "ACRA_CCID",
    "INVOICE_NUMBER",
    "RECEIPT_NUMBER",
    "BRANCH_LOCATION",
    "TRX_NUMBER",
    "TRX_DATE",
    "DUE_DATE",
    "TRX_TYPE",
    "TRANSATION_BALANCE",
    "TRANSACTION_REMAINING_BALANCE",
    "AGE",
    "CURRENT_BALANCE",
    "THIRTY_DAYS_BAL",
    "SIXTY_DAYS_BAL",
    "NINETY_DAYS_BAL",
    "ONE_EIGHTY_DAYS_BAL",
    "THREE_SIXTY_DAYS_BAL",
    "OVER_THREE_SIXTY_DAYS_BAL",
    "LAST_PAYMENT_DATE",
    "CUSTOMER_ACCOUNT_STATUS",
    "SITE_CREDIT_HOLD",
    "CUSTOMER_PROFILE_CLASS",
    "COLLECTOR_NAME",
    "CREDIT_ANALYST",
    "ACCOUNT_MANAGER",
    "ACCOUNT_BALANCE",
    "CUST_PAYMENT_TERM",
    "REMIT_TO_ADDRESS_CODE",
    "STMT_BY_JOB",
    "SEND_STATEMENT_FLAG",
    "SEND_CREDIT_BAL_FLAG",
    "TRX_CUSTOMER_ID",
    "TRX_BILL_SITE_USE_ID",
    "CUSTOMER_PO_NUMBER",
    "PMT_STATUS",
    SALESREP_NUMBER SALESREP_NUMBER,
    YARD_CREDIT_LIMIT CREDIT_LIMIT,
    TWELVE_MONTHS_SALES TWELVE_MONTHS_SALES,
    CUST_PAYMENT_TERM TERMS,
    acct_phone_number PHONE_NUMBER,
    NULL IN_PROCESS,
    TO_NUMBER(NULL) LARGEST_BALANCE,
    TO_NUMBER(NULL) AVERAGE_DAYS,
    TO_NUMBER(NULL) AMOUNT_PAID,
    ACCOUNT_STATUS ACCOUNT_STATUS,
    NULL Territory,
    trx_bill_to_address1 ,
    trx_bill_to_address2 ,
    trx_bill_to_address3 ,
    trx_bill_to_address4 ,
    trx_bill_to_city ,
    trx_bill_to_city_prov ,
    trx_bill_to_zip_code ,
    trx_bill_to_country,
    trx_bill_to_address1||','||decode(trim(trx_bill_to_address2),null,null,trx_bill_to_address2||',')||
    trx_bill_to_city||','||trx_bill_to_city_prov||','||trx_bill_to_zip_code trx_address
  FROM xxeis.XXWC_AR_CUSTOMER_BALANCE_MV/
set scan on define on
prompt Creating View Data for XXWC AR Escheatment Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_AGING_BAD_EXP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_AGING_BAD_EXP_V',222,'','','','','DM027741','XXEIS','Eis Xxwc Ar Aging Bad Exp V','EXAABEV','','');
--Delete View Columns for EIS_XXWC_AR_AGING_BAD_EXP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_AGING_BAD_EXP_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_AGING_BAD_EXP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','ACCOUNT_BALANCE',222,'Account Balance','ACCOUNT_BALANCE','','','','DM027741','NUMBER','','','Account Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','ACCOUNT_MANAGER',222,'Account Manager','ACCOUNT_MANAGER','','','','DM027741','VARCHAR2','','','Account Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CREDIT_ANALYST',222,'Credit Analyst','CREDIT_ANALYST','','','','DM027741','VARCHAR2','','','Credit Analyst','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','COLLECTOR_NAME',222,'Collector Name','COLLECTOR_NAME','','','','DM027741','VARCHAR2','','','Collector Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_PROFILE_CLASS',222,'Customer Profile Class','CUSTOMER_PROFILE_CLASS','','','','DM027741','VARCHAR2','','','Customer Profile Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','SITE_CREDIT_HOLD',222,'Site Credit Hold','SITE_CREDIT_HOLD','','','','DM027741','VARCHAR2','','','Site Credit Hold','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_ACCOUNT_STATUS',222,'Customer Account Status','CUSTOMER_ACCOUNT_STATUS','','','','DM027741','VARCHAR2','','','Customer Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','LAST_PAYMENT_DATE',222,'Last Payment Date','LAST_PAYMENT_DATE','','','','DM027741','DATE','','','Last Payment Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','OVER_THREE_SIXTY_DAYS_BAL',222,'Over Three Sixty Days Bal','OVER_THREE_SIXTY_DAYS_BAL','','~~2','','DM027741','NUMBER','','','Over Three Sixty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','THREE_SIXTY_DAYS_BAL',222,'Three Sixty Days Bal','THREE_SIXTY_DAYS_BAL','','~~2','','DM027741','NUMBER','','','Three Sixty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','ONE_EIGHTY_DAYS_BAL',222,'One Eighty Days Bal','ONE_EIGHTY_DAYS_BAL','','~~2','','DM027741','NUMBER','','','One Eighty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','NINETY_DAYS_BAL',222,'Ninety Days Bal','NINETY_DAYS_BAL','','~~2','','DM027741','NUMBER','','','Ninety Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','SIXTY_DAYS_BAL',222,'Sixty Days Bal','SIXTY_DAYS_BAL','','~~2','','DM027741','NUMBER','','','Sixty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','THIRTY_DAYS_BAL',222,'Thirty Days Bal','THIRTY_DAYS_BAL','','~~2','','DM027741','NUMBER','','','Thirty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','AGE',222,'Age','AGE','','','','DM027741','NUMBER','','','Age','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRANSACTION_REMAINING_BALANCE',222,'Transaction Remaining Balance','TRANSACTION_REMAINING_BALANCE','','','','DM027741','NUMBER','','','Transaction Remaining Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','DUE_DATE',222,'Due Date','DUE_DATE','','','','DM027741','DATE','','','Due Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','RECEIPT_NUMBER',222,'Receipt Number','RECEIPT_NUMBER','','','','DM027741','VARCHAR2','','','Receipt Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','DM027741','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BRANCH_LOCATION',222,'Branch Location','BRANCH_LOCATION','','','','DM027741','VARCHAR2','','','Branch Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','DM027741','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CURRENT_BALANCE',222,'Current Balance','CURRENT_BALANCE','','','','DM027741','NUMBER','','','Current Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_ACCOUNT_NUMBER',222,'Customer Account Number','CUSTOMER_ACCOUNT_NUMBER','','','','DM027741','VARCHAR2','','','Customer Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','PRISM_CUSTOMER_NUMBER',222,'Prism Customer Number','PRISM_CUSTOMER_NUMBER','','','','DM027741','VARCHAR2','','','Prism Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRANSATION_BALANCE',222,'Transation Balance','TRANSATION_BALANCE','','~~2','','DM027741','NUMBER','','','Transation Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_SITE_NAME',222,'Trx Bill To Site Name','TRX_BILL_TO_SITE_NAME','','','','DM027741','VARCHAR2','','','Trx Bill To Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','DM027741','DATE','','','Trx Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_PARTY_SITE_NUMBER',222,'Trx Party Site Number','TRX_PARTY_SITE_NUMBER','','','','DM027741','VARCHAR2','','','Trx Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_TYPE',222,'Trx Type','TRX_TYPE','','','','DM027741','VARCHAR2','','','Trx Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','SALESREP_NUMBER',222,'Salesrep Number','SALESREP_NUMBER','','','','DM027741','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUST_PAYMENT_TERM',222,'Cust Payment Term','CUST_PAYMENT_TERM','','','','DM027741','VARCHAR2','','','Cust Payment Term','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ADDRESS1',222,'Bill To Address1','BILL_TO_ADDRESS1','','','','DM027741','VARCHAR2','','','Bill To Address1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ADDRESS2',222,'Bill To Address2','BILL_TO_ADDRESS2','','','','DM027741','VARCHAR2','','','Bill To Address2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ADDRESS3',222,'Bill To Address3','BILL_TO_ADDRESS3','','','','DM027741','VARCHAR2','','','Bill To Address3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ADDRESS4',222,'Bill To Address4','BILL_TO_ADDRESS4','','','','DM027741','VARCHAR2','','','Bill To Address4','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_CITY',222,'Bill To City','BILL_TO_CITY','','','','DM027741','VARCHAR2','','','Bill To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_CITY_PROVINCE',222,'Bill To City Province','BILL_TO_CITY_PROVINCE','','','','DM027741','VARCHAR2','','','Bill To City Province','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_COUNTRY',222,'Bill To Country','BILL_TO_COUNTRY','','','','DM027741','VARCHAR2','','','Bill To Country','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_ZIP_CODE',222,'Bill To Zip Code','BILL_TO_ZIP_CODE','','','','DM027741','VARCHAR2','','','Bill To Zip Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_PO_NUMBER',222,'Customer Po Number','CUSTOMER_PO_NUMBER','','','','DM027741','VARCHAR2','','','Customer Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','PMT_STATUS',222,'Pmt Status','PMT_STATUS','','','','DM027741','VARCHAR2','','','Pmt Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','AMOUNT_PAID',222,'Amount Paid','AMOUNT_PAID','','','','DM027741','NUMBER','','','Amount Paid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CREDIT_LIMIT',222,'Credit Limit','CREDIT_LIMIT','','','','DM027741','NUMBER','','','Credit Limit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','IN_PROCESS',222,'In Process','IN_PROCESS','','','','DM027741','VARCHAR2','','','In Process','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','LARGEST_BALANCE',222,'Largest Balance','LARGEST_BALANCE','','','','DM027741','NUMBER','','','Largest Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','PHONE_NUMBER',222,'Phone Number','PHONE_NUMBER','','','','DM027741','VARCHAR2','','','Phone Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TERMS',222,'Terms','TERMS','','','','DM027741','VARCHAR2','','','Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TERRITORY',222,'Territory','TERRITORY','','','','DM027741','VARCHAR2','','','Territory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TWELVE_MONTHS_SALES',222,'Twelve Months Sales','TWELVE_MONTHS_SALES','','','','DM027741','NUMBER','','','Twelve Months Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','ACCOUNT_STATUS',222,'Account Status','ACCOUNT_STATUS','','','','DM027741','VARCHAR2','','','Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_ADDRESS1',222,'Trx Bill To Address1','TRX_BILL_TO_ADDRESS1','','','','DM027741','VARCHAR2','','','Trx Bill To Address1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_ADDRESS2',222,'Trx Bill To Address2','TRX_BILL_TO_ADDRESS2','','','','DM027741','VARCHAR2','','','Trx Bill To Address2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_ADDRESS3',222,'Trx Bill To Address3','TRX_BILL_TO_ADDRESS3','','','','DM027741','VARCHAR2','','','Trx Bill To Address3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_ADDRESS4',222,'Trx Bill To Address4','TRX_BILL_TO_ADDRESS4','','','','DM027741','VARCHAR2','','','Trx Bill To Address4','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_CITY',222,'Trx Bill To City','TRX_BILL_TO_CITY','','','','DM027741','VARCHAR2','','','Trx Bill To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_CITY_PROV',222,'Trx Bill To City Prov','TRX_BILL_TO_CITY_PROV','','','','DM027741','VARCHAR2','','','Trx Bill To City Prov','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_COUNTRY',222,'Trx Bill To Country','TRX_BILL_TO_COUNTRY','','','','DM027741','VARCHAR2','','','Trx Bill To Country','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_ADDRESS',222,'Trx Address','TRX_ADDRESS','','','','DM027741','VARCHAR2','','','Trx Address','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','ACRA_CCID',222,'Acra Ccid','ACRA_CCID','','','','DM027741','NUMBER','','','Acra Ccid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_PARTY_SITE_NAME',222,'Bill To Party Site Name','BILL_TO_PARTY_SITE_NAME','','','','DM027741','VARCHAR2','','','Bill To Party Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_PARTY_SITE_NUMBER',222,'Bill To Party Site Number','BILL_TO_PARTY_SITE_NUMBER','','','','DM027741','VARCHAR2','','','Bill To Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_PRISM_SITE_NUMBER',222,'Bill To Prism Site Number','BILL_TO_PRISM_SITE_NUMBER','','','','DM027741','VARCHAR2','','','Bill To Prism Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_SITE_NAME',222,'Bill To Site Name','BILL_TO_SITE_NAME','','','','DM027741','VARCHAR2','','','Bill To Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','BILL_TO_SITE_USE_ID',222,'Bill To Site Use Id','BILL_TO_SITE_USE_ID','','','','DM027741','NUMBER','','','Bill To Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CASH_RECEIPT_ID',222,'Cash Receipt Id','CASH_RECEIPT_ID','','','','DM027741','NUMBER','','','Cash Receipt Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','DM027741','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','DM027741','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','PAYMENT_SCHEDULE_ID',222,'Payment Schedule Id','PAYMENT_SCHEDULE_ID','','','','DM027741','NUMBER','','','Payment Schedule Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','RCTA_CCID',222,'Rcta Ccid','RCTA_CCID','','','','DM027741','NUMBER','','','Rcta Ccid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','REMIT_TO_ADDRESS_CODE',222,'Remit To Address Code','REMIT_TO_ADDRESS_CODE','','','','DM027741','VARCHAR2','','','Remit To Address Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','SEND_CREDIT_BAL_FLAG',222,'Send Credit Bal Flag','SEND_CREDIT_BAL_FLAG','','','','DM027741','VARCHAR2','','','Send Credit Bal Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','SEND_STATEMENT_FLAG',222,'Send Statement Flag','SEND_STATEMENT_FLAG','','','','DM027741','VARCHAR2','','','Send Statement Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','STMT_BY_JOB',222,'Stmt By Job','STMT_BY_JOB','','','','DM027741','VARCHAR2','','','Stmt By Job','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_SITE_USE_ID',222,'Trx Bill Site Use Id','TRX_BILL_SITE_USE_ID','','','','DM027741','NUMBER','','','Trx Bill Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_CUSTOMER_ID',222,'Trx Customer Id','TRX_CUSTOMER_ID','','','','DM027741','NUMBER','','','Trx Customer Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_NUMBER',222,'Trx Number','TRX_NUMBER','','','','DM027741','VARCHAR2','','','Trx Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_PARTY_SITE_NAME',222,'Trx Party Site Name','TRX_PARTY_SITE_NAME','','','','DM027741','VARCHAR2','','','Trx Party Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','AVERAGE_DAYS',222,'Average Days','AVERAGE_DAYS','','','','DM027741','NUMBER','','','Average Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_BAD_EXP_V','TRX_BILL_TO_ZIP_CODE',222,'Trx Bill To Zip Code','TRX_BILL_TO_ZIP_CODE','','','','DM027741','VARCHAR2','','','Trx Bill To Zip Code','','','');
--Inserting View Components for EIS_XXWC_AR_AGING_BAD_EXP_V
--Inserting View Component Joins for EIS_XXWC_AR_AGING_BAD_EXP_V
END;
/
set scan on define on
prompt Creating Report LOV Data for XXWC AR Escheatment Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - XXWC AR Escheatment Report
xxeis.eis_rs_ins.lov( 222,'select distinct state from hz_locations','','Customer State','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select account_name,account_number from hz_cust_accounts','','AR Customer Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for XXWC AR Escheatment Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - XXWC AR Escheatment Report
xxeis.eis_rs_utility.delete_report_rows( 'XXWC AR Escheatment Report' );
--Inserting Report - XXWC AR Escheatment Report
xxeis.eis_rs_ins.r( 222,'XXWC AR Escheatment Report','','','','','','DM027741','EIS_XXWC_AR_AGING_BAD_EXP_V','Y','','','DM027741','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - XXWC AR Escheatment Report
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'TRX_BILL_TO_SITE_NAME','Site Name','Trx Bill To Site Name','','','default','','4','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'TRX_DATE','Invoice Date','Trx Date','','','default','','8','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'TRX_PARTY_SITE_NUMBER','Site Number','Trx Party Site Number','','','default','','3','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'TRX_TYPE','Transaction Type','Trx Type','','','default','','6','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'TRANSACTION_REMAINING_BALANCE','Transaction Remaining Balance','Transaction Remaining Balance','','~T~D~2','default','','12','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'BRANCH_LOCATION','Branch Location','Branch Location','','','default','','5','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'CUSTOMER_ACCOUNT_NUMBER','Customer Number','Customer Account Number','','','default','','2','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','1','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'ACCOUNT_BALANCE','Account Total Balance','Account Balance','','~T~D~2','default','','13','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'DUE_DATE','Due Date','Due Date','','','default','','9','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','7','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'LAST_PAYMENT_DATE','Last Payment Date','Last Payment Date','','','default','','10','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'BILL_TO_ADDRESS1','Bill To Address1','Bill To Address1','','','','','14','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'BILL_TO_ADDRESS2','Bill To Address2','Bill To Address2','','','','','15','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'BILL_TO_ADDRESS3','Bill To Address3','Bill To Address3','','','','','16','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'BILL_TO_ADDRESS4','Bill To Address4','Bill To Address4','','','','','17','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'BILL_TO_CITY','Bill To City','Bill To City','','','','','18','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'BILL_TO_CITY_PROVINCE','Bill To City Province','Bill To City Province','','','','','19','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'BILL_TO_ZIP_CODE','Bill To Zip Code','Bill To Zip Code','','','','','20','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
xxeis.eis_rs_ins.rc( 'XXWC AR Escheatment Report',222,'TRANSATION_BALANCE','Transation Balance','Transation Balance','','','','','11','N','','','','','','','','DM027741','N','N','','EIS_XXWC_AR_AGING_BAD_EXP_V','','');
--Inserting Report Parameters - XXWC AR Escheatment Report
xxeis.eis_rs_ins.rp( 'XXWC AR Escheatment Report',222,'Customer Name','Customer Name','CUSTOMER_NAME','IN','AR Customer Name LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC AR Escheatment Report',222,'Bill To City Province','Bill To City Province','BILL_TO_CITY_PROVINCE','IN','Customer State','','VARCHAR2','N','Y','3','','Y','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( 'XXWC AR Escheatment Report',222,'Last Invoice (Sales) Date','Invoice (Sales) Date','TRX_DATE','<=','','','DATE','Y','Y','2','','Y','CONSTANT','DM027741','Y','N','','','');
--Inserting Report Conditions - XXWC AR Escheatment Report
xxeis.eis_rs_ins.rcn( 'XXWC AR Escheatment Report',222,'EXAABEV.TRANSACTION_REMAINING_BALANCE','<','0','','','Y','4','N','DM027741');
xxeis.eis_rs_ins.rcn( 'XXWC AR Escheatment Report',222,'CUSTOMER_NAME','IN',':Customer Name','','','Y','1','Y','DM027741');
xxeis.eis_rs_ins.rcn( 'XXWC AR Escheatment Report',222,'BILL_TO_CITY_PROVINCE','IN',':Bill To City Province','','','Y','3','Y','DM027741');
xxeis.eis_rs_ins.rcn( 'XXWC AR Escheatment Report',222,'TRX_DATE','<=',':Last Invoice (Sales) Date','','','Y','2','Y','DM027741');
--Inserting Report Sorts - XXWC AR Escheatment Report
xxeis.eis_rs_ins.rs( 'XXWC AR Escheatment Report',222,'TRX_DATE','ASC','DM027741','','');
xxeis.eis_rs_ins.rs( 'XXWC AR Escheatment Report',222,'TRX_PARTY_SITE_NUMBER','ASC','DM027741','','');
xxeis.eis_rs_ins.rs( 'XXWC AR Escheatment Report',222,'CUSTOMER_NAME','ASC','DM027741','','');
--Inserting Report Triggers - XXWC AR Escheatment Report
--Inserting Report Templates - XXWC AR Escheatment Report
--Inserting Report Portals - XXWC AR Escheatment Report
--Inserting Report Dashboards - XXWC AR Escheatment Report
xxeis.eis_rs_ins.r_dash( 'XXWC AR Escheatment Report','Dynamic 668','Dynamic 668','absolute line','large','Account Total Balance','Account Total Balance','Account Total Balance','Account Total Balance','Avg','DM027741');
xxeis.eis_rs_ins.r_dash( 'XXWC AR Escheatment Report','Dynamic 665','Dynamic 665','absolute line','large','Last Payment Date','Last Payment Date','Account Total Balance','Account Total Balance','Avg','DM027741');
--Inserting Report Security - XXWC AR Escheatment Report
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','222','','20678',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','222','','51208',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','222','','50993',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','222','','50854',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','222','','50879',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','222','','50877',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','222','','50878',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','222','','23917',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','222','','23916',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'XXWC AR Escheatment Report','','MW014115','',222,'DM027741','','');
--Inserting Report Pivots - XXWC AR Escheatment Report
END;
/
set scan on define on
