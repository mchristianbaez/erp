--Report Name            : Cycle Count Adjustments - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_INV_CYCLE_CNT_ADJ_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_CYCLE_CNT_ADJ_V
Create or replace View XXEIS.EIS_XXWC_INV_CYCLE_CNT_ADJ_V
 AS 
SELECT msi.concatenated_segments item
         ,REPLACE (msi.description, '~', '-') item_description -- Added by Mahender For TMS#20150116-00084 on 05/18/15
         --,REPLACE (msil.description, '~', '-') item_description -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
         ,mmt.subinventory_code subinventory
         ,TRUNC (mmt.transaction_date) transaction_date
         ,                                                            --mac.description abc_class_description,
          --mac.abc_class_name abc_class_name,
          --mmt.creation_date creation_date,
          --mcch.cycle_count_header_name cycle_count_name,
          --oap.period_name gl_period,
          --oap.period_start_date gl_period_start_date,
          mmt.actual_cost item_cost
         ,                                                                       --mmt.revision item_revision,
          --mil.concatenated_segments stock_locators,
          --mmt.new_cost new_cost,
          --pp.name project_name,
          --pp.segment1 project_number,
          mmt.primary_quantity quantity
         ,ROUND ( (mmt.primary_quantity * mmt.actual_cost), fnd_profile.VALUE ('REPORT_QUANTITY_PRECISION'))
             VALUE
         ,                                                                                    --mtr.reason_id,
          --mtr.reason_name reason,
          --pt.task_name task_name,
          --pt.task_number task_number,
          --xxeis.EIS_INV_UTILITY_PKG.get_lookup_meaning('MTL_TRANSACTION_ACTION',MMT.TRANSACTION_ACTION_ID) Transaction_Action,
          --mmt.transaction_reference transaction_reference,
          --mts.transaction_source_type_name,
          mtt.transaction_type_name transaction_type
         ,                                                      --msi.primary_unit_of_measure unit_of_measure,
          --decode(mmt.costed_flag,'N','No',nvl(mmt.costed_flag,'Yes')) valued_flag,
          --mcch.cycle_count_header_name,
          --mtt.transaction_type_id,
          --mil.inventory_location_id,
          --mcci.cycle_count_header_id,
          --mac.abc_class_id,
          --pp.project_id,
          --pt.task_id,
          --oap.acct_period_id,
          --msi.inventory_item_id,
          --msi.organization_id,
          haou.organization_name inv_org_name
         ,haou.organization_code inv_org_code            --Added by Mahender for TMS#40314-00007 on 09/25/2014
         ,mmt.transaction_id
         ,xxeis.eis_inv_utility_pkg.get_fnd_user_name (mmt.created_by) created_by_user
         ,                                                                        --mmt.transaction_action_id,
                                                                                  --mmt.transaction_source_id,
          --mmt.department_id,
          --mmt.error_explanation,
          --mmt.vendor_lot_number supplier_lot,
          --mmt.source_line_id,
          --mmt.parent_transaction_id,
          --mmt.shipment_number shipment_number,
          --mmt.waybill_airbill waybill_airbill,
          --mmt.freight_code freight_code,
          --mmt.number_of_containers,
          --mmt.rcv_transaction_id,
          --mmt.move_transaction_id,
          --mmt.completion_transaction_id,
          --mmt.operation_seq_num opertion_sequence,
          --mmt.expenditure_type ,
          --mmt.transaction_set_id mmt_transaction_set_id ,
          --mmt.transaction_source_name ,
          mmt.transaction_uom uom
         ,                                                                       --mmt.transfer_subinventory ,
          --mmt.transfer_transaction_id mmt_transfer_transaction_id,
          --mil.organization_id mil_organization_id,
          --mcch.cycle_count_header_id mcch_cycle_count_header_id,
          --mcci.inventory_item_id mcci_inventory_item_id,
          --oap.organization_id oap_organization_id,
          --msil.inventory_item_id msil_inventory_item_id,
          --msil.organization_id msil_organization_id,
          --msil.language,
          --mp.organization_id mp_organization_id,
          --haou.organization_id haou_organization_id,
          --mts.transaction_source_type_id mts_transaction_source_type_id,
          xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class (msi.inventory_item_id, msi.organization_id)
             cat_class
         ,mp.organization_code org
         ,gcc.segment4 gl_account
         ,                                                                             --descr#flexfield#start
          DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', mmt.attribute1, 'I')
            ,NULL)
             mmt#wcrentala#home_branch
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute10, NULL)
             mmt#wcrentala#vendor
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute11, NULL)
             mmt#wcrentala#po_number
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute12, NULL)
             mmt#wcrentala#item_number
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute13, NULL)
             mmt#wcrentala#cer_number
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute14, NULL)
             mmt#wcrentala#misc_issue_tra
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', mmt.attribute2, 'I')
            ,NULL)
             mmt#wcrentala#custodian_bran
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FA_MAJOR_CATEGORY'
                                                                          ,mmt.attribute3
                                                                          ,'I')
            ,NULL)
             mmt#wcrentala#major_category
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute4, NULL)
             mmt#wcrentala#minor_category
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FA_STATE'
                                                                          ,mmt.attribute5
                                                                          ,'I')
            ,NULL)
             mmt#wcrentala#state
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FA_CITY'
                                                                          ,mmt.attribute6
                                                                          ,'I')
            ,NULL)
             mmt#wcrentala#city
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('Yes_No', mmt.attribute9, 'F')
            ,NULL)
             mmt#wcrentala#transfer_to_ma
         ,DECODE (
             mmt.attribute_category
            ,'WC Return to Vendor', xxeis.eis_rs_dff.decode_valueset ('XXWC_INV_SUPPLIER_NUMBER'
                                                                     ,mmt.attribute1
                                                                     ,'F')
            ,NULL)
             mmt#wcreturnt#supplier_numbe
         ,DECODE (mmt.attribute_category, 'WC Return to Vendor', mmt.attribute2, NULL)
             mmt#wcreturnt#return_number
         ,DECODE (mmt.attribute_category, 'WC Return to Vendor', mmt.attribute3, NULL)
             mmt#wcreturnt#return_unit_pr
         ,mp.attribute1 mp#factory_planner_data_dire
         ,mp.attribute10 mp#fru
         ,mp.attribute11 mp#location_number
         ,xxeis.eis_rs_dff.decode_valueset ('HR_DE_EMPLOYEES', mp.attribute13, 'F')
             mp#branch_operations_manager
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DELIVERY_CHARGE_EXEMPT', mp.attribute14, 'I')
             mp#deliver_charge
         ,mp.attribute2 mp#factory_planner_executabl
         ,mp.attribute3 mp#factory_planner_user
         ,mp.attribute4 mp#factory_planner_host
         ,mp.attribute5 mp#factory_planner_port_numb
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PRICE_ZONES', mp.attribute6, 'F') mp#pricing_zone
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_ORG_TYPE', mp.attribute7, 'I') mp#org_type
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DISTRICT', mp.attribute8, 'I') mp#district
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_REGION', mp.attribute9, 'I') mp#region
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob
         ,DECODE (msi.attribute_category
                 ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F')
                 ,NULL)
             msi#hds#drop_shipment_eligab
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL) msi#hds#sku_description
         ,DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_65
         ,DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F')
                 ,NULL)
             msi#wc#orm_d_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number
         ,DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I')
                 ,NULL)
             msi#wc#hazmat_container
         ,DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator
         ,DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu
         ,DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I')
                 ,NULL)
             msi#wc#taxware_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units
         ,DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F')
                 ,NULL)
             msi#wc#keep_item_active
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F')
                 ,NULL)
             msi#wc#pesticide_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl
         ,DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state
         ,DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_#
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I')
                 ,NULL)
             msi#wc#hazmat_packaging_grou
     --descr#flexfield#end
     --kff#start
     --,
     --MIL.SEGMENT1 "MIL#101#STOCKLOCATIONS" ,
     --xxeis.eis_rs_dff.decode_valueset('XXWC_STOCK_LOCATIONS',MIL.SEGMENT1,'N') "MIL#101#STOCKLOCATIONS_DESC"
     --kff#end
     FROM mtl_cycle_count_headers mcch
         ,mtl_transaction_types mtt
         --,                                                                       --mtl_item_locations_kfv mil,
         --mtl_cycle_count_items mcci,
         --mtl_abc_classes mac,
         --pa_tasks pt,
         --pa_projects pp,
         --org_acct_periods oap,
         -- mtl_system_items_tl msil  -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
         ,mtl_system_items_kfv msi
         ,mtl_material_transactions mmt
         ,mtl_parameters mp
         ,gl_code_combinations_kfv gcc
         ,                                                                      --mtl_transaction_reasons mtr,
          org_organization_definitions haou
    --mtl_txn_source_types mts
    WHERE --msi.organization_id = mp.organization_id -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
         mmt  .organization_id = mp.organization_id
          AND mmt.organization_id = msi.organization_id -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          AND mmt.inventory_item_id = msi.inventory_item_id
          AND mmt.transaction_source_type_id = 9
          --AND mcci.cycle_count_header_id (+) = mmt.transaction_source_id
          --AND mcci.inventory_item_id (+)     = mmt.inventory_item_id
          --AND mac.abc_class_id (+)           = mcci.abc_class_id
          --AND oap.organization_id (+)        = mmt.organization_id
          --AND oap.acct_period_id (+)         = mmt.acct_period_id
          --AND msil.inventory_item_id(+) = msi.inventory_item_id   -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
          --AND msil.organization_id(+) = msi.organization_id   -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
          --AND msil.language(+) = USERENV ('LANG')             -- Commented by Mahender For TMS#20150116-00084 on 05/18/15
          --and mmt.project_id                 = pp.project_id (+)
          --and mmt.task_id                    = pt.task_id (+)
          --AND mmt.locator_id                 = mil.inventory_location_id(+)
          --AND mil.organization_id (+)        = mmt.organization_id
          AND mmt.transaction_type_id = mtt.transaction_type_id(+)
          AND mmt.transaction_source_id = mcch.cycle_count_header_id(+)
          AND mcch.organization_id(+) = mmt.organization_id
          --AND mtr.reason_id (+)              = mmt.reason_id
          AND haou.organization_id = msi.organization_id
          AND mcch.inventory_adjustment_account = gcc.code_combination_id(+)
          --AND mts.transaction_source_type_id = mmt.transaction_source_type_id
          --          AND TRUNC(mmt.transaction_date) >= TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)  -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          --          AND TRUNC(mmt.transaction_date) <= TRUNC (xxeis.eis_rs_xxwc_com_util_pkg.get_date_to)  -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          AND mmt.transaction_date >=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_from
                              ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                     ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 0.25                               -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          AND mmt.transaction_date <=
                   TO_DATE (
                      TO_CHAR (xxeis.eis_rs_xxwc_com_util_pkg.get_date_to
                              ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                     ,xxeis.eis_rs_utility.get_date_format || ' HH24:MI:SS')
                 + 1.25                                -- Added by Mahender For TMS#20150116-00084 on 05/18/15
          AND EXISTS
                 (SELECT 1
                    FROM xxeis.eis_org_access_v
                   WHERE organization_id = msi.organization_id)/
set scan on define on
prompt Creating View Data for Cycle Count Adjustments - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_CYCLE_CNT_ADJ_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V',401,'Cycle Count Adjustment Transactions','','','','MR020532','XXEIS','Eis Inv Cyclecount Trxs V','EICTV','','');
--Delete View Columns for EIS_XXWC_INV_CYCLE_CNT_ADJ_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_CYCLE_CNT_ADJ_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_CYCLE_CNT_ADJ_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','CREATED_BY_USER',401,'Created By User','CREATED_BY_USER','','','','MR020532','VARCHAR2','','','Created By User','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','MR020532','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','SUBINVENTORY',401,'Subinventory','SUBINVENTORY','','','','MR020532','VARCHAR2','','','Subinventory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','TRANSACTION_ID',401,'Transaction Id','TRANSACTION_ID','','','','MR020532','NUMBER','','','Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','TRANSACTION_TYPE',401,'Transaction Type','TRANSACTION_TYPE','','','','MR020532','VARCHAR2','','','Transaction Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','ITEM',401,'Item','ITEM','','','','MR020532','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','QUANTITY',401,'Quantity','QUANTITY','','','','MR020532','NUMBER','','','Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','TRANSACTION_DATE',401,'Transaction Date','TRANSACTION_DATE','','','','MR020532','DATE','','','Transaction Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','INV_ORG_NAME',401,'Inv Org Name','INV_ORG_NAME','','','','MR020532','VARCHAR2','','','Inv Org Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','CAT_CLASS',401,'Cat Class','CAT_CLASS','','','','MR020532','VARCHAR2','','','Cat Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','ITEM_COST',401,'Item Cost','ITEM_COST','','','','MR020532','NUMBER','','','Item Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','UOM',401,'Uom','UOM','','','','MR020532','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','VALUE',401,'Value','VALUE','','','','MR020532','NUMBER','','','Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','GL_ACCOUNT',401,'Gl Account','GL_ACCOUNT','','','','MR020532','VARCHAR2','','','Gl Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','INV_ORG_CODE',401,'Inv Org Code','INV_ORG_CODE','','','','MR020532','VARCHAR2','','','Inv Org Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#HOME_BRANCH',401,'Descriptive flexfield: Transaction history Column Name: Home Branch Context: WC Rental Asset Transfer','MMT#WCRENTALA#HOME_BRANCH','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE1','Mmt#Wc Rental Asset Transfer#Home Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#VENDOR',401,'Descriptive flexfield: Transaction history Column Name: Vendor Context: WC Rental Asset Transfer','MMT#WCRENTALA#VENDOR','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE10','Mmt#Wc Rental Asset Transfer#Vendor','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#PO_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: PO Number Context: WC Rental Asset Transfer','MMT#WCRENTALA#PO_NUMBER','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE11','Mmt#Wc Rental Asset Transfer#Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#ITEM_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: Item Number Context: WC Rental Asset Transfer','MMT#WCRENTALA#ITEM_NUMBER','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE12','Mmt#Wc Rental Asset Transfer#Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#CER_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: CER Number Context: WC Rental Asset Transfer','MMT#WCRENTALA#CER_NUMBER','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE13','Mmt#Wc Rental Asset Transfer#Cer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#MISC_ISSUE_TRA',401,'Descriptive flexfield: Transaction history Column Name: Misc Issue Transaction ID Context: WC Rental Asset Transfer','MMT#WCRENTALA#MISC_ISSUE_TRA','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE14','Mmt#Wc Rental Asset Transfer#Misc Issue Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#CUSTODIAN_BRAN',401,'Descriptive flexfield: Transaction history Column Name: Custodian Branch Context: WC Rental Asset Transfer','MMT#WCRENTALA#CUSTODIAN_BRAN','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE2','Mmt#Wc Rental Asset Transfer#Custodian Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#MAJOR_CATEGORY',401,'Descriptive flexfield: Transaction history Column Name: Major Category Context: WC Rental Asset Transfer','MMT#WCRENTALA#MAJOR_CATEGORY','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE3','Mmt#Wc Rental Asset Transfer#Major Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#MINOR_CATEGORY',401,'Descriptive flexfield: Transaction history Column Name: Minor Category Context: WC Rental Asset Transfer','MMT#WCRENTALA#MINOR_CATEGORY','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE4','Mmt#Wc Rental Asset Transfer#Minor Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#STATE',401,'Descriptive flexfield: Transaction history Column Name: State Context: WC Rental Asset Transfer','MMT#WCRENTALA#STATE','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE5','Mmt#Wc Rental Asset Transfer#State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#CITY',401,'Descriptive flexfield: Transaction history Column Name: City Context: WC Rental Asset Transfer','MMT#WCRENTALA#CITY','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE6','Mmt#Wc Rental Asset Transfer#City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRENTALA#TRANSFER_TO_MA',401,'Descriptive flexfield: Transaction history Column Name: Transfer to MassAdd Context: WC Rental Asset Transfer','MMT#WCRENTALA#TRANSFER_TO_MA','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE9','Mmt#Wc Rental Asset Transfer#Transfer To Massadd','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRETURNT#SUPPLIER_NUMBE',401,'Descriptive flexfield: Transaction history Column Name: Supplier Number Context: WC Return to Vendor','MMT#WCRETURNT#SUPPLIER_NUMBE','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE1','Mmt#Wc Return To Vendor#Supplier Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRETURNT#RETURN_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: Return Number Context: WC Return to Vendor','MMT#WCRETURNT#RETURN_NUMBER','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE2','Mmt#Wc Return To Vendor#Return Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MMT#WCRETURNT#RETURN_UNIT_PR',401,'Descriptive flexfield: Transaction history Column Name: Return Unit Price Context: WC Return to Vendor','MMT#WCRETURNT#RETURN_UNIT_PR','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE3','Mmt#Wc Return To Vendor#Return Unit Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#FACTORY_PLANNER_DATA_DIRE',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Data Directory','MP#FACTORY_PLANNER_DATA_DIRE','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE1','Mp#Factory Planner Data Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#FRU',401,'Descriptive flexfield: Organization parameters Column Name: FRU','MP#FRU','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE10','Mp#Fru','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#LOCATION_NUMBER',401,'Descriptive flexfield: Organization parameters Column Name: Location Number','MP#LOCATION_NUMBER','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE11','Mp#Location Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#BRANCH_OPERATIONS_MANAGER',401,'Descriptive flexfield: Organization parameters Column Name: Branch Operations Manager','MP#BRANCH_OPERATIONS_MANAGER','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE13','Mp#Branch Operations Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#DELIVER_CHARGE',401,'Descriptive flexfield: Organization parameters Column Name: Deliver Charge','MP#DELIVER_CHARGE','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE14','Mp#Deliver Charge','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#FACTORY_PLANNER_EXECUTABL',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Executable Directory','MP#FACTORY_PLANNER_EXECUTABL','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE2','Mp#Factory Planner Executable Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#FACTORY_PLANNER_USER',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner User','MP#FACTORY_PLANNER_USER','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE3','Mp#Factory Planner User','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#FACTORY_PLANNER_HOST',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Host','MP#FACTORY_PLANNER_HOST','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE4','Mp#Factory Planner Host','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#FACTORY_PLANNER_PORT_NUMB',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Port Number','MP#FACTORY_PLANNER_PORT_NUMB','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE5','Mp#Factory Planner Port Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#PRICING_ZONE',401,'Descriptive flexfield: Organization parameters Column Name: Pricing Zone','MP#PRICING_ZONE','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE6','Mp#Pricing Zone','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#ORG_TYPE',401,'Descriptive flexfield: Organization parameters Column Name: Org Type','MP#ORG_TYPE','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE7','Mp#Org Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#DISTRICT',401,'Descriptive flexfield: Organization parameters Column Name: District','MP#DISTRICT','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE8','Mp#District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MP#REGION',401,'Descriptive flexfield: Organization parameters Column Name: Region','MP#REGION','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE9','Mp#Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#HDS#LOB',401,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',401,'Descriptive flexfield: Items Column Name: Drop Shipment Eligable Context: HDS','MSI#HDS#DROP_SHIPMENT_ELIGAB','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment Eligable','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#HDS#INVOICE_UOM',401,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#INVOICE_UOM','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#HDS#PRODUCT_ID',401,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#PRODUCT_ID','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#HDS#VENDOR_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#VENDOR_PART_NUMBER','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#HDS#UNSPSC_CODE',401,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_CODE','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#HDS#UPC_PRIMARY',401,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_PRIMARY','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#HDS#SKU_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_DESCRIPTION','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#CA_PROP_65',401,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_PROP_65','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#COUNTRY_OF_ORIGIN',401,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#COUNTRY_OF_ORIGIN','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#ORM_D_FLAG',401,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_FLAG','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#STORE_VELOCITY','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_VELOCITY','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#YEARLY_STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#YEARLY_STORE_VELOCITY','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#YEARLY_DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#YEARLY_DC_VELOCITY','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#PRISM_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_PART_NUMBER','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#HAZMAT_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#HAZMAT_DESCRIPTION','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#HAZMAT_CONTAINER',401,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#HAZMAT_CONTAINER','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#GTP_INDICATOR',401,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_INDICATOR','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#LAST_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#LAST_LEAD_TIME','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#AMU',401,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#RESERVE_STOCK',401,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#RESERVE_STOCK','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#TAXWARE_CODE',401,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#TAXWARE_CODE','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#AVERAGE_UNITS',401,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#AVERAGE_UNITS','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#PRODUCT_CODE',401,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#PRODUCT_CODE','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#IMPORT_DUTY_',401,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#IMPORT_DUTY_','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#KEEP_ITEM_ACTIVE',401,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#KEEP_ITEM_ACTIVE','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#PESTICIDE_FLAG',401,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#PESTICIDE_FLAG','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#CALC_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#CALC_LEAD_TIME','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#VOC_GL',401,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#PESTICIDE_FLAG_STATE',401,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#PESTICIDE_FLAG_STATE','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#VOC_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_CATEGORY','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#VOC_SUB_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_SUB_CATEGORY','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#MSDS_#',401,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MSI#WC#HAZMAT_PACKAGING_GROU',401,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#HAZMAT_PACKAGING_GROU','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','ORG',401,'Org','ORG','','','','MR020532','VARCHAR2','','','Org','','','');
--Inserting View Components for EIS_XXWC_INV_CYCLE_CNT_ADJ_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_CYCLE_COUNT_ITEMS',401,'MTL_CYCLE_COUNT_ITEMS','MCCI','MCCI','MR020532','MR020532','-1','Defines Items To Be Used In Cycle Count','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_ABC_CLASSES',401,'MTL_ABC_CLASSES','MAC','MAC','MR020532','MR020532','-1','Inventory Abc Classes','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','PA_TASKS',401,'PA_TASKS','PT','PT','MR020532','MR020532','-1','User-Defined Subdivisions Of Project Work','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','PA_PROJECTS',401,'PA_PROJECTS_ALL','PP','PP','MR020532','MR020532','-1','Information About Projects','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','ORG_ACCT_PERIODS',401,'ORG_ACCT_PERIODS','OAP','OAP','MR020532','MR020532','-1','Organization Accounting Period Definition Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_MATERIAL_TRANSACTIONS',401,'MTL_MATERIAL_TRANSACTIONS','MMT','MMT','MR020532','MR020532','-1','Material Transaction Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_PARAMETERS',401,'MTL_PARAMETERS','MP','MP','MR020532','MR020532','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_TRANSACTION_REASONS',401,'MTL_TRANSACTION_REASONS','MTR','MTR','MR020532','MR020532','-1','Inventory Transaction Reasons Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_TXN_SOURCE_TYPES',401,'MTL_TXN_SOURCE_TYPES','MTS','MTS','MR020532','MR020532','-1','Valid Transaction Source Types','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_ITEM_LOCATIONS_KFV',401,'MTL_ITEM_LOCATIONS','MIL','MIL','MR020532','MR020532','-1','Item Locations','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','MR020532','MR020532','-1','Organization Defination','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','MR020532','MR020532','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_CYCLE_COUNT_HEADERS',401,'MTL_CYCLE_COUNT_HEADERS','MCCH','MCCH','MR020532','MR020532','-1','Defines Cycle Count Header Information','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_TRANSACTION_TYPES',401,'MTL_TRANSACTION_TYPES','MTT','MTT','MR020532','MR020532','-1','Inventory Transaction Types Table','','','','');
--Inserting View Component Joins for EIS_XXWC_INV_CYCLE_CNT_ADJ_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_MATERIAL_TRANSACTIONS','MMT',401,'EICTV.TRANSACTION_ID','=','MMT.TRANSACTION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_PARAMETERS','MP',401,'EICTV.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_TRANSACTION_REASONS','MTR',401,'EICTV.REASON_ID','=','MTR.REASON_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_TXN_SOURCE_TYPES','MTS',401,'EICTV.MTS_TRANSACTION_SOURCE_TYPE_ID','=','MTS.TRANSACTION_SOURCE_TYPE_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EICTV.MIL_ORGANIZATION_ID','=','MIL.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EICTV.INVENTORY_LOCATION_ID','=','MIL.INVENTORY_LOCATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','HR_ORGANIZATION_UNITS','HAOU',401,'EICTV.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EICTV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EICTV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_CYCLE_COUNT_HEADERS','MCCH',401,'EICTV.MCCH_CYCLE_COUNT_HEADER_ID','=','MCCH.CYCLE_COUNT_HEADER_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_TRANSACTION_TYPES','MTT',401,'EICTV.TRANSACTION_TYPE_ID','=','MTT.TRANSACTION_TYPE_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_CYCLE_COUNT_ITEMS','MCCI',401,'EICTV.CYCLE_COUNT_HEADER_ID','=','MCCI.CYCLE_COUNT_HEADER_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_CYCLE_COUNT_ITEMS','MCCI',401,'EICTV.MCCI_INVENTORY_ITEM_ID','=','MCCI.INVENTORY_ITEM_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','MTL_ABC_CLASSES','MAC',401,'EICTV.ABC_CLASS_ID','=','MAC.ABC_CLASS_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','PA_TASKS','PT',401,'EICTV.TASK_ID','=','PT.TASK_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','PA_PROJECTS','PP',401,'EICTV.PROJECT_ID','=','PP.PROJECT_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','ORG_ACCT_PERIODS','OAP',401,'EICTV.OAP_ORGANIZATION_ID','=','OAP.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_CYCLE_CNT_ADJ_V','ORG_ACCT_PERIODS','OAP',401,'EICTV.ACCT_PERIOD_ID','=','OAP.ACCT_PERIOD_ID(+)','','','','Y','MR020532','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Cycle Count Adjustments - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.lov( 401,'SELECT   organization_code,organization_name
    FROM org_organization_definitions
   where operating_unit = fnd_profile.value (''ORG_ID'')
union
SELECT ''All Organizations'' organization_code, ''All'' organization_name  FROM dual','','EIS_INV_INVENTORY_ORGS_LOV','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select user_name from fnd_user','','EIS_INV_USER','List of All FND users','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT   transaction_type_name
    FROM mtl_transaction_types
   WHERE transaction_source_type_id = 9
ORDER BY transaction_type_name','','INV_TRANSACTION_TYPES_CYCLE_COUNT','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Cycle Count Adjustments - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Cycle Count Adjustments - WC
xxeis.eis_rs_utility.delete_report_rows( 'Cycle Count Adjustments - WC' );
--Inserting Report - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.r( 401,'Cycle Count Adjustments - WC','','Cycle Count Adjustment Transactions Report displays the inventory transactions that are associated with cycle count adjustments.','','','','MR020532','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'ITEM','Item','Item','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'QUANTITY','Quantity','Quantity','','~,~.~0','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'SUBINVENTORY','Subinventory','Subinventory','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'TRANSACTION_DATE','Transaction Date','Transaction Date','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'TRANSACTION_ID','Transaction Id','Transaction Id','','~~~','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'TRANSACTION_TYPE','Transaction Type','Transaction Type','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'INV_ORG_NAME','Inv Org Name','Inv Org Name','','','','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'CAT_CLASS','Cat Class','Cat Class','','','','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'CREATED_BY_USER','Created By User','Created By User','','','','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'ITEM_COST','Item Cost','Item Cost','','','','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'UOM','Uom','Uom','','','','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'VALUE','Value','Value','','','','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Adjustments - WC',401,'GL_ACCOUNT','Gl Account','Gl Account','','','','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_CYCLE_CNT_ADJ_V','','');
--Inserting Report Parameters - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.rp( 'Cycle Count Adjustments - WC',401,'Created By User','Created By User','CREATED_BY_USER','IN','EIS_INV_USER','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Adjustments - WC',401,'Organization','Organization','INV_ORG_CODE','IN','EIS_INV_INVENTORY_ORGS_LOV','','VARCHAR2','Y','Y','1','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Adjustments - WC',401,'Inventory Item','Inventory Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Adjustments - WC',401,'Subinventory','Subinventory','SUBINVENTORY','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Adjustments - WC',401,'Transaction Start Date','Transaction Start Date','TRANSACTION_DATE','>=','','','DATE','Y','Y','6','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Adjustments - WC',401,'Transaction Type','Transaction Type','TRANSACTION_TYPE','IN','INV_TRANSACTION_TYPES_CYCLE_COUNT','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Adjustments - WC',401,'Transaction End Date','Transaction End Date','TRANSACTION_DATE','<=','','','DATE','Y','Y','7','','N','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.rcn( 'Cycle Count Adjustments - WC',401,'CREATED_BY_USER','IN',':Created By User','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Cycle Count Adjustments - WC',401,'SUBINVENTORY','IN',':Subinventory','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Cycle Count Adjustments - WC',401,'TRANSACTION_TYPE','IN',':Transaction Type','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Cycle Count Adjustments - WC',401,'ITEM','IN',':Inventory Item','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Cycle Count Adjustments - WC',401,'INV_ORG_CODE','IN',':Organization','','','N','2','N','MR020532');
xxeis.eis_rs_ins.rcn( 'Cycle Count Adjustments - WC',401,'','','','','and (''All Organizations'' in (:Organization)) or (INV_ORG_CODE in (:Organization))','Y','0','','MR020532');
--Inserting Report Sorts - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.rs( 'Cycle Count Adjustments - WC',401,'TRANSACTION_TYPE','ASC','MR020532','','');
xxeis.eis_rs_ins.rs( 'Cycle Count Adjustments - WC',401,'ITEM','ASC','MR020532','','');
--Inserting Report Triggers - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.rt( 'Cycle Count Adjustments - WC',401,'Begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_from(:Transaction Start Date);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_to(:Transaction End Date);

end;
','B','Y','MR020532');
--Inserting Report Templates - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.R_Tem( 'Cycle Count Adjustments - WC','Cycle Count Adjustments - WC','Seeded template for Cycle Count Adjustments - WC','','','','','','','','','','','Cycle Count Adjustments - WC.rtf','MR020532');
--Inserting Report Portals - Cycle Count Adjustments - WC
--Inserting Report Dashboards - Cycle Count Adjustments - WC
--Inserting Report Security - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.rsec( 'Cycle Count Adjustments - WC','401','','50884',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Adjustments - WC','401','','50855',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Adjustments - WC','401','','50981',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Adjustments - WC','401','','50882',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Adjustments - WC','401','','50883',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Adjustments - WC','401','','51004',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Adjustments - WC','401','','50619',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Adjustments - WC','201','','50892',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Adjustments - WC','401','','50862',401,'MR020532','','');
--Inserting Report Pivots - Cycle Count Adjustments - WC
xxeis.eis_rs_ins.rpivot( 'Cycle Count Adjustments - WC',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Adjustments - WC',401,'Pivot','ITEM','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Adjustments - WC',401,'Pivot','ITEM_DESCRIPTION','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Adjustments - WC',401,'Pivot','QUANTITY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Adjustments - WC',401,'Pivot','SUBINVENTORY','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Adjustments - WC',401,'Pivot','TRANSACTION_ID','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Adjustments - WC',401,'Pivot','TRANSACTION_TYPE','PAGE_FIELD','','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
