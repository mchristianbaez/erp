grant select on MTL_CYCLE_COUNT_ENTRIES_V  to XXEIS;
grant select on MFG_LOOKUPS   to XXEIS; 
grant select on MTL_ITEM_LOCATIONS  to XXEIS;   
grant select ON mtl_abc_classes  to XXEIS;              
grant select on MTL_CYCLE_COUNT_HEADERS to XXEIS;  
grant select on  MTL_CYCLE_COUNT_ITEMS    to XXEIS;   
grant select ON  mtl_cycle_count_entries   to XXEIS; 
create or replace synonym XXEIS.MTL_CYCLE_COUNT_ENTRIES_V  for APPS.MTL_CYCLE_COUNT_ENTRIES_V ;
create or replace synonym XXEIS.MFG_LOOKUPS  for APPS.MFG_LOOKUPS ;
create or replace synonym XXEIS.MTL_ITEM_LOCATIONS  for APPS.MTL_ITEM_LOCATIONS ;
create or replace synonym XXEIS.MTL_ABC_CLASSES  for APPS.MTL_ABC_CLASSES ;
create or replace synonym XXEIS.MTL_CYCLE_COUNT_HEADERS  for APPS.MTL_CYCLE_COUNT_HEADERS ;
create or replace synonym XXEIS.MTL_CYCLE_COUNT_ITEMS  for APPS.MTL_CYCLE_COUNT_ITEMS ;
create or replace synonym XXEIS.MTL_CYCLE_COUNT_ENTRIES  for APPS.MTL_CYCLE_COUNT_ENTRIES ;
/