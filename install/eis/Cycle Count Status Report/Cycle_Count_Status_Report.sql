--Report Name            : Cycle Count Status Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_CYCLE_STATUS_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_CYCLE_STATUS_V
Create or replace View XXEIS.EIS_XXWC_CYCLE_STATUS_V
 AS 
SELECT SUM( CASE WHEN (CCE.entry_status_code in(2,3))THEN 1 else 0 end) COUNT_NOT_APPROVED,
MP.ATTRIBUTE8 DISTRICT,
MP.ATTRIBUTE9 REGION,
ood.organization_name org,
ood.organization_code org_code,
COUNT(sys.concatenated_segments) count_items
, TRUNC(CCE.COUNT_DATE_FIRST) BEGINING_COUNT_DATE
, TRUNC(CCE.CREATION_DATE) CREATION_DATE
,sys.cycle_count_enabled_flag  ---- Diane add 2/19 Start TMS 20140219-00091
,sys.mtl_transactions_enabled_flag
,sys.stock_enabled_flag  ----- Diane add 2/19 End TMS 20140219-00091
from MTL_CYCLE_COUNT_ENTRIES CCE,
MTL_PARAMETERS MP,
ORG_ORGANIZATION_DEFINITIONS OOD,
MTL_SYSTEM_ITEMS_VL SYS
WHERE 1=1 --and ((cce.entry_status_code = 1) or (CCE.ENTRY_STATUS_CODE = 2) or (CCE.ENTRY_STATUS_CODE = 3) or (CCE.ENTRY_STATUS_CODE = 5))
and ((cce.entry_status_code = 1) or(CCE.ENTRY_STATUS_CODE = 2) or (CCE.ENTRY_STATUS_CODE = 3))
and MP.ORGANIZATION_ID = CCE.ORGANIZATION_ID
AND OOD.ORGANIZATION_ID = CCE.ORGANIZATION_ID
and SYS.INVENTORY_ITEM_ID = CCE.INVENTORY_ITEM_ID
AND SYS.ORGANIZATION_ID= CCE.ORGANIZATION_ID
AND    nvl(sys.cycle_count_enabled_flag,'N') = 'Y'   ----- Diane add 2/19 Start TMS 20140219-00091
AND    nvl(sys.mtl_transactions_enabled_flag,'N') = 'Y'
AND    nvl(sys.stock_enabled_flag,'N') = 'Y'   ------ Diane add 2/19 End TMS 20140219-00091
GROUP BY MP.ATTRIBUTE8,
MP.ATTRIBUTE9,
ood.organization_name,
ood.organization_code,
TRUNC(CCE.COUNT_DATE_FIRST),
TRUNC(CCE.CREATION_DATE)
,sys.cycle_count_enabled_flag   --- Diane add 2/19 Start TMS 20140219-00091
,sys.mtl_transactions_enabled_flag
,sys.stock_enabled_flag    ------ Diane add 2/19 End TMS 20140219-00091
/
set scan on define on
prompt Creating View Data for Cycle Count Status Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_CYCLE_STATUS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_CYCLE_STATUS_V',401,'','','','','DM027741','XXEIS','Eis Xxwc Cycle Status V','EXCSV','','');
--Delete View Columns for EIS_XXWC_CYCLE_STATUS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_CYCLE_STATUS_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_CYCLE_STATUS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','BEGINING_COUNT_DATE',401,'Begining Count Date','BEGINING_COUNT_DATE','','','','DM027741','DATE','','','Begining Count Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','ORG_CODE',401,'Org Code','ORG_CODE','','','','DM027741','VARCHAR2','','','Org Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','REGION',401,'Region','REGION','','','','DM027741','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','DISTRICT',401,'District','DISTRICT','','','','DM027741','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','COUNT_ITEMS',401,'Count Items','COUNT_ITEMS','','','','DM027741','NUMBER','','','Count Items','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','COUNT_NOT_APPROVED',401,'Count Not Approved','COUNT_NOT_APPROVED','','','','DM027741','NUMBER','','','Count Not Approved','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','DM027741','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','ORG',401,'Org','ORG','','','','DM027741','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','CYCLE_COUNT_ENABLED_FLAG',401,'Cycle Count Enabled Flag','CYCLE_COUNT_ENABLED_FLAG','','','','DM027741','VARCHAR2','','','Cycle Count Enabled Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','MTL_TRANSACTIONS_ENABLED_FLAG',401,'Mtl Transactions Enabled Flag','MTL_TRANSACTIONS_ENABLED_FLAG','','','','DM027741','VARCHAR2','','','Mtl Transactions Enabled Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CYCLE_STATUS_V','STOCK_ENABLED_FLAG',401,'Stock Enabled Flag','STOCK_ENABLED_FLAG','','','','DM027741','VARCHAR2','','','Stock Enabled Flag','','','');
--Inserting View Components for EIS_XXWC_CYCLE_STATUS_V
--Inserting View Component Joins for EIS_XXWC_CYCLE_STATUS_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Cycle Count Status Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Cycle Count Status Report
xxeis.eis_rs_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC INV ORGANIZATIONS LOV','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','District Lov','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Cycle Count Status Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Cycle Count Status Report
xxeis.eis_rs_utility.delete_report_rows( 'Cycle Count Status Report' );
--Inserting Report - Cycle Count Status Report
xxeis.eis_rs_ins.r( 401,'Cycle Count Status Report','','Details cycle count status of counts created but not approved.','','','','DM027741','EIS_XXWC_CYCLE_STATUS_V','Y','','','DM027741','','N','White Cap Reports','','Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Cycle Count Status Report
xxeis.eis_rs_ins.rc( 'Cycle Count Status Report',401,'BEGINING_COUNT_DATE','Count Date','Begining Count Date','','','default','','5','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Status Report',401,'DISTRICT','District','District','','','default','','2','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Status Report',401,'REGION','Region','Region','','','default','','1','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Status Report',401,'COUNT_ITEMS','Count of Items','Count Items','','~~~','default','','6','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Status Report',401,'COUNT_NOT_APPROVED','Count of items not approved','Count Not Approved','','~~~','default','','7','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Status Report',401,'ORG_CODE','Organization','Org Code','','','default','','3','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','');
xxeis.eis_rs_ins.rc( 'Cycle Count Status Report',401,'CREATION_DATE','Count Created','Creation Date','','','default','','4','N','','','','','','','','DM027741','N','N','','EIS_XXWC_CYCLE_STATUS_V','','');
--Inserting Report Parameters - Cycle Count Status Report
xxeis.eis_rs_ins.rp( 'Cycle Count Status Report',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','','Y','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Status Report',401,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','','Y','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Status Report',401,'Org','Org','ORG_CODE','IN','XXWC INV ORGANIZATIONS LOV','','VARCHAR2','Y','Y','3','','Y','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Status Report',401,'Org List','Org List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Status Report',401,'Count Date Beginning','Count Date Beginning','CREATION_DATE','>=','','','DATE','N','Y','5','','Y','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cycle Count Status Report',401,'Count date ending','Count date ending','CREATION_DATE','<=','','','DATE','N','Y','6','','Y','CONSTANT','DM027741','Y','N','','','');
--Inserting Report Conditions - Cycle Count Status Report
xxeis.eis_rs_ins.rcn( 'Cycle Count Status Report',401,'REGION','IN',':Region','','','Y','1','Y','DM027741');
xxeis.eis_rs_ins.rcn( 'Cycle Count Status Report',401,'DISTRICT','IN',':District','','','Y','2','Y','DM027741');
xxeis.eis_rs_ins.rcn( 'Cycle Count Status Report',401,'ORG_CODE','IN',':Org','','','Y','3','Y','DM027741');
xxeis.eis_rs_ins.rcn( 'Cycle Count Status Report',401,'CREATION_DATE','>=',':Count Date Beginning','','','Y','5','Y','DM027741');
xxeis.eis_rs_ins.rcn( 'Cycle Count Status Report',401,'CREATION_DATE','<=',':Count date ending','','','Y','6','Y','DM027741');
xxeis.eis_rs_ins.rcn( 'Cycle Count Status Report',401,'','','','','AND ( :Org List is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:Org List
  and x.list_type     =''Org''
 and x.process_id    = :system.process_id
and  EXCSV.ORG_CODE =regexp_replace(x.list_value,''[^[a-z,A-Z,0-9]]*'')))','Y','0','','DM027741');
--Inserting Report Sorts - Cycle Count Status Report
xxeis.eis_rs_ins.rs( 'Cycle Count Status Report',401,'REGION','ASC','DM027741','1','');
xxeis.eis_rs_ins.rs( 'Cycle Count Status Report',401,'DISTRICT','ASC','DM027741','2','');
--Inserting Report Triggers - Cycle Count Status Report
xxeis.eis_rs_ins.rt( 'Cycle Count Status Report',401,'Declare
L_TEMP         varchar2(240);
begin
L_TEMP         :=:Org List ;
if l_temp is not null then
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID => :SYSTEM.PROCESS_ID,
                        P_LIST_NAME => L_TEMP ,
                        P_LIST_TYPE => ''Org''
                        );
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_flag_value:=''Y'';
else
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.g_flag_value:=''N'';
end if;
end;
','B','Y','DM027741');
xxeis.eis_rs_ins.rt( 'Cycle Count Status Report',401,'BEGIN
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(P_PROCESS_ID    => :SYSTEM.PROCESS_ID);
END;','A','Y','DM027741');
--Inserting Report Templates - Cycle Count Status Report
--Inserting Report Portals - Cycle Count Status Report
--Inserting Report Dashboards - Cycle Count Status Report
--Inserting Report Security - Cycle Count Status Report
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','50884',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','50855',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','50981',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','50882',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','50883',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','51004',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','50619',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','50867',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','50849',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','401','','50868',401,'DM027741','','');
xxeis.eis_rs_ins.rsec( 'Cycle Count Status Report','201','','50892',401,'DM027741','','');
--Inserting Report Pivots - Cycle Count Status Report
xxeis.eis_rs_ins.rpivot( 'Cycle Count Status Report',401,'Count Status','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Count Status
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','COUNT_ITEMS','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','COUNT_NOT_APPROVED','DATA_FIELD','SUM','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','REGION','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','DISTRICT','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','BEGINING_COUNT_DATE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cycle Count Status Report',401,'Count Status','ORG_CODE','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- Count Status
END;
/
set scan on define on
