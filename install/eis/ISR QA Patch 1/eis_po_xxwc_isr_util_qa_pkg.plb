create or replace
PACKAGE BODY  eis_po_xxwc_isr_util_qa_pkg AS

function get_vendor_name (p_inventory_item_id number, p_organization_id number)
RETURN VARCHAR2
is
 l_vendor_name varchar2(240);
BEGIN

    select max(pov.vendor_name )
    into l_vendor_name
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov
    where 1=1
    and ass.inventory_item_id(+)=p_inventory_item_id
    and ass.organization_id(+)=p_organization_id
    and rco.sourcing_rule_id(+)=ass.sourcing_rule_id
    and sso.sr_receipt_id(+)=rco.sr_receipt_id
    and pov.vendor_id(+)=sso.vendor_id;

  return l_vendor_name;
 exception
 WHEN OTHERS THEN
   return null;
END;


function get_vendor_number (p_inventory_item_id number, p_organization_id number)
RETURN VARCHAR2
is
 l_vendor_number varchar2(240);
BEGIN

    select max(pov.segment1 )
    into l_vendor_number
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov
    where 1=1
    and ass.inventory_item_id(+)=p_inventory_item_id
    and ass.organization_id(+)=p_organization_id
    and rco.sourcing_rule_id(+)=ass.sourcing_rule_id
    and sso.sr_receipt_id(+)=rco.sr_receipt_id
    and pov.vendor_id(+)=sso.vendor_id;

  return l_vendor_number;
 exception
 WHEN OTHERS THEN
   return null;
END;


FUNCTION GET_INV_CAT_CLASS (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
BEGIN
        SELECT Mcv.SEGMENT2
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Inventory Category'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  RETURN NULL;
END;

FUNCTION Get_inv_cat_seg1 (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
Begin
        SELECT Mcv.SEGMENT1
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Inventory Category'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  RETURN NULL;
End;

FUNCTION Get_inv_vel_cat_class (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
BEGIN
        SELECT Mcv.SEGMENT1
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Sales Velocity'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  Return Null;
END;

function get_isr_item_cost (p_inventory_item_id number,p_organization_id number) return number
is
l_item_cost number;
begin

  select unit_price
  into l_item_cost
  from po_lines_all
  where po_line_id in
            ( select max(pol.po_line_id)
             from po_headers poh,
                  po_line_locations poll,
                  po_lines pol,
                  po_vendors   pov
             where poh.type_lookup_code      = 'BLANKET'
             and   poh.po_header_id          = pol.po_header_id
             and   pol.po_line_id            = poll.po_line_id(+)
             and   nvl(poh.cancel_flag,'N')='N'
            --and  poll.quantity_received > 0
            and  pol.item_id                 = p_inventory_item_id
            and (poh.global_agreement_flag ='Y' or poll.ship_to_organization_id = p_organization_id)
            and poh.vendor_id                = pov.vendor_id
            and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)
            );

  Return l_item_cost;

      exception when others then
    return  null;

end;

function get_isr_bpa_doc (p_inventory_item_id number,p_organization_id number) return number
is
l_bpa_doc number;
begin

  select segment1
  into l_bpa_doc
  from po_headers_all
  where po_header_id in
            ( select max(poh.po_header_id)
             from po_headers poh,
                  po_line_locations poll,
                  po_lines_all pol,
                  po_vendors   pov
             where poh.type_lookup_code      = 'BLANKET'
             and   poh.po_header_id          = pol.po_header_id
             and   nvl(poh.cancel_flag,'N')='N'
             and   pol.po_line_id            = poll.po_line_id(+)
            --and  poll.quantity_received > 0
              and  pol.item_id                 = p_inventory_item_id
              and (poh.global_agreement_flag ='Y' or poll.ship_to_organization_id = p_organization_id)
              and poh.vendor_id                = pov.vendor_id
              and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)
            );

  Return l_bpa_doc;

      exception when others then
    return  null;

end;

   FUNCTION get_isr_open_po_qty (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN NUMBER
   is
      L_OPEN_QTY                  number;
      L_INTERNAL_RECEIPT_QTY      number;
      L_ONORDER_QTY               number;
      L_COMPONENT_QTY             number;
      l_assembly_qy               number;
   BEGIN
      SELECT SUM (qty)
        INTO l_open_qty
        FROM (SELECT SUM (
                          poll.quantity
                        - NVL (poll.quantity_received, 0)
                        - NVL (poll.quantity_cancelled, 0))
                        qty
                FROM po_headers poh,
                     po_line_locations poll,
                     po_lines pol,
                     po_vendors pov
               WHERE     1 = 1
                     AND NVL (poh.cancel_flag, 'N') = 'N'
                     AND NVL (pol.cancel_flag, 'N') = 'N'
                     AND NVL (poll.cancel_flag, 'N') = 'N'
                     AND NVL (poh.closed_code, 'OPEN') NOT IN
                            ('FINALLY CLOSED', 'CLOSED')
                     AND NVL (poll.closed_code, 'OPEN') NOT IN
                            ('CLOSED', 'FINALLY CLOSED')
                     AND NVL (pol.closed_code, 'OPEN') NOT IN
                            ('CLOSED', 'FINALLY CLOSED')
                     AND poh.type_lookup_code IN
                            ('STANDARD', 'BLANKET', 'PLANNED')
                     AND poh.po_header_id = pol.po_header_id
                     AND pol.po_line_id = poll.po_line_id
                     AND poll.quantity - NVL (poll.quantity_received, 0) > 0
                     AND pol.item_id = p_inventory_item_id
                     AND poll.ship_to_organization_id = p_organization_id
                     and POH.VENDOR_ID = POV.VENDOR_ID);
                     
    --query to get internal receipt qty---

    select  SUM(OEL.ORDERED_QUANTITY-NVL(OEL.FULFILLED_QUANTITY,0)) 
    into l_internal_receipt_qty
    from
        APPS.OE_ORDER_LINES_ALL             OEL,
        apps.OE_ORDER_HEADERS_ALL           OEH,
        APPS.PO_REQUISITION_HEADERS_ALL     PRH
    where OEH.HEADER_ID        = OEL.HEADER_ID
    and OEH.SOURCE_DOCUMENT_ID = PRh.REQUISITION_HEADER_ID
    and OEL.SOURCE_TYPE_CODE   = 'INTERNAL'
    and OEL.FLOW_STATUS_CODE   ='CLOSED'
    and OEL.INVENTORY_ITEM_ID     =  P_INVENTORY_ITEM_ID
    AND OeL.SHIP_FROM_ORG_ID      =  P_ORGANIZATION_ID
    and not exists(select 1 
                    from APPS.RCV_TRANSACTIONS RT,
                          APPS.PO_REQUISITION_LINES_ALL PRL
                    where RT.SOURCE_DOCUMENT_CODE     ='REQ'
                    and   RT.REQUISITION_LINE_ID      = PRL.REQUISITION_LINE_ID
                    and   PRL.REQUISITION_HEADER_ID   = PRH.REQUISITION_HEADER_ID
                    );
--how ROP and Min-Max get the WIP Demand (If the item is a component on a work order).  

      select SUM(O.REQUIRED_QUANTITY - O.QUANTITY_ISSUED)
      into L_COMPONENT_QTY
         FROM wip_discrete_jobs d,
              wip_requirement_operations o
         WHERE o.wip_entity_id     = d.wip_entity_id
         and O.ORGANIZATION_ID   = D.ORGANIZATION_ID
         AND d.organization_id   = P_ORGANIZATION_ID
         and O.INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID
         AND o.date_required    <= sysdate
         AND o.required_quantity >= o.quantity_issued
         AND o.operation_seq_num > 0
         AND d.status_type in (1,3,4,6)
         and O.WIP_SUPPLY_TYPE not in (5,6)
       --  AND nvl(o.supply_subinventory,1) = decode(:subinv,NULL,nvl(o.supply_subinventory,1),:subinv)
         AND  NOT EXISTS(SELECT/*+ index(mtl MTL_DEMAND_N12)*/  wip.wip_entity_id
                      FROM wip_so_allocations wip, mtl_demand mtl
                      WHERE wip_entity_id = o.wip_entity_id
                      AND wip.organization_id = P_ORGANIZATION_ID
                      AND wip.organization_id = mtl.organization_id
                      AND wip.demand_source_header_id = mtl.demand_source_header_id
                      AND wip.demand_source_line = mtl.demand_source_line
                      and WIP.DEMAND_SOURCE_DELIVERY = MTL.DEMAND_SOURCE_DELIVERY
                      AND mtl.inventory_item_id = P_INVENTORY_ITEM_ID);
--how ROP and Min-Max get WIP Supply (If it's an assembly item on a work order).

        SELECT SUM(NVL(start_quantity,0)
               - NVL(quantity_completed,0)
               - NVL(QUANTITY_SCRAPPED,0))
         into l_assembly_qy
         FROM wip_discrete_jobs
         WHERE organization_id = P_ORGANIZATION_ID
         AND primary_item_id =P_INVENTORY_ITEM_ID
         AND status_type in (1,3,4,6)
         and JOB_TYPE in (1,3)
         and SCHEDULED_COMPLETION_DATE <= TO_DATE(TO_CHAR(sysdate),'DD-MON-RR');
   
      
      L_ONORDER_QTY:= NVL(L_OPEN_QTY,0)+NVL(L_INTERNAL_RECEIPT_QTY,0)+nvl(L_COMPONENT_QTY,0)+nvl(l_assembly_qy,0);
      
      return l_onorder_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


function get_isr_open_req_qty (p_inventory_item_id number,p_organization_id number) return number
is
l_open_qty number;
begin


        SELECT SUM(prdl.req_line_quantity) QTY
        into l_open_qty
        from  po_requisition_headers prh,
              po_req_distributions prdl,
              po_requisition_lines prl              
        where prh.requisition_header_id = prl.requisition_header_id  
        AND prdl.requisition_line_id=prl.requisition_line_id
        and item_id=p_inventory_item_id
        and nvl(prh.cancel_flag,'N')='N'
        and nvl(prl.cancel_flag,'N')='N'
        and nvl(prl.closed_code,'OPEN')='OPEN'
        and destination_organization_id=p_organization_id
        AND not exists (select 1
                        from po_action_history pah 
                        WHERE  pah.object_id = prh.requisition_header_id
                          AND pah.object_type_code  = 'REQUISITION'
                          AND pah.action_code       = 'CANCEL'
                       )
        and not exists (select 1
                        from po_distributions pod
                        WHERE POD.REQ_DISTRIBUTION_ID=PRDL.DISTRIBUTION_ID
                       )
       and not exists(select 1 from oe_order_headers oh
                       where order_type_id = 1011                       
                       and oh.source_document_id = prl.REQUISITION_HEADER_ID);

   --   and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)


  Return l_open_qty;

      exception when others then
    return  null;

end;

function get_isr_avail_qty (p_inventory_item_id number,p_organization_id number) return number
is
l_demand_qty number:=0;
l_avail_qty  number;
BEGIN
--This is code is commneted by srinivas--
     /* select sum(ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))
      into  l_demand_qty
      from  oe_order_lines_all ol
      where OL.INVENTORY_ITEM_ID     = P_INVENTORY_ITEM_ID
      and   OL.SHIP_FROM_ORG_ID      = P_ORGANIZATION_ID
      and   (ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))>0
      and   ol.flow_status_code not  in ('CANCELLED','CLOSED');*/
      
      --The following code added by srinivas---
      
  SELECT NVL(SUM(MR.RESERVATION_QUANTITY),0)
        into  l_demand_qty
      FROM    OE_ORDER_LINES OL,
              MTL_SALES_ORDERS MSO,
              MTL_RESERVATIONS MR
      WHERE MR.INVENTORY_ITEM_ID      = P_INVENTORY_ITEM_ID
        AND   MR.ORGANIZATION_ID        = P_ORGANIZATION_ID
        AND   MR.DEMAND_SOURCE_LINE_ID  = OL.LINE_ID
--        AND  ol.flow_status_CODE not in ('CLOSED')
        AND   MR.DEMAND_SOURCE_HEADER_ID=MSO.SALES_ORDER_ID;

     l_avail_qty:=(get_onhand_inv(p_inventory_item_id,p_organization_id)-nvl(l_demand_qty,0));
     return l_avail_qty;
    exception when no_data_found then
     return  l_avail_qty;

end;

function get_isr_ss_cnt (p_inventory_item_id number,p_organization_id number) return number
is
l_ss_item_cnt number;
begin

      select count(*)
      into  l_ss_item_cnt
      from  mtl_system_items_kfv msi
      where msi.inventory_item_id        = p_inventory_item_id
      and   msi.source_organization_id   = p_organization_id   ;

  Return l_ss_item_cnt;

      exception when others then
    return  null;

end;


function get_isr_sourcing_rule (p_inventory_item_id number,p_organization_id number) return varchar2
is
l_sr_name varchar2(500);
begin
      select max(msr.sourcing_rule_name )
      into l_sr_name
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         mrp_sourcing_rules msr,
         po_vendors pov
    where 1=1
    and ass.inventory_item_id(+)=p_inventory_item_id
    and ass.organization_id(+)=p_organization_id
    and rco.sourcing_rule_id(+)=ass.sourcing_rule_id
    and sso.sr_receipt_id(+)=rco.sr_receipt_id
    and msr.sourcing_rule_id =ass.sourcing_rule_id
    and pov.vendor_id(+)=sso.vendor_id;

    return  l_sr_name;
    EXCEPTION
 when others then
   return null;
end;

function get_isr_ss (p_inventory_item_id number,p_organization_id number) return number
is
l_ss  number;
begin
     select mst.safety_stock_quantity
     into l_ss
    from mtl_safety_stocks_view mst,org_acct_periods oap,
    org_organization_definitions ood
    where ood.organization_id=oap.organization_id
    and inventory_item_id= p_inventory_item_id
    and mst.organization_id=p_organization_id
    and mst.organization_id=oap.organization_id
    and  trunc(mst.effectivity_date) between period_start_date
    and nvl(period_close_date,sysdate)
        and  TRUNC(sysdate) between PERIOD_START_DATE
    and NVL(PERIOD_CLOSE_DATE,sysdate);

    return l_ss;

EXCEPTION
 when others then
   return null;
end;

function GET_INT_REQ_SO_QTY  (P_INVENTORY_ITEM_ID number,P_ORGANIZATION_ID number)
 RETURN NUMBER IS 

L_INTER_SALES_ORDER_QTY NUMBER;
L_INTERNAL_REQ_QTY      NUMBER;
L_BOOKED_QTY            NUMBER;
L_AVIALBLE_QTY          NUMBER;
l_avialble_qty2         number;

BEGIN

      
 --query to get internal sales order qty--  
 
   /*   SELECT   SUM(OEL.ORDERED_QUANTITY-NVL(OL.FULFILLED_QUANTITY,0))
      into L_INTER_SALES_ORDER_QTY
FROM  OE_ORDER_LINES         OEL,
      OE_ORDER_HEADERS        OEH
     -- PO_REQUISITION_HEADERS  PORH,
     -- PO_REQUISITION_LINES    PORL
  where OEH.HEADER_ID               = OEL.HEADER_ID
 -- AND OEL.SOURCE_DOCUMENT_ID        = PORH.REQUISITION_HEADER_ID
--  AND OEL.SOURCE_DOCUMENT_LINE_ID   = PORL.REQUISITION_LINE_ID
 -- AND PORH.REQUISITION_HEADER_ID    = PORL.REQUISITION_HEADER_ID
  and OEL.SOURCE_TYPE_CODE          ='INTERNAL'
  and oeh. order_type_id = 1011   
 -- and PORL.SOURCE_TYPE_CODE         ='INVENTORY'
--  AND OEL.ORDER_SOURCE_ID           = 10             --order_source_id for 'Internal'
--AND oel.orig_sys_document_ref = Int_Req_num'
 -- and OEL.ORG_ID                    = PORH.ORG_ID
  and OEL.INVENTORY_ITEM_ID          = P_INVENTORY_ITEM_ID
  and OEL.SHIP_FROM_ORG_ID           = P_ORGANIZATION_ID
--  and PORH.TRANSFERRED_TO_OE_FLAG        = 'Y'
  and OL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED')
 and  exists(select 1 
                      from PO_REQUISITION_LINES prl
                       where  oh.source_document_id = prl.REQUISITION_HEADER_ID);*/
         select SUM(OEL.ORDERED_QUANTITY - NVL(OEL.FULFILLED_QUANTITY,0)- NVL(mr.reservation_quantity , 0))
          into  L_INTER_SALES_ORDER_QTY
          from  OE_ORDER_LINES_ALL         OEL,
                OE_ORDER_HEADERS_ALL        OEH,
                MTL_SALES_ORDERS MSO,
                MTL_RESERVATIONS MR
         where OEH.HEADER_ID                = OEL.HEADER_ID
          and OEL.SOURCE_TYPE_CODE          = 'INTERNAL'
          and OEH. ORDER_TYPE_ID            = 1011   
          and MR.INVENTORY_ITEM_ID(+)       = OEL.INVENTORY_ITEM_ID
          and MR.ORGANIZATION_ID (+)        = OEL.SHIP_FROM_ORG_ID
          AND MR.DEMAND_SOURCE_LINE_ID(+)   = OEL.LINE_ID
          and MSO.SALES_ORDER_ID(+)         = MR.DEMAND_SOURCE_HEADER_ID
         -- and OEL.ORDER_SOURCE_ID           = 10 
          and OEL.INVENTORY_ITEM_ID         = P_INVENTORY_ITEM_ID
          and OEL.SHIP_FROM_ORG_ID          = P_ORGANIZATION_ID
          and OeL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED')
          and  exists(select 1 
                              from PO_REQUISITION_LINES_ALL PRL
                               where  oeh.source_document_id = prl.REQUISITION_HEADER_ID);
  
  --query to get internal req which are not interfaced for sales orders ---
  
  
  SELECT SUM(PRL.QUANTITY)
  into L_INTERNAL_REQ_QTY
  from  PO_REQUISITION_LINES      PRL,
        PO_REQUISITION_HEADERS    PRH
  where PRH.REQUISITION_HEADER_ID   = PRL.REQUISITION_HEADER_ID
  AND PRH.TYPE_LOOKUP_CODE          = 'INTERNAL'
  AND PRL.SOURCE_TYPE_CODE          = 'INVENTORY'
  AND PRL.ITEM_ID                   = P_INVENTORY_ITEM_ID
  and PRL.SOURCE_ORGANIZATION_ID    = P_ORGANIZATION_ID
  AND PRH.TRANSFERRED_TO_OE_FLAG    = 'N';
  
  --Query to get all booked sales order qty's exculding the return orders and open quotes and internale sales orders
  
  SELECT  SUM(OL.ORDERED_QUANTITY - NVL(OL.FULFILLED_QUANTITY,0)- NVL(mr.reservation_quantity , 0))
      INTO  L_BOOKED_QTY
      FROM  OE_ORDER_LINES OL,
            OE_ORDER_HEADERS OH,
            MTL_SALES_ORDERS MSO,
            MTL_RESERVATIONS MR
      where OH.HEADER_ID                  =  OL.HEADER_ID
      and OL.INVENTORY_ITEM_ID          =  P_INVENTORY_ITEM_ID
      and OL.SHIP_FROM_ORG_ID           =  P_ORGANIZATION_ID
      and MR.INVENTORY_ITEM_ID(+)       = OL.INVENTORY_ITEM_ID
      and MR.ORGANIZATION_ID (+)        = OL.SHIP_FROM_ORG_ID
      AND MR.DEMAND_SOURCE_LINE_ID(+)   = OL.LINE_ID
      and MSO.SALES_ORDER_ID(+)         = MR.DEMAND_SOURCE_HEADER_ID
      AND (OL.ORDERED_QUANTITY-NVL(OL.FULFILLED_QUANTITY,0))>0
      AND OH.FLOW_STATUS_CODE IN ('BOOKED')
      and OH.TRANSACTION_PHASE_CODE     <> 'N'--NOT ALLOWED TO OPEN QUOTES
      and OL.LINE_CATEGORY_CODE         <>'RETURN' --NOT ALLOWED TO RETURN oRDERS
      and OL.SOURCE_TYPE_CODE           <> 'INTERNAL'--Not allowed to Internale Sales Orders
      --and OH. ORDER_TYPE_ID            <> 1012   --Not allowed to Internale Sales Orders
      --and OL.ORDER_SOURCE_ID           <> 10 --Not allowed to Internale Sales Orders
      and OL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED');
  
  L_AVIALBLE_QTY := GET_ISR_AVAIL_QTY (P_INVENTORY_ITEM_ID ,P_ORGANIZATION_ID );

 L_AVIALBLE_QTY2 := L_AVIALBLE_QTY-(nvl(L_BOOKED_QTY,0)+nvl(L_INTER_SALES_ORDER_QTY,0)+nvl(L_INTERNAL_REQ_QTY,0));
 
 RETURN L_AVIALBLE_QTY2;
 
 EXCEPTION WHEN OTHERS THEN
 L_AVIALBLE_QTY2 :=0;
 end;

function Get_isr_rpt_dc_mod_sub  return varchar2
is
begin
return G_isr_rpt_dc_mod_sub;
end;

function GET_ONHAND_INV (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number)
Return Number
is
 l_Onhand_Inv Number;
BEGIN

   select nvl(sum(moq.transaction_quantity),0)
    Into  L_Onhand_Inv
    from mtl_onhand_quantities_detail moq
    Where Inventory_Item_Id          =P_Inventory_Item_Id
    and organization_id              =p_organization_id;

  return l_Onhand_Inv;
 exception
 WHEN OTHERS THEN
   return 0;
END;

FUNCTION Get_primary_bin_loc (P_INVENTORY_ITEM_ID NUMBER, P_ORGANIZATION_ID NUMBER)
RETURN VARCHAR2
IS
 l_seg VARCHAR2(5000);
BEGIN

    Select mil.Segment1
    INTO  L_SEG
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
         MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID  = P_INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID      =P_ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR = MIL.INVENTORY_LOCATION_ID
    and mil.segment1  like '1%';


  return l_seg;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;



End;
/