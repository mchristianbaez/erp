--Report Name            : Hazmat Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating View Data for Hazmat Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_HAZMAT_V',401,'','','','','MR020532','XXEIS','Eis Rs Xxwc Inv Hazmat V','EXIHV','','');
--Delete View Columns for EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_HAZMAT_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSDS_NUMBER',401,'Msds Number','MSDS_NUMBER','','','','MR020532','VARCHAR2','','','Msds Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','VOC_SUB_CATEGORY',401,'Voc Sub Category','VOC_SUB_CATEGORY','','','','MR020532','VARCHAR2','','','Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','VOC_CATEGORY',401,'Voc Category','VOC_CATEGORY','','','','MR020532','VARCHAR2','','','Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PESTICIDE_FLAG_STATE',401,'Pesticide Flag State','PESTICIDE_FLAG_STATE','','','','MR020532','VARCHAR2','','','Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','VOCNUMBER',401,'Vocnumber','VOCNUMBER','','','','MR020532','VARCHAR2','','','Vocnumber','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PESTICIDE_FLAG',401,'Pesticide Flag','PESTICIDE_FLAG','','','','MR020532','VARCHAR2','','','Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','CAPROP65',401,'Caprop65','CAPROP65','','','','MR020532','VARCHAR2','','','Caprop65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','MR020532','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','MR020532','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','LOCATION',401,'Location','LOCATION','','','','MR020532','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','CONTAINER_TYPE',401,'Container Type','CONTAINER_TYPE','','','','MR020532','VARCHAR2','','','Container Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','HAZARDOUS_MATERIAL_DESCRIPTION',401,'Hazardous Material Description','HAZARDOUS_MATERIAL_DESCRIPTION','','','','MR020532','VARCHAR2','','','Hazardous Material Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','HAZARD_CLASS',401,'Hazard Class','HAZARD_CLASS','','','','MR020532','VARCHAR2','','','Hazard Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MAXIMUM',401,'Maximum','MAXIMUM','','','','MR020532','NUMBER','','','Maximum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MINIMUM',401,'Minimum','MINIMUM','','','','MR020532','NUMBER','','','Minimum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','ONHAND',401,'Onhand','ONHAND','','','','MR020532','NUMBER','','','Onhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','ORMD',401,'Ormd','ORMD','','','','MR020532','VARCHAR2','','','Ormd','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PACKING_GROUP',401,'Packing Group','PACKING_GROUP','','','','MR020532','VARCHAR2','','','Packing Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','UN_DESCRIPTION',401,'Un Description','UN_DESCRIPTION','','','','MR020532','VARCHAR2','','','Un Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','UN_NUMBER',401,'Un Number','UN_NUMBER','','','','MR020532','VARCHAR2','','','Un Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','WEIGHT',401,'Weight','WEIGHT','','','','MR020532','NUMBER','','','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','HAZMAT_FLAG',401,'Hazmat Flag','HAZMAT_FLAG','','','','MR020532','VARCHAR2','','','Hazmat Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','COUNTRY_OF_ORIGIN',401,'Country Of Origin','COUNTRY_OF_ORIGIN','','','','MR020532','VARCHAR2','','','Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','UOM',401,'Uom','UOM','','','','MR020532','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Org Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Inv Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PERIOD_NAME',401,'Period Name','PERIOD_NAME','','','','MR020532','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#HDS#LOB',401,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#HDS#DROP_SHIPMENT',401,'Descriptive flexfield: Items Column Name: Drop Shipment Context: HDS','MSI#HDS#Drop_Shipment','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#HDS#INVOICE_UOM',401,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#HDS#PRODUCT_ID',401,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#HDS#VENDOR_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#HDS#UNSPSC_CODE',401,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#HDS#UPC_PRIMARY',401,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#HDS#SKU_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#CA_PROP_65',401,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#COUNTRY_OF_ORIGIN',401,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#ORM_D_FLAG',401,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#YEARLY_STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#YEARLY_DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#PRISM_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#HAZMAT_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#HAZMAT_CONTAINER',401,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#GTP_INDICATOR',401,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#LAST_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#AMU',401,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#RESERVE_STOCK',401,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#TAXWARE_CODE',401,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#AVERAGE_UNITS',401,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#PRODUCT_CODE',401,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#IMPORT_DUTY_',401,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#KEEP_ITEM_ACTIVE',401,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#PESTICIDE_FLAG',401,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#CALC_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#VOC_GL',401,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#PESTICIDE_FLAG_STATE',401,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#VOC_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#VOC_SUB_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#MSDS_#',401,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#WC#HAZMAT_PACKAGING_GROU',401,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',401,'Descriptive flexfield: Items Column Name: Drop Shipment Eligable Context: HDS','MSI#HDS#Drop_Shipment_Eligab','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment Eligable','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','ALTERNATE_BIN_LOC',401,'Alternate Bin Loc','ALTERNATE_BIN_LOC','','','','MR020532','VARCHAR2','','','Alternate Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PRIMARY_BIN_LOC',401,'Primary Bin Loc','PRIMARY_BIN_LOC','','','','MR020532','VARCHAR2','','','Primary Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','MR020532','NUMBER','','','Shelf Life Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','CA_PROP_65',401,'Ca Prop 65','CA_PROP_65','','','','MR020532','VARCHAR2','','','Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','DOT_PROPER_SHIPPING_NAME',401,'Dot Proper Shipping Name','DOT_PROPER_SHIPPING_NAME','','','','MR020532','VARCHAR2','','','Dot Proper Shipping Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','LIMITED_QTY_FLAG',401,'Limited Qty Flag','LIMITED_QTY_FLAG','','','','MR020532','VARCHAR2','','','Limited Qty Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSDS',401,'Msds','MSDS','','','','MR020532','VARCHAR2','','','Msds','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PACKAGING_GROUP',401,'Packaging Group','PACKAGING_GROUP','','','','MR020532','VARCHAR2','','','Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','VOC_CONTENT',401,'Voc Content','VOC_CONTENT','','','','MR020532','VARCHAR2','','','Voc Content','','','');
--Inserting View Components for EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_HAZMAT_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','MR020532','MR020532','-1','Inventory Item Definitions','','','','');
--Inserting View Component Joins for EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_HAZMAT_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIHV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_HAZMAT_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIHV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','MR020532','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Hazmat Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Hazmat Report
xxeis.eis_rs_ins.lov( 401,'select distinct  attribute1 CA_Prop_65 from mtl_system_items_kfv where ATTRIBUTE_CATEGORY=''WC''','','INV CA Prop 65','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct attribute3 Pesticide_Flag from mtl_system_items_kfv where ATTRIBUTE_CATEGORY=''WC''','','INV Pesticide Flag','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct attribute6 VOC_Category from mtl_system_items_kfv where ATTRIBUTE_CATEGORY=''WC''','','INV VOC Category','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
    FROM  ORG_ORGANIZATION_DEFINITIONS OOD,
      HR_OPERATING_UNITS HOU
    WHERE  OOD.OPERATING_UNIT = HOU.ORGANIZATION_ID
      AND HOU.ORGANIZATION_ID  = FND_PROFILE.VALUE(''ORG_ID'')
      ORDER BY organization_code','','XXWC Inventory Org List','List of All Inventory Orgs under a given operating unit.','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Hazmat Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Hazmat Report
xxeis.eis_rs_utility.delete_report_rows( 'Hazmat Report' );
--Inserting Report - Hazmat Report
xxeis.eis_rs_ins.r( 401,'Hazmat Report','','Provide a listing of HazMat items: Chemical Report:  Inventory, UN #, ORMD, MSDS - Y/N; Chem Report:  VOC or insecticides by product; Chem Report:  Restricted Areas by product','','','','MR020532','EIS_XXWC_INV_HAZMAT_V','Y','','','MR020532','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Hazmat Report
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'LOCATION','Organization','Location','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'PART_NUMBER','Part Number','Part Number','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'PESTICIDE_FLAG','Pesticides Flag','Pesticide Flag','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'PESTICIDE_FLAG_STATE','Pesticide Flag State','Pesticide Flag State','','','default','','16','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'VOC_CATEGORY','VOC Category','Voc Category','','','default','','19','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'VOC_SUB_CATEGORY','VOC Sub Category','Voc Sub Category','','','default','','20','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'CONTAINER_TYPE','Container Type','Container Type','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'HAZARD_CLASS','Hazard Class','Hazard Class','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'MAXIMUM','Max','Maximum','','~~~','default','','24','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'MINIMUM','Min','Minimum','','~~~','default','','23','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'ONHAND','Qty On Hand','Onhand','','~~~','default','','22','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'UN_DESCRIPTION','Un Description','Un Description','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'UN_NUMBER','Un Number','Un Number','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'WEIGHT','Weight','Weight','','~T~D~2','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'HAZMAT_FLAG','Hazmat Flag','Hazmat Flag','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'COUNTRY_OF_ORIGIN','Country Of Origin','Country Of Origin','','','default','','21','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'UOM','Uom','Uom','','','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'ALTERNATE_BIN_LOC','Alternate Bin Loc','Alternate Bin Loc','','','default','','26','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'PRIMARY_BIN_LOC','Primary Bin Loc','Primary Bin Loc','','','default','','25','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'SHELF_LIFE_DAYS','Shelf Life Days','Shelf Life Days','','~~~','default','','27','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'CA_PROP_65','Ca Prop 65','Ca Prop 65','','','default','','17','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'DOT_PROPER_SHIPPING_NAME','Dot Proper Shipping Name','Dot Proper Shipping Name','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'LIMITED_QTY_FLAG','Limited Qty Flag','Limited Qty Flag','','','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'PACKAGING_GROUP','Packaging Group','Packaging Group','','','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'MSDS','MSDS Number','Msds','','','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'VOC_CONTENT','Voc Content','Voc Content','','','','','18','N','','','','','','','','MR020532','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
--Inserting Report Parameters - Hazmat Report
xxeis.eis_rs_ins.rp( 'Hazmat Report',401,'Organization','Organization','LOCATION','IN','XXWC Inventory Org List','','VARCHAR2','Y','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Hazmat Report',401,'CA Prop 65','CA Prop 65','CA_PROP_65','IN','INV CA Prop 65','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Hazmat Report',401,'Pesticide Flag','Pesticide Flag','PESTICIDE_FLAG','IN','INV Pesticide Flag','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Hazmat Report',401,'VOC Category','VOC Category','VOC_CATEGORY','IN','INV VOC Category','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Hazmat Report
xxeis.eis_rs_ins.rcn( 'Hazmat Report',401,'LOCATION','IN',':Organization','','','Y','1','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Hazmat Report',401,'PESTICIDE_FLAG','IN',':Pesticide Flag','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Hazmat Report',401,'VOC_CATEGORY','IN',':VOC Category','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Hazmat Report',401,'CA_PROP_65','IN',':CA Prop 65','','','Y','2','Y','MR020532');
--Inserting Report Sorts - Hazmat Report
xxeis.eis_rs_ins.rs( 'Hazmat Report',401,'PART_NUMBER','ASC','MR020532','','');
--Inserting Report Triggers - Hazmat Report
--Inserting Report Templates - Hazmat Report
xxeis.eis_rs_ins.R_Tem( 'Hazmat Report','Hazmat Report','Provide a listing of HazMat items: Chemical Report:  Inventory, UN #, ORMD, MSDS - Y/N; Chem Report:  VOC or insecticides by product; Chem Report:  Restricted Areas by product','','','','','','','','','','','','MR020532');
--Inserting Report Portals - Hazmat Report
--Inserting Report Dashboards - Hazmat Report
--Inserting Report Security - Hazmat Report
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50619',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50924',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','51052',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50879',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50851',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50852',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50821',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','20005','','50880',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','51029',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50882',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50883',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50981',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50855',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50884',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','20634',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','20005','','50900',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50895',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50865',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50864',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50849',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','660','','50871',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50862',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','707','','51104',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50990',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','201','','50983',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','201','','50621',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','201','','50893',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','201','','51369',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','201','','50910',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','201','','50892',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','201','','50921',401,'MR020532','','');
--Inserting Report Pivots - Hazmat Report
END;
/
set scan on define on
