--Report Name            : Consignment Product Sold
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AP_CONSN_PRDSOLD_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AP_CONSN_PRDSOLD_V
Create or replace View XXEIS.EIS_XXWC_AP_CONSN_PRDSOLD_V
(LOCATION,PART_NUMBER,DESCRIPTION,ORDERED_QUANTITY,PO_COST,ONHAND_QTY,SUPPLIER_NUMBER) AS 
SELECT mtp.organization_code Location,
    Msi.Segment1 Part_Number,
    Msi.Description Description,
    SUM (NVL(rctl.quantity_invoiced, rctl.quantity_credited)) ORDERED_QUANTITY,
    SUM(DECODE(nvl(APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id),0),0,ol.unit_cost,APPS.xxwc_mv_routines_pkg.get_order_line_cost(ol.line_id))) Po_Cost,
    Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Onhand_Qty(Msi.Inventory_Item_Id,Msi.Organization_Id) Onhand_Qty,
    Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Vendor_Number(Msi.Inventory_Item_Id,Msi.Organization_Id) Supplier_Number
    --Oh.Order_Number Order_Number,
    --TRUNC(Rct.Trx_Date) Invoice_Date,
    --TRUNC(Oh.Ordered_Date) Ordered_Date,
    --HCA.ACCOUNT_NUMBER CUSTOMER_NUMBER,
    --NVL(Hca.Account_Name,Hzp.party_name) Customer_Name
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM Oe_Order_Headers Oh,
    Oe_Order_Lines Ol,
    Hz_Cust_Accounts Hca,
    Hz_Parties Hzp,
    Mtl_Parameters Mtp,
    Gl_Code_Combinations_Kfv Gcc,
    Mtl_System_Items_Kfv Msi,
    Ra_Customer_Trx Rct,
    Ra_customer_trx_lines rctl
  WHERE ol.header_id            = oh.header_id
  AND oh.sold_to_org_id         = hca.cust_account_id(+)
  AND hca.party_id              = hzp.party_id(+)
  AND ol.ship_from_org_id       = mtp.organization_id
  AND mtp.cost_of_sales_account = gcc.code_combination_id
  AND ol.inventory_item_id      = msi.inventory_item_id
  AND ol.ship_from_org_id       = msi.organization_id
  AND msi.consigned_flag        = 1
  AND rctl.customer_trx_id      = rct.customer_trx_id
  AND rctl.interface_line_context              = 'ORDER ENTRY'
  AND rct.interface_header_context             = 'ORDER ENTRY'
  AND trunc(rct.trx_date) >=  nvl(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from,trunc(rct.trx_date))
  AND trunc(rct.trx_date) <=  nvl(xxeis.eis_rs_xxwc_com_util_pkg.get_date_to,trunc(rct.trx_date))
  AND TO_CHAR (rctl.interface_line_attribute6) = TO_CHAR (ol.line_id)
  AND TO_CHAR(oh.order_number)       = rct.interface_header_attribute1
  AND EXISTS
    (SELECT 1
    FROM xxeis.eis_org_access_v
    WHERE organization_id = mtp.organization_id
    )
  GROUP BY mtp.organization_code,
    Msi.Segment1,
    Msi.Description,
    Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Onhand_Qty(Msi.Inventory_Item_Id,Msi.Organization_Id),
    Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Vendor_Number(Msi.Inventory_Item_Id,Msi.Organization_Id)
/
set scan on define on
prompt Creating View Data for Consignment Product Sold
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AP_CONSN_PRDSOLD_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AP_CONSN_PRDSOLD_V',200,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ap Consn Prdsold V','EXACPV1','','');
--Delete View Columns for EIS_XXWC_AP_CONSN_PRDSOLD_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AP_CONSN_PRDSOLD_V',200,FALSE);
--Inserting View Columns for EIS_XXWC_AP_CONSN_PRDSOLD_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','SUPPLIER_NUMBER',200,'Supplier Number','SUPPLIER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','PO_COST',200,'Po Cost','PO_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Po Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','DESCRIPTION',200,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','PART_NUMBER',200,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','LOCATION',200,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','ONHAND_QTY',200,'Onhand Qty','ONHAND_QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Onhand Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','ORDERED_QUANTITY',200,'Ordered Quantity','ORDERED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Ordered Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','INVOICE_DATE',200,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','CUSTOMER_NAME',200,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','CUSTOMER_NUMBER',200,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_CONSN_PRDSOLD_V','ORDERED_DATE',200,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','');
--Inserting View Components for EIS_XXWC_AP_CONSN_PRDSOLD_V
--Inserting View Component Joins for EIS_XXWC_AP_CONSN_PRDSOLD_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Consignment Product Sold
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Consignment Product Sold
xxeis.eis_rs_ins.lov( 200,'SELECT distinct segment1
   FROM PO_VENDORS
   order by segment1','','SUPPLIER_NUMBER','SUPPLIER_NUMBER','XXEIS_RS_ADMIN',NULL,'','','');
END;
/
set scan on define on
prompt Creating Report Data for Consignment Product Sold
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Consignment Product Sold
xxeis.eis_rs_utility.delete_report_rows( 'Consignment Product Sold' );
--Inserting Report - Consignment Product Sold
xxeis.eis_rs_ins.r( 200,'Consignment Product Sold','','Detailed listing of consignment product sold for a selected time period.  Consignment product is defined in the consignment field in the general planning tab of the item master.','','','','XXEIS_RS_ADMIN','EIS_XXWC_AP_CONSN_PRDSOLD_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','EXCEL,','N');
--Inserting Report Columns - Consignment Product Sold
xxeis.eis_rs_ins.rc( 'Consignment Product Sold',200,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDSOLD_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Sold',200,'LOCATION','Location','Location','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDSOLD_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Sold',200,'PART_NUMBER','Part Number','Part Number','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDSOLD_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Sold',200,'PO_COST','PO Cost','Po Cost','','~T~D~2','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDSOLD_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Sold',200,'SUPPLIER_NUMBER','Supplier Number','Supplier Number','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDSOLD_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Sold',200,'ONHAND_QTY','On Hand Quantity','Onhand Qty','','~~~','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDSOLD_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Sold',200,'ORDERED_QUANTITY','Quantity','Ordered Quantity','','~~~','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDSOLD_V','','');
xxeis.eis_rs_ins.rc( 'Consignment Product Sold',200,'EXTENDED PO COST','Extended PO Cost','Ordered Quantity','NUMBER','~T~D~','default','','6','Y','','','','','','','(EXACPV1.ORDERED_QUANTITY*EXACPV1.PO_COST)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_CONSN_PRDSOLD_V','','');
--Inserting Report Parameters - Consignment Product Sold
xxeis.eis_rs_ins.rp( 'Consignment Product Sold',200,'Supplier Number','Supplier Number','SUPPLIER_NUMBER','IN','SUPPLIER_NUMBER','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Consignment Product Sold',200,'Start Date','Start Date','INVOICE_DATE','>=','','','DATE','Y','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Consignment Product Sold',200,'End Date','End Date','INVOICE_DATE','<=','','','DATE','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
--Inserting Report Conditions - Consignment Product Sold
xxeis.eis_rs_ins.rcn( 'Consignment Product Sold',200,'SUPPLIER_NUMBER','IN',':Supplier Number','','','Y','1','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Consignment Product Sold
xxeis.eis_rs_ins.rs( 'Consignment Product Sold',200,'LOCATION','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Consignment Product Sold
xxeis.eis_rs_ins.rt( 'Consignment Product Sold',200,'begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_to(:End Date);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_from(:Start Date);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Consignment Product Sold
--Inserting Report Portals - Consignment Product Sold
--Inserting Report Dashboards - Consignment Product Sold
--Inserting Report Security - Consignment Product Sold
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50934',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50936',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50932',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50768',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50767',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50766',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50773',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50742',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50730',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50637',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50620',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50704',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50707',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50933',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50763',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50770',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50764',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50771',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50970',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50935',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50783',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50740',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50864',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50867',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50743',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50862',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50744',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50902',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50760',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50781',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50782',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50863',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50741',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50765',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50865',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50866',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','50878',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','20005','','50880',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','','SO004816','',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','','LB048272','',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','','10012196','',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','','MM050208','',200,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Consignment Product Sold','200','','20639',200,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Consignment Product Sold
xxeis.eis_rs_ins.rpivot( 'Consignment Product Sold',200,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Consignment Product Sold',200,'Pivot','DESCRIPTION','PAGE_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Consignment Product Sold',200,'Pivot','LOCATION','PAGE_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Consignment Product Sold',200,'Pivot','PART_NUMBER','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Consignment Product Sold',200,'Pivot','SUPPLIER_NUMBER','PAGE_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Consignment Product Sold',200,'Pivot','EXTENDED PO COST','DATA_FIELD','SUM','','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
