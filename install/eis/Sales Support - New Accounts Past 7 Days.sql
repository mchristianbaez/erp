--Report Name            : Sales Support - New Accounts Past 7 Days
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_631484_AGWGWY_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_631484_AGWGWY_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_631484_AGWGWY_V
 ("PARTY_CREATION_DATE","ACCOUNT_CREATION_DATE","ACCOUNT_NUMBER","CLEANSED_NAME","ACCOUNT_NAME","PROFILE_CLASS_NAME","PREDOMINANT_TRADE","CUSTOMER__CLASSIFICATION","SALES_CHANNEL","BILLING_ADDRESS_1","BILLING_ADDRESS_2","BILLING_ADDRESS_3","BILLING_ADDRESS_4","BILLING_CITY","BILLING_STATE","BILLING_ZIP","SIC_CODE","ACCOUNT_MANAGER_NAME","EEID","PHONE_NUMBER","EMAIL_ADDRESS","SITE_NAME"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_631484_AGWGWY_V
 ("PARTY_CREATION_DATE","ACCOUNT_CREATION_DATE","ACCOUNT_NUMBER","CLEANSED_NAME","ACCOUNT_NAME","PROFILE_CLASS_NAME","PREDOMINANT_TRADE","CUSTOMER__CLASSIFICATION","SALES_CHANNEL","BILLING_ADDRESS_1","BILLING_ADDRESS_2","BILLING_ADDRESS_3","BILLING_ADDRESS_4","BILLING_CITY","BILLING_STATE","BILLING_ZIP","SIC_CODE","ACCOUNT_MANAGER_NAME","EEID","PHONE_NUMBER","EMAIL_ADDRESS","SITE_NAME"
) as ');
l_stmt :=  'select 
hp.creation_date Party_Creation_Date,
hca.creation_date Account_Creation_Date,
hca.account_number Account_Number,
trim(
replace(
  replace(
    replace(
      replace(
        replace(hca.account_name,''CASH'',''''),
        ''cash'',''''),
           ''%'','' ''),
            ''/'',''''),
              ''Cash'','''')
                ) Cleansed_Name,
hca.account_name Account_Name,
hcpc.name  Profile_Class_Name,
hca.attribute9 Predominant_Trade,
hca.customer_class_code Customer__Classification,
hca.sales_channel_code Sales_Channel,
hl.address1 Billing_Address_1,
hl.address2 Billing_Address_2,
hl.address3 Billing_Address_3,
hl.address4 Billing_Address_4,
hl.city Billing_City,
hl.state Billing_State,
hl.postal_code Billing_Zip,
hop.sic_code SIC_Code,
jrdv.resource_name Account_Manager_Name,
jrdv.source_number EEID,
HCPTS.RAW_PHONE_NUMBER PHONE_NUMBER,
HCPTS2.EMAIL_ADDRESS EMAIL_ADDRESS,
hcsua.location SITE_NAME
from 
ar.hz_organization_profiles hop,
ar.hz_party_sites hps,
ar.hz_locations hl,
ar.hz_customer_profiles hcp,
ar.hz_cust_profile_classes hcpc,
ar.hz_parties hp
inner join ar.hz_cust_accounts hca
on hp.party_id = hca.party_id
inner join ar.hz_cust_acct_sites_all hcasa
on hca.cust_account_id = hcasa.cust_account_id
inner join ar.hz_cust_site_uses_all hcsua
on hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
left outer join apps.jtf_rs_salesreps jrs
on jrs.salesrep_id = hcsua.primary_salesrep_id
left outer join apps.jtf_rs_defresources_v jrdv
on jrs.resource_id = jrdv.resource_id
left outer join ar.hz_contact_points hcpts
on hcpts.owner_table_id = hp.party_id
and hcpts.contact_point_type = ''PHONE'' and hcpts.primary_flag = ''Y'' and hcpts.status = ''A'' and hcpts.owner_table_name = ''HZ_PARTIES''
left outer join ar.hz_contact_points hcpts2
on hcpts2.owner_table_id = hp.party_id
and hcpts2.contact_point_type = ''EMAIL'' and hcpts2.primary_flag = ''Y'' and hcpts2.status = ''A'' and hcpts2.owner_table_name = ''HZ_PARTIES''
where
hop.party_id = hp.party_id
and hcasa.party_site_id = hps.party_site_id
and hps.location_id = hl.location_id
and hca.cust_account_id = hcp.cust_account_id
and hcp.profile_class_id = hcpc.profile_class_id
and hcsua.site_use_code = ''BILL_TO''
and hcsua.primary_flag = ''Y''
and hcp.site_use_id is null
and (jrs.org_id= ''162'' or  (jrs.org_id is null and hcsua.primary_salesrep_id is null))
and hop.effective_end_date is  null
and hcpc.name not in (''Intercompany Customers'', ''WC Branches'')
and hcsua.org_id = ''162''
AND TRUNC(HCA.CREATION_DATE) >TRUNC(SYSDATE)-7

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Object Data XXEIS_631484_AGWGWY_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_631484_AGWGWY_V
xxeis.eis_rsc_ins.v( 'XXEIS_631484_AGWGWY_V',222,'Paste SQL View for Sales Support - New Accounts Past 7 Days','','','','SG019472','APPS','Sales Support - New Accounts Past 7 Days View','X6AV','','Y','VIEW','US','','','');
--Delete Object Columns for XXEIS_631484_AGWGWY_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_631484_AGWGWY_V',222,FALSE);
--Inserting Object Columns for XXEIS_631484_AGWGWY_V
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','PARTY_CREATION_DATE',222,'','','','','','SG019472','DATE','','','Party Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','ACCOUNT_CREATION_DATE',222,'','','','','','SG019472','DATE','','','Account Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','ACCOUNT_NUMBER',222,'','','','','','SG019472','VARCHAR2','','','Account Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','CLEANSED_NAME',222,'','','','','','SG019472','VARCHAR2','','','Cleansed Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','ACCOUNT_NAME',222,'','','','','','SG019472','VARCHAR2','','','Account Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','PROFILE_CLASS_NAME',222,'','','','','','SG019472','VARCHAR2','','','Profile Class Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','BILLING_ADDRESS_1',222,'','','','','','SG019472','VARCHAR2','','','Billing Address 1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','BILLING_ADDRESS_2',222,'','','','','','SG019472','VARCHAR2','','','Billing Address 2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','BILLING_ADDRESS_3',222,'','','','','','SG019472','VARCHAR2','','','Billing Address 3','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','BILLING_ADDRESS_4',222,'','','','','','SG019472','VARCHAR2','','','Billing Address 4','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','BILLING_CITY',222,'','','','','','SG019472','VARCHAR2','','','Billing City','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','BILLING_STATE',222,'','','','','','SG019472','VARCHAR2','','','Billing State','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','BILLING_ZIP',222,'','','','','','SG019472','VARCHAR2','','','Billing Zip','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','SIC_CODE',222,'','','','','','SG019472','VARCHAR2','','','Sic Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','ACCOUNT_MANAGER_NAME',222,'','','','','','SG019472','VARCHAR2','','','Account Manager Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','PHONE_NUMBER',222,'','','','','','SG019472','VARCHAR2','','','Phone Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','EMAIL_ADDRESS',222,'','','','','','SG019472','VARCHAR2','','','Email Address','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','CUSTOMER__CLASSIFICATION',222,'','','','','','SG019472','VARCHAR2','','','Customer  Classification','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','EEID',222,'','','','','','SG019472','VARCHAR2','','','Eeid','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','PREDOMINANT_TRADE',222,'','','','','','SG019472','VARCHAR2','','','Predominant Trade','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','SALES_CHANNEL',222,'','','','','','SG019472','VARCHAR2','','','Sales Channel','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_631484_AGWGWY_V','SITE_NAME',222,'Site Name','','','','','SG019472','VARCHAR2','','','Site Name','','','','','');
--Inserting Object Components for XXEIS_631484_AGWGWY_V
--Inserting Object Component Joins for XXEIS_631484_AGWGWY_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report Sales Support - New Accounts Past 7 Days
prompt Creating Report Data for Sales Support - New Accounts Past 7 Days
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Sales Support - New Accounts Past 7 Days
xxeis.eis_rsc_utility.delete_report_rows( 'Sales Support - New Accounts Past 7 Days',222 );
--Inserting Report - Sales Support - New Accounts Past 7 Days
xxeis.eis_rsc_ins.r( 222,'Sales Support - New Accounts Past 7 Days','','Provides a list of all accounts created in past 7 days','','','','XXEIS_RS_ADMIN','XXEIS_631484_AGWGWY_V','Y','','select 
hp.creation_date Party_Creation_Date,
hca.creation_date Account_Creation_Date,
hca.account_number Account_Number,
trim(
replace(
  replace(
    replace(
      replace(
        replace(hca.account_name,''CASH'',''''),
        ''cash'',''''),
           ''%'','' ''),
            ''/'',''''),
              ''Cash'','''')
                ) Cleansed_Name,
hca.account_name Account_Name,
hcpc.name  Profile_Class_Name,
hca.attribute9 Predominant_Trade,
hca.customer_class_code Customer__Classification,
hca.sales_channel_code Sales_Channel,
hl.address1 Billing_Address_1,
hl.address2 Billing_Address_2,
hl.address3 Billing_Address_3,
hl.address4 Billing_Address_4,
hl.city Billing_City,
hl.state Billing_State,
hl.postal_code Billing_Zip,
hop.sic_code SIC_Code,
jrdv.resource_name Account_Manager_Name,
jrdv.source_number EEID,
HCPTS.RAW_PHONE_NUMBER PHONE_NUMBER,
HCPTS2.EMAIL_ADDRESS EMAIL_ADDRESS,
hcsua.location SITE_NAME
from 
ar.hz_organization_profiles hop,
ar.hz_party_sites hps,
ar.hz_locations hl,
ar.hz_customer_profiles hcp,
ar.hz_cust_profile_classes hcpc,
ar.hz_parties hp
inner join ar.hz_cust_accounts hca
on hp.party_id = hca.party_id
inner join ar.hz_cust_acct_sites_all hcasa
on hca.cust_account_id = hcasa.cust_account_id
inner join ar.hz_cust_site_uses_all hcsua
on hcasa.cust_acct_site_id = hcsua.cust_acct_site_id
left outer join apps.jtf_rs_salesreps jrs
on jrs.salesrep_id = hcsua.primary_salesrep_id
left outer join apps.jtf_rs_defresources_v jrdv
on jrs.resource_id = jrdv.resource_id
left outer join ar.hz_contact_points hcpts
on hcpts.owner_table_id = hp.party_id
and hcpts.contact_point_type = ''PHONE'' and hcpts.primary_flag = ''Y'' and hcpts.status = ''A'' and hcpts.owner_table_name = ''HZ_PARTIES''
left outer join ar.hz_contact_points hcpts2
on hcpts2.owner_table_id = hp.party_id
and hcpts2.contact_point_type = ''EMAIL'' and hcpts2.primary_flag = ''Y'' and hcpts2.status = ''A'' and hcpts2.owner_table_name = ''HZ_PARTIES''
where
hop.party_id = hp.party_id
and hcasa.party_site_id = hps.party_site_id
and hps.location_id = hl.location_id
and hca.cust_account_id = hcp.cust_account_id
and hcp.profile_class_id = hcpc.profile_class_id
and hcsua.site_use_code = ''BILL_TO''
and hcsua.primary_flag = ''Y''
and hcp.site_use_id is null
and (jrs.org_id= ''162'' or  (jrs.org_id is null and hcsua.primary_salesrep_id is null))
and hop.effective_end_date is  null
and hcpc.name not in (''Intercompany Customers'', ''WC Branches'')
and hcsua.org_id = ''162''
AND TRUNC(HCA.CREATION_DATE) >TRUNC(SYSDATE)-7
','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,HTML,Pivot Excel,EXCEL,','N','','','','','','N','APPS','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Sales Support - New Accounts Past 7 Days
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'ACCOUNT_MANAGER_NAME','Account Manager Name','','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'BILLING_ZIP','Billing Zip','','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'ACCOUNT_NUMBER','Account Number','','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'PROFILE_CLASS_NAME','Profile Class Name','','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'CLEANSED_NAME','Cleansed Name','','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'BILLING_STATE','Billing State','','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'BILLING_ADDRESS_3','Billing Address 3','','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'BILLING_CITY','Billing City','','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'BILLING_ADDRESS_2','Billing Address 2','','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'ACCOUNT_NAME','Account Name','','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'BILLING_ADDRESS_4','Billing Address 4','','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'EMAIL_ADDRESS','Email Address','','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'ACCOUNT_CREATION_DATE','Account Creation Date','','','DD-MON-RRRR','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'PHONE_NUMBER','Phone Number','','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'SIC_CODE','Sic Code','','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'BILLING_ADDRESS_1','Billing Address 1','','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'EEID','Eeid','','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'CUSTOMER__CLASSIFICATION','Customer  Classification','','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'PREDOMINANT_TRADE','Predominant Trade','','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'SALES_CHANNEL','Sales Channel','','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_631484_AGWGWY_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Sales Support - New Accounts Past 7 Days',222,'SITE_NAME','Site Name','Site Name','VARCHAR2','','','','5','','Y','','','','','','','XXEIS_RS_ADMIN','','','','XXEIS_631484_AGWGWY_V','','','','US','','');
--Inserting Report Parameters - Sales Support - New Accounts Past 7 Days
--Inserting Dependent Parameters - Sales Support - New Accounts Past 7 Days
--Inserting Report Conditions - Sales Support - New Accounts Past 7 Days
--Inserting Report Sorts - Sales Support - New Accounts Past 7 Days
xxeis.eis_rsc_ins.rs( 'Sales Support - New Accounts Past 7 Days',222,'ACCOUNT_CREATION_DATE','DESC','XXEIS_RS_ADMIN','1','');
--Inserting Report Triggers - Sales Support - New Accounts Past 7 Days
--inserting report templates - Sales Support - New Accounts Past 7 Days
--Inserting Report Portals - Sales Support - New Accounts Past 7 Days
--inserting report dashboards - Sales Support - New Accounts Past 7 Days
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Sales Support - New Accounts Past 7 Days','222','XXEIS_631484_AGWGWY_V','XXEIS_631484_AGWGWY_V','N','');
--inserting report security - Sales Support - New Accounts Past 7 Days
xxeis.eis_rsc_ins.rsec( 'Sales Support - New Accounts Past 7 Days','879','','XXWC_CUSTOMER_HIERARCHY_MGR',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Sales Support - New Accounts Past 7 Days','879','','XXWC_CUSTOMER_HIERARCHY_MTN',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Sales Support - New Accounts Past 7 Days','','DM027741','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Sales Support - New Accounts Past 7 Days','20005','','XXWC_VIEW_ALL_EIS_REPORTS',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Sales Support - New Accounts Past 7 Days','','SS084202','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Sales Support - New Accounts Past 7 Days','','10011288','',222,'XXEIS_RS_ADMIN','','N','');
--Inserting Report Pivots - Sales Support - New Accounts Past 7 Days
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Sales Support - New Accounts Past 7 Days
xxeis.eis_rsc_ins.rv( 'Sales Support - New Accounts Past 7 Days','','Sales Support - New Accounts Past 7 Days','SA059956','23-NOV-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
