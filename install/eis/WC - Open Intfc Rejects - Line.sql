--Report Name            : WC - Open Intfc Rejects - Line
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_1463_WVRGIP_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_1463_WVRGIP_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_1463_WVRGIP_V
 ("CREATION_DATE","REJECT_LOOKUP_CODE","PO_NUMBER","GROUP_ID","VENDOR_NUM","INVOICE_NUM","INVOICE_DATE","INVOICE_AMOUNT","SOURCE","ATTRIBUTE1","INV_CREATE_DATE","VENDOR_NAME","SEGMENT1","LINE_NUMBER"
) as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_1463_WVRGIP_V
 ("CREATION_DATE","REJECT_LOOKUP_CODE","PO_NUMBER","GROUP_ID","VENDOR_NUM","INVOICE_NUM","INVOICE_DATE","INVOICE_AMOUNT","SOURCE","ATTRIBUTE1","INV_CREATE_DATE","VENDOR_NAME","SEGMENT1","LINE_NUMBER"
) as ');
l_stmt :=  'SELECT AIR.CREATION_DATE
     , AIR.REJECT_LOOKUP_CODE
     , AII.PO_NUMBER
     , AII.GROUP_ID
     , AII.VENDOR_NUM
     , AII.INVOICE_NUM
     , AII.INVOICE_DATE
     , AII.INVOICE_AMOUNT
     , AII.SOURCE
     , AII.ATTRIBUTE1
     , TRUNC(AII.CREATION_DATE) INV_CREATE_DATE
     , ASS.VENDOR_NAME
     , ASS.SEGMENT1
     , AILI.LINE_NUMBER
FROM AP.AP_INTERFACE_REJECTIONS AIR
   , AP.AP_INVOICE_LINES_INTERFACE AILI
   , AP.AP_INVOICES_INTERFACE AII
   , AP.AP_SUPPLIERS ASS
WHERE AIR.PARENT_TABLE = ''AP_INVOICE_LINES_INTERFACE''
  AND ASS.VENDOR_ID = AII.VENDOR_ID (+)
  AND AIR.PARENT_ID = AILI.INVOICE_LINE_ID
  AND AILI.INVOICE_ID = AII.INVOICE_ID
  AND AII.ORG_ID = 162

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Object Data XXEIS_1463_WVRGIP_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1463_WVRGIP_V
xxeis.eis_rsc_ins.v( 'XXEIS_1463_WVRGIP_V',200,'Paste SQL View for WC - Open Intfc Rejects - Line','','','','10012196','APPS','WC - Open Intfc Rejects - Line View','X1WV1','','Y','VIEW','US','','','');
--Delete Object Columns for XXEIS_1463_WVRGIP_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1463_WVRGIP_V',200,FALSE);
--Inserting Object Columns for XXEIS_1463_WVRGIP_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','CREATION_DATE',200,'','','','','','10012196','DATE','','','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','REJECT_LOOKUP_CODE',200,'','','','','','10012196','VARCHAR2','','','Reject Lookup Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','PO_NUMBER',200,'','','','','','10012196','VARCHAR2','','','Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','GROUP_ID',200,'','','','','','10012196','VARCHAR2','','','Group Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','VENDOR_NUM',200,'','','','','','10012196','VARCHAR2','','','Vendor Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','INVOICE_NUM',200,'','','','','','10012196','VARCHAR2','','','Invoice Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','INVOICE_DATE',200,'','','','','','10012196','DATE','','','Invoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','INVOICE_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Invoice Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','SOURCE',200,'','','','','','10012196','VARCHAR2','','','Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','ATTRIBUTE1',200,'','','','','','10012196','VARCHAR2','','','Attribute1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','VENDOR_NAME',200,'','','','','','10012196','VARCHAR2','','','Vendor Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','SEGMENT1',200,'','','','','','10012196','VARCHAR2','','','Segment1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','INV_CREATE_DATE',200,'','','','','','10012196','DATE','','','Inv Create Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_1463_WVRGIP_V','LINE_NUMBER',200,'','','','','','10012196','NUMBER','','','Line Number','','','','US','');
--Inserting Object Components for XXEIS_1463_WVRGIP_V
--Inserting Object Component Joins for XXEIS_1463_WVRGIP_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC - Open Intfc Rejects - Line
prompt Creating Report Data for WC - Open Intfc Rejects - Line
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC - Open Intfc Rejects - Line
xxeis.eis_rsc_utility.delete_report_rows( 'WC - Open Intfc Rejects - Line',200 );
--Inserting Report - WC - Open Intfc Rejects - Line
xxeis.eis_rsc_ins.r( 200,'WC - Open Intfc Rejects - Line','','WC - Open Intfc Rejects - Line','','','','10012196','XXEIS_1463_WVRGIP_V','Y','','SELECT AIR.CREATION_DATE
     , AIR.REJECT_LOOKUP_CODE
     , AII.PO_NUMBER
     , AII.GROUP_ID
     , AII.VENDOR_NUM
     , AII.INVOICE_NUM
     , AII.INVOICE_DATE
     , AII.INVOICE_AMOUNT
     , AII.SOURCE
     , AII.ATTRIBUTE1
     , TRUNC(AII.CREATION_DATE) INV_CREATE_DATE
     , ASS.VENDOR_NAME
     , ASS.SEGMENT1
     , AILI.LINE_NUMBER
FROM AP.AP_INTERFACE_REJECTIONS AIR
   , AP.AP_INVOICE_LINES_INTERFACE AILI
   , AP.AP_INVOICES_INTERFACE AII
   , AP.AP_SUPPLIERS ASS
WHERE AIR.PARENT_TABLE = ''AP_INVOICE_LINES_INTERFACE''
  AND ASS.VENDOR_ID = AII.VENDOR_ID (+)
  AND AIR.PARENT_ID = AILI.INVOICE_LINE_ID
  AND AILI.INVOICE_ID = AII.INVOICE_ID
  AND AII.ORG_ID = 162
','10012196','','N','Invoices','','CSV,Pivot Excel,EXCEL,','','','','','','','N','APPS','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - WC - Open Intfc Rejects - Line
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'CREATION_DATE','Creation Date','','','','','','8','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'REJECT_LOOKUP_CODE','Reject Lookup Code','','','','','','10','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'PO_NUMBER','Po Number','','','','','','12','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'GROUP_ID','Group Id','','','','','','11','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'VENDOR_NUM','Vendor Num','','','','','','2','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'INVOICE_NUM','Invoice Num','','','','','','4','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'INVOICE_DATE','Invoice Date','','','','','','5','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'INVOICE_AMOUNT','Invoice Amount','','','','','','6','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'SOURCE','Source','','','','','','9','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'ATTRIBUTE1','Attribute1','','','','','','7','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'VENDOR_NAME','Vendor Name','','','','','','3','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'SEGMENT1','Segment1','','','','','','1','N','','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'INV_CREATE_DATE','Inv Create Date','','','','','','13','','Y','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'WC - Open Intfc Rejects - Line',200,'LINE_NUMBER','Line Number','','','','','','14','','Y','','','','','','','10012196','N','N','','XXEIS_1463_WVRGIP_V','','','SUM','US','','');
--Inserting Report Parameters - WC - Open Intfc Rejects - Line
xxeis.eis_rsc_ins.rp( 'WC - Open Intfc Rejects - Line',200,'Inv Create Date','','INV_CREATE_DATE','=','','','DATE','N','Y','1','Y','Y','CURRENT_DATE','10012196','Y','N','','Start Date','','XXEIS_1463_WVRGIP_V','','','US','');
--Inserting Dependent Parameters - WC - Open Intfc Rejects - Line
--Inserting Report Conditions - WC - Open Intfc Rejects - Line
xxeis.eis_rsc_ins.rcnh( 'WC - Open Intfc Rejects - Line',200,'X1WV1.INV_CREATE_DATE = Inv Create Date','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','INV_CREATE_DATE','','Inv Create Date','','','','','XXEIS_1463_WVRGIP_V','','','','','','EQUALS','Y','Y','','','','','1',200,'WC - Open Intfc Rejects - Line','X1WV1.INV_CREATE_DATE = Inv Create Date');
--Inserting Report Sorts - WC - Open Intfc Rejects - Line
--Inserting Report Triggers - WC - Open Intfc Rejects - Line
--inserting report templates - WC - Open Intfc Rejects - Line
--Inserting Report Portals - WC - Open Intfc Rejects - Line
--inserting report dashboards - WC - Open Intfc Rejects - Line
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC - Open Intfc Rejects - Line','200','XXEIS_1463_WVRGIP_V','XXEIS_1463_WVRGIP_V','N','');
--inserting report security - WC - Open Intfc Rejects - Line
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects - Line','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects - Line','200','','XXWC_PAY_VENDOR_MSTR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects - Line','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects - Line','200','','XXWC_PAY_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects - Line','200','','XXWC_PAYABLES_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects - Line','200','','XXWC_PAY_DISBURSE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects - Line','200','','XXWC_PAY_W_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - Open Intfc Rejects - Line','200','','XXWC_PAY_NO_CALENDAR',200,'10012196','','','');
--Inserting Report Pivots - WC - Open Intfc Rejects - Line
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- WC - Open Intfc Rejects - Line
xxeis.eis_rsc_ins.rv( 'WC - Open Intfc Rejects - Line','','WC - Open Intfc Rejects - Line','SA059956','08-AUG-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
