--Report Name            : Orders Exception Report Test
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Orders Exception Report Test
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_ORDER_EXP_TEST_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_ORDER_EXP_TEST_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Order Exp V','EXOOEV','','');
--Delete View Columns for EIS_XXWC_OM_ORDER_EXP_TEST_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_ORDER_EXP_TEST_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_ORDER_EXP_TEST_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','LOC',660,'Loc','LOC','','','','SA059956','VARCHAR2','','','Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','EXCEPTION_DESCRIPTION',660,'Exception Description','EXCEPTION_DESCRIPTION','','','','SA059956','VARCHAR2','','','Exception Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','EXT_ORDER_TOTAL',660,'Ext Order Total','EXT_ORDER_TOTAL','','','','SA059956','NUMBER','','','Ext Order Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','EXCEPTION_DATE',660,'Exception Date','EXCEPTION_DATE','','','','SA059956','DATE','','','Exception Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','SA059956','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','EXCEPTION_RESAON',660,'Exception Resaon','EXCEPTION_RESAON','','','','SA059956','VARCHAR2','','','Exception Resaon','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','SA059956','VARCHAR2','','','Sales Person Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','CREATED_BY',660,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','QTY',660,'Qty','QTY','','','','SA059956','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','SHIP_METHOD',660,'Ship Method','SHIP_METHOD','','','','SA059956','VARCHAR2','','','Ship Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_TEST_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','SA059956','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_OM_ORDER_EXP_TEST_V
--Inserting View Component Joins for EIS_XXWC_OM_ORDER_EXP_TEST_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Orders Exception Report Test
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Orders Exception Report Test
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS
WHERE  RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Orders Exception Report Test
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Orders Exception Report Test
xxeis.eis_rs_utility.delete_report_rows( 'Orders Exception Report Test' );
--Inserting Report - Orders Exception Report Test
xxeis.eis_rs_ins.r( 660,'Orders Exception Report Test','','White Cap Exception Report','','','','SA059956','EIS_XXWC_OM_ORDER_EXP_TEST_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Orders Exception Report Test
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'EXCEPTION_DESCRIPTION','Exception Description','Exception Description','','','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'EXCEPTION_RESAON','Exception Reason','Exception Resaon','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'EXT_ORDER_TOTAL','Line Total','Ext Order Total','','~T~D~2','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'ORDER_DATE','Order Date','Order Date','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'ORDER_TYPE','Order Type','Order Type','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'CREATED_BY','Created By','Created By','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'QTY','Qty','Qty','','~~~','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'LOC','Loc','Loc','','','','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'SHIP_METHOD','Ship Method','Ship Method','','','','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report Test',660,'EXCEPTION_DATE','Exception Date','Exception Date','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_OM_ORDER_EXP_TEST_V','','');
--Inserting Report Parameters - Orders Exception Report Test
xxeis.eis_rs_ins.rp( 'Orders Exception Report Test',660,'Sales Person Name','Sales Person Name','SALES_PERSON_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders Exception Report Test',660,'Organization','Organization','LOC','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Orders Exception Report Test
xxeis.eis_rs_ins.rcn( 'Orders Exception Report Test',660,'','','','','AND PROCESS_ID=:SYSTEM.PROCESS_ID','Y','1','','SA059956');
--Inserting Report Sorts - Orders Exception Report Test
xxeis.eis_rs_ins.rs( 'Orders Exception Report Test',660,'EXCEPTION_RESAON','ASC','SA059956','1','1');
--Inserting Report Triggers - Orders Exception Report Test
xxeis.eis_rs_ins.rt( 'Orders Exception Report Test',660,'begin
xxeis.eis_xxwc_om_order_exp_pkg.process_order_exception(
p_process_id 		=> :SYSTEM.PROCESS_ID,
p_organization		=> :Organization,
p_sales_person_name => :Sales Person Name);
end;','B','Y','SA059956');
xxeis.eis_rs_ins.rt( 'Orders Exception Report Test',660,'begin
xxeis.eis_xxwc_om_order_exp_pkg.clear_temp_tables(
p_process_id 		=> :SYSTEM.PROCESS_ID);
end;','A','Y','SA059956');
--Inserting Report Templates - Orders Exception Report Test
--Inserting Report Portals - Orders Exception Report Test
--Inserting Report Dashboards - Orders Exception Report Test
--Inserting Report Security - Orders Exception Report Test
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','51044',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','51509',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','51045',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','50860',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','50886',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','50859',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','50858',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','50901',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','51025',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','660','','50857',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','20005','','50900',660,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report Test','','HY016540','',660,'SA059956','','');
--Inserting Report Pivots - Orders Exception Report Test
xxeis.eis_rs_ins.rpivot( 'Orders Exception Report Test',660,'Summary by reason','1','1,0|1,2,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Summary by reason
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report Test',660,'Summary by reason','EXT_ORDER_TOTAL','DATA_FIELD','SUM','Ext Total','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report Test',660,'Summary by reason','EXCEPTION_RESAON','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report Test',660,'Summary by reason','ORDER_NUMBER','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report Test',660,'Summary by reason','CREATED_BY','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report Test',660,'Summary by reason','QTY','ROW_FIELD','','','4','','');
--Inserting Report Summary Calculation Columns For Pivot- Summary by reason
END;
/
set scan on define on
