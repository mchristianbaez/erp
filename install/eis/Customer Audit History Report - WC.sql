--Report Name            : Customer Audit History Report - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_AR_CUST_AUDIT_HIST_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_CUST_AUDIT_HIST_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V',222,'','','','','SA059956','XXEIS','Eis Xxwc Ar Cust Audit Hist V','EXACAHV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_AR_CUST_AUDIT_HIST_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_CUST_AUDIT_HIST_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_CUST_AUDIT_HIST_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','DESCRIPTION',222,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','TABLE_NAME',222,'Table Name','TABLE_NAME','','','','SA059956','VARCHAR2','','','Table Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','NEW_VALUE',222,'New Value','NEW_VALUE','','','','SA059956','VARCHAR2','','','New Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','OLD_VALUE',222,'Old Value','OLD_VALUE','','','','SA059956','VARCHAR2','','','Old Value','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','ATRIBUTE_NAME',222,'Atribute Name','ATRIBUTE_NAME','','','','SA059956','VARCHAR2','','','Atribute Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','SITE_NAME',222,'Site Name','SITE_NAME','','','','SA059956','VARCHAR2','','','Site Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','SITE_NUMBER',222,'Site Number','SITE_NUMBER','','','','SA059956','VARCHAR2','','','Site Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','ACCOUNT_NAME',222,'Account Name','ACCOUNT_NAME','','','','SA059956','VARCHAR2','','','Account Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','ACCOUNT_NUMBER',222,'Account Number','ACCOUNT_NUMBER','','','','SA059956','VARCHAR2','','','Account Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','USER_NAME',222,'User Name','USER_NAME','','','','SA059956','VARCHAR2','','','User Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','USER_NTID',222,'User Ntid','USER_NTID','','','','SA059956','VARCHAR2','','','User Ntid','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','AUDIT_TYPE',222,'Audit Type','AUDIT_TYPE','','','','SA059956','VARCHAR2','','','Audit Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','AUDIT_DATE',222,'Audit Date','AUDIT_DATE','','','','SA059956','DATE','','','Audit Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','TRUNC_AUDIT_DATE',222,'Trunc Audit Date','TRUNC_AUDIT_DATE','','','','SA059956','DATE','','','Trunc Audit Date','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','CREDIT_REPORT_FLAG',222,'Credit Report Flag','CREDIT_REPORT_FLAG','','','','SA059956','VARCHAR2','','','Credit Report Flag','','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_AUDIT_HIST_V','PROFILE_CLASS',222,'Profile Class','PROFILE_CLASS','','','','SA059956','VARCHAR2','','','Profile Class','','','','','');
--Inserting Object Components for EIS_XXWC_AR_CUST_AUDIT_HIST_V
--Inserting Object Component Joins for EIS_XXWC_AR_CUST_AUDIT_HIST_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for Customer Audit History Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Customer Audit History Report - WC
xxeis.eis_rsc_ins.lov( '','Select ''Yes''  yes_no
from dual
Union
Select ''No'' yes_no
from dual','','Yes_No_Lov','This lov provides two values yes,no.','XXEIS_RS_ADMIN',NULL,'N','','','Y','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT hca.account_number customer_number,NVL(hca.account_name, hp.party_name) customer_name
FROM hz_cust_accounts hca,
  hz_parties hp
where hca.party_id=hp.party_id
ORDER BY lpad(hca.account_number,10)','','XXWC Customer Number LOV','Displays List of Values for Customer Number','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for Customer Audit History Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Customer Audit History Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Customer Audit History Report - WC',222 );
--Inserting Report - Customer Audit History Report - WC
xxeis.eis_rsc_ins.r( 222,'Customer Audit History Report - WC','','This report will provide the requestor with a list of updates made to the account or site level for the customer account requested.  Additionally the requestor may choose to run the report only for Credit Limit updates.','','','','SA059956','EIS_XXWC_AR_CUST_AUDIT_HIST_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Customer Audit History Report - WC
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'ACCOUNT_NAME','Account Name','Account Name','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'ACCOUNT_NUMBER','Account Number','Account Number','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'ATRIBUTE_NAME','Attribute Name','Atribute Name','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'AUDIT_DATE','Audit Date','Audit Date','','MM/DD/RR HH:MI AM','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'AUDIT_TYPE','Audit Type','Audit Type','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'DESCRIPTION','Table Description','Description','','','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'NEW_VALUE','New Value','New Value','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'OLD_VALUE','Old Value','Old Value','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'SITE_NAME','Site Name','Site Name','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'SITE_NUMBER','Site Number','Site Number','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'TABLE_NAME','Table Name','Table Name','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'USER_NAME','User Name','User Name','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'USER_NTID','User NTID','User Ntid','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'Customer Audit History Report - WC',222,'PROFILE_CLASS','Profile Class','Profile Class','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','US','','');
--Inserting Report Parameters - Customer Audit History Report - WC
xxeis.eis_rsc_ins.rp( 'Customer Audit History Report - WC',222,'Customer Account Number','Customer Account Number','ACCOUNT_NUMBER','IN','XXWC Customer Number LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Audit History Report - WC',222,'Start Date','Start Date','TRUNC_AUDIT_DATE','>=','','select add_months(sysdate,-12) from dual','DATE','Y','Y','2','N','Y','SQL','SA059956','Y','N','','Start Date','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Audit History Report - WC',222,'End Date','End Date','TRUNC_AUDIT_DATE','<=','','','DATE','N','Y','3','N','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Audit History Report - WC',222,'Credit Limit Report Only','Credit Limit Report Only','CREDIT_REPORT_FLAG','IN','Yes_No_Lov','''Yes''','VARCHAR2','N','Y','4','N','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','US','');
--Inserting Dependent Parameters - Customer Audit History Report - WC
--Inserting Report Conditions - Customer Audit History Report - WC
xxeis.eis_rsc_ins.rcnh( 'Customer Audit History Report - WC',222,'EXACAHV.ACCOUNT_NUMBER IN Customer Account Number','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT_NUMBER','','Customer Account Number','','','','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Audit History Report - WC','EXACAHV.ACCOUNT_NUMBER IN Customer Account Number');
xxeis.eis_rsc_ins.rcnh( 'Customer Audit History Report - WC',222,'EXACAHV.TRUNC_AUDIT_DATE >= Start Date','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','TRUNC_AUDIT_DATE','','Start Date','','','','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',222,'Customer Audit History Report - WC','EXACAHV.TRUNC_AUDIT_DATE >= Start Date');
xxeis.eis_rsc_ins.rcnh( 'Customer Audit History Report - WC',222,'EXACAHV.TRUNC_AUDIT_DATE <= End Date','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','TRUNC_AUDIT_DATE','','End Date','','','','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',222,'Customer Audit History Report - WC','EXACAHV.TRUNC_AUDIT_DATE <= End Date');
xxeis.eis_rsc_ins.rcnh( 'Customer Audit History Report - WC',222,'EXACAHV.CREDIT_REPORT_FLAG IN Credit Limit Report Only','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','CREDIT_REPORT_FLAG','','Credit Limit Report Only','','','','','EIS_XXWC_AR_CUST_AUDIT_HIST_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Audit History Report - WC','EXACAHV.CREDIT_REPORT_FLAG IN Credit Limit Report Only');
--Inserting Report Sorts - Customer Audit History Report - WC
xxeis.eis_rsc_ins.rs( 'Customer Audit History Report - WC',222,'AUDIT_DATE','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Customer Audit History Report - WC',222,'ACCOUNT_NUMBER','ASC','SA059956','2','');
xxeis.eis_rsc_ins.rs( 'Customer Audit History Report - WC',222,'SITE_NUMBER','ASC','SA059956','3','');
--Inserting Report Triggers - Customer Audit History Report - WC
--inserting report templates - Customer Audit History Report - WC
xxeis.eis_rsc_ins.r_tem( 'Customer Audit History Report - WC','Customer Audit History Report - WC','Customer Audit History Report - WC','','','','','','','','','','','','SA059956','X','','','Y','Y','N','N');
--Inserting Report Portals - Customer Audit History Report - WC
--inserting report dashboards - Customer Audit History Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Customer Audit History Report - WC','222','EIS_XXWC_AR_CUST_AUDIT_HIST_V','EIS_XXWC_AR_CUST_AUDIT_HIST_V','N','');
--inserting report security - Customer Audit History Report - WC
xxeis.eis_rsc_ins.rsec( 'Customer Audit History Report - WC','101','','XXCUS_GL_MANAGER',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Audit History Report - WC','222','','RECEIVABLES_MANAGER',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Audit History Report - WC','222','','XXWC_CRE_CREDIT_COLL_MGR',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Audit History Report - WC','222','','XXWC_CRE_ASSOC_CASH_APP_MGR',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Audit History Report - WC','','LA023190','',222,'SA059956','','N','');
--Inserting Report Pivots - Customer Audit History Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Customer Audit History Report - WC
xxeis.eis_rsc_ins.rv( 'Customer Audit History Report - WC','','Customer Audit History Report - WC','SA059956','21-FEB-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
