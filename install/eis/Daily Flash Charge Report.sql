--Report Name            : Daily Flash Charge Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Daily Flash Charge Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_DAILY_FLASH_NEW_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Daily Flash Sale V','EXADFSV','','');
--Delete View Columns for EIS_XXWC_AR_DAILY_FLASH_NEW_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_DAILY_FLASH_NEW_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_DAILY_FLASH_NEW_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','DATE1',660,'Date1','DATE1','','','','XXEIS_RS_ADMIN','DATE','','','Date1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','SALE_AMT',660,'Sale Amt','SALE_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','SALE_COST',660,'Sale Cost','SALE_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','PAYMENT_TYPE',660,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type','','','');
--Inserting View Components for EIS_XXWC_AR_DAILY_FLASH_NEW_V
--Inserting View Component Joins for EIS_XXWC_AR_DAILY_FLASH_NEW_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Charge Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Charge Report
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Charge Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Charge Report
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Charge Report' );
--Inserting Report - Daily Flash Charge Report
xxeis.eis_rs_ins.r( 660,'Daily Flash Charge Report','','The purpose of this extract is to provide Finance with a daily report of all charge sales by branch.  The selected date parameter represents the desired date of sales (e.g. For sales reported on Aug 1st, set the Date parameter = Aug. 1st.  This report is to be processed daily and intended to accompany the daily extracts, flash_charge and flash_cash.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_DAILY_FLASH_NEW_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Daily Flash Charge Report
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report',660,'GROSS_PROFIT','GP$','','NUMBER','~T~D~2','default','','5','Y','','','','','','','(nvl(EXADFSV.SALE_AMT,0)-nvl(EXADFSV.sale_cost,0))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report',660,'BRANCH_LOCATION','Location','Branch Location','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','~~~','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report',660,'SALE_AMT','Net Sales Dollars','Sale Amt','','~T~D~2','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report',660,'DATE1','Date','Date1','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
--Inserting Report Parameters - Daily Flash Charge Report
xxeis.eis_rs_ins.rp( 'Daily Flash Charge Report',660,'Location','WC Branch Number','BRANCH_LOCATION','IN','OM Warehouse All','','VARCHAR2','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Charge Report',660,'Date','Prior day�s date','DATE1','IN','','select trunc(sysdate-1) from dual','DATE','Y','Y','1','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Charge Report
xxeis.eis_rs_ins.rcn( 'Daily Flash Charge Report',660,'','','','','AND ( ''All'' IN (:Location) OR (BRANCH_LOCATION IN (:Location)))','Y','1','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Charge Report
xxeis.eis_rs_ins.rs( 'Daily Flash Charge Report',660,'BRANCH_LOCATION','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Daily Flash Charge Report
xxeis.eis_rs_ins.rt( 'Daily Flash Charge Report',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_payment_type(''CHARGE'');
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Charge Report
--Inserting Report Portals - Daily Flash Charge Report
--Inserting Report Dashboards - Daily Flash Charge Report
--Inserting Report Security - Daily Flash Charge Report
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report','','TB003018','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report','','JS020126','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report','','MT009628','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report','20005','','51207',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Daily Flash Charge Report
xxeis.eis_rs_ins.rpivot( 'Daily Flash Charge Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Charge Report',660,'Pivot','GROSS_PROFIT','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Charge Report',660,'Pivot','BRANCH_LOCATION','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Charge Report',660,'Pivot','INVOICE_COUNT','DATA_FIELD','COUNT','Invoice Count','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
