--Report Name            : WC Daily Failed Concurrent Programs Rpt
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for WC Daily Failed Concurrent Programs Rpt
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_DAILY_FAILED_PGMS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_DAILY_FAILED_PGMS_V',85000,'','','','','MR020532','XXEIS','Eis Xxwc Dly Fld Conprog V','EXDFCV','','');
--Delete View Columns for EIS_XXWC_DAILY_FAILED_PGMS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_DAILY_FAILED_PGMS_V',85000,FALSE);
--Inserting View Columns for EIS_XXWC_DAILY_FAILED_PGMS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_FAILED_PGMS_V','COMPLETION_TEXT',85000,'Completion Text','COMPLETION_TEXT','','','','MR020532','VARCHAR2','','','Completion Text','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_FAILED_PGMS_V','PARAMETERS',85000,'Parameters','PARAMETERS','','','','MR020532','VARCHAR2','','','Parameters','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_FAILED_PGMS_V','DATE_COMPLETED',85000,'Date Completed','DATE_COMPLETED','','','','MR020532','DATE','','','Date Completed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_FAILED_PGMS_V','DATE_STARTED',85000,'Date Started','DATE_STARTED','','','','MR020532','DATE','','','Date Started','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_FAILED_PGMS_V','REQUESTOR',85000,'Requestor','REQUESTOR','','','','MR020532','VARCHAR2','','','Requestor','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_FAILED_PGMS_V','STATUS',85000,'Status','STATUS','','','','MR020532','VARCHAR2','','','Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_FAILED_PGMS_V','CONCURRENT_PROGRAM_NAME',85000,'Concurrent Program Name','CONCURRENT_PROGRAM_NAME','','','','MR020532','VARCHAR2','','','Concurrent Program Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_DAILY_FAILED_PGMS_V','REQUEST_ID',85000,'Request Id','REQUEST_ID','','','','MR020532','NUMBER','','','Request Id','','','');
--Inserting View Components for EIS_XXWC_DAILY_FAILED_PGMS_V
--Inserting View Component Joins for EIS_XXWC_DAILY_FAILED_PGMS_V
END;
/
set scan on define on
prompt Creating Report Data for WC Daily Failed Concurrent Programs Rpt
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC Daily Failed Concurrent Programs Rpt
xxeis.eis_rs_utility.delete_report_rows( 'WC Daily Failed Concurrent Programs Rpt' );
--Inserting Report - WC Daily Failed Concurrent Programs Rpt
xxeis.eis_rs_ins.r( 85000,'WC Daily Failed Concurrent Programs Rpt','','To Capture Reporting failed programs in past 24 hours. Publish and distribute report via email to Sales, SC, Finance, GL, Rebates functional teams. Sample report attached.','','','','MR020532','EIS_XXWC_DAILY_FAILED_PGMS_V','Y','','','MR020532','','N','WC Knowledge Management Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - WC Daily Failed Concurrent Programs Rpt
xxeis.eis_rs_ins.rc( 'WC Daily Failed Concurrent Programs Rpt',85000,'COMPLETION_TEXT','Completion Text','Completion Text','','','','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_FAILED_PGMS_V','','');
xxeis.eis_rs_ins.rc( 'WC Daily Failed Concurrent Programs Rpt',85000,'CONCURRENT_PROGRAM_NAME','Concurrent Program Name','Concurrent Program Name','','','','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_FAILED_PGMS_V','','');
xxeis.eis_rs_ins.rc( 'WC Daily Failed Concurrent Programs Rpt',85000,'DATE_COMPLETED','Date Completed','Date Completed','','','','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_FAILED_PGMS_V','','');
xxeis.eis_rs_ins.rc( 'WC Daily Failed Concurrent Programs Rpt',85000,'DATE_STARTED','Date Started','Date Started','','','','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_FAILED_PGMS_V','','');
xxeis.eis_rs_ins.rc( 'WC Daily Failed Concurrent Programs Rpt',85000,'PARAMETERS','Parameters','Parameters','','','','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_FAILED_PGMS_V','','');
xxeis.eis_rs_ins.rc( 'WC Daily Failed Concurrent Programs Rpt',85000,'REQUESTOR','Requestor','Requestor','','','','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_FAILED_PGMS_V','','');
xxeis.eis_rs_ins.rc( 'WC Daily Failed Concurrent Programs Rpt',85000,'REQUEST_ID','Request Id','Request Id','','','','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_FAILED_PGMS_V','','');
xxeis.eis_rs_ins.rc( 'WC Daily Failed Concurrent Programs Rpt',85000,'STATUS','Status','Status','','','','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_DAILY_FAILED_PGMS_V','','');
--Inserting Report Parameters - WC Daily Failed Concurrent Programs Rpt
--Inserting Report Conditions - WC Daily Failed Concurrent Programs Rpt
--Inserting Report Sorts - WC Daily Failed Concurrent Programs Rpt
--Inserting Report Triggers - WC Daily Failed Concurrent Programs Rpt
--Inserting Report Templates - WC Daily Failed Concurrent Programs Rpt
--Inserting Report Portals - WC Daily Failed Concurrent Programs Rpt
--Inserting Report Dashboards - WC Daily Failed Concurrent Programs Rpt
--Inserting Report Security - WC Daily Failed Concurrent Programs Rpt
xxeis.eis_rs_ins.rsec( 'WC Daily Failed Concurrent Programs Rpt','20005','','50900',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC Daily Failed Concurrent Programs Rpt','20005','','51207',85000,'MR020532','','');
--Inserting Report Pivots - WC Daily Failed Concurrent Programs Rpt
END;
/
set scan on define on
