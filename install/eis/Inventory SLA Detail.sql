--Report Name            : Inventory SLA Detail
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Inventory SLA Detail
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_SLA_QUERY
xxeis.eis_rs_ins.v( 'XXEIS_SLA_QUERY',401,'','','','','SA059956','XXEIS','Xxeis Sla Query','XSQ','','');
--Delete View Columns for XXEIS_SLA_QUERY
xxeis.eis_rs_utility.delete_view_rows('XXEIS_SLA_QUERY',401,FALSE);
--Inserting View Columns for XXEIS_SLA_QUERY
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','UNROUNDED_ACCOUNTED_CR',401,'Unrounded Accounted Cr','UNROUNDED_ACCOUNTED_CR','','','','SA059956','NUMBER','','','Unrounded Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF10',401,'Ref10','REF10','','','','SA059956','DATE','','','Ref10','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF2',401,'Ref2','REF2','','','','SA059956','VARCHAR2','','','Ref2','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','ENTERED_CR',401,'Entered Cr','ENTERED_CR','','','','SA059956','NUMBER','','','Entered Cr','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF6',401,'Ref6','REF6','','','','SA059956','VARCHAR2','','','Ref6','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF14',401,'Ref14','REF14','','','','SA059956','NUMBER','','','Ref14','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','UNROUNDED_ACCOUNTED_DR',401,'Unrounded Accounted Dr','UNROUNDED_ACCOUNTED_DR','','','','SA059956','NUMBER','','','Unrounded Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','PERIOD_NAME',401,'Period Name','PERIOD_NAME','','','','SA059956','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','ACCOUNTED_CR',401,'Accounted Cr','ACCOUNTED_CR','','','','SA059956','NUMBER','','','Accounted Cr','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF12',401,'Ref12','REF12','','','','SA059956','NUMBER','','','Ref12','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','ACCOUNTED_DR',401,'Accounted Dr','ACCOUNTED_DR','','','','SA059956','NUMBER','','','Accounted Dr','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF7',401,'Ref7','REF7','','','','SA059956','VARCHAR2','','','Ref7','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF1',401,'Ref1','REF1','','','','SA059956','VARCHAR2','','','Ref1','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF4',401,'Ref4','REF4','','','','SA059956','VARCHAR2','','','Ref4','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','GL_SL_LINK_TABLE',401,'Gl Sl Link Table','GL_SL_LINK_TABLE','','','','SA059956','VARCHAR2','','','Gl Sl Link Table','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','GL_SL_LINK_ID',401,'Gl Sl Link Id','GL_SL_LINK_ID','','','','SA059956','NUMBER','','','Gl Sl Link Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF5',401,'Ref5','REF5','','','','SA059956','VARCHAR2','','','Ref5','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF8',401,'Ref8','REF8','','','','SA059956','VARCHAR2','','','Ref8','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','AE_HEADER_ID',401,'Ae Header Id','AE_HEADER_ID','','','','SA059956','NUMBER','','','Ae Header Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF11',401,'Ref11','REF11','','','','SA059956','NUMBER','','','Ref11','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','ENTERED_DR',401,'Entered Dr','ENTERED_DR','','','','SA059956','NUMBER','','','Entered Dr','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','EVENT_TYPE_CODE',401,'Event Type Code','EVENT_TYPE_CODE','','','','SA059956','VARCHAR2','','','Event Type Code','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','CONCATENATED_SEGMENTS',401,'Concatenated Segments','CONCATENATED_SEGMENTS','','','','SA059956','VARCHAR2','','','Concatenated Segments','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','ACCOUNTING_CLASS_CODE',401,'Accounting Class Code','ACCOUNTING_CLASS_CODE','','','','SA059956','VARCHAR2','','','Accounting Class Code','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF13',401,'Ref13','REF13','','','','SA059956','NUMBER','','','Ref13','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','ACCOUNTING_DATE',401,'Accounting Date','ACCOUNTING_DATE','','','','SA059956','DATE','','','Accounting Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','JE_CATEGORY_NAME',401,'Je Category Name','JE_CATEGORY_NAME','','','','SA059956','VARCHAR2','','','Je Category Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF9',401,'Ref9','REF9','','','','SA059956','DATE','','','Ref9','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_SLA_QUERY','REF3',401,'Ref3','REF3','','','','SA059956','VARCHAR2','','','Ref3','','','');
--Inserting View Components for XXEIS_SLA_QUERY
--Inserting View Component Joins for XXEIS_SLA_QUERY
END;
/
set scan on define on
prompt Creating Report LOV Data for Inventory SLA Detail
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Inventory SLA Detail
xxeis.eis_rs_ins.lov( 401,'select  distinct per.period_name , per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     led.accounted_period_type = per.period_type
order by per.period_year desc,per.period_num asc','null','EIS INV PERIOD NAME','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct concatenated_segments
from apps.gl_code_combinations_kfv','','XXWC ACCRUAL_ACCOUNT','ACCRUAL ACCOUNT FROM THE apps.gl_code_combinations_kfv TABLE','ID020048',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Inventory SLA Detail
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory SLA Detail
xxeis.eis_rs_utility.delete_report_rows( 'Inventory SLA Detail' );
--Inserting Report - Inventory SLA Detail
xxeis.eis_rs_ins.r( 401,'Inventory SLA Detail','','','','','','SA059956','XXEIS_SLA_QUERY','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Inventory SLA Detail
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'ACCOUNTED_CR','Accounted Cr','Accounted Cr','','','','','9','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'ACCOUNTED_DR','Accounted Dr','Accounted Dr','','','','','8','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'ACCOUNTING_CLASS_CODE','Accounting Class Code','Accounting Class Code','','','','','7','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'ACCOUNTING_DATE','Accounting Date','Accounting Date','','','','','3','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'AE_HEADER_ID','Ae Header Id','Ae Header Id','','','','','5','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'CONCATENATED_SEGMENTS','Concatenated Segments','Concatenated Segments','','','','','1','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'ENTERED_CR','Entered Cr','Entered Cr','','','','','11','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'ENTERED_DR','Entered Dr','Entered Dr','','','','','10','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'EVENT_TYPE_CODE','Event Type Code','Event Type Code','','','','','2','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'GL_SL_LINK_ID','Gl Sl Link Id','Gl Sl Link Id','','','','','14','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'GL_SL_LINK_TABLE','Gl Sl Link Table','Gl Sl Link Table','','','','','15','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'JE_CATEGORY_NAME','Je Category Name','Je Category Name','','','','','6','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'PERIOD_NAME','Period Name','Period Name','','','','','4','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF1','Ref1','Ref1','','','','','16','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF10','Ref10','Ref10','','','','','25','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF11','Ref11','Ref11','','','','','26','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF12','Ref12','Ref12','','','','','27','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF13','Ref13','Ref13','','','','','28','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF14','Ref14','Ref14','','','','','29','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF2','Ref2','Ref2','','','','','17','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF3','Ref3','Ref3','','','','','18','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF4','Ref4','Ref4','','','','','19','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF5','Ref5','Ref5','','','','','20','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF6','Ref6','Ref6','','','','','21','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF7','Ref7','Ref7','','','','','22','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF8','Ref8','Ref8','','','','','23','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'REF9','Ref9','Ref9','','','','','24','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'UNROUNDED_ACCOUNTED_CR','Unrounded Accounted Cr','Unrounded Accounted Cr','','','','','13','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
xxeis.eis_rs_ins.rc( 'Inventory SLA Detail',401,'UNROUNDED_ACCOUNTED_DR','Unrounded Accounted Dr','Unrounded Accounted Dr','','','','','12','N','','','','','','','','SA059956','N','N','','XXEIS_SLA_QUERY','','');
--Inserting Report Parameters - Inventory SLA Detail
xxeis.eis_rs_ins.rp( 'Inventory SLA Detail',401,'Period_Name','','PERIOD_NAME','IN','EIS INV PERIOD NAME','','VARCHAR2','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory SLA Detail',401,'Accrual Account','','CONCATENATED_SEGMENTS','IN','XXWC ACCRUAL_ACCOUNT','','VARCHAR2','N','Y','2','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Inventory SLA Detail
xxeis.eis_rs_ins.rcn( 'Inventory SLA Detail',401,'PERIOD_NAME','IN',':Period_Name','','','Y','1','Y','SA059956');
xxeis.eis_rs_ins.rcn( 'Inventory SLA Detail',401,'CONCATENATED_SEGMENTS','IN',':Accrual Account','','','Y','2','Y','SA059956');
--Inserting Report Sorts - Inventory SLA Detail
--Inserting Report Triggers - Inventory SLA Detail
--Inserting Report Templates - Inventory SLA Detail
--Inserting Report Portals - Inventory SLA Detail
--Inserting Report Dashboards - Inventory SLA Detail
--Inserting Report Security - Inventory SLA Detail
xxeis.eis_rs_ins.rsec( 'Inventory SLA Detail','707','','51104',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Inventory SLA Detail','','SS084202','',401,'SA059956','','');
--Inserting Report Pivots - Inventory SLA Detail
END;
/
set scan on define on
