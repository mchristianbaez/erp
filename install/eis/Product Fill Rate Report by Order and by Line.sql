--Report Name            : Product Fill Rate Report by Order and by Line
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_INV_PROD_FILL_RATE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_PROD_FILL_RATE_V
Create or replace View XXEIS.EIS_XXWC_INV_PROD_FILL_RATE_V
 AS 
SELECT common_output_id ,
    process_id PROCESS_ID,
    varchar2_col1 bin_loc,
    varchar2_col2 bpm_rpm,
    varchar2_col3 part_number,
    varchar2_col4 source,
    varchar2_col5 part_desc,
    number_col1 rvw,
    number_col2 lt,
    varchar2_col6 cl,
    varchar2_col7 stk,
    number_col3 MIN,
    number_col4 MAX,
    varchar2_col8 amu,
    varchar2_col9 lg,
    number_col5 hit4_sales,
    number_col6 hit6_sales,
    number_col7 l_30_bo,
    number_col8 l_60_bo,
    number_col9 l_90_bo,
    number_col10 qty_ord,
    number_col11 shipped,
    number_col12 qty_bo,
    varchar2_col10 oh,
    number_col13 qoh,
    number_col14 on_ord,
    number_col15 avail,
    varchar2_col11 buyer,
    varchar2_col12 region,
    varchar2_col13 dist,
    number_col16 jan_sales,
    number_col17 FEB_SALES,
    number_col18 mar_sales,
    number_col19 apr_sales,
    number_col20 MAY_SALES,
    number_col21 JUNE_SALES,
    number_col22 jul_sales,
    number_col23 AUG_SALES,
    number_col24 sep_sales,
    number_col25 OCT_SALES,
    number_col26 nov_sales,
    number_col27 dec_sales,
    number_col28 comp_orders,
    number_col29 bo_orders,
    number_col30 total_orders,
    number_col31 comp_lines,
    number_col32 bo_lines,
    NUMBER_COL33 TOTAL_LINES,
    VARCHAR2_COL14 REPORT_TYPE,
    VARCHAR2_COL15 VELOCITY_CLASS,
    VARCHAR2_COL16 location,
    VARCHAR2_COL17 SBUYER,
    number_col34 bl_comp_lines,
    number_col35 bl_bo_lines,
    NUMBER_COL36 BL_TOTAL_LINES,
    VARCHAR2_COL18 SVELOCITY,
    NUMBER_COL37 SVL_COMP_LINES,
    NUMBER_COL38 SVL_BO_LINES,
    NUMBER_COL39 SVL_TOTAL_LINES,
    VARCHAR2_COL19 branch,
    VARCHAR2_COL20 ORDER_NUMBER,    --- ORDER_NUMBER
    VARCHAR2_COL21 Customer_NUMBER, --- Customer_NUMBER
    VARCHAR2_COL22 SALESREP_NAME,   --- SALES REP NAME
    VARCHAR2_COL23 BO_FLAG,         --- BO_FLAG
    VARCHAR2_COL24 BO_Region,       --- Region
    VARCHAR2_COL25 BO_District,     --- District
    VARCHAR2_COL26 ORG_NAME,        ---
    NUMBER_COL40 AVAIL2,
    VARCHAR2_COL27 REG_SUM_DPM_RPM,
    VARCHAR2_COL28 Buyer_DPM_SUM,
    VARCHAR2_COL29 SV_DPM_SUM,
    VARCHAR2_COL30 VENDOR_NUM, --- Vendor#
    VARCHAR2_COL31 VENDOR_NAME,--- Vendor_name
    NUMBER_COL41 PPLT,         --- Vendor#
    NUMBER_COL42 PLT,          --- Vendor_name
    NUMBER_COL43 CLT,          --- Vendor#
    NUMBER_COL44 Safety_Stock, --- Vendor_name
    NUMBER_COL45 Source_ORGS,
    VARCHAR2_COL32 UOM,
    VARCHAR2_COL33 RES_QTY,
    VARCHAR2_COL34 Cat,
    VARCHAR2_COL35 ST,
    NUMBER_COL46 REG_BK_COMP_LINES,
    NUMBER_COL47 BY_BK_COMP_LINES,
    NUMBER_COL48 SVL_BK_COMP_LINES,
    NUMBER_COL49 LOC_COMP_ORDERS,
    NUMBER_COL50 LOC_BO_ORDERS,
    NUMBER_COL51 LOC_TOT_ORDERS,
    NUMBER_COL52 LOC_COMP_LINES,
    NUMBER_COL53 LOC_BO_LINES,
    NUMBER_COL54 LOC_TOT_LINES,
    NUMBER_COL54 LOC_BK_COMP_LINES,
    VARCHAR2_COL36 LOC_REG_SUM,
    VARCHAR2_COL37 LOC_DIST_SUM,
    VARCHAR2_COL38 LOC_ORG_SUM,
    VARCHAR2_COL39 LOC_STK_SUM,
    VARCHAR2_COL40 LOC_DPM_RPM_SUM,
    NUMBER_COL56 LOCATION_COMP_LINES, -- LOCATION_COMP_LINES,
    NUMBER_COL57 LOCATION_BO_LINES, -- LOCATION_BO_LINES,
    NUMBER_COL58 LOCATION_TOT_LINES,-- LOC_TOT_LINES,
    NUMBER_COL59 LOCATION_BK_COMP_LINES,-- LOC_BK_COMP_LINES,
    VARCHAR2_COL41 LOCATION_REG_SUM,-- LOC_REG_SUM,
    VARCHAR2_COL42 LOCATION_DIST_SUM,-- LOC_DIST_SUM,
    VARCHAR2_COL43 LOCATION_ORG_SUM,-- LOC_ORG_SUM,
    VARCHAR2_COL44 LOCATION_STK_SUM,-- LOC_STK_SUM,
    VARCHAR2_COL45 LOCATION_DPM_RPM_SUM-- LOC_DPM_RPM_SUM
  FROM XXEIS.EIS_RS_COMMON_OUTPUTS
/
set scan on define on
prompt Creating View Data for Product Fill Rate Report by Order and by Line
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_PROD_FILL_RATE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_PROD_FILL_RATE_V',201,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Inv Prod Fill Rate V','EXIPFRV','','');
--Delete View Columns for EIS_XXWC_INV_PROD_FILL_RATE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_PROD_FILL_RATE_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_INV_PROD_FILL_RATE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','JAN_SALES',201,'Jan Sales','JAN_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Jan Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','DIST',201,'Dist','DIST','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Dist','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','REGION',201,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BUYER',201,'Buyer','BUYER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','AVAIL',201,'Avail','AVAIL','','','','XXEIS_RS_ADMIN','NUMBER','','','Avail','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','ON_ORD',201,'On Ord','ON_ORD','','','','XXEIS_RS_ADMIN','NUMBER','','','On Ord','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','QOH',201,'Qoh','QOH','','','','XXEIS_RS_ADMIN','NUMBER','','','Qoh','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','OH',201,'Oh','OH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oh','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','QTY_BO',201,'Qty Bo','QTY_BO','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty Bo','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SHIPPED',201,'Shipped','SHIPPED','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipped','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','QTY_ORD',201,'Qty Ord','QTY_ORD','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty Ord','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','L_90_BO',201,'L 90 Bo','L_90_BO','','','','XXEIS_RS_ADMIN','NUMBER','','','L 90 Bo','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','L_60_BO',201,'L 60 Bo','L_60_BO','','','','XXEIS_RS_ADMIN','NUMBER','','','L 60 Bo','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','L_30_BO',201,'L 30 Bo','L_30_BO','','','','XXEIS_RS_ADMIN','NUMBER','','','L 30 Bo','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','HIT6_SALES',201,'Hit6 Sales','HIT6_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Hit6 Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LG',201,'Lg','LG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Lg','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','AMU',201,'Amu','AMU','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','MAX',201,'Max','MAX','','','','XXEIS_RS_ADMIN','NUMBER','','','Max','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','MIN',201,'Min','MIN','','','','XXEIS_RS_ADMIN','NUMBER','','','Min','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','STK',201,'Stk','STK','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Stk','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','CL',201,'Cl','CL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cl','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LT',201,'Lt','LT','','','','XXEIS_RS_ADMIN','NUMBER','','','Lt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','RVW',201,'Rvw','RVW','','','','XXEIS_RS_ADMIN','NUMBER','','','Rvw','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','PART_DESC',201,'Part Desc','PART_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SOURCE',201,'Source','SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','PART_NUMBER',201,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BPM_RPM',201,'Bpm Rpm','BPM_RPM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bpm Rpm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BO_ORDERS',201,'Bo Orders','BO_ORDERS','','','','XXEIS_RS_ADMIN','NUMBER','','','Bo Orders','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','COMP_ORDERS',201,'Comp Orders','COMP_ORDERS','','','','XXEIS_RS_ADMIN','NUMBER','','','Comp Orders','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','DEC_SALES',201,'Dec Sales','DEC_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Dec Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','NOV_SALES',201,'Nov Sales','NOV_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Nov Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','OCT_SALES',201,'Oct Sales','OCT_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Oct Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SEP_SALES',201,'Sep Sales','SEP_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sep Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','AUG_SALES',201,'Aug Sales','AUG_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Aug Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','JUL_SALES',201,'Jul Sales','JUL_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Jul Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','JUNE_SALES',201,'June Sales','JUNE_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','June Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','MAY_SALES',201,'May Sales','MAY_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','May Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','APR_SALES',201,'Apr Sales','APR_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Apr Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','MAR_SALES',201,'Mar Sales','MAR_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Mar Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','FEB_SALES',201,'Feb Sales','FEB_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Feb Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','REPORT_TYPE',201,'Report Type','REPORT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','TOTAL_LINES',201,'Total Lines','TOTAL_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Total Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BO_LINES',201,'Bo Lines','BO_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Bo Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','COMP_LINES',201,'Comp Lines','COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','TOTAL_ORDERS',201,'Total Orders','TOTAL_ORDERS','','','','XXEIS_RS_ADMIN','NUMBER','','','Total Orders','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION',201,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','VELOCITY_CLASS',201,'Velocity Class','VELOCITY_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Velocity Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SBUYER',201,'Sbuyer','SBUYER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sbuyer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BL_BO_LINES',201,'Bl Bo Lines','BL_BO_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Bl Bo Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BL_COMP_LINES',201,'Bl Comp Lines','BL_COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Bl Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BL_TOTAL_LINES',201,'Bl Total Lines','BL_TOTAL_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Bl Total Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SVELOCITY',201,'Svelocity','SVELOCITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Svelocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SVL_BO_LINES',201,'Svl Bo Lines','SVL_BO_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Svl Bo Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SVL_COMP_LINES',201,'Svl Comp Lines','SVL_COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Svl Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SVL_TOTAL_LINES',201,'Svl Total Lines','SVL_TOTAL_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Svl Total Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','HIT4_SALES',201,'Hit4 Sales','HIT4_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Hit4 Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BIN_LOC',201,'Bin Loc','BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','COMMON_OUTPUT_ID',201,'Common Output Id','COMMON_OUTPUT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Common Output Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BRANCH',201,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','AVAIL2',201,'Avail2','AVAIL2','','','','XXEIS_RS_ADMIN','NUMBER','','','Avail2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BO_DISTRICT',201,'Bo District','BO_DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bo District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BO_FLAG',201,'Bo Flag','BO_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bo Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BO_REGION',201,'Bo Region','BO_REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bo Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BUYER_DPM_SUM',201,'Buyer Dpm Sum','BUYER_DPM_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer Dpm Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','CUSTOMER_NUMBER',201,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','ORDER_NUMBER',201,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','ORG_NAME',201,'Org Name','ORG_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Org Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','REG_SUM_DPM_RPM',201,'Reg Sum Dpm Rpm','REG_SUM_DPM_RPM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reg Sum Dpm Rpm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SALESREP_NAME',201,'Salesrep Name','SALESREP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SV_DPM_SUM',201,'Sv Dpm Sum','SV_DPM_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sv Dpm Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','VENDOR_NUM',201,'Vendor Num','VENDOR_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','CLT',201,'Clt','CLT','','','','XXEIS_RS_ADMIN','NUMBER','','','Clt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','PLT',201,'Plt','PLT','','','','XXEIS_RS_ADMIN','NUMBER','','','Plt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','PPLT',201,'Pplt','PPLT','','','','XXEIS_RS_ADMIN','NUMBER','','','Pplt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','RES_QTY',201,'Res Qty','RES_QTY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Res Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SAFETY_STOCK',201,'Safety Stock','SAFETY_STOCK','','','','XXEIS_RS_ADMIN','NUMBER','','','Safety Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SOURCE_ORGS',201,'Source Orgs','SOURCE_ORGS','','','','XXEIS_RS_ADMIN','NUMBER','','','Source Orgs','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','UOM',201,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','CAT',201,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','ST',201,'St','ST','','','','XXEIS_RS_ADMIN','VARCHAR2','','','St','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','BY_BK_COMP_LINES',201,'By Bk Comp Lines','BY_BK_COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','By Bk Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_BK_COMP_LINES',201,'Loc Bk Comp Lines','LOC_BK_COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Loc Bk Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_BO_LINES',201,'Loc Bo Lines','LOC_BO_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Loc Bo Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_BO_ORDERS',201,'Loc Bo Orders','LOC_BO_ORDERS','','','','XXEIS_RS_ADMIN','NUMBER','','','Loc Bo Orders','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_COMP_LINES',201,'Loc Comp Lines','LOC_COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Loc Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_COMP_ORDERS',201,'Loc Comp Orders','LOC_COMP_ORDERS','','','','XXEIS_RS_ADMIN','NUMBER','','','Loc Comp Orders','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_DIST_SUM',201,'Loc Dist Sum','LOC_DIST_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc Dist Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_DPM_RPM_SUM',201,'Loc Dpm Rpm Sum','LOC_DPM_RPM_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc Dpm Rpm Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_ORG_SUM',201,'Loc Org Sum','LOC_ORG_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc Org Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_REG_SUM',201,'Loc Reg Sum','LOC_REG_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc Reg Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_STK_SUM',201,'Loc Stk Sum','LOC_STK_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc Stk Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_TOT_LINES',201,'Loc Tot Lines','LOC_TOT_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Loc Tot Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOC_TOT_ORDERS',201,'Loc Tot Orders','LOC_TOT_ORDERS','','','','XXEIS_RS_ADMIN','NUMBER','','','Loc Tot Orders','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','REG_BK_COMP_LINES',201,'Reg Bk Comp Lines','REG_BK_COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Reg Bk Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','SVL_BK_COMP_LINES',201,'Svl Bk Comp Lines','SVL_BK_COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Svl Bk Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION_BK_COMP_LINES',201,'Location Bk Comp Lines','LOCATION_BK_COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Location Bk Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION_BO_LINES',201,'Location Bo Lines','LOCATION_BO_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Location Bo Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION_COMP_LINES',201,'Location Comp Lines','LOCATION_COMP_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Location Comp Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION_DIST_SUM',201,'Location Dist Sum','LOCATION_DIST_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location Dist Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION_DPM_RPM_SUM',201,'Location Dpm Rpm Sum','LOCATION_DPM_RPM_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location Dpm Rpm Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION_ORG_SUM',201,'Location Org Sum','LOCATION_ORG_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location Org Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION_REG_SUM',201,'Location Reg Sum','LOCATION_REG_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location Reg Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION_STK_SUM',201,'Location Stk Sum','LOCATION_STK_SUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location Stk Sum','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_PROD_FILL_RATE_V','LOCATION_TOT_LINES',201,'Location Tot Lines','LOCATION_TOT_LINES','','','','XXEIS_RS_ADMIN','NUMBER','','','Location Tot Lines','','','');
--Inserting View Components for EIS_XXWC_INV_PROD_FILL_RATE_V
--Inserting View Component Joins for EIS_XXWC_INV_PROD_FILL_RATE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Product Fill Rate Report by Order and by Line
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Product Fill Rate Report by Order and by Line
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,ood.organization_name organization_name  FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE EXISTS(SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V WHERE organization_id = ood.organization_id) ORDER BY organization_code','','PO Organization Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Product Fill Rate Report by Order and by Line
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Product Fill Rate Report by Order and by Line
xxeis.eis_rs_utility.delete_report_rows( 'Product Fill Rate Report by Order and by Line' );
--Inserting Report - Product Fill Rate Report by Order and by Line
xxeis.eis_rs_ins.r( 201,'Product Fill Rate Report by Order and by Line','','','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_PROD_FILL_RATE_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Product Fill Rate Report by Order and by Line
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BPM_RPM','DPM/RPM','Bpm Rpm','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BUYER','Buyer','Buyer','','','default','','46','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'CL','Cl','Cl','','','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'COMP_LINES','Complete Lines','Comp Lines','','~~~','default','','65','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'OH','OH','Oh','CHAR','','default','','40','Y','','','','','','','case when (avail>0) and(qty_ord<=qoh) then ''X'' else null end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LG','LG','Lg','CHAR','','default','','29','Y','','','','','','','case when QTY_ORD>MAX then ''X'' else null end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'COMP_ORDERS','Complete Orders','Comp Orders','','~~~','default','','62','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'DEC_SALES','Dec','Dec Sales','','~~~','default','','58','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'DIST','By Reg District','Dist','','','default','','60','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'FEB_SALES','Feb','Feb Sales','','~~~','default','','48','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'HIT6_SALES','Hit6','Hit6 Sales','','~~~','default','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'JAN_SALES','Jan','Jan Sales','','~~~','default','','47','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'JUL_SALES','Jul','Jul Sales','','~~~','default','','53','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'JUNE_SALES','June','June Sales','','~~~','default','','52','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'L_30_BO','30','L 30 Bo','','~~~','default','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'L_60_BO','60','L 60 Bo','','~~~','default','','33','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'L_90_BO','90','L 90 Bo','','~~~','default','','34','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'MAR_SALES','Mar','Mar Sales','','~~~','default','','49','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'MAX','Max','Max','','~~~','default','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'MAY_SALES','May','May Sales','','~~~','default','','51','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'MIN','Min','Min','','~~~','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'NOV_SALES','Nov','Nov Sales','','~~~','default','','57','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'OCT_SALES','Oct','Oct Sales','','~~~','default','','56','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'ON_ORD','On Ord','On Ord','','~~~','default','','42','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'PART_DESC','Description','Part Desc','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'PART_NUMBER','Item Number','Part Number','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'QOH','QOH','Qoh','','~~~','default','','41','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'QTY_BO','Qty Bo','Qty Bo','','~~~','default','','37','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'QTY_ORD','Qty Ord','Qty Ord','','~~~','default','','35','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'REGION','By Region','Region','','','default','','59','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'REPORT_TYPE','Report Type','Report Type','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SEP_SALES','Sep','Sep Sales','','~~~','default','','55','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SHIPPED','Shipped','Shipped','','~~~','default','','36','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SOURCE','Source','Source','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'STK','Stk','Stk','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'TOTAL_LINES','Total Lines','Total Lines','','~~~','default','','67','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'TOTAL_ORDERS','Total Orders','Total Orders','','~~~','default','','64','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'AMU','Amu','Amu','','','default','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'APR_SALES','Apr','Apr Sales','','~~~','default','','50','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'AUG_SALES','Aug','Aug Sales','','~~~','default','','54','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'AVAIL','Avail','Avail','','~~~','default','','43','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BO_LINES','BO Lines','Bo Lines','','~~~','default','','66','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BO_ORDERS','BO Orders','Bo Orders','','~~~','default','','63','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOCATION','Org','Location','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'VELOCITY_CLASS','Velocity Class','Velocity Class','','','default','','71','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'AOS','Aver Order Size','Velocity Class','NUMBER','~~~','default','','31','Y','','','','','','','EXIPFRV.HIT6_SALES/decode(EXIPFRV.HIT4_SALES,0,1,EXIPFRV.HIT4_SALES)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SBUYER','By Buyer Name','Sbuyer','','','default','','70','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BL_BO_LINES','By Buyer Bo Lines','Bl Bo Lines','','~~~','default','','73','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BL_COMP_LINES','By Buyer Comp Lines','Bl Comp Lines','','~~~','default','','72','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BL_TOTAL_LINES','By Buyer Total Lines','Bl Total Lines','','~~~','default','','74','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SVELOCITY','By Cl','Svelocity','','','default','','76','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SVL_BO_LINES','By Cl Bo Lines','Svl Bo Lines','','~~~','default','','78','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SVL_COMP_LINES','By Cl Comp Lines','Svl Comp Lines','','~~~','default','','77','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SVL_TOTAL_LINES','By Cl Total Lines','Svl Total Lines','','~~~','default','','79','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'AVAIL2','Avail2','Avail2','','~~~','default','','44','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BO_DISTRICT','District','Bo District','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BO_FLAG','BO Flag','Bo Flag','','','default','','38','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BO_REGION','Region','Bo Region','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BUYER_DPM_SUM','By DPM/RPM','Buyer Dpm Sum','','','default','','69','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'CUSTOMER_NUMBER','Cust Num','Customer Number','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'ORDER_NUMBER','Ord Num','Order Number','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'ORG_NAME','Org Name','Org Name','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'REG_SUM_DPM_RPM','DPM/RPM','Reg Sum Dpm Rpm','','','default','','61','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SALESREP_NAME','Salesrep Name','Salesrep Name','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SV_DPM_SUM','By Cl DPM/RPM','Sv Dpm Sum','','','default','','75','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'VENDOR_NAME','Vendor','Vendor Name','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'VENDOR_NUM','Vendor#','Vendor Num','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'CLT','CLT','Clt','','~~~','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'PLT','PLT','Plt','','~~~','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'PPLT','PPLT','Pplt','','~~~','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'RES_QTY','Res Qty','Res Qty','','','default','','45','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SAFETY_STOCK','Safety Stock','Safety Stock','','~~~','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'SOURCE_ORGS','Source Orgs','Source Orgs','','~~~','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'UOM','Uom','Uom','','','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'CAT','CC','Cat','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'ST','St','St','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'BY_BK_COMP_LINES','By Buyer Booked Comp Lines','By Bk Comp Lines','','~~~','default','','81','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_BO_LINES','By Loc BO Lines','Loc Bo Lines','','~~~','default','','91','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_BO_ORDERS','By Loc BO Orders','Loc Bo Orders','','~~~','default','','88','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_COMP_LINES','By Loc Complete Lines','Loc Comp Lines','','~~~','default','','90','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_COMP_ORDERS','By Loc Complete Orders','Loc Comp Orders','','~~~','default','','89','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_DIST_SUM','By Loc District','Loc Dist Sum','','','default','','85','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_DPM_RPM_SUM','By Loc DPM/RPM','Loc Dpm Rpm Sum','','','default','','86','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_ORG_SUM','By Loc Org','Loc Org Sum','','','default','','82','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_REG_SUM','By Loc Region','Loc Reg Sum','','','default','','84','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_STK_SUM','BY Loc Stk','Loc Stk Sum','','','default','','83','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_TOT_LINES','By Loc Total Lines','Loc Tot Lines','','~~~','default','','92','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOC_TOT_ORDERS','By Loc Total Orders','Loc Tot Orders','','~~~','default','','87','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOCATION_BO_LINES','By Location BO Lines','Location Bo Lines','','~~~','default','','100','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOCATION_COMP_LINES','By Location Comp Lines','Location Comp Lines','','~~~','default','','99','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOCATION_DIST_SUM','By Location Dist','Location Dist Sum','','','default','','94','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOCATION_DPM_RPM_SUM','By Location DPM/RPM','Location Dpm Rpm Sum','','','default','','95','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOCATION_ORG_SUM','By Location Org','Location Org Sum','','','default','','96','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOCATION_REG_SUM','By Location Region','Location Reg Sum','','','default','','97','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOCATION_STK_SUM','By Location Stk','Location Stk Sum','','','default','','98','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
xxeis.eis_rs_ins.rc( 'Product Fill Rate Report by Order and by Line',201,'LOCATION_TOT_LINES','By Location Total Lines','Location Tot Lines','','~~~','default','','101','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_PROD_FILL_RATE_V','','');
--Inserting Report Parameters - Product Fill Rate Report by Order and by Line
xxeis.eis_rs_ins.rp( 'Product Fill Rate Report by Order and by Line',201,'Region','Region','','IN','Region Lov','','VARCHAR2','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Fill Rate Report by Order and by Line',201,'District','District','','IN','District Lov','','VARCHAR2','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Fill Rate Report by Order and by Line',201,'Location','Location','','IN','PO Organization Lov','','VARCHAR2','N','Y','3','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Fill Rate Report by Order and by Line',201,'Location List','Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Fill Rate Report by Order and by Line',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','5','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Fill Rate Report by Order and by Line',201,'Source List','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','6','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Fill Rate Report by Order and by Line',201,'Cat Class List','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','7','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Fill Rate Report by Order and by Line',201,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','8','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Product Fill Rate Report by Order and by Line
xxeis.eis_rs_ins.rcn( 'Product Fill Rate Report by Order and by Line',201,'EXIPFRV.PROCESS_ID','=',':SYSTEM.PROCESS_ID','','','Y','1','N','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Product Fill Rate Report by Order and by Line
xxeis.eis_rs_ins.rs( 'Product Fill Rate Report by Order and by Line',201,'REPORT_TYPE','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Product Fill Rate Report by Order and by Line
xxeis.eis_rs_ins.rt( 'Product Fill Rate Report by Order and by Line',201,'begin
XXEIS.EIS_INV_XXWC_PRODFILL_RATE_PKG.PROD_FILL_RATE(
                      P_PROCESS_ID       =>:SYSTEM.PROCESS_ID,
                      P_REGION              =>:Region,
                      P_DISTRICT           =>:District,
                      P_LOCATION          =>:Location,
                      P_LOCATION_LIST  =>:Location List,
                      P_VENDOR_LIST     =>:Vendor List,
                      P_SOURCE_LIST     =>:Source List,
                      P_CAT_CLASS_LIST=>:Cat Class List,
                      P_ITEM_LIST          =>:Item List                      
);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Product Fill Rate Report by Order and by Line
--Inserting Report Portals - Product Fill Rate Report by Order and by Line
--Inserting Report Dashboards - Product Fill Rate Report by Order and by Line
--Inserting Report Security - Product Fill Rate Report by Order and by Line
--Inserting Report Pivots - Product Fill Rate Report by Order and by Line
xxeis.eis_rs_ins.rpivot( 'Product Fill Rate Report by Order and by Line',201,'Line FR By Class','1','1,0|1,1,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Line FR By Class
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'Line FR By Class','SVL_COMP_LINES','DATA_FIELD','SUM','Comp Lines','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'Line FR By Class','SVL_BO_LINES','DATA_FIELD','SUM','BO Lines','3','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'Line FR By Class','SVL_TOTAL_LINES','DATA_FIELD','SUM','Total Lines','4','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'Line FR By Class','SVELOCITY','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'Line FR By Class','SV_DPM_SUM','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'Line FR By Class','REPORT_TYPE','PAGE_FIELD','','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Line FR By Class
xxeis.eis_rs_ins.rpivot_sum_cal( 'Product Fill Rate Report by Order and by Line',201,'Line FR By Class','(<EXIPFRV.SVL_COMP_LINES>/<EXIPFRV.SVL_TOTAL_LINES>)*100','Line Fill Rate%','1');
xxeis.eis_rs_ins.rpivot( 'Product Fill Rate Report by Order and by Line',201,'By RPM DPM','2','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - By RPM DPM
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By RPM DPM','REPORT_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By RPM DPM','BL_COMP_LINES','DATA_FIELD','SUM','Comp Lines','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By RPM DPM','BL_BO_LINES','DATA_FIELD','SUM','BO Lines','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By RPM DPM','BL_TOTAL_LINES','DATA_FIELD','SUM','Total Lines','3','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By RPM DPM','BUYER_DPM_SUM','ROW_FIELD','','','1','1','');
--Inserting Report Summary Calculation Columns For Pivot- By RPM DPM
xxeis.eis_rs_ins.rpivot_sum_cal( 'Product Fill Rate Report by Order and by Line',201,'By RPM DPM','(<EXIPFRV.BL_COMP_LINES>/<EXIPFRV.BL_TOTAL_LINES>)*100','Line Fill Rate%','1');
xxeis.eis_rs_ins.rpivot( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','3','1,0|1,1,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - By Region & District
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','REGION','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','TOTAL_LINES','DATA_FIELD','SUM','Total Lines','7','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','TOTAL_ORDERS','DATA_FIELD','SUM','Total Orders','4','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','BO_LINES','DATA_FIELD','SUM','BO Lines','6','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','BO_ORDERS','DATA_FIELD','SUM','BO Orders','3','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','COMP_LINES','DATA_FIELD','SUM','Complete Lines','5','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','COMP_ORDERS','DATA_FIELD','SUM','Complete Orders','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','DIST','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','REPORT_TYPE','PAGE_FIELD','','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- By Region & District
xxeis.eis_rs_ins.rpivot_sum_cal( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','(<EXIPFRV.COMP_LINES>/<EXIPFRV.TOTAL_LINES>)*100','Line Fill Rate%','2');
xxeis.eis_rs_ins.rpivot_sum_cal( 'Product Fill Rate Report by Order and by Line',201,'By Region & District','(<EXIPFRV.COMP_ORDERS>/<EXIPFRV.TOTAL_ORDERS>)*100','Order Fill Rate%','1');
xxeis.eis_rs_ins.rpivot( 'Product Fill Rate Report by Order and by Line',201,'By Loc','4','0,0|1,1,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - By Loc
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','REPORT_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_TOT_ORDERS','DATA_FIELD','SUM','Total Orders','3','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_BO_ORDERS','DATA_FIELD','SUM','BO Orders','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_COMP_ORDERS','DATA_FIELD','SUM','Complete Orders','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_COMP_LINES','DATA_FIELD','SUM','Complete Lines','4','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_BO_LINES','DATA_FIELD','SUM','BO Lines','5','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_TOT_LINES','DATA_FIELD','SUM','Total Lines','6','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_ORG_SUM','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_DIST_SUM','ROW_FIELD','','','2','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_REG_SUM','ROW_FIELD','','','3','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Loc','LOC_DPM_RPM_SUM','ROW_FIELD','','','4','0','');
--Inserting Report Summary Calculation Columns For Pivot- By Loc
xxeis.eis_rs_ins.rpivot_sum_cal( 'Product Fill Rate Report by Order and by Line',201,'By Loc','(<EXIPFRV.LOC_COMP_ORDERS>/<EXIPFRV.LOC_TOT_ORDERS>)*100','Order Fill Rate%','1');
xxeis.eis_rs_ins.rpivot_sum_cal( 'Product Fill Rate Report by Order and by Line',201,'By Loc','(<EXIPFRV.LOC_COMP_LINES>/<EXIPFRV.LOC_TOT_LINES>)*100','Line Fill Rate%','2');
xxeis.eis_rs_ins.rpivot( 'Product Fill Rate Report by Order and by Line',201,'By Location','5','0,0|1,1,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - By Location
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Location','REPORT_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Location','LOCATION_COMP_LINES','DATA_FIELD','SUM','Complete Lines','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Location','LOCATION_BO_LINES','DATA_FIELD','SUM','BO Lines','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Location','LOCATION_TOT_LINES','DATA_FIELD','SUM','Total Lines','3','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Location','LOCATION_ORG_SUM','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Location','LOCATION_STK_SUM','ROW_FIELD','','','2','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Location','LOCATION_REG_SUM','ROW_FIELD','','','3','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Location','LOCATION_DIST_SUM','ROW_FIELD','','','4','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Product Fill Rate Report by Order and by Line',201,'By Location','LOCATION_DPM_RPM_SUM','ROW_FIELD','','','5','0','');
--Inserting Report Summary Calculation Columns For Pivot- By Location
xxeis.eis_rs_ins.rpivot_sum_cal( 'Product Fill Rate Report by Order and by Line',201,'By Location','(<EXIPFRV.LOCATION_COMP_LINES>/<EXIPFRV.LOCATION_TOT_LINES>)*100','Line Fill Rate%','1');
END;
/
set scan on define on
