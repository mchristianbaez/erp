--Report Name            : Daily Flash Charge  NEW July 10
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_DAILY_FLASH_NEW_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_DAILY_FLASH_NEW_V
Create or replace View XXEIS.EIS_XXWC_AR_DAILY_FLASH_NEW_V
(DATE1,BRANCH_LOCATION,SALE_AMT,SALE_COST,PAYMENT_TYPE,INVOICE_COUNT) AS 
SELECT 
    TRUNC (XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM) DATE1,
    OOD.ORGANIZATION_CODE BRANCH_LOCATION,
    SUM((DECODE(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PROMO_ITEM(OL.INVENTORY_ITEM_ID,OL.SHIP_FROM_ORG_ID), 'N', ( NVL (RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED) * NVL (OL.UNIT_SELLING_PRICE, 0)), 'Y', 0 ))+  NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CHARGER_AMT(OH.HEADER_ID),0)) SALE_AMT,
    SUM(DECODE(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PROMO_ITEM(OL.INVENTORY_ITEM_ID,OL.SHIP_FROM_ORG_ID), 'N', NVL (RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED)   * NVL (APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST (OL.LINE_ID) , 0), 'Y', -1*(NVL(RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED) * NVL (OL.UNIT_SELLING_PRICE, 0)))) SALE_COST,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE PAYMENT_TYPE,
    COUNT(DISTINCT XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DIST_INVOICE_NUM ( RCT.CUSTOMER_TRX_ID, RCT.TRX_NUMBER, OTLH.NAME)) INVOICE_COUNT
 ---Primary Keys
  FROM RA_CUSTOMER_TRX            RCT,
    RA_CUSTOMER_TRX_LINES         RCTL,
    OE_ORDER_HEADERS              OH,
    oe_order_lines                ol,
    org_organization_definitions  ood,
    OE_TRANSACTION_TYPES_VL       OTL,
    oe_transaction_types_vl       otlh
  WHERE 1 = 1
    --AND trunc(Rct.creation_date) = trunc(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
  AND rct.creation_date >= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 0.25
  AND rct.creation_date <= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 1.25
    --and TO_CHAR(RCT.CREATION_DATE,'DD-MON-YY HH:MM:SS')    >= TO_CHAR(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM ,'DD-MON-YY HH:MM:SS') +0.25
    -- AND TO_CHAR(Rct.creation_date ,'DD-MON-YY HH:MM:SS')   <= to_char(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from ,'DD-MON-YY HH:MM:SS') +1.25
  AND TO_CHAR (oh.order_number)                = TO_CHAR (rct.interface_header_attribute1)
  AND ood.organization_id(+)                   = ol.ship_from_org_id
  AND TO_CHAR (rctl.interface_line_attribute6) = TO_CHAR (ol.line_id)
  AND rctl.inventory_item_id                   = ol.inventory_item_id
  AND otl.transaction_type_id                  = ol.line_type_id
  AND otlh.transaction_type_id                 = oh.order_type_id
  AND RCTL.CUSTOMER_TRX_ID                     = RCT.CUSTOMER_TRX_ID
 -- and oh.order_number='10394217'
  --AND RCT.CUSTOMER_TRX_ID IN (2737270,2737271)
  AND ol.ordered_item NOT                     IN('CONTOFFSET','CONTBILL')
    /* AND NOT EXISTS(SELECT 1
    from dual
    where ol.ordered_item  in('CONTOFFSET','CONTBILL')
    )*/
  AND ol.header_id                 = oh.header_id
  AND rctl.interface_line_context  = 'ORDER ENTRY'
  AND rct.interface_header_context = 'ORDER ENTRY'
  AND NOT EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc,
      hz_cust_accounts hca
    WHERE hca.party_id       = hcp.party_id
    AND oh.sold_to_org_id    = hcp.cust_account_id
    AND hcp.site_use_id     IS NULL
    AND hcp.profile_class_id = hcpc.profile_class_id(+)
    AND hcpc.name LIKE 'WC%Branches%'
    )
  AND NOT EXISTS
    (SELECT 'Y'
    FROM  MTL_SYSTEM_ITEMS_B MSI,
          GL_CODE_COMBINATIONS_KFV GCC
    WHERE 1                     =1
    AND MSI.INVENTORY_ITEM_ID   = OL.INVENTORY_ITEM_ID
    AND MSI.ORGANIZATION_ID     = OL.SHIP_FROM_ORG_ID
    AND GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
    AND GCC.SEGMENT4            ='646080'
    )
    /* AND NOT EXISTS
    (SELECT 1
    FROM OE_PRICE_ADJUSTMENTS_V
    WHERE HEADER_ID     = OL.HEADER_ID
    AND LINE_ID         = OL.LINE_ID
    AND adjustment_name ='BRANCH_COST_MODIFIER'
    )*/
  AND ( ( xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type = 'CASH'
  AND ( EXISTS
    (SELECT 1
    FROM oe_payments oep
    WHERE ( ( oep.payment_type_code IN ('CASH', 'CHECK', 'CREDIT_CARD')
    AND oep.header_id                = ol.header_id))
    )
  OR ( otl.order_category_code = 'RETURN'
  AND EXISTS
    (SELECT 1
    FROM xxwc_om_cash_refund_tbl xoc
    WHERE xoc.return_header_id = oh.header_id
    ))))
  OR ( xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type = 'CHARGE'
  AND ( ( NOT EXISTS
    (SELECT 1 FROM oe_payments oep WHERE oep.header_id = ol.header_id
    )
  AND otl.order_category_code <> 'RETURN')
  OR ( otl.order_category_code = 'RETURN'
  AND NOT EXISTS
    (SELECT 1
    FROM xxwc_om_cash_refund_tbl xoc1
    WHERE XOC1.RETURN_HEADER_ID = OH.HEADER_ID
    )))))GROUP BY TRUNC (XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM), OOD.ORGANIZATION_CODE, XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE/
set scan on define on
prompt Creating View Data for Daily Flash Charge  NEW July 10
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_DAILY_FLASH_NEW_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Daily Flash Sale V','EXADFSV','','');
--Delete View Columns for EIS_XXWC_AR_DAILY_FLASH_NEW_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_DAILY_FLASH_NEW_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_DAILY_FLASH_NEW_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','DATE1',660,'Date1','DATE1','','','','XXEIS_RS_ADMIN','DATE','','','Date1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','SALE_AMT',660,'Sale Amt','SALE_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Amt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','SALE_COST',660,'Sale Cost','SALE_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_NEW_V','PAYMENT_TYPE',660,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type','','','');
--Inserting View Components for EIS_XXWC_AR_DAILY_FLASH_NEW_V
--Inserting View Component Joins for EIS_XXWC_AR_DAILY_FLASH_NEW_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Charge  NEW July 10
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Charge  NEW July 10
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Charge  NEW July 10
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Charge  NEW July 10
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Charge  NEW July 10' );
--Inserting Report - Daily Flash Charge  NEW July 10
xxeis.eis_rs_ins.r( 660,'Daily Flash Charge  NEW July 10','','The purpose of this extract is to provide Finance with a daily report of all charge sales by branch.  The selected date parameter represents the desired date of sales (e.g. For sales reported on Aug 1st, set the Date parameter = Aug. 1st.  This report is to be processed daily and intended to accompany the daily extracts, flash_charge and flash_cash.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_DAILY_FLASH_NEW_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,HTML,EXCEL,','N');
--Inserting Report Columns - Daily Flash Charge  NEW July 10
xxeis.eis_rs_ins.rc( 'Daily Flash Charge  NEW July 10',660,'GROSS_PROFIT','GP$','','NUMBER','~T~D~2','default','','5','Y','','','','','','','(nvl(EXADFSV.SALE_AMT,0)-nvl(EXADFSV.sale_cost,0))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge  NEW July 10',660,'BRANCH_LOCATION','Location','Branch Location','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge  NEW July 10',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','~~~','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge  NEW July 10',660,'SALE_AMT','Net Sales Dollars','Sale Amt','','~T~D~2','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge  NEW July 10',660,'DATE1','Date','Date1','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_NEW_V','','');
--Inserting Report Parameters - Daily Flash Charge  NEW July 10
xxeis.eis_rs_ins.rp( 'Daily Flash Charge  NEW July 10',660,'Location','WC Branch Number','BRANCH_LOCATION','IN','OM Warehouse All','','VARCHAR2','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Charge  NEW July 10',660,'Date','Prior days date','DATE1','IN','','select trunc(sysdate-1) from dual','DATE','Y','Y','1','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Charge  NEW July 10
xxeis.eis_rs_ins.rcn( 'Daily Flash Charge  NEW July 10',660,'','','','','AND ( ''All'' IN (:Location) OR (BRANCH_LOCATION IN (:Location)))','Y','1','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Charge  NEW July 10
xxeis.eis_rs_ins.rs( 'Daily Flash Charge  NEW July 10',660,'BRANCH_LOCATION','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Daily Flash Charge  NEW July 10
xxeis.eis_rs_ins.rt( 'Daily Flash Charge  NEW July 10',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_payment_type(''CHARGE'');
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Charge  NEW July 10
--Inserting Report Portals - Daily Flash Charge  NEW July 10
--Inserting Report Dashboards - Daily Flash Charge  NEW July 10
--Inserting Report Security - Daily Flash Charge  NEW July 10
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge  NEW July 10','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge  NEW July 10','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge  NEW July 10','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge  NEW July 10','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge  NEW July 10','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge  NEW July 10','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge  NEW July 10','20005','','50900',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Daily Flash Charge  NEW July 10
xxeis.eis_rs_ins.rpivot( 'Daily Flash Charge  NEW July 10',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Charge  NEW July 10',660,'Pivot','GROSS_PROFIT','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Charge  NEW July 10',660,'Pivot','BRANCH_LOCATION','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Daily Flash Charge  NEW July 10',660,'Pivot','INVOICE_COUNT','DATA_FIELD','COUNT','Invoice Count','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
