--Report Name            : Daily Flash Delivery � NEW July 10
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
Create or replace View XXEIS.EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
(CURRENT_FISCAL_MONTH,CURRENT_FISCAL_YEAR,DELIVERY_TYPE,BRANCH_LOCATION,INVOICE_COUNT,SALES) AS 
SELECT  GP.PERIOD_NAME CURRENT_FISCAL_MONTH,
        gp.period_year current_fiscal_year,
        xxeis.eis_rs_xxwc_com_util_pkg.get_shipping_mthd3 ( ol.shipping_method_code) delivery_type,
        mp.organization_code branch_location,
        COUNT ( DISTINCT XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DIST_INVOICE_NUM ( RCT.CUSTOMER_TRX_ID, RCT.TRX_NUMBER, OTLH.NAME)) INVOICE_COUNT,
    --SUM ( NVL (rctl.quantity_invoiced, rctl.quantity_credited) * NVL (ol.unit_selling_price, 0)) sales
    --SUM(DECODE(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PROMO_ITEM(OL.INVENTORY_ITEM_ID,OL.SHIP_FROM_ORG_ID), 'N', ( NVL (RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED) * NVL (OL.UNIT_SELLING_PRICE, 0))+XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CHARGER_AMT(OH.HEADER_ID), 'Y', 0 )) sales
        SUM((DECODE(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PROMO_ITEM(OL.INVENTORY_ITEM_ID,OL.SHIP_FROM_ORG_ID), 'N', ( NVL (RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED) * NVL (OL.UNIT_SELLING_PRICE, 0)), 'Y', 0 ))+  NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CHARGER_AMT(OH.HEADER_ID),0)) SALES
    -- NVL(SUM(DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL(Ol.Unit_Selling_Price,0)),0) sales
    -- SUM(oep.payment_amount) cash_sales,
    --   xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type payment_type
  FROM ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    oe_order_headers oh,
    oe_order_lines ol,
    org_organization_definitions mp,
    gl_periods gp,
    gl_sets_of_books gsob,
    oe_transaction_types_vl otl,
    oe_transaction_types_vl otlh
WHERE 1 = 1
    --AND TRUNC(rct.creation_date)       = TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
    /* AND rct.creation_date >= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 0.25
    AND rct.creation_date <= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 1.25*/
  AND rctl.customer_trx_id                                   = rct.customer_trx_id
  AND RCTL.INTERFACE_LINE_ATTRIBUTE6                         = TO_CHAR (OL.LINE_ID)
  AND RCTl.INTERFACE_LINE_ATTRIBUTE1                       	 = TO_CHAR(OH.ORDER_NUMBER)
  AND rctl.interface_line_context(+)                         = 'ORDER ENTRY'
  AND RCT.INTERFACE_HEADER_CONTEXT(+)                        = 'ORDER ENTRY'
  and rct.interface_header_attribute1                        = to_char(oh.order_number)
  AND ol.header_id                                           = oh.header_id
  AND OTL.TRANSACTION_TYPE_ID                                = OL.LINE_TYPE_ID
  and otl.org_id                                             = ol.org_id
  AND OTLH.TRANSACTION_TYPE_ID                               = OH.ORDER_TYPE_ID
  and OTLH.org_id                                            = oh.org_id
  AND TO_CHAR (oh.order_number)                              = rct.interface_header_attribute1
  AND mp.organization_id(+)                                  = ol.ship_from_org_id
  AND gsob.set_of_books_id                                   = mp.set_of_books_id
  AND ol.ordered_item  NOT IN('CONTOFFSET','CONTBILL')
  and GSOB.PERIOD_SET_NAME                                   = GP.PERIOD_SET_NAME
    /*and ((XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PERIOD_NAME_FROM is null
    AND gp.period_name                                        IN
    (SELECT period_name
    FROM gl_periods gp1
    WHERE gp1.period_set_name = gp.period_set_name
    AND TRUNC(sysdate) BETWEEN TRUNC(gp.start_date) AND TRUNC(gp.end_date)
    ) )
  OR (xxeis.eis_rs_xxwc_com_util_pkg.get_period_name_from IS NOT NULL
  and GP.PERIOD_NAME                                       = XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PERIOD_NAME_FROM) )*/
  and GP.PERIOD_NAME  = xxeis.eis_rs_xxwc_com_util_pkg.get_period_name_from
  AND rct.creation_date BETWEEN gp.start_date+0.25 AND gp.end_date+1.25
    /*AND EXISTS
    (SELECT 1
    FROM oe_payments oep
    WHERE oep.header_id        = ol.header_id
    AND oep.payment_type_code IN ('CASH','CREDIT_CARD')
    )*/
    /* New Added*/
  AND NOT EXISTS
    (SELECT 'Y'
    FROM MTL_SYSTEM_ITEMS_B MSI,
      MTL_ITEM_CATEGORIES_V MIC,
      GL_CODE_COMBINATIONS_KFV GCC
    WHERE 1                     =1
    AND MSI.INVENTORY_ITEM_ID   = MIC.INVENTORY_ITEM_ID
    AND MSI.ORGANIZATION_ID     = MIC.ORGANIZATION_ID
    AND MSI.INVENTORY_ITEM_ID   = OL.INVENTORY_ITEM_ID
    AND MSI.ORGANIZATION_ID     = OL.SHIP_FROM_ORG_ID
    AND GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
    AND GCC.SEGMENT4            ='646080'
    )
    /* New Added*/
  AND NOT EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc,
      hz_cust_accounts hca
    WHERE hca.party_id       = hcp.party_id
    AND oh.sold_to_org_id    = hcp.cust_account_id
    AND hcp.site_use_id     IS NULL
    AND hcp.profile_class_id = hcpc.profile_class_id(+)
    AND hcpc.name LIKE 'WC%Branches%'
    )
    /*AND NOT EXISTS
    (SELECT 1
    FROM OE_PRICE_ADJUSTMENTS_V
    WHERE HEADER_ID     = OL.HEADER_ID
    AND LINE_ID         = OL.LINE_ID
    AND adjustment_name ='BRANCH_COST_MODIFIER'
    )*/
  GROUP BY GP.PERIOD_NAME, gp.period_year, xxeis.eis_rs_xxwc_com_util_pkg.get_shipping_mthd3 ( ol.shipping_method_code), mp.organization_code/
set scan on define on
prompt Creating View Data for Daily Flash Delivery � NEW July 10
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Flsh Sale By Del V','EXAFSBDV','','');
--Delete View Columns for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_FLSH_SALE_DL_NEW_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','CURRENT_FISCAL_MONTH',660,'Current Fiscal Month','CURRENT_FISCAL_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Current Fiscal Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','CURRENT_FISCAL_YEAR',660,'Current Fiscal Year','CURRENT_FISCAL_YEAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Current Fiscal Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','DELIVERY_TYPE',660,'Delivery Type','DELIVERY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Delivery Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','SALES',660,'Sales','SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count','','','');
--Inserting View Components for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
--Inserting View Component Joins for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Delivery � NEW July 10
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Delivery � NEW July 10
xxeis.eis_rs_ins.lov( 660,'select  distinct per.period_name , per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     led.accounted_period_type = per.period_type
order by per.period_num asc,
per.period_year desc','','OM PERIOD NAMES','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Delivery � NEW July 10
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Delivery � NEW July 10
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Delivery � NEW July 10' );
--Inserting Report - Daily Flash Delivery � NEW July 10
xxeis.eis_rs_ins.r( 660,'Daily Flash Delivery � NEW July 10','','The purpose of this extract is to provide Finance with a daily report of all sales by branch and aggregated by delivery type.  The selected date parameter represents the desired date of sales (e.g. For sales reported on Aug 1st, set the Date parameter = Aug. 1st.   This report is to be processed daily and intended to accompany the daily extracts, flash_charge and flash_cash.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,HTML,EXCEL,','N');
--Inserting Report Columns - Daily Flash Delivery � NEW July 10
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery � NEW July 10',660,'BRANCH_LOCATION','Location','Branch Location','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery � NEW July 10',660,'DELIVERY_TYPE','Delivery Type','Delivery Type','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery � NEW July 10',660,'SALES','Net Sales Dollars','Sales','','~T~D~2','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery � NEW July 10',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','~~~','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery � NEW July 10',660,'CURRENT_FISCAL_MONTH','Fiscal Month','Current Fiscal Month','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery � NEW July 10',660,'CURRENT_FISCAL_YEAR','Fiscal Year','Current Fiscal Year','','~~~','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','');
--Inserting Report Parameters - Daily Flash Delivery � NEW July 10
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery � NEW July 10',660,'Location','Location','BRANCH_LOCATION','IN','OM Warehouse All','','VARCHAR2','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery � NEW July 10',660,'Period Name','Period Name','','IN','OM PERIOD NAMES','','VARCHAR2','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Delivery � NEW July 10
xxeis.eis_rs_ins.rcn( 'Daily Flash Delivery � NEW July 10',660,'','','','','AND ( ''All'' IN (:Location) OR (BRANCH_LOCATION IN (:Location)))','Y','1','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Delivery � NEW July 10
--Inserting Report Triggers - Daily Flash Delivery � NEW July 10
xxeis.eis_rs_ins.rt( 'Daily Flash Delivery � NEW July 10',660,'begin

xxeis.eis_rs_xxwc_com_util_pkg.SET_PERIOD_NAME(:Period Name);

end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Delivery � NEW July 10
--Inserting Report Portals - Daily Flash Delivery � NEW July 10
--Inserting Report Dashboards - Daily Flash Delivery � NEW July 10
--Inserting Report Security - Daily Flash Delivery � NEW July 10
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery � NEW July 10','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery � NEW July 10','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery � NEW July 10','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery � NEW July 10','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery � NEW July 10','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery � NEW July 10','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery � NEW July 10','20005','','50900',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Daily Flash Delivery � NEW July 10
END;
/
set scan on define on
