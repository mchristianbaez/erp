--Report Name            : AR Aging Summary report by Customer
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIX_XXWC_AR_TST_SUMMARY_V
set scan off define off
prompt Creating View XXEIS.EIX_XXWC_AR_TST_SUMMARY_V
Create or replace View XXEIS.EIX_XXWC_AR_TST_SUMMARY_V
(BILL_TO_CUSTOMER_NAME,PARENT_CUSTOMER_NAME,BILL_TO_CUSTOMER_NUMBER,PARENT_CUSTOMER_NUMBER,OUTSTANDING_AMOUNT,BUCKET_CURRENT,BUCKET_1_TO_30,BUCKET_31_TO_60,BUCKET_61_TO_90,BUCKET_91_TO_180,BUCKET_181_TO_360,BUCKET_361_DAYS_AND_ABOVE,PROFILE_CLASS,CREDIT_HOLD,CUSTOMER_ACCOUNT_STATUS,COLLECTOR,CREDIT_ANALYST_NAME) AS 
SELECT main.BILL_TO_CUSTOMER_NAME,
    main.PARENT_CUSTOMER_NAME,
    main.BILL_TO_CUSTOMER_NUMBER,
    main.PARENT_CUSTOMER_NUMBER,
    SUM(main.OUTSTANDING_AMOUNT) OUTSTANDING_AMOUNT,
    SUM(main.BUCKET_CURRENT) BUCKET_CURRENT,
    SUM(main.BUCKET_1_TO_30) BUCKET_1_TO_30,
    SUM(main.BUCKET_31_TO_60) BUCKET_31_TO_60,
    SUM(main.BUCKET_61_TO_90) BUCKET_61_TO_90,
    SUM(main.BUCKET_91_TO_180) BUCKET_91_TO_180,
    SUM(main.BUCKET_181_TO_360) BUCKET_181_TO_360,
    SUM(main.BUCKET_361_DAYS_AND_ABOVE) BUCKET_361_DAYS_AND_ABOVE,
    main.PROFILE_CLASS,
    main.CREDIT_HOLD,
    main.CUSTOMER_ACCOUNT_STATUS,
    main.COLLECTOR,
    main.CREDIT_ANALYST_NAME
  FROM
    (SELECT NVL (cust.account_name, party.party_name) bill_to_customer_name,
      NVL (parent_cust.Account_Name, parent_Party.Party_Name) parent_Customer_Name,
      CUST.ACCOUNT_NUMBER BILL_TO_CUSTOMER_NUMBER,
      PARENT_CUST.ACCOUNT_NUMBER PARENT_CUSTOMER_NUMBER,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, NULL, NULL, (PS.AMOUNT_DUE_ORIGINAL)  *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class )) OUTSTANDING_AMOUNT,
      SUM( XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING (PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, NULL, 0, (PS.AMOUNT_DUE_ORIGINAL)     *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class )) BUCKET_CURRENT,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING (PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 1, 30, (PS.AMOUNT_DUE_ORIGINAL)        *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class )) BUCKET_1_TO_30,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 31, 60, (PS.AMOUNT_DUE_ORIGINAL)      *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class ))BUCKET_31_TO_60,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 61, 90, (PS.AMOUNT_DUE_ORIGINAL)      *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class ))BUCKET_61_TO_90,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 91, 180, (PS.AMOUNT_DUE_ORIGINAL)     *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class )) BUCKET_91_TO_180,
      SUM( XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 181, 360, (PS.AMOUNT_DUE_ORIGINAL)   *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class ))BUCKET_181_TO_360,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 361, 9999999, (PS.AMOUNT_DUE_ORIGINAL)*NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class ))BUCKET_361_DAYS_AND_ABOVE,
      HZPC.name PROFILE_CLASS,
      HZP.CREDIT_HOLD,
      CUSTOMER_STATUS.MEANING CUSTOMER_ACCOUNT_STATUS,
      AC.name COLLECTOR,
      JRDV.RESOURCE_NAME CREDIT_ANALYST_NAME
    FROM Ra_Cust_Trx_Types Ctt,
      Ra_Customer_Trx Trx,
      Hz_Cust_Accounts Cust,
      Hz_Parties Party,
      Ar_Payment_Schedules Ps,
      RA_CUST_TRX_LINE_GL_DIST GLD,
      hz_cust_acct_relate cust_rel,
      hz_cust_accounts parent_cust ,
      hz_parties parent_party,
      AR_lookups CUSTOMER_STATUS,
      HZ_CUSTOMER_PROFILES HZP,
      HZ_CUST_PROFILE_CLASSES HZPC,
      AR_COLLECTORS AC,
      JTF_RS_DEFRESOURCES_V JRDV
    WHERE TRUNC (Ps.Gl_Date)   <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
    AND Trx.Cust_Trx_Type_Id    = Ctt.Cust_Trx_Type_Id
    AND Trx.Bill_To_Customer_Id = Cust.Cust_Account_Id
    AND Cust.Party_Id           = Party.Party_Id
    AND Ps.Customer_Trx_Id      = Trx.Customer_Trx_Id
    AND Trx.Customer_Trx_Id     = Gld.Customer_Trx_Id
    AND Gld.Account_Class       = 'REC'
    AND GLD.LATEST_REC_FLAG     = 'Y'
      --AND CTT.ORG_ID                                     = 162
    AND cust_rel.related_cust_account_id(+)            =cust.cust_account_id
    AND cust_rel.cust_account_id                       =parent_cust.cust_account_id(+)
    AND parent_cust.party_id                           = parent_party.party_id(+)
    AND NVL(customer_status.lookup_type,'ACCOUNT_STATUS') = 'ACCOUNT_STATUS'
    AND customer_status.lookup_code(+)                 = HZP.ACCOUNT_STATUS
    AND NVL(CUST_REL.STATUS,'A')                       ='A'
    AND HZP.cust_account_id                            = cust.cust_account_id
    AND HZP.site_use_id                               IS NULL
    AND hzp.profile_class_id                           = hzpc.profile_class_id
    AND HZP.COLLECTOR_ID                               = AC.COLLECTOR_ID(+)
    AND HZP.CREDIT_ANALYST_ID                          = JRDV.RESOURCE_ID(+)
      -- AND PARTY.PARTY_ID=200813
    GROUP BY NVL (CUST.ACCOUNT_NAME, PARTY.PARTY_NAME),
      NVL (PARENT_CUST.ACCOUNT_NAME, PARENT_PARTY.PARTY_NAME),
      CUST.ACCOUNT_NUMBER,
      PARENT_CUST.ACCOUNT_NUMBER,
      HZPC.name ,
      HZP.CREDIT_HOLD,
      CUSTOMER_STATUS.MEANING,
      AC.name,
      JRDV.RESOURCE_NAME
    UNION
    SELECT NVL (cust.account_name, party.party_name) customer_name,
      NVL (parent_cust.Account_Name, parent_Party.Party_Name) parent_Customer_Name,
      cust.account_number customer_number,
      PARENT_CUST.ACCOUNT_NUMBER PARENT_CUSTOMER_NUMBER,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, NULL, NULL, PS.AMOUNT_DUE_ORIGINAL  *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class)) OUTSTANDING_AMOUNT,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, NULL, 0, PS.AMOUNT_DUE_ORIGINAL     *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class)) BUCKET_CURRENT,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 1, 30, PS.AMOUNT_DUE_ORIGINAL       *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class)) BUCKET_1_TO_30,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 31, 60, PS.AMOUNT_DUE_ORIGINAL      *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class)) BUCKET_31_TO_60,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 61, 90, PS.AMOUNT_DUE_ORIGINAL      *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class)) BUCKET_61_TO_90,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 91, 180, PS.AMOUNT_DUE_ORIGINAL     *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class)) BUCKET_91_TO_180,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 181, 360, PS.AMOUNT_DUE_ORIGINAL    *NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class)) BUCKET_181_TO_360,
      SUM(XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.AMOUNT_REMAINING ( PS.PAYMENT_SCHEDULE_ID, PS.DUE_DATE, 361, 9999999, PS.AMOUNT_DUE_ORIGINAL*NVL(PS.EXCHANGE_RATE,1), PS.AMOUNT_APPLIED, PS.AMOUNT_CREDITED, PS.AMOUNT_ADJUSTED, PS.class)) BUCKET_361_DAYS_AND_ABOVE,
      HZPC.name PROFILE_CLASS,
      HZP.CREDIT_HOLD,
      CUSTOMER_STATUS.MEANING CUSTOMER_ACCOUNT_STATUS,
      AC.name COLLECTOR,
      JRDV.RESOURCE_NAME CREDIT_ANALYST_NAME
    FROM Ar_Payment_Schedules Ps,
      AR_CASH_RECEIPTS CR,
      Hz_Cust_Accounts Cust,
      HZ_PARTIES PARTY,
      hz_cust_acct_relate cust_rel,
      hz_cust_accounts parent_cust ,
      HZ_PARTIES PARENT_PARTY,
      AR_lookups customer_status,
      HZ_CUSTOMER_PROFILES HZP,
      HZ_CUST_PROFILE_CLASSES HZPC,
      AR_COLLECTORS AC,
      JTF_RS_DEFRESOURCES_V JRDV
      --  Ra_Terms Rt
    WHERE TRUNC (Ps.Gl_Date)                          <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
    AND PS.CASH_RECEIPT_ID                             = CR.CASH_RECEIPT_ID
    AND Ps.Customer_Id                                 = Cust.Cust_Account_Id(+)
    AND CUST.PARTY_ID                                  = PARTY.PARTY_ID(+)
    AND cust_rel.related_cust_account_id(+)            = cust.cust_account_id
    AND cust_rel.cust_account_id                       = parent_cust.cust_account_id(+)
    AND PARENT_CUST.PARTY_ID                           = PARENT_PARTY.PARTY_ID(+)
    AND NVL(customer_status.lookup_type,'ACCOUNT_STATUS') = 'ACCOUNT_STATUS'
    AND customer_status.lookup_code(+)                 = HZP.ACCOUNT_STATUS
    AND NVL(cust_rel.status,'A')                       = 'A'
    AND HZP.cust_account_id                            = cust.cust_account_id
    AND HZP.site_use_id                               IS NULL
    AND hzp.profile_class_id                           = hzpc.profile_class_id
    AND HZP.COLLECTOR_ID                               = AC.COLLECTOR_ID(+)
    AND HZP.CREDIT_ANALYST_ID                          = JRDV.RESOURCE_ID(+)
      -- AND PARTY.PARTY_ID=200813
    AND NOT EXISTS
      (SELECT 1
      FROM Ar_Cash_Receipt_History Crh1
      WHERE Crh1.Cash_Receipt_Id = Cr.Cash_Receipt_Id
      AND TRUNC (Crh1.Gl_Date)  <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
      AND Status                 = 'REVERSED'
      )
    GROUP BY NVL (CUST.ACCOUNT_NAME, PARTY.PARTY_NAME),
      NVL (PARENT_CUST.ACCOUNT_NAME, PARENT_PARTY.PARTY_NAME),
      CUST.ACCOUNT_NUMBER,
      PARENT_CUST.ACCOUNT_NUMBER,
      HZPC.name,
      HZP.CREDIT_HOLD,
      CUSTOMER_STATUS.MEANING,
      AC.name,
      JRDV.RESOURCE_NAME
    )MAIN
  GROUP BY main.BILL_TO_CUSTOMER_NAME,
    main.PARENT_CUSTOMER_NAME,
    main.BILL_TO_CUSTOMER_NUMBER,
    main.PARENT_CUSTOMER_NUMBER,
    main.PROFILE_CLASS,
    main.CREDIT_HOLD,
    main.CUSTOMER_ACCOUNT_STATUS,
    main.COLLECTOR,
    main.CREDIT_ANALYST_NAME/
set scan on define on
prompt Creating View Data for AR Aging Summary report by Customer
set scan off define off
DECLARE
BEGIN 
--Inserting View EIX_XXWC_AR_TST_SUMMARY_V
xxeis.eis_rs_ins.v( 'EIX_XXWC_AR_TST_SUMMARY_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eix Xxwc Ar Summary V','EXASV1');
--Delete View Columns for EIX_XXWC_AR_TST_SUMMARY_V
xxeis.eis_rs_utility.delete_view_rows('EIX_XXWC_AR_TST_SUMMARY_V',222,FALSE);
--Inserting View Columns for EIX_XXWC_AR_TST_SUMMARY_V
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','BUCKET_361_DAYS_AND_ABOVE',222,'Bucket 361 Days And Above','BUCKET_361_DAYS_AND_ABOVE','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 361 Days And Above');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','BUCKET_181_TO_360',222,'Bucket 181 To 360','BUCKET_181_TO_360','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 181 To 360');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','BUCKET_91_TO_180',222,'Bucket 91 To 180','BUCKET_91_TO_180','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 91 To 180');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','BUCKET_61_TO_90',222,'Bucket 61 To 90','BUCKET_61_TO_90','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 61 To 90');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','BUCKET_31_TO_60',222,'Bucket 31 To 60','BUCKET_31_TO_60','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 31 To 60');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','BUCKET_1_TO_30',222,'Bucket 1 To 30','BUCKET_1_TO_30','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 1 To 30');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','BUCKET_CURRENT',222,'Bucket Current','BUCKET_CURRENT','','','','XXEIS_RS_ADMIN','NUMBER','','','Bucket Current');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','OUTSTANDING_AMOUNT',222,'Outstanding Amount','OUTSTANDING_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Outstanding Amount');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','BILL_TO_CUSTOMER_NUMBER',222,'Bill To Customer Number','BILL_TO_CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Customer Number');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','BILL_TO_CUSTOMER_NAME',222,'Bill To Customer Name','BILL_TO_CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To Customer Name');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','PARENT_CUSTOMER_NUMBER',222,'Parent Customer Number','PARENT_CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Parent Customer Number');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','PARENT_CUSTOMER_NAME',222,'Parent Customer Name','PARENT_CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Parent Customer Name');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','CREDIT_HOLD',222,'Credit Hold','CREDIT_HOLD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Hold');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','CUSTOMER_ACCOUNT_STATUS',222,'Customer Account Status','CUSTOMER_ACCOUNT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Account Status');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','PROFILE_CLASS',222,'Profile Class','PROFILE_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Profile Class');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','COLLECTOR',222,'Collector','COLLECTOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Collector');
xxeis.eis_rs_ins.vc( 'EIX_XXWC_AR_TST_SUMMARY_V','CREDIT_ANALYST_NAME',222,'Credit Analyst Name','CREDIT_ANALYST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Analyst Name');
--Inserting View Components for EIX_XXWC_AR_TST_SUMMARY_V
--Inserting View Component Joins for EIX_XXWC_AR_TST_SUMMARY_V
END;
/
set scan on define on
prompt Creating Report LOV Data for AR Aging Summary report by Customer
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - AR Aging Summary report by Customer
xxeis.eis_rs_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
	where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select name from ar_collectors','null','Collector','Displays list of values for Collector','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select distinct name from hz_cust_profile_classes','null','PROFILE CLASS','This LOV lists all the profile classes of the customers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 222,'select  customer_status.meaning Status
from  fnd_lookup_values_vl customer_status
where customer_status.lookup_type= ''CODE_STATUS''
 and customer_status.view_application_id=222','','Customer Status','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT  resource_name resource_name
 FROM jtf_rs_role_relations a,
  jtf_rs_roles_vl b,
  jtf_rs_resource_extns_vl c
WHERE a.role_resource_type  = ''RS_INDIVIDUAL''
AND a.role_resource_id      = c.resource_id
AND a.role_id               = b.role_id
AND b.role_code             = ''CREDIT_ANALYST''
AND c.category              = ''EMPLOYEE''
AND NVL(a.delete_flag,''N'') <> ''Y''
Order BY resource_name','','Credit Analyst','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select distinct credit_hold from hz_customer_profiles','','Credit Holds','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for AR Aging Summary report by Customer
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - AR Aging Summary report by Customer
xxeis.eis_rs_utility.delete_report_rows( 'AR Aging Summary report by Customer' );
--Inserting Report - AR Aging Summary report by Customer
xxeis.eis_rs_ins.r( 222,'AR Aging Summary report by Customer','','','','','','XXEIS_RS_ADMIN','EIX_XXWC_AR_TST_SUMMARY_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - AR Aging Summary report by Customer
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'BILL_TO_CUSTOMER_NAME','Customer Name','Bill To Customer Name','','','','','1','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'BILL_TO_CUSTOMER_NUMBER','Customer Number','Bill To Customer Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'BUCKET_181_TO_360','Bucket 181 To 360','Bucket 181 To 360','','','','','13','N','','DATA_FIELD','','SUM','','7','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'BUCKET_1_TO_30','Bucket 1 To 30','Bucket 1 To 30','','','','','9','N','','DATA_FIELD','','SUM','','3','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'BUCKET_31_TO_60','Bucket 31 To 60','Bucket 31 To 60','','','','','10','N','','DATA_FIELD','','SUM','','4','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'BUCKET_361_DAYS_AND_ABOVE','Bucket 361 Days And Above','Bucket 361 Days And Above','','','','','14','N','','DATA_FIELD','','SUM','','8','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'BUCKET_61_TO_90','Bucket 61 To 90','Bucket 61 To 90','','','','','11','N','','DATA_FIELD','','SUM','','5','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'BUCKET_91_TO_180','Bucket 91 To 180','Bucket 91 To 180','','','','','12','N','','DATA_FIELD','','SUM','','6','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'BUCKET_CURRENT','Bucket Current','Bucket Current','','','','','8','N','','DATA_FIELD','','SUM','','2','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'OUTSTANDING_AMOUNT','Customer Account Balance','Outstanding Amount','','','','','7','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'COLLECTOR','Collector','Collector','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'CREDIT_HOLD','Credit Hold','Credit Hold','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'CUSTOMER_ACCOUNT_STATUS','Customer Account Status','Customer Account Status','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
xxeis.eis_rs_ins.rc( 'AR Aging Summary report by Customer',222,'PROFILE_CLASS','Profile Class','Profile Class','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIX_XXWC_AR_TST_SUMMARY_V','','');
--Inserting Report Parameters - AR Aging Summary report by Customer
xxeis.eis_rs_ins.rp( 'AR Aging Summary report by Customer',222,'Customer Name','Customer Name','BILL_TO_CUSTOMER_NAME','IN','Customer Name','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary report by Customer',222,'As of Date','As of Date','','IN','','','DATE','Y','Y','7','','N','CURRENT_DATE','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary report by Customer',222,'Customer Account Status','Customer Account Status','CUSTOMER_ACCOUNT_STATUS','IN','Customer Status','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary report by Customer',222,'Collector','Collector','COLLECTOR','IN','Collector','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary report by Customer',222,'Credit Analyst','Credit Analyst','CREDIT_ANALYST_NAME','IN','Credit Analyst','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary report by Customer',222,'Credit Hold','Credit Hold','CREDIT_HOLD','IN','Credit Holds','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'AR Aging Summary report by Customer',222,'Profile Class','Profile Class','PROFILE_CLASS','IN','PROFILE CLASS','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - AR Aging Summary report by Customer
xxeis.eis_rs_ins.rcn( 'AR Aging Summary report by Customer',222,'BILL_TO_CUSTOMER_NAME','IN',':Customer Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary report by Customer',222,'CUSTOMER_ACCOUNT_STATUS','IN',':Customer Account Status','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary report by Customer',222,'COLLECTOR','IN',':Collector','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary report by Customer',222,'CREDIT_ANALYST_NAME','IN',':Credit Analyst','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary report by Customer',222,'CREDIT_HOLD','IN',':Credit Hold','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'AR Aging Summary report by Customer',222,'PROFILE_CLASS','IN',':Profile Class','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - AR Aging Summary report by Customer
--Inserting Report Triggers - AR Aging Summary report by Customer
xxeis.eis_rs_ins.rt( 'AR Aging Summary report by Customer',222,'Begin

 xxeis.eis_rs_ar_fin_com_util_pkg.set_asof_date(:As of Date);

End;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - AR Aging Summary report by Customer
--Inserting Report Portals - AR Aging Summary report by Customer
--Inserting Report Dashboards - AR Aging Summary report by Customer
--Inserting Report Security - AR Aging Summary report by Customer
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','10012196','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','LB048272','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','MM050208','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','SO004816','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','10010432','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','LC053655','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','RB054040','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','RV003897','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','SS084202','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','222','','50871',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','222','','50638',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','222','','50622',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','401','','50872',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','222','','20678',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','20005','','50880',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Aging Summary report by Customer','','10011289','',222,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Cash Drawer Reconciliation
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_CASH_DRWR_RECON_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_CASH_DRWR_RECON_V
Create or replace View XXEIS.EIS_XXWC_AR_CASH_DRWR_RECON_V
(ORDER_NUMBER,ORG_ID,PAYMENT_TYPE_CODE,TAKEN_BY,CASH_TYPE,CHK_CARDNO,CARD_NUMBER,CARD_ISSUER_CODE,CARD_ISSUER_NAME,CASH_AMOUNT,CUSTOMER_NUMBER,CUSTOMER_NAME,BRANCH_NUMBER,CHECK_NUMBER,CASH_DATE,PAYMENT_CHANNEL_NAME,NAME,PAYMENT_NUMBER,PAYMENT_AMOUNT,ORDER_AMOUNT,PARTY_ID,CUST_ACCOUNT_ID,INVOICE_NUMBER,INVOICE_DATE,SEGMENT_NUMBER,HEADER_ID,CASH_RECEIPT_ID) AS 
SELECT OH.ORDER_NUMBER,
    oh.org_id,
    olkp.meaning PAYMENT_TYPE_CODE,
    PPF.LAST_NAME TAKEN_BY,
    CASE
      WHEN OE.PAYMENT_TYPE_CODE IN ('CASH','CHECK','CREDIT_CARD')
      AND NOT EXISTS
        (SELECT ORDERED_ITEM
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        AND ORDERED_ITEM LIKE '%DEPOSIT%'
        )
      AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )
      THEN 'CASH SALES'
      WHEN OE.PAYMENT_TYPE_CODE IN ('CASH','CHECK','CREDIT_CARD')
      AND EXISTS
        (SELECT ORDERED_ITEM
        FROM OE_ORDER_LINES OL
        WHERE OL.HEADER_ID=OH.HEADER_ID
        AND ORDERED_ITEM LIKE '%DEPOSIT%'
        )
      AND NOT EXISTS
        (SELECT header_id
        FROM xxwc_om_cash_refund_tbl re
        WHERE re.RETURN_HEADER_ID=oh.header_id
        )
      THEN 'DEPOSIT'
    END CASH_TYPE,
    DECODE(OE.PAYMENT_TYPE_CODE,'CHECK',OE.CHECK_NUMBER,'CREDIT_CARD',SUBSTR(ITE.CARD_NUMBER,-4)) CHK_CARDNO,
    SUBSTR(ITE.CARD_NUMBER,                                                                  -4) CARD_NUMBER,
    ITE.CARD_ISSUER_CODE,
    ITE.CARD_ISSUER_NAME,
    OE.PAYMENT_AMOUNT CASH_AMOUNT,
     hca.account_number customer_number,
    HZP.PARTY_NAME CUSTOMER_NAME,
    OOD.ORGANIZATION_CODE BRANCH_NUMBER,
    OE.CHECK_NUMBER,
    OH.ORDERED_DATE CASH_DATE,
    ITE.PAYMENT_CHANNEL_NAME,
    ARC.name,
    PAYMENT_NUMBER,
        (select SUM( OE.PAYMENT_AMOUNT)
    from OE_PAYMENTS
    where HEADER_ID=OH.HEADER_ID
    )  PAYMENT_AMOUNT,
    Case When Payment_Number=1 Then
  (SELECT ROUND(SUM((OL.ORDERED_QUANTITY*OL.UNIT_SELLING_PRICE)+ OL.TAX_VALUE),2) ORDER_AMOUNT
    FROM OE_ORDER_LINES OL
    WHERE OL.HEADER_ID=OH.HEADER_ID
    )
    ELSE 0 END ORDER_AMOUNT,
    HZP.PARTY_ID,
    HCA.CUST_ACCOUNT_ID,
    (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT
    WHERE TO_CHAR(OH.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    )Invoice_Number,
  --   NULL CREDIT_NUMBER,
    (SELECT MAX(RCT.TRX_DATE)
    FROM RA_CUSTOMER_TRX RCT
    WHERE TO_CHAR(OH.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    )INVOICE_DATE,
    (SELECT MAX(GCC.SEGMENT2)
    FROM OE_ORDER_LINES L,
      RA_CUSTOMER_TRX_LINES RCTL,
      RA_CUSTOMER_TRX RCT,
      RA_CUST_TRX_LINE_GL_DIST RCTG,
      GL_CODE_COMBINATIONS_KFV GCC
    WHERE OH.HEADER_ID           =L.HEADER_ID
    AND TO_CHAR(OH.ORDER_NUMBER) =RCTL.INTERFACE_LINE_ATTRIBUTE1
    AND TO_CHAR(L.LINE_ID)       =RCTL.INTERFACE_LINE_ATTRIBUTE6
    AND RCT.CUSTOMER_TRX_ID      =RCTL.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_ID     =RCTG.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_LINE_ID=RCTG.CUSTOMER_TRX_LINE_ID
    AND rctg.code_combination_id =gcc.code_combination_id
    )Segment_Number,
    Oh.Header_Id Header_Id,
    0 cash_receipt_id
  FROM OE_ORDER_HEADERS OH,
    oe_payments oe,
    oe_lookups olkp,
    hz_parties hzp,
    HZ_CUST_ACCOUNTS HCA,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    IBY_TRXN_EXTENSIONS_V ITE,
    PER_ALL_PEOPLE_F PPF,
    FND_USER FU,
    AR_RECEIPT_METHODS arc
  WHERE OH.SOLD_TO_ORG_ID =HCA.CUST_ACCOUNT_ID
  AND OE.HEADER_ID        =OH.HEADER_ID
  AND HZP.PARTY_ID        =HCA.PARTY_ID
  AND OH.SHIP_FROM_ORG_ID =OOD.ORGANIZATION_ID
  AND FU.USER_ID          =OH.CREATED_BY
  AND fu.employee_id      =PPF.PERSON_ID(+)
  AND OH.ORDERED_DATE BETWEEN NVL(PPF.EFFECTIVE_START_DATE,OH.ORDERED_DATE) AND NVL(PPF.EFFECTIVE_END_DATE,OH.ORDERED_DATE)
  AND OE.TRXN_EXTENSION_ID =ITE.TRXN_EXTENSION_ID(+)
  AND oe.receipt_method_id =arc.receipt_method_id(+)
  AND oe.payment_type_code = olkp.lookup_code
  And Olkp.Lookup_Type     = 'PAYMENT TYPE'
  -- And Oh.Header_Id=26587
  And Not Exists
    (SELECT 1 FROM XXWC_OM_CASH_REFUND_TBL XOC WHERE XOC.RETURN_HEADER_ID=OH.HEADER_ID
    )
  AND EXISTS
    (SELECT 1
    FROM RA_CUSTOMER_TRX RCT
    Where To_Char(Oh.Order_Number) =Rct.Interface_Header_Attribute1
    )
UNION
 SELECT OH.ORDER_NUMBER,
    oh.org_id,
    olkp.meaning PAYMENT_TYPE_CODE,
    PPF.LAST_NAME TAKEN_BY,
    'CASH RETURN' CASH_TYPE,
    DECODE(XMCR.PAYMENT_TYPE_CODE,'CHECK',OE.CHECK_NUMBER,'CREDIT_CARD',SUBSTR(ITE.CARD_NUMBER,-4)) CHK_CARDNO,
    SUBSTR(ITE.CARD_NUMBER,                                                                    -4) CARD_NUMBER,
    ITE.CARD_ISSUER_CODE,
    Ite.Card_Issuer_Name,
    xmcr.refund_amount*-1  cash_amount,
    hca.account_number customer_number,
    HZP.PARTY_NAME CUSTOMER_NAME,
    OOD.ORGANIZATION_CODE BRANCH_NUMBER,
    OE.CHECK_NUMBER,
    OH.ORDERED_DATE CASH_DATE,
    ITE.PAYMENT_CHANNEL_NAME,
    Arc.Name,
    null PAYMENT_NUMBER,
    (select SUM(XX.REFUND_AMOUNT)
    from XXWC_OM_CASH_REFUND_TBL XX
    where XX.RETURN_HEADER_ID=XMCR.RETURN_HEADER_ID
    and XX.PAYMENT_TYPE_CODE <> 'CHECK'
    )Payment_Amount,
    xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_cash_drawer_amt(oh.header_id) ORDER_AMOUNT,
    HZP.PARTY_ID,
    HCA.CUST_ACCOUNT_ID,
    (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT,
      OE_ORDER_HEADERS H
    WHERE H.HEADER_ID           =XMCR.HEADER_ID
    AND TO_CHAR(H.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    )Invoice_Number,
   /*   (SELECT MAX(RCT.TRX_NUMBER)
    FROM RA_CUSTOMER_TRX RCT,
      Oe_Order_Headers H
    WHERE H.HEADER_ID           =XMCR.RETURN_HEADER_ID
    And To_Char(H.Order_Number) =Rct.Interface_Header_Attribute1
    )CREDIT_NUMBER,*/
    (SELECT MAX(RCT.TRX_DATE)
    FROM RA_CUSTOMER_TRX RCT,
      OE_ORDER_HEADERS H1
    WHERE TO_CHAR(H1.ORDER_NUMBER) =RCT.INTERFACE_HEADER_ATTRIBUTE1
    AND H1.HEADER_ID               =XMCR.HEADER_ID
    )INVOICE_DATE,
    (SELECT MAX(GCC.SEGMENT2)
    FROM OE_ORDER_LINES L,
      OE_ORDER_HEADERS H2,
      RA_CUSTOMER_TRX_LINES RCTL,
      RA_CUSTOMER_TRX RCT,
      RA_CUST_TRX_LINE_GL_DIST RCTG,
      GL_CODE_COMBINATIONS_KFV GCC
    WHERE H2.HEADER_ID           =XMCR.HEADER_ID
    AND H2.HEADER_ID             =L.HEADER_ID
    AND TO_CHAR(H2.order_number) =rct.interface_HEADER_attribute1
    AND TO_CHAR(L.LINE_ID)       =RCTL.INTERFACE_LINE_ATTRIBUTE6
    AND RCT.CUSTOMER_TRX_ID      =RCTL.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_ID     =RCTG.CUSTOMER_TRX_ID
    AND RCTL.CUSTOMER_TRX_LINE_ID=RCTG.CUSTOMER_TRX_LINE_ID
    AND RCTG.CODE_COMBINATION_ID =GCC.CODE_COMBINATION_ID
    )Segment_Number,
       Oh.Header_Id Header_Id,
       XMCR.cash_receipt_id cash_receipt_id
  from  OE_ORDER_HEADERS                OH,
        OE_PAYMENTS                     OE,
        OE_LOOKUPS                      OLKP,
        HZ_PARTIES                      HZP,
        HZ_CUST_ACCOUNTS                HCA,
        ORG_ORGANIZATION_DEFINITIONS    OOD,
        XXWC_OM_CASH_REFUND_TBL         XMCR,
        IBY_TRXN_EXTENSIONS_V           ITE,
        PER_ALL_PEOPLE_F                PPF,
        FND_USER                        FU,
        AR_RECEIPT_METHODS              arc
  WHERE OE.HEADER_ID(+)     =OH.HEADER_ID
  AND oH.sold_to_org_id     =hca.cust_account_id
  AND HZP.PARTY_ID          =HCA.PARTY_ID
  AND OH.SHIP_FROM_ORG_ID   =OOD.ORGANIZATION_ID
  AND xmcr.RETURN_header_id =oh.header_id
  AND FU.USER_ID            =OH.CREATED_BY
  AND fu.employee_id        =PPF.PERSON_ID(+)
  AND OH.ORDERED_DATE BETWEEN NVL(PPF.EFFECTIVE_START_DATE,OH.ORDERED_DATE) AND NVL(PPF.EFFECTIVE_END_DATE,OH.ORDERED_DATE)
  And Oe.Trxn_Extension_Id =Ite.Trxn_Extension_Id(+)
-- And xmcr.RETURN_header_id=26587
  AND XMCR.PAYMENT_TYPE_CODE <> 'CHECK'
  AND oe.receipt_method_id   =arc.receipt_method_id(+)
  AND xmcr.payment_type_code = olkp.lookup_code
  And Olkp.Lookup_Type       = 'PAYMENT TYPE'
   Order By  Header_Id,Order_Amount Desc/
set scan on define on
prompt Creating View Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_CASH_DRWR_RECON_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Cash Drwr Recon V','EXACDRV');
--Delete View Columns for EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_CASH_DRWR_RECON_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_CASH_DRWR_RECON_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_DATE',222,'Cash Date','CASH_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Cash Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','BRANCH_NUMBER',222,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_AMOUNT',222,'Cash Amount','CASH_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Cash Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_ISSUER_NAME',222,'Card Issuer Name','CARD_ISSUER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CASH_TYPE',222,'Cash Type','CASH_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cash Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_TYPE_CODE',222,'Payment Type Code','PAYMENT_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','TAKEN_BY',222,'Taken By','TAKEN_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Taken By');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','SEGMENT_NUMBER',222,'Segment Number','SEGMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CHK_CARDNO',222,'Chk Cardno','CHK_CARDNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Chk Cardno');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORDER_AMOUNT',222,'Order Amount','ORDER_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','NAME',222,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_CHANNEL_NAME',222,'Payment Channel Name','PAYMENT_CHANNEL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Channel Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CHECK_NUMBER',222,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_ISSUER_CODE',222,'Card Issuer Code','CARD_ISSUER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','CARD_NUMBER',222,'Card Number','CARD_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','ORG_ID',222,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_CASH_DRWR_RECON_V','PAYMENT_NUMBER',222,'Payment Number','PAYMENT_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Number');
--Inserting View Components for EIS_XXWC_AR_CASH_DRWR_RECON_V
--Inserting View Component Joins for EIS_XXWC_AR_CASH_DRWR_RECON_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Cash Drawer Reconciliation
xxeis.eis_rs_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Cash Drawer Reconciliation
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Cash Drawer Reconciliation
xxeis.eis_rs_utility.delete_report_rows( 'Cash Drawer Reconciliation' );
--Inserting Report - Cash Drawer Reconciliation
xxeis.eis_rs_ins.r( 222,'Cash Drawer Reconciliation','','This report provides a total listing of all Cash, Check and Credit Card Sales by Branch (Customer, Taken By, Order Number, Cash Date, Cash Amount, Cash Type, Invoice Number, Invoice Date, Payment Type, Check/Credit Card No.)','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_CASH_DRWR_RECON_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'BRANCH_NUMBER','Branch Number','Branch Number','','','','','1','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CASH_AMOUNT','Cash Amount','Cash Amount','','','','','9','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CASH_DATE','Cash Date','Cash Date','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CASH_TYPE','Cash Type','Cash Type','','','','','15','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CHK_CARDNO','Check/Card No','Chk Cardno','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','3','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'NAME','Receipt Method','Name','','','','','14','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'ORDER_AMOUNT','Order Amount','Order Amount','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'ORDER_NUMBER','Order Number','Order Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'PAYMENT_TYPE_CODE','Payment Type','Payment Type Code','','','','','11','N','','ROW_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'SEGMENT_NUMBER','Segment Number','Segment Number','','','','','17','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'TAKEN_BY','Created By','Taken By','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','4','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'CARD_ISSUER_NAME','Card Type','Card Issuer Name','','','','','13','N','','ROW_FIELD','','','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Reconciliation',222,'DIFFERENCE','DIFFERENCE','Card Issuer Name','NUMBER','~,~2','','0','10','Y','','','','','','','case when  EXACDRV.order_amount <> 0  then (EXACDRV.order_amount-EXACDRV.PAYMENT_AMOUNT) else 0 end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_CASH_DRWR_RECON_V','','');
--Inserting Report Parameters - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rp( 'Cash Drawer Reconciliation',222,'Location','Branch Number','BRANCH_NUMBER','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cash Drawer Reconciliation',222,'From Date','From Date','CASH_DATE','>=','','','DATE','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cash Drawer Reconciliation',222,'To Date','To Date','CASH_DATE','<=','','','DATE','Y','Y','3','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rcn( 'Cash Drawer Reconciliation',222,'BRANCH_NUMBER','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cash Drawer Reconciliation',222,'TRUNC(CASH_DATE)','>=',':From Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Cash Drawer Reconciliation',222,'TRUNC(CASH_DATE)','<=',':To Date','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Cash Drawer Reconciliation
--Inserting Report Triggers - Cash Drawer Reconciliation
--Inserting Report Templates - Cash Drawer Reconciliation
--Inserting Report Portals - Cash Drawer Reconciliation
--Inserting Report Dashboards - Cash Drawer Reconciliation
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 751','Dynamic 751','pie','large','Cash Amount','Cash Amount','Customer Name','Customer Name','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 752','Dynamic 752','pie','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 753','Dynamic 753','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 754','Dynamic 754','absolute line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 755','Dynamic 755','point','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 756','Dynamic 756','stacked line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 757','Dynamic 757','scatter','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 758','Dynamic 758','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_dash( 'Cash Drawer Reconciliation','Dynamic 759','Dynamic 759','horizontal stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
--Inserting Report Security - Cash Drawer Reconciliation
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','401','','50835',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','50838',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','401','','50840',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50721',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50662',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50720',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50723',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50801',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50665',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50722',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50780',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50667',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','50668',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','20434',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','101','','20475',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','701','','50546',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','660','','21623',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','','LB048272','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Reconciliation','20005','','50880',222,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : AR Customer and AP Vendor Match Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AP_AR_OPEN_BALANCE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AP_AR_OPEN_BALANCE_V
Create or replace View XXEIS.EIS_XXWC_AP_AR_OPEN_BALANCE_V
(CUSTOMER_NAME,CUSTOMER_NUMBER,CUSTOMER_ADDR_STREET_ADDRESS1,CUSTOMER_ADDR_STREET_ADDRESS2,CUSTOMER_CITY_ADDRESS,CUSTOMER_STATE_ADDRESS,CUSTOMER_ADDRESS_ZIPCODE,CUSTOMER_TELEPHONE_NUMBER,VENDOR_NAME,VENDOR_NUMBER,VENDOR_ADDRESS_STREET,VENDOR_ADDRESS_CITY,VENDOR_ADDRESS_STATE,VENDOR_ADDRESS_ZIPCODE,VENDOR_TELEPHONE_NUMBER,VENDOR_TAX_ID,CUSTOMER_TAX_ID,VENDOR_SITE_CODE,AR_OPEN_BALNACE,AP_OPEN_BALNACE) AS 
select 
HCA.ACCOUNT_NAME            CUSTOMER_NAME,
HCA.ACCOUNT_NUMBER            CUSTOMER_NUMBER,
HP.ADDRESS1                   CUSTOMER_ADDR_STREET_ADDRESS1,
HP.ADDRESS2                   CUSTOMER_ADDR_STREET_ADDRESS2,
HP.CITY                       CUSTOMER_CITY_ADDRESS,
HP.STATE                      CUSTOMER_STATE_ADDRESS,
HP.POSTAL_CODE                CUSTOMER_ADDRESS_ZIPCODE, 
HP.PRIMARY_PHONE_AREA_CODE ||'-'|| HP.PRIMARY_PHONE_NUMBER          CUSTOMER_TELEPHONE_NUMBER,
PV.VENDOR_NAME                VENDOR_NAME,
PV.SEGMENT1                   VENDOR_NUMBER,
PVS.ADDRESS_LINE1             VENDOR_ADDRESS_STREET,
PVS.CITY                      VENDOR_ADDRESS_CITY,
PVS.STATE                     VENDOR_ADDRESS_STATE,
PVS.ZIP                       VENDOR_ADDRESS_ZIPCODE,
PVS.PHONE                     VENDOR_TELEPHONE_NUMBER,
PV.NUM_1099                   VENDOR_TAX_ID,
HP.JGZZ_FISCAL_CODE           CUSTOMER_TAX_ID,
pvs.VENDOR_SITE_CODE          VENDOR_SITE_CODE,
sum(PS.AMOUNT_DUE_REMAINING)  AR_OPEN_BALNACE,
SUM(APS.AMOUNT_REMAINING)     AP_OPEN_BALNACE
FROM
HZ_PARTIES            HP,
HZ_CUST_ACCOUNTS      HCA,
--HZ_CUST_ACCT_SITES    HCAS,
--HZ_CUST_SITE_USES     HCSU,
--HZ_PARTY_SITES        HPS,
--HZ_LOCATIONS          HL,
--HZ_CONTACT_POINTS     HCP,
RA_CUSTOMER_TRX       TRX,
AR_PAYMENT_SCHEDULES  PS,
PO_VENDORS            PV,
PO_VENDOR_SITES       PVS ,
AP_INVOICES           API,
AP_PAYMENT_SCHEDULES  APS,
hr_operating_units    hou
where HP.PARTY_ID               = HCA.PARTY_ID
--AND HCA.CUST_ACCOUNT_ID         = HCAS.CUST_ACCOUNT_ID
--AND HCAS.CUST_ACCT_SITE_ID      = HCSU.CUST_ACCT_SITE_ID
--AND HPS.PARTY_SITE_ID           = HCAS.PARTY_SITE_ID
--AND HPS.LOCATION_ID             = HL.LOCATION_ID
--and HP.PARTY_ID                 = HCP.OWNER_TABLE_ID(+)
--AND HCP.OWNER_TABLE_NAME        = 'HZ_PARTY_SITES'--'HZ_PARTIES'
--AND HCP.PRIMARY_FLAG(+)         = 'Y'
--AND HCSU.SITE_USE_CODE          = 'BILL_TO'
--AND HCP.PHONE_NUMBER IS NOT NULL
and TRX.BILL_TO_CUSTOMER_ID     = HCA.CUST_ACCOUNT_ID
--AND TRX.BILL_TO_SITE_USE_ID     = HCSU.SITE_USE_ID
and TRX.CUSTOMER_TRX_ID         = PS.CUSTOMER_TRX_ID
AND (PV.VENDOR_NAME              = HP.PARTY_NAME OR PVS.PHONE = HP.PRIMARY_PHONE_NUMBER)
AND PV.VENDOR_ID                = PVS.VENDOR_ID
AND API.VENDOR_ID               = PV.VENDOR_ID
and API.ORG_ID                  = PVS.ORG_ID
AND API.INVOICE_ID              = APS.INVOICE_ID
AND API.ORG_ID                  = APS.ORG_ID
AND hou.name         = 'HDS White Cap - Org'
and HOU.ORGANIZATION_ID = PVS.ORG_ID
and NVL(PVS.PAY_SITE_FLAG,'Y')='Y'
--AND HP.PARTY_NAME='HD SUPPLY WATERWORKS'
and API.CANCELLED_DATE   is null 
GROUP BY HCA.ACCOUNT_NAME, HCA.ACCOUNT_NUMBER, HP.ADDRESS1, HP.ADDRESS2, HP.CITY, HP.STATE, HP.POSTAL_CODE, HP.PRIMARY_PHONE_AREA_CODE ||'-'|| HP.PRIMARY_PHONE_NUMBER, PV.VENDOR_NAME, PV.SEGMENT1, PVS.ADDRESS_LINE1, PVS.CITY, PVS.STATE, PVS.ZIP, PVS.PHONE, PV.NUM_1099, HP.JGZZ_FISCAL_CODE, pvs.VENDOR_SITE_CODE/
set scan on define on
prompt Creating View Data for AR Customer and AP Vendor Match Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AP_AR_OPEN_BALANCE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ap Ar Open Balance V','EXAAOBV');
--Delete View Columns for EIS_XXWC_AP_AR_OPEN_BALANCE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AP_AR_OPEN_BALANCE_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AP_AR_OPEN_BALANCE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','AR_OPEN_BALNACE',222,'Ar Open Balnace','AR_OPEN_BALNACE','','','','XXEIS_RS_ADMIN','NUMBER','','','Ar Open Balnace');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','VENDOR_TAX_ID',222,'Vendor Tax Id','VENDOR_TAX_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Tax Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','VENDOR_TELEPHONE_NUMBER',222,'Vendor Telephone Number','VENDOR_TELEPHONE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Telephone Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','VENDOR_ADDRESS_ZIPCODE',222,'Vendor Address Zipcode','VENDOR_ADDRESS_ZIPCODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Address Zipcode');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','VENDOR_ADDRESS_STATE',222,'Vendor Address State','VENDOR_ADDRESS_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Address State');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','VENDOR_ADDRESS_CITY',222,'Vendor Address City','VENDOR_ADDRESS_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Address City');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','VENDOR_ADDRESS_STREET',222,'Vendor Address Street','VENDOR_ADDRESS_STREET','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Address Street');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','VENDOR_NUMBER',222,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','VENDOR_NAME',222,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','CUSTOMER_TELEPHONE_NUMBER',222,'Customer Telephone Number','CUSTOMER_TELEPHONE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Telephone Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','CUSTOMER_ADDRESS_ZIPCODE',222,'Customer Address Zipcode','CUSTOMER_ADDRESS_ZIPCODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Address Zipcode');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','CUSTOMER_STATE_ADDRESS',222,'Customer State Address','CUSTOMER_STATE_ADDRESS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer State Address');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','CUSTOMER_CITY_ADDRESS',222,'Customer City Address','CUSTOMER_CITY_ADDRESS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer City Address');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','CUSTOMER_ADDR_STREET_ADDRESS2',222,'Customer Addr Street Address2','CUSTOMER_ADDR_STREET_ADDRESS2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Addr Street Address2');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','CUSTOMER_ADDR_STREET_ADDRESS1',222,'Customer Addr Street Address1','CUSTOMER_ADDR_STREET_ADDRESS1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Addr Street Address1');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','AP_OPEN_BALNACE',222,'Ap Open Balnace','AP_OPEN_BALNACE','','','','XXEIS_RS_ADMIN','NUMBER','','','Ap Open Balnace');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','VENDOR_SITE_CODE',222,'Vendor Site Code','VENDOR_SITE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AP_AR_OPEN_BALANCE_V','CUSTOMER_TAX_ID',222,'Customer Tax Id','CUSTOMER_TAX_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Tax Id');
--Inserting View Components for EIS_XXWC_AP_AR_OPEN_BALANCE_V
--Inserting View Component Joins for EIS_XXWC_AP_AR_OPEN_BALANCE_V
END;
/
set scan on define on
prompt Creating Report Data for AR Customer and AP Vendor Match Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - AR Customer and AP Vendor Match Report
xxeis.eis_rs_utility.delete_report_rows( 'AR Customer and AP Vendor Match Report' );
--Inserting Report - AR Customer and AP Vendor Match Report
xxeis.eis_rs_ins.r( 222,'AR Customer and AP Vendor Match Report','','The purpose of this report to detail the AR customers that are also AP vendors to determine the open balance based on the amount owed to White Cap and the amount White Cap owes to the customer.','','','','XXEIS_RS_ADMIN','EIS_XXWC_AP_AR_OPEN_BALANCE_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - AR Customer and AP Vendor Match Report
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'AR_OPEN_BALNACE','Ar Open Balnace','Ar Open Balnace','','','','','20','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_ADDRESS_ZIPCODE','Customer Address Zipcode','Customer Address Zipcode','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_ADDR_STREET_ADDRESS1','Customer Addr Street Address1','Customer Addr Street Address1','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_ADDR_STREET_ADDRESS2','Customer Addr Street Address2','Customer Addr Street Address2','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_CITY_ADDRESS','Customer City Address','Customer City Address','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','1','N','','ROW_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_STATE_ADDRESS','Customer State Address','Customer State Address','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_TELEPHONE_NUMBER','Customer Telephone Number','Customer Telephone Number','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'VENDOR_ADDRESS_CITY','Vendor Address City','Vendor Address City','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'VENDOR_ADDRESS_STATE','Vendor Address State','Vendor Address State','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'VENDOR_ADDRESS_STREET','Vendor Address Street','Vendor Address Street','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'VENDOR_ADDRESS_ZIPCODE','Vendor Address Zipcode','Vendor Address Zipcode','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','11','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'VENDOR_TAX_ID','Vendor Tax Id','Vendor Tax Id','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'VENDOR_TELEPHONE_NUMBER','Vendor Telephone Number','Vendor Telephone Number','','','','','19','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'AP_OPEN_BALNACE','Ap Open Balnace','Ap Open Balnace','','','','','21','N','','DATA_FIELD','','SUM','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'VENDOR_SITE_CODE','Vendor Site Code','Vendor Site Code','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
xxeis.eis_rs_ins.rc( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_TAX_ID','Customer Tax Id','Customer Tax Id','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AP_AR_OPEN_BALANCE_V','','');
--Inserting Report Parameters - AR Customer and AP Vendor Match Report
--Inserting Report Conditions - AR Customer and AP Vendor Match Report
--Inserting Report Sorts - AR Customer and AP Vendor Match Report
xxeis.eis_rs_ins.rs( 'AR Customer and AP Vendor Match Report',222,'CUSTOMER_NAME','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'AR Customer and AP Vendor Match Report',222,'VENDOR_NAME','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - AR Customer and AP Vendor Match Report
--Inserting Report Templates - AR Customer and AP Vendor Match Report
--Inserting Report Portals - AR Customer and AP Vendor Match Report
--Inserting Report Dashboards - AR Customer and AP Vendor Match Report
--Inserting Report Security - AR Customer and AP Vendor Match Report
xxeis.eis_rs_ins.rsec( 'AR Customer and AP Vendor Match Report','222','','50871',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Customer and AP Vendor Match Report','222','','50638',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Customer and AP Vendor Match Report','222','','50622',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Customer and AP Vendor Match Report','401','','50872',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Customer and AP Vendor Match Report','222','','20778',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Customer and AP Vendor Match Report','222','','21404',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Customer and AP Vendor Match Report','222','','20678',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Customer and AP Vendor Match Report','','LB048272','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'AR Customer and AP Vendor Match Report','20005','','50880',222,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
