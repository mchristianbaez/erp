--Report Name            : HDS AP Terminated Employees Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_TERM_EMP
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_TERM_EMP
xxeis.eis_rsc_ins.v( 'XXEIS_TERM_EMP',91000,'Paste SQL View for HDS Terminated Employees Report','1.0','','','XXEIS_RS_ADMIN','XXEIS','HDS Terminated Employees Report View','X4DV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_TERM_EMP
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_TERM_EMP',91000,FALSE);
--Inserting Object Columns for XXEIS_TERM_EMP
xxeis.eis_rsc_ins.vc( 'XXEIS_TERM_EMP','EMPLOYEE_NUMBER',91000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Employee Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_TERM_EMP','SEGMENT1',91000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_TERM_EMP','NAME',91000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_TERM_EMP','PROCESS_FLAG',91000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Process Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_TERM_EMP','LAST_DAY_PAID',91000,'','','','','','XXEIS_RS_ADMIN','DATE','','','Last Day Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_TERM_EMP','CREATION_DATE',91000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_TERM_EMP','SUPERVISOR',91000,'','','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supervisor','','','','US');
--Inserting Object Components for XXEIS_TERM_EMP
--Inserting Object Component Joins for XXEIS_TERM_EMP
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report HDS AP Terminated Employees Report
prompt Creating Report Data for HDS AP Terminated Employees Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(91000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS AP Terminated Employees Report
xxeis.eis_rsc_utility.delete_report_rows( 'HDS AP Terminated Employees Report' );
--Inserting Report - HDS AP Terminated Employees Report
xxeis.eis_rsc_ins.r( 91000,'HDS AP Terminated Employees Report','','Run daily to get a listing of new employee terminations','','','','XXEIS_RS_ADMIN','XXEIS_TERM_EMP','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - HDS AP Terminated Employees Report
xxeis.eis_rsc_ins.rc( 'HDS AP Terminated Employees Report',91000,'CREATION_DATE','Creation Date','','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_TERM_EMP','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Terminated Employees Report',91000,'EMPLOYEE_NUMBER','Employee Number','','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_TERM_EMP','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Terminated Employees Report',91000,'LAST_DAY_PAID','Last Day Paid','','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_TERM_EMP','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Terminated Employees Report',91000,'NAME','Name','','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_TERM_EMP','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Terminated Employees Report',91000,'PROCESS_FLAG','Process Flag','','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_TERM_EMP','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Terminated Employees Report',91000,'SUPERVISOR','Supervisor','','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_TERM_EMP','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS AP Terminated Employees Report',91000,'SEGMENT1','LOB','','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_TERM_EMP','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS AP Terminated Employees Report
xxeis.eis_rsc_ins.rp( 'HDS AP Terminated Employees Report',91000,'Creation_Date','Creation_Date','CREATION_DATE','=','','','DATE','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_TERM_EMP','','','US','');
--Inserting Dependent Parameters - HDS AP Terminated Employees Report
--Inserting Report Conditions - HDS AP Terminated Employees Report
xxeis.eis_rsc_ins.rcnh( 'HDS AP Terminated Employees Report',91000,'PROCESS_FLAG IN ''T'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PROCESS_FLAG','','','','','','','XXEIS_TERM_EMP','','','','','','IN','Y','N','','''T''','','','1',91000,'HDS AP Terminated Employees Report','PROCESS_FLAG IN ''T'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS AP Terminated Employees Report',91000,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND TO_CHAR(to_date(X4DV.CREATION_DATE, ''mmddyyyy''), ''DD-MON-YYYY'') = :Creation_Date','1',91000,'HDS AP Terminated Employees Report','Free Text ');
--Inserting Report Sorts - HDS AP Terminated Employees Report
--Inserting Report Triggers - HDS AP Terminated Employees Report
--inserting report templates - HDS AP Terminated Employees Report
--Inserting Report Portals - HDS AP Terminated Employees Report
--inserting report dashboards - HDS AP Terminated Employees Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS AP Terminated Employees Report','91000','XXEIS_TERM_EMP','XXEIS_TERM_EMP','N','');
--inserting report security - HDS AP Terminated Employees Report
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','PAYABLES_MANAGER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_TRNS_ENTRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_INQUIRY_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_DISBURSEMTS_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_ADMIN_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','PAYABLES_INQUIRY',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_OIE_USER',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_MGR_NOSUP_US_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_PYBLS_MNGR',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_PYABLS_MNGR_CAN',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','101','','XXCUS_GL_MANAGER_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_MGR_NOSUP_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_INQ_CANADA',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_DISBURSEMTS_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS AP Terminated Employees Report','200','','HDS_AP_ADMIN_CAD_GSC',91000,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS AP Terminated Employees Report
--Inserting Report   Version details- HDS AP Terminated Employees Report
xxeis.eis_rsc_ins.rv( 'HDS AP Terminated Employees Report','','HDS AP Terminated Employees Report','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 91000');
END IF;
END;
/
