--Report Name            : White Cap Sales Rep Listing
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_SALESREP_LISTING_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_SALESREP_LISTING_V
Create or replace View XXEIS.EIS_XXWC_SALESREP_LISTING_V
 AS 
SELECT c.resource_name,
    b.user_name,
    a.salesrep_id,
    a.salesrep_number,
    b.source_mgr_name,
    (SELECT fu.user_name FROM fnd_user fu WHERE b.source_mgr_id = fu.employee_id
    ) Manager_NTID,
    'Account Manager' Account_Type,
    'Account Manager' Account_Type_P
  FROM JTF.JTF_RS_SALESREPS a,
    JTF.JTF_RS_RESOURCE_EXTNS b,
    JTF.JTF_RS_Resource_Extns_TL c
  WHERE a.status    = 'A'
  AND a.resource_id = b.resource_id
  AND a.resource_id = c.resource_id
  AND c.category    = 'EMPLOYEE'
  UNION
  SELECT c.resource_name,
    b.user_name,
    a.salesrep_id,
    a.salesrep_number,
    dw.fullname,
    dw.ntid,
    'House Account' Account_Type,
    'House Account' Account_Type_P
  FROM JTF.JTF_RS_SALESREPS a,
    JTF.JTF_RS_Resource_Extns b,
    JTF.JTF_RS_Resource_Extns_TL c,
    XXWC.DW_HOUSEACCTS dw
  WHERE a.resource_id   = b.resource_id
  AND a.resource_id     = c.resource_id
  AND a.salesrep_number = dw.salesrepnumber(+)
  AND c.category        = 'OTHER'
  UNION
  SELECT c.resource_name,
    b.user_name,
    a.salesrep_id,
    a.salesrep_number,
    dw.fullname,
    dw.ntid,
    'House Account' Account_Type,
    'ALL' Account_Type_P
  FROM JTF.JTF_RS_SALESREPS a,
    JTF.JTF_RS_Resource_Extns b,
    JTF.JTF_RS_Resource_Extns_TL c,
    XXWC.DW_HOUSEACCTS dw
  WHERE a.resource_id   = b.resource_id
  AND a.resource_id     = c.resource_id
  AND a.salesrep_number = dw.salesrepnumber(+)
  AND c.category        = 'OTHER'
  UNION
  SELECT c.resource_name,
    b.user_name,
    a.salesrep_id,
    a.salesrep_number,
    b.source_mgr_name,
    (SELECT fu.user_name FROM fnd_user fu WHERE b.source_mgr_id = fu.employee_id
    ) Manager_NTID,
    'Account Manager' Account_Type,
    'ALL' Account_Type_P
  FROM JTF.JTF_RS_SALESREPS a,
    JTF.JTF_RS_RESOURCE_EXTNS b,
    JTF.JTF_RS_Resource_Extns_TL c
  WHERE a.status    = 'A'
  AND a.resource_id = b.resource_id
  AND a.resource_id = c.resource_id
  AND c.category    = 'EMPLOYEE'
  ORDER BY resource_name/
set scan on define on
prompt Creating View Data for White Cap Sales Rep Listing
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_SALESREP_LISTING_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_SALESREP_LISTING_V',222,'','','','','HT038687','XXEIS','Eis Xxwc Salesrep Listing V','EXSLV','','');
--Delete View Columns for EIS_XXWC_SALESREP_LISTING_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_SALESREP_LISTING_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_SALESREP_LISTING_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_SALESREP_LISTING_V','ACCOUNT TYPE',222,'Account Type','ACCOUNT TYPE','','','','HT038687','VARCHAR2','','','Account Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_SALESREP_LISTING_V','MANAGER_NTID',222,'Manager Ntid','MANAGER_NTID','','','','HT038687','VARCHAR2','','','Manager Ntid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_SALESREP_LISTING_V','SOURCE_MGR_NAME',222,'Source Mgr Name','SOURCE_MGR_NAME','','','','HT038687','VARCHAR2','','','Source Mgr Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_SALESREP_LISTING_V','SALESREP_NUMBER',222,'Salesrep Number','SALESREP_NUMBER','','','','HT038687','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_SALESREP_LISTING_V','SALESREP_ID',222,'Salesrep Id','SALESREP_ID','','','','HT038687','NUMBER','','','Salesrep Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_SALESREP_LISTING_V','USER_NAME',222,'User Name','USER_NAME','','','','HT038687','VARCHAR2','','','User Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_SALESREP_LISTING_V','RESOURCE_NAME',222,'Resource Name','RESOURCE_NAME','','','','HT038687','VARCHAR2','','','Resource Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_SALESREP_LISTING_V','ACCOUNT_TYPE',222,'Account Type','ACCOUNT_TYPE','','','','HT038687','VARCHAR2','','','Account Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_SALESREP_LISTING_V','ACCOUNT_TYPE_P',222,'Account Type P','ACCOUNT_TYPE_P','','','','HT038687','VARCHAR2','','','Account Type P','','','');
--Inserting View Components for EIS_XXWC_SALESREP_LISTING_V
--Inserting View Component Joins for EIS_XXWC_SALESREP_LISTING_V
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap Sales Rep Listing
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap Sales Rep Listing
xxeis.eis_rs_ins.lov( 222,'SELECT        
''Account Manager'' acccount_type
FROM         JTF.JTF_RS_SALESREPS a
,JTF.JTF_RS_RESOURCE_EXTNS b
,JTF.JTF_RS_Resource_Extns_TL c
WHERE     a.status = ''A'' and
a.resource_id = b.resource_id and
         a.resource_id = c.resource_id and
          c.category = ''EMPLOYEE''
UNION
SELECT        
''House Account''  account_type
FROM        JTF.JTF_RS_SALESREPS a
,JTF.JTF_RS_Resource_Extns b
,JTF.JTF_RS_Resource_Extns_TL c
,XXWC.DW_HOUSEACCTS dw
WHERE     a.resource_id = b.resource_id and
          a.resource_id = c.resource_id and
          a.salesrep_number = dw.salesrepnumber and
          c.category = ''OTHER''
UNION
SELECT        
 CASE
  WHEN c.category in (''EMPLOYEE'',''OTHER'') then ''ALL''
 END 
 FROM        JTF.JTF_RS_SALESREPS a
,JTF.JTF_RS_Resource_Extns b
,JTF.JTF_RS_Resource_Extns_TL c
,XXWC.DW_HOUSEACCTS dw
WHERE     a.resource_id = b.resource_id and
          a.resource_id = c.resource_id and
          a.salesrep_number = dw.salesrepnumber and
          c.category in (''EMPLOYEE'',''OTHER'')','','Account Type','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap Sales Rep Listing
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap Sales Rep Listing
xxeis.eis_rs_utility.delete_report_rows( 'White Cap Sales Rep Listing' );
--Inserting Report - White Cap Sales Rep Listing
xxeis.eis_rs_ins.r( 222,'White Cap Sales Rep Listing','','The purpose of this report is to provide a complete listing of active White Cap Sales Representatives including Account Managers and House Accounts, as well as their respective Manager.','','','','HT038687','EIS_XXWC_SALESREP_LISTING_V','Y','','','HT038687','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - White Cap Sales Rep Listing
xxeis.eis_rs_ins.rc( 'White Cap Sales Rep Listing',222,'MANAGER_NTID','Manager NTID','Manager Ntid','','','default','','6','N','','','','','','','','HT038687','N','N','','EIS_XXWC_SALESREP_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Sales Rep Listing',222,'RESOURCE_NAME','Sales Rep Name','Resource Name','','','default','','1','N','','','','','','','','HT038687','N','N','','EIS_XXWC_SALESREP_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Sales Rep Listing',222,'SALESREP_ID','Sales Rep ID','Salesrep Id','','~~~','default','','3','N','','','','','','','','HT038687','N','N','','EIS_XXWC_SALESREP_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Sales Rep Listing',222,'SALESREP_NUMBER','Sales Rep Number','Salesrep Number','','','default','','4','N','','','','','','','','HT038687','N','N','','EIS_XXWC_SALESREP_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Sales Rep Listing',222,'SOURCE_MGR_NAME','Manager Name','Source Mgr Name','','','default','','5','N','','','','','','','','HT038687','N','N','','EIS_XXWC_SALESREP_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Sales Rep Listing',222,'USER_NAME','Sales Rep NTID','User Name','','','default','','2','N','','','','','','','','HT038687','N','N','','EIS_XXWC_SALESREP_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Sales Rep Listing',222,'ACCOUNT_TYPE','Account Type','Account Type','','','','','8','','Y','','','','','','','HT038687','N','N','','EIS_XXWC_SALESREP_LISTING_V','','');
--Inserting Report Parameters - White Cap Sales Rep Listing
xxeis.eis_rs_ins.rp( 'White Cap Sales Rep Listing',222,'Account_Type','Account_Type','ACCOUNT_TYPE_P','IN','Account Type','ALL','VARCHAR2','N','Y','1','','Y','CONSTANT','HT038687','Y','N','','','');
--Inserting Report Conditions - White Cap Sales Rep Listing
xxeis.eis_rs_ins.rcn( 'White Cap Sales Rep Listing',222,'ACCOUNT_TYPE_P','IN',':Account_Type','','','Y','1','Y','HT038687');
--Inserting Report Sorts - White Cap Sales Rep Listing
xxeis.eis_rs_ins.rs( 'White Cap Sales Rep Listing',222,'RESOURCE_NAME','ASC','HT038687','1','');
--Inserting Report Triggers - White Cap Sales Rep Listing
--Inserting Report Templates - White Cap Sales Rep Listing
--Inserting Report Portals - White Cap Sales Rep Listing
--Inserting Report Dashboards - White Cap Sales Rep Listing
--Inserting Report Security - White Cap Sales Rep Listing
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','21623',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','701','','50546',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','51045',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','50901',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','51025',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','50860',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','50886',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','50859',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','50858',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','50857',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','222','','22480',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','20005','','50843',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','20005','','51207',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','20005','','50861',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','20005','','50900',222,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'White Cap Sales Rep Listing','661','','50891',222,'HT038687','','');
--Inserting Report Pivots - White Cap Sales Rep Listing
END;
/
set scan on define on
