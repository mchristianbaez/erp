--Report Name            : HDS Payments - Weekly Credit and Deduction
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1016488_WGNYFJ_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1016488_WGNYFJ_V
xxeis.eis_rsc_ins.v( 'XXEIS_1016488_WGNYFJ_V',682,'Paste SQL View for HDS Payment - Weekly Credit and Deduction','1.0','','','DV003828','APPS','HDS Payment - Weekly Credit and Deduction View','X1WV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1016488_WGNYFJ_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1016488_WGNYFJ_V',682,FALSE);
--Inserting Object Columns for XXEIS_1016488_WGNYFJ_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','DOCUMENT_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Document Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','MVID',682,'','','','','','DV003828','VARCHAR2','','','Mvid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','MVID_NAME',682,'','','','','','DV003828','VARCHAR2','','','Mvid Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','BUSINESS_UNIT',682,'','','','','','DV003828','VARCHAR2','','','Business Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','DOCUMENT_NUMBER',682,'','','','','','DV003828','VARCHAR2','','','Document Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','PAYMENT_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','TRANSF_ADJ_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Transf Adj Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','DEPOSIT_DATE',682,'','','','','','DV003828','DATE','','','Deposit Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','DOCUMENT_DATE',682,'','','','','','DV003828','DATE','','','Document Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','AMOUNT',682,'','','','~T~D~2','','DV003828','NUMBER','','','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','GL_DATE',682,'','','','','','DV003828','DATE','','','Gl Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','IMAGE_URL',682,'','','','','','DV003828','VARCHAR2','','','Image Url','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','TIMEFRAME',682,'','','','','','DV003828','VARCHAR2','','','Timeframe','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','CREATION_DATE',682,'','','','','','DV003828','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','USER_NAME',682,'','','','','','DV003828','VARCHAR2','','','User Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','LAST_UPDATE_DATE',682,'','','','','','DV003828','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','CURRENCY_CODE',682,'','','','','','DV003828','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','ORG_NAME',682,'','','','','','DV003828','VARCHAR2','','','Org Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','STATUS',682,'','','','','','DV003828','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','REVERSAL_REASON',682,'','','','','','DV003828','VARCHAR2','','','Reversal Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','ORG_ID',682,'','','','','','DV003828','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1016488_WGNYFJ_V','REBATE_TYPE',682,'','','','','','DV003828','VARCHAR2','','','Rebate Type','','','','US');
--Inserting Object Components for XXEIS_1016488_WGNYFJ_V
--Inserting Object Component Joins for XXEIS_1016488_WGNYFJ_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Payments - Weekly Credit and Deduction
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Payments - Weekly Credit and Deduction
xxeis.eis_rsc_ins.lov( 682,'SELECT distinct hp.party_name LOB
 FROM apps.hz_parties hp
WHERE  hp.attribute1=''HDS_BU''
and ( hp.party_name in (''ELECTRICAL'',''UTILISERV'')
      or hp.status =''A'')','','HDS BU','BU FROM apps.hz_parties it''s party_name = LOB BUSINESS UNIT','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for HDS Payments - Weekly Credit and Deduction
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Payments - Weekly Credit and Deduction
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Payments - Weekly Credit and Deduction' );
--Inserting Report - HDS Payments - Weekly Credit and Deduction
xxeis.eis_rsc_ins.r( 682,'HDS Payments - Weekly Credit and Deduction','','Shows Credit Memo and Deduction payments
','','','','MC027824','XXEIS_1016488_WGNYFJ_V','Y','','SELECT DISTINCT 
      decode(CRH.STATUS,''REVERSED'',''REVERSAL'',''PAYMENT'') Document_Type, 
      HCA.ATTRIBUTE2 MVID,
      HP.PARTY_NAME MVID_NAME,
      HP1.PARTY_NAME BUSINESS_UNIT,
      CR.RECEIPT_NUMBER DOCUMENT_NUMBER,
      RM.PRINTED_NAME PAYMENT_TYPE,
      null Transf_Adj_Type,
      CR.DEPOSIT_DATE,
      CR.RECEIPT_DATE DOCUMENT_DATE,
      DECODE(CRH.STATUS,''REVERSED'',CR.AMOUNT*-1,CR.AMOUNT) AMOUNT,
      TRUNC(DECODE(CR.STATUS,''REV'',CRH.GL_DATE,SCH.GL_DATE)) GL_DATE,
      CR.COMMENTS IMAGE_URL,
      CR.ATTRIBUTE2 TIMEFRAME,
      CR.ATTRIBUTE10 REBATE_TYPE,
      trunc(CR.CREATION_DATE) CREATION_DATE,
      USR.USER_NAME USER_NAME,
      CR.LAST_UPDATE_DATE,
      CR.CURRENCY_CODE CURRENCY_CODE,
      HR.NAME ORG_NAME,
      CR.STATUS STATUS,
      CR.REVERSAL_COMMENTS REVERSAL_REASON, 
      CR.ORG_ID ORG_ID    
 FROM  
       APPS.AR_CASH_RECEIPTS_ALL CR ,
      AR.AR_CASH_RECEIPT_HISTORY_ALL CRH,
      AR.AR_RECEIVABLE_APPLICATIONS_ALL RA,
      APPS.AR_RECEIPT_METHODS RM,
      APPS.HZ_PARTIES HP,
      APPS.HZ_CUST_ACCOUNTS HCA,
      APPS.hz_parties hp1,
      APPS.HR_OPERATING_UNITS HR,
      APPS.AR_PAYMENT_SCHEDULES_ALL SCH,
      APPS.AR_RECEIVABLES_TRX_ALL         ART ,
      APPS.FND_USER USR      
WHERE    1 = 1
      AND CR.ATTRIBUTE1                  = TO_CHAR(HP1.PARTY_ID)
      AND CR.RECEIPT_METHOD_ID           = RM.RECEIPT_METHOD_ID
      AND RA.CASH_RECEIPT_ID(+)          = CR.CASH_RECEIPT_ID
      AND HR.ORGANIZATION_ID             = CR.ORG_ID
      AND CRH.cash_receipt_id=CR.cash_receipt_id
      AND SCH.CASH_RECEIPT_ID= CR.CASH_RECEIPT_ID 
      AND HCA.CUST_ACCOUNT_ID            = CR.PAY_FROM_CUSTOMER
      AND HCA.PARTY_ID                   = HP.PARTY_ID
      AND USR.USER_ID                    = CR.CREATED_BY
      AND CR.ORG_ID                     IN (101, 102)
','MC027824','','N','PAYMENTS ','','CSV,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS Payments - Weekly Credit and Deduction
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'ORG_NAME','Org Name','','','','default','','20','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'MVID','Mvid','','','','default','','2','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'MVID_NAME','Mvid Name','','','','default','','3','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'DOCUMENT_DATE','Document Date','','','','default','','9','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'USER_NAME','User Name','','','','default','','16','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'BUSINESS_UNIT','Business Unit','','','','default','','4','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'DOCUMENT_NUMBER','Document Number','','','','default','','5','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'PAYMENT_TYPE','Payment Type','','','','default','','6','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'GL_DATE','Gl Date','','','','default','','11','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'DEPOSIT_DATE','Deposit Date','','','','default','','8','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'CURRENCY_CODE','Currency Code','','','','default','','18','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'LAST_UPDATE_DATE','Last Update Date','','','','default','','17','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'CREATION_DATE','Creation Date','','','','default','','15','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'AMOUNT','Amount','','','~,~.~2','default','','10','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'TIMEFRAME','Timeframe','','','','default','','13','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'IMAGE_URL','Image Url','','','','default','','12','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'TRANSF_ADJ_TYPE','Transf Adj Type','','','','default','','7','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'REVERSAL_REASON','Reversal Reason','','','','default','','21','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'STATUS','Status','','','','default','','19','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'DOCUMENT_TYPE','Document Type','','','','default','','1','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payments - Weekly Credit and Deduction',682,'REBATE_TYPE','Rebate Type','','','','','','14','N','','','','','','','','MC027824','N','N','','XXEIS_1016488_WGNYFJ_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Payments - Weekly Credit and Deduction
xxeis.eis_rsc_ins.rp( 'HDS Payments - Weekly Credit and Deduction',682,'Business Unit','','BUSINESS_UNIT','IN','HDS BU','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1016488_WGNYFJ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payments - Weekly Credit and Deduction',682,'Gl Date From','','GL_DATE','>=','','','DATE','N','Y','3','','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1016488_WGNYFJ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payments - Weekly Credit and Deduction',682,'GL Date To','','GL_DATE','<=','','','DATE','N','Y','4','','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1016488_WGNYFJ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payments - Weekly Credit and Deduction',682,'Payment Type','','PAYMENT_TYPE','IN','','''REBATE_CREDIT'',''REBATE_DEDUCTION''','VARCHAR2','N','Y','6','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1016488_WGNYFJ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payments - Weekly Credit and Deduction',682,'Creation Date From','','CREATION_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1016488_WGNYFJ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payments - Weekly Credit and Deduction',682,'Creation Date To','','CREATION_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1016488_WGNYFJ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payments - Weekly Credit and Deduction',682,'Org Id','','ORG_ID','IN','','''101''','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1016488_WGNYFJ_V','','','US','');
--Inserting Dependent Parameters - HDS Payments - Weekly Credit and Deduction
--Inserting Report Conditions - HDS Payments - Weekly Credit and Deduction
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Weekly Credit and Deduction',682,'BUSINESS_UNIT IN :Business Unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BUSINESS_UNIT','','Business Unit','','','','','XXEIS_1016488_WGNYFJ_V','','','','','','IN','Y','Y','','','','','1',682,'HDS Payments - Weekly Credit and Deduction','BUSINESS_UNIT IN :Business Unit ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Weekly Credit and Deduction',682,'CREATION_DATE >= :Creation Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date From','','','','','XXEIS_1016488_WGNYFJ_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',682,'HDS Payments - Weekly Credit and Deduction','CREATION_DATE >= :Creation Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Weekly Credit and Deduction',682,'CREATION_DATE <= :Creation Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date To','','','','','XXEIS_1016488_WGNYFJ_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',682,'HDS Payments - Weekly Credit and Deduction','CREATION_DATE <= :Creation Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Weekly Credit and Deduction',682,'GL_DATE <= :GL Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','GL Date To','','','','','XXEIS_1016488_WGNYFJ_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',682,'HDS Payments - Weekly Credit and Deduction','GL_DATE <= :GL Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Weekly Credit and Deduction',682,'GL_DATE >= :Gl Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','GL_DATE','','Gl Date From','','','','','XXEIS_1016488_WGNYFJ_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',682,'HDS Payments - Weekly Credit and Deduction','GL_DATE >= :Gl Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Weekly Credit and Deduction',682,'ORG_ID IN :Org Id ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORG_ID','','Org Id','','','','','XXEIS_1016488_WGNYFJ_V','','','','','','IN','Y','Y','','','','','1',682,'HDS Payments - Weekly Credit and Deduction','ORG_ID IN :Org Id ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Weekly Credit and Deduction',682,'PAYMENT_TYPE IN :Payment Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_TYPE','','Payment Type','','','','','XXEIS_1016488_WGNYFJ_V','','','','','','IN','Y','Y','','','','','1',682,'HDS Payments - Weekly Credit and Deduction','PAYMENT_TYPE IN :Payment Type ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payments - Weekly Credit and Deduction',682,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and (X1WV.PAYMENT_TYPE in  (''REBATE_CREDIT'',''REBATE_DEDUCTION'')
OR X1WV.DOCUMENT_TYPE =''TRANSFER/ADJUSTMENTS'')','1',682,'HDS Payments - Weekly Credit and Deduction','Free Text ');
--Inserting Report Sorts - HDS Payments - Weekly Credit and Deduction
xxeis.eis_rsc_ins.rs( 'HDS Payments - Weekly Credit and Deduction',682,'BUSINESS_UNIT','ASC','MC027824','1','');
xxeis.eis_rsc_ins.rs( 'HDS Payments - Weekly Credit and Deduction',682,'DOCUMENT_TYPE','ASC','MC027824','2','');
xxeis.eis_rsc_ins.rs( 'HDS Payments - Weekly Credit and Deduction',682,'MVID','ASC','MC027824','3','');
xxeis.eis_rsc_ins.rs( 'HDS Payments - Weekly Credit and Deduction',682,'DOCUMENT_NUMBER','ASC','MC027824','4','');
--Inserting Report Triggers - HDS Payments - Weekly Credit and Deduction
--inserting report templates - HDS Payments - Weekly Credit and Deduction
--Inserting Report Portals - HDS Payments - Weekly Credit and Deduction
--inserting report dashboards - HDS Payments - Weekly Credit and Deduction
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Payments - Weekly Credit and Deduction','682','XXEIS_1016488_WGNYFJ_V','XXEIS_1016488_WGNYFJ_V','N','');
--inserting report security - HDS Payments - Weekly Credit and Deduction
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Weekly Credit and Deduction','682','','XXCUS_TM_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Weekly Credit and Deduction','682','','XXCUS_TM_ADMIN_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Weekly Credit and Deduction','682','','XXCUS_TM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Weekly Credit and Deduction','682','','XXCUS_TM_ADM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payments - Weekly Credit and Deduction','20005','','XXWC_VIEW_ALL_EIS_REPORTS',682,'MC027824','','','');
--Inserting Report Pivots - HDS Payments - Weekly Credit and Deduction
--Inserting Report   Version details- HDS Payments - Weekly Credit and Deduction
xxeis.eis_rsc_ins.rv( 'HDS Payments - Weekly Credit and Deduction','','HDS Payments - Weekly Credit and Deduction','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
