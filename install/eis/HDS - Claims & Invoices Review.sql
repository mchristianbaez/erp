--Report Name            : HDS - Claims & Invoices Review
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_1121489_GCHMJX_V
CREATE OR REPLACE VIEW APPS.XXEIS_1121489_GCHMJX_V (MVID, MV_NAME, ORG_ID, BU, CLAIM_NUMBER, INVOICE_NUMBER, AGREEMENT_YEAR, PLAN_ID, AGREEMENT_NAME, PAYMENT_METHOD, FREQUENCY, CLAIM_AMOUNT, CLAIM_DATE, ORIGINAL_INVOICE_AMT, ADJUSTED_INVOICE_AMT, INVOICE_AMOUNT)
AS
  SELECT MVID ,
    MV_NAME ,
    ORG_ID ,
    BU ,
    CLAIM_NUMBER ,
    INVOICE_NUMBER ,
    AGREEMENT_YEAR ,
    PLAN_ID ,
    AGREEMENT_NAME ,
    PAYMENT_METHOD ,
    FREQUENCY ,
    CLAIM_AMOUNT ,
    CLAIM_DATE ,
    ORIGINAL ORIGINAL_INVOICE_AMT ,
    ADJUSTED ADJUSTED_INVOICE_AMT ,
    NVL(ORIGINAL,0)+NVL(ADJUSTED,0) INVOICE_AMOUNT
  FROM
    (SELECT C.CUSTOMER_ATTRIBUTE2 MVID ,
      C.PARTY_NAME MV_NAME ,
      HDR.ORG_ID ,
      ra.attribute2 BU ,
      HDR.CLAIM_NUMBER ,
      RA.TRX_NUMBER INVOICE_NUMBER ,
      QLHV.ATTRIBUTE7 AGREEMENT_YEAR ,
      xrf.activity_id PLAN_ID ,
      XRF.ATTRIBUTE2 AGREEMENT_NAME ,
      XRF.ATTRIBUTE8 PAYMENT_METHOD ,
      HDR.ATTRIBUTE3 FREQUENCY ,
      HDR.ACCTD_AMOUNT CLAIM_AMOUNT ,
      APSA.AMOUNT_DUE_ORIGINAL ORIGINAL ,
      apsa.amount_adjusted ADJUSTED ,
      TRUNC(HDR.CLAIM_DATE) CLAIM_DATE
    FROM APPS.OZF_CLAIMS_ALL HDR,
      APPS.OZF_CLAIM_LINES_UTIL_ALL LN,
      APPS.OZF_CLAIM_LINES_ALL XRF,
      XXCUS.XXCUS_REBATE_CUSTOMERS C,
      APPS.RA_CUSTOMER_TRX_ALL RA,
      APPS.AR_PAYMENT_SCHEDULES_ALL apsa,
      APPS.QP_LIST_HEADERS_VL QLHV
    WHERE 1                    = 1
    AND HDR.CLAIM_ID           = XRF.CLAIM_ID
    AND LN.CLAIM_LINE_ID       = XRF.CLAIM_LINE_ID
    AND HDR.ORG_ID            IN ('101', '102')
    AND HDR.CUST_ACCOUNT_ID    =C.CUSTOMER_ID
    AND HDR.CLAIM_NUMBER       =RA.INTERFACE_HEADER_ATTRIBUTE1(+)
    AND APSA.CUSTOMER_TRX_ID(+)=RA.CUSTOMER_TRX_ID
    AND xrf.activity_id        =QLHV.LIST_HEADER_ID
    GROUP BY C.CUSTOMER_ATTRIBUTE2 ,
      C.PARTY_NAME ,
      HDR.ORG_ID ,
      ra.attribute2 ,
      HDR.CLAIM_NUMBER ,
      RA.TRX_NUMBER ,
      QLHV.ATTRIBUTE7 ,
      XRF.ATTRIBUTE2 ,
      XRF.ATTRIBUTE8 ,
      HDR.ATTRIBUTE3 ,
      HDR.ACCTD_AMOUNT ,
      xrf.activity_id ,
      APSA.AMOUNT_DUE_ORIGINAL ,
      apsa.amount_adjusted ,
      TRUNC(HDR.CLAIM_DATE) ,
      HDR.ATTRIBUTE6
    ORDER BY XRF.ATTRIBUTE2 ,
      HDR.ORG_ID ,
      RA.TRX_NUMBER
) 
/
prompt Creating Object Data XXEIS_1121489_GCHMJX_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1121489_GCHMJX_V
xxeis.eis_rsc_ins.v( 'XXEIS_1121489_GCHMJX_V',682,'Paste SQL View for HDS - CLAIMS/INVOICES GENERATED','1.0','','','DV003828','APPS','HDS - CLAIMS/INVOICES GENERATED View','X1GV12','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1121489_GCHMJX_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1121489_GCHMJX_V',682,FALSE);
--Inserting Object Columns for XXEIS_1121489_GCHMJX_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','MVID',682,'','','','','','DV003828','VARCHAR2','','','Mvid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','ORG_ID',682,'','','','','','DV003828','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','CLAIM_NUMBER',682,'','','','','','DV003828','VARCHAR2','','','Claim Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','INVOICE_NUMBER',682,'','','','','','DV003828','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','AGREEMENT_YEAR',682,'','','','','','DV003828','VARCHAR2','','','Agreement Year','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','AGREEMENT_NAME',682,'','','','','','DV003828','VARCHAR2','','','Agreement Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','PAYMENT_METHOD',682,'','','','','','DV003828','VARCHAR2','','','Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','FREQUENCY',682,'','','','','','DV003828','VARCHAR2','','','Frequency','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','CLAIM_AMOUNT',682,'','','','~T~D~2','','DV003828','NUMBER','','','Claim Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','CLAIM_DATE',682,'','','','','','DV003828','DATE','','','Claim Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','BU',682,'','','','','','DV003828','VARCHAR2','','','Bu','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','ADJUSTED_INVOICE_AMT',682,'','','','~T~D~2','','DV003828','NUMBER','','','Adjusted Invoice Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','INVOICE_AMOUNT',682,'','','','~T~D~2','','DV003828','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','MV_NAME',682,'','','','','','DV003828','VARCHAR2','','','Mv Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','ORIGINAL_INVOICE_AMT',682,'','','','~T~D~2','','DV003828','NUMBER','','','Original Invoice Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1121489_GCHMJX_V','PLAN_ID',682,'','','','','','DV003828','NUMBER','','','Plan Id','','','','US');
--Inserting Object Components for XXEIS_1121489_GCHMJX_V
--Inserting Object Component Joins for XXEIS_1121489_GCHMJX_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for HDS - Claims & Invoices Review
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS - Claims & Invoices Review
xxeis.eis_rsc_ins.lov( 682,'select distinct nvl(attribute7, ''NULL'') agreement_year
from apps.QP_LIST_HEADERS_VL','','LOV AGREEMENT_YEAR','LOV attribute7 from apps.QP_LIST_HEADERS_VL  table is the agreement year','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_MVID''','','LOV VENDOR_NAME','VENDOR NAME FROM ar.HZ_PARTIES
','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'SELECT 
DISTINCT ATTRIBUTE5
FROM APPS.QP_LIST_HEADERS_VL
where attribute5 is  not null','','PAYMENT METHOD','Payment MEthod as 
SELECT 
DISTINCT ATTRIBUTE5
FROM APPS.QP_LIST_HEADERS_VL
where attribute5 is  not null','DV003828',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for HDS - Claims & Invoices Review
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS - Claims & Invoices Review
xxeis.eis_rsc_utility.delete_report_rows( 'HDS - Claims & Invoices Review' );
--Inserting Report - HDS - Claims & Invoices Review
xxeis.eis_rsc_ins.r( 682,'HDS - Claims & Invoices Review','','Shows  Claims and related Invoice numbers  generated when Create Invoice Process is run. Use this report for balancing purposes','','','','MC027824','XXEIS_1121489_GCHMJX_V','Y','','SELECT 
    MVID
    ,MV_NAME
    ,ORG_ID
    ,BU
    ,CLAIM_NUMBER
    ,INVOICE_NUMBER
    ,AGREEMENT_YEAR
    ,PLAN_ID
    ,AGREEMENT_NAME
    ,PAYMENT_METHOD
    ,FREQUENCY
    ,CLAIM_AMOUNT
    ,CLAIM_DATE
    ,ORIGINAL ORIGINAL_INVOICE_AMT
    ,ADJUSTED ADJUSTED_INVOICE_AMT
    ,nvl(ORIGINAL,0)+nvl(ADJUSTED,0) INVOICE_AMOUNT
    FROM
    (
SELECT 
     C.CUSTOMER_ATTRIBUTE2 MVID
    ,C.PARTY_NAME MV_NAME
    ,HDR.ORG_ID
    ,ra.attribute2 BU
    ,HDR.CLAIM_NUMBER
    ,RA.TRX_NUMBER INVOICE_NUMBER
    ,QLHV.ATTRIBUTE7 AGREEMENT_YEAR
    ,xrf.activity_id PLAN_ID
    ,XRF.ATTRIBUTE2 AGREEMENT_NAME
    ,XRF.ATTRIBUTE8 PAYMENT_METHOD
    ,HDR.ATTRIBUTE3 FREQUENCY
    ,HDR.ACCTD_AMOUNT CLAIM_AMOUNT
    ,APSA.AMOUNT_DUE_ORIGINAL ORIGINAL
    ,apsa.amount_adjusted ADJUSTED
    ,TRUNC(HDR.CLAIM_DATE) CLAIM_DATE
FROM 
   APPS.OZF_CLAIMS_ALL HDR,
   APPS.OZF_CLAIM_LINES_UTIL_ALL LN,
   APPS.OZF_CLAIM_LINES_ALL XRF,
   XXCUS.XXCUS_REBATE_CUSTOMERS C,
  APPS.RA_CUSTOMER_TRX_ALL  RA,
  APPS.AR_PAYMENT_SCHEDULES_ALL apsa,
   APPS.QP_LIST_HEADERS_VL QLHV  
WHERE 1              = 1 
    AND HDR.CLAIM_ID    = XRF.CLAIM_ID
    AND LN.CLAIM_LINE_ID = XRF.CLAIM_LINE_ID
    AND HDR.ORG_ID      IN (''101'', ''102'')
    AND HDR.CUST_ACCOUNT_ID=C.CUSTOMER_ID
    AND HDR.CLAIM_NUMBER=RA.INTERFACE_HEADER_ATTRIBUTE1(+)
    AND APSA.CUSTOMER_TRX_ID(+)=RA.CUSTOMER_TRX_ID
    and xrf.activity_id=QLHV.LIST_HEADER_ID
GROUP BY
     C.CUSTOMER_ATTRIBUTE2 
    ,C.PARTY_NAME 
    ,HDR.ORG_ID
    ,ra.attribute2
    ,HDR.CLAIM_NUMBER
    ,RA.TRX_NUMBER 
    ,QLHV.ATTRIBUTE7
    ,XRF.ATTRIBUTE2 
    ,XRF.ATTRIBUTE8 
    ,HDR.ATTRIBUTE3 
    ,HDR.ACCTD_AMOUNT 
    ,xrf.activity_id
    ,APSA.AMOUNT_DUE_ORIGINAL
    ,apsa.amount_adjusted
    ,TRUNC(HDR.CLAIM_DATE) 
    ,HDR.ATTRIBUTE6     
    ORDER BY 
    XRF.ATTRIBUTE2
    ,HDR.ORG_ID
    ,RA.TRX_NUMBER 
)
','MC027824','','N','PAYMENTS ','','CSV,Pivot Excel,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - HDS - Claims & Invoices Review
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'CLAIM_DATE','Claim Date','','','','default','','10','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'CLAIM_NUMBER','Claim Number','','','','default','','9','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'FREQUENCY','Frequency','','','','default','','8','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'CLAIM_AMOUNT','Claim Amount','','','~,~.~2','default','','11','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'PAYMENT_METHOD','Payment Method','','','','default','','7','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'AGREEMENT_YEAR','Agreement Year','','','','default','','2','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'MVID','Mvid','','','','default','','4','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'ORG_ID','Org Id','','','~T~D~0','default','','1','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'INVOICE_NUMBER','Invoice Number','','','','default','','12','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'AGREEMENT_NAME','Agreement Name','','','','default','','6','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'BU','Bu','','','','default','','3','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'ADJUSTED_INVOICE_AMT','Adjusted Invoice Amt','','','~,~.~2','default','','14','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'INVOICE_AMOUNT','Invoice Amount','','','~,~.~2','default','','15','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'MV_NAME','Mv Name','','','','default','','5','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS - Claims & Invoices Review',682,'ORIGINAL_INVOICE_AMT','Original Invoice Amt','','','~,~.~2','default','','13','N','','','','','','','','MC027824','N','N','','XXEIS_1121489_GCHMJX_V','','','SUM','US','');
--Inserting Report Parameters - HDS - Claims & Invoices Review
xxeis.eis_rsc_ins.rp( 'HDS - Claims & Invoices Review',682,'Agreement Year','','AGREEMENT_YEAR','IN','LOV AGREEMENT_YEAR','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1121489_GCHMJX_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Claims & Invoices Review',682,'Claim Date From','','CLAIM_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1121489_GCHMJX_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Claims & Invoices Review',682,'Payment Method','','PAYMENT_METHOD','IN','PAYMENT METHOD','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1121489_GCHMJX_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Claims & Invoices Review',682,'Claim Date To','','CLAIM_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1121489_GCHMJX_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS - Claims & Invoices Review',682,'Vendor','','MV_NAME','IN','LOV VENDOR_NAME','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_1121489_GCHMJX_V','','','US','');
--Inserting Dependent Parameters - HDS - Claims & Invoices Review
--Inserting Report Conditions - HDS - Claims & Invoices Review
xxeis.eis_rsc_ins.rcnh( 'HDS - Claims & Invoices Review',682,'AGREEMENT_YEAR IN :Agreement Year ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','AGREEMENT_YEAR','','Agreement Year','','','','','XXEIS_1121489_GCHMJX_V','','','','','','IN','Y','Y','','','','','1',682,'HDS - Claims & Invoices Review','AGREEMENT_YEAR IN :Agreement Year ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Claims & Invoices Review',682,'CLAIM_DATE >= :Claim Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CLAIM_DATE','','Claim Date From','','','','','XXEIS_1121489_GCHMJX_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',682,'HDS - Claims & Invoices Review','CLAIM_DATE >= :Claim Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Claims & Invoices Review',682,'CLAIM_DATE <= :Claim Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CLAIM_DATE','','Claim Date To','','','','','XXEIS_1121489_GCHMJX_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',682,'HDS - Claims & Invoices Review','CLAIM_DATE <= :Claim Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Claims & Invoices Review',682,'MV_NAME IN :Vendor ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','MV_NAME','','Vendor','','','','','XXEIS_1121489_GCHMJX_V','','','','','','IN','Y','Y','','','','','1',682,'HDS - Claims & Invoices Review','MV_NAME IN :Vendor ');
xxeis.eis_rsc_ins.rcnh( 'HDS - Claims & Invoices Review',682,'PAYMENT_METHOD IN :Payment Method ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_METHOD','','Payment Method','','','','','XXEIS_1121489_GCHMJX_V','','','','','','IN','Y','Y','','','','','1',682,'HDS - Claims & Invoices Review','PAYMENT_METHOD IN :Payment Method ');
--Inserting Report Sorts - HDS - Claims & Invoices Review
xxeis.eis_rsc_ins.rs( 'HDS - Claims & Invoices Review',682,'BU','ASC','MC027824','1','');
xxeis.eis_rsc_ins.rs( 'HDS - Claims & Invoices Review',682,'MVID','ASC','MC027824','2','');
xxeis.eis_rsc_ins.rs( 'HDS - Claims & Invoices Review',682,'AGREEMENT_NAME','ASC','MC027824','3','');
--Inserting Report Triggers - HDS - Claims & Invoices Review
--inserting report templates - HDS - Claims & Invoices Review
--Inserting Report Portals - HDS - Claims & Invoices Review
--inserting report dashboards - HDS - Claims & Invoices Review
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS - Claims & Invoices Review','682','XXEIS_1121489_GCHMJX_V','XXEIS_1121489_GCHMJX_V','N','');
--inserting report security - HDS - Claims & Invoices Review
xxeis.eis_rsc_ins.rsec( 'HDS - Claims & Invoices Review','682','','XXCUS_TM_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'HDS - Claims & Invoices Review','682','','XXCUS_TM_ADMIN_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'HDS - Claims & Invoices Review','682','','XXCUS_TM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'HDS - Claims & Invoices Review','682','','XXCUS_TM_ADM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'HDS - Claims & Invoices Review','20005','','XXWC_VIEW_ALL_EIS_REPORTS',682,'MC027824','','','');
--Inserting Report Pivots - HDS - Claims & Invoices Review
--Inserting Report   Version details- HDS - Claims & Invoices Review
xxeis.eis_rsc_ins.rv( 'HDS - Claims & Invoices Review','','HDS - Claims & Invoices Review','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
