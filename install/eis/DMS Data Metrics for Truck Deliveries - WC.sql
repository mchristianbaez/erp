--Report Name            : DMS Data Metrics for Truck Deliveries - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_DMS_TRUCK_DELIVERY_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_DMS_TRUCK_DELIVERY_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Dms Truck Delivery V','EXDTDV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_DMS_TRUCK_DELIVERY_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_DMS_TRUCK_DELIVERY_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_DMS_TRUCK_DELIVERY_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','HZ_PARTY_ID',660,'Hz Party Id','HZ_PARTY_ID','','','','SA059956','NUMBER','','','Hz Party Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','HCA_CUST_ACCOUNT_ID',660,'Hca Cust Account Id','HCA_CUST_ACCOUNT_ID','','','','SA059956','NUMBER','','','Hca Cust Account Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','OOD_ORGANIZATION_ID',660,'Ood Organization Id','OOD_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Ood Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','OOH_HEADER_ID',660,'Ooh Header Id','OOH_HEADER_ID','','','','SA059956','NUMBER','','','Ooh Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','ORDER_TYPE_ID',660,'Order Type Id','ORDER_TYPE_ID','','','','SA059956','NUMBER','','','Order Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','ORDER_CREATION_DATE',660,'Order Creation Date','ORDER_CREATION_DATE','','','','SA059956','DATE','','','Order Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','SHIPMENT_DATE',660,'Shipment Date','SHIPMENT_DATE','','','','SA059956','DATE','','','Shipment Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','SHIP_METHOD',660,'Ship Method','SHIP_METHOD','','','','SA059956','VARCHAR2','','','Ship Method','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','SA059956','VARCHAR2','','','Order Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','ASSOCIATE_NAME',660,'Associate Name','ASSOCIATE_NAME','','','','SA059956','VARCHAR2','','','Associate Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','BRANCH',660,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','DISTRICT',660,'District','DISTRICT','','','','SA059956','VARCHAR2','','','District','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','REGION',660,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US','');
--Inserting Object Components for EIS_XXWC_DMS_TRUCK_DELIVERY_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','OE_ORDER_HEADERS_ALL',660,'OE_ORDER_HEADERS_ALL','OOH','OOH','SA059956','SA059956','-1','Oe Order Headers All Stores Header Information For','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','OOD','OOD','SA059956','SA059956','-1','Inventory Control Options And Defaults','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','SA059956','SA059956','-1','Stores Information About Customer Accounts.','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','HZ_PARTIES',660,'HZ_PARTIES','HZ','HZ','SA059956','SA059956','-1','Information About Parties Such As Organizations, P','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_DMS_TRUCK_DELIVERY_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','OE_ORDER_HEADERS_ALL','OOH',660,'EXDTDV.OOH_HEADER_ID','=','OOH.HEADER_ID(+)','','','1','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','MTL_PARAMETERS','OOD',660,'EXDTDV.OOD_ORGANIZATION_ID','=','OOD.ORGANIZATION_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','HZ_CUST_ACCOUNTS','HCA',660,'EXDTDV.HCA_CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_DMS_TRUCK_DELIVERY_V','HZ_PARTIES','HZ',660,'EXDTDV.HZ_PARTY_ID','=','HZ.PARTY_ID(+)','','','','Y','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report DMS Data Metrics for Truck Deliveries - WC
prompt Creating Report Data for DMS Data Metrics for Truck Deliveries - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - DMS Data Metrics for Truck Deliveries - WC
xxeis.eis_rsc_utility.delete_report_rows( 'DMS Data Metrics for Truck Deliveries - WC',660 );
--Inserting Report - DMS Data Metrics for Truck Deliveries - WC
xxeis.eis_rsc_ins.r( 660,'DMS Data Metrics for Truck Deliveries - WC','','Report provides the DMS usage percentage for truck deliveries','','','','SA059956','EIS_XXWC_DMS_TRUCK_DELIVERY_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - DMS Data Metrics for Truck Deliveries - WC
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'ASSOCIATE_NAME','Associate Name','Associate Name','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'BRANCH','Branch','Branch','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'DISTRICT','District','District','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'REGION','Region','Region','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'SHIP_METHOD','Ship Method','Ship Method','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'SHIPMENT_DATE','Shipment Date','Shipment Date','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS Data Metrics for Truck Deliveries - WC',660,'ORDER_CREATION_DATE','Creation Date','Order Creation Date','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','US','','');
--Inserting Report Parameters - DMS Data Metrics for Truck Deliveries - WC
xxeis.eis_rsc_ins.rp( 'DMS Data Metrics for Truck Deliveries - WC',660,'To Date','To Date','ORDER_CREATION_DATE','<=','','','DATE','Y','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','US','');
xxeis.eis_rsc_ins.rp( 'DMS Data Metrics for Truck Deliveries - WC',660,'From Date','From Date','ORDER_CREATION_DATE','>=','','','DATE','Y','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','US','');
--Inserting Dependent Parameters - DMS Data Metrics for Truck Deliveries - WC
--Inserting Report Conditions - DMS Data Metrics for Truck Deliveries - WC
xxeis.eis_rsc_ins.rcnh( 'DMS Data Metrics for Truck Deliveries - WC',660,'EXDTDV.ORDER_CREATION_DATE >= From Date','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_CREATION_DATE','','From Date','','','','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'DMS Data Metrics for Truck Deliveries - WC','EXDTDV.ORDER_CREATION_DATE >= From Date');
xxeis.eis_rsc_ins.rcnh( 'DMS Data Metrics for Truck Deliveries - WC',660,'EXDTDV.ORDER_CREATION_DATE <= To Date','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_CREATION_DATE','','To Date','','','','','EIS_XXWC_DMS_TRUCK_DELIVERY_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'DMS Data Metrics for Truck Deliveries - WC','EXDTDV.ORDER_CREATION_DATE <= To Date');
--Inserting Report Sorts - DMS Data Metrics for Truck Deliveries - WC
xxeis.eis_rsc_ins.rs( 'DMS Data Metrics for Truck Deliveries - WC',660,'ORDER_NUMBER','ASC','SA059956','1','');
--Inserting Report Triggers - DMS Data Metrics for Truck Deliveries - WC
--inserting report templates - DMS Data Metrics for Truck Deliveries - WC
--Inserting Report Portals - DMS Data Metrics for Truck Deliveries - WC
--inserting report dashboards - DMS Data Metrics for Truck Deliveries - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'DMS Data Metrics for Truck Deliveries - WC','660','EIS_XXWC_DMS_TRUCK_DELIVERY_V','EIS_XXWC_DMS_TRUCK_DELIVERY_V','N','');
--inserting report security - DMS Data Metrics for Truck Deliveries - WC
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','ONT_ICP_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_SHIPPING_TRANSACTIONS',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS Data Metrics for Truck Deliveries - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
--Inserting Report Pivots - DMS Data Metrics for Truck Deliveries - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- DMS Data Metrics for Truck Deliveries - WC
xxeis.eis_rsc_ins.rv( 'DMS Data Metrics for Truck Deliveries - WC','','DMS Data Metrics for Truck Deliveries - WC','SA059956','15-AUG-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
