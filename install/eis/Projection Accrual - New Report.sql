/*
 Ticket# TMS  20160620-00178 / ESMS 357886
 Author: Balaguru Seshadri
 Date: 06/30/2016
 Report Name            : Projection Accrual - New Report
 Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator.   
*/
prompt Creating View Data for Projection Accrual - New Report
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_2141503_NAGSSW_V
xxeis.eis_rs_ins.v( 'XXEIS_2141503_NAGSSW_V',682,'Paste SQL View for Projection Accrual - New Function Test','1.0','','','BS006141','APPS','Projection Accrual - New Function Test View','X2NV','','');
--Delete View Columns for XXEIS_2141503_NAGSSW_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_2141503_NAGSSW_V',682,FALSE);
--Inserting View Columns for XXEIS_2141503_NAGSSW_V
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','MVID',682,'','','','','','BS006141','VARCHAR2','','','Mvid','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','VENDOR',682,'','','','','','BS006141','VARCHAR2','','','Vendor','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','LOB',682,'','','','','','BS006141','VARCHAR2','','','Lob','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','BU',682,'','','','','','BS006141','VARCHAR2','','','Bu','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','PURCHASES',682,'','','','','','BS006141','NUMBER','','','Purchases','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','OTHER_YEARS_ADJ',682,'','','','','','BS006141','NUMBER','','','Other Years Adj','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','PRIOR_YEAR_ADJ',682,'','','','','','BS006141','NUMBER','','','Prior Year Adj','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','CURRENT_YEAR_ADJ',682,'','','','','','BS006141','NUMBER','','','Current Year Adj','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','CURRENT_PERIOD_ACCRUALS',682,'','','','','','BS006141','NUMBER','','','Current Period Accruals','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','PROJECTED_ACCRUALS',682,'','','','','','BS006141','NUMBER','','','Projected Accruals','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','REBATE_PURCHASES',682,'','','','','','BS006141','NUMBER','','','Rebate Purchases','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','REBATE_OTHER_YEARS_ADJ',682,'','','','','','BS006141','NUMBER','','','Rebate Other Years Adj','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','REBATE_PRIOR_YEAR_ADJ',682,'','','','','','BS006141','NUMBER','','','Rebate Prior Year Adj','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','REBATE_CURRENT_YEAR_ADJ',682,'','','','','','BS006141','NUMBER','','','Rebate Current Year Adj','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','REBATE_PERIOD_ACCRUALS',682,'','','','','','BS006141','NUMBER','','','Rebate Period Accruals','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','REBATE_PROJECTED_ACCRUALS',682,'','','','','','BS006141','NUMBER','','','Rebate Projected Accruals','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','COOP_PURCHASES',682,'','','','','','BS006141','NUMBER','','','Coop Purchases','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','COOP_OTHER_YEARS_ADJ',682,'','','','','','BS006141','NUMBER','','','Coop Other Years Adj','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','COOP_PRIOR_YEAR_ADJ',682,'','','','','','BS006141','NUMBER','','','Coop Prior Year Adj','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','COOP_CURRENT_YEAR_ADJ',682,'','','','','','BS006141','NUMBER','','','Coop Current Year Adj','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','COOP_PERIOD_ACCRUALS',682,'','','','','','BS006141','NUMBER','','','Coop Period Accruals','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_2141503_NAGSSW_V','COOP_PROJECTED_ACCRUALS',682,'','','','','','BS006141','NUMBER','','','Coop Projected Accruals','','','');
--Inserting View Components for XXEIS_2141503_NAGSSW_V
--Inserting View Component Joins for XXEIS_2141503_NAGSSW_V
END;
/
set scan on define on
prompt Creating Report Data for Projection Accrual - New Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Projection Accrual - New Report
xxeis.eis_rs_utility.delete_report_rows( 'Projection Accrual - New Report' );
--Inserting Report - Projection Accrual - New Report
xxeis.eis_rs_ins.r( 682,'Projection Accrual - New Report','','Projection Accrual - Running this report parallel to verify current fiscal data in production.','','','','BS006141','XXEIS_2141503_NAGSSW_V','Y','','select 
   mvid,
   vendor,
   lob,
   bu,
   purchases,
   other_years_adj,
   prior_year_adj,
   current_year_adj,
   current_period_accruals,
   projected_accruals,
   rebate_purchases,
   rebate_other_years_adj,
   rebate_prior_year_adj,
   rebate_current_year_adj,
   rebate_period_accruals,
   rebate_projected_accruals,
   coop_purchases,
   coop_other_years_adj,
   coop_prior_year_adj,
   coop_current_year_adj,
   coop_period_accruals,
   coop_projected_accruals
from table
(
apps.xxcus_ozf_get_projection_accr())
','BS006141','','N','ADHOC - SUMMARY ACCRUAL DETAILS','','EXCEL,','');
--Inserting Report Columns - Projection Accrual - New Report
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'REBATE_PRIOR_YEAR_ADJ','Rebate Prior Year Adj','','','~~~','default','','13','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'REBATE_CURRENT_YEAR_ADJ','Rebate Current Year Adj','','','~~~','default','','14','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'PURCHASES','Purchases','','','~~~','default','','5','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'OTHER_YEARS_ADJ','Other Years Adj','','','~~~','default','','6','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'CURRENT_PERIOD_ACCRUALS','Current Period Accruals','','','~~~','default','','9','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'MVID','MVID','','','','default','','2','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'COOP_PURCHASES','Coop Purchases','','','~~~','default','','17','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'VENDOR','Vendor','','','','default','','3','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'REBATE_PROJECTED_ACCRUALS','Rebate Projected Accruals','','','~~~','default','','16','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'LOB','LOB','','','','default','','1','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'REBATE_OTHER_YEARS_ADJ','Rebate Other Years Adj','','','~~~','default','','12','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'REBATE_PURCHASES','Rebate Purchases','','','~~~','default','','11','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'COOP_PRIOR_YEAR_ADJ','Coop Prior Year Adj','','','~~~','default','','18','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'PROJECTED_ACCRUALS','Projected Accruals','','','~~~','default','','10','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'COOP_CURRENT_YEAR_ADJ','Coop Current Year Adj','','','~~~','default','','19','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'CURRENT_YEAR_ADJ','Current Year Adj','','','~~~','default','','8','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'REBATE_PERIOD_ACCRUALS','Rebate Period Accruals','','','~~~','default','','15','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'COOP_OTHER_YEARS_ADJ','Coop Other Years Adj','','','~~~','default','','20','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'PRIOR_YEAR_ADJ','Prior Year Adj','','','~~~','default','','7','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'COOP_PERIOD_ACCRUALS','Coop Period Accruals','','','~~~','default','','21','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'COOP_PROJECTED_ACCRUALS','Coop Projected Accruals','','','~~~','default','','22','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
xxeis.eis_rs_ins.rc( 'Projection Accrual - New Report',682,'BU','BU','','','','default','','4','N','','','','','','','','BS006141','N','N','','XXEIS_2141503_NAGSSW_V','','');
--Inserting Report Parameters - Projection Accrual - New Report
xxeis.eis_rs_ins.rp( 'Projection Accrual - New Report',682,'Current Business Days','','','IN','','','NUMERIC','Y','Y','1','','N','CONSTANT','BS006141','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Projection Accrual - New Report',682,'Total Business Days','','','IN','','','NUMERIC','Y','Y','2','','N','CONSTANT','BS006141','Y','N','','','');
--Inserting Report Conditions - Projection Accrual - New Report
--Inserting Report Sorts - Projection Accrual - New Report
--Inserting Report Triggers - Projection Accrual - New Report
xxeis.eis_rs_ins.rt( 'Projection Accrual - New Report',682,'begin
xxeis_rebates_com_util_pkg.g_past_period := null;
xxeis_rebates_com_util_pkg.g_current_days := :Current Business Days;
xxeis_rebates_com_util_pkg.g_total_days := :Total Business Days;
end; 


','B','Y','BS006141');
--Inserting Report Templates - Projection Accrual - New Report
--Inserting Report Portals - Projection Accrual - New Report
--Inserting Report Dashboards - Projection Accrual - New Report
--Inserting Report Security - Projection Accrual - New Report
--Inserting Report Pivots - Projection Accrual - New Report
END;
/
set scan on define on
