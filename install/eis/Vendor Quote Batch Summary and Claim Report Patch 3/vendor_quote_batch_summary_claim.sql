--Report Name            : Vendor Quote Batch Summary and Claim Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_OM_VENDOR_QUOTE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_VENDOR_QUOTE_V
Create or replace View XXEIS.EIS_XXWC_OM_VENDOR_QUOTE_V
 AS 
SELECT NAME,
    salesperson_name,
    salespeson_number,
    master_account_name,
    master_account_number,
    job_name,
    job_number,
    vendor_number,
    vendor_name,
    oracle_quote_number,
    invoice_number,
    invoice_date,
    part_number,
    uom,
    description,
    bpa,
    po_cost po_cost,
    average_cost average_cost,
    special_cost special_cost,
    (po_cost - special_cost) unit_claim_value,
    (po_cost - special_cost) * qty rebate,
    gl_coding,
    gl_string,
    loc,
    created_by,
    customer_trx_id,
    order_number,
    line_number,
    creation_date,
    location,
    QTY,
    process_id
  FROM
    (SELECT
      /*+ index (QPL XXWC_QP_LIST_LINES_N2) */
      qll.start_date_active ,
      qlh.start_date_active ,
      qll.end_date_active,
      qlh.end_date_active,
      rct.trx_date,
      qlh. NAME,
      jrs.NAME salesperson_name,
      jrs.salesrep_number salespeson_number,
      hcs.account_name master_account_name,
      hcs.account_number master_account_number,
      hcsu.LOCATION job_name,
      hps.party_site_number job_number,
      pov.segment1 vendor_number,
      pov.vendor_name vendor_name,
      xxcpl.vendor_quote_number oracle_quote_number,
      rct.trx_number invoice_number,
      TRUNC(rct.trx_date) invoice_date,
      msi.concatenated_segments part_number,
      msi.primary_unit_of_measure uom,
      msi.description description,
      CASE
        WHEN upper (pov.vendor_name) LIKE 'SIMPSON%'
        THEN xxeis.eis_rs_xxwc_com_util_pkg.get_best_buy ( msi.inventory_item_id, msi.organization_id, pov.vendor_id)
        ELSE NULL
      END bpa,
      NVL ( xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_po_cost ( msi.inventory_item_id, msi.organization_id, ol.creation_date), msi.list_price_per_unit) po_cost,
      xxcpl.average_cost average_cost,
      xxcpl.special_cost special_cost,
      '400-900'
      || '-'
      || gcc.segment2 gl_coding,
      gcc.concatenated_segments gl_string,
      gcc.segment2 loc,
      ppf1.full_name created_by,
      rct.customer_trx_id customer_trx_id,
      oh.order_number order_number,
      ol.line_number line_number,
      oh.creation_date creation_date,
      mp.organization_code location,
      NVL(rctl.quantity_invoiced, rctl.quantity_credited) QTY,
      temp.process_id process_id
    FROM oe_order_headers oh,
      oe_order_lines ol,
      qp_qualifiers qq,
      qp_list_headers qlh,
      apps.xxwc_om_contract_pricing_hdr xxcph,
      apps.xxwc_om_contract_pricing_lines xxcpl,
      qp_list_lines qll,
      mtl_system_items_b_kfv msi,
      fnd_user fu,
      per_all_people_f ppf1,
      po_vendors pov,
      ra_salesreps jrs,
      ra_customer_trx rct,
      ra_customer_trx_lines rctl,
      ra_cust_trx_line_gl_dist rctlgl,
      gl_code_combinations_kfv gcc,
      hz_cust_accounts hcs,
      hz_parties hp,
      hz_cust_site_uses hcsu,
      hz_cust_acct_sites hcas,
      hz_party_sites hps,
      MTL_PARAMETERS MP,
      xxeis.XXWC_VENDOR_QUOTE_TEMP temp
    WHERE 1                         =1
    AND oh.header_id                = ol.header_id
    AND ol.flow_status_code         = 'CLOSED'
    AND qq.qualifier_context        = 'CUSTOMER'
    AND qq.comparison_operator_code = '='
    AND qq.active_flag              = 'Y'
    AND ( (qlh.attribute10          = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.ship_to_org_id)) )
    OR (qlh.attribute10             = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) )
    OR (qlh.attribute10             = ('CSP-NATIONAL')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) ) )
    AND qlh.list_header_id          = qq.list_header_id
    AND qlh.active_flag             = 'Y'
    AND xxcph.agreement_id          = TO_CHAR(qlh.attribute14)
      --AND xxcph.revision_number           = TO_CHAR(qlh.attribute15)
    AND xxcpl.agreement_id              = xxcph.agreement_id
    AND xxcpl.agreement_type            = 'VQN'
    AND qlh.list_header_id              = qll.list_header_id
    AND xxcpl.product_value             = msi.segment1
    AND xxcpl.agreement_line_id         = TO_CHAR(qll.attribute2)
    AND msi.inventory_item_id           = ol.inventory_item_id
    AND msi.organization_id             = ol.ship_from_org_id
    AND fu.user_id                      = oh.created_by
    AND fu.employee_id(+)               = ppf1.person_id
    AND pov.vendor_id                   = xxcpl.vendor_id
    AND oh.salesrep_id                  = jrs.salesrep_id(+)
    AND oh.org_id                       = jrs.org_id(+)
    AND rct.interface_header_context    = 'ORDER ENTRY'
    AND rct.interface_header_attribute1 = TO_CHAR(oh.order_number)
    AND rctl.interface_line_context     = 'ORDER ENTRY'
    AND rctl.interface_line_attribute6  = TO_CHAR (ol.line_id)
      --AND rctl.sales_order                = oh.order_number
    AND rctl.customer_trx_id        = rct.customer_trx_id
    AND rctl.line_type              = 'LINE'
    AND rctlgl.customer_trx_id      = rctl.customer_trx_id
    AND rctlgl.customer_trx_line_id = rctl.customer_trx_line_id
    AND rctlgl.account_class        = 'REV'
    AND gcc.code_combination_id     = rctlgl.code_combination_id
    AND hcs.cust_account_id         = rct.ship_to_customer_id
    AND hcs.party_id                = hp.party_id
    AND hcsu.site_use_id            = rct.ship_to_site_use_id
    AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
    AND hcas.party_site_id          = hps.party_site_id
    AND MP.ORGANIZATION_ID          = MSI.ORGANIZATION_ID
      --AND QLL.START_DATE_ACTIVE          <= NVL(QLL.END_DATE_ACTIVE,RCT.TRX_DATE)
    AND (TRUNC(NVL(QLL.START_DATE_ACTIVE,RCT.TRX_DATE))<=TRUNC(RCT.TRX_DATE)
    AND TRUNC(NVL(QLL.END_DATE_ACTIVE,RCT.TRX_DATE))   >= TRUNC(RCT.TRX_DATE))
      /*AND (TRUNC(QLL.END_DATE_ACTIVE)  >=TRUNC(RCT.TRX_DATE)
      OR QLL.END_DATE_ACTIVE           IS NULL)*/
    AND TEMP.HEADER_ID = OH.HEADER_ID
    )
/
set scan on define on
prompt Creating View Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_VENDOR_QUOTE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_VENDOR_QUOTE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Vendor Batch V','EXOVBV','','');
--Delete View Columns for EIS_XXWC_OM_VENDOR_QUOTE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_VENDOR_QUOTE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_VENDOR_QUOTE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LOC',660,'Loc','LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','GL_STRING',660,'Gl String','GL_STRING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl String','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','GL_CODING',660,'Gl Coding','GL_CODING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Coding','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','REBATE',660,'Rebate','REBATE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Rebate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','UNIT_CLAIM_VALUE',660,'Unit Claim Value','UNIT_CLAIM_VALUE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit Claim Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PO_COST',660,'Po Cost','PO_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Po Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','DESCRIPTION',660,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','UOM',660,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','JOB_NUMBER',660,'Job Number','JOB_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','JOB_NAME',660,'Job Name','JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','MASTER_ACCOUNT_NUMBER',660,'Master Account Number','MASTER_ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','MASTER_ACCOUNT_NAME',660,'Master Account Name','MASTER_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Account Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SALESPESON_NUMBER',660,'Salespeson Number','SALESPESON_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salespeson Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SALESPERSON_NAME',660,'Salesperson Name','SALESPERSON_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesperson Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SPECIAL_COST',660,'Special Cost','SPECIAL_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Special Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LOCATION',660,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','ORACLE_QUOTE_NUMBER',660,'Oracle Quote Number','ORACLE_QUOTE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Quote Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','BPA',660,' Bpa','BPA','','','','XXEIS_RS_ADMIN','NUMBER','','',' Bpa','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','NAME',660,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','CUSTOMER_TRX_ID',660,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_OM_VENDOR_QUOTE_V
--Inserting View Component Joins for EIS_XXWC_OM_VENDOR_QUOTE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT VENDOR_NAME 
    FROM PO_VENDORS POV','','OM Vendor Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct segment1 Vendor_Number from po_vendors','','OM Vendor Number LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_utility.delete_report_rows( 'Vendor Quote Batch Summary and Claim Report' );
--Inserting Report - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.r( 660,'Vendor Quote Batch Summary and Claim Report','','Provides order, credit customer and vendor details for all closed orders associated with a Vendor Quote.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_VENDOR_QUOTE_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'CREATED_BY','Created By','Created By','','','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'DESCRIPTION','Description','Description','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'JOB_NAME','Job Name','Job Name','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'JOB_NUMBER','Site Number','Job Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LOC','Loc','Loc','','','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NAME','Master Account Name','Master Account Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NUMBER','Master Account Number','Master Account Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'PART_NUMBER','Part Number','Part Number','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'REBATE','Rebate','Rebate','','~T~D~5','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SALESPERSON_NAME','Salesperson Name','Salesperson Name','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SALESPESON_NUMBER','Salespeson Number','Salespeson Number','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'UOM','Uom','Uom','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'GL_CODING','GL Coding','Gl Coding','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'PO_COST','Po Cost','Po Cost','','~T~D~5','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'UNIT_CLAIM_VALUE','Unit Claim Value','Unit Claim Value','','~T~D~5','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'GL_STRING','GL String','Gl String','','','default','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LOCATION','Location','Location','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'QTY','Qty','Qty','','~~~','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'ORACLE_QUOTE_NUMBER','Vendor Quote Number','Oracle Quote Number','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SPECIAL_COST','Special Cost','Special Cost','','~T~D~5','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'BPA','Best Buy',' Bpa','','~T~D~5','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'AVERAGE_COST','Average Cost','Average Cost','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','27','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
--Inserting Report Parameters - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Date Range From','Date Range From','INVOICE_DATE','>=','','','DATE','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Vendor Name','Vendor Name','VENDOR_NAME','IN','OM Vendor Name LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Vendor Number','Vendor Number','VENDOR_NUMBER','IN','OM Vendor Number LOV','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Date Range To','Date Range To','INVOICE_DATE','<=','','','DATE','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Bill To Customer','Bill To Customer','MASTER_ACCOUNT_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NAME','IN',':Vendor Name','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NUMBER','IN',':Vendor Number','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NAME','IN',':Bill To Customer','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'PROCESS_ID','IN',':SYSTEM.PROCESS_ID','','','Y','5','N','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_DATE','>=',':Date Range From','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_DATE','<=',':Date Range To','','','Y','2','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Vendor Quote Batch Summary and Claim Report
--Inserting Report Triggers - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rt( 'Vendor Quote Batch Summary and Claim Report',660,'begin
XXWC_VENDOR_QUOTE_TMP_PKG.GET_HEADER_ID(P_PROCESS_ID=>:SYSTEM.PROCESS_ID,
P_INV_START_DATE=>:Date Range From,
P_INV_END_DATE=>:Date Range To);
END;
','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.R_Tem( 'Vendor Quote Batch Summary and Claim Report','Vendor Quote Batch Summary and Claim Report','Seeded Template for Vendor Quote Batch Summary and Claim Report','','','','','','','','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_Tem( 'Vendor Quote Batch Summary and Claim Report','Vendor Quote Report','Seeded template for Vendor Quote Report','','','','','','','','','','','Vendor Quote Report.rtf','XXEIS_RS_ADMIN');
--Inserting Report Portals - Vendor Quote Batch Summary and Claim Report
--Inserting Report Dashboards - Vendor Quote Batch Summary and Claim Report
--Inserting Report Security - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50870',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50871',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50869',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','20005','','50900',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rpivot( 'Vendor Quote Batch Summary and Claim Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
