--Report Name            : Intercompany AR Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_GL_INTERCOMP_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_GL_INTERCOMP_V
Create or replace View XXEIS.EIS_XXWC_GL_INTERCOMP_V
(CUSTOMER_NUMBER,CUSTOMER_NAME,GL_DATE,INVOICE_NUMBER,INVOICE_DATE,DUE_DATE,INVOICE_AMOUNT,AGE,TOTAL,BUCKET_CURRENT,BUCKET_1_TO_30,BUCKET_31_TO_60,BUCKET_61_TO_90,BUCKET_91_TO_180,BUCKET_181_TO_360,BUCKET_361_DAYS_AND_ABOVE,PROFILE_CLASS,PERIOD_NAME,APPLICATION_ID,SET_OF_BOOKS_ID,TERRITORY_ID,CUST_ACCOUNT_ID,PARTY_ID,CUSTOMER_TRX_ID,PAYMENT_SCHEDULE_ID,HCA#PARTY_TYPE,HCA#VNDR_CODE_AND_FRULOC,HCA#BRANCH_DESCRIPTION,HCA#CUSTOMER_SOURCE,HCA#LEGAL_COLLECTION,HCA#COLLECTION_AGENCY,HCA#YES#NOTE_LINE_#5,HCA#YES#NOTE_LINE_#1,HCA#YES#NOTE_LINE_#2,HCA#YES#NOTE_LINE_#3,HCA#YES#NOTE_LINE_#4,PARTY#PARTY_TYPE,PARTY#GVID_ID) AS 
SELECT hca.account_number customer_number,
    party.party_name customer_name,
    ps.gl_date,
    ct.trx_number invoice_number,
    ct.trx_date invoice_date,
    arpt_sql_func_util.get_first_real_due_date(ct.customer_trx_id, ct.term_id, ct.trx_date) due_date,
    ps.acctd_amount_due_remaining invoice_amount,
    CEIL (XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.GET_ASOF_DATE                                                                        - PS.DUE_DATE) Age,
    xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (ps.payment_schedule_id, ps.due_date, NULL, NULL, (ps.amount_due_original)*NVL(ps.exchange_rate,1), ps.amount_applied, ps.amount_credited, ps.amount_adjusted, ps.CLASS
    --null
    ) Total,
    xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (ps.payment_schedule_id, ps.due_date, NULL, 0, (ps.amount_due_original)*NVL(ps.exchange_rate,1), ps.amount_applied, ps.amount_credited, ps.amount_adjusted, ps.CLASS
    --null
    ) bucket_current,
    xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (ps.payment_schedule_id, ps.due_date, 1, 30, (ps.amount_due_original)*NVL(ps.exchange_rate,1), ps.amount_applied, ps.amount_credited, ps.amount_adjusted, ps.CLASS
    --null
    ) bucket_1_to_30,
    xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (ps.payment_schedule_id, ps.due_date, 31, 60, (ps.amount_due_original)*NVL(ps.exchange_rate,1), ps.amount_applied, ps.amount_credited, ps.amount_adjusted, ps.CLASS
    --null
    ) bucket_31_to_60,
    xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (ps.payment_schedule_id, ps.due_date, 61, 90, (ps.amount_due_original)*NVL(ps.exchange_rate,1), ps.amount_applied, ps.amount_credited, ps.amount_adjusted, ps.CLASS
    --null
    ) bucket_61_to_90,
    xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (ps.payment_schedule_id, ps.due_date, 91, 180, (ps.amount_due_original)*NVL(ps.exchange_rate,1), ps.amount_applied, ps.amount_credited, ps.amount_adjusted, ps.CLASS
    --null
    ) bucket_91_to_180,
    xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (ps.payment_schedule_id, ps.due_date, 181, 360, (ps.amount_due_original)*NVL(ps.exchange_rate,1), ps.amount_applied, ps.amount_credited, ps.amount_adjusted, ps.CLASS
    --null
    ) bucket_181_to_360,
    xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining (ps.payment_schedule_id, ps.due_date, 361, 9999999, (ps.amount_due_original)*NVL(ps.exchange_rate,1), ps.amount_applied, ps.amount_credited, ps.amount_adjusted, ps.CLASS
    --null
    ) bucket_361_days_and_above,
    'Intercompany Customers' profile_class,
    gps.period_name,
    gps.application_id,
    gps.set_of_books_id,
    rt.territory_id,
    hca.cust_account_id,
    party.party_id,
    ct.customer_trx_id,
    ps.payment_schedule_id
    --descr#flexfield#start
    
 ,xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',HCA.ATTRIBUTE1,'I') HCA#Party_Type
 ,HCA.ATTRIBUTE2 HCA#Vndr_Code_and_FRULOC
 ,HCA.ATTRIBUTE3 HCA#Branch_Description
 ,HCA.ATTRIBUTE4 HCA#Customer_Source
 ,xxeis.eis_rs_dff.decode_valueset( 'XXWC_LEGAL_COLLECTION',HCA.ATTRIBUTE5,'F') HCA#Legal_Collection
 ,xxeis.eis_rs_dff.decode_valueset( 'XXWC_LEGAL_AGENCY',HCA.ATTRIBUTE6,'F') HCA#Collection_Agency
 ,DECODE(HCA.ATTRIBUTE_CATEGORY ,'Yes',HCA.ATTRIBUTE16, NULL) HCA#Yes#Note_Line_#5
 ,DECODE(HCA.ATTRIBUTE_CATEGORY ,'Yes',HCA.ATTRIBUTE17, NULL) HCA#Yes#Note_Line_#1
 ,DECODE(HCA.ATTRIBUTE_CATEGORY ,'Yes',HCA.ATTRIBUTE18, NULL) HCA#Yes#Note_Line_#2
 ,DECODE(HCA.ATTRIBUTE_CATEGORY ,'Yes',HCA.ATTRIBUTE19, NULL) HCA#Yes#Note_Line_#3
 ,DECODE(HCA.ATTRIBUTE_CATEGORY ,'Yes',HCA.ATTRIBUTE20, NULL) HCA#Yes#Note_Line_#4
 ,xxeis.eis_rs_dff.decode_valueset( 'XXCUS_PARTY_TYPE',PARTY.ATTRIBUTE1,'I') PARTY#Party_Type
 ,PARTY.ATTRIBUTE2 PARTY#GVID_ID
--descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM hz_cust_accounts hca,
    hz_parties party,
    ra_customer_trx ct,
    ra_territories rt,
    ar_payment_schedules ps,
    hr_operating_units hou,
    gl_period_statuses gps
  WHERE 1                    =1
  AND hca.party_id           = party.party_id
  AND ct.bill_to_customer_id = hca.cust_account_id
  AND ct.territory_id        = rt.territory_id(+)
  AND ps.customer_trx_id     = ct.customer_trx_id
  AND ct.org_id              = hou.organization_id
  AND gps.application_id     = 101
  AND gps.set_of_books_id    = hou.set_of_books_id 
  AND EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc
    WHERE 1                 =1
    AND hca.cust_account_id =hcp.cust_account_id
    AND hcp.profile_class_id=hcpc.profile_class_id
    AND hcpc.name           = 'Intercompany Customers'
    )
  AND TRUNC(ps.gl_date) BETWEEN gps.start_date AND gps.end_date
  AND TRUNC (PS.GL_DATE) <= XXEIS.EIS_RS_AR_FIN_COM_UTIL_PKG.GET_ASOF_DATE/
set scan on define on
prompt Creating View Data for Intercompany AR Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_GL_INTERCOMP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_GL_INTERCOMP_V',101,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Ar Intercomp V','EXGIV');
--Delete View Columns for EIS_XXWC_GL_INTERCOMP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_GL_INTERCOMP_V',101,FALSE);
--Inserting View Columns for EIS_XXWC_GL_INTERCOMP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','BUCKET_361_DAYS_AND_ABOVE',101,'Bucket 361 Days And Above','BUCKET_361_DAYS_AND_ABOVE','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 361 Days And Above');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','BUCKET_181_TO_360',101,'Bucket 181 To 360','BUCKET_181_TO_360','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 181 To 360');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','BUCKET_91_TO_180',101,'Bucket 91 To 180','BUCKET_91_TO_180','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 91 To 180');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','BUCKET_61_TO_90',101,'Bucket 61 To 90','BUCKET_61_TO_90','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 61 To 90');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','BUCKET_31_TO_60',101,'Bucket 31 To 60','BUCKET_31_TO_60','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 31 To 60');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','BUCKET_1_TO_30',101,'Bucket 1 To 30','BUCKET_1_TO_30','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Bucket 1 To 30');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','BUCKET_CURRENT',101,'Bucket Current','BUCKET_CURRENT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Bucket Current');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','TOTAL',101,'Total','TOTAL','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Total');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','AGE',101,'Age','AGE','','','','XXEIS_RS_ADMIN','NUMBER','','','Age');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','INVOICE_AMOUNT',101,'Invoice Amount','INVOICE_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','DUE_DATE',101,'Due Date','DUE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Due Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','INVOICE_DATE',101,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','INVOICE_NUMBER',101,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','CUSTOMER_NAME',101,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','CUSTOMER_NUMBER',101,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','PROFILE_CLASS',101,'Profile Class','PROFILE_CLASS','','','','XXEIS_RS_ADMIN','CHAR','','','Profile Class');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','PAYMENT_SCHEDULE_ID',101,'Payment Schedule Id','PAYMENT_SCHEDULE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Schedule Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','CUSTOMER_TRX_ID',101,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','PARTY_ID',101,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','CUST_ACCOUNT_ID',101,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','TERRITORY_ID',101,'Territory Id','TERRITORY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Territory Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','GL_DATE',101,'Gl Date','GL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Gl Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','APPLICATION_ID',101,'Application Id','APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Application Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','SET_OF_BOOKS_ID',101,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#PARTY_TYPE',101,'Descriptive flexfield: Customer Information Column Name: Party Type','HCA#Party_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE1','Hca#Party Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#VNDR_CODE_AND_FRULOC',101,'Descriptive flexfield: Customer Information Column Name: Vndr Code and FRULOC','HCA#Vndr_Code_and_FRULOC','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE2','Hca#Vndr Code And Fruloc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#BRANCH_DESCRIPTION',101,'Descriptive flexfield: Customer Information Column Name: Branch Description','HCA#Branch_Description','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE3','Hca#Branch Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#CUSTOMER_SOURCE',101,'Descriptive flexfield: Customer Information Column Name: Customer Source','HCA#Customer_Source','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE4','Hca#Customer Source');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#LEGAL_COLLECTION',101,'Descriptive flexfield: Customer Information Column Name: Legal Collection','HCA#Legal_Collection','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE5','Hca#Legal Collection');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#COLLECTION_AGENCY',101,'Descriptive flexfield: Customer Information Column Name: Collection Agency','HCA#Collection_Agency','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE6','Hca#Collection Agency');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#YES#NOTE_LINE_#5',101,'Descriptive flexfield: Customer Information Column Name: Note Line #5 Context: Yes','HCA#Yes#Note_Line_#5','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE16','Hca#Yes#Note Line #5');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#YES#NOTE_LINE_#1',101,'Descriptive flexfield: Customer Information Column Name: Note Line #1 Context: Yes','HCA#Yes#Note_Line_#1','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE17','Hca#Yes#Note Line #1');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#YES#NOTE_LINE_#2',101,'Descriptive flexfield: Customer Information Column Name: Note Line #2 Context: Yes','HCA#Yes#Note_Line_#2','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE18','Hca#Yes#Note Line #2');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#YES#NOTE_LINE_#3',101,'Descriptive flexfield: Customer Information Column Name: Note Line #3 Context: Yes','HCA#Yes#Note_Line_#3','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE19','Hca#Yes#Note Line #3');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','HCA#YES#NOTE_LINE_#4',101,'Descriptive flexfield: Customer Information Column Name: Note Line #4 Context: Yes','HCA#Yes#Note_Line_#4','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_CUST_ACCOUNTS','ATTRIBUTE20','Hca#Yes#Note Line #4');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','PARTY#PARTY_TYPE',101,'Descriptive flexfield: Party Information Column Name: Party Type','PARTY#Party_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Party#Party Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_GL_INTERCOMP_V','PARTY#GVID_ID',101,'Descriptive flexfield: Party Information Column Name: GVID_ID','PARTY#GVID_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','HZ_PARTIES','ATTRIBUTE2','Party#Gvid Id');
--Inserting View Components for EIS_XXWC_GL_INTERCOMP_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_GL_INTERCOMP_V','HZ_CUST_ACCOUNTS',101,'HZ_CUST_ACCOUNTS','HCA','HCA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_GL_INTERCOMP_V','HZ_PARTIES',101,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_GL_INTERCOMP_V','RA_CUSTOMER_TRX',101,'RA_CUSTOMER_TRX_ALL','CT','CT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Header-Level Information About Invoices, Debit Mem','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_GL_INTERCOMP_V','RA_TERRITORIES',101,'RA_TERRITORIES','RT','RT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Territory Information','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_GL_INTERCOMP_V','AR_PAYMENT_SCHEDULES',101,'AR_PAYMENT_SCHEDULES_ALL','PS','PS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','All Transactions Except Adjustments And Miscellane','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_GL_INTERCOMP_V','GL_PERIOD_STATUSES',101,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','');
--Inserting View Component Joins for EIS_XXWC_GL_INTERCOMP_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_GL_INTERCOMP_V','HZ_CUST_ACCOUNTS','HCA',101,'EXGIV.CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_GL_INTERCOMP_V','HZ_PARTIES','PARTY',101,'EXGIV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_GL_INTERCOMP_V','RA_CUSTOMER_TRX','CT',101,'EXGIV.CUSTOMER_TRX_ID','=','CT.CUSTOMER_TRX_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_GL_INTERCOMP_V','RA_TERRITORIES','RT',101,'EXGIV.TERRITORY_ID','=','RT.TERRITORY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_GL_INTERCOMP_V','AR_PAYMENT_SCHEDULES','PS',101,'EXGIV.PAYMENT_SCHEDULE_ID','=','PS.PAYMENT_SCHEDULE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_GL_INTERCOMP_V','GL_PERIOD_STATUSES','GPS',101,'EXGIV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_GL_INTERCOMP_V','GL_PERIOD_STATUSES','GPS',101,'EXGIV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_GL_INTERCOMP_V','GL_PERIOD_STATUSES','GPS',101,'EXGIV.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','Y','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Intercompany AR Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Intercompany AR Report
xxeis.eis_rs_ins.lov( 101,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','null','EIS_GL_PERIOD_LOV','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 101,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','GL CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Intercompany AR Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Intercompany AR Report
xxeis.eis_rs_utility.delete_report_rows( 'Intercompany AR Report' );
--Inserting Report - Intercompany AR Report
xxeis.eis_rs_ins.r( 101,'Intercompany AR Report','','Provide the total listing of all intercompany receivables with other LOBs (Customer Number, Customer Name, LOB, CR Territory, Invoice number, Invoice Date, Due Date, Invoice Amount, Age (by Days), Total, Current, 1-30 Past Due, 31-60 Past Due, 61-90 Past Due, 91-180 Past Due, 181-360 Past Due, 360+ Past Due)','','','','XXEIS_RS_ADMIN','EIS_XXWC_GL_INTERCOMP_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Intercompany AR Report
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'AGE','Age','Age','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'BUCKET_181_TO_360','181-360 Past Due','Bucket 181 To 360','','','','','15','N','','DATA_FIELD','','','','7','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'BUCKET_1_TO_30','1-30 Past Due','Bucket 1 To 30','','','','','11','N','','DATA_FIELD','','SUM','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'BUCKET_31_TO_60','31-60 Past Due','Bucket 31 To 60','','','','','12','N','','DATA_FIELD','','SUM','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'BUCKET_361_DAYS_AND_ABOVE','360+ Past Due','Bucket 361 Days And Above','','','','','16','N','','DATA_FIELD','','SUM','','8','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'BUCKET_61_TO_90','61-90 Past Due','Bucket 61 To 90','','','','','13','N','','DATA_FIELD','','SUM','','5','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'BUCKET_91_TO_180','91-180 Past Due','Bucket 91 To 180','','','','','14','N','','DATA_FIELD','','SUM','','6','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'BUCKET_CURRENT','Current','Bucket Current','','','','','10','N','','DATA_FIELD','','SUM','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','2','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','1','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'DUE_DATE','Due Date','Due Date','','','','','6','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'INVOICE_AMOUNT','Invoice Amount','Invoice Amount','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','5','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','4','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'TOTAL','Total','Total','','','','','9','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
xxeis.eis_rs_ins.rc( 'Intercompany AR Report',101,'PROFILE_CLASS','Profile Class','Profile Class','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_GL_INTERCOMP_V','','');
--Inserting Report Parameters - Intercompany AR Report
xxeis.eis_rs_ins.rp( 'Intercompany AR Report',101,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','GL CUSTOMER NUMBER','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Intercompany AR Report',101,'Fiscal Month','Fiscal Month','PERIOD_NAME','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Intercompany AR Report
xxeis.eis_rs_ins.rcn( 'Intercompany AR Report',101,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Intercompany AR Report',101,'PERIOD_NAME','IN',':Fiscal Month','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Intercompany AR Report
xxeis.eis_rs_ins.rs( 'Intercompany AR Report',101,'CUSTOMER_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Intercompany AR Report
xxeis.eis_rs_ins.rt( 'Intercompany AR Report',101,'Begin

 xxeis.eis_rs_ar_fin_com_util_pkg.set_asof_date(to_date(sysdate,''dd-mon-yyyy''));

End;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Intercompany AR Report
--Inserting Report Portals - Intercompany AR Report
--Inserting Report Dashboards - Intercompany AR Report
--Inserting Report Security - Intercompany AR Report
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50831',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50945',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50946',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50947',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50617',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50618',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50702',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','20005','','50880',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','','LB048272','',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','','10012196','',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','','MM050208','',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','','SO004816','',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','20434',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50721',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50662',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50720',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50723',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50801',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50665',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50722',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50780',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50667',101,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Intercompany AR Report','101','','50668',101,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
