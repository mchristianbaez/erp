--Report Name            : Contract Pricing Report - Internal Test
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Contract Pricing Report - Internal Test
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_CONT_MODI_PRI_TEST_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_CONT_MODI_PRI_TEST_V',660,'','','','','PK059658','XXEIS','Eis Xxwc Cont Modi Pricing V','EXCMPV','','');
--Delete View Columns for EIS_XXWC_CONT_MODI_PRI_TEST_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_CONT_MODI_PRI_TEST_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_CONT_MODI_PRI_TEST_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CURRENT_LOCATION_COST',660,'Current Location Cost','CURRENT_LOCATION_COST','','','','PK059658','NUMBER','','','Current Location Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','ACTUAL_GROSS_MARGIN',660,'Actual Gross Margin','ACTUAL_GROSS_MARGIN','','','','PK059658','NUMBER','','','Actual Gross Margin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','ACTUAL_SALES',660,'Actual Sales','ACTUAL_SALES','','','','PK059658','NUMBER','','','Actual Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','UNITS_SOLD',660,'Units Sold','UNITS_SOLD','','','','PK059658','NUMBER','','','Units Sold','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CONTRACT_PRICE',660,'Contract Price','CONTRACT_PRICE','','','','PK059658','NUMBER','','','Contract Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','APP',660,'App','APP','','','','PK059658','VARCHAR2','','','App','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','LIST_PRICE',660,'List Price','LIST_PRICE','','','','PK059658','NUMBER','','','List Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','GM',660,'Gm','GM','','','','PK059658','NUMBER','','','Gm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CAT_CLASS',660,'Cat Class','CAT_CLASS','','','','PK059658','VARCHAR2','','','Cat Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','DESCRIPTION',660,'Description','DESCRIPTION','','','','PK059658','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','PRODUCT',660,'Product','PRODUCT','','','','PK059658','VARCHAR2','','','Product','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','PK059658','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','MODIFIER_NAME',660,'Modifier Name','MODIFIER_NAME','','','','PK059658','VARCHAR2','','','Modifier Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','VALUE1',660,'Value1','VALUE1','','','','PK059658','NUMBER','','','Value1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CAT',660,'Cat','CAT','','','','PK059658','VARCHAR2','','','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CAT_CLASS_DESC',660,'Cat Class Desc','CAT_CLASS_DESC','','','','PK059658','VARCHAR2','','','Cat Class Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CAT_DESC',660,'Cat Desc','CAT_DESC','','','','PK059658','VARCHAR2','','','Cat Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CONTRACT_TYPE',660,'Contract Type','CONTRACT_TYPE','','','','PK059658','VARCHAR2','','','Contract Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','SALESREP',660,'Salesrep','SALESREP','','','','PK059658','VARCHAR2','','','Salesrep','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','ACCOUNT_NUMBER',660,'Account Number','ACCOUNT_NUMBER','','','','PK059658','VARCHAR2','','','Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','LOCATION',660,'Location','LOCATION','','','','PK059658','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','PARTY_SITE_NUMBER',660,'Party Site Number','PARTY_SITE_NUMBER','','','','PK059658','VARCHAR2','','','Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','PRICE_TYPE',660,'Price Type','PRICE_TYPE','','','','PK059658','VARCHAR2','','','Price Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','VERSION_NO',660,'Version No','VERSION_NO','','','','PK059658','VARCHAR2','','','Version No','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','NEW_PRICE_OR_NEW_GM_DISC',660,'New Price Or New Gm Disc','NEW_PRICE_OR_NEW_GM_DISC','','','','PK059658','NUMBER','','','New Price Or New Gm Disc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','QUALIFIER_ATTRIBUTE',660,'Qualifier Attribute','QUALIFIER_ATTRIBUTE','','','','PK059658','VARCHAR2','','','Qualifier Attribute','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','TYPE',660,'Type','TYPE','','','','PK059658','VARCHAR2','','','Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','UNIT_COST',660,'Unit Cost','UNIT_COST','','','','PK059658','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','PK059658','NUMBER','','','Process Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','ARITHMETIC_OPERATOR',660,'Arithmetic Operator','ARITHMETIC_OPERATOR','','','','PK059658','VARCHAR2','','','Arithmetic Operator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CATEGORY_ID',660,'Category Id','CATEGORY_ID','','','','PK059658','NUMBER','','','Category Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','PK059658','VARCHAR2','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','PK059658','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','FORMULA_NAME',660,'Formula Name','FORMULA_NAME','','','','PK059658','VARCHAR2','','','Formula Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','PK059658','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','LIST_HEADER_ID',660,'List Header Id','LIST_HEADER_ID','','','','PK059658','NUMBER','','','List Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','OPERAND',660,'Operand','OPERAND','','','','PK059658','NUMBER','','','Operand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','PRICE_BY_CAT',660,'Price By Cat','PRICE_BY_CAT','','','','PK059658','VARCHAR2','','','Price By Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','PRODUCT_ATTRIBUTE_TYPE',660,'Product Attribute Type','PRODUCT_ATTRIBUTE_TYPE','','','','PK059658','VARCHAR2','','','Product Attribute Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','TRANSACTION_CODE',660,'Transaction Code','TRANSACTION_CODE','','','','PK059658','VARCHAR2','','','Transaction Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','PERSON_ID',660,'Person Id','PERSON_ID','','','','PK059658','NUMBER','','','Person Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_PRI_TEST_V','COMMON_OUTPUT_ID',660,'Common Output Id','COMMON_OUTPUT_ID','','','','PK059658','NUMBER','','','Common Output Id','','','');
--Inserting View Components for EIS_XXWC_CONT_MODI_PRI_TEST_V
--Inserting View Component Joins for EIS_XXWC_CONT_MODI_PRI_TEST_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Contract Pricing Report - Internal Test
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Contract Pricing Report - Internal Test
xxeis.eis_rs_ins.lov( 660,'select distinct organization_code org_code,organization_name name,organization_id from org_organization_definitions','','Organization Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ''Include'' Inc_Excl FROM dual
union
SELECT ''Exclude'' Inc_Excl FROM dual
','','Include_Exclude Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ''Include'' Inc_Excl FROM dual
union
SELECT ''Exclude'' Inc_Excl FROM dual
','','Include_Exclude Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ''Include'' Inc_Excl FROM dual
union
SELECT ''Exclude'' Inc_Excl FROM dual
','','Include_Exclude Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct jrse.source_name
from  jtf_rs_salesreps jrs ,
  jtf_rs_resource_extns jrse
 where  jrs.resource_id                    = jrse.resource_id','','District Manager Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct jrse.source_name SalesRep
from  jtf_rs_salesreps jrs ,
 jtf_rs_resource_extns jrse
 where jrs.resource_id  = jrse.resource_id','','XXWC Regional Manager Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct jrse.source_name  from
jtf_rs_salesreps jrs,
jtf_rs_resource_extns jrse
where    jrs.resource_id                    = jrse.resource_id','','XXWC Sales Manager Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT NAME  MODIFIER_NAME , DESCRIPTION MODIFIER_DESCRIPTION
  FROM QP_LIST_HEADERS QH
 WHERE QH.attribute10   = ''Contract Pricing''','','XXWC CSP Modifer Name','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','PK059658',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','PK059658',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Contract Pricing Report - Internal Test
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Contract Pricing Report - Internal Test
xxeis.eis_rs_utility.delete_report_rows( 'Contract Pricing Report - Internal Test' );
--Inserting Report - Contract Pricing Report - Internal Test
xxeis.eis_rs_ins.r( 660,'Contract Pricing Report - Internal Test','','','','','','PK059658','EIS_XXWC_CONT_MODI_PRI_TEST_V','Y','','','PK059658','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Contract Pricing Report - Internal Test
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'ACTUAL_SALES','ACTUAL SALES','Actual Sales','','~T~D~2','default','','22','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'PRODUCT','ITEM','Product','','','default','','14','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'UNITS_SOLD','UNITS SOLD','Units Sold','','~T~D~2','default','','21','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'VALUE1','VALUE','Value1','','~T~D~2','default','','19','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'APP','MODIFIER TYPE','App','','','default','','18','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'CAT_CLASS','CAT CLASS','Cat Class','','','default','','12','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'CONTRACT_PRICE','CONTRACT SELLING PRICE','Contract Price','','~T~D~2','default','','20','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'CURRENT_LOCATION_COST','CURRENT LOC COST','Current Location Cost','','~T~D~2','default','','25','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'CUSTOMER_NAME','CUSTOMER NAME','Customer Name','','','default','','1','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'DESCRIPTION','ITEM DESCRIPTION','Description','','','default','','15','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'GM','MIN GM','Gm','','~T~D~2','default','','16','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'LIST_PRICE','LIST PRICE','List Price','','~T~D~2','default','','17','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'MODIFIER_NAME','MODIFIER NAME','Modifier Name','','','default','','7','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'CAT','CAT','Cat','','','default','','10','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'CAT_CLASS_DESC','CAT CLASS DESCRIPTION','Cat Class Desc','','','default','','13','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'CAT_DESC','CATEGORY DESCRIPTION','Cat Desc','','','default','','11','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'CONTRACT_TYPE','AGREEMENT TYPE','Contract Type','','','default','','9','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'SALESREP','SALESPERSON NAME','Salesrep','','','default','','8','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'ACTUAL_GROSS_MARGIN','ACTUAL GM','Actual Gross Margin','NUMBER','~T~D~2','default','','24','Y','','','','','','','case when excmpv.INVENTORY_ITEM_ID = 0 then 0 when NVL(EXCMPV.UNITS_SOLD,0) =0 THEN 0 WHEN NVL((EXCMPV.ACTUAL_SALES/EXCMPV.UNITS_SOLD),0) =0 THEN 0 ELSE ((((EXCMPV.ACTUAL_SALES/EXCMPV.UNITS_SOLD)-UNIT_COST)/(EXCMPV.ACTUAL_SALES/EXCMPV.UNITS_SOLD))*100) END','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'LOC_COST_GM','LOC COST GM','Actual Gross Margin','NUMBER','~T~D~2','default','','26','Y','','','','','','','case when EXCMPV.CONTRACT_PRICE>0 then ((EXCMPV.CONTRACT_PRICE-EXCMPV.CURRENT_LOCATION_COST)/(EXCMPV.CONTRACT_PRICE))*100 else 0 end','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'ACCOUNT_NUMBER','MASTER ACCOUNT NUMBER','Account Number','','','default','','2','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'LOCATION','LOCATION NAME','Location','','','default','','6','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'PARTY_SITE_NUMBER','SITE NUMBER','Party Site Number','','','default','','5','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'PRICE_TYPE','PRICE TYPE','Price Type','','','default','','4','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Report - Internal Test',660,'VERSION_NO','AGREEMENT NUMBER','Version No','','','default','','3','N','','','','','','','','PK059658','N','N','','EIS_XXWC_CONT_MODI_PRI_TEST_V','','');
--Inserting Report Parameters - Contract Pricing Report - Internal Test
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'Start Date','Start Date','','IN','','','DATE','Y','Y','1','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'End Date','End Date','','IN','','','DATE','Y','Y','2','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'Cat/Class Priced Lines','Category Priced Items','','IN','Include_Exclude Lov','','VARCHAR2','Y','Y','4','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'RVP','RVP','','IN','XXWC Regional Manager Lov','','VARCHAR2','N','Y','5','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'DM','DM','','IN','District Manager Lov','','VARCHAR2','N','Y','6','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'SalesPerson','SalesPerson','','IN','XXWC Sales Manager Lov','','VARCHAR2','N','Y','7','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'Master Accounts','Master Accounts','','IN','Include_Exclude Lov','','VARCHAR2','Y','Y','8','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'Job Accounts','Job Accounts','','IN','Include_Exclude Lov','','VARCHAR2','Y','Y','9','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'Current Gross Margin Cost Loc','Current Gross Margin Cost Loc','','IN','Organization Lov','','VARCHAR2','Y','Y','3','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'Modifier Name','Modifier Name','','IN','XXWC CSP Modifer Name','','VARCHAR2','N','Y','12','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'Customer Number','Customer Number','','IN','OM CUSTOMER NUMBER','','NUMERIC','N','Y','11','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Contract Pricing Report - Internal Test',660,'Customer Name','Customer Name','','IN','OM CUSTOMER NAME','','NUMERIC','N','Y','10','','N','CONSTANT','PK059658','Y','N','','','');
--Inserting Report Conditions - Contract Pricing Report - Internal Test
xxeis.eis_rs_ins.rcn( 'Contract Pricing Report - Internal Test',660,'','','','','AND PROCESS_ID   = :SYSTEM.PROCESS_ID','Y','1','','PK059658');
--Inserting Report Sorts - Contract Pricing Report - Internal Test
xxeis.eis_rs_ins.rs( 'Contract Pricing Report - Internal Test',660,'CUSTOMER_NAME','ASC','PK059658','1','');
xxeis.eis_rs_ins.rs( 'Contract Pricing Report - Internal Test',660,'MODIFIER_NAME','ASC','PK059658','2','');
--Inserting Report Triggers - Contract Pricing Report - Internal Test
xxeis.eis_rs_ins.rt( 'Contract Pricing Report - Internal Test',660,'begin
xxeis.EIS_XXOM_XXWC_CONT_PRIC_PKG.CONTRACT_PRICING_MODIFIER  
                                    (
                                      P_PROCESS_ID           =>:SYSTEM.PROCESS_ID,
                                      P_START_DATE            =>:Start Date,
                                      P_END_DATE                =>:End Date,
                                      P_Current_Loc             =>  :Current Gross Margin Cost Loc,
                                      P_CatClass                   => NULL,
                                      P_CAT_PRICED_ITEMS  =>:Cat/Class Priced Lines,
                                      P_RVP                          =>:RVP,
                                      P_DM                           =>:DM,
                                      P_SALESPERSON           =>:SalesPerson,
                                      P_JOB_ACCOUNT           =>:Job Accounts,
                                      P_MASTER_ACCOUNT    =>:Master Accounts,
                                      P_CUST_ID     => :Customer Name,
                                      P_CUSTOMER_ID => :Customer Number,
                                      P_MODIFIER_NAME      => :Modifier Name
                                   );
end;','B','Y','PK059658');
--Inserting Report Templates - Contract Pricing Report - Internal Test
--Inserting Report Portals - Contract Pricing Report - Internal Test
--Inserting Report Dashboards - Contract Pricing Report - Internal Test
--Inserting Report Security - Contract Pricing Report - Internal Test
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','660','','51044',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','660','','51045',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','660','','50901',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','660','','51025',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','661','','50891',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','660','','50886',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','660','','50859',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','660','','50858',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','660','','50857',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','660','','50860',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','','BS006141','',660,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Report - Internal Test','','KP012542','',660,'PK059658','','');
--Inserting Report Pivots - Contract Pricing Report - Internal Test
END;
/
set scan on define on
