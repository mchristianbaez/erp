--Report Name            : HDS AP_IExpense_Trxns_Details Tax Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
/*prompt Creating View Data for HDS AP_IExpense_Trxns_Details Tax Report
set scan off define off*/
DECLARE
BEGIN 
--Inserting View XXWC_EIS_TAX_PORTION_VW
xxeis.eis_rs_ins.v( 'XXWC_EIS_TAX_PORTION_VW',91000,'','','','','ID020048','XXEIS','Xxwc Eis Tax Portion Vw','XETPV','','');
--Delete View Columns for XXWC_EIS_TAX_PORTION_VW
xxeis.eis_rs_utility.delete_view_rows('XXWC_EIS_TAX_PORTION_VW',91000,FALSE);
--Inserting View Columns for XXWC_EIS_TAX_PORTION_VW
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ORG_ID',91000,'Org Id','ORG_ID','','','','ID020048','NUMBER','','','Org Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CC_TRNS_CATEGORY',91000,'Cc Trns Category','CC_TRNS_CATEGORY','','','','ID020048','VARCHAR2','','','Cc Trns Category','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ORACLE_SEGMENT7',91000,'Oracle Segment7','ORACLE_SEGMENT7','','','','ID020048','VARCHAR2','','','Oracle Segment7','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ORACLE_SEGMENT6',91000,'Oracle Segment6','ORACLE_SEGMENT6','','','','ID020048','VARCHAR2','','','Oracle Segment6','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','PROJECT_NAME',91000,'Project Name','PROJECT_NAME','','','','ID020048','VARCHAR2','','','Project Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','PROJECT_NUMBER',91000,'Project Number','PROJECT_NUMBER','','','','ID020048','VARCHAR2','','','Project Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','REFERENCE_NUMBER',91000,'Reference Number','REFERENCE_NUMBER','','','','ID020048','VARCHAR2','','','Reference Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','EXPENSE_REPORT_STATUS',91000,'Expense Report Status','EXPENSE_REPORT_STATUS','','','','ID020048','VARCHAR2','','','Expense Report Status','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','QUERY_DESCR',91000,'Query Descr','QUERY_DESCR','','','','ID020048','VARCHAR2','','','Query Descr','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','QUERY_NUM',91000,'Query Num','QUERY_NUM','','','','ID020048','VARCHAR2','','','Query Num','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','VOUCHNO',91000,'Vouchno','VOUCHNO','','','','ID020048','VARCHAR2','','','Vouchno','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','APPROVED_IN_GL',91000,'Approved In Gl','APPROVED_IN_GL','','','','ID020048','VARCHAR2','','','Approved In Gl','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','APP_POST_FLAG',91000,'App Post Flag','APP_POST_FLAG','','','','ID020048','VARCHAR2','','','App Post Flag','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','REPORT_REJECT_CODE',91000,'Report Reject Code','REPORT_REJECT_CODE','','','','ID020048','VARCHAR2','','','Report Reject Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ADVANCE_NUMBER',91000,'Advance Number','ADVANCE_NUMBER','','','','ID020048','VARCHAR2','','','Advance Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ADVANCE_GL_DATE',91000,'Advance Gl Date','ADVANCE_GL_DATE','','','','ID020048','DATE','','','Advance Gl Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ADVANCE_FLAG',91000,'Advance Flag','ADVANCE_FLAG','','','','ID020048','VARCHAR2','','','Advance Flag','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ADVANCE_DISTRIBUTION_NUMBER',91000,'Advance Distribution Number','ADVANCE_DISTRIBUTION_NUMBER','','','','ID020048','VARCHAR2','','','Advance Distribution Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ADVANCE_INVOICE_TO_APPLY',91000,'Advance Invoice To Apply','ADVANCE_INVOICE_TO_APPLY','','','','ID020048','VARCHAR2','','','Advance Invoice To Apply','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','REPORT_TYPE',91000,'Report Type','REPORT_TYPE','','','','ID020048','VARCHAR2','','','Report Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','APPROVER_NAME',91000,'Approver Name','APPROVER_NAME','','','','ID020048','VARCHAR2','','','Approver Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','APPROVER_ID',91000,'Approver Id','APPROVER_ID','','','','ID020048','NUMBER','','','Approver Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','EXPENSE_CURRENT_APPROVER_ID',91000,'Expense Current Approver Id','EXPENSE_CURRENT_APPROVER_ID','','','','ID020048','NUMBER','','','Expense Current Approver Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','WORKFLOW_APPROVED_FLAG',91000,'Workflow Approved Flag','WORKFLOW_APPROVED_FLAG','','','','ID020048','VARCHAR2','','','Workflow Approved Flag','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','IMPORTED_TO_GL',91000,'Imported To Gl','IMPORTED_TO_GL','','','','ID020048','VARCHAR2','','','Imported To Gl','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','IMPORTED_TO_AP',91000,'Imported To Ap','IMPORTED_TO_AP','','','','ID020048','VARCHAR2','','','Imported To Ap','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','AMT_DUE_EMPLOYEE',91000,'Amt Due Employee','AMT_DUE_EMPLOYEE','','','','ID020048','NUMBER','','','Amt Due Employee','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','AMT_DUE_CCARD_COMPANY',91000,'Amt Due Ccard Company','AMT_DUE_CCARD_COMPANY','','','','ID020048','NUMBER','','','Amt Due Ccard Company','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','RECEIPT_CURRENCY_AMOUNT',91000,'Receipt Currency Amount','RECEIPT_CURRENCY_AMOUNT','','','','ID020048','NUMBER','','','Receipt Currency Amount','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','TRANSACTION_AMOUNT',91000,'Transaction Amount','TRANSACTION_AMOUNT','','','','ID020048','NUMBER','','','Transaction Amount','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CATEGORY_CODE',91000,'Category Code','CATEGORY_CODE','','','','ID020048','VARCHAR2','','','Category Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ATTRIBUTE_CATEGORY',91000,'Attribute Category','ATTRIBUTE_CATEGORY','','','','ID020048','VARCHAR2','','','Attribute Category','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','RATE_PER_MILE',91000,'Rate Per Mile','RATE_PER_MILE','','','','ID020048','NUMBER','','','Rate Per Mile','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','MILES',91000,'Miles','MILES','','','','ID020048','NUMBER','','','Miles','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','REPORT_TOTAL',91000,'Report Total','REPORT_TOTAL','','','','ID020048','NUMBER','','','Report Total','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ACCOUNTING_DATE',91000,'Accounting Date','ACCOUNTING_DATE','','','','ID020048','DATE','','','Accounting Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','MCC_CODE',91000,'Mcc Code','MCC_CODE','','','','ID020048','VARCHAR2','','','Mcc Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','MCC_CODE_NO',91000,'Mcc Code No','MCC_CODE_NO','','','','ID020048','VARCHAR2','','','Mcc Code No','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CARD_PROGRAM_NAME',91000,'Card Program Name','CARD_PROGRAM_NAME','','','','ID020048','VARCHAR2','','','Card Program Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CARD_PROGRAM_ID',91000,'Card Program Id','CARD_PROGRAM_ID','','','','ID020048','NUMBER','','','Card Program Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','DT_SENTTO_GL',91000,'Dt Sentto Gl','DT_SENTTO_GL','','','','ID020048','DATE','','','Dt Sentto Gl','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','AVG_MILEAGE_RATE',91000,'Avg Mileage Rate','AVG_MILEAGE_RATE','','','','ID020048','NUMBER','','','Avg Mileage Rate','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','TRIP_DISTANCE',91000,'Trip Distance','TRIP_DISTANCE','','','','ID020048','NUMBER','','','Trip Distance','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','DAILY_DISTANCE',91000,'Daily Distance','DAILY_DISTANCE','','','','ID020048','NUMBER','','','Daily Distance','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','DISTANCE_UNIT_CODE',91000,'Distance Unit Code','DISTANCE_UNIT_CODE','','','','ID020048','VARCHAR2','','','Distance Unit Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','DESTINATION_TO',91000,'Destination To','DESTINATION_TO','','','','ID020048','VARCHAR2','','','Destination To','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','DESTINATION_FROM',91000,'Destination From','DESTINATION_FROM','','','','ID020048','VARCHAR2','','','Destination From','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','MERCHANT_LOCATION',91000,'Merchant Location','MERCHANT_LOCATION','','','','ID020048','VARCHAR2','','','Merchant Location','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','POSTED_DATE',91000,'Posted Date','POSTED_DATE','','','','ID020048','DATE','','','Posted Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','TRANSACTION_DATE',91000,'Transaction Date','TRANSACTION_DATE','','','','ID020048','DATE','','','Transaction Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CARDMEMBER_NAME',91000,'Cardmember Name','CARDMEMBER_NAME','','','','ID020048','VARCHAR2','','','Cardmember Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CREDIT_CARD_TRX_ID',91000,'Credit Card Trx Id','CREDIT_CARD_TRX_ID','','','','ID020048','NUMBER','','','Credit Card Trx Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ATTENDEES_CTI2',91000,'Attendees Cti2','ATTENDEES_CTI2','','','','ID020048','VARCHAR2','','','Attendees Cti2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ATTENDEES_CTI1',91000,'Attendees Cti1','ATTENDEES_CTI1','','','','ID020048','VARCHAR2','','','Attendees Cti1','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ATTENDEES_TE',91000,'Attendees Te','ATTENDEES_TE','','','','ID020048','VARCHAR2','','','Attendees Te','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ATTENDEES_EMP',91000,'Attendees Emp','ATTENDEES_EMP','','','','ID020048','VARCHAR2','','','Attendees Emp','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ATTENDEES',91000,'Attendees','ATTENDEES','','','','ID020048','VARCHAR2','','','Attendees','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','REMARKS',91000,'Remarks','REMARKS','','','','ID020048','VARCHAR2','','','Remarks','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','END_EXPENSE_DATE',91000,'End Expense Date','END_EXPENSE_DATE','','','','ID020048','DATE','','','End Expense Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','START_EXPENSE_DATE',91000,'Start Expense Date','START_EXPENSE_DATE','','','','ID020048','DATE','','','Start Expense Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','JUSTIFICATION',91000,'Justification','JUSTIFICATION','','','','ID020048','VARCHAR2','','','Justification','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','BUSINESS_PURPOSE',91000,'Business Purpose','BUSINESS_PURPOSE','','','','ID020048','VARCHAR2','','','Business Purpose','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','MERCHANT_NAME',91000,'Merchant Name','MERCHANT_NAME','','','','ID020048','VARCHAR2','','','Merchant Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','VENDOR_NAME',91000,'Vendor Name','VENDOR_NAME','','','','ID020048','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','DESCRIPTION',91000,'Description','DESCRIPTION','','','','ID020048','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','REPORT_SUBMITTED_DATE',91000,'Report Submitted Date','REPORT_SUBMITTED_DATE','','','','ID020048','DATE','','','Report Submitted Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CREATION_DATE',91000,'Creation Date','CREATION_DATE','','','','ID020048','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','EMP_DEFAULT_COSTCTR',91000,'Emp Default Costctr','EMP_DEFAULT_COSTCTR','','','','ID020048','VARCHAR2','','','Emp Default Costctr','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','EMP_DEFAULT_LOC',91000,'Emp Default Loc','EMP_DEFAULT_LOC','','','','ID020048','VARCHAR2','','','Emp Default Loc','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','EMP_DEFAULT_PROD',91000,'Emp Default Prod','EMP_DEFAULT_PROD','','','','ID020048','VARCHAR2','','','Emp Default Prod','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','EMPLOYEE_NUMBER',91000,'Employee Number','EMPLOYEE_NUMBER','','','','ID020048','VARCHAR2','','','Employee Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','DISTRIBUTION_LINE_NUMBER',91000,'Distribution Line Number','DISTRIBUTION_LINE_NUMBER','','','','ID020048','NUMBER','','','Distribution Line Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','INVOICE_NUMBER',91000,'Invoice Number','INVOICE_NUMBER','','','','ID020048','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CURRENCY_CODE',91000,'Currency Code','CURRENCY_CODE','','','','ID020048','VARCHAR2','','','Currency Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','SUB_CATEGORY',91000,'Sub Category','SUB_CATEGORY','','','','ID020048','VARCHAR2','','','Sub Category','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CATEGORY',91000,'Category','CATEGORY','','','','ID020048','VARCHAR2','','','Category','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ORACLE_ACCOUNTS',91000,'Oracle Accounts','ORACLE_ACCOUNTS','','','','ID020048','VARCHAR2','','','Oracle Accounts','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ACCOUNT_DESCR',91000,'Account Descr','ACCOUNT_DESCR','','','','ID020048','VARCHAR2','','','Account Descr','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','COST_CENTER_DESCR',91000,'Cost Center Descr','COST_CENTER_DESCR','','','','ID020048','VARCHAR2','','','Cost Center Descr','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','LOCATION_DESCR',91000,'Location Descr','LOCATION_DESCR','','','','ID020048','VARCHAR2','','','Location Descr','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','PRODUCT_DESCR',91000,'Product Descr','PRODUCT_DESCR','','','','ID020048','VARCHAR2','','','Product Descr','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','CC_FISCAL_PERIOD',91000,'Cc Fiscal Period','CC_FISCAL_PERIOD','','','','ID020048','VARCHAR2','','','Cc Fiscal Period','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','QST_RECEIVABLE',91000,'Qst Receivable','QST_RECEIVABLE','','','','ID020048','NUMBER','','','Qst Receivable','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','QST_TAX_PORTION',91000,'Qst Tax Portion','QST_TAX_PORTION','','','','ID020048','NUMBER','','','Qst Tax Portion','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','HST_GST_RECEIVABLE',91000,'Hst Gst Receivable','HST_GST_RECEIVABLE','','','','ID020048','NUMBER','','','Hst Gst Receivable','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','PROVINCIAL_RITC_RECAPTURE',91000,'Provincial Ritc Recapture','PROVINCIAL_RITC_RECAPTURE','','','','ID020048','NUMBER','','','Provincial Ritc Recapture','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','TAX_PORTION',91000,'Tax Portion','TAX_PORTION','','','','ID020048','NUMBER','','','Tax Portion','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','TAX_PROVINCE',91000,'Tax Province','TAX_PROVINCE','','','','ID020048','VARCHAR2','','','Tax Province','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','HST_AMOUNT',91000,'Hst Amount','HST_AMOUNT','','','','ID020048','NUMBER','','','Hst Amount','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','QST_AMOUNT',91000,'Qst Amount','QST_AMOUNT','','','','ID020048','NUMBER','','','Qst Amount','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','GST_AMOUNT',91000,'Gst Amount','GST_AMOUNT','','','','ID020048','NUMBER','','','Gst Amount','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','PST_AMOUNT',91000,'Pst Amount','PST_AMOUNT','','','','ID020048','NUMBER','','','Pst Amount','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','SUB_ACCOUNT',91000,'Sub Account','SUB_ACCOUNT','','','','ID020048','VARCHAR2','','','Sub Account','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ACCOUNT',91000,'Account','ACCOUNT','','','','ID020048','VARCHAR2','','','Account','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','BRANCH',91000,'Branch','BRANCH','','','','ID020048','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ORACLE_ACCOUNT',91000,'Oracle Account','ORACLE_ACCOUNT','','','','ID020048','VARCHAR2','','','Oracle Account','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ORACLE_COST_CENTER',91000,'Oracle Cost Center','ORACLE_COST_CENTER','','','','ID020048','VARCHAR2','','','Oracle Cost Center','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ORACLE_LOCATION',91000,'Oracle Location','ORACLE_LOCATION','','','','ID020048','VARCHAR2','','','Oracle Location','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','ORACLE_PRODUCT',91000,'Oracle Product','ORACLE_PRODUCT','','','','ID020048','VARCHAR2','','','Oracle Product','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','LOB_BRANCH',91000,'Lob Branch','LOB_BRANCH','','','','ID020048','VARCHAR2','','','Lob Branch','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','FRU',91000,'Fru','FRU','','','','ID020048','VARCHAR2','','','Fru','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','SUB_TOTAL',91000,'Sub Total','SUB_TOTAL','','','','ID020048','NUMBER','','','Sub Total','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','LINE_AMOUNT',91000,'Line Amount','LINE_AMOUNT','','','','ID020048','NUMBER','','','Line Amount','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','FULL_NAME',91000,'Full Name','FULL_NAME','','','','ID020048','VARCHAR2','','','Full Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','EXPENSE_REPORT_NUMBER',91000,'Expense Report Number','EXPENSE_REPORT_NUMBER','','','','ID020048','NUMBER','','','Expense Report Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','EXPENSE_TYPE',91000,'Expense Type','EXPENSE_TYPE','','','','ID020048','VARCHAR2','','','Expense Type','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_TAX_PORTION_VW','PERIOD_NAME',91000,'Period Name','PERIOD_NAME','','','','ID020048','VARCHAR2','','','Period Name','','','');
--Inserting View Components for XXWC_EIS_TAX_PORTION_VW
--Inserting View Component Joins for XXWC_EIS_TAX_PORTION_VW
END;
/
/*set scan on define on
prompt Creating Report LOV Data for HDS AP_IExpense_Trxns_Details Tax Report
set scan off define off*/
DECLARE
BEGIN 
--Inserting Report LOVs - HDS AP_IExpense_Trxns_Details Tax Report
xxeis.eis_rs_ins.lov( 91000,'select DISTINCT EMPLOYEE_NUMBER from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_EMPLOYEE_NUMBER T&E','Oracle Employee number from the bullet train table','ID020048',NULL,'N','','');
xxeis.eis_rs_ins.lov( 91000,'select DISTINCT FULL_NAME from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_EMPLOYEE_NAME T&E','Oracle Employee name from the bullet train table','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 91000,'select DISTINCT ORACLE_LOCATION from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_ORACLE_LOCATION T&E','Oracle Location number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 91000,'select DISTINCT ORACLE_PRODUCT from XXCUS.XXCUS_BULLET_IEXP_TBL','','XXCUS_ORACLE_PRODUCT T&E','Oracle product number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','select distinct fru from xxcus.xxcus_bullet_iexp_tbl','','XXCUS_T&E_FRU','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
/*set scan on define on
prompt Creating Report Data for HDS AP_IExpense_Trxns_Details Tax Report
set scan off define off*/
DECLARE
BEGIN 
--Deleting Report data - HDS AP_IExpense_Trxns_Details Tax Report
xxeis.eis_rs_utility.delete_report_rows( 'HDS AP_IExpense_Trxns_Details Tax Report' );
--Inserting Report - HDS AP_IExpense_Trxns_Details Tax Report
xxeis.eis_rs_ins.r( 91000,'HDS AP_IExpense_Trxns_Details Tax Report','','','','','','ID020048','XXWC_EIS_TAX_PORTION_VW','Y','','','ID020048','','N','Canada Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - HDS AP_IExpense_Trxns_Details Tax Report
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'ACCOUNT','Account','Account','','','','','14','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'BRANCH','Branch','Branch','','','','','13','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'EXPENSE_REPORT_NUMBER','Expense Report Number','Expense Report Number','','','','','3','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'EXPENSE_TYPE','Expense Type','Expense Type','','','','','2','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'FRU','Fru','Fru','','','','','7','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'FULL_NAME','Full Name','Full Name','','','','','4','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'GST_AMOUNT','Gst Amount','Gst Amount','','','','','17','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'HST_AMOUNT','Hst Amount','Hst Amount','','','','','19','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'HST_GST_RECEIVABLE','Hst Gst Receivable','Hst Gst Receivable','','','','','23','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'LINE_AMOUNT','Line Amount','Line Amount','','','','','5','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'LOB_BRANCH','Lob Branch','Lob Branch','','','','','8','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'ORACLE_ACCOUNT','Oracle Account','Oracle Account','','','','','12','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'ORACLE_COST_CENTER','Oracle Cost Center','Oracle Cost Center','','','','','11','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'ORACLE_LOCATION','Oracle Location','Oracle Location','','','','','10','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'ORACLE_PRODUCT','Oracle Product','Oracle Product','','','','','9','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'PERIOD_NAME','Period Name','Period Name','','','','','1','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'PROVINCIAL_RITC_RECAPTURE','Provincial Ritc Recapture','Provincial Ritc Recapture','','','','','22','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'PST_AMOUNT','Pst Amount','Pst Amount','','','','','16','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'QST_AMOUNT','Qst Amount','Qst Amount','','','','','18','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'QST_RECEIVABLE','Qst Receivable','Qst Receivable','','','','','25','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'QST_TAX_PORTION','Qst Tax Portion','Qst Tax Portion','','','','','24','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'SUB_ACCOUNT','Sub Account','Sub Account','','','','','15','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'SUB_TOTAL','Sub Total','Sub Total','','','','','6','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'TAX_PORTION','Tax Portion','Tax Portion','','','','','21','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
xxeis.eis_rs_ins.rc( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'TAX_PROVINCE','Tax Province','Tax Province','','','','','20','N','','','','','','','','ID020048','N','N','','XXWC_EIS_TAX_PORTION_VW','','');
--Inserting Report Parameters - HDS AP_IExpense_Trxns_Details Tax Report
xxeis.eis_rs_ins.rp( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'VISA Posted Date Range From','VISA Posted Date Range FromFrom','POSTED_DATE','>=','','01-May-2005','DATE','Y','Y','1','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'VISA Posted Date Range To','VISA Posted Date Range To','POSTED_DATE','<=','','','DATE','Y','Y','2','','N','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'Oracle Product','','ORACLE_PRODUCT','IN','XXCUS_ORACLE_PRODUCT T&E','','VARCHAR2','N','Y','3','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'Employee Name','','FULL_NAME','IN','XXCUS_EMPLOYEE_NAME T&E','','VARCHAR2','N','Y','4','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'BranchFRU','','FRU','IN','XXCUS_T&E_FRU','','VARCHAR2','N','Y','5','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'Oracle Location','','ORACLE_LOCATION','IN','XXCUS_ORACLE_LOCATION T&E','','VARCHAR2','N','Y','6','','Y','CONSTANT','ID020048','Y','N','','','');
xxeis.eis_rs_ins.rp( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'Employee Number','','EMPLOYEE_NUMBER','IN','XXCUS_EMPLOYEE_NUMBER T&E','','VARCHAR2','N','Y','7','','Y','CONSTANT','ID020048','Y','N','','','');
--Inserting Report Conditions - HDS AP_IExpense_Trxns_Details Tax Report
xxeis.eis_rs_ins.rcn( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'XETPV.ORG_ID','=','''167''','','','Y','7','N','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'ORACLE_PRODUCT','IN',':Oracle Product','','','Y','3','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'FULL_NAME','IN',':Employee Name','','','Y','4','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'FRU','IN',':BranchFRU','','','Y','5','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'ORACLE_LOCATION','IN',':Oracle Location','','','Y','6','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'EMPLOYEE_NUMBER','IN',':Employee Number','','','Y','7','Y','ID020048');
xxeis.eis_rs_ins.rcn( 'HDS AP_IExpense_Trxns_Details Tax Report',91000,'','','','','AND ( (  ( XETPV.POSTED_DATE >= :VISA Posted Date Range From OR XETPV.POSTED_DATE IS NULL  )  ) )
AND ( (  ( XETPV.POSTED_DATE <= :VISA Posted Date Range To OR XETPV.POSTED_DATE IS NULL  )  ) )
','Y','1','','ID020048');
--Inserting Report Sorts - HDS AP_IExpense_Trxns_Details Tax Report
--Inserting Report Triggers - HDS AP_IExpense_Trxns_Details Tax Report
--Inserting Report Templates - HDS AP_IExpense_Trxns_Details Tax Report
--Inserting Report Portals - HDS AP_IExpense_Trxns_Details Tax Report
--Inserting Report Dashboards - HDS AP_IExpense_Trxns_Details Tax Report
--Inserting Report Security - HDS AP_IExpense_Trxns_Details Tax Report
--Inserting Report Pivots - HDS AP_IExpense_Trxns_Details Tax Report
END;
/
--set scan on define on
