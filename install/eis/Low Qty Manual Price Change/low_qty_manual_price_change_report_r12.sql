--Report Name            : Low Qty Manual Price Change Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
Create or replace View XXEIS.EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
 AS 
SELECT
  ood.organization_name branch_name,
  mp.organization_code branch,
    rs.NAME account_manager,
    xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name(oh.created_by,oh.creation_date) created_by,
    --fu.user_name                                                             CREATED_BY,
    xxeis.eis_rs_xxwc_com_util_pkg.get_pricing_resp(oh.created_by) pricing_responsibility,
    decode(ohh.hold_name,'Pricing Guardrail Hold',ohh.hold_name) hold_name,
    -- DECODE(OHH.HOLD_NAME,'Pricing Guardrail Hold',OHH.RELEASED_BY)          APPROVED_BY,
    --DECODE(OHH.HOLD_NAME,'Pricing Guardrail Hold',xxeis.eis_rs_xxwc_com_util_pkg.get_releaseby(OHH.RELEASED_BY,OHH.CREATION_DATE))APPROVED_BY,
    NULL approved_by,
    --DECODE(OHH.HOLD_NAME,'Pricing Guardrail Hold',FLVR.MEANING) REL_RES_CODE,
    NULL rel_res_code,
    --DECODE(OHH.HOLD_NAME,'Pricing Guardrail Hold',OHH.RELEASE_COMMENT) REL_RES_COMM,
    NULL rel_res_comm,
    hp.party_name customer_name,
    hca.account_number customer_number,
    to_char(oh.order_number) order_number,
    trunc(oh.ordered_date) ordered_date,
    rct.trx_number invoice_number,
    rct.trx_date invoice_date,
    ott.NAME order_type,
    CASE
      WHEN upper(flv.meaning) LIKE '%WALK%IN%'
      OR upper(flv.meaning) LIKE '%WILL%CALL%'
      THEN flv.meaning
      ELSE 'Delivery'
    END ship_method,
    --DECODE(flv.meaning,'5. Walk In','5. Walk In','8.  WCD-Will Call','8.  WCD-Will Call', '0. Will Call','0. Will Call','Delivery') SHIP_METHOD,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
    -- MSI.CONCATENATED_SEGMENTS                                                 ITEM_NUMBER,
    ol.ordered_item item_number,
    msi.description item_description,
    nvl(decode(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity),0) sales_qty,
    ol.unit_list_price base_price,
    (nvl(ol.unit_list_price,0) -xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id)) original_unit_selling_price,
    (nvl(ol.unit_list_price,0) -xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id))*nvl(decode(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity),0) orginal_extend_price,
    (nvl(ol.unit_list_price,0) -xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id))-nvl(ol.unit_selling_price,0) dollar_lost,
    xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_name(ol.header_id,ol.line_id) modifer_number,
    ol.unit_selling_price final_selling_price,
    nvl(ol.unit_selling_price,0)  *nvl(decode(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity),0) final_extended_price,
    (nvl(ol.unit_selling_price,0) -nvl(ol.unit_cost,0))*100/decode(nvl(ol.unit_selling_price,0),0,1,ol.unit_selling_price) final_gm,
    --((NVL(OL.UNIT_LIST_PRICE,0)   -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID)) -NVL(OL.UNIT_COST,0))*100/DECODE(NVL(OL.UNIT_SELLING_PRICE,0),0,1,OL.UNIT_SELLING_PRICE) ORGINIAL_GM,
    0 orginial_gm,
    apps.qp_qp_form_pricing_attr.get_meaning(opa.arithmetic_operator, 'ARITHMETIC_OPERATOR') application_method,
    --(OL.UNIT_LIST_PRICE -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID))-OL.UNIT_SELLING_PRICE DOLLAR_LOST,
    mp.attribute8 district,
    mp.attribute9 region,
    ol.inventory_item_id,
    ol.ship_from_org_id,
    ol.ordered_quantity,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LOW_HIGH_F(MSI.CONCATENATED_SEGMENTS,DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY)) QTY_TYPE,
    NVL(OL.UNIT_COST,0) UNIT_COST
  FROM oe_order_headers oh,
    oe_order_lines ol,
    oe_transaction_types_vl ott,
    fnd_lookup_values flv,
    hz_cust_accounts hca,
    hz_parties hp,
    oe_holds_history_v ohh,
    ra_salesreps rs,
    org_organization_definitions ood,
    mtl_parameters mp,
    mtl_system_items_b_kfv msi,
    oe_price_adjustments_v opa,
    ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    fnd_user fu
--    xxeis_wc_price_adj_v pri_adj
  --  FND_LOOKUP_VALUES FLVR
  WHERE oh.header_id          = ol.header_id
  AND ott.transaction_type_id = oh.order_type_id
  AND flv.lookup_type         = 'SHIP_METHOD'
  AND flv.lookup_code         = ol.shipping_method_code
  AND hca.cust_account_id     = ol.sold_to_org_id
  AND hca.party_id            = hp.party_id
  AND ohh.header_id(+)        = ol.header_id
  AND ohh.line_id(+)          = ol.line_id
  AND rs.salesrep_id(+)       = ol.salesrep_id
  AND rs.org_id(+)            = ol.org_id
 -- AND OL.SHIP_FROM_ORG_ID     = OOD.ORGANIZATION_ID
  AND mp.organization_id      = ol.ship_from_org_id
  AND msi.organization_id     = ol.ship_from_org_id
  AND msi.inventory_item_id   = ol.inventory_item_id
  AND opa.header_id(+)        = ol.header_id
  and opa.line_id(+)          = ol.line_id
  and ood.organization_id= mp.organization_id
--  and (pri_adj.header_id=ol.header_id and nvl(pri_adj.line_id,ol.line_id)=ol.line_id)
  --AND  opa.price_adjustment_id = xxeis.eis_rs_xxwc_com_util_pkg.GET_LAST_APP_PRICE_ADJUST_ID(ol.header_id, ol.line_id)
    /* AND opa.price_adjustment_id = (select price_adjustment_id
    from ( select price_adjustment_id
    from OE_PRICE_ADJUSTMENTS_V opa1
    where opa1.header_id = oh.header_id
    AND (opa1.line_id = ol.line_id or opa1.line_id  is null)
    AND OPA1.LIST_LINE_TYPE_CODE(+)   = 'DIS'
    ORDER BY pricing_group_sequence desc
    ) where rownum = 1
    )      */
  AND opa.list_line_type_code(+)      = 'DIS'
  AND opa.adjustment_name            IN ('AMOUNT_LINE_DISCOUNT','NEW PRICE','PERCENT_LINE_DISCOUNT','NEW_PRICE_DISCOUNT')
  AND rct.interface_header_context    = 'ORDER ENTRY'
  AND rct.interface_header_attribute1 = to_char(oh.order_number)
  AND rctl.interface_line_context     = 'ORDER ENTRY'
  AND upper(ol.flow_status_code)     <>'CANCELLED'
  AND rctl.sales_order                = oh.order_number
  AND rctl.interface_line_attribute6  = to_char(ol.line_id)
  AND rctl.interface_line_attribute1  = to_char(oh.order_number)
  AND rctl.customer_trx_id            = rct.customer_trx_id
  AND fu.user_id                      = oh.created_by
--  AND FLVR.LOOKUP_TYPE(+)             = 'RELEASE_REASON'
  and upper(ohh.hold_name(+))         = 'PRICING GUARDRAIL HOLD'
  --and mp.organization_code='710'
 -- AND FLVR.LOOKUP_CODE(+)             = OHH.RELEASE_REASON_CODE
 -- AND FLVR.VIEW_APPLICATION_ID(+)     = 660
  --AND OH.HEADER_ID=1101634  
  UNION
  SELECT
   ood.organization_name branch_name,
  mp.organization_code branch,
    rs.NAME account_manager,
    xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name(oh.created_by,oh.creation_date) created_by,
    -- fu.user_name                                                             CREATED_BY,
    xxeis.eis_rs_xxwc_com_util_pkg.get_pricing_resp(oh.created_by) pricing_responsibility,
    decode(ohh.hold_name,'Pricing Guardrail Hold',ohh.hold_name) hold_name,
    --DECODE(OHH.HOLD_NAME,'Pricing Guardrail Hold',xxeis.eis_rs_xxwc_com_util_pkg.get_releaseby(OHH.RELEASED_BY,OHH.CREATION_DATE))APPROVED_BY,
    NULL approved_by,
    --DECODE(OHH.HOLD_NAME,'Pricing Guardrail Hold',FLVR.MEANING) REL_RES_CODE,
    NULL rel_res_code,
    --DECODE(OHH.HOLD_NAME,'Pricing Guardrail Hold',OHH.RELEASE_COMMENT) REL_RES_COMM,
    NULL rel_res_comm,
    hp.party_name customer_name,
    hca.account_number customer_number,
    to_char(oh.order_number) order_number,
    trunc(oh.ordered_date) ordered_date,
    NULL invoice_number,
    NULL invoice_date,
    ott.NAME order_type,
    CASE
      WHEN upper(flv.meaning) LIKE '%WALK%IN%'
      OR upper(flv.meaning) LIKE '%WILL%CALL%'
      THEN flv.meaning
      ELSE 'Delivery'
    END ship_method,
    --DECODE(flv.meaning,'5. Walk In','5. Walk In','8.  WCD-Will Call','8.  WCD-Will Call', '0. Will Call','0. Will Call','Delivery') SHIP_METHOD,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
    --msi.concatenated_segments                                                 item_number,
    ol.ordered_item item_number,
    msi.description item_description,
    decode(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) sales_qty,
    --NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY) SALES_QTY,
    ol.unit_list_price base_price,
    (nvl(ol.unit_list_price,0) -xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id)) original_unit_selling_price,
    (nvl(ol.unit_list_price,0) -xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id))*nvl(decode(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity),0) orginal_extend_price,
    (nvl(ol.unit_list_price,0) -xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_amt(ol.header_id,ol.line_id))-nvl(ol.unit_selling_price,0) dollar_lost,
    xxeis.eis_rs_xxwc_com_util_pkg.get_adj_auto_modifier_name(ol.header_id,ol.line_id) modifer_number,
    ol.unit_selling_price final_selling_price,
    nvl(ol.unit_selling_price,0)  * nvl(decode(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity),0) final_extended_price,
    (nvl(ol.unit_selling_price,0) -nvl(ol.unit_cost,0))*100/decode(nvl(ol.unit_selling_price,0),0,1,ol.unit_selling_price) final_gm,
    --((NVL(OL.UNIT_LIST_PRICE,0)   -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID)) -NVL(OL.UNIT_COST,0))*100/DECODE(NVL(OL.UNIT_SELLING_PRICE,0),0,1,OL.UNIT_SELLING_PRICE) ORGINIAL_GM,
    0 orginial_gm,
    apps.qp_qp_form_pricing_attr.get_meaning(opa.arithmetic_operator, 'ARITHMETIC_OPERATOR') application_method,
    --(OL.UNIT_LIST_PRICE -XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ADJ_AUTO_MODIFIER_AMT(OL.HEADER_ID,OL.LINE_ID))-OL.UNIT_SELLING_PRICE DOLLAR_LOST,
    mp.attribute8 district,
    mp.attribute9 region,
    ol.inventory_item_id,
    ol.ship_from_org_id,
    ol.ordered_quantity,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LOW_HIGH_F(MSI.CONCATENATED_SEGMENTS,DECODE(OL.LINE_CATEGORY_CODE,'RETURN',(OL.ORDERED_QUANTITY*-1),OL.ORDERED_QUANTITY)) QTY_TYPE,
    NVL(OL.UNIT_COST,0) UNIT_COST
  FROM oe_order_headers oh,
    oe_order_lines ol,
    oe_transaction_types_vl ott,
    fnd_lookup_values flv,
    hz_cust_accounts hca,
    hz_parties hp,
    oe_holds_history_v ohh,
    ra_salesreps rs,
    org_organization_definitions ood,
    mtl_parameters mp,
    mtl_system_items_b_kfv msi,
    oe_price_adjustments_v opa,
    fnd_user fu
--    xxeis_wc_price_adj_v pri_adj
  ---  FND_LOOKUP_VALUES FLVR
  WHERE oh.header_id              = ol.header_id
  AND ott.transaction_type_id     = oh.order_type_id
  AND flv.lookup_type             = 'SHIP_METHOD'
  AND flv.lookup_code             = ol.shipping_method_code
  AND hca.cust_account_id         = ol.sold_to_org_id
  AND hca.party_id                = hp.party_id
  AND ohh.header_id(+)            = ol.header_id
  AND ohh.line_id(+)              = ol.line_id
  AND rs.salesrep_id(+)           = ol.salesrep_id
  AND rs.org_id(+)                = ol.org_id
 -- AND OL.SHIP_FROM_ORG_ID         = OOD.ORGANIZATION_ID
  AND mp.organization_id          = ol.ship_from_org_id
  AND msi.organization_id         = ol.ship_from_org_id
  AND msi.inventory_item_id       = ol.inventory_item_id
  AND opa.header_id(+)            = ol.header_id
  AND opa.line_id(+)              = ol.line_id
  AND upper(ol.flow_status_code) <>'CANCELLED'
  and ood.organization_id= mp.organization_id
--  and (pri_adj.header_id=ol.header_id and nvl(pri_adj.line_id,ol.line_id)=ol.line_id )
  --AND opa.price_adjustment_id     = xxeis.eis_rs_xxwc_com_util_pkg.GET_LAST_APP_PRICE_ADJUST_ID(ol.header_id, ol.line_id)
    /*AND opa.price_adjustment_id = (select price_adjustment_id
    from ( select price_adjustment_id
    from OE_PRICE_ADJUSTMENTS_V opa1
    where opa1.header_id = oh.header_id
    AND (opa1.line_id = ol.line_id or opa1.line_id  is null)
    AND OPA1.LIST_LINE_TYPE_CODE(+)   = 'DIS'
    ORDER BY pricing_group_sequence desc
    ) where rownum = 1
    )    */
  AND opa.list_line_type_code(+)         = 'DIS'
  AND opa.adjustment_name               IN ('AMOUNT_LINE_DISCOUNT','NEW PRICE','PERCENT_LINE_DISCOUNT','NEW_PRICE_DISCOUNT')
  AND fu.user_id                         = oh.created_by
 -- AND FLVR.LOOKUP_TYPE(+)                = 'RELEASE_REASON'
 -- AND FLVR.LOOKUP_CODE(+)                = OHH.RELEASE_REASON_CODE
  and upper(ohh.hold_name(+))            = 'PRICING GUARDRAIL HOLD'
  --and mp.organization_code='710'
 -- AND FLVR.VIEW_APPLICATION_ID(+)        = 660
  AND (ol.invoice_interface_status_code IS NULL
  OR ( NOT EXISTS
    (SELECT   1
    FROM ra_customer_trx_lines ril
    WHERE to_char(ol.line_id) = ril.interface_line_attribute6
    AND ril.interface_line_context='ORDER ENTRY'
    ) ) )/
set scan on define on
prompt Creating View Data for Low Qty Manual Price Change Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Low Qty Man Pri Chg V','EXLQMPCV','','');
--Delete View Columns for EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','QTY_TYPE',660,'Qty Type','QTY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Qty Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','REGION',660,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','DISTRICT',660,'District','DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','FINAL_GM',660,'Final Gm','FINAL_GM','','','','XXEIS_RS_ADMIN','NUMBER','','','Final Gm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','FINAL_SELLING_PRICE',660,'Final Selling Price','FINAL_SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Final Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','MODIFER_NUMBER',660,'Modifer Number','MODIFER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Modifer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORIGINAL_UNIT_SELLING_PRICE',660,'Original Unit Selling Price','ORIGINAL_UNIT_SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Original Unit Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','SALES_QTY',660,'Sales Qty','SALES_QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','REL_RES_CODE',660,'Rel Res Code','REL_RES_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rel Res Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ACCOUNT_MANAGER',660,'Account Manager','ACCOUNT_MANAGER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','BRANCH',660,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','APPROVED_BY',660,'Approved By','APPROVED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approved By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','BRANCH_NAME',660,'Branch Name','BRANCH_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','APPLICATION_METHOD',660,'Application Method','APPLICATION_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Application Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','FINAL_EXTENDED_PRICE',660,'Final Extended Price','FINAL_EXTENDED_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Final Extended Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORGINAL_EXTEND_PRICE',660,'Orginal Extend Price','ORGINAL_EXTEND_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Orginal Extend Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','BASE_PRICE',660,'Base Price','BASE_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Base Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','SHIP_METHOD',660,'Ship Method','SHIP_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','HOLD_NAME',660,'Hold Name','HOLD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hold Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','PRICING_RESPONSIBILITY',660,'Pricing Responsibility','PRICING_RESPONSIBILITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pricing Responsibility','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORDERED_QUANTITY',660,'Ordered Quantity','ORDERED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Ordered Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','SHIP_FROM_ORG_ID',660,'Ship From Org Id','SHIP_FROM_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ship From Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','DOLLAR_LOST',660,'Dollar Lost','DOLLAR_LOST','','','','XXEIS_RS_ADMIN','NUMBER','','','Dollar Lost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','REL_RES_COMM',660,'Rel Res Comm','REL_RES_COMM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rel Res Comm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','ORGINIAL_GM',660,'Orginial Gm','ORGINIAL_GM','','','','XXEIS_RS_ADMIN','NUMBER','','','Orginial Gm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','UNIT_COST',660,'Unit Cost','UNIT_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Cost','','','');
--Inserting View Components for EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
--Inserting View Component Joins for EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Low Qty Manual Price Change Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Low Qty Manual Price Change Report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS, HR_OPERATING_UNITS OU
WHERE RS.org_id = OU.organization_id
AND RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select  fu.USER_NAME,FU.USER_ID    from  fnd_user fu','','OM USER NAME','This gives the user''s names','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT MSI.SEGMENT1   ITEM_NUMBER,
MSI.DESCRIPTION  ITEM_DESCRIPTION from mtl_system_items_b msi','','OM Item Number LOV','Order Item numbers','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select DESCRIPTION ITEM_DESCRIPTION from MTL_SYSTEM_ITEMS_KFV
where exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = organization_id)

','','OM ITEM DESCRIPTION','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT organization_code org_code,
  organization_name org_name
FROM org_organization_definitions
UNION
SELECT ''All'', ''All Organizations'' FROM Dual','','Branch Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct pov.segment1 vendor_number, pov.vendor_name vendor_name  from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC VENDOR NUMBER','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ''LOW'' X FROM DUAL
UNION
select ''HIGH'' x from dual','','XXWC_QTY_TYPE','This is custom lov','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct pov.vendor_name vendor_name  ,pov.segment1 vendor_number from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC_VEN_NAME','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Low Qty Manual Price Change Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Low Qty Manual Price Change Report
xxeis.eis_rs_utility.delete_report_rows( 'Low Qty Manual Price Change Report' );
--Inserting Report - Low Qty Manual Price Change Report
xxeis.eis_rs_ins.r( 660,'Low Qty Manual Price Change Report','','The purpose of this report is to distinguish between low quantity and high quanity manual price adjustments, as well as review the corresponding reason codes for the manual price changes that went on hold.','','','','XXEIS_RS_ADMIN','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','Y','','','XXEIS_RS_ADMIN','','Y','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Low Qty Manual Price Change Report
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'ACCOUNT_MANAGER','Account Manager','Account Manager','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'CREATED_BY','Created By','Created By','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'DISTRICT','District','District','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'FINAL_GM','Final GM%','Final Gm','','~,~.~2','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'FINAL_SELLING_PRICE','Final Selling Price','Final Selling Price','','~,~.~5','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'MODIFER_NUMBER','Modifer Number','Modifer Number','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'ORIGINAL_UNIT_SELLING_PRICE','Original Unit Selling Price','Original Unit Selling Price','','~,~.~5','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'QTY_TYPE','Qty Type','Qty Type','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'REGION','Region','Region','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'SALES_QTY','Sales Qty','Sales Qty','','~T~D~0','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'APPROVED_BY','Approved By','Approved By','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'REL_RES_CODE','Hold Release Reason Code','Rel Res Code','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'DOLLARS_LOST','Dollar Lost','Rel Res Code','NUMBER','~T~D~5','default','','15','Y','','','','','','','(NVL(EXLQMPCV.ORGINAL_EXTEND_PRICE,0)-NVL(EXLQMPCV.FINAL_EXTENDED_PRICE,0))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'BRANCH_NAME','Branch Name','Branch Name','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
xxeis.eis_rs_ins.rc( 'Low Qty Manual Price Change Report',660,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_LOW_QTY_MAN_PRI_CHG_V','','');
--Inserting Report Parameters - Low Qty Manual Price Change Report
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Account Manager','Account Manager','ACCOUNT_MANAGER','IN','OM SALES REP','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Branch #','Branch','BRANCH','IN','Branch Lov','','VARCHAR2','N','Y','9','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Created By','Created By','CREATED_BY','IN','OM USER NAME','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','15','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','14','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Item Description','Item Description','ITEM_DESCRIPTION','IN','OM ITEM DESCRIPTION','','VARCHAR2','N','Y','13','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Item #','Item Number','ITEM_NUMBER','IN','OM Item Number LOV','','VARCHAR2','N','Y','12','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Order Start Date','Order Start Date','ORDERED_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Qty Type','Qty Type','QTY_TYPE','IN','XXWC_QTY_TYPE','','VARCHAR2','N','Y','16','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Vendor Name','Vendor Name','VENDOR_NAME','IN','XXWC_VEN_NAME','','VARCHAR2','N','Y','11','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Vendor #','Vendor Number','VENDOR_NUMBER','IN','XXWC VENDOR NUMBER','','VARCHAR2','N','Y','10','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Order End Date','Order End Date','ORDERED_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Invoice Start Date','Invoice Start Date','INVOICE_DATE','>=','','','DATE','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Low Qty Manual Price Change Report',660,'Invoice End Date','Invoice End Date','INVOICE_DATE','<=','','','DATE','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Low Qty Manual Price Change Report
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'ACCOUNT_MANAGER','IN',':Account Manager','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'CREATED_BY','IN',':Created By','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'CUSTOMER_NAME','IN',':Customer Name','','','Y','15','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','14','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'ITEM_DESCRIPTION','IN',':Item Description','','','Y','13','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'ITEM_NUMBER','IN',':Item #','','','Y','12','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'QTY_TYPE','IN',':Qty Type','','','Y','16','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'REGION','IN',':Region','','','Y','7','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'VENDOR_NAME','IN',':Vendor Name','','','Y','11','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'VENDOR_NUMBER','IN',':Vendor #','','','Y','10','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'DISTRICT','IN',':District','','','Y','8','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'ORDERED_DATE','>=',':Order Start Date','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'ORDERED_DATE','<=',':Order End Date','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'INVOICE_DATE','>=',':Invoice Start Date','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'INVOICE_DATE','<=',':Invoice End Date','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Low Qty Manual Price Change Report',660,'','','','','AND ( :Branch # IS NULL OR ''All'' IN (:Branch #) OR (BRANCH IN (:Branch #)))
','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Low Qty Manual Price Change Report
--Inserting Report Triggers - Low Qty Manual Price Change Report
--Inserting Report Templates - Low Qty Manual Price Change Report
--Inserting Report Portals - Low Qty Manual Price Change Report
--Inserting Report Dashboards - Low Qty Manual Price Change Report
--Inserting Report Security - Low Qty Manual Price Change Report
xxeis.eis_rs_ins.rsec( 'Low Qty Manual Price Change Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Low Qty Manual Price Change Report','661','','50891',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Low Qty Manual Price Change Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Low Qty Manual Price Change Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Low Qty Manual Price Change Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Low Qty Manual Price Change Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Low Qty Manual Price Change Report
END;
/
set scan on define on
