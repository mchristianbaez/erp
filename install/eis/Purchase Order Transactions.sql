--Report Name            : Purchase Order Transactions
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_INV_PO_TRANSACTIONS_V
set scan off define off
prompt Creating View XXEIS.EIS_INV_PO_TRANSACTIONS_V
Create or replace View XXEIS.EIS_INV_PO_TRANSACTIONS_V
 AS 
SELECT msi.concatenated_segments item
         ,REPLACE (msil.description, '~', '-') item_description
         ,ph.segment1 po_number
         ,rsh.receipt_num receipt_number
         ,mmt.subinventory_code subinventory
         ,TRUNC (mmt.transaction_date) transaction_date
         ,pv.vendor_name vendor_name
         ,pv.segment1 vendor_number
         ,mmt.creation_date creation_date
         ,oap.period_name gl_period
         ,oap.period_start_date gl_period_start_date
         ,mmt.actual_cost unit_cost
         ,mmt.revision item_revision
         ,mil.concatenated_segments stock_locators
         ,pl.line_num po_line_number
         ,pp.name project_name
         ,pp.segment1 project_number
         ,mmt.primary_quantity quantity
         , (mmt.actual_cost * mmt.primary_quantity) VALUE
         ,mtr.reason_id
         ,mtr.reason_name reason
         ,pt.task_name task_name
         ,pt.task_number task_number
         ,xxeis.eis_inv_utility_pkg.get_lookup_meaning ('MTL_TRANSACTION_ACTION', mmt.transaction_action_id)
             transaction_action
         ,mmt.transaction_reference transaction_reference
         ,mts.transaction_source_type_name
         ,mtt.transaction_type_name transaction_type
         ,msi.primary_unit_of_measure unit_of_measure
         ,DECODE (mmt.costed_flag, 'N', 'No', NVL (mmt.costed_flag, 'Yes')) valued_flag
         ,mtt.transaction_type_id
         ,mil.inventory_location_id
         ,pt.task_id
         ,pp.project_id
         ,oap.acct_period_id
         ,msi.inventory_item_id
         ,rsh.shipment_header_id
         ,rt.transaction_id rcv_transaction_id
         ,pv.vendor_id
         ,pl.po_header_id
         ,pl.line_num
         ,rsh.shipment_num
         ,rsh.receipt_num
         ,haou.name inv_org_name
         ,mmt.organization_id
         ,mmt.transaction_source_type_id
         ,mmt.transaction_action_id
         ,mmt.transaction_source_id
         ,mmt.department_id
         ,mmt.error_explanation
         ,mmt.vendor_lot_number supplier_lot
         ,mmt.source_line_id
         ,mmt.parent_transaction_id
         ,mmt.shipment_number shipment_number
         ,mmt.waybill_airbill waybill_airbill
         ,mmt.freight_code freight_code
         ,mmt.number_of_containers
         ,mmt.move_transaction_id
         ,mmt.completion_transaction_id
         ,mmt.operation_seq_num opertion_sequence
         ,mmt.expenditure_type
         ,mmt.transaction_set_id
         ,mmt.transaction_source_name
         ,mmt.transaction_uom
         ,mmt.transfer_subinventory
         ,mmt.transfer_transaction_id
         ,mmt.transaction_id mmt_transaction_id
         ,mil.organization_id mil_organization_id
         ,oap.organization_id oap_organization_id
         ,msil.inventory_item_id msil_inventory_item_id
         ,msil.organization_id msil_organization_id
         ,msil.language
         ,msi.organization_id msi_organization_id
         ,pl.po_line_id
         ,ph.po_header_id ph_po_header_id
         ,mp.organization_id mp_organization_id
         ,haou.organization_id haou_organization_id
         ,mts.transaction_source_type_id mts_transaction_source_type_id
         --descr#flexfield#start

         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', mmt.attribute1, 'I')
            ,NULL)
             mmt#wcrentala#home_branch
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute10, NULL)
             mmt#wcrentala#vendor
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute11, NULL)
             mmt#wcrentala#po_number
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute12, NULL)
             mmt#wcrentala#item_number
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute13, NULL)
             mmt#wcrentala#cer_number
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute14, NULL)
             mmt#wcrentala#misc_issue_tra
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FRU', mmt.attribute2, 'I')
            ,NULL)
             mmt#wcrentala#custodian_bran
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FA_MAJOR_CATEGORY'
                                                                          ,mmt.attribute3
                                                                          ,'I')
            ,NULL)
             mmt#wcrentala#major_category
         ,DECODE (mmt.attribute_category, 'WC Rental Asset Transfer', mmt.attribute4, NULL)
             mmt#wcrentala#minor_category
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FA_STATE'
                                                                          ,mmt.attribute5
                                                                          ,'I')
            ,NULL)
             mmt#wcrentala#state
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('XXCUS_FA_CITY'
                                                                          ,mmt.attribute6
                                                                          ,'I')
            ,NULL)
             mmt#wcrentala#city
         ,DECODE (
             mmt.attribute_category
            ,'WC Rental Asset Transfer', xxeis.eis_rs_dff.decode_valueset ('Yes_No', mmt.attribute9, 'F')
            ,NULL)
             mmt#wcrentala#transfer_to_ma
         ,DECODE (
             mmt.attribute_category
            ,'WC Return to Vendor', xxeis.eis_rs_dff.decode_valueset ('XXWC_INV_SUPPLIER_NUMBER'
                                                                     ,mmt.attribute1
                                                                     ,'F')
            ,NULL)
             mmt#wcreturnt#supplier_numbe
         ,DECODE (mmt.attribute_category, 'WC Return to Vendor', mmt.attribute2, NULL)
             mmt#wcreturnt#return_number
         ,DECODE (mmt.attribute_category, 'WC Return to Vendor', mmt.attribute3, NULL)
             mmt#wcreturnt#return_unit_pr
         ,mp.attribute1 mp#factory_planner_data_dire
         ,mp.attribute10 mp#fru
         ,mp.attribute11 mp#location_number
         ,xxeis.eis_rs_dff.decode_valueset ('HR_DE_EMPLOYEES', mp.attribute13, 'F')
             mp#branch_operations_manager
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DELIVERY_CHARGE_EXEMPT', mp.attribute14, 'I')
             mp#deliver_charge
         ,mp.attribute2 mp#factory_planner_executabl
         ,mp.attribute3 mp#factory_planner_user
         ,mp.attribute4 mp#factory_planner_host
         ,mp.attribute5 mp#factory_planner_port_numb
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_PRICE_ZONES', mp.attribute6, 'F') mp#pricing_zone
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_ORG_TYPE', mp.attribute7, 'I') mp#org_type
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_DISTRICT', mp.attribute8, 'I') mp#district
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_REGION', mp.attribute9, 'I') mp#region
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute1, NULL) msi#hds#lob
         ,DECODE (msi.attribute_category
                 ,'HDS', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute10, 'F')
                 ,NULL)
             msi#hds#drop_shipment_eligab
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute15, NULL) msi#hds#invoice_uom
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute2, NULL) msi#hds#product_id
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute3, NULL) msi#hds#vendor_part_number
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute4, NULL) msi#hds#unspsc_code
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute5, NULL) msi#hds#upc_primary
         ,DECODE (msi.attribute_category, 'HDS', msi.attribute6, NULL) msi#hds#sku_description
         ,DECODE (msi.attribute_category, 'WC', msi.attribute1, NULL) msi#wc#ca_prop_65
         ,DECODE (msi.attribute_category, 'WC', msi.attribute10, NULL) msi#wc#country_of_origin
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute11, 'F')
                 ,NULL)
             msi#wc#orm_d_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute12, NULL) msi#wc#store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute13, NULL) msi#wc#dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute14, NULL) msi#wc#yearly_store_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute15, NULL) msi#wc#yearly_dc_velocity
         ,DECODE (msi.attribute_category, 'WC', msi.attribute16, NULL) msi#wc#prism_part_number
         ,DECODE (msi.attribute_category, 'WC', msi.attribute17, NULL) msi#wc#hazmat_description
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC HAZMAT CONTAINER', msi.attribute18, 'I')
                 ,NULL)
             msi#wc#hazmat_container
         ,DECODE (msi.attribute_category, 'WC', msi.attribute19, NULL) msi#wc#gtp_indicator
         ,DECODE (msi.attribute_category, 'WC', msi.attribute2, NULL) msi#wc#last_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute20, NULL) msi#wc#amu
         ,DECODE (msi.attribute_category, 'WC', msi.attribute21, NULL) msi#wc#reserve_stock
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC TAXWARE CODE', msi.attribute22, 'I')
                 ,NULL)
             msi#wc#taxware_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute25, NULL) msi#wc#average_units
         ,DECODE (msi.attribute_category, 'WC', msi.attribute26, NULL) msi#wc#product_code
         ,DECODE (msi.attribute_category, 'WC', msi.attribute27, NULL) msi#wc#import_duty_
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute29, 'F')
                 ,NULL)
             msi#wc#keep_item_active
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('Yes_No', msi.attribute3, 'F')
                 ,NULL)
             msi#wc#pesticide_flag
         ,DECODE (msi.attribute_category, 'WC', msi.attribute30, NULL) msi#wc#calc_lead_time
         ,DECODE (msi.attribute_category, 'WC', msi.attribute4, NULL) msi#wc#voc_gl
         ,DECODE (msi.attribute_category, 'WC', msi.attribute5, NULL) msi#wc#pesticide_flag_state
         ,DECODE (msi.attribute_category, 'WC', msi.attribute6, NULL) msi#wc#voc_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute7, NULL) msi#wc#voc_sub_category
         ,DECODE (msi.attribute_category, 'WC', msi.attribute8, NULL) msi#wc#msds_#
         ,DECODE (msi.attribute_category
                 ,'WC', xxeis.eis_rs_dff.decode_valueset ('XXWC_HAZMAT_PACKAGE_GROUP', msi.attribute9, 'I')
                 ,NULL)
             msi#wc#hazmat_packaging_grou
         ,DECODE (ph.attribute_category, 'Standard Purchase Order', ph.attribute1, NULL)
             ph#standardp#need_by_date
         ,DECODE (ph.attribute_category, 'Standard Purchase Order', ph.attribute2, NULL)
             ph#standardp#freight_terms_t
         ,DECODE (ph.attribute_category, 'Standard Purchase Order', ph.attribute3, NULL)
             ph#standardp#carrier_terms_t
         ,DECODE (ph.attribute_category, 'Standard Purchase Order', ph.attribute4, NULL)
             ph#standardp#fob_terms_tab
         ,DECODE (pv.attribute_category, 'R11 Vendor ID', pv.attribute15, NULL) pv#r11vendor#r11_vendor_id
         ,DECODE (pv.attribute_category, 'Tax Exempt Flag', pv.attribute1, NULL) pv#taxexempt#exemptions
         ,rsh.attribute1 rsh#printed
         --descr#flexfield#end


         --kff#start
         ,mil.segment1 "MIL#101#STOCKLOCATIONS"
         ,xxeis.eis_rs_dff.decode_valueset ('XXWC_STOCK_LOCATIONS', mil.segment1, 'N')
             "MIL#101#STOCKLOCATIONS_DESC"
     --kff#end
     FROM mtl_transaction_types mtt
         ,mtl_item_locations_kfv mil
         ,pa_tasks pt
         ,pa_projects pp
         ,org_acct_periods oap
         ,mtl_system_items_tl msil
         ,mtl_system_items_kfv msi
         ,mtl_material_transactions mmt
         ,rcv_shipment_headers rsh
         ,rcv_transactions rt
         ,po_vendors pv
         ,po_lines pl
         ,po_headers ph
         ,mtl_parameters mp
         ,mtl_transaction_reasons mtr
         ,hr_all_organization_units haou
         ,mtl_txn_source_types mts
    WHERE     msi.organization_id = mp.organization_id
          AND mmt.organization_id = mp.organization_id
          AND mmt.inventory_item_id = msi.inventory_item_id
          AND mmt.transaction_source_type_id = 1
          AND ph.po_header_id = mmt.transaction_source_id
          AND rt.transaction_id = mmt.rcv_transaction_id
          AND pl.po_line_id = rt.po_line_id
          AND rsh.shipment_header_id = rt.shipment_header_id
          AND pv.vendor_id = ph.vendor_id
          AND oap.organization_id(+) = mmt.organization_id
          AND oap.acct_period_id(+) = mmt.acct_period_id
          AND msil.inventory_item_id(+) = msi.inventory_item_id
          AND msil.organization_id(+) = msi.organization_id
          AND msil.language(+) = USERENV ('LANG')
          AND mmt.project_id = pp.project_id(+)
          AND mmt.task_id = pt.task_id(+)
          AND mmt.locator_id = mil.inventory_location_id(+)
          AND mil.organization_id(+) = mmt.organization_id
          AND mmt.transaction_type_id = mtt.transaction_type_id(+)
          AND mtr.reason_id(+) = mmt.reason_id
          AND haou.organization_id = mmt.organization_id
          AND mts.transaction_source_type_id = mmt.transaction_source_type_id
          AND EXISTS
                 (SELECT 1
                    FROM xxeis.eis_org_access_v
                   WHERE organization_id = msi.organization_id)/
set scan on define on
prompt Creating View Data for Purchase Order Transactions
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_INV_PO_TRANSACTIONS_V
xxeis.eis_rs_ins.v( 'EIS_INV_PO_TRANSACTIONS_V',401,'Purchase Order Transactions','','','','MR020532','XXEIS','Eis Inv Po Transactions V','EIPTV1','','');
--Delete View Columns for EIS_INV_PO_TRANSACTIONS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_INV_PO_TRANSACTIONS_V',401,FALSE);
--Inserting View Columns for EIS_INV_PO_TRANSACTIONS_V
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_TYPE',401,'Transaction Type','TRANSACTION_TYPE','','','','MR020532','VARCHAR2','','','Transaction Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','LANGUAGE',401,'Language','LANGUAGE~LANGUAGE','','','','MR020532','VARCHAR2','','','Language','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TASK_NAME',401,'Task Name','TASK_NAME','','','','MR020532','VARCHAR2','','','Task Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_ACTION',401,'Transaction Action','TRANSACTION_ACTION','','','','MR020532','VARCHAR2','','','Transaction Action','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_DATE',401,'Transaction Date','TRANSACTION_DATE','','','','MR020532','DATE','','','Transaction Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_REFERENCE',401,'Transaction Reference','TRANSACTION_REFERENCE','','','','MR020532','VARCHAR2','','','Transaction Reference','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','ACCT_PERIOD_ID',401,'Acct Period Id','ACCT_PERIOD_ID~ACCT_PERIOD_ID','','~T~D~2','','MR020532','NUMBER','','','Acct Period Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','SUBINVENTORY',401,'Subinventory','SUBINVENTORY','','','','MR020532','VARCHAR2','','','Subinventory','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','UNIT_COST',401,'Unit Cost','UNIT_COST~UNIT_COST','','~T~D~2','','MR020532','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','VALUE',401,'Value','VALUE~VALUE','','','','MR020532','NUMBER','','','Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','GL_PERIOD',401,'Gl Period','GL_PERIOD~GL_PERIOD','','','','MR020532','VARCHAR2','','','Gl Period','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','ITEM',401,'Item','ITEM~ITEM','','','','MR020532','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PO_LINE_NUMBER',401,'Po Line Number','PO_LINE_NUMBER~PO_LINE_NUMBER','','','','MR020532','NUMBER','','','Po Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PO_NUMBER',401,'Po Number','PO_NUMBER~PO_NUMBER','','','','MR020532','VARCHAR2','','','Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','QUANTITY',401,'Quantity','QUANTITY~QUANTITY','','','','MR020532','NUMBER','','','Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','RECEIPT_NUMBER',401,'Receipt Number','RECEIPT_NUMBER~RECEIPT_NUMBER','','','','MR020532','VARCHAR2','','','Receipt Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','STOCK_LOCATORS',401,'Stock Locators','STOCK_LOCATORS~STOCK_LOCATORS','','','','MR020532','VARCHAR2','','','Stock Locators','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TASK_NUMBER',401,'Task Number','TASK_NUMBER~TASK_NUMBER','','','','MR020532','VARCHAR2','','','Task Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','UNIT_OF_MEASURE',401,'Unit Of Measure','UNIT_OF_MEASURE~UNIT_OF_MEASURE','','','','MR020532','VARCHAR2','','','Unit Of Measure','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','VALUED_FLAG',401,'Valued Flag','VALUED_FLAG~VALUED_FLAG','','','','MR020532','VARCHAR2','','','Valued Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME~VENDOR_NAME','','','','MR020532','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER~VENDOR_NUMBER','','','','MR020532','VARCHAR2','','','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','INV_ORG_NAME',401,'Inv Org Name','INV_ORG_NAME','','','','MR020532','VARCHAR2','','','Inv Org Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','MR020532','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','GL_PERIOD_START_DATE',401,'Gl Period Start Date','GL_PERIOD_START_DATE','','','','MR020532','DATE','','','Gl Period Start Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','MR020532','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','ITEM_REVISION',401,'Item Revision','ITEM_REVISION','','','','MR020532','VARCHAR2','','','Item Revision','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PROJECT_NUMBER',401,'Project Number','PROJECT_NUMBER','','','','MR020532','VARCHAR2','','','Project Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PROJECT_NAME',401,'Project Name','PROJECT_NAME','','','','MR020532','VARCHAR2','','','Project Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','REASON',401,'Reason','REASON','','','','MR020532','VARCHAR2','','','Reason','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_SOURCE_NAME',401,'Transaction Source Name','TRANSACTION_SOURCE_NAME','','','','MR020532','VARCHAR2','','','Transaction Source Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_UOM',401,'Transaction Uom','TRANSACTION_UOM','','','','MR020532','VARCHAR2','','','Transaction Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSFER_SUBINVENTORY',401,'Transfer Subinventory','TRANSFER_SUBINVENTORY','','','','MR020532','VARCHAR2','','','Transfer Subinventory','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSFER_TRANSACTION_ID',401,'Transfer Transaction Id','TRANSFER_TRANSACTION_ID','','','','MR020532','NUMBER','','','Transfer Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','ERROR_EXPLANATION',401,'Error Explanation','ERROR_EXPLANATION','','','','MR020532','VARCHAR2','','','Error Explanation','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','SUPPLIER_LOT',401,'Supplier Lot','SUPPLIER_LOT','','','','MR020532','VARCHAR2','','','Supplier Lot','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','SOURCE_LINE_ID',401,'Source Line Id','SOURCE_LINE_ID','','','','MR020532','NUMBER','','','Source Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PARENT_TRANSACTION_ID',401,'Parent Transaction Id','PARENT_TRANSACTION_ID','','','','MR020532','NUMBER','','','Parent Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','HAOU_ORGANIZATION_ID',401,'Haou Organization Id','HAOU_ORGANIZATION_ID~HAOU_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Haou Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MIL_ORGANIZATION_ID',401,'Mil Organization Id','MIL_ORGANIZATION_ID~MIL_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mil Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT_TRANSACTION_ID',401,'Mmt Transaction Id','MMT_TRANSACTION_ID~MMT_TRANSACTION_ID','','','','MR020532','NUMBER','','','Mmt Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP_ORGANIZATION_ID',401,'Mp Organization Id','MP_ORGANIZATION_ID~MP_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Mp Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSIL_INVENTORY_ITEM_ID',401,'Msil Inventory Item Id','MSIL_INVENTORY_ITEM_ID~MSIL_INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Msil Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSIL_ORGANIZATION_ID',401,'Msil Organization Id','MSIL_ORGANIZATION_ID~MSIL_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Msil Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID~MSI_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MTS_TRANSACTION_SOURCE_TYPE_ID',401,'Mts Transaction Source Type Id','MTS_TRANSACTION_SOURCE_TYPE_ID~MTS_TRANSACTION_SOURCE_TYPE_ID','','','','MR020532','NUMBER','','','Mts Transaction Source Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','OAP_ORGANIZATION_ID',401,'Oap Organization Id','OAP_ORGANIZATION_ID~OAP_ORGANIZATION_ID','','','','MR020532','NUMBER','','','Oap Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PH_PO_HEADER_ID',401,'Ph Po Header Id','PH_PO_HEADER_ID~PH_PO_HEADER_ID','','','','MR020532','NUMBER','','','Ph Po Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PO_LINE_ID',401,'Po Line Id','PO_LINE_ID~PO_LINE_ID','','','','MR020532','NUMBER','','','Po Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_SOURCE_TYPE_NAME',401,'Transaction Source Type Name','TRANSACTION_SOURCE_TYPE_NAME~TRANSACTION_SOURCE_TYPE_NAME','','','','MR020532','VARCHAR2','','','Transaction Source Type Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','DEPARTMENT_ID',401,'Department Id','DEPARTMENT_ID~DEPARTMENT_ID','','','','MR020532','NUMBER','','','Department Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','INVENTORY_LOCATION_ID',401,'Inventory Location Id','INVENTORY_LOCATION_ID~INVENTORY_LOCATION_ID','','','','MR020532','NUMBER','','','Inventory Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','LINE_NUM',401,'Line Num','LINE_NUM~LINE_NUM','','','','MR020532','NUMBER','','','Line Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PO_HEADER_ID',401,'Po Header Id','PO_HEADER_ID~PO_HEADER_ID','','','','MR020532','NUMBER','','','Po Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','RECEIPT_NUM',401,'Receipt Num','RECEIPT_NUM~RECEIPT_NUM','','','','MR020532','VARCHAR2','','','Receipt Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','SHIPMENT_HEADER_ID',401,'Shipment Header Id','SHIPMENT_HEADER_ID~SHIPMENT_HEADER_ID','','','','MR020532','NUMBER','','','Shipment Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','SHIPMENT_NUM',401,'Shipment Num','SHIPMENT_NUM~SHIPMENT_NUM','','','','MR020532','VARCHAR2','','','Shipment Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_ACTION_ID',401,'Transaction Action Id','TRANSACTION_ACTION_ID~TRANSACTION_ACTION_ID','','','','MR020532','NUMBER','','','Transaction Action Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_SET_ID',401,'Transaction Set Id','TRANSACTION_SET_ID~TRANSACTION_SET_ID','','','','MR020532','NUMBER','','','Transaction Set Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_SOURCE_ID',401,'Transaction Source Id','TRANSACTION_SOURCE_ID~TRANSACTION_SOURCE_ID','','','','MR020532','NUMBER','','','Transaction Source Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_TYPE_ID',401,'Transaction Type Id','TRANSACTION_TYPE_ID~TRANSACTION_TYPE_ID','','','','MR020532','NUMBER','','','Transaction Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','VENDOR_ID',401,'Vendor Id','VENDOR_ID~VENDOR_ID','','','','MR020532','NUMBER','','','Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','SHIPMENT_NUMBER',401,'Shipment Number','SHIPMENT_NUMBER','','','','MR020532','VARCHAR2','','','Shipment Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','WAYBILL_AIRBILL',401,'Waybill Airbill','WAYBILL_AIRBILL','','','','MR020532','VARCHAR2','','','Waybill Airbill','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','FREIGHT_CODE',401,'Freight Code','FREIGHT_CODE','','','','MR020532','VARCHAR2','','','Freight Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','NUMBER_OF_CONTAINERS',401,'Number Of Containers','NUMBER_OF_CONTAINERS','','','','MR020532','NUMBER','','','Number Of Containers','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','RCV_TRANSACTION_ID',401,'Rcv Transaction Id','RCV_TRANSACTION_ID','','','','MR020532','NUMBER','','','Rcv Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MOVE_TRANSACTION_ID',401,'Move Transaction Id','MOVE_TRANSACTION_ID','','','','MR020532','NUMBER','','','Move Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','COMPLETION_TRANSACTION_ID',401,'Completion Transaction Id','COMPLETION_TRANSACTION_ID','','','','MR020532','NUMBER','','','Completion Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','OPERTION_SEQUENCE',401,'Opertion Sequence','OPERTION_SEQUENCE','','','','MR020532','NUMBER','','','Opertion Sequence','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','EXPENDITURE_TYPE',401,'Expenditure Type','EXPENDITURE_TYPE','','','','MR020532','VARCHAR2','','','Expenditure Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','MR020532','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','MR020532','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TRANSACTION_SOURCE_TYPE_ID',401,'Transaction Source Type Id','TRANSACTION_SOURCE_TYPE_ID','','','','MR020532','NUMBER','','','Transaction Source Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','REASON_ID',401,'Reason Id','REASON_ID','','','','MR020532','NUMBER','','','Reason Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','TASK_ID',401,'Task Id','TASK_ID','','','','MR020532','NUMBER','','','Task Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PROJECT_ID',401,'Project Id','PROJECT_ID','','','','MR020532','NUMBER','','','Project Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MIL#101#STOCKLOCATIONS',401,'Key Flex Field Column - MIL#101#STOCKLOCATIONS','MIL#101#STOCKLOCATIONS','','','','MR020532','VARCHAR2','MTL_ITEM_LOCATIONS','SEGMENT1','Stock Locations#101','','1015063','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MIL#101#STOCKLOCATIONS_DESC',401,'Key Flex Field Column - MIL#101#STOCKLOCATIONS_DESC','MIL#101#STOCKLOCATIONS_DESC','','','','MR020532','VARCHAR2','MTL_ITEM_LOCATIONS','SEGMENT1','Stock Locations#101#DESC','','1015063','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#HOME_BRANCH',401,'Descriptive flexfield: Transaction history Column Name: Home Branch Context: WC Rental Asset Transfer','MMT#WCRentalA#Home_Branch','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE1','Mmt#Wc Rental Asset Transfer#Home Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#VENDOR',401,'Descriptive flexfield: Transaction history Column Name: Vendor Context: WC Rental Asset Transfer','MMT#WCRentalA#Vendor','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE10','Mmt#Wc Rental Asset Transfer#Vendor','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#PO_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: PO Number Context: WC Rental Asset Transfer','MMT#WCRentalA#PO_Number','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE11','Mmt#Wc Rental Asset Transfer#Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#ITEM_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: Item Number Context: WC Rental Asset Transfer','MMT#WCRentalA#Item_Number','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE12','Mmt#Wc Rental Asset Transfer#Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#CER_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: CER Number Context: WC Rental Asset Transfer','MMT#WCRentalA#CER_Number','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE13','Mmt#Wc Rental Asset Transfer#Cer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#MISC_ISSUE_TRA',401,'Descriptive flexfield: Transaction history Column Name: Misc Issue Transaction ID Context: WC Rental Asset Transfer','MMT#WCRentalA#Misc_Issue_Tra','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE14','Mmt#Wc Rental Asset Transfer#Misc Issue Transaction Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#CUSTODIAN_BRAN',401,'Descriptive flexfield: Transaction history Column Name: Custodian Branch Context: WC Rental Asset Transfer','MMT#WCRentalA#Custodian_Bran','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE2','Mmt#Wc Rental Asset Transfer#Custodian Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#MAJOR_CATEGORY',401,'Descriptive flexfield: Transaction history Column Name: Major Category Context: WC Rental Asset Transfer','MMT#WCRentalA#Major_Category','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE3','Mmt#Wc Rental Asset Transfer#Major Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#MINOR_CATEGORY',401,'Descriptive flexfield: Transaction history Column Name: Minor Category Context: WC Rental Asset Transfer','MMT#WCRentalA#Minor_Category','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE4','Mmt#Wc Rental Asset Transfer#Minor Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#STATE',401,'Descriptive flexfield: Transaction history Column Name: State Context: WC Rental Asset Transfer','MMT#WCRentalA#State','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE5','Mmt#Wc Rental Asset Transfer#State','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#CITY',401,'Descriptive flexfield: Transaction history Column Name: City Context: WC Rental Asset Transfer','MMT#WCRentalA#City','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE6','Mmt#Wc Rental Asset Transfer#City','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRENTALA#TRANSFER_TO_MA',401,'Descriptive flexfield: Transaction history Column Name: Transfer to MassAdd Context: WC Rental Asset Transfer','MMT#WCRentalA#Transfer_to_Ma','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE9','Mmt#Wc Rental Asset Transfer#Transfer To Massadd','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRETURNT#SUPPLIER_NUMBE',401,'Descriptive flexfield: Transaction history Column Name: Supplier Number Context: WC Return to Vendor','MMT#WCReturnt#Supplier_Numbe','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE1','Mmt#Wc Return To Vendor#Supplier Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRETURNT#RETURN_NUMBER',401,'Descriptive flexfield: Transaction history Column Name: Return Number Context: WC Return to Vendor','MMT#WCReturnt#Return_Number','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE2','Mmt#Wc Return To Vendor#Return Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MMT#WCRETURNT#RETURN_UNIT_PR',401,'Descriptive flexfield: Transaction history Column Name: Return Unit Price Context: WC Return to Vendor','MMT#WCReturnt#Return_Unit_Pr','','','','MR020532','VARCHAR2','MTL_MATERIAL_TRANSACTIONS','ATTRIBUTE3','Mmt#Wc Return To Vendor#Return Unit Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#FACTORY_PLANNER_DATA_DIRE',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Data Directory','MP#Factory_Planner_Data_Dire','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE1','Mp#Factory Planner Data Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#FRU',401,'Descriptive flexfield: Organization parameters Column Name: FRU','MP#FRU','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE10','Mp#Fru','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#LOCATION_NUMBER',401,'Descriptive flexfield: Organization parameters Column Name: Location Number','MP#Location_Number','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE11','Mp#Location Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#BRANCH_OPERATIONS_MANAGER',401,'Descriptive flexfield: Organization parameters Column Name: Branch Operations Manager','MP#Branch_Operations_Manager','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE13','Mp#Branch Operations Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#DELIVER_CHARGE',401,'Descriptive flexfield: Organization parameters Column Name: Deliver Charge','MP#Deliver_Charge','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE14','Mp#Deliver Charge','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#FACTORY_PLANNER_EXECUTABL',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Executable Directory','MP#Factory_Planner_Executabl','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE2','Mp#Factory Planner Executable Directory','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#FACTORY_PLANNER_USER',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner User','MP#Factory_Planner_User','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE3','Mp#Factory Planner User','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#FACTORY_PLANNER_HOST',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Host','MP#Factory_Planner_Host','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE4','Mp#Factory Planner Host','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#FACTORY_PLANNER_PORT_NUMB',401,'Descriptive flexfield: Organization parameters Column Name: Factory Planner Port Number','MP#Factory_Planner_Port_Numb','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE5','Mp#Factory Planner Port Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#PRICING_ZONE',401,'Descriptive flexfield: Organization parameters Column Name: Pricing Zone','MP#Pricing_Zone','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE6','Mp#Pricing Zone','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#ORG_TYPE',401,'Descriptive flexfield: Organization parameters Column Name: Org Type','MP#Org_Type','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE7','Mp#Org Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#DISTRICT',401,'Descriptive flexfield: Organization parameters Column Name: District','MP#District','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE8','Mp#District','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MP#REGION',401,'Descriptive flexfield: Organization parameters Column Name: Region','MP#Region','','','','MR020532','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE9','Mp#Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#HDS#LOB',401,'Descriptive flexfield: Items Column Name: LOB Context: HDS','MSI#HDS#LOB','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',401,'Descriptive flexfield: Items Column Name: Drop Shipment Eligable Context: HDS','MSI#HDS#Drop_Shipment_Eligab','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Hds#Drop Shipment Eligable','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#HDS#INVOICE_UOM',401,'Descriptive flexfield: Items Column Name: Invoice UOM Context: HDS','MSI#HDS#Invoice_UOM','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#HDS#PRODUCT_ID',401,'Descriptive flexfield: Items Column Name: Product ID Context: HDS','MSI#HDS#Product_ID','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#HDS#VENDOR_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: Vendor Part Number Context: HDS','MSI#HDS#Vendor_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#HDS#UNSPSC_CODE',401,'Descriptive flexfield: Items Column Name: UNSPSC Code Context: HDS','MSI#HDS#UNSPSC_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#HDS#UPC_PRIMARY',401,'Descriptive flexfield: Items Column Name: UPC Primary Context: HDS','MSI#HDS#UPC_Primary','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#HDS#SKU_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: SKU Description Context: HDS','MSI#HDS#SKU_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#CA_PROP_65',401,'Descriptive flexfield: Items Column Name: CA Prop 65 Context: WC','MSI#WC#CA_Prop_65','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE1','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#COUNTRY_OF_ORIGIN',401,'Descriptive flexfield: Items Column Name: Country of Origin Context: WC','MSI#WC#Country_of_Origin','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE10','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#ORM_D_FLAG',401,'Descriptive flexfield: Items Column Name: ORM-D Flag Context: WC','MSI#WC#ORM_D_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE11','Msi#Wc#Orm-D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Store Velocity Context: WC','MSI#WC#Store_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE12','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: DC Velocity Context: WC','MSI#WC#DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE13','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#YEARLY_STORE_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly Store Velocity Context: WC','MSI#WC#Yearly_Store_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE14','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#YEARLY_DC_VELOCITY',401,'Descriptive flexfield: Items Column Name: Yearly DC Velocity Context: WC','MSI#WC#Yearly_DC_Velocity','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE15','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#PRISM_PART_NUMBER',401,'Descriptive flexfield: Items Column Name: PRISM Part Number Context: WC','MSI#WC#PRISM_Part_Number','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE16','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#HAZMAT_DESCRIPTION',401,'Descriptive flexfield: Items Column Name: Hazmat Description Context: WC','MSI#WC#Hazmat_Description','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE17','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#HAZMAT_CONTAINER',401,'Descriptive flexfield: Items Column Name: Hazmat Container Context: WC','MSI#WC#Hazmat_Container','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE18','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#GTP_INDICATOR',401,'Descriptive flexfield: Items Column Name: GTP Indicator Context: WC','MSI#WC#GTP_Indicator','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE19','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#LAST_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Last Lead Time Context: WC','MSI#WC#Last_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE2','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#AMU',401,'Descriptive flexfield: Items Column Name: AMU Context: WC','MSI#WC#AMU','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE20','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#RESERVE_STOCK',401,'Descriptive flexfield: Items Column Name: Reserve Stock Context: WC','MSI#WC#Reserve_Stock','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE21','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#TAXWARE_CODE',401,'Descriptive flexfield: Items Column Name: Taxware Code Context: WC','MSI#WC#Taxware_Code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE22','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#AVERAGE_UNITS',401,'Descriptive flexfield: Items Column Name: Average Units Context: WC','MSI#WC#Average_Units','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE25','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#PRODUCT_CODE',401,'Descriptive flexfield: Items Column Name: Product code Context: WC','MSI#WC#Product_code','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE26','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#IMPORT_DUTY_',401,'Descriptive flexfield: Items Column Name: Import Duty % Context: WC','MSI#WC#Import_Duty_','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE27','Msi#Wc#Import Duty %','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#KEEP_ITEM_ACTIVE',401,'Descriptive flexfield: Items Column Name: Keep Item Active Context: WC','MSI#WC#Keep_Item_Active','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE29','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#PESTICIDE_FLAG',401,'Descriptive flexfield: Items Column Name: Pesticide Flag Context: WC','MSI#WC#Pesticide_Flag','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE3','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#CALC_LEAD_TIME',401,'Descriptive flexfield: Items Column Name: Calc Lead Time Context: WC','MSI#WC#Calc_Lead_Time','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE30','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#VOC_GL',401,'Descriptive flexfield: Items Column Name: VOC G/L Context: WC','MSI#WC#VOC_GL','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE4','Msi#Wc#Voc G/L','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#PESTICIDE_FLAG_STATE',401,'Descriptive flexfield: Items Column Name: Pesticide Flag State Context: WC','MSI#WC#Pesticide_Flag_State','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE5','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#VOC_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Category Context: WC','MSI#WC#VOC_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE6','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#VOC_SUB_CATEGORY',401,'Descriptive flexfield: Items Column Name: VOC Sub Category Context: WC','MSI#WC#VOC_Sub_Category','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE7','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#MSDS_#',401,'Descriptive flexfield: Items Column Name: MSDS # Context: WC','MSI#WC#MSDS_#','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE8','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','MSI#WC#HAZMAT_PACKAGING_GROU',401,'Descriptive flexfield: Items Column Name: Hazmat Packaging Group Context: WC','MSI#WC#Hazmat_Packaging_Grou','','','','MR020532','VARCHAR2','MTL_SYSTEM_ITEMS_B','ATTRIBUTE9','Msi#Wc#Hazmat Packaging Group','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PH#STANDARDP#NEED_BY_DATE',401,'Descriptive flexfield: PO Headers Column Name: Need-By Date Context: Standard Purchase Order','PH#StandardP#Need_By_Date','','','','MR020532','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE1','Ph#Standard Purchase Order#Need-By Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PH#STANDARDP#FREIGHT_TERMS_T',401,'Descriptive flexfield: PO Headers Column Name: Freight Terms (TERMS Tab) Context: Standard Purchase Order','PH#StandardP#Freight_Terms_T','','','','MR020532','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE2','Ph#Standard Purchase Order#Freight Terms Terms Tab','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PH#STANDARDP#CARRIER_TERMS_T',401,'Descriptive flexfield: PO Headers Column Name: Carrier (TERMS Tab) Context: Standard Purchase Order','PH#StandardP#Carrier_TERMS_T','','','','MR020532','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE3','Ph#Standard Purchase Order#Carrier Terms Tab','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PH#STANDARDP#FOB_TERMS_TAB',401,'Descriptive flexfield: PO Headers Column Name: FOB (TERMS Tab) Context: Standard Purchase Order','PH#StandardP#FOB_TERMS_Tab','','','','MR020532','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE4','Ph#Standard Purchase Order#Fob Terms Tab','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PV#R11VENDOR#R11_VENDOR_ID',401,'Descriptive flexfield: Vendors Column Name: R11 Vendor ID Context: R11 Vendor ID','PV#R11Vendor#R11_Vendor_ID','','','','MR020532','VARCHAR2','PO_VENDORS','ATTRIBUTE15','Pv#R11 Vendor Id#R11 Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','PV#TAXEXEMPT#EXEMPTIONS',401,'Descriptive flexfield: Vendors Column Name: Exemptions Context: Tax Exempt Flag','PV#TaxExempt#Exemptions','','','','MR020532','VARCHAR2','PO_VENDORS','ATTRIBUTE1','Pv#Tax Exempt Flag#Exemptions','','','');
xxeis.eis_rs_ins.vc( 'EIS_INV_PO_TRANSACTIONS_V','RSH#PRINTED',401,'Descriptive flexfield: RCV_SHIPMENT_HEADERS Column Name: Printed','RSH#Printed','','','','MR020532','VARCHAR2','RCV_SHIPMENT_HEADERS','ATTRIBUTE1','Rsh#Printed','','','');
--Inserting View Components for EIS_INV_PO_TRANSACTIONS_V
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','PO_LINES',401,'PO_LINES_ALL','PL','PL','MR020532','MR020532','-1','Purchase Document Lines (For Purchase Orders, Purc','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','PO_HEADERS',401,'PO_HEADERS_ALL','PH','PH','MR020532','MR020532','-1','Document Headers (For Purchase Orders, Purchase Ag','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','MTL_PARAMETERS',401,'MTL_PARAMETERS','MP','MP','MR020532','MR020532','-1','Inventory Control Options And Defaults','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','MTL_TRANSACTION_REASONS',401,'MTL_TRANSACTION_REASONS','MTR','MTR','MR020532','MR020532','-1','Inventory Transaction Reasons Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','HR_ORGANIZATION_UNITS',401,'HR_ALL_ORGANIZATION_UNITS','HAOU','HAOU','MR020532','MR020532','-1','HR_ALL_ORGANIZATION_UNITS','N','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','MTL_TXN_SOURCE_TYPES',401,'MTL_TXN_SOURCE_TYPES','MTS','MTS','MR020532','MR020532','-1','Valid Transaction Source Types','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','MTL_ITEM_LOCATIONS_KFV',401,'MTL_ITEM_LOCATIONS','MIL','MIL','MR020532','MR020532','-1','Item Locations','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','PA_PROJECTS',401,'PA_PROJECTS_ALL','PP','PP','MR020532','MR020532','-1','Information About Projects','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','MTL_TRANSACTION_TYPES',401,'MTL_TRANSACTION_TYPES','MTT','MTT','MR020532','MR020532','-1','Inventory Transaction Types Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','PA_TASKS',401,'PA_TASKS','PT','PT','MR020532','MR020532','-1','User-Defined Subdivisions Of Project Work','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','ORG_ACCT_PERIODS',401,'ORG_ACCT_PERIODS','OAP','OAP','MR020532','MR020532','-1','Organization Accounting Period Definition Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','MR020532','MR020532','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','MTL_MATERIAL_TRANSACTIONS',401,'MTL_MATERIAL_TRANSACTIONS','MMT','MMT','MR020532','MR020532','-1','Material Transaction Table','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','RCV_SHIPMENT_HEADERS',401,'RCV_SHIPMENT_HEADERS','RSH','RSH','MR020532','MR020532','-1','Shipment And Receipt Header Information','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','RCV_TRANSACTIONS',401,'RCV_TRANSACTIONS','RT','RT','MR020532','MR020532','-1','Receiving Transactions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_INV_PO_TRANSACTIONS_V','PO_VENDORS',401,'PO_VENDORS','PV','PV','MR020532','MR020532','-1','Suppliers','','','','');
--Inserting View Component Joins for EIS_INV_PO_TRANSACTIONS_V
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','PO_LINES','PL',401,'EIPTV1.PO_LINE_ID','=','PL.PO_LINE_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','PO_HEADERS','PH',401,'EIPTV1.PH_PO_HEADER_ID','=','PH.PO_HEADER_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','MTL_PARAMETERS','MP',401,'EIPTV1.MP_ORGANIZATION_ID','=','MP.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','MTL_TRANSACTION_REASONS','MTR',401,'EIPTV1.REASON_ID','=','MTR.REASON_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','HR_ORGANIZATION_UNITS','HAOU',401,'EIPTV1.HAOU_ORGANIZATION_ID','=','HAOU.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','MTL_TXN_SOURCE_TYPES','MTS',401,'EIPTV1.MTS_TRANSACTION_SOURCE_TYPE_ID','=','MTS.TRANSACTION_SOURCE_TYPE_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIPTV1.INVENTORY_LOCATION_ID','=','MIL.INVENTORY_LOCATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','MTL_ITEM_LOCATIONS_KFV','MIL',401,'EIPTV1.MIL_ORGANIZATION_ID','=','MIL.ORGANIZATION_ID(+)','','','','','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','PA_PROJECTS','PP',401,'EIPTV1.PROJECT_ID','=','PP.PROJECT_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','MTL_TRANSACTION_TYPES','MTT',401,'EIPTV1.TRANSACTION_TYPE_ID','=','MTT.TRANSACTION_TYPE_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','PA_TASKS','PT',401,'EIPTV1.TASK_ID','=','PT.TASK_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','ORG_ACCT_PERIODS','OAP',401,'EIPTV1.OAP_ORGANIZATION_ID','=','OAP.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','ORG_ACCT_PERIODS','OAP',401,'EIPTV1.ACCT_PERIOD_ID','=','OAP.ACCT_PERIOD_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EIPTV1.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EIPTV1.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','MTL_MATERIAL_TRANSACTIONS','MMT',401,'EIPTV1.MMT_TRANSACTION_ID','=','MMT.TRANSACTION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','RCV_SHIPMENT_HEADERS','RSH',401,'EIPTV1.SHIPMENT_HEADER_ID','=','RSH.SHIPMENT_HEADER_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','RCV_TRANSACTIONS','RT',401,'EIPTV1.RCV_TRANSACTION_ID','=','RT.TRANSACTION_ID(+)','','','','Y','MR020532','','');
xxeis.eis_rs_ins.vcj( 'EIS_INV_PO_TRANSACTIONS_V','PO_VENDORS','PV',401,'EIPTV1.VENDOR_ID','=','PV.VENDOR_ID(+)','','','','Y','MR020532','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Purchase Order Transactions
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Purchase Order Transactions
xxeis.eis_rs_ins.lov( 401,'SELECT DISTINCT concatenated_segments item, description FROM mtl_system_items_kfv msi,
                org_organization_definitions ood
          WHERE msi.organization_id = ood.organization_id
            AND ood.operating_unit = fnd_profile.VALUE (''ORG_ID'')
       ORDER BY concatenated_segments','','EIS_INV_ITEM_LOV','List of all inventory items.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select SECONDARY_INVENTORY_NAME SUB_INVENTORY,HAOU.NAME ORGANIZATION_NAME
from MTL_SECONDARY_INVENTORIES MSI,
     HR_ALL_ORGANIZATION_UNITS HAOU
WHERE 1=1
AND MSI.ORGANIZATION_ID = HAOU.organization_id','','EIS_INV_SUBINVENTORY_LOV','List of All SubInventories.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select vendor_name
from po_vendors
order by  vendor_name
','','EIS_INV_VENDOR_NAME_LOV','List all the Vendors','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'select segment1 vendor_number from po_vendors','','EIS_INV_VENDOR_NUM_LOV','List the Vendor Numbers','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 401,'SELECT   transaction_type_name
    FROM mtl_transaction_types
   WHERE transaction_source_type_id = 1
ORDER BY transaction_type_name','','INV_TRANSACTION_TYPES_PO','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT   organization_name,organization_code
    FROM org_organization_definitions
   where operating_unit = fnd_profile.value (''ORG_ID'')
union
SELECT ''All'' organization_code, ''All Organizations'' organization_name  FROM dual','','EIS XXWC ORG NAME','LOV to Retrive Organization Name','MR020532',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Purchase Order Transactions
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Purchase Order Transactions
xxeis.eis_rs_utility.delete_report_rows( 'Purchase Order Transactions' );
--Inserting Report - Purchase Order Transactions
xxeis.eis_rs_ins.r( 401,'Purchase Order Transactions','','The Purchase Order Transaction Report displays the material transactions of receipts from a Purchase Order into stores,  return to vendor by issuing from stores or a PO delivery adjustment.','','','','MR020532','EIS_INV_PO_TRANSACTIONS_V','Y','','','MR020532','','N','Transaction Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Purchase Order Transactions
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'UNIT_COST','Unit Cost','Unit Cost','','','','','29','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'VALUE','Value','Value','','','','','30','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'LANGUAGE','Language','Language','','','','','27','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'CREATION_DATE','Creation Date','Creation Date','','','','','8','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'GL_PERIOD','Gl Period','Gl Period','','','','','9','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'ACCT_PERIOD_ID','Acct Period Id','Acct Period Id','','','','','28','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'GL_PERIOD_START_DATE','Gl Period Start Date','Gl Period Start Date','','','','','10','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'INV_ORG_NAME','Inv Org Name','Inv Org Name','','','','','4','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'ITEM','Item','Item','','','','','1','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','2','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'ITEM_REVISION','Item Revision','Item Revision','','','','','11','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'PO_LINE_NUMBER','Po Line Number','Po Line Number','','','','','12','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'PO_NUMBER','PO Number','Po Number','','','','','6','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'PROJECT_NAME','Project Name','Project Name','','','','','13','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'PROJECT_NUMBER','Project Number','Project Number','','','','','14','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'QUANTITY','Quantity','Quantity','','~T~D~2','','','15','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'REASON','Reason','Reason','','','','','16','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'RECEIPT_NUMBER','Receipt Number','Receipt Number','','','','','17','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'STOCK_LOCATORS','Stock Locators','Stock Locators','','','','','5','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'SUBINVENTORY','Subinventory','Subinventory','','','','','3','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'TASK_NAME','Task Name','Task Name','','','','','18','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'TASK_NUMBER','Task Number','Task Number','','','','','19','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'TRANSACTION_ACTION','Transaction Action','Transaction Action','','','','','20','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'TRANSACTION_DATE','Transaction Date','Transaction Date','','','','','21','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'TRANSACTION_REFERENCE','Transaction Reference','Transaction Reference','','','','','22','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'TRANSACTION_TYPE','Transaction Type','Transaction Type','','','','','7','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'UNIT_OF_MEASURE','Unit Of Measure','Unit Of Measure','','','','','23','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'VALUED_FLAG','Valued Flag','Valued Flag','','','','','24','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','25','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
xxeis.eis_rs_ins.rc( 'Purchase Order Transactions',401,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','','','26','N','','','','','','','','MR020532','N','N','','EIS_INV_PO_TRANSACTIONS_V','','');
--Inserting Report Parameters - Purchase Order Transactions
xxeis.eis_rs_ins.rp( 'Purchase Order Transactions',401,'Subinventory','Subinventory','SUBINVENTORY','IN','EIS_INV_SUBINVENTORY_LOV','','VARCHAR2','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Purchase Order Transactions',401,'Transaction Start Date','Transaction Start Date','TRANSACTION_DATE','>=','','','DATE','N','Y','7','','Y','CONSTANT','MR020532','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Purchase Order Transactions',401,'Transaction Type','Transaction Type','TRANSACTION_TYPE','IN','INV_TRANSACTION_TYPES_PO','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Purchase Order Transactions',401,'Transaction End Date','Transaction End Date','TRANSACTION_DATE','<=','','','DATE','N','Y','8','','Y','CONSTANT','MR020532','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Purchase Order Transactions',401,'Vendor Name','Vendor Name','VENDOR_NAME','IN','EIS_INV_VENDOR_NAME_LOV','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Purchase Order Transactions',401,'Vendor Number','Vendor Number','VENDOR_NUMBER','IN','EIS_INV_VENDOR_NUM_LOV','','VARCHAR2','N','Y','6','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Purchase Order Transactions',401,'Organization','Organization','INV_ORG_NAME','IN','EIS XXWC ORG NAME','','VARCHAR2','N','Y','1','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Purchase Order Transactions',401,'Inventory Item','Inventory Item','ITEM','IN','EIS_INV_ITEM_LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Purchase Order Transactions
xxeis.eis_rs_ins.rcn( 'Purchase Order Transactions',401,'ITEM','IN',':Inventory Item','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Purchase Order Transactions',401,'VENDOR_NAME','IN',':Vendor Name','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Purchase Order Transactions',401,'VENDOR_NUMBER','IN',':Vendor Number','','','Y','6','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Purchase Order Transactions',401,'SUBINVENTORY','IN',':Subinventory','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Purchase Order Transactions',401,'TRANSACTION_DATE','>=',':Transaction Start Date','','','Y','7','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Purchase Order Transactions',401,'TRANSACTION_TYPE','IN',':Transaction Type','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Purchase Order Transactions',401,'TRANSACTION_DATE','<=',':Transaction End Date','','','Y','8','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Purchase Order Transactions',401,'','','','','AND ((DECODE(:Organization, NULL,''All'',:Organization) in (''All'')) OR (inv_org_name in (:Organization)))','Y','0','','MR020532');
--Inserting Report Sorts - Purchase Order Transactions
--Inserting Report Triggers - Purchase Order Transactions
--Inserting Report Templates - Purchase Order Transactions
xxeis.eis_rs_ins.R_Tem( 'Purchase Order Transactions','Purchase Order Transactions','The Purchase Order Transaction Report displays the material transactions of receipts from a Purchase Order into stores,  return to vendor by issuing from stores or a PO delivery adjustment.','','','','','','','','','','','','MR020532');
--Inserting Report Portals - Purchase Order Transactions
--Inserting Report Dashboards - Purchase Order Transactions
--Inserting Report Security - Purchase Order Transactions
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','20634',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50851',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50619',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50882',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50883',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50981',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50855',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50884',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','20005','','50900',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50895',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50865',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50864',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','401','','50849',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','660','','50871',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','707','','51104',401,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Purchase Order Transactions','201','','50892',401,'MR020532','','');
--Inserting Report Pivots - Purchase Order Transactions
xxeis.eis_rs_ins.rpivot( 'Purchase Order Transactions',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','VALUE','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','INV_ORG_NAME','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','ITEM','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','ITEM_DESCRIPTION','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','PO_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','QUANTITY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','REASON','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','RECEIPT_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','SUBINVENTORY','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','TRANSACTION_ACTION','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','TRANSACTION_DATE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','TRANSACTION_TYPE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Purchase Order Transactions',401,'Pivot','VENDOR_NAME','PAGE_FIELD','','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
