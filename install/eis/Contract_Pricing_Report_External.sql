--Report Name            : Contract Pricing Report - External
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_CONT_MODI_PRICING_V1
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_CONT_MODI_PRICING_V1
xxeis.eis_rsc_ins.v( 'EIS_XXWC_CONT_MODI_PRICING_V1',660,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Cont Modi Pricing V','EXCMPV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_CONT_MODI_PRICING_V1
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_CONT_MODI_PRICING_V1',660,FALSE);
--Inserting Object Columns for EIS_XXWC_CONT_MODI_PRICING_V1
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CURRENT_LOCATION_COST',660,'Current Location Cost','CURRENT_LOCATION_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Current Location Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','ACTUAL_GROSS_MARGIN',660,'Actual Gross Margin','ACTUAL_GROSS_MARGIN','','','','ANONYMOUS','NUMBER','','','Actual Gross Margin','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','ACTUAL_SALES',660,'Actual Sales','ACTUAL_SALES','','','','ANONYMOUS','NUMBER','','','Actual Sales','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','UNITS_SOLD',660,'Units Sold','UNITS_SOLD','','~T~D~2','','ANONYMOUS','NUMBER','','','Units Sold','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CONTRACT_PRICE',660,'Contract Price','CONTRACT_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Contract Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','APP',660,'App','APP','','','','ANONYMOUS','VARCHAR2','','','App','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','LIST_PRICE',660,'List Price','LIST_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','List Price','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','GM',660,'Gm','GM','','','','ANONYMOUS','NUMBER','','','Gm','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CAT_CLASS',660,'Cat Class','CAT_CLASS','','','','ANONYMOUS','VARCHAR2','','','Cat Class','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','DESCRIPTION',660,'Description','DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','PRODUCT',660,'Product','PRODUCT','','','','ANONYMOUS','VARCHAR2','','','Product','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','ANONYMOUS','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','MODIFIER_NAME',660,'Modifier Name','MODIFIER_NAME','','','','ANONYMOUS','VARCHAR2','','','Modifier Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','VALUE1',660,'Value1','VALUE1','','','','ANONYMOUS','NUMBER','','','Value1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CAT',660,'Cat','CAT','','','','ANONYMOUS','VARCHAR2','','','Cat','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CAT_CLASS_DESC',660,'Cat Class Desc','CAT_CLASS_DESC','','','','ANONYMOUS','VARCHAR2','','','Cat Class Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CAT_DESC',660,'Cat Desc','CAT_DESC','','','','ANONYMOUS','VARCHAR2','','','Cat Desc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CONTRACT_TYPE',660,'Contract Type','CONTRACT_TYPE','','','','ANONYMOUS','VARCHAR2','','','Contract Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','SALESREP',660,'Salesrep','SALESREP','','','','ANONYMOUS','VARCHAR2','','','Salesrep','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','ACCOUNT_NUMBER',660,'Account Number','ACCOUNT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Account Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','LOCATION',660,'Location','LOCATION','','','','ANONYMOUS','VARCHAR2','','','Location','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','PARTY_SITE_NUMBER',660,'Party Site Number','PARTY_SITE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Party Site Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','PRICE_TYPE',660,'Price Type','PRICE_TYPE','','','','ANONYMOUS','VARCHAR2','','','Price Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','VERSION_NO',660,'Version No','VERSION_NO','','','','ANONYMOUS','VARCHAR2','','','Version No','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','NEW_PRICE_OR_NEW_GM_DISC',660,'New Price Or New Gm Disc','NEW_PRICE_OR_NEW_GM_DISC','','~T~D~2','','ANONYMOUS','NUMBER','','','New Price Or New Gm Disc','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','QUALIFIER_ATTRIBUTE',660,'Qualifier Attribute','QUALIFIER_ATTRIBUTE','','','','ANONYMOUS','VARCHAR2','','','Qualifier Attribute','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','TYPE',660,'Type','TYPE','','','','ANONYMOUS','VARCHAR2','','','Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','UNIT_COST',660,'Unit Cost','UNIT_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Cost','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','ARITHMETIC_OPERATOR',660,'Arithmetic Operator','ARITHMETIC_OPERATOR','','','','ANONYMOUS','VARCHAR2','','','Arithmetic Operator','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CATEGORY_ID',660,'Category Id','CATEGORY_ID','','','','ANONYMOUS','NUMBER','','','Category Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','ANONYMOUS','VARCHAR2','','','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','','','Cust Account Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','FORMULA_NAME',660,'Formula Name','FORMULA_NAME','','','','ANONYMOUS','VARCHAR2','','','Formula Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','LIST_HEADER_ID',660,'List Header Id','LIST_HEADER_ID','','','','ANONYMOUS','NUMBER','','','List Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','OPERAND',660,'Operand','OPERAND','','','','ANONYMOUS','NUMBER','','','Operand','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','PRICE_BY_CAT',660,'Price By Cat','PRICE_BY_CAT','','','','ANONYMOUS','VARCHAR2','','','Price By Cat','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','PRODUCT_ATTRIBUTE_TYPE',660,'Product Attribute Type','PRODUCT_ATTRIBUTE_TYPE','','','','ANONYMOUS','VARCHAR2','','','Product Attribute Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','TRANSACTION_CODE',660,'Transaction Code','TRANSACTION_CODE','','','','ANONYMOUS','VARCHAR2','','','Transaction Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','PERSON_ID',660,'Person Id','PERSON_ID','','','','ANONYMOUS','NUMBER','','','Person Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CONT_MODI_PRICING_V1','COMMON_OUTPUT_ID',660,'Common Output Id','COMMON_OUTPUT_ID','','','','ANONYMOUS','NUMBER','','','Common Output Id','','','','US','');
--Inserting Object Components for EIS_XXWC_CONT_MODI_PRICING_V1
--Inserting Object Component Joins for EIS_XXWC_CONT_MODI_PRICING_V1
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Contract Pricing Report - External
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Contract Pricing Report - External
xxeis.eis_rsc_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct organization_code org_code,organization_name name,organization_id from org_organization_definitions','','Organization Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ''Include'' Inc_Excl FROM dual
union
SELECT ''Exclude'' Inc_Excl FROM dual
','','Include_Exclude Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct jrse.source_name
from  jtf_rs_salesreps jrs ,
  jtf_rs_resource_extns jrse
 where  jrs.resource_id                    = jrse.resource_id','','District Manager Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct jrse.source_name SalesRep
from  jtf_rs_salesreps jrs ,
 jtf_rs_resource_extns jrse
 where jrs.resource_id  = jrse.resource_id','','XXWC Regional Manager Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct jrse.source_name  from
jtf_rs_salesreps jrs,
jtf_rs_resource_extns jrse
where    jrs.resource_id                    = jrse.resource_id','','XXWC Sales Manager Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT NAME  MODIFIER_NAME , DESCRIPTION MODIFIER_DESCRIPTION
  FROM QP_LIST_HEADERS QH
 WHERE QH.attribute10   = ''Contract Pricing''','','XXWC CSP Modifer Name','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Contract Pricing Report - External
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Contract Pricing Report - External
xxeis.eis_rsc_utility.delete_report_rows( 'Contract Pricing Report - External',660 );
--Inserting Report - Contract Pricing Report - External
xxeis.eis_rsc_ins.r( 660,'Contract Pricing Report - External','','Contract Pricing Report - External','','','','XXEIS_RS_ADMIN','EIS_XXWC_CONT_MODI_PRICING_V1','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','N','','US','','','','','','','  7#$# or 10#$# or 11#$# or 12#$# ','','','','','','','','','');
--Inserting Report Columns - Contract Pricing Report - External
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'ACTUAL_SALES','ACTUAL SALES','Actual Sales','','~T~D~2','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'PRODUCT','ITEM','Product','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'UNITS_SOLD','UNITS SOLD','Units Sold','','~T~D~2','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'VALUE1','VALUE','Value1','','~T~D~2','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'APP','MODIFIER TYPE','App','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'CAT_CLASS','CAT CLASS','Cat Class','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'CONTRACT_PRICE','CONTRACT SELLING PRICE','Contract Price','','~T~D~2','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'CUSTOMER_NAME','CUSTOMER NAME','Customer Name','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'DESCRIPTION','ITEM DESCRIPTION','Description','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'MODIFIER_NAME','MODIFIER NAME','Modifier Name','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'CAT','CAT','Cat','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'CAT_CLASS_DESC','CAT CLASS DESCRIPTION','Cat Class Desc','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'CAT_DESC','CATEGORY DESCRIPTION','Cat Desc','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'CONTRACT_TYPE','AGREEMENT TYPE','Contract Type','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'SALESREP','SALESPERSON NAME','Salesrep','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'ACCOUNT_NUMBER','MASTER ACCOUNT NUMBER','Account Number','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'LOCATION','LOCATION NAME','Location','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'PARTY_SITE_NUMBER','SITE NUMBER','Party Site Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'PRICE_TYPE','PRICE TYPE','Price Type','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Contract Pricing Report - External',660,'VERSION_NO','AGREEMENT NUMBER','Version No','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_PRICING_V1','','','GROUP_BY','US','','');
--Inserting Report Parameters - Contract Pricing Report - External
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'Customer Name','Customer Name','','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','10','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'Customer Number','Customer Number','','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','11','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'Start Date','Start Date','','IN','','','DATE','Y','Y','1','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'End Date','End Date','','IN','','','DATE','Y','Y','2','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'Cat/Class Priced Lines','Category Priced Items','','IN','Include_Exclude Lov','','VARCHAR2','Y','Y','4','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'RVP','RVP','','IN','XXWC Regional Manager Lov','','VARCHAR2','N','Y','5','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'DM','DM','','IN','District Manager Lov','','VARCHAR2','N','Y','6','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'SalesPerson','SalesPerson','','IN','XXWC Sales Manager Lov','','VARCHAR2','N','Y','7','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'Master Accounts','Master Accounts','','IN','Include_Exclude Lov','','VARCHAR2','Y','Y','8','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'Job Accounts','Job Accounts','','IN','Include_Exclude Lov','','VARCHAR2','Y','Y','9','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'Current Gross Margin Cost Loc','Current Gross Margin Cost Loc','','IN','Organization Lov','','VARCHAR2','Y','Y','3','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
xxeis.eis_rsc_ins.rp( 'Contract Pricing Report - External',660,'Modifier Name','Modifier Name','','IN','XXWC CSP Modifer Name','','VARCHAR2','N','Y','12','Y','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','US','');
--Inserting Dependent Parameters - Contract Pricing Report - External
--Inserting Report Conditions - Contract Pricing Report - External
xxeis.eis_rsc_ins.rcnh( 'Contract Pricing Report - External',660,'PROCESS_ID IN :SYSTEM.PROCESS_ID ','ADVANCED','','1#$#','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','PROCESS_ID','','','','','','','EIS_XXWC_CONT_MODI_PRICING_V1','','','','','','IN','Y','N','',':SYSTEM.PROCESS_ID','','','1',660,'Contract Pricing Report - External','PROCESS_ID IN :SYSTEM.PROCESS_ID ');
--Inserting Report Sorts - Contract Pricing Report - External
xxeis.eis_rsc_ins.rs( 'Contract Pricing Report - External',660,'CUSTOMER_NAME','ASC','XXEIS_RS_ADMIN','1','');
xxeis.eis_rsc_ins.rs( 'Contract Pricing Report - External',660,'MODIFIER_NAME','ASC','XXEIS_RS_ADMIN','2','');
--Inserting Report Triggers - Contract Pricing Report - External
xxeis.eis_rsc_ins.rt( 'Contract Pricing Report - External',660,'begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_from(:Start Date);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_to(:End Date);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_cost_location(:Current Gross Margin Cost Loc);
xxeis.EIS_OM_XXWC_CONT_PRIC_PKG.CONTRACT_PRICING_MODIFIER
                                    (
                                      P_PROCESS_ID           =>:SYSTEM.PROCESS_ID,
                                      P_START_DATE            =>:Start Date,
                                      P_END_DATE                =>:End Date,
                                      P_CatClass                   => NULL,
                                      P_CAT_PRICED_ITEMS  =>:Cat/Class Priced Lines,
                                      P_RVP                          =>:RVP,
                                      P_DM                           =>:DM,
                                      P_SALESPERSON           =>:SalesPerson,
                                      P_MASTER_ACCOUNT    =>:Master Accounts,
                                      P_JOB_ACCOUNT           =>:Job Accounts,
                                      P_CUSTOMER_NAME      => :Customer Name,
                                      P_CUSTOMER_NUMBER => :Customer Number,
                                      P_MODIFIER_NAME      => :Modifier Name
                                   );
end;','B','Y','XXEIS_RS_ADMIN','');
--inserting report templates - Contract Pricing Report - External
--Inserting Report Portals - Contract Pricing Report - External
--inserting report dashboards - Contract Pricing Report - External
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Contract Pricing Report - External','660','EIS_XXWC_CONT_MODI_PRICING_V1','EIS_XXWC_CONT_MODI_PRICING_V1','N','');
--inserting report security - Contract Pricing Report - External
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','661','','XXWC_PRICING_MANAGER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','','PP018915','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Contract Pricing Report - External','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - Contract Pricing Report - External
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Contract Pricing Report - External
xxeis.eis_rsc_ins.rv( 'Contract Pricing Report - External','','Contract Pricing Report - External','SA059956','04-SEP-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
