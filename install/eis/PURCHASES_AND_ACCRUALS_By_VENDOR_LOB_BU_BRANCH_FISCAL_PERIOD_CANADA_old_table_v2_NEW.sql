--Report Name            : PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_PUR_ACC_OLD_TABLE_V
xxeis.eis_rs_ins.v( 'EIS_PUR_ACC_OLD_TABLE_V',682,'','','','','PK059658','XXEIS','EIS PUR Acc Old Table V','EPAOTV','','');
--Delete View Columns for EIS_PUR_ACC_OLD_TABLE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_PUR_ACC_OLD_TABLE_V',682,FALSE);
--Inserting View Columns for EIS_PUR_ACC_OLD_TABLE_V
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','TOTAL',682,'Total','TOTAL','','','','PK059658','NUMBER','','','Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','COOP',682,'Coop','COOP','','','','PK059658','NUMBER','','','Coop','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','REBATE',682,'Rebate','REBATE','','','','PK059658','NUMBER','','','Rebate','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','ACCRUAL_PURCHASES',682,'Accrual Purchases','ACCRUAL_PURCHASES','','','','PK059658','NUMBER','','','Accrual Purchases','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','FISCAL_PURCHASES',682,'Fiscal Purchases','FISCAL_PURCHASES','','','','PK059658','NUMBER','','','Fiscal Purchases','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','POSTED_FIN_LOCATION',682,'Posted Fin Location','POSTED_FIN_LOCATION','','','','PK059658','VARCHAR2','','','Posted Fin Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','LOB_BRANCH',682,'Lob Branch','LOB_BRANCH','','','','PK059658','VARCHAR2','','','Lob Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','BU',682,'Bu','BU','','','','PK059658','VARCHAR2','','','Bu','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','LOB',682,'Lob','LOB','','','','PK059658','VARCHAR2','','','Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','VENDOR',682,'Vendor','VENDOR','','','','PK059658','VARCHAR2','','','Vendor','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','CUST_ID',682,'Cust Id','CUST_ID','','','','PK059658','NUMBER','','','Cust Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','PERIOD_ID',682,'Period Id','PERIOD_ID','','','','PK059658','NUMBER','','','Period Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','AGREEMENT_YEAR',682,'Agreement Year','AGREEMENT_YEAR','','','','PK059658','NUMBER','','','Agreement Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','PROCESS_ID',682,'Process Id','PROCESS_ID','','','','PK059658','NUMBER','','','Process Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','FISCAL_PERIOD',682,'Fiscal Period','FISCAL_PERIOD','','','','PK059658','VARCHAR2','','','Fiscal Period','','','');
xxeis.eis_rs_ins.vc( 'EIS_PUR_ACC_OLD_TABLE_V','MVID',682,'Mvid','MVID','','','','PK059658','VARCHAR2','','','Mvid','','','');
--Inserting View Components for EIS_PUR_ACC_OLD_TABLE_V
--Inserting View Component Joins for EIS_PUR_ACC_OLD_TABLE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
xxeis.eis_rs_ins.lov( 682,'select distinct nvl(attribute7, ''NULL'') agreement_year
from apps.QP_LIST_HEADERS_VL','','LOV AGREEMENT_YEAR','LOV attribute7 from apps.QP_LIST_HEADERS_VL  table is the agreement year','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_MVID''','','LOV VENDOR_NAME','VENDOR NAME FROM ar.HZ_PARTIES
','ID020048',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'select distinct period_name
from xla_ae_headers','','LOV PERIOD_NAME','LOV PERIOD NAME FROM xla_ae_headers table','ID020048',NULL,'N','','');
xxeis.eis_rs_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_LOB''
and party_name not in (''PLUMBING null'', ''INDUSTRIAL PVF'')','','HDS LOB_NAME','LOV party_name From ar.HZ_PARTIES is the LOB','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
xxeis.eis_rs_utility.delete_report_rows( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)' );
--Inserting Report - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
xxeis.eis_rs_ins.r( 682,'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)','','','','','','PK059658','EIS_PUR_ACC_OLD_TABLE_V','Y','','','PK059658','','N','ADHOC - SUMMARY ACCRUAL DETAILS','','CSV,EXCEL,','N');
--Inserting Report Columns - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'ACCRUAL_PURCHASES','Accrual Purchases','Accrual Purchases','','','','','11','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'AGREEMENT_YEAR','Agreement Year','Agreement Year','','','','','1','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'BU','Bu','Bu','','','','','7','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'COOP','Coop','Coop','','','','','13','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'FISCAL_PURCHASES','Fiscal Purchases','Fiscal Purchases','','','','','10','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'LOB','Lob','Lob','','','','','6','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'LOB_BRANCH','Lob Branch','Lob Branch','','','','','8','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'PERIOD_ID','Period Id','Period Id','','','','','2','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'POSTED_FIN_LOCATION','Posted Fin Location','Posted Fin Location','','','','','9','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'REBATE','Rebate','Rebate','','','','','12','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'TOTAL','Total','Total','','','','','14','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'VENDOR','Vendor','Vendor','','','','','5','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'FISCAL_PERIOD','Fiscal Period','Fiscal Period','','','','','3','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
xxeis.eis_rs_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'MVID','Mvid','Mvid','','','','','4','N','','','','','','','','PK059658','N','N','','EIS_PUR_ACC_OLD_TABLE_V','','');
--Inserting Report Parameters - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'Agreement Year','Agreement Year','AGREEMENT_YEAR','IN','LOV AGREEMENT_YEAR','','VARCHAR2','N','Y','1','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'Lob','Lob','LOB','IN','HDS LOB_NAME','','VARCHAR2','N','Y','3','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'Lob Branch','Lob Branch','LOB_BRANCH','IN','','','VARCHAR2','N','Y','5','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'Vendor','Vendor','VENDOR','IN','LOV VENDOR_NAME','','VARCHAR2','N','Y','4','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'Fiscal Period','Fiscal Period','FISCAL_PERIOD','IN','LOV PERIOD_NAME','','VARCHAR2','Y','Y','2','','N','CONSTANT','PK059658','Y','N','','','');
--Inserting Report Conditions - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
xxeis.eis_rs_ins.rcn( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'','','','','and PROCESS_ID = :SYSTEM.PROCESS_ID','Y','1','','PK059658');
--Inserting Report Sorts - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
xxeis.eis_rs_ins.rs( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'LOB','ASC','PK059658','2','');
xxeis.eis_rs_ins.rs( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'BU','ASC','PK059658','3','');
xxeis.eis_rs_ins.rs( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'LOB_BRANCH','ASC','PK059658','4','');
--Inserting Report Triggers - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
xxeis.eis_rs_ins.rt( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'BEGIN
xxeis.EIS_XXWC_PURCHASE_ACC_RPT_PKG.GET_ACC_PUR_DTLS (p_process_id  =>  :SYSTEM.PROCESS_ID,
                            p_Lob_Branch 	 =>	:Lob Branch,
                            p_Agreement_Year =>	:Agreement Year,
                            P_FISCAL_PERIOD  =>	:Fiscal Period,
                            P_LOB 	 =>	:Lob,
                            P_VENDOR   =>	:Vendor
	);
END;','B','Y','PK059658');
xxeis.eis_rs_ins.rt( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)',682,'begin
XXEIS.eis_xxwc_purchase_acc_rpt_pkg.CLEAR_TEMP_TABLES(P_PROCESS_ID=> :SYSTEM.PROCESS_ID);
end;','A','Y','PK059658');
--Inserting Report Templates - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
--Inserting Report Portals - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
--Inserting Report Dashboards - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
--Inserting Report Security - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
--Inserting Report Pivots - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA) - old table v2(NEW)
END;
/
set scan on define on
