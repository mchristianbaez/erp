--Report Name            : Orders Exception Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_ORDER_EXP_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_ORDER_EXP_V
Create or replace View XXEIS.EIS_XXWC_OM_ORDER_EXP_V
 AS 
SELECT /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N2) index(ol XXWC_OE_ORDER_LINES_ALL_N3) */
    MP.ORGANIZATION_CODE LOC,
    'Order On Hold' Exception_Resaon,
    oh.ORDER_NUMBER ORDER_NUMBER,
    TRUNC(OH.ORDERED_DATE) ORDER_DATE,
    --OHd.CREATION_DATE EXCEPTION_DATE,
    ooh.CREATION_DATE EXCEPTION_DATE,
    NVL( HCA.ACCOUNT_NAME,HP.PARTY_NAME) CUSTOMER_NAME,
    OTTL.name ORDER_TYPE,
    CASE
      WHEN xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_line_id(ol.line_id) = 1
      THEN ROUND((ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(OL.ORDERED_QUANTITY,0)* NVL(OL.UNIT_SELLING_PRICE,0),2)+ NVL(OL.TAX_VALUE,0)),2)
      ELSE NULL
    END EXT_ORDER_TOTAL,
    DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(OL.ORDERED_QUANTITY,0) QTY,
    ohd.description exception_description,
    REP.NAME SALES_PERSON_NAME,
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_EMPOLYEE_NAME(FU.USER_ID,OH.CREATION_DATE) CREATED_BY,
  nvl(flv.meaning,ol.SHIPPING_METHOD_CODE) ship_method
  FROM oe_order_headers oh,
    oe_order_lines ol,
   -- ORG_ORGANIZATION_DEFINITIONS OOD,
   MTL_PARAMETERS MP,
    HZ_CUST_ACCOUNTS HCA,
    HZ_PARTIES HP,
    oe_transaction_types_vl OTTL,
    OE_ORDER_HOLDS ooh,
    oe_hold_sources ohs,
    OE_HOLD_DEFINITIONS ohd,
    ra_salesreps rep,
  --  PER_PEOPLE_F PPF,
    FND_USER FU,
    FND_LOOKUP_VALUES flv
  where oh.header_id           = ol.header_id
  AND ol.SHIP_FROM_ORG_ID      = MP.ORGANIZATION_ID
  AND oh.ORDER_TYPE_ID         = OTTL.TRANSACTION_TYPE_ID
  AND OH.SALESREP_ID           = REP.SALESREP_ID(+)
  AND OH.ORG_ID                = REP.ORG_ID(+)
  AND oh.SOLD_TO_ORG_ID        = hca.cust_account_id
  AND OL.FLOW_STATUS_CODE NOT IN ('CLOSED','CANCELLED')
  AND ohd.name NOT            IN('Pricing Guardrail Hold','Pricing Guardrail Hold - Limited User', 'XXWC_PRICE_CHANGE_HOLD')
  AND HCA.PARTY_ID             = HP.PARTY_ID
  AND ooh.header_id            = oh.header_id
  AND ol.LINE_ID               = ooh.LINE_ID(+)
  AND ohs.hold_source_id       = ooh.hold_source_id
  AND ooh.hold_release_id     IS NULL
  AND OHS.HOLD_ID              = OHD.HOLD_ID
  AND FU.USER_ID               = OH.CREATED_BY
  AND FLV.LOOKUP_TYPE  (+)           = 'SHIP_METHOD'
 AND FLV.LOOKUP_CODE    (+)         = OL.SHIPPING_METHOD_CODE
 UNION ALL
   SELECT /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N2) index(ol XXWC_OE_ORDER_LINES_ALL_N3) */
    MP.ORGANIZATION_CODE LOC,
    'Order On Hold' Exception_Resaon,
    oh.ORDER_NUMBER ORDER_NUMBER,
    TRUNC(OH.ORDERED_DATE) ORDER_DATE,
    --OHd.CREATION_DATE EXCEPTION_DATE,
    ooh.CREATION_DATE EXCEPTION_DATE,
    NVL( HCA.ACCOUNT_NAME,HP.PARTY_NAME) CUSTOMER_NAME,
    OTTL.name ORDER_TYPE,
    CASE
      WHEN xxeis.EIS_RS_XXWC_COM_UTIL_PKG.get_line_id(ol.line_id) = 1
      THEN ROUND((ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(OL.ORDERED_QUANTITY,0)* NVL(OL.UNIT_SELLING_PRICE,0),2)+ NVL(OL.TAX_VALUE,0)),2)
      ELSE NULL
    END EXT_ORDER_TOTAL,
    DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(OL.ORDERED_QUANTITY,0) QTY,
    ohd.description exception_description,
    REP.NAME SALES_PERSON_NAME,
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_EMPOLYEE_NAME(FU.USER_ID,OH.CREATION_DATE) CREATED_BY,
  nvl(flv.meaning,ol.SHIPPING_METHOD_CODE) ship_method
  FROM oe_order_headers oh,
    oe_order_lines ol,
   -- ORG_ORGANIZATION_DEFINITIONS OOD,
   MTL_PARAMETERS MP,
    HZ_CUST_ACCOUNTS HCA,
    HZ_PARTIES HP,
    oe_transaction_types_vl OTTL,
    OE_ORDER_HOLDS ooh,
    oe_hold_sources ohs,
    OE_HOLD_DEFINITIONS ohd,
    ra_salesreps rep,
  --  PER_PEOPLE_F PPF,
    FND_USER FU,
    FND_LOOKUP_VALUES flv
  where oh.header_id           = ol.header_id
  AND ol.SHIP_FROM_ORG_ID      = MP.ORGANIZATION_ID
  AND oh.ORDER_TYPE_ID         = OTTL.TRANSACTION_TYPE_ID
  AND OH.SALESREP_ID           = REP.SALESREP_ID(+)
  AND OH.ORG_ID                = REP.ORG_ID(+)
  AND oh.SOLD_TO_ORG_ID        = hca.cust_account_id
  AND OL.FLOW_STATUS_CODE NOT IN ('CLOSED','CANCELLED')
  AND ohd.name NOT            IN('Pricing Guardrail Hold','Pricing Guardrail Hold - Limited User', 'XXWC_PRICE_CHANGE_HOLD')
  AND HCA.PARTY_ID             = HP.PARTY_ID
  AND OOH.HEADER_ID            = OH.HEADER_ID
  AND ooh.LINE_ID IS NULL
  AND ohs.hold_source_id       = ooh.hold_source_id
  AND ooh.hold_release_id     IS NULL
  AND OHS.HOLD_ID              = OHD.HOLD_ID
  AND FU.USER_ID               = OH.CREATED_BY
  AND FLV.LOOKUP_TYPE  (+)           = 'SHIP_METHOD'
 AND FLV.LOOKUP_CODE    (+)         = OL.SHIPPING_METHOD_CODE
  UNION ALL
SELECT   /*+ INDEX(ol XXWC_OE_ORDER_LINES_ALL_N3) */
      MP.ORGANIZATION_CODE LOC,
    CASE
      WHEN OTTL.name          ='STANDARD ORDER'
      AND OL.flow_status_code = 'AWAITING_RECEIPT'
      THEN 'Open Direct'
      WHEN OTTL.name          ='RETURN ORDER'
      AND OL.flow_status_code = 'AWAITING_RETURN'
      THEN 'Open RMA'
      WHEN OL.flow_status_code ='PRE-BILLING_ACCEPTANCE'
      THEN 'Pending Pre Billing'
     WHEN (OTTL.NAME = 'COUNTER ORDER'  AND OL.FLOW_STATUS_CODE <>'ENTERED')
     -- THEN 'Booked Not Closed'
      THEN 'Booked Counter Orders > 1-day'
       WHEN (OTTL.NAME = 'COUNTER ORDER' AND OL.FLOW_STATUS_CODE ='ENTERED')
       then 'Entered Not Booked'
      ELSE NULL
    END Exception_Resaon,
    oh.ORDER_NUMBER ORDER_NUMBER,
    TRUNC(oh.ORDERED_DATE) ORDER_DATE,
    CASE
      WHEN OTTL.name          ='STANDARD ORDER'
      AND OL.flow_status_code = 'AWAITING_RECEIPT'
      THEN TRUNC(oh.ORDERED_DATE)
      WHEN OTTL.name          ='RETURN ORDER'
      AND OL.flow_status_code = 'AWAITING_RETURN'
      THEN TRUNC(oh.ORDERED_DATE)
      WHEN OL.flow_status_code ='PRE-BILLING_ACCEPTANCE'
      THEN TRUNC(OL.ACTUAL_SHIPMENT_DATE)
      WHEN (OTTL.name='COUNTER ORDER'  and OL.FLOW_STATUS_CODE <>'ENTERED')
      THEN TRUNC(OH.ORDERED_DATE)+1
      WHEN (OTTL.NAME='COUNTER ORDER'  AND OL.FLOW_STATUS_CODE ='ENTERED')
      THEN TRUNC(OH.ORDERED_DATE)
      ELSE NULL
    END EXCEPTION_DATE,
    NVL( HCA.ACCOUNT_NAME,HP.PARTY_NAME) CUSTOMER_NAME,
    OTTL.name ORDER_TYPE,
    ROUND((ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(OL.ORDERED_QUANTITY,0)* NVL(OL.UNIT_SELLING_PRICE,0),2)+ NVL(OL.TAX_VALUE,0)),2) EXT_ORDER_TOTAL,
    DECODE(ol.line_category_code,'RETURN',              -1,1)* NVL(OL.ORDERED_QUANTITY,0) QTY,
    CASE
      WHEN OTTL.name          ='STANDARD ORDER'
      AND OL.flow_status_code = 'AWAITING_RECEIPT'
      THEN 'Direct Ship waiting to be Received'
      WHEN OTTL.name          ='RETURN ORDER'
      AND OL.flow_status_code = 'AWAITING_RETURN'
      THEN 'RMA waiting to be Received'
      WHEN OL.flow_status_code ='PRE-BILLING_ACCEPTANCE'
      THEN 'Item waiting on Fulfillment Acceptance'
      WHEN (OTTL.name='COUNTER ORDER' and OL.FLOW_STATUS_CODE <>'ENTERED')
      THEN 'Item waiting on Lot Numbers'
      WHEN (OTTL.NAME = 'COUNTER ORDER' AND OL.FLOW_STATUS_CODE ='ENTERED')
       then 'Needs to be Reviewed and Booked'
      ELSE NULL
    END Exception_Description,
    rep.name sales_person_name,
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_EMPOLYEE_NAME(FU.USER_ID,OH.CREATION_DATE) CREATED_BY,
   nvl(flv.meaning,ol.shipping_method_code) ship_method
  FROM oe_order_headers oh,
    oe_order_lines ol,
  MTL_PARAMETERS MP,
    HZ_CUST_ACCOUNTS HCA,
    HZ_PARTIES HP,
    oe_transaction_types_vl OTTL,
    ra_salesreps rep,
   -- PER_PEOPLE_F PPF,
    FND_USER FU,
    FND_LOOKUP_VALUES flv
  where oh.header_id      = ol.HEADER_ID
  AND ol.SHIP_FROM_ORG_ID = mp.ORGANIZATION_ID
  AND oh.ORDER_TYPE_ID    = OTTL.TRANSACTION_TYPE_ID
  AND OH.SALESREP_ID      = REP.SALESREP_ID(+)
  AND OH.ORG_ID           = REP.ORG_ID(+)
  AND oh.SOLD_TO_ORG_ID   = hca.cust_account_id
  AND HCA.PARTY_ID        = HP.PARTY_ID
  AND FU.USER_ID          = OH.CREATED_BY
  AND FLV.LOOKUP_TYPE (+)            = 'SHIP_METHOD'
  AND FLV.LOOKUP_CODE (+)            = OL.SHIPPING_METHOD_CODE
  --and fu.employee_id      = ppf.person_id(+)
 -- AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (oh.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (oh.creation_date) )
  AND OL.FLOW_STATUS_CODE NOT IN ('CLOSED','CANCELLED')
  AND OTTL.name               IN('STANDARD ORDER','COUNTER ORDER','RETURN ORDER','REPAIR ORDER')
  AND ((OTTL.NAME              != 'COUNTER ORDER' AND OL.flow_status_code IN('AWAITING_RECEIPT','AWAITING_RETURN','PRE-BILLING_ACCEPTANCE'))
  OR(OTTL.NAME                 = 'COUNTER ORDER'
  AND TRUNC(SYSDATE)           > TRUNC(ORDERED_DATE) )
  )
  /* � Counter Orders that are not closed, due to a Hold, should only display once under Order On Hold*/
   and not exists( select DISTINCT OOH.HEADER_ID
                from    OE_ORDER_HEADERS OHE,
                        OE_ORDER_LINES OLE,
                        OE_ORDER_HOLDS ooh,
                        OE_HOLD_SOURCES OHS,
                        OE_HOLD_DEFINITIONS OHD,
                        OE_TRANSACTION_TYPES_VL OTTL
                        where OHE.HEADER_ID         = OLE.HEADER_ID
                          and OHE.HEADER_ID         = OH.HEADER_ID
                          and OLE.LINE_ID           = OL.LINE_ID
                          AND OOH.HEADER_ID         = OHE.HEADER_ID
                          --and OLE.LINE_ID           = OOH.LINE_ID(+)
                          AND ohs.hold_source_id    = ooh.hold_source_id
                          and OOH.HOLD_RELEASE_ID   is null
                          and OHS.HOLD_ID           = OHD.HOLD_ID
                          and OHE.ORDER_TYPE_ID     = OTTL.TRANSACTION_TYPE_ID
                          AND OTTL.NAME             = 'COUNTER ORDER'
                          /*and OHD.name not in('Pricing Guardrail Hold',
                                              'Pricing Guardrail Hold - Limited User',
                                                'XXWC_PRICE_CHANGE_HOLD'
                                              )*/
                                              )
  union all
SELECT /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N2) index(ol XXWC_OE_ORDER_LINES_ALL_N3) */
MP.ORGANIZATION_CODE LOC,
    'Out for Delivery' Exception_Resaon,
    oh.ORDER_NUMBER ORDER_NUMBER,
    TRUNC(OH.ORDERED_DATE) ORDER_DATE,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.YY_GET_DOC_DATE(oh.header_id, ol.line_id,ol.INVENTORY_ITEM_ID,ol.ship_from_org_id,oh.ship_from_org_id) EXCEPTION_DATE,
    NVL( HCA.ACCOUNT_NAME,HP.PARTY_NAME) CUSTOMER_NAME,
    OTTL.name ORDER_TYPE,
    ROUND((ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(OL.ORDERED_QUANTITY,0)* NVL(OL.UNIT_SELLING_PRICE,0),2)+ NVL(OL.TAX_VALUE,0)),2) EXT_ORDER_TOTAL,
    DECODE(ol.line_category_code,'RETURN',              -1,1)* NVL(OL.ORDERED_QUANTITY,0) QTY,
    'Item Shipped but Not Confirmed' Exception_Description,
    rep.name sales_person_name,
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_EMPOLYEE_NAME(FU.USER_ID,OH.CREATION_DATE) CREATED_BY,
    nvl(flv.meaning,ol.shipping_method_code) ship_method
  FROM oe_order_headers oh,
    oe_order_lines ol,
    MTL_PARAMETERS MP,
    HZ_CUST_ACCOUNTS HCA,
    HZ_PARTIES HP,
    oe_transaction_types_vl OTTL,
    ra_salesreps rep,
   -- PER_PEOPLE_F PPF,
    FND_USER FU,
      FND_LOOKUP_VALUES flv
  where oh.header_id      = ol.header_id
  AND ol.SHIP_FROM_ORG_ID = MP.ORGANIZATION_ID
  AND oh.ORDER_TYPE_ID    = OTTL.TRANSACTION_TYPE_ID
  AND OH.SALESREP_ID      = REP.SALESREP_ID(+)
  AND OH.ORG_ID           = REP.ORG_ID(+)
  AND oh.SOLD_TO_ORG_ID   = hca.cust_account_id
  AND HCA.PARTY_ID        = HP.PARTY_ID
  AND FU.USER_ID          = OH.CREATED_BY
  AND FLV.LOOKUP_TYPE (+)            = 'SHIP_METHOD'
  AND FLV.LOOKUP_CODE (+)            = OL.SHIPPING_METHOD_CODE
 -- AND FU.EMPLOYEE_ID      = PPF.PERSON_ID(+)
 -- AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (oh.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (oh.creation_date) )
  AND OL.FLOW_STATUS_CODE NOT IN ('CLOSED','CANCELLED')
  AND OTTL.NAME      IN('STANDARD ORDER','RETURN ORDER','REPAIR ORDER')
  AND ol.flow_status_code = 'AWAITING_SHIPPING'
  AND (SYSDATE - NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.YY_GET_DOC_DATE(OH.HEADER_ID, OL.LINE_ID,OL.INVENTORY_ITEM_ID,OL.SHIP_FROM_ORG_ID,OH.SHIP_FROM_ORG_ID),SYSDATE)  > 1 )/
set scan on define on
prompt Creating View Data for Orders Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_ORDER_EXP_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Order Exp V','EXOOEV','','');
--Delete View Columns for EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_ORDER_EXP_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_DESCRIPTION',660,'Exception Description','EXCEPTION_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Exception Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXT_ORDER_TOTAL',660,'Ext Order Total','EXT_ORDER_TOTAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Ext Order Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_DATE',660,'Exception Date','EXCEPTION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Exception Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_RESAON',660,'Exception Resaon','EXCEPTION_RESAON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Exception Resaon','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Person Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','SHIP_METHOD',660,'Ship Method','SHIP_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','LOC',660,'Loc','LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc','','','');
--Inserting View Components for EIS_XXWC_OM_ORDER_EXP_V
--Inserting View Component Joins for EIS_XXWC_OM_ORDER_EXP_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Orders Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Orders Exception Report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS, HR_OPERATING_UNITS OU
WHERE RS.org_id = OU.organization_id
AND RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Orders Exception Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Orders Exception Report
xxeis.eis_rs_utility.delete_report_rows( 'Orders Exception Report' );
--Inserting Report - Orders Exception Report
xxeis.eis_rs_ins.r( 660,'Orders Exception Report','','White Cap Exception Report','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_ORDER_EXP_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Orders Exception Report
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXCEPTION_DATE','Exception Date','Exception Date','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXCEPTION_DESCRIPTION','Exception Description','Exception Description','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXCEPTION_RESAON','Exception Reason','Exception Resaon','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXT_ORDER_TOTAL','Line Total','Ext Order Total','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'ORDER_DATE','Order Date','Order Date','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'CREATED_BY','Created By','Created By','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'QTY','Qty','Qty','','~~~','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'LOC','Loc','Loc','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'SHIP_METHOD','Ship Method','Ship Method','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
--Inserting Report Parameters - Orders Exception Report
xxeis.eis_rs_ins.rp( 'Orders Exception Report',660,'Sales Person Name','Sales Person Name','SALES_PERSON_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders Exception Report',660,'Organization','Organization','LOC','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Orders Exception Report
xxeis.eis_rs_ins.rcn( 'Orders Exception Report',660,'SALES_PERSON_NAME','IN',':Sales Person Name','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Orders Exception Report',660,'','','','','AND ( ''All'' IN (:Organization) OR (Loc IN (:Organization)))','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Orders Exception Report
xxeis.eis_rs_ins.rs( 'Orders Exception Report',660,'EXCEPTION_RESAON','ASC','XXEIS_RS_ADMIN','1','');
--Inserting Report Triggers - Orders Exception Report
--Inserting Report Templates - Orders Exception Report
--Inserting Report Portals - Orders Exception Report
--Inserting Report Dashboards - Orders Exception Report
--Inserting Report Security - Orders Exception Report
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51509',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51045',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51025',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','20005','','50900',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Orders Exception Report
xxeis.eis_rs_ins.rpivot( 'Orders Exception Report',660,'Summary by reason','1','1,0|1,2,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Summary by reason
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','EXT_ORDER_TOTAL','DATA_FIELD','SUM','Ext Total','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','EXCEPTION_RESAON','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','ORDER_NUMBER','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','CREATED_BY','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','QTY','ROW_FIELD','','','4','','');
--Inserting Report Summary Calculation Columns For Pivot- Summary by reason
END;
/
set scan on define on
