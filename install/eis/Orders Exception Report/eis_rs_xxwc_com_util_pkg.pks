create or replace PACKAGE             xxeis.eis_rs_xxwc_com_util_pkg authid current_user
IS
  g_date_from           DATE;
  g_date_to             DATE;
  g_vdate               DATE;
  g_payment_set_id      NUMBER := 0;
  g_cash_date           DATE   := NULL;
  g_organization_id     NUMBER;
  g_cancelled_count     NUMBER;
  g_dispositioned_count NUMBER;
  g_period_name         VARCHAR2 (50);
  g_ledger_id           NUMBER;
  g_report_usage_type   VARCHAR2 (50);
  g_process_id          NUMBER;
  g_report_run_type     VARCHAR2 (100);
  g_period_name_from    VARCHAR2 (100);
  g_period_name_to      VARCHAR2 (100);
  g_payment_type        VARCHAR2 (500);
  g_isr_rpt_dc_mod_sub  VARCHAR2 (50);
  g_isr_rpt_tool_repair VARCHAR2 (50);
  g_start_bin           VARCHAR2 (100);
  g_end_bin             VARCHAR2 (100);
  g_flag_value          VARCHAR2 (1) := 'N';
  g_item_flag           VARCHAR2 (1) := 'N';
  g_cat_flag            VARCHAR2 (1) := 'N';
type view_tab
IS
  TABLE OF NUMBER INDEX BY binary_integer;
type order_count_tab
IS
  TABLE OF NUMBER INDEX BY binary_integer;
  g_order_tab order_count_tab;
  g_order_count NUMBER := 0;
  g_order_id    NUMBER := 0; --Added for Product Fill Rate Report by Order and by Line report use
type g_item_tab
IS
  record
  (
    item mtl_system_items_b.segment1%type ,
    item_desc mtl_system_items_b.description%type ,
    cat mtl_categories_kfv.segment1%type ,
    cat_desc fnd_flex_values_vl.description%type ,
    cat_class mtl_categories_kfv.concatenated_segments%type ,
    inventory_item_id NUMBER );
  g_item_rec g_item_tab;
  g_view_tab view_tab;
  g_manger_type    VARCHAR2 (5);
  g_salerep_num    NUMBER;
  g_prev_header_id NUMBER;
  g_prev_cust_id   NUMBER;
  g_cash_from_date DATE;
  g_cash_to_date   DATE;
  g_pre_header_id  NUMBER;
type inv_cnv_data
IS
  record
  (
    custno  VARCHAR2 (20) DEFAULT NULL ,
    invno   VARCHAR2 (20) DEFAULT NULL ,
    ordnum  VARCHAR2 (20) DEFAULT NULL ,
    shipvia VARCHAR2 (30) DEFAULT NULL ,
    warehse VARCHAR2 (30) DEFAULT NULL );
  g_cnv_inv_tab inv_cnv_data;
type inv_rental_rec
IS
  record
  (
    trx_date          DATE DEFAULT NULL ,
    trx_number        VARCHAR2 (30) DEFAULT NULL ,
    inventory_item_id NUMBER DEFAULT NULL ,
    header_id         NUMBER DEFAULT NULL ,
    customer_id       NUMBER DEFAULT NULL );
  g_inv_rental_tab inv_rental_rec;
type tb_data_tab
IS
  TABLE OF NUMBER INDEX BY binary_integer;
  g_tb_data_tab tb_data_tab;
  FUNCTION get_cash_from_date
    RETURN DATE;
  FUNCTION get_cash_to_date
    RETURN DATE;
  FUNCTION get_charger_amt(
      p_header_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_period_name(
      p_ledger_id NUMBER,
      p_gl_date   DATE)
    RETURN VARCHAR2;
  FUNCTION get_vendor_name(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_vendor_number(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_vendor_name(
      p_line_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_vendor_number(
      p_line_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_beg_inv(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_date              DATE)
    RETURN NUMBER;
  FUNCTION get_end_inv(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_date              DATE)
    RETURN NUMBER;
  FUNCTION get_cogs_amt(
      p_inventory_item_id NUMBER ,
      p_organization_id   NUMBER ,
      p_start_date        DATE ,
      p_end_date          DATE)
    RETURN NUMBER;
  FUNCTION get_item_selling_price(
      p_inventory_item_id NUMBER,
      p_org_id            NUMBER)
    RETURN NUMBER;
  FUNCTION get_last_order_num(
      p_inventory_item_id NUMBER,
      p_customer_id       NUMBER)
    RETURN NUMBER;
  FUNCTION get_last_invoice_num(
      p_inventory_item_id NUMBER,
      p_customer_id       NUMBER,
      p_header_id         NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_last_billed_date(
      p_inventory_item_id NUMBER,
      p_customer_id       NUMBER,
      p_header_id         NUMBER)
    RETURN DATE;
  FUNCTION get_new_selling_price(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_customer_id       NUMBER)
    RETURN NUMBER;
  FUNCTION get_list_price(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  --FUNCTION Get_item_list_Price (p_inventory_item_id NUMBER,p_organization_id NUMBER) RETURN NUMBER;
  FUNCTION get_last_list_price(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_curr_lp_start_date(
      p_list_header_id NUMBER,
      p_list_line_id   NUMBER)
    RETURN DATE;
  FUNCTION get_curr_lp_end_date(
      p_list_header_id NUMBER,
      p_list_line_id   NUMBER)
    RETURN DATE;
  FUNCTION get_last_lp_start_date(
      p_list_header_id NUMBER,
      p_list_line_id   NUMBER)
    RETURN DATE;
  FUNCTION get_mtd_sales(
      p_inventory_item_id NUMBER ,
      p_organization_id   NUMBER ,
      p_start_date        DATE DEFAULT NULL ,
      p_end_date          DATE DEFAULT NULL)
    RETURN NUMBER;
  FUNCTION get_tot_sales(
      p_inventory_item_id NUMBER ,
      p_organization_id   NUMBER ,
      p_start_date        DATE DEFAULT NULL ,
      p_end_date          DATE DEFAULT NULL)
    RETURN NUMBER;
  FUNCTION get_ytd_sales(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_year_start_date   DATE)
    RETURN NUMBER;
  FUNCTION get_header_hold(
      p_order_header_id NUMBER,
      p_line_id         NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_line_hold(
      p_order_header_id NUMBER,
      p_line_id         NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_header_hold_date(
      p_order_header_id NUMBER,
      p_line_id         NUMBER)
    RETURN DATE;
  FUNCTION get_line_hold_date(
      p_order_header_id NUMBER,
      p_line_id         NUMBER)
    RETURN DATE;
  FUNCTION get_discount_amt(
      p_order_header_id NUMBER,
      p_line_id         NUMBER DEFAULT NULL)
    RETURN NUMBER;
  FUNCTION get_special_cost(
      p_header_id         NUMBER,
      p_line_id           NUMBER,
      p_inventory_item_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_freight_amt(
      p_order_header_id NUMBER,
      p_line_id         NUMBER DEFAULT NULL)
    RETURN NUMBER;
  PROCEDURE set_date_from(
      p_date_from DATE);
  PROCEDURE set_date_to(
      p_date_to DATE);
  FUNCTION get_date_from
    RETURN DATE;
  FUNCTION get_date_to
    RETURN DATE;
  FUNCTION get_cancelled_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_line_disp_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_cancelled_count(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_line_disp_count(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  PROCEDURE set_period_name_from(
      p_period_name_from VARCHAR);
  PROCEDURE set_period_name_to(
      p_period_name_to VARCHAR);
  FUNCTION get_effective_period_num(
      p_period VARCHAR2,
      p_ledger_id IN NUMBER)
    RETURN NUMBER;
  FUNCTION get_period_name_from
    RETURN VARCHAR;
  FUNCTION get_period_name_to
    RETURN VARCHAR;
  FUNCTION get_period_end_date(
      p_period_name VARCHAR2,
      p_ledger_id   NUMBER)
    RETURN DATE;
  FUNCTION get_period_start_date(
      p_period_name VARCHAR2,
      p_ledger_id   NUMBER)
    RETURN DATE;
  FUNCTION get_last_year_period_name(
      p_ledger_id NUMBER)
    RETURN VARCHAR;
  FUNCTION get_year_start_date(
      p_period_name VARCHAR2,
      p_ledger_id   NUMBER)
    RETURN DATE;
  FUNCTION get_tax_jurdisjiction_code(
      p_tax_line_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_shipping_mthd(
      p_freight_code VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_shipvia(
      p_custno     VARCHAR2,
      p_trx_number VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_ordnum(
      p_custno     VARCHAR2,
      p_trx_number VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_warehse(
      p_custno     VARCHAR2,
      p_trx_number VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_shipping_mthd2(
      p_party_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_po_cost(
      p_requisition_header_id NUMBER,
      p_po_header_id          NUMBER)
    RETURN NUMBER;
  FUNCTION get_rental_start_date(
      p_line_id NUMBER)
    RETURN DATE;
  FUNCTION get_onhand_inv(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_onhand_date(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN DATE;
  FUNCTION get_onhand_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_date              DATE DEFAULT NULL)
    RETURN NUMBER;
  FUNCTION get_sub_onhand_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_date              DATE DEFAULT NULL)
    RETURN NUMBER;
  FUNCTION get_item_cost(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_item_demand_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_item_crossref(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_primary_bin_loc(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_alternate_bin_loc(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_final_end_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_date              DATE)
    RETURN NUMBER;
  FUNCTION get_schedule_part_flag(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_sale_cost(
      p_customer_trx_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_open_sales_orders(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_inv_cat_class(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_inv_gm(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_item_category_id  NUMBER)
    RETURN NUMBER;
  FUNCTION get_inv_cat_seg1(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_inv_prod_cat_class(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_inv_vel_cat_class(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_inv_cat_class_desc(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_reason_code(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_sales_profit(
      p_header_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_sales_profit_percentage(
      p_header_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_rental_days(
      p_header_id       NUMBER,
      p_link_to_line_id NUMBER,
      p_line_id         NUMBER)
    RETURN NUMBER;
  PROCEDURE set_payment_type(
      p_payment_type VARCHAR2);
  FUNCTION get_payment_type
    RETURN VARCHAR2;
  FUNCTION get_shipping_mthd3(
      p_freight_code VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_shipping_mthd4(
      p_freight_code VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_gsa_modifier_amt(
      p_product_val    VARCHAR2,
      p_list_price_amt NUMBER)
    RETURN NUMBER;
  FUNCTION get_gsa_disc_amt(
      p_product_val    VARCHAR2,
      p_list_price_amt NUMBER)
    RETURN NUMBER;
  FUNCTION get_adj_auto_modifier_amt(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_line_modifier_name(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_item_avg_cost(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_fly_item_selling_price(
      p_header_id         NUMBER,
      p_inventory_item_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_line_ship_confirm_flag(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_line_rma_flag(
      p_line_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_rental_unit_selling_price(
      p_line_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_line_shipment_date(
      p_line_id NUMBER)
    RETURN DATE;
  FUNCTION get_trader(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_mfgadjust(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_subinventory(
      p_organization_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_last_updt_by(
      p_line_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_cash_amount(
      p_header_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_isr_rpt_dc_mod_sub
    RETURN VARCHAR2;
  FUNCTION get_order_line_status(
      p_line_id IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_order_line_status(
      p_line_id          IN NUMBER,
      p_flow_status_code IN VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_item_purchased(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_isr_item_cost(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_isr_bpa_doc(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_isr_open_po_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_isr_avail_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_isr_ss_cnt(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_bin_loc_flag(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_cash_drawer_amt(
      p_header_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_grossmgr_rpt_sale_rep_flag(
      p_salerep_number VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_invoice_amt(
      p_header_id IN NUMBER)
    RETURN NUMBER;
  FUNCTION get_cust_invoice_amt(
      p_trx_id IN NUMBER)
    RETURN NUMBER;
  FUNCTION get_fixed_assets_cnt(
      p_organization_id IN NUMBER)
    RETURN NUMBER;
  FUNCTION get_bo_flag(
      p_line_id IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_backorder_qty(
      p_header_id IN NUMBER,
      p_line_id   IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_po_vendor_name(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_po_vendor_number(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_unit_cost(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_segment1          VARCHAR2)
    RETURN NUMBER;
  FUNCTION get_subinv_onhand_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_date              DATE DEFAULT NULL)
    RETURN NUMBER;
  FUNCTION get_lot_status(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_line_id           NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_isr_open_req_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_ssd_req_number(
      p_header_id         NUMBER,
      p_line_id           NUMBER,
      p_inventory_item_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_isr_sourcing_rule(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_isr_ss(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_tot_sales_dollors(
      p_inventory_item_id NUMBER ,
      p_organization_id   NUMBER ,
      p_start_date        DATE DEFAULT NULL ,
      p_end_date          DATE DEFAULT NULL)
    RETURN NUMBER;
  FUNCTION get_tot_sales_dlr(
      p_line_id    NUMBER,
      p_start_date DATE DEFAULT NULL,
      p_end_date   DATE DEFAULT NULL)
    RETURN NUMBER;
  FUNCTION get_tot_prizingzone_sales_dlr(
      p_inventory_item_id NUMBER ,
      p_organization_id   NUMBER ,
      p_zone              VARCHAR2 ,
      p_start_date        DATE DEFAULT NULL ,
      p_end_date          DATE DEFAULT NULL)
    RETURN NUMBER;
  FUNCTION get_customer_total(
      p_cust_account_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_rental_long_bill_days(
      p_shp_date DATE,
      p_end_date DATE)
    RETURN NUMBER;
  FUNCTION get_customer_receipt_total(
      p_cust_account_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_rental_bill_days(
      p_shp_date   DATE,
      p_start_date DATE,
      p_end_date   DATE)
    RETURN NUMBER;
  FUNCTION get_accrued_amt(
      p_days       NUMBER,
      p_dff_price  NUMBER,
      p_list_price NUMBER)
    RETURN NUMBER;
  --FUNCTION Get_Curr_list_Price (P_SEGMENT VARCHAR2) RETURN NUMBER;
  --FUNCTION Get_Curr_list_Price  (p_segment1 varchar2) RETURN NUMBER;
  FUNCTION get_curr_list_price(
      p_item_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_curr_list_price(
      p_item_id           NUMBER,
      p_pricing_attribute VARCHAR2)
    RETURN NUMBER;
  PROCEDURE get_curr_sold_sales(
      p_item_id      IN NUMBER ,
      p_cat_id       IN NUMBER ,
      p_cat          IN VARCHAR2 ,
      p_cust_acct_id IN NUMBER ,
      p_start_date   IN DATE ,
      p_end_date     IN DATE ,
      p_units_sold OUT NUMBER ,
      p_actual_sales OUT NUMBER ,
      p_unit_cost OUT NUMBER ,
      p_process_id IN NUMBER);
  PROCEDURE get_curr_sold_sales(
      p_item_id        IN NUMBER ,
      p_cat_id         IN NUMBER ,
      p_cat            IN VARCHAR2 ,
      p_cust_acct_id   IN NUMBER ,
      p_list_header_id IN NUMBER ,
      p_units_sold OUT NUMBER ,
      p_actual_sales OUT NUMBER ,
      p_unit_cost OUT NUMBER ,
      p_process_id IN NUMBER);
  --function get_application_method(p_inventory_item_id number,p_organization_id number) return varchar2;
  FUNCTION get_application_method(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_application_value(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_shipped_qty(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_returned_qty(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_sales_dollars(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_return_dollars(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_customer_amt_due(
      p_cust_account_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_average_cost(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_dist_invoice_num(
      p_customer_trx_id NUMBER,
      p_invoice_number  VARCHAR2,
      p_order_type      VARCHAR2)
    RETURN VARCHAR2;
  FUNCTION get_average_cost(
      p_inventory_item_id NUMBER ,
      p_organization_id   NUMBER ,
      p_shipment_date     DATE ,
      p_line_id           NUMBER)
    RETURN NUMBER;
  --FUNCTION get_salesrep_name (p_inventory_item_id NUMBER, p_customer_id NUMBER) RETURN Number;
  FUNCTION xxwc_cal_restock_per(
      l_header_id IN NUMBER)
    RETURN NUMBER;
  FUNCTION xxwc_get_fiscal_year(
      p_org_id IN NUMBER,
      p_date   IN DATE)
    RETURN NUMBER;
  FUNCTION xxwc_get_fiscal_month(
      p_org_id IN NUMBER,
      p_date   IN DATE)
    RETURN VARCHAR2;
  FUNCTION calc_ordered_amount(
      p_header_id IN NUMBER)
    RETURN NUMBER;
  FUNCTION get_ap_balance(
      p_vendor_id NUMBER,
      p_org_id    NUMBER)
    RETURN NUMBER;
  FUNCTION get_ar_balance(
      p_bill_to_customer_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_receipt_date_period(
      p_trx_id NUMBER)
    RETURN DATE;
  FUNCTION get_trx_period_name(
      p_trx_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_usetax_amt(
      p_amount    NUMBER,
      p_post_code NUMBER)
    RETURN NUMBER;
  FUNCTION get_usetax_rate(
      p_post_code NUMBER)
    RETURN NUMBER;
  FUNCTION get_usetax_rate(
      p_post_code VARCHAR2)
    RETURN NUMBER;
  FUNCTION get_usetax_amt(
      p_amount    NUMBER,
      p_post_code VARCHAR2)
    RETURN NUMBER;
  FUNCTION get_venquote_glstring(
      p_order_number IN VARCHAR2,
      p_type         IN VARCHAR2,
      p_line_id      IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_int_req_so_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_org_item_reserve_qty(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_credit_memo_tax_val(
      p_type                 VARCHAR2,
      p_customer_trx_id      NUMBER,
      p_customer_trx_line_id NUMBER)
    RETURN NUMBER;
  FUNCTION get_best_buy(
      p_item_id         NUMBER,
      p_organization_id NUMBER,
      p_vendor_number   VARCHAR2)
    RETURN NUMBER;
  FUNCTION get_contract_customer_name(
      p_site_use_code  VARCHAR2,
      p_qual_attribute VARCHAR2)
    RETURN VARCHAR2;
  PROCEDURE set_cost_location(
      p_organization_code VARCHAR2);
  FUNCTION get_cost_location
    RETURN NUMBER;
  FUNCTION get_req_source(
      p_organization_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_rental_item(
      p_line_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_rental_item_desc(
      p_line_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_account_type_code(
      p_transaction_id NUMBER,
      p_account_id     NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_cogs(
      p_inventory_item_id NUMBER ,
      p_organization_id   NUMBER ,
      p_start_date        DATE ,
      p_end_date          DATE)
    RETURN NUMBER;
  FUNCTION get_turns(
      p_inventory_item_id NUMBER ,
      p_organization_id   NUMBER ,
      p_start_date        DATE ,
      p_end_date          DATE)
    RETURN NUMBER;
  FUNCTION get_turns(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN NUMBER;
  FUNCTION get_order_line_po_cost(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER,
      p_order_date        DATE)
    RETURN NUMBER;
  FUNCTION get_adj_auto_modifier_name(
      p_header_id NUMBER,
      p_line_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_oldest_born_date(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN DATE;
  FUNCTION get_cash_date(
      p_payment_set_id NUMBER)
    RETURN DATE;
  FUNCTION get_po_om_order_number(
      p_header_id       NUMBER,
      p_line_id         NUMBER,
      p_trx_src_type_id NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_order_count(
      p_order_number NUMBER)
    RETURN NUMBER; --Added this function to count Order number in Invoice Pre Register Report
  PROCEDURE get_order_count_status(
      p_header_id IN NUMBER,
      p_order_status OUT NUMBER,
      p_order_count OUT NUMBER); --Added for Product Fill Rate Report by Order and by Line report use
  PROCEDURE get_line_status(
      p_line_id IN NUMBER,
      p_closed_status OUT NUMBER,
      p_bo_status OUT NUMBER); -- Added to find status of line in Product Fill Rate Report by Order and by Line report
  PROCEDURE init_item_details(
      p_inventory_item_id NUMBER);
  PROCEDURE parse_param_list(
      p_process_id IN NUMBER,
      p_list_name  IN VARCHAR2,
      p_list_type  IN VARCHAR2);
  PROCEDURE parse_cleanup_table(
      p_process_id IN NUMBER);
  FUNCTION get_flag_value
    RETURN VARCHAR2;
  FUNCTION get_item_flag_value
    RETURN VARCHAR2;
  FUNCTION get_cat_flag_value
    RETURN VARCHAR2;
  FUNCTION get_bill_line_cost(
      p_header_id         IN NUMBER,
      p_inventory_item_id IN NUMBER)
    RETURN NUMBER;
  FUNCTION get_inv_cat_segments(
      p_inventory_item_id NUMBER,
      p_organization_id   NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_empolyee_name(
      p_employee_id IN NUMBER,
      p_date        IN DATE)
    RETURN VARCHAR2;
  FUNCTION get_payment_amount(
      p_order_number   IN NUMBER,
      p_payment_set_id IN NUMBER,
      p_header_id      IN NUMBER)
    RETURN NUMBER;
  FUNCTION get_order_freight(
      p_header_id IN NUMBER)
    RETURN NUMBER;
  PROCEDURE set_period_name(
      p_period_name_from VARCHAR);
  FUNCTION get_open_order_status(
      p_item_id         IN NUMBER,
      p_organization_id IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_doc_date(
      p_header_id     IN NUMBER ,
      p_line_id       IN NUMBER ,
      p_item_id       IN NUMBER ,
      p_org_id        IN NUMBER ,
      p_header_org_id IN NUMBER)
    RETURN DATE;
  FUNCTION get_line_id(
      p_line_id IN NUMBER)
    RETURN NUMBER;
  FUNCTION get_promo_item(
      p_item_id IN NUMBER,
      p_org_id  IN NUMBER)
    RETURN VARCHAR2;
  FUNCTION get_delivery_charge_amt(
      p_trx_id IN NUMBER)
    RETURN NUMBER;
  FUNCTION yy_get_doc_date(
      p_header_id     IN NUMBER ,
      p_line_id       IN NUMBER ,
      p_item_id       IN NUMBER ,
      p_org_id        IN NUMBER ,
      p_header_org_id IN NUMBER)
    RETURN DATE;
  FUNCTION get_reference_order(
      p_header_id IN NUMBER,
      p_line_id   IN NUMBER)
    RETURN NUMBER;
  FUNCTION get_person_name(
      p_employee_id IN NUMBER,
      p_date        IN DATE)
    RETURN VARCHAR2;
  --   PROCEDURE POPULATE_LOCATIONS(P_PROCESS_ID IN NUMBER) ;
  --  PROCEDURE CLEANUP_POPULATE_LOCATIONS(P_PROCESS_ID IN NUMBER) ;
  FUNCTION get_balance_amount(
      p_order_number   IN NUMBER,
      p_payment_set_id IN NUMBER,
      p_header_id      IN NUMBER)
    return number;
  FUNCTION get_last_app_price_adjust_id(
    p_header_id IN NUMBER,
    p_line_id   IN NUMBER)
  return number;
  FUNCTION get_low_high_f(
    p_item IN VARCHAR2,
    p_qty  IN NUMBER)
  return varchar2;
  FUNCTION get_pricing_resp(
    p_user_id IN NUMBER)
  RETURN VARCHAR2 ;
END eis_rs_xxwc_com_util_pkg;
/