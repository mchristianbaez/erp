--Report Name            : Orders Exception Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_ORDER_EXP_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_ORDER_EXP_V
Create or replace View XXEIS.EIS_XXWC_OM_ORDER_EXP_V
 AS 
SELECT
    /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N2) index(ol XXWC_OE_ORDER_LINES_ALL_N3) */
    mp.organization_code loc,
    'Order On Hold' exception_resaon,
    oh.order_number order_number,
    TRUNC(oh.ordered_date) order_date,
    --OHd.CREATION_DATE EXCEPTION_DATE,
    ooh.creation_date exception_date,
    NVL( hca.account_name,hp.party_name) customer_name,
    ottl.NAME order_type,
    CASE
      WHEN xxeis.eis_rs_xxwc_com_util_pkg.get_line_id(ol.line_id) = 1
      THEN ROUND((ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(ol.ordered_quantity,0)* NVL(ol.unit_selling_price,0),2)+ NVL(ol.tax_value,0)),2)
      ELSE NULL
    END ext_order_total,
    DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(ol.ordered_quantity,0) qty,
    ohd.description exception_description,
    rep.NAME sales_person_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name(fu.user_id,oh.creation_date) created_by,
    NVL(flv.meaning,ol.shipping_method_code) ship_method
  FROM oe_order_headers oh,
    oe_order_lines ol,
    -- ORG_ORGANIZATION_DEFINITIONS OOD,
    mtl_parameters mp,
    hz_cust_accounts hca,
    hz_parties hp,
    oe_transaction_types_vl ottl,
    oe_order_holds ooh,
    oe_hold_sources ohs,
    oe_hold_definitions ohd,
    ra_salesreps rep,
    --  PER_PEOPLE_F PPF,
    fnd_user fu,
    fnd_lookup_values flv
  WHERE oh.header_id           = ol.header_id
  AND ol.ship_from_org_id      = mp.organization_id
  AND oh.order_type_id         = ottl.transaction_type_id
  AND oh.salesrep_id           = rep.salesrep_id(+)
  AND oh.org_id                = rep.org_id(+)
  AND oh.sold_to_org_id        = hca.cust_account_id
  AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED')
  AND ohd.NAME NOT            IN('Pricing Guardrail Hold','Pricing Guardrail Hold - Limited User', 'XXWC_PRICE_CHANGE_HOLD')
  AND hca.party_id             = hp.party_id
  AND ooh.header_id            = oh.header_id
  AND ol.line_id               = ooh.line_id(+)
  AND ohs.hold_source_id       = ooh.hold_source_id
  AND ooh.hold_release_id     IS NULL
  AND ohs.hold_id              = ohd.hold_id
  AND fu.user_id               = oh.created_by
  AND flv.lookup_type (+)      = 'SHIP_METHOD'
  AND flv.lookup_code (+)      = ol.shipping_method_code
  UNION ALL
  SELECT
    /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N2) index(ol XXWC_OE_ORDER_LINES_ALL_N3) */
    mp.organization_code loc,
    'Order On Hold' exception_resaon,
    oh.order_number order_number,
    TRUNC(oh.ordered_date) order_date,
    --OHd.CREATION_DATE EXCEPTION_DATE,
    ooh.creation_date exception_date,
    NVL( hca.account_name,hp.party_name) customer_name,
    ottl.NAME order_type,
    CASE
      WHEN xxeis.eis_rs_xxwc_com_util_pkg.get_line_id(ol.line_id) = 1
      THEN ROUND((ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(ol.ordered_quantity,0)* NVL(ol.unit_selling_price,0),2)+ NVL(ol.tax_value,0)),2)
      ELSE NULL
    END ext_order_total,
    DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(ol.ordered_quantity,0) qty,
    ohd.description exception_description,
    rep.NAME sales_person_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name(fu.user_id,oh.creation_date) created_by,
    NVL(flv.meaning,ol.shipping_method_code) ship_method
  FROM oe_order_headers oh,
    oe_order_lines ol,
    -- ORG_ORGANIZATION_DEFINITIONS OOD,
    mtl_parameters mp,
    hz_cust_accounts hca,
    hz_parties hp,
    oe_transaction_types_vl ottl,
    oe_order_holds ooh,
    oe_hold_sources ohs,
    oe_hold_definitions ohd,
    ra_salesreps rep,
    --  PER_PEOPLE_F PPF,
    fnd_user fu,
    fnd_lookup_values flv
  WHERE oh.header_id           = ol.header_id
  AND ol.ship_from_org_id      = mp.organization_id
  AND oh.order_type_id         = ottl.transaction_type_id
  AND oh.salesrep_id           = rep.salesrep_id(+)
  AND oh.org_id                = rep.org_id(+)
  AND oh.sold_to_org_id        = hca.cust_account_id
  AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED')
  AND ohd.NAME NOT            IN('Pricing Guardrail Hold','Pricing Guardrail Hold - Limited User', 'XXWC_PRICE_CHANGE_HOLD')
  AND hca.party_id             = hp.party_id
  AND ooh.header_id            = oh.header_id
  AND ooh.line_id             IS NULL
  AND ohs.hold_source_id       = ooh.hold_source_id
  AND ooh.hold_release_id     IS NULL
  AND ohs.hold_id              = ohd.hold_id
  AND fu.user_id               = oh.created_by
  AND flv.lookup_type (+)      = 'SHIP_METHOD'
  AND flv.lookup_code (+)      = ol.shipping_method_code
  UNION ALL
  SELECT
    /*+ INDEX(ol XXWC_OE_ORDER_LINES_ALL_N3) */
    mp.organization_code loc,
    CASE
      WHEN OL.FLOW_STATUS_CODE = 'INVOICE_HOLD' /* Invoice on hold added on 03-oct-2013*/
      THEN  'Invoice on Hold'
      WHEN ottl.NAME          ='STANDARD ORDER'
      AND ol.flow_status_code = 'AWAITING_RECEIPT'
      THEN 'Open Direct'
      WHEN ottl.NAME          ='RETURN ORDER'
      AND ol.flow_status_code = 'AWAITING_RETURN'
      THEN 'Open RMA'
      WHEN ol.flow_status_code ='PRE-BILLING_ACCEPTANCE'
      THEN 'Pending Pre Billing'
      WHEN (ottl.NAME          = 'COUNTER ORDER'
      AND ol.flow_status_code <>'ENTERED')
        -- THEN 'Booked Not Closed'
      THEN 'Counter Order > 1- Day'
      WHEN (ottl.NAME         = 'COUNTER ORDER'
      AND ol.flow_status_code ='ENTERED')
      THEN 'Entered Not Booked'
      ELSE NULL
    END exception_resaon,
    oh.order_number order_number,
    TRUNC(oh.ordered_date) order_date,
    CASE
      WHEN OL.FLOW_STATUS_CODE = 'INVOICE_HOLD'  /* Invoice on hold added on 03-oct-2013*/
      THEN TRUNC(oh.ordered_date)
      WHEN ottl.NAME          ='STANDARD ORDER'
      AND ol.flow_status_code = 'AWAITING_RECEIPT'
      THEN TRUNC(oh.ordered_date)
      WHEN ottl.NAME          ='RETURN ORDER'
      AND ol.flow_status_code = 'AWAITING_RETURN'
      THEN TRUNC(oh.ordered_date)
      WHEN ol.flow_status_code ='PRE-BILLING_ACCEPTANCE'
      THEN TRUNC(ol.actual_shipment_date)
      WHEN (ottl.NAME          ='COUNTER ORDER'
      AND ol.flow_status_code <>'ENTERED')
      THEN TRUNC(oh.ordered_date)+1
      WHEN (ottl.NAME         ='COUNTER ORDER'
      AND ol.flow_status_code ='ENTERED')
      THEN TRUNC(oh.ordered_date)
      ELSE NULL
    END exception_date,
    NVL( hca.account_name,hp.party_name) customer_name,
    ottl.NAME order_type,
    ROUND((ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(ol.ordered_quantity,0)* NVL(ol.unit_selling_price,0),2)+ NVL(ol.tax_value,0)),2) ext_order_total,
    DECODE(ol.line_category_code,'RETURN',              -1,1)* NVL(ol.ordered_quantity,0) qty,
    CASE
      WHEN OL.FLOW_STATUS_CODE = 'INVOICE_HOLD' /* Invoice on hold added on 03-oct-2013*/
      THEN 'Invoice Hold Applied - Review for Release'
      WHEN ottl.NAME          ='STANDARD ORDER'
      AND ol.flow_status_code = 'AWAITING_RECEIPT'
      THEN 'Direct Ship waiting to be Received'
      WHEN ottl.NAME          ='RETURN ORDER'
      AND ol.flow_status_code = 'AWAITING_RETURN'
      THEN 'RMA waiting to be Received'
      WHEN ol.flow_status_code ='PRE-BILLING_ACCEPTANCE'
      THEN 'Item waiting on Fulfillment Acceptance'
      WHEN (ottl.NAME          ='COUNTER ORDER'
      AND ol.flow_status_code <>'ENTERED')
      THEN 'Counter Order Needs Review'
      WHEN (ottl.NAME         = 'COUNTER ORDER'
      AND ol.flow_status_code ='ENTERED')
      THEN 'Needs to be Reviewed and Booked'
      ELSE NULL
    END exception_description,
    rep.NAME sales_person_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name(fu.user_id,oh.creation_date) created_by,
    NVL(flv.meaning,ol.shipping_method_code) ship_method
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters mp,
    hz_cust_accounts hca,
    hz_parties hp,
    oe_transaction_types_vl ottl,
    ra_salesreps rep,
    -- PER_PEOPLE_F PPF,
    fnd_user fu,
    fnd_lookup_values flv
  WHERE oh.header_id      = ol.header_id
  AND ol.ship_from_org_id = mp.organization_id
  AND oh.order_type_id    = ottl.transaction_type_id
  AND oh.salesrep_id      = rep.salesrep_id(+)
  AND oh.org_id           = rep.org_id(+)
  AND oh.sold_to_org_id   = hca.cust_account_id
  AND hca.party_id        = hp.party_id
  AND fu.user_id          = oh.created_by
  AND flv.lookup_type (+) = 'SHIP_METHOD'
  AND flv.lookup_code (+) = ol.shipping_method_code
    --and fu.employee_id      = ppf.person_id(+)
    -- AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (oh.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (oh.creation_date) )
  AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED')
  AND ottl.NAME               IN('STANDARD ORDER','COUNTER ORDER','RETURN ORDER','REPAIR ORDER')
  AND ((ottl.NAME             != 'COUNTER ORDER'
  AND ol.flow_status_code     IN('AWAITING_RECEIPT','AWAITING_RETURN','PRE-BILLING_ACCEPTANCE','INVOICE_HOLD'))----added 'INVOICE_HOLD' on 9/26/2013
  OR(ottl.NAME                 = 'COUNTER ORDER'
  AND TRUNC(SYSDATE)           > TRUNC(ordered_date) ) )
    /* � Counter Orders that are not closed, due to a Hold, should only display once under Order On Hold*/
  AND NOT EXISTS
    ( SELECT DISTINCT ooh.header_id
    FROM oe_order_headers ohe,
      oe_order_lines ole,
      oe_order_holds ooh,
      oe_hold_sources ohs,
      oe_hold_definitions ohd,
      oe_transaction_types_vl ottl
    WHERE ohe.header_id = ole.header_id
    AND ohe.header_id   = oh.header_id
    AND ole.line_id     = ol.line_id
    AND ooh.header_id   = ohe.header_id
      --and OLE.LINE_ID           = OOH.LINE_ID(+)
    AND ohs.hold_source_id   = ooh.hold_source_id
    AND ooh.hold_release_id IS NULL
    AND ohs.hold_id          = ohd.hold_id
    AND OHE.ORDER_TYPE_ID    = OTTL.TRANSACTION_TYPE_ID
    AND OTTL.NAME            = 'COUNTER ORDER' 
   --- or (ol.flow_status_code='INVOICE_HOLD' and ohd.NAME='Invoicing Hold' ))
--   or(ol.flow_status_code='INVOICE_HOLD' and   OHD.name not in('Pricing Guardrail Hold',
--      'Pricing Guardrail Hold - Limited User',
--      'XXWC_PRICE_CHANGE_HOLD'
--      )))
    --or (ottl.NAME in('RETURN ORDER','REPAIR ORDER') and ol.flow_status_code in('INVOICE_HOLD')) ) ---Added or condition
    /*and OHD.name not in('Pricing Guardrail Hold',
      'Pricing Guardrail Hold - Limited User',
      'XXWC_PRICE_CHANGE_HOLD'
      )*/
    )
  UNION ALL
  SELECT
    /*+ INDEX(oh XXWC_OE_ORDER_HEADERS_ALL_N2) index(ol XXWC_OE_ORDER_LINES_ALL_N3) */
    mp.organization_code loc,
    'Out for Delivery' exception_resaon,
    oh.order_number order_number,
    TRUNC(oh.ordered_date) order_date,
    xxeis.eis_rs_xxwc_com_util_pkg.yy_get_doc_date(oh.header_id, ol.line_id,ol.inventory_item_id,ol.ship_from_org_id,oh.ship_from_org_id) exception_date,
    NVL( hca.account_name,hp.party_name) customer_name,
    ottl.NAME order_type,
    ROUND((ROUND(DECODE(ol.line_category_code,'RETURN', -1,1)* NVL(ol.ordered_quantity,0)* NVL(ol.unit_selling_price,0),2)+ NVL(ol.tax_value,0)),2) ext_order_total,
    DECODE(ol.line_category_code,'RETURN',              -1,1)* NVL(ol.ordered_quantity,0) qty,
    'Item Shipped but Not Confirmed' exception_description,
    rep.NAME sales_person_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_empolyee_name(fu.user_id,oh.creation_date) created_by,
    NVL(flv.meaning,ol.shipping_method_code) ship_method
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_parameters mp,
    hz_cust_accounts hca,
    hz_parties hp,
    oe_transaction_types_vl ottl,
    ra_salesreps rep,
    -- PER_PEOPLE_F PPF,
    fnd_user fu,
    fnd_lookup_values flv
  WHERE oh.header_id      = ol.header_id
  AND ol.ship_from_org_id = mp.organization_id
  AND oh.order_type_id    = ottl.transaction_type_id
  AND oh.salesrep_id      = rep.salesrep_id(+)
  AND oh.org_id           = rep.org_id(+)
  AND oh.sold_to_org_id   = hca.cust_account_id
  AND hca.party_id        = hp.party_id
  AND fu.user_id          = oh.created_by
  AND flv.lookup_type (+) = 'SHIP_METHOD'
  AND flv.lookup_code (+) = ol.shipping_method_code
    -- AND FU.EMPLOYEE_ID      = PPF.PERSON_ID(+)
    -- AND TRUNC (oh.creation_date) BETWEEN NVL (ppf.effective_start_date, TRUNC (oh.creation_date) ) AND NVL (ppf.effective_end_date, TRUNC (oh.creation_date) )
  AND ol.flow_status_code NOT                                                                                                                                      IN ('CLOSED','CANCELLED')
  AND ottl.NAME                                                                                                                                                    IN('STANDARD ORDER','RETURN ORDER','REPAIR ORDER')
  AND ol.flow_status_code                                                                                                                                           = 'AWAITING_SHIPPING'
  AND (SYSDATE - NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.YY_GET_DOC_DATE(OH.HEADER_ID, OL.LINE_ID,OL.INVENTORY_ITEM_ID,OL.SHIP_FROM_ORG_ID,OH.SHIP_FROM_ORG_ID),SYSDATE) > 1 )/
set scan on define on
prompt Creating View Data for Orders Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_ORDER_EXP_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Order Exp V','EXOOEV','','');
--Delete View Columns for EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_ORDER_EXP_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_ORDER_EXP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_DESCRIPTION',660,'Exception Description','EXCEPTION_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Exception Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXT_ORDER_TOTAL',660,'Ext Order Total','EXT_ORDER_TOTAL','','','','XXEIS_RS_ADMIN','NUMBER','','','Ext Order Total','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_DATE',660,'Exception Date','EXCEPTION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Exception Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','EXCEPTION_RESAON',660,'Exception Resaon','EXCEPTION_RESAON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Exception Resaon','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Person Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','SHIP_METHOD',660,'Ship Method','SHIP_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_ORDER_EXP_V','LOC',660,'Loc','LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc','','','');
--Inserting View Components for EIS_XXWC_OM_ORDER_EXP_V
--Inserting View Component Joins for EIS_XXWC_OM_ORDER_EXP_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Orders Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Orders Exception Report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS, HR_OPERATING_UNITS OU
WHERE RS.org_id = OU.organization_id
AND RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Orders Exception Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Orders Exception Report
xxeis.eis_rs_utility.delete_report_rows( 'Orders Exception Report' );
--Inserting Report - Orders Exception Report
xxeis.eis_rs_ins.r( 660,'Orders Exception Report','','White Cap Exception Report','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_ORDER_EXP_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Orders Exception Report
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXCEPTION_DATE','Exception Date','Exception Date','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXCEPTION_DESCRIPTION','Exception Description','Exception Description','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXCEPTION_RESAON','Exception Reason','Exception Resaon','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'EXT_ORDER_TOTAL','Line Total','Ext Order Total','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'ORDER_DATE','Order Date','Order Date','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'ORDER_TYPE','Order Type','Order Type','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'CREATED_BY','Created By','Created By','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'QTY','Qty','Qty','','~~~','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'LOC','Loc','Loc','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
xxeis.eis_rs_ins.rc( 'Orders Exception Report',660,'SHIP_METHOD','Ship Method','Ship Method','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_ORDER_EXP_V','','');
--Inserting Report Parameters - Orders Exception Report
xxeis.eis_rs_ins.rp( 'Orders Exception Report',660,'Sales Person Name','Sales Person Name','SALES_PERSON_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders Exception Report',660,'Organization','Organization','LOC','IN','OM Warehouse All','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Orders Exception Report
xxeis.eis_rs_ins.rcn( 'Orders Exception Report',660,'SALES_PERSON_NAME','IN',':Sales Person Name','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Orders Exception Report',660,'LOC','IN',':Organization','','','N','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Orders Exception Report',660,'','','','','AND ( ''All'' IN (:Organization) OR (Loc IN (:Organization)))','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Orders Exception Report
xxeis.eis_rs_ins.rs( 'Orders Exception Report',660,'EXCEPTION_RESAON','ASC','XXEIS_RS_ADMIN','1','');
--Inserting Report Triggers - Orders Exception Report
--Inserting Report Templates - Orders Exception Report
--Inserting Report Portals - Orders Exception Report
--Inserting Report Dashboards - Orders Exception Report
--Inserting Report Security - Orders Exception Report
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51509',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51045',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','51025',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Orders Exception Report','20005','','50900',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Orders Exception Report
xxeis.eis_rs_ins.rpivot( 'Orders Exception Report',660,'Summary by reason','1','1,0|1,2,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Summary by reason
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','EXT_ORDER_TOTAL','DATA_FIELD','SUM','Ext Total','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','EXCEPTION_RESAON','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','ORDER_NUMBER','ROW_FIELD','','','3','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','CREATED_BY','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders Exception Report',660,'Summary by reason','QTY','ROW_FIELD','','','4','','');
--Inserting Report Summary Calculation Columns For Pivot- Summary by reason
END;
/
set scan on define on
