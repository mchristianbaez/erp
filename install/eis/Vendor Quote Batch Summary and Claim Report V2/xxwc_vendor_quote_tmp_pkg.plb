CREATE OR REPLACE package body XXWC_VENDOR_QUOTE_TMP_PKG  as
    procedure GET_HEADER_ID (P_process_id in number,P_INV_START_DATE in date,P_INV_END_DATE in date) is
    L_STMT long;
    
    BEGIN 
    insert into xxeis.XXWC_VENDOR_QUOTE_TEMP
    (PROCESS_ID,HEADER_ID)
    SELECT distinct P_process_id,
    OH.HEADER_ID 
       FROM oe_order_headers oh,
      oe_order_lines ol,
      qp_qualifiers qq,
      qp_list_headers qlh,
      apps.xxwc_om_contract_pricing_hdr xxcph,
      apps.xxwc_om_contract_pricing_lines xxcpl,
      qp_list_lines qll,
      mtl_system_items_b_kfv msi,
      fnd_user fu,
      per_all_people_f ppf1,
      po_vendors pov,
      ra_salesreps jrs,
      ra_customer_trx rct,
      ra_customer_trx_lines rctl,
      ra_cust_trx_line_gl_dist rctlgl,
      gl_code_combinations_kfv gcc,
      hz_cust_accounts hcs,
      hz_parties hp,
      hz_cust_site_uses hcsu,
      hz_cust_acct_sites hcas,
      hz_party_sites hps,
      MTL_PARAMETERS MP
    --  xxeis.XXWC_VENDOR_QUOTE_TEMP temp
    WHERE 1                             =1
    AND oh.header_id                    = ol.header_id
    AND ol.flow_status_code             = 'CLOSED'
    AND qq.qualifier_context            = 'CUSTOMER'
    AND qq.comparison_operator_code     = '='
    AND qq.active_flag                  = 'Y'
    AND ( (qlh.attribute10              = ('Contract Pricing')
    AND qq.qualifier_attribute          = 'QUALIFIER_ATTRIBUTE11'
    AND (qq.qualifier_attr_value        = TO_CHAR(oh.ship_to_org_id)) )
    OR (qlh.attribute10                 = ('Contract Pricing')
    AND qq.qualifier_attribute          = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value        = TO_CHAR(oh.sold_to_org_id)) )
    OR (qlh.attribute10                 = ('CSP-NATIONAL')
    AND qq.qualifier_attribute          = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value        = TO_CHAR(oh.sold_to_org_id)) ) )
    AND qlh.list_header_id              = qq.list_header_id
    AND qlh.active_flag                 = 'Y'
    AND xxcph.agreement_id              = TO_CHAR(qlh.attribute14)
    AND xxcph.revision_number           = TO_CHAR(qlh.attribute15)
    AND xxcpl.agreement_id              = xxcph.agreement_id
    AND xxcpl.agreement_type            = 'VQN'
    AND qlh.list_header_id              = qll.list_header_id
    AND xxcpl.product_value             = ol.ordered_item
    AND xxcpl.agreement_line_id         = TO_CHAR(qll.attribute2)
    AND msi.inventory_item_id           = ol.inventory_item_id
    AND msi.organization_id             = ol.ship_from_org_id
    AND fu.user_id                      = oh.created_by
    AND fu.employee_id(+)               = ppf1.person_id
    AND pov.vendor_id                   = xxcpl.vendor_id
    AND oh.salesrep_id                  = jrs.salesrep_id(+)
    AND oh.org_id                       = jrs.org_id(+)
    AND rct.interface_header_context    = 'ORDER ENTRY'
    AND rct.interface_header_attribute1 = TO_CHAR(oh.order_number)
    AND rctl.interface_line_context     = 'ORDER ENTRY'
    AND rctl.interface_line_attribute6  = TO_CHAR (ol.line_id)
    AND rctl.sales_order                = oh.order_number
    AND rctl.customer_trx_id            = rct.customer_trx_id
    AND rctl.line_type                  = 'LINE'
    AND rctlgl.customer_trx_id          = rctl.customer_trx_id
    AND rctlgl.customer_trx_line_id     = rctl.customer_trx_line_id
    AND rctlgl.account_class            = 'REV'
    AND gcc.code_combination_id         = rctlgl.code_combination_id
    AND hcs.cust_account_id             = rct.ship_to_customer_id
    AND hcs.party_id                    = hp.party_id
    AND hcsu.site_use_id                = rct.ship_to_site_use_id
    AND hcas.cust_acct_site_id          = hcsu.cust_acct_site_id
    AND hcas.party_site_id              = hps.party_site_id
    and MP.ORGANIZATION_ID              = MSI.ORGANIZATION_ID
   -- and TRUNC(RCT.TRX_DATE) between '26-Sep-2013' and '04-Oct-2013';
    and TRUNC(RCT.TRX_DATE) between P_INV_START_DATE and P_INV_END_DATE;

    end;
    end XXWC_VENDOR_QUOTE_TMP_PKG;
/
