--Report Name            : Specials Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_INV_SPECIAL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_INV_SPECIAL_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Rs Xxwc Inv Special V','EXISV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_INV_SPECIAL_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','BUYER_CODE',401,'Buyer Code','BUYER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','ONHAND',401,'Onhand','ONHAND','','','','XXEIS_RS_ADMIN','NUMBER','','','Onhand','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','WEIGHT',401,'Weight','WEIGHT','','','','XXEIS_RS_ADMIN','NUMBER','','','Weight','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Averagecost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Selling Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','UOM',401,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','CAT',401,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','GP',401,'Gp','GP','','','','XXEIS_RS_ADMIN','NUMBER','','','Gp','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','OPEN_SALES_ORDERS',401,'Open Sales Orders','OPEN_SALES_ORDERS','','','','XXEIS_RS_ADMIN','NUMBER','','','Open Sales Orders','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','NO_OF_ITEM_PURCHASED',401,'No Of Item Purchased','NO_OF_ITEM_PURCHASED','','','','XXEIS_RS_ADMIN','NUMBER','','','No Of Item Purchased','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','OLDEST_BORN_DATE',401,'Oldest Born Date','OLDEST_BORN_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Oldest Born Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','AGENT_ID',401,'Agent Id','AGENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Agent Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inv Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','DATE_RECEIVED',401,'Date Received','DATE_RECEIVED','','','','XXEIS_RS_ADMIN','DATE','','','Date Received','','','','US');
--Inserting Object Components for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','PO_AGENTS_V',401,'PO_AGENTS','POA','POA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Buyers Table','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Categories','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXISV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXISV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Specials Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Specials Report
xxeis.eis_rsc_ins.lov( 401,'SELECT ''Include Specials with Oh Hand > 7 days'' onhand_type FROM dual
UNION
SELECT ''Include Specials with Oh Hand < 7 days'' onhand_type FROM dual
union
SELECT ''ALL specials'' onhand_type FROM dual','','INV SPECIALONHAND TYPE','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Specials Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Specials Report
xxeis.eis_rsc_utility.delete_report_rows( 'Specials Report' );
--Inserting Report - Specials Report
xxeis.eis_rsc_ins.r( 401,'Specials Report','','report on all special parts ordered; special parts on hand with or without demand; report to flag special parts to become standard parts (i.e. they are used a lot)','','','','SA059956','EIS_XXWC_INV_SPECIAL_V','Y','','','SA059956','','N','White Cap Reports','','EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Specials Report
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'AVERAGECOST','Avg Cost','Averagecost','','~T~D~2','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'BUYER_CODE','Buyer','Buyer Code','','','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'CAT','Cat Class','Cat','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'DESCRIPTION','Description','Description','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'ONHAND','On Hand','Onhand','','~~~','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'PART_NUMBER','Part Number','Part Number','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'UOM','UOM','Uom','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'WEIGHT','Weight','Weight','','~~~','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'GP','GP%','Gp','','~T~D~2','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'ORGANIZATION_CODE','Org Number','Organization Code','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'NO_OF_ITEM_PURCHASED','No Of Times Purchased','No Of Item Purchased','','~~~','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'SELLING_PRICE','Selling Price','Selling Price','','~T~D~0','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'OLDEST_BORN_DATE','Oldest Born Date','Oldest Born Date','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'OPEN_SALES_ORDERS','Open Sales Orders','Open Sales Orders','','~~~','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Specials Report',401,'Extended_Values','Extended Value','Extended_Values','NUMBER','~~~','default','','12','Y','Y','','','','','','(EXISV.ONHAND)*(EXISV.AVERAGECOST)','SA059956','N','N','','EIS_XXWC_INV_SPECIAL_V','','','','US','');
--Inserting Report Parameters - Specials Report
xxeis.eis_rsc_ins.rp( 'Specials Report',401,'Organization','Organization','ORGANIZATION_CODE','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_SPECIAL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Specials Report',401,'Select Items for report','Select Items for report','','IN','INV SPECIALONHAND TYPE','''ALL specials''','VARCHAR2','N','Y','2','N','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_INV_SPECIAL_V','','','US','');
--Inserting Dependent Parameters - Specials Report
--Inserting Report Conditions - Specials Report
xxeis.eis_rsc_ins.rcnh( 'Specials Report',401,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND ((:Select Items for report = ''Include Specials with Oh Hand > 7 days''
          AND  (sysdate -EXISV.date_received) >7 AnD  EXISV.ONHAND > 0)
     OR (:Select Items for report =''Include Specials with Oh Hand < 7 days''
          AND  (sysdate -EXISV.date_received) <= 7 AnD  EXISV.ONHAND > 0)
     OR (:Select Items for report =''ALL specials'')
     )','1',401,'Specials Report','Free Text ');
--Inserting Report Sorts - Specials Report
xxeis.eis_rsc_ins.rs( 'Specials Report',401,'ORGANIZATION_CODE','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'Specials Report',401,'PART_NUMBER','ASC','SA059956','2','');
--Inserting Report Triggers - Specials Report
xxeis.eis_rsc_ins.rt( 'Specials Report',401,'BEGIN
xxeis.EIS_RS_XXWC_COM_UTIL_PKG_EXTN.g_process_id := :SYSTEM.PROCESS_ID;
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG_EXTN.SPECIAL_REP_PRC(
P_PROCESS_ID => :SYSTEM.PROCESS_ID,
P_ORGANIZATION_CODE => :Organization);
END;','B','Y','SA059956','AQ');
--inserting report templates - Specials Report
--Inserting Report Portals - Specials Report
xxeis.eis_rsc_ins.r_port( 'Specials Report','XXWC_PUR_TOP_RPTS','401','Specials Report','Specials On-Hand','OA.jsp?page=/eis/oracle/apps/xxeis/central/reporting/webui/EISRSCLaunchPG&EisProduct=Reporting&Portal=Yes&mod=Inventory','','EXCEL,','CONC','N','SA059956');
--inserting report dashboards - Specials Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Specials Report','401','EIS_XXWC_INV_SPECIAL_V','EIS_XXWC_INV_SPECIAL_V','N','');
--inserting report security - Specials Report
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','HDS_INVNTRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_AO_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','20005','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_INVENTORY_CONTROL_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_INV_PLANNER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_INVENTORY_SPEC_SCC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_INVENTORY_SUPER_USER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','INVENTORY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_RECEIVING_ASSOCIATE',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_AO_BIN_MTN_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_AO_BIN_MTN_CYCLE_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_AO_INV_ADJ_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','660','','XXWC_AO_OEENTRY_REC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_AO_BIN_MTN_CYCLE_INV_ADJ',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','707','','XXWC_COST_MANAGEMENT_INQ',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_DATA_MNGT_SC',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','401','','XXWC_AO_INV_ADJ_PO_RPT',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','201','','XXWC_PURCHASING_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Specials Report','201','','XXWC_PURCHASING_BUYER',401,'SA059956','','','');
--Inserting Report Pivots - Specials Report
xxeis.eis_rsc_ins.rpivot( 'Specials Report',401,'Onhand By Born date and Item','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Onhand By Born date and Item
xxeis.eis_rsc_ins.rpivot_dtls( 'Specials Report',401,'Onhand By Born date and Item','ORGANIZATION_CODE','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Specials Report',401,'Onhand By Born date and Item','ONHAND','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Specials Report',401,'Onhand By Born date and Item','OLDEST_BORN_DATE','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Specials Report',401,'Onhand By Born date and Item','PART_NUMBER','ROW_FIELD','','','2','1','');
--Inserting Report Summary Calculation Columns For Pivot- Onhand By Born date and Item
--Inserting Report   Version details- Specials Report
xxeis.eis_rsc_ins.rv( 'Specials Report','','Specials Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
