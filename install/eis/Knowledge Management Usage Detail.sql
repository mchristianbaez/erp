--Report Name            : Knowledge Management Usage Detail
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_681488_NTXJVQ_V
--Connecting to schema APPS
accept SID prompt "Please enter database ( connect string )  : "
accept apps_password prompt "Please enter APPS password : " hide
connect APPS/@
set scan off define off
prompt Creating View APPS.XXEIS_681488_NTXJVQ_V
Create or replace View APPS.XXEIS_681488_NTXJVQ_V
 AS 
SELECT
  soln.set_number SOLUTION_NUMBER
 ,soln.name SOLUTION_NAME
 ,hist.USED_TYPE ACTION
 ,fu.user_name
 ,fu.description
 ,fu.EMAIL_ADDRESS
 ,fru_loc.lob_branch branch
 ,orgs.region
 ,hist.creation_date
    "TIME_STAMP"
FROM
  apps.cs_kb_set_used_hists hist
 ,apps.cs_kb_sets_vl soln
 ,apps.fnd_logins fl
 ,apps.fnd_user fu
 ,(SELECT
     DISTINCT u.user_name
             ,gc.SEGMENT1 LOB
             ,gc.SEGMENT2 location_code
   FROM
     apps.per_people_f per
     INNER JOIN apps.per_all_assignments_f ass
       ON ass.person_id = per.person_id
     INNER JOIN apps.gl_code_combinations gc
       ON gc.code_combination_id = ass.default_code_comb_id
     INNER JOIN apps.fnd_user u
       ON u.employee_id = per.person_id
   WHERE 
     NVL(per.effective_end_date, SYSDATE + 1) > SYSDATE AND
     NVL(ass.effective_end_date, SYSDATE +1) > SYSDATE AND 
     NVL(u.end_date, SYSDATE + 1) > SYSDATE) ass
  , (select distinct FRU, PROPERTY_ID, LOB_BRANCH from apxcmmn.HDS_FRULOC_VW@apxprd_lnk.hsi.hughessupply.com) fru_loc
  , (select distinct attribute9 region, organization_code from inv.mtl_parameters) orgs
WHERE
  hist.set_id = soln.set_id 
  AND hist.last_update_login = fl.login_id 
  AND fl.user_id = fu.user_id 
  AND ass.user_name = fu.USER_NAME
  AND fru_loc.property_id = ass.location_code
  AND orgs.organization_code = fru_loc.lob_branch
  AND hist.creation_date between sysdate - 500 and sysdate
order by
  8 desc

/
set scan on define on
prompt Creating View Data for Knowledge Management Usage Detail
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_681488_NTXJVQ_V
xxeis.eis_rs_ins.v( 'XXEIS_681488_NTXJVQ_V',85000,'Paste SQL View for Knowledge Management Usage Detail','1.0','','','10011289','APPS','Knowledge Management Usage Detail View','X6NV','','');
--Delete View Columns for XXEIS_681488_NTXJVQ_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_681488_NTXJVQ_V',85000,FALSE);
--Inserting View Columns for XXEIS_681488_NTXJVQ_V
xxeis.eis_rs_ins.vc( 'XXEIS_681488_NTXJVQ_V','SOLUTION_NUMBER',85000,'','','','','','10011289','VARCHAR2','','','Solution Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_681488_NTXJVQ_V','SOLUTION_NAME',85000,'','','','','','10011289','VARCHAR2','','','Solution Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_681488_NTXJVQ_V','ACTION',85000,'','','','','','10011289','VARCHAR2','','','Action','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_681488_NTXJVQ_V','USER_NAME',85000,'','','','','','10011289','VARCHAR2','','','User Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_681488_NTXJVQ_V','DESCRIPTION',85000,'','','','','','10011289','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_681488_NTXJVQ_V','EMAIL_ADDRESS',85000,'','','','','','10011289','VARCHAR2','','','Email Address','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_681488_NTXJVQ_V','BRANCH',85000,'','','','','','10011289','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_681488_NTXJVQ_V','REGION',85000,'','','','','','10011289','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_681488_NTXJVQ_V','TIME_STAMP',85000,'','','','','','10011289','DATE','','','Time Stamp','','','');
--Inserting View Components for XXEIS_681488_NTXJVQ_V
--Inserting View Component Joins for XXEIS_681488_NTXJVQ_V
END;
/
set scan on define on
prompt Creating Report Data for Knowledge Management Usage Detail
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Knowledge Management Usage Detail
xxeis.eis_rs_utility.delete_report_rows( 'Knowledge Management Usage Detail' );
--Inserting Report - Knowledge Management Usage Detail
xxeis.eis_rs_ins.r( 85000,'Knowledge Management Usage Detail','','Report shows the detailed usage of KM solutions. It shows each time a user has viewed or rated a solution. Created by the WC IT group but the data is applicable to all users.','','','','10011289','XXEIS_681488_NTXJVQ_V','Y','','/*Report shows the detailed usage of KM solutions. It shows each time a user has viewed or rated a solution.*/
SELECT
  soln.set_number SOLUTION_NUMBER
 ,soln.name SOLUTION_NAME
 ,hist.USED_TYPE ACTION
 ,fu.user_name
 ,fu.description
 ,fu.EMAIL_ADDRESS
 ,fru_loc.lob_branch branch
 ,orgs.region
 ,hist.creation_date
    "TIME_STAMP"
FROM
  apps.cs_kb_set_used_hists hist
 ,apps.cs_kb_sets_vl soln
 ,apps.fnd_logins fl
 ,apps.fnd_user fu
 ,(SELECT
     DISTINCT u.user_name
             ,gc.SEGMENT1 LOB
             ,gc.SEGMENT2 location_code
   FROM
     apps.per_people_f per
     INNER JOIN apps.per_all_assignments_f ass
       ON ass.person_id = per.person_id
     INNER JOIN apps.gl_code_combinations gc
       ON gc.code_combination_id = ass.default_code_comb_id
     INNER JOIN apps.fnd_user u
       ON u.employee_id = per.person_id
   WHERE 
     NVL(per.effective_end_date, SYSDATE + 1) > SYSDATE AND
     NVL(ass.effective_end_date, SYSDATE +1) > SYSDATE AND 
     NVL(u.end_date, SYSDATE + 1) > SYSDATE) ass
  , (select distinct FRU, PROPERTY_ID, LOB_BRANCH from apxcmmn.HDS_FRULOC_VW@apxprd_lnk.hsi.hughessupply.com) fru_loc
  , (select distinct attribute9 region, organization_code from inv.mtl_parameters) orgs
WHERE
  hist.set_id = soln.set_id 
  AND hist.last_update_login = fl.login_id 
  AND fl.user_id = fu.user_id 
  AND ass.user_name = fu.USER_NAME
  AND fru_loc.property_id = ass.location_code
  AND orgs.organization_code = fru_loc.lob_branch
  AND hist.creation_date between sysdate - 500 and sysdate
order by
  8 desc
','10011289','','N','WC Knowledge Management Reports','','Pivot Excel,','');
--Inserting Report Columns - Knowledge Management Usage Detail
xxeis.eis_rs_ins.rc( 'Knowledge Management Usage Detail',85000,'SOLUTION_NAME','Solution Name','','','','default','','2','N','','','','','','','','10011289','N','N','','XXEIS_681488_NTXJVQ_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Usage Detail',85000,'DESCRIPTION','Description','','','','default','','3','N','','','','','','','','10011289','N','N','','XXEIS_681488_NTXJVQ_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Usage Detail',85000,'EMAIL_ADDRESS','Email Address','','','','default','','5','N','','','','','','','','10011289','N','N','','XXEIS_681488_NTXJVQ_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Usage Detail',85000,'SOLUTION_NUMBER','Solution Number','','','','default','','1','N','','','','','','','','10011289','N','N','','XXEIS_681488_NTXJVQ_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Usage Detail',85000,'USER_NAME','User Name','','','','default','','4','N','','','','','','','','10011289','N','N','','XXEIS_681488_NTXJVQ_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Usage Detail',85000,'ACTION','Action','','','','default','','8','N','','','','','','','','10011289','N','N','','XXEIS_681488_NTXJVQ_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Usage Detail',85000,'BRANCH','Branch','','','','default','','10','','Y','','','','','','','10011289','N','N','','XXEIS_681488_NTXJVQ_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Usage Detail',85000,'REGION','Region','','','','default','','11','','Y','','','','','','','10011289','N','N','','XXEIS_681488_NTXJVQ_V','','');
xxeis.eis_rs_ins.rc( 'Knowledge Management Usage Detail',85000,'TIME_STAMP','Time Stamp','','','MM/DD/RR HH24:MI','default','','12','','Y','','','','','','','10011289','N','N','','XXEIS_681488_NTXJVQ_V','','');
--Inserting Report Parameters - Knowledge Management Usage Detail
xxeis.eis_rs_ins.rp( 'Knowledge Management Usage Detail',85000,'Time Stamp','Start of reporting time period','TIME_STAMP','>=','','select current_timestamp - 14 from dual','DATE','Y','Y','1','','Y','SQL','10011289','Y','N','','','');
--Inserting Report Conditions - Knowledge Management Usage Detail
xxeis.eis_rs_ins.rcn( 'Knowledge Management Usage Detail',85000,'TIME_STAMP','>=',':Time Stamp','','','Y','1','Y','10011289');
--Inserting Report Sorts - Knowledge Management Usage Detail
xxeis.eis_rs_ins.rs( 'Knowledge Management Usage Detail',85000,'TIME_STAMP','DESC','10011289','','');
--Inserting Report Triggers - Knowledge Management Usage Detail
--Inserting Report Templates - Knowledge Management Usage Detail
--Inserting Report Portals - Knowledge Management Usage Detail
--Inserting Report Dashboards - Knowledge Management Usage Detail
--Inserting Report Security - Knowledge Management Usage Detail
xxeis.eis_rs_ins.rsec( 'Knowledge Management Usage Detail','20005','','50843',85000,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Knowledge Management Usage Detail','20005','','51207',85000,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Knowledge Management Usage Detail','20005','','50861',85000,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Knowledge Management Usage Detail','20005','','50900',85000,'10011289','','');
--Inserting Report Pivots - Knowledge Management Usage Detail
xxeis.eis_rs_ins.rpivot( 'Knowledge Management Usage Detail',85000,'By Solution','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - By Solution
xxeis.eis_rs_ins.rpivot_dtls( 'Knowledge Management Usage Detail',85000,'By Solution','ACTION','COLUMN_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Knowledge Management Usage Detail',85000,'By Solution','SOLUTION_NAME','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Knowledge Management Usage Detail',85000,'By Solution','REGION','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Knowledge Management Usage Detail',85000,'By Solution','TIME_STAMP','DATA_FIELD','COUNT','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- By Solution
END;
/
set scan on define on
