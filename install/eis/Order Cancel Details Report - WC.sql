--Report Name            : Order Cancel Details Report - WC
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_OM_ORDER_CANCEL_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_ORDER_CANCEL_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_ORDER_CANCEL_V',660,'','','','','ANONYMOUS','XXEIS','Eis Om Order Cancellations V','EOOCV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_ORDER_CANCEL_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_ORDER_CANCEL_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_ORDER_CANCEL_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','CUSTOMER',660,'Customer','CUSTOMER','','','','ANONYMOUS','VARCHAR2','','','Customer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','ANONYMOUS','VARCHAR2','','','Warehouse','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','POSTAL_CODE_BILL_TO',660,'Postal Code Bill To','POSTAL_CODE_BILL_TO','','','','ANONYMOUS','VARCHAR2','','','Postal Code Bill To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_COUNTY',660,'Bill To County','BILL_TO_COUNTY','','','','ANONYMOUS','VARCHAR2','','','Bill To County','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_STATE',660,'Bill To State','BILL_TO_STATE','','','','ANONYMOUS','VARCHAR2','','','Bill To State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_CITY',660,'Bill To City','BILL_TO_CITY','','','','ANONYMOUS','VARCHAR2','','','Bill To City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_ADDRESS4',660,'Bill To Address4','BILL_TO_ADDRESS4','','','','ANONYMOUS','VARCHAR2','','','Bill To Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_ADDRESS3',660,'Bill To Address3','BILL_TO_ADDRESS3','','','','ANONYMOUS','VARCHAR2','','','Bill To Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_ADDRESS2',660,'Bill To Address2','BILL_TO_ADDRESS2','','','','ANONYMOUS','VARCHAR2','','','Bill To Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_ADDRESS1',660,'Bill To Address1','BILL_TO_ADDRESS1','','','','ANONYMOUS','VARCHAR2','','','Bill To Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO',660,'Bill To','BILL_TO','','','','ANONYMOUS','VARCHAR2','','','Bill To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','POSTAL_CODE_SHIP_TO',660,'Postal Code Ship To','POSTAL_CODE_SHIP_TO','','','','ANONYMOUS','VARCHAR2','','','Postal Code Ship To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_COUNTY',660,'Ship To County','SHIP_TO_COUNTY','','','','ANONYMOUS','VARCHAR2','','','Ship To County','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_STATE',660,'Ship To State','SHIP_TO_STATE','','','','ANONYMOUS','VARCHAR2','','','Ship To State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_CITY',660,'Ship To City','SHIP_TO_CITY','','','','ANONYMOUS','VARCHAR2','','','Ship To City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_ADDRESS4',660,'Ship To Address4','SHIP_TO_ADDRESS4','','','','ANONYMOUS','VARCHAR2','','','Ship To Address4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_ADDRESS3',660,'Ship To Address3','SHIP_TO_ADDRESS3','','','','ANONYMOUS','VARCHAR2','','','Ship To Address3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_ADDRESS2',660,'Ship To Address2','SHIP_TO_ADDRESS2','','','','ANONYMOUS','VARCHAR2','','','Ship To Address2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_ADDRESS1',660,'Ship To Address1','SHIP_TO_ADDRESS1','','','','ANONYMOUS','VARCHAR2','','','Ship To Address1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO',660,'Ship To','SHIP_TO','','','','ANONYMOUS','VARCHAR2','','','Ship To','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','ANONYMOUS','VARCHAR2','','','Order Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','CHARGE_PERIODICITY_CODE',660,'Charge Periodicity Code','CHARGE_PERIODICITY_CODE','','','','ANONYMOUS','VARCHAR2','','','Charge Periodicity Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HIST_CREATED_BY',660,'Hist Created By','HIST_CREATED_BY','','','','ANONYMOUS','NUMBER','','','Hist Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','ANONYMOUS','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','LAST_UPDATE_DATE',660,'Last Update Date','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','','','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','CANCELLED_QUANTITY',660,'Cancelled Quantity','CANCELLED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Cancelled Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HIST_CREATION_DATE',660,'Hist Creation Date','HIST_CREATION_DATE','','','','ANONYMOUS','DATE','','','Hist Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OPERATING_UNIT',660,'Operating Unit','OPERATING_UNIT','','','','ANONYMOUS','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','ANONYMOUS','DATE','','','Ordered Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','ITEM',660,'Item','ITEM','','','','ANONYMOUS','VARCHAR2','','','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HIST_COMMENTS',660,'Hist Comments','HIST_COMMENTS','','','','ANONYMOUS','VARCHAR2','','','Hist Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','CANCEL_REASON',660,'Cancel Reason','CANCEL_REASON','','','','ANONYMOUS','VARCHAR2','','','Cancel Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','TRANSACTION_TYPE_ID',660,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','ANONYMOUS','NUMBER','','','Transaction Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','ANONYMOUS','NUMBER','','','Cust Account Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','CUST_ACCT_SITE_ID',660,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','ANONYMOUS','NUMBER','','','Cust Acct Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','PARTY_SITE_ID',660,'Party Site Id','PARTY_SITE_ID','','','','ANONYMOUS','NUMBER','','','Party Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','ANONYMOUS','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','LOCATION_ID',660,'Location Id','LOCATION_ID','','','','ANONYMOUS','NUMBER','','','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIPPING_ORG_ID',660,'Shipping Org Id','SHIPPING_ORG_ID','','','','ANONYMOUS','NUMBER','','','Shipping Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','ORDER_HEADER_ID',660,'Order Header Id','ORDER_HEADER_ID','','','','ANONYMOUS','NUMBER','','','Order Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','ORDER_LINE_ID',660,'Order Line Id','ORDER_LINE_ID','','','','ANONYMOUS','NUMBER','','','Order Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','LINE_ID',660,'Line Id','LINE_ID','','','','ANONYMOUS','NUMBER','','','Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','ORDER_TYPE_ID',660,'Order Type Id','ORDER_TYPE_ID','','','','ANONYMOUS','NUMBER','','','Order Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','ORG_ID',660,'Org Id','ORG_ID','','','','ANONYMOUS','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','ANONYMOUS','NUMBER','','','Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','LAST_UPDATED_BY',660,'Last Updated By','LAST_UPDATED_BY','','','','ANONYMOUS','VARCHAR2','','','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_FROM_ORG_ID',660,'Ship From Org Id','SHIP_FROM_ORG_ID','','','','ANONYMOUS','NUMBER','','','Ship From Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','TRANSACTION_TYPE',660,'Transaction Type','TRANSACTION_TYPE','','','','ANONYMOUS','VARCHAR2','','','Transaction Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HROU_ORGANIZATION_ID',660,'Hrou Organization Id','HROU_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Hrou Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU_STE_USE_ID',660,'Ship To Hcsu Ste Use Id','SHIP_TO_HCSU_STE_USE_ID','','','','ANONYMOUS','NUMBER','','','Ship To Hcsu Ste Use Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU_SITE_ID',660,'Bill To Hcsu Site Id','BILL_TO_HCSU_SITE_ID','','','','ANONYMOUS','NUMBER','','','Bill To Hcsu Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Print_Prices_on',660,'Descriptive flexfield (DFF): Address Information Column Name: Print Prices on Order','BILL_TO_HCAS#Print_Prices_on','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE1','Bill To Hcas#Print Prices On Order','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Print_Prices_on',660,'Descriptive flexfield (DFF): Address Information Column Name: Print Prices on Order','SHIP_TO_HCAS#Print_Prices_on','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE1','Ship To Hcas#Print Prices On Order','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Job_Information',660,'Descriptive flexfield (DFF): Address Information Column Name: Job Information on File?','SHIP_TO_HCAS#Job_Information','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE14','Ship To Hcas#Job Information On File?','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Job_Information',660,'Descriptive flexfield (DFF): Address Information Column Name: Job Information on File?','BILL_TO_HCAS#Job_Information','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE14','Bill To Hcas#Job Information On File?','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Tax_Exemption_T',660,'Descriptive flexfield (DFF): Address Information Column Name: Tax Exemption Type','BILL_TO_HCAS#Tax_Exemption_T','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE15','Bill To Hcas#Tax Exemption Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Tax_Exemption_T',660,'Descriptive flexfield (DFF): Address Information Column Name: Tax Exemption Type','SHIP_TO_HCAS#Tax_Exemption_T','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE15','Ship To Hcas#Tax Exemption Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Tax_Exempt1',660,'Descriptive flexfield (DFF): Address Information Column Name: Tax Exempt','BILL_TO_HCAS#Tax_Exempt1','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE16','Bill To Hcas#Tax Exempt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Tax_Exempt1',660,'Descriptive flexfield (DFF): Address Information Column Name: Tax Exempt','SHIP_TO_HCAS#Tax_Exempt1','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE16','Ship To Hcas#Tax Exempt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#PRISM_Number',660,'Descriptive flexfield (DFF): Address Information Column Name: PRISM Number','BILL_TO_HCAS#PRISM_Number','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE17','Bill To Hcas#Prism Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#PRISM_Number',660,'Descriptive flexfield (DFF): Address Information Column Name: PRISM Number','SHIP_TO_HCAS#PRISM_Number','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE17','Ship To Hcas#Prism Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Mandatory_PO_Nu',660,'Descriptive flexfield (DFF): Address Information Column Name: Mandatory PO Number','SHIP_TO_HCAS#Mandatory_PO_Nu','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE3','Ship To Hcas#Mandatory Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Mandatory_PO_Nu',660,'Descriptive flexfield (DFF): Address Information Column Name: Mandatory PO Number','BILL_TO_HCAS#Mandatory_PO_Nu','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE3','Bill To Hcas#Mandatory Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Lien_Release_Da',660,'Descriptive flexfield (DFF): Address Information Column Name: Lien Release Date','BILL_TO_HCAS#Lien_Release_Da','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE5','Bill To Hcas#Lien Release Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Lien_Release_Da',660,'Descriptive flexfield (DFF): Address Information Column Name: Lien Release Date','SHIP_TO_HCAS#Lien_Release_Da','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE5','Ship To Hcas#Lien Release Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Yes#Notice_to_O',660,'Descriptive flexfield (DFF): Address Information Column Name: Notice to Owner-Job Total Context: Yes','SHIP_TO_HCAS#Yes#Notice_to_O','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE18','Ship To Hcas#Yes#Notice To Owner-Job Total','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Yes#Notice_to_O',660,'Descriptive flexfield (DFF): Address Information Column Name: Notice to Owner-Job Total Context: Yes','BILL_TO_HCAS#Yes#Notice_to_O','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE18','Bill To Hcas#Yes#Notice To Owner-Job Total','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Yes#Notice_to_O1',660,'Descriptive flexfield (DFF): Address Information Column Name: Notice to Owner-Prelim Notice Context: Yes','SHIP_TO_HCAS#Yes#Notice_to_O1','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE19','Ship To Hcas#Yes#Notice To Owner-Prelim Notice','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Yes#Notice_to_O1',660,'Descriptive flexfield (DFF): Address Information Column Name: Notice to Owner-Prelim Notice Context: Yes','BILL_TO_HCAS#Yes#Notice_to_O1','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE19','Bill To Hcas#Yes#Notice To Owner-Prelim Notice','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Yes#Notice_to_O2',660,'Descriptive flexfield (DFF): Address Information Column Name: Notice to Owner-Prelim Date Context: Yes','BILL_TO_HCAS#Yes#Notice_to_O2','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE20','Bill To Hcas#Yes#Notice To Owner-Prelim Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Yes#Notice_to_O2',660,'Descriptive flexfield (DFF): Address Information Column Name: Notice to Owner-Prelim Date Context: Yes','SHIP_TO_HCAS#Yes#Notice_to_O2','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE20','Ship To Hcas#Yes#Notice To Owner-Prelim Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#Customer_Site_C',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Customer Site Classification','SHIP_TO_HCSU#Customer_Site_C','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE1','Ship To Hcsu#Customer Site Classification','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#Customer_Site_C',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Customer Site Classification','BILL_TO_HCSU#Customer_Site_C','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE1','Bill To Hcsu#Customer Site Classification','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#Government_Fund',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Government Funded?','BILL_TO_HCSU#Government_Fund','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE2','Bill To Hcsu#Government Funded?','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#Government_Fund',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Government Funded?','SHIP_TO_HCSU#Government_Fund','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE2','Ship To Hcsu#Government Funded?','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#Thomas_Guide_Pa',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Thomas Guide Page','BILL_TO_HCSU#Thomas_Guide_Pa','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE3','Bill To Hcsu#Thomas Guide Page','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#Thomas_Guide_Pa',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Thomas Guide Page','SHIP_TO_HCSU#Thomas_Guide_Pa','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE3','Ship To Hcsu#Thomas Guide Page','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#Dodge_Number',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Dodge Number','SHIP_TO_HCSU#Dodge_Number','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE4','Ship To Hcsu#Dodge Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#Dodge_Number',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Dodge Number','BILL_TO_HCSU#Dodge_Number','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE4','Bill To Hcsu#Dodge Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#FUTURE_USE',660,'Descriptive flexfield (DFF): Site Use Information Column Name: FUTURE USE','BILL_TO_HCSU#FUTURE_USE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE5','Bill To Hcsu#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#FUTURE_USE',660,'Descriptive flexfield (DFF): Site Use Information Column Name: FUTURE USE','SHIP_TO_HCSU#FUTURE_USE','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE5','Ship To Hcsu#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#Salesrep_#2',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Salesrep #2','BILL_TO_HCSU#Salesrep_#2','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE6','Bill To Hcsu#Salesrep #2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#Salesrep_#2',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Salesrep #2','SHIP_TO_HCSU#Salesrep_#2','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE6','Ship To Hcsu#Salesrep #2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#Salesrep_Spilt_',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Salesrep Spilt #1','SHIP_TO_HCSU#Salesrep_Spilt_','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE7','Ship To Hcsu#Salesrep Spilt #1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#Salesrep_Spilt_',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Salesrep Spilt #1','BILL_TO_HCSU#Salesrep_Spilt_','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE7','Bill To Hcsu#Salesrep Spilt #1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#Salesrep_Spilt_1',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Salesrep Spilt #2','BILL_TO_HCSU#Salesrep_Spilt_1','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE8','Bill To Hcsu#Salesrep Spilt #2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#Salesrep_Spilt_1',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Salesrep Spilt #2','SHIP_TO_HCSU#Salesrep_Spilt_1','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE8','Ship To Hcsu#Salesrep Spilt #2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#Permit_Number',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Permit Number','BILL_TO_HCSU#Permit_Number','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE9','Bill To Hcsu#Permit Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#Permit_Number',660,'Descriptive flexfield (DFF): Site Use Information Column Name: Permit Number','SHIP_TO_HCSU#Permit_Number','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE9','Ship To Hcsu#Permit Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HL#Pay_To_Vendor_ID',660,'Descriptive flexfield (DFF): TCA Location Information Column Name: Pay To Vendor ID','BILL_TO_HL#Pay_To_Vendor_ID','','','','ANONYMOUS','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE1','Bill To Hl#Pay To Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HL#Pay_To_Vendor_ID',660,'Descriptive flexfield (DFF): TCA Location Information Column Name: Pay To Vendor ID','SHIP_TO_HL#Pay_To_Vendor_ID','','','','ANONYMOUS','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE1','Ship To Hl#Pay To Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HL#LOB',660,'Descriptive flexfield (DFF): TCA Location Information Column Name: LOB','SHIP_TO_HL#LOB','','','','ANONYMOUS','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE2','Ship To Hl#Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HL#LOB',660,'Descriptive flexfield (DFF): TCA Location Information Column Name: LOB','BILL_TO_HL#LOB','','','','ANONYMOUS','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE2','Bill To Hl#Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HL#Pay_To_Vendor_Cod',660,'Descriptive flexfield (DFF): TCA Location Information Column Name: Pay To Vendor Code','SHIP_TO_HL#Pay_To_Vendor_Cod','','','','ANONYMOUS','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE3','Ship To Hl#Pay To Vendor Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HL#Pay_To_Vendor_Cod',660,'Descriptive flexfield (DFF): TCA Location Information Column Name: Pay To Vendor Code','BILL_TO_HL#Pay_To_Vendor_Cod','','','','ANONYMOUS','VARCHAR2','HZ_LOCATIONS','ATTRIBUTE3','Bill To Hl#Pay To Vendor Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HPS#Rebt_Pay_to_Vndr',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt Pay to Vndr ID','SHIP_TO_HPS#Rebt_Pay_to_Vndr','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE2','Ship To Hps#Rebt Pay To Vndr Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HPS#Rebt_Pay_to_Vndr',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt Pay to Vndr ID','BILL_TO_HPS#Rebt_Pay_to_Vndr','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE2','Bill To Hps#Rebt Pay To Vndr Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HPS#Rebt_LOB',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt LOB','BILL_TO_HPS#Rebt_LOB','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE3','Bill To Hps#Rebt Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HPS#Rebt_LOB',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt LOB','SHIP_TO_HPS#Rebt_LOB','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE3','Ship To Hps#Rebt Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HPS#Rebt_Vndr_Code',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt Vndr Code','BILL_TO_HPS#Rebt_Vndr_Code','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE4','Bill To Hps#Rebt Vndr Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HPS#Rebt_Vndr_Code',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt Vndr Code','SHIP_TO_HPS#Rebt_Vndr_Code','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE4','Ship To Hps#Rebt Vndr Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HPS#Rebt_Vndr_Flag',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt Vndr Flag','SHIP_TO_HPS#Rebt_Vndr_Flag','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE5','Ship To Hps#Rebt Vndr Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HPS#Rebt_Vndr_Flag',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt Vndr Flag','BILL_TO_HPS#Rebt_Vndr_Flag','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE5','Bill To Hps#Rebt Vndr Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HPS#Rebt_Vndr_Tax_ID',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt Vndr Tax ID Nbr','SHIP_TO_HPS#Rebt_Vndr_Tax_ID','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE6','Ship To Hps#Rebt Vndr Tax Id Nbr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HPS#Rebt_Vndr_Tax_ID',660,'Descriptive flexfield (DFF): Party Site Information Column Name: Rebt Vndr Tax ID Nbr','BILL_TO_HPS#Rebt_Vndr_Tax_ID','','','','ANONYMOUS','VARCHAR2','HZ_PARTY_SITES','ATTRIBUTE6','Bill To Hps#Rebt Vndr Tax Id Nbr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Factory_Planner_Data_Dir',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Factory Planner Data Directory','MTP#Factory_Planner_Data_Dir','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE1','Mtp#Factory Planner Data Directory','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#FRU',660,'Descriptive flexfield (DFF): Organization parameters Column Name: FRU','MTP#FRU','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE10','Mtp#Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Location_Number',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Location Number','MTP#Location_Number','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE11','Mtp#Location Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Branch_Operations_Manage',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Branch Operations Manager','MTP#Branch_Operations_Manage','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE13','Mtp#Branch Operations Manager','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Factory_Planner_Executab',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Factory Planner Executable Directory','MTP#Factory_Planner_Executab','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE2','Mtp#Factory Planner Executable Directory','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Factory_Planner_User',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Factory Planner User','MTP#Factory_Planner_User','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE3','Mtp#Factory Planner User','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Factory_Planner_Host',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Factory Planner Host','MTP#Factory_Planner_Host','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE4','Mtp#Factory Planner Host','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Factory_Planner_Port_Num',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Factory Planner Port Number','MTP#Factory_Planner_Port_Num','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE5','Mtp#Factory Planner Port Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Pricing_Zone',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Pricing Zone','MTP#Pricing_Zone','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE6','Mtp#Pricing Zone','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Org_Type',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Org Type','MTP#Org_Type','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE7','Mtp#Org Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#District',660,'Descriptive flexfield (DFF): Organization parameters Column Name: District','MTP#District','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE8','Mtp#District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Region',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Region','MTP#Region','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE9','Mtp#Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#Rental_Term',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: Rental Term','OH#Rental_Term','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE1','Oh#Rental Term','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#Order_Channel',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: Order Channel','OH#Order_Channel','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE2','Oh#Order Channel','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#Picked_By',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: Picked By','OH#Picked_By','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE3','Oh#Picked By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#Load_Checked_By',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: Load Checked By','OH#Load_Checked_By','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE4','Oh#Load Checked By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#Delivered_By',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: Delivered By','OH#Delivered_By','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE5','Oh#Delivered By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Estimated_Return_Date',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Estimated Return Date','OL#Estimated_Return_Date','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE1','Ol#Estimated Return Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#ReRent_PO',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: ReRent PO','OL#ReRent_PO','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE10','Ol#Rerent Po','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Force_Ship',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Force Ship','OL#Force_Ship','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE11','Ol#Force Ship','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Application_Method',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Application Method','OL#Application_Method','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE13','Ol#Application Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Item_on_Blowout',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Item on Blowout?','OL#Item_on_Blowout','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE15','Ol#Item On Blowout?','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#POF_Std_Line',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: POF Std Line','OL#POF_Std_Line','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE19','Ol#Pof Std Line','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#ReRental_Billing_Terms',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: ReRental Billing Terms','OL#ReRental_Billing_Terms','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE2','Ol#Rerental Billing Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Pricing_Guardrail',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Pricing Guardrail','OL#Pricing_Guardrail','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE20','Ol#Pricing Guardrail','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Print_Expired_Product_Dis',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Print Expired Product Disclaim','OL#Print_Expired_Product_Dis','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE3','Ol#Print Expired Product Disclaim','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Rental_Charge',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Rental Charge','OL#Rental_Charge','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE4','Ol#Rental Charge','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Vendor_Quote_Cost',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Vendor Quote Cost','OL#Vendor_Quote_Cost','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE5','Ol#Vendor Quote Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#PO_Cost_for_Vendor_Quote',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: PO Cost for Vendor Quote','OL#PO_Cost_for_Vendor_Quote','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE6','Ol#Po Cost For Vendor Quote','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Serial_Number',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Serial Number','OL#Serial_Number','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE7','Ol#Serial Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Engineering_Cost',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Engineering Cost','OL#Engineering_Cost','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE8','Ol#Engineering Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','LAST_MODIFIED_BY',660,'Last Modified By','LAST_MODIFIED_BY','','','','ANONYMOUS','VARCHAR2','','','Last Modified By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Joint_Check_Agr',660,'Descriptive flexfield (DFF): Address Information Column Name: Joint Check Agreement','BILL_TO_HCAS#Joint_Check_Agr','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE12','Bill To Hcas#Joint Check Agreement','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Joint_Check_Agr',660,'Descriptive flexfield (DFF): Address Information Column Name: Joint Check Agreement','SHIP_TO_HCAS#Joint_Check_Agr','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE12','Ship To Hcas#Joint Check Agreement','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCAS#Owner_Financed',660,'Descriptive flexfield (DFF): Address Information Column Name: Owner Financed','BILL_TO_HCAS#Owner_Financed','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE13','Bill To Hcas#Owner Financed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCAS#Owner_Financed',660,'Descriptive flexfield (DFF): Address Information Column Name: Owner Financed','SHIP_TO_HCAS#Owner_Financed','','','','ANONYMOUS','VARCHAR2','HZ_CUST_ACCT_SITES_ALL','ATTRIBUTE13','Ship To Hcas#Owner Financed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#B2B_Address',660,'Descriptive flexfield (DFF): Site Use Information Column Name: B2B Address','BILL_TO_HCSU#B2B_Address','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE10','Bill To Hcsu#B2b Address','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#B2B_Address',660,'Descriptive flexfield (DFF): Site Use Information Column Name: B2B Address','SHIP_TO_HCSU#B2B_Address','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE10','Ship To Hcsu#B2b Address','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','SHIP_TO_HCSU#B2B_Shipping_Wa',660,'Descriptive flexfield (DFF): Site Use Information Column Name: B2B Shipping Warehouse','SHIP_TO_HCSU#B2B_Shipping_Wa','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE11','Ship To Hcsu#B2b Shipping Warehouse','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','BILL_TO_HCSU#B2B_Shipping_Wa',660,'Descriptive flexfield (DFF): Site Use Information Column Name: B2B Shipping Warehouse','BILL_TO_HCSU#B2B_Shipping_Wa','','','','ANONYMOUS','VARCHAR2','HZ_CUST_SITE_USES_ALL','ATTRIBUTE11','Bill To Hcsu#B2b Shipping Warehouse','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Party_Type',660,'Descriptive flexfield (DFF): Party Information Column Name: Party Type Context: 101','HZP#101#Party_Type','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Hzp#101#Party Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#InterCompany_Receiva',660,'Descriptive flexfield (DFF): Party Information Column Name: InterCompany Receivable Context: 101','HZP#101#InterCompany_Receiva','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE10','Hzp#101#Intercompany Receivable','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Coop_Override',660,'Descriptive flexfield (DFF): Party Information Column Name: Coop Override Context: 101','HZP#101#Coop_Override','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE11','Hzp#101#Coop Override','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Payment_Override',660,'Descriptive flexfield (DFF): Party Information Column Name: Payment Override Context: 101','HZP#101#Payment_Override','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE12','Hzp#101#Payment Override','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Default_Payment_Acco',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Payment Account Context: 101','HZP#101#Default_Payment_Acco','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE13','Hzp#101#Default Payment Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Default_Payment_Loca',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Payment Location Context: 101','HZP#101#Default_Payment_Loca','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE14','Hzp#101#Default Payment Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Collector',660,'Descriptive flexfield (DFF): Party Information Column Name: Collector Context: 101','HZP#101#Collector','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE3','Hzp#101#Collector','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Default_Product_Segm',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Product Segment Context: 101','HZP#101#Default_Product_Segm','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE4','Hzp#101#Default Product Segment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Default_Location_Seg',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Location Segment Context: 101','HZP#101#Default_Location_Seg','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE5','Hzp#101#Default Location Segment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Default_Coop_Account',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Coop Account Segment Context: 101','HZP#101#Default_Coop_Account','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE6','Hzp#101#Default Coop Account Segment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Default_Rebate_Accou',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Rebate Account Segment Context: 101','HZP#101#Default_Rebate_Accou','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE7','Hzp#101#Default Rebate Account Segment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#Default_Cost_Center',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Cost Center Context: 101','HZP#101#Default_Cost_Center','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE8','Hzp#101#Default Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#101#InterCompany__Payabl',660,'Descriptive flexfield (DFF): Party Information Column Name: InterCompany  Payable Context: 101','HZP#101#InterCompany__Payabl','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE9','Hzp#101#Intercompany  Payable','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Party_Type',660,'Descriptive flexfield (DFF): Party Information Column Name: Party Type Context: 102','HZP#102#Party_Type','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE1','Hzp#102#Party Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#InterCompany_Receiva',660,'Descriptive flexfield (DFF): Party Information Column Name: InterCompany Receivable Context: 102','HZP#102#InterCompany_Receiva','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE10','Hzp#102#Intercompany Receivable','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Coop_Override',660,'Descriptive flexfield (DFF): Party Information Column Name: Coop Override Context: 102','HZP#102#Coop_Override','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE11','Hzp#102#Coop Override','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Payment_Override',660,'Descriptive flexfield (DFF): Party Information Column Name: Payment Override Context: 102','HZP#102#Payment_Override','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE12','Hzp#102#Payment Override','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Default_Payment_Acco',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Payment Account Context: 102','HZP#102#Default_Payment_Acco','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE13','Hzp#102#Default Payment Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Default_Payment_Loca',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Payment Location Context: 102','HZP#102#Default_Payment_Loca','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE14','Hzp#102#Default Payment Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Collector',660,'Descriptive flexfield (DFF): Party Information Column Name: Collector Context: 102','HZP#102#Collector','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE3','Hzp#102#Collector','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Default_Product_Segm',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Product Segment Context: 102','HZP#102#Default_Product_Segm','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE4','Hzp#102#Default Product Segment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Default_Location_Seg',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Location Segment Context: 102','HZP#102#Default_Location_Seg','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE5','Hzp#102#Default Location Segment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Default_Coop_Account',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Coop Account Segment Context: 102','HZP#102#Default_Coop_Account','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE6','Hzp#102#Default Coop Account Segment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Default_Rebate_Accou',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Rebate Account Segment Context: 102','HZP#102#Default_Rebate_Accou','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE7','Hzp#102#Default Rebate Account Segment','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#Default_Cost_Center',660,'Descriptive flexfield (DFF): Party Information Column Name: Default Cost Center Context: 102','HZP#102#Default_Cost_Center','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE8','Hzp#102#Default Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZP#102#InterCompany_Payable',660,'Descriptive flexfield (DFF): Party Information Column Name: InterCompany Payable Context: 102','HZP#102#InterCompany_Payable','','','','ANONYMOUS','VARCHAR2','HZ_PARTIES','ATTRIBUTE9','Hzp#102#Intercompany Payable','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Deliver_Charge_Exempt',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Deliver Charge Exempt','MTP#Deliver_Charge_Exempt','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE14','Mtp#Deliver Charge Exempt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTP#Replenishment_Planning_L',660,'Descriptive flexfield (DFF): Organization parameters Column Name: Replenishment Planning Loc','MTP#Replenishment_Planning_L','','','','ANONYMOUS','VARCHAR2','MTL_PARAMETERS','ATTRIBUTE15','Mtp#Replenishment Planning Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#Authorized_Buyer',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: Authorized Buyer','OH#Authorized_Buyer','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE11','Oh#Authorized Buyer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#Authorized_Buyer_Notes',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: Authorized Buyer Notes','OH#Authorized_Buyer_Notes','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE12','Oh#Authorized Buyer Notes','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#Customer_Notes',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: Customer Notes','OH#Customer_Notes','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE13','Oh#Customer Notes','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#Order_Created_By',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: Order Created By','OH#Order_Created_By','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE7','Oh#Order Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OH#PRISM_Returns',660,'Descriptive flexfield (DFF): Additional Header Information Column Name: PRISM Returns','OH#PRISM_Returns','','','','ANONYMOUS','VARCHAR2','OE_ORDER_HEADERS_ALL','ATTRIBUTE8','Oh#Prism Returns','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Rental_Date',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Rental Date','OL#Rental_Date','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE12','Ol#Rental Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#GSA_Off_Contract_Notifica',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: GSA Off Contract Notification','OL#GSA_Off_Contract_Notifica','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE14','Ol#Gsa Off Contract Notification','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#Out_For_Delivery',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: Out For Delivery','OL#Out_For_Delivery','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE16','Ol#Out For Delivery','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#WC_Customer_Quote_Line_Se',660,'Descriptive flexfield (DFF): Additional Line Attribute Information Column Name: WC Customer Quote Line Seq','OL#WC_Customer_Quote_Line_Se','','','','ANONYMOUS','VARCHAR2','OE_ORDER_LINES_ALL','ATTRIBUTE18','Ol#Wc Customer Quote Line Seq','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#DO_NOT_USE_BKO_VISIBILI',660,'Ol#Do Not Use Bko Visibili','OL#DO_NOT_USE_BKO_VISIBILI','','','','ANONYMOUS','VARCHAR2','','','Ol#Do Not Use Bko Visibili','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_ORDER_CANCEL_V','OL#DO_NOT_USE_RENTAL_BILL_',660,'Ol#Do Not Use Rental Bill ','OL#DO_NOT_USE_RENTAL_BILL_','','','','ANONYMOUS','VARCHAR2','','','Ol#Do Not Use Rental Bill ','','','','');
--Inserting Object Components for EIS_XXWC_OM_ORDER_CANCEL_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','ANONYMOUS','ANONYMOUS','-1','Oe Order Headers All Stores Header Information For','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','ANONYMOUS','ANONYMOUS','-1','Oe Order Lines All Stores Information For All Orde','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HR_ORGANIZATION_UNITS',660,'HR_ALL_ORGANIZATION_UNITS','HROU','HROU','ANONYMOUS','ANONYMOUS','-1','HR_ALL_ORGANIZATION_UNITS','N','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_CUST_SITE_USES',660,'HZ_CUST_SITE_USES_ALL','SHIP_TO_HCSU','SHIP_TO_HCSU','ANONYMOUS','ANONYMOUS','-1','Stores Business Purposes Assigned To Customer Acco','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','SHIP_TO_HCAS','SHIP_TO_HCAS','ANONYMOUS','ANONYMOUS','-1','Stores All Customer Account Sites Across All Opera','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_PARTY_SITES',660,'HZ_PARTY_SITES','SHIP_TO_HPS','SHIP_TO_HPS','ANONYMOUS','ANONYMOUS','-1','Links Party To Physical Locations','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_LOCATIONS',660,'HZ_LOCATIONS','SHIP_TO_HL','SHIP_TO_HL','ANONYMOUS','ANONYMOUS','-1','Physical Addresses','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_CUST_SITE_USES',660,'HZ_CUST_SITE_USES_ALL','BILL_TO_HCSU','BILL_TO_HCSU','ANONYMOUS','ANONYMOUS','-1','Stores Business Purposes Assigned To Customer Acco','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','BILL_TO_HCAS','BILL_TO_HCAS','ANONYMOUS','ANONYMOUS','-1','Stores All Customer Account Sites Across All Opera','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_PARTY_SITES',660,'HZ_PARTY_SITES','BILL_TO_HPS','BILL_TO_HPS','ANONYMOUS','ANONYMOUS','-1','Links Party To Physical Locations','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_LOCATIONS',660,'HZ_LOCATIONS','BILL_TO_HL','BILL_TO_HL','ANONYMOUS','ANONYMOUS','-1','Physical Addresses','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','ANONYMOUS','ANONYMOUS','-1','Information About Parties Such As Organizations, P','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','ANONYMOUS','ANONYMOUS','-1','Inventory Control Options And Defaults','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_OM_ORDER_CANCEL_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','OE_ORDER_HEADERS','OH',660,'EOOCV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','OE_ORDER_LINES','OL',660,'EOOCV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HR_ORGANIZATION_UNITS','HROU',660,'EOOCV.HROU_ORGANIZATION_ID','=','HROU.ORGANIZATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_CUST_SITE_USES','SHIP_TO_HCSU',660,'EOOCV.SHIP_TO_HCSU_STE_USE_ID','=','SHIP_TO_HCSU.SITE_USE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_CUST_ACCT_SITES','SHIP_TO_HCAS',660,'EOOCV.CUST_ACCT_SITE_ID','=','SHIP_TO_HCAS.CUST_ACCT_SITE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_LOCATIONS','BILL_TO_HL',660,'EOOCV.LOCATION_ID','=','BILL_TO_HL.LOCATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_PARTIES','HZP',660,'EOOCV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_PARTY_SITES','SHIP_TO_HPS',660,'EOOCV.PARTY_SITE_ID','=','SHIP_TO_HPS.PARTY_SITE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_LOCATIONS','SHIP_TO_HL',660,'EOOCV.LOCATION_ID','=','SHIP_TO_HL.LOCATION_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_CUST_SITE_USES','BILL_TO_HCSU',660,'EOOCV.bill_to_hcsu_site_id','=','BILL_TO_HCSU.SITE_USE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_CUST_ACCT_SITES','BILL_TO_HCAS',660,'EOOCV.CUST_ACCT_SITE_ID','=','BILL_TO_HCAS.CUST_ACCT_SITE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','HZ_PARTY_SITES','BILL_TO_HPS',660,'EOOCV.PARTY_SITE_ID','=','BILL_TO_HPS.PARTY_SITE_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_OM_ORDER_CANCEL_V','MTL_PARAMETERS','MTP',660,'EOOCV.shipping_org_id','=','MTP.ORGANIZATION_ID(+)','','','','Y','ANONYMOUS');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Order Cancel Details Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Order Cancel Details Report - WC
xxeis.eis_rsc_ins.lov( 660,' SELECT OH.ORDER_NUMBER      ORDER_NUMBER,
        NVL(CUST.ACCOUNT_NUMBER, PARTY.PARTY_NUMBER)     CUSTOMER_NUMBER,
        NVL(CUST.ACCOUNT_NAME, PARTY.PARTY_NAME)     CUSTOMER_NAME,
        OH.FLOW_STATUS_CODE  STATUS,
        OH.ORDERED_DATE      ORDERED_DATE
FROM   OE_ORDER_HEADERS OH,
       HZ_PARTIES        PARTY,
       HZ_CUST_ACCOUNTS  CUST
WHERE PARTY.PARTY_ID=CUST.PARTY_ID
AND   CUST.CUST_ACCOUNT_ID =OH.SOLD_TO_ORG_ID','','OM ORDER NUMBER','This gives the Order Number','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_tl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT C.LOCATION  SHIP_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''SHIP_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID
','','OM SHIP TO LOCATION','This gives ship to locations','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT C.LOCATION  BILL_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''BILL_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID','','OM BILL TO LOCATION','This gives bill to locations','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select  distinct hou.name from hr_organization_units hou','','OM OPERATING UNIT','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select meaning cancel_reason,DESCRIPTION  from fnd_lookup_values_vl  where lookup_type=''CANCEL_CODE''','','OM CANCEL REASON','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Order Cancel Details Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Order Cancel Details Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Order Cancel Details Report - WC' );
--Inserting Report - Order Cancel Details Report - WC
xxeis.eis_rsc_ins.r( 660,'Order Cancel Details Report - WC','','This report gives the Sales order (Lines) information which are cancelled including
customer,cancelled item,cancelled quantity,Cancelled reason and cancelled date etc.','','','','SK026277','EIS_XXWC_OM_ORDER_CANCEL_V','Y','','','SK026277','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Order Cancel Details Report - WC
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'CUSTOMER','Customer','Customer','','','default','','1','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'HIST_COMMENTS','Hist Comments','Hist Comments','','','default','','27','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'HIST_CREATION_DATE','Hist Creation Date','Hist Creation Date','','','default','','30','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'ITEM','Item','Item','','','default','','23','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'LAST_UPDATE_DATE','Last Update Date','Last Update Date','','','default','','31','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'BILL_TO','Bill To','Bill To','','','default','','6','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'BILL_TO_ADDRESS1','Bill To Address1','Bill To Address1','','','default','','7','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'BILL_TO_ADDRESS2','Bill To Address2','Bill To Address2','','','default','','8','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'BILL_TO_ADDRESS3','Bill To Address3','Bill To Address3','','','default','','9','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'BILL_TO_ADDRESS4','Bill To Address4','Bill To Address4','','','default','','10','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'BILL_TO_CITY','Bill To City','Bill To City','','','default','','11','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'BILL_TO_COUNTY','Bill To County','Bill To County','','','default','','12','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'BILL_TO_STATE','Bill To State','Bill To State','','','default','','13','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'CANCELLED_QUANTITY','Cancelled Quantity','Cancelled Quantity','','~T~D~2','default','','24','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'CANCEL_REASON','Cancel Reason','Cancel Reason','','','default','','25','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'CHARGE_PERIODICITY_CODE','Charge Periodicity Code','Charge Periodicity Code','','','default','','26','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'CREATION_DATE','Creation Date','Creation Date','','','default','','28','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'OPERATING_UNIT','Operating Unit','Operating Unit','','','default','','2','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','4','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','3','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','5','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'POSTAL_CODE_BILL_TO','Postal Code Bill To','Postal Code Bill To','','','default','','32','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'POSTAL_CODE_SHIP_TO','Postal Code Ship To','Postal Code Ship To','','','default','','33','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'SHIP_TO','Ship To','Ship To','','','default','','14','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'SHIP_TO_ADDRESS1','Ship To Address1','Ship To Address1','','','default','','15','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'SHIP_TO_ADDRESS2','Ship To Address2','Ship To Address2','','','default','','16','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'SHIP_TO_ADDRESS3','Ship To Address3','Ship To Address3','','','default','','17','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'SHIP_TO_ADDRESS4','Ship To Address4','Ship To Address4','','','default','','18','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'SHIP_TO_CITY','Ship To City','Ship To City','','','default','','19','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'SHIP_TO_COUNTY','Ship To County','Ship To County','','','default','','20','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'SHIP_TO_STATE','Ship To State','Ship To State','','','default','','21','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'WAREHOUSE','Warehouse','Warehouse','','','default','','34','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'LINE_NUMBER','Line Number','Line Number','','','default','','22','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'LAST_UPDATED_BY','Last Updated By','Last Updated By','','','default','','35','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Order Cancel Details Report - WC',660,'LAST_MODIFIED_BY','Last Modified By','Last Modified By','','','default','','36','N','Y','','','','','','','SK026277','N','N','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','US','');
--Inserting Report Parameters - Order Cancel Details Report - WC
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Cancel Reason','Cancel Reason','CANCEL_REASON','IN','OM CANCEL REASON','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Bill To','','BILL_TO','IN','OM BILL TO LOCATION','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Customer','','CUSTOMER','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Operating Unit','','OPERATING_UNIT','IN','OM OPERATING UNIT','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Order Type','','ORDER_TYPE','IN','OM ORDER TYPE','','VARCHAR2','N','Y','8','Y','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Ship To','','SHIP_TO','IN','OM SHIP TO LOCATION','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Warehouse','','WAREHOUSE','IN','OM WAREHOUSE','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Order Date From','','ORDERED_DATE','>=','','','DATE','N','Y','2','Y','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Order Date To','','ORDERED_DATE','<=','','','DATE','N','Y','3','Y','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Order Number From','Order Number From','ORDER_NUMBER','>=','OM ORDER NUMBER','','NUMBER','N','Y','10','N','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Order Cancel Details Report - WC',660,'Order Number To','Order Number To','ORDER_NUMBER','<=','OM ORDER NUMBER','','NUMBER','N','Y','11','N','Y','CONSTANT','SK026277','Y','N','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','US','');
--Inserting Dependent Parameters - Order Cancel Details Report - WC
--Inserting Report Conditions - Order Cancel Details Report - WC
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'BILL_TO IN :Bill To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BILL_TO','','Bill To','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','IN','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','BILL_TO IN :Bill To ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'CANCEL_REASON IN :Cancel Reason ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CANCEL_REASON','','Cancel Reason','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','IN','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','CANCEL_REASON IN :Cancel Reason ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'CUSTOMER IN :Customer ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER','','Customer','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','IN','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','CUSTOMER IN :Customer ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'OPERATING_UNIT IN :Operating Unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','IN','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','OPERATING_UNIT IN :Operating Unit ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'ORDERED_DATE >= :Order Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_DATE','','Order Date From','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','ORDERED_DATE >= :Order Date From ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'ORDERED_DATE <= :Order Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDERED_DATE','','Order Date To','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','ORDERED_DATE <= :Order Date To ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'ORDER_NUMBER >= :Order Number From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_NUMBER','','Order Number From','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','ORDER_NUMBER >= :Order Number From ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'ORDER_NUMBER <= :Order Number To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_NUMBER','','Order Number To','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','ORDER_NUMBER <= :Order Number To ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'ORDER_TYPE IN :Order Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORDER_TYPE','','Order Type','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','IN','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','ORDER_TYPE IN :Order Type ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'SHIP_TO IN :Ship To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIP_TO','','Ship To','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','IN','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','SHIP_TO IN :Ship To ');
xxeis.eis_rsc_ins.rcnh( 'Order Cancel Details Report - WC',660,'WAREHOUSE IN :Warehouse ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','WAREHOUSE','','Warehouse','','','','','EIS_XXWC_OM_ORDER_CANCEL_V','','','','','','IN','Y','Y','','','','','1',660,'Order Cancel Details Report - WC','WAREHOUSE IN :Warehouse ');
--Inserting Report Sorts - Order Cancel Details Report - WC
--Inserting Report Triggers - Order Cancel Details Report - WC
--inserting report templates - Order Cancel Details Report - WC
xxeis.eis_rsc_ins.r_tem( 'Order Cancel Details Report - WC','Order Cancel Details Report - WC','Seeded template for Order Cancel Details Report - WC','','','','','','','','','','','Order Cancel Details Report - WC.rtf','SK026277','X','','','Y','Y','','');
--Inserting Report Portals - Order Cancel Details Report - WC
--inserting report dashboards - Order Cancel Details Report - WC
xxeis.eis_rsc_ins.R_dash( 'Order Cancel Details Report - WC','Dynamic 1078','Dynamic 1078','pie','large','Bill To','Bill To','Bill To','Bill To','Count','SK026277');
xxeis.eis_rsc_ins.R_dash( 'Order Cancel Details Report - WC','Dynamic 1146','Dynamic 1146','vertical percent bar','large','Cancel Reason','Cancel Reason','Operating Unit','Operating Unit','Count','SK026277');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Order Cancel Details Report - WC','660','EIS_XXWC_OM_ORDER_CANCEL_V','EIS_XXWC_OM_ORDER_CANCEL_V','N','');
--inserting report security - Order Cancel Details Report - WC
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_ORDER_MGMT_SUPER_USER',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_AO_OEENTRY',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_AO_OEENTRY_REC',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_AO_OEENTRY_PO_RPT',660,'SK026277','','','');
xxeis.eis_rsc_ins.rsec( 'Order Cancel Details Report - WC','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'SK026277','','','');
--Inserting Report Pivots - Order Cancel Details Report - WC
xxeis.eis_rsc_ins.rpivot( 'Order Cancel Details Report - WC',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Order Cancel Details Report - WC',660,'Pivot','CANCELLED_QUANTITY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Order Cancel Details Report - WC',660,'Pivot','CANCEL_REASON','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Order Cancel Details Report - WC',660,'Pivot','CUSTOMER','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Order Cancel Details Report - WC',660,'Pivot','ITEM','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Order Cancel Details Report - WC',660,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Order Cancel Details Report - WC',660,'Pivot','ORDER_NUMBER','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Order Cancel Details Report - WC',660,'Pivot','WAREHOUSE','PAGE_FIELD','','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Order Cancel Details Report - WC
xxeis.eis_rsc_ins.rv( 'Order Cancel Details Report - WC','','Order Cancel Details Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
