--Report Name            : Moving Average Cost Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_INV_XXWC_MOVING_AVG_COST_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_INV_XXWC_MOVING_AVG_COST_V
xxeis.eis_rsc_ins.v( 'EIS_INV_XXWC_MOVING_AVG_COST_V',401,'','','','','SA059956','XXEIS','Eis Inv Xxwc Moving Avg Cost V','EIXMACV','','','VIEW','US','','');
--Delete Object Columns for EIS_INV_XXWC_MOVING_AVG_COST_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_INV_XXWC_MOVING_AVG_COST_V',401,FALSE);
--Inserting Object Columns for EIS_INV_XXWC_MOVING_AVG_COST_V
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','USER_UPDATED_BY',401,'User Updated By','USER_UPDATED_BY','','','','SA059956','VARCHAR2','','','User Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','NEW_COST',401,'New Cost','NEW_COST','','','','SA059956','NUMBER','','','New Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','PRIOR_COST',401,'Prior Cost','PRIOR_COST','','','','SA059956','NUMBER','','','Prior Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','ACTUAL_COST',401,'Actual Cost','ACTUAL_COST','','','','SA059956','NUMBER','','','Actual Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','LOCATION',401,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','SKU',401,'Sku','SKU','','','','SA059956','VARCHAR2','','','Sku','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','MMT_TRANSACTION_ID',401,'Mmt Transaction Id','MMT_TRANSACTION_ID','','','','SA059956','NUMBER','','','Mmt Transaction Id','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','MSI_INVENTORY_ITEM_ID',401,'Msi Inventory Item Id','MSI_INVENTORY_ITEM_ID','','','','SA059956','NUMBER','','','Msi Inventory Item Id','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_INV_XXWC_MOVING_AVG_COST_V','MSI_ORGANIZATION_ID',401,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','SA059956','NUMBER','','','Msi Organization Id','','','','');
--Inserting Object Components for EIS_INV_XXWC_MOVING_AVG_COST_V
xxeis.eis_rsc_ins.vcomp( 'EIS_INV_XXWC_MOVING_AVG_COST_V','MTL_MATERIAL_TRANSACTIONS',401,'MTL_MATERIAL_TRANSACTIONS','MMT','MMT','SA059956','SA059956','-1','Material Transaction Table','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_INV_XXWC_MOVING_AVG_COST_V','MTL_SYSTEM_ITEMS_B',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','SA059956','SA059956','-1','Inventory Item Definitions','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_INV_XXWC_MOVING_AVG_COST_V
xxeis.eis_rsc_ins.vcj( 'EIS_INV_XXWC_MOVING_AVG_COST_V','MTL_MATERIAL_TRANSACTIONS','MMT',401,'EIXMACV.MMT_TRANSACTION_ID','=','MMT.TRANSACTION_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_INV_XXWC_MOVING_AVG_COST_V','MTL_SYSTEM_ITEMS_B','MSI',401,'EIXMACV.MSI_INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','SA059956');
xxeis.eis_rsc_ins.vcj( 'EIS_INV_XXWC_MOVING_AVG_COST_V','MTL_SYSTEM_ITEMS_B','MSI',401,'EIXMACV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report Moving Average Cost Report - WC
prompt Creating Report Data for Moving Average Cost Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Moving Average Cost Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Moving Average Cost Report - WC' );
--Inserting Report - Moving Average Cost Report - WC
xxeis.eis_rsc_ins.r( 401,'Moving Average Cost Report - WC','','This report shows all instances of adjustments made to moving average cost.','','','','SA059956','EIS_INV_XXWC_MOVING_AVG_COST_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Moving Average Cost Report - WC
xxeis.eis_rsc_ins.rc( 'Moving Average Cost Report - WC',401,'ACTUAL_COST','Actual Cost','Actual Cost','','~T~D~2','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Moving Average Cost Report - WC',401,'CREATION_DATE','Creation Date','Creation Date','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Moving Average Cost Report - WC',401,'LOCATION','Location','Location','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Moving Average Cost Report - WC',401,'NEW_COST','New Cost','New Cost','','~T~D~2','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Moving Average Cost Report - WC',401,'PRIOR_COST','Prior Cost','Prior Cost','','~T~D~2','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Moving Average Cost Report - WC',401,'SKU','SKU','Sku','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Moving Average Cost Report - WC',401,'USER_UPDATED_BY','User Updated by','User Updated By','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','','US','');
--Inserting Report Parameters - Moving Average Cost Report - WC
xxeis.eis_rsc_ins.rp( 'Moving Average Cost Report - WC',401,'Creation Date From','Creation Date From','CREATION_DATE','>=','','','DATE','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Moving Average Cost Report - WC',401,'Creation Date To','Creation Date To','CREATION_DATE','<=','','','DATE','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','US','');
--Inserting Dependent Parameters - Moving Average Cost Report - WC
--Inserting Report Conditions - Moving Average Cost Report - WC
xxeis.eis_rsc_ins.rcnh( 'Moving Average Cost Report - WC',401,'EIXMACV.CREATION_DATE >= Creation Date From','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date From','','','','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',401,'Moving Average Cost Report - WC','EIXMACV.CREATION_DATE >= Creation Date From');
xxeis.eis_rsc_ins.rcnh( 'Moving Average Cost Report - WC',401,'EIXMACV.CREATION_DATE <= Creation Date To','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Creation Date To','','','','','EIS_INV_XXWC_MOVING_AVG_COST_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',401,'Moving Average Cost Report - WC','EIXMACV.CREATION_DATE <= Creation Date To');
--Inserting Report Sorts - Moving Average Cost Report - WC
xxeis.eis_rsc_ins.rs( 'Moving Average Cost Report - WC',401,'CREATION_DATE','ASC','SA059956','1','');
--Inserting Report Triggers - Moving Average Cost Report - WC
--inserting report templates - Moving Average Cost Report - WC
--Inserting Report Portals - Moving Average Cost Report - WC
--inserting report dashboards - Moving Average Cost Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Moving Average Cost Report - WC','401','EIS_INV_XXWC_MOVING_AVG_COST_V','EIS_INV_XXWC_MOVING_AVG_COST_V','N','');
--inserting report security - Moving Average Cost Report - WC
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','GNRL_LDGR_FSS',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXCUS_GL_MANAGER',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXWC_GL_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXCUS_GL_INQUIRY',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXCUS_GL_ACCOUNTANT_USD_PS',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXCUS_GL_ACCOUNTANT_USD',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXCUS_GL_ACCOUNTANT_CAD_PS',401,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Moving Average Cost Report - WC','101','','XXCUS_GL_ACCOUNTANT_CAD',401,'SA059956','','','');
--Inserting Report Pivots - Moving Average Cost Report - WC
--Inserting Report   Version details- Moving Average Cost Report - WC
xxeis.eis_rsc_ins.rv( 'Moving Average Cost Report - WC','','Moving Average Cost Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
