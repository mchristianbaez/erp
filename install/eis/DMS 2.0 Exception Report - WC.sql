--Report Name            : DMS 2.0 Exception Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_DMS_EXCEPTION_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_DMS_EXCEPTION_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_DMS_EXCEPTION_V',660,'','','','','SA059956','XXEIS','Eis Xxwc Om Dms Exception V','EXODEV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_OM_DMS_EXCEPTION_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_DMS_EXCEPTION_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_DMS_EXCEPTION_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','BRANCH',660,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','SA059956','NUMBER','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','DELIVERY_ID',660,'Delivery Id','DELIVERY_ID','','','','SA059956','NUMBER','','','Delivery Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','SA059956','NUMBER','','','Line Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','SA059956','VARCHAR2','','','Item Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','STOP_EXCEPTION',660,'Stop Exception','STOP_EXCEPTION','','','','SA059956','VARCHAR2','','','Stop Exception','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','LINE_EXCEPTION',660,'Line Exception','LINE_EXCEPTION','','','','SA059956','VARCHAR2','','','Line Exception','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','DRIVER_NAME',660,'Driver Name','DRIVER_NAME','','','','SA059956','VARCHAR2','','','Driver Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','ROUTE_NAME',660,'Route Name','ROUTE_NAME','','','','SA059956','VARCHAR2','','','Route Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_DMS_EXCEPTION_V','DISPATCH_DATE',660,'Dispatch Date','DISPATCH_DATE','','','','SA059956','DATE','','','Dispatch Date','','','','US','');
--Inserting Object Components for EIS_XXWC_OM_DMS_EXCEPTION_V
--Inserting Object Component Joins for EIS_XXWC_OM_DMS_EXCEPTION_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for DMS 2.0 Exception Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - DMS 2.0 Exception Report - WC
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT xomd.branch
FROM xxwc.xxwc_om_dms2_ship_confirm_tbl xomd
WHERE xomd.branch IS NOT NULL
ORDER BY 1','','DMS Branch Exception LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for DMS 2.0 Exception Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - DMS 2.0 Exception Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'DMS 2.0 Exception Report - WC',660 );
--Inserting Report - DMS 2.0 Exception Report - WC
xxeis.eis_rsc_ins.r( 660,'DMS 2.0 Exception Report - WC','','This report will give you the details if the deliveries are flagged as Exception in Descartes (On Demand)','','','','SA059956','EIS_XXWC_OM_DMS_EXCEPTION_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','N','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - DMS 2.0 Exception Report - WC
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'BRANCH','Branch','Branch','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'CREATION_DATE','Creation Date','Creation Date','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'DRIVER_NAME','Driver Name','Driver Name','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'ITEM_NUMBER','Item Number','Item Number','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'LINE_EXCEPTION','Line Exception','Line Exception','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'LINE_NUMBER','Line Number','Line Number','','~~~','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'ROUTE_NAME','Route Name','Route Name','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'STOP_EXCEPTION','Stop Exception','Stop Exception','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'DMS 2.0 Exception Report - WC',660,'DELIVERY_ID','Delivery ID','Delivery Id','','~~~','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','US','','');
--Inserting Report Parameters - DMS 2.0 Exception Report - WC
xxeis.eis_rsc_ins.rp( 'DMS 2.0 Exception Report - WC',660,'Branch','Branch','BRANCH','IN','DMS Branch Exception LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','US','');
xxeis.eis_rsc_ins.rp( 'DMS 2.0 Exception Report - WC',660,'Start Date','Creation Date','CREATION_DATE','>=','','','DATE','Y','Y','2','N','Y','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','US','');
xxeis.eis_rsc_ins.rp( 'DMS 2.0 Exception Report - WC',660,'End Date','End Date','CREATION_DATE','<=','','','DATE','Y','Y','3','N','Y','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','US','');
--Inserting Dependent Parameters - DMS 2.0 Exception Report - WC
--Inserting Report Conditions - DMS 2.0 Exception Report - WC
xxeis.eis_rsc_ins.rcnh( 'DMS 2.0 Exception Report - WC',660,'EXODEV.BRANCH IN Branch','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','BRANCH','','Branch','','','','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','','','IN','Y','Y','','','','','1',660,'DMS 2.0 Exception Report - WC','EXODEV.BRANCH IN Branch');
xxeis.eis_rsc_ins.rcnh( 'DMS 2.0 Exception Report - WC',660,'EXODEV.CREATION_DATE >= Start Date','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','Start Date','','','','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'DMS 2.0 Exception Report - WC','EXODEV.CREATION_DATE >= Start Date');
xxeis.eis_rsc_ins.rcnh( 'DMS 2.0 Exception Report - WC',660,'EXODEV.CREATION_DATE <= End Date','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','CREATION_DATE','','End Date','','','','','EIS_XXWC_OM_DMS_EXCEPTION_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'DMS 2.0 Exception Report - WC','EXODEV.CREATION_DATE <= End Date');
--Inserting Report Sorts - DMS 2.0 Exception Report - WC
xxeis.eis_rsc_ins.rs( 'DMS 2.0 Exception Report - WC',660,'ORDER_NUMBER','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'DMS 2.0 Exception Report - WC',660,'LINE_NUMBER','ASC','SA059956','2','');
--Inserting Report Triggers - DMS 2.0 Exception Report - WC
--inserting report templates - DMS 2.0 Exception Report - WC
--Inserting Report Portals - DMS 2.0 Exception Report - WC
--inserting report dashboards - DMS 2.0 Exception Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'DMS 2.0 Exception Report - WC','660','EIS_XXWC_OM_DMS_EXCEPTION_V','EIS_XXWC_OM_DMS_EXCEPTION_V','N','');
--inserting report security - DMS 2.0 Exception Report - WC
xxeis.eis_rsc_ins.rsec( 'DMS 2.0 Exception Report - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS 2.0 Exception Report - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS 2.0 Exception Report - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'DMS 2.0 Exception Report - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
--Inserting Report Pivots - DMS 2.0 Exception Report - WC
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- DMS 2.0 Exception Report - WC
xxeis.eis_rsc_ins.rv( 'DMS 2.0 Exception Report - WC','','DMS 2.0 Exception Report - WC','SA059956','21-MAR-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
