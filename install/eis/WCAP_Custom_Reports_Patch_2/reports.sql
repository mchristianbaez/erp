--Report Name            : Direct Shipments Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_INV_DRCT_SHPMTS_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_DRCT_SHPMTS_V
Create or replace View XXEIS.EIS_XXWC_INV_DRCT_SHPMTS_V
(REQ_NUMBER,END_DATE,PERIOD_NAME,LOCATION,ORGANIZATION_ID,BRANCH_NUMBER,SEGMENT_LOCATION,PO_NUMBER,RECEIVED_DATE,PO_COST,ORDER_NUMBER,HEADER_ID,LINE_ID,APPLICATION_ID,SET_OF_BOOKS_ID,CODE_COMBINATION_ID) AS 
select distinct req_h.segment1 req_number,
    gps.end_date ,
    gps.period_name,
    substr(gcc.segment2,3)    location,
    mtp.organization_id ,
    mtp.organization_code     branch_number,
    gcc.segment2              segment_location ,
    ph.segment1               po_number,
    trunc(rsh.creation_date)  received_date,
    xxeis.eis_rs_xxwc_com_util_pkg.get_po_cost(od.requisition_header_id,od.po_header_id) po_cost,
    oh.order_number,
    ---Primary Keys
    oh.header_id ,
    ol.line_id ,
    gps.application_id ,
    gps.set_of_books_id ,
    gcc.code_combination_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  From  Po_Requisition_Headers   Req_H,
        Po_Headers               Ph,
        Oe_Drop_Ship_Sources     Od,
        Oe_Order_Lines           Ol,
        Oe_Order_Headers         Oh,
        Hr_Operating_Units       Hou,
        Rcv_Shipment_Headers     Rsh,
        Rcv_Shipment_Lines       Rsl,
        Mtl_Parameters           Mtp,
        Gl_Period_Statuses       Gps,
        Gl_Code_Combinations_Kfv Gcc
  where 1                         =1
  and od.header_id                = oh.header_id
  and od.line_id                  = ol.line_id
  AND req_h.interface_source_code = 'ORDER ENTRY'
  And Req_H.Requisition_Header_Id = Od.Requisition_Header_Id
  And Od.Po_Header_Id             = Ph.Po_Header_Id
  And Ol.Header_Id                = Oh.Header_Id
  And Hou.Organization_Id         = Oh.Org_Id
  And Od.Po_Header_Id             = Rsl.Po_Header_Id(+)
  And Od.Po_Line_Id               = Rsl.Po_Line_Id(+)
  And Ol.Ship_From_Org_Id         = Mtp.Organization_Id(+)
  And Rsl.Shipment_Header_Id      = Rsh.Shipment_Header_Id(+)
  And Gps.Application_Id          = 101
  And Gps.Set_Of_Books_Id         = Hou.Set_Of_Books_Id
  And Trunc(Oh.Ordered_Date) Between Gps.Start_Date And Gps.End_Date
  And Gcc.Code_Combination_Id(+) = Mtp.Cost_Of_Sales_Account
  and exists
      ( select 1
        from xxeis.eis_org_access_v
        where organization_id = mtp.organization_id
     )

/
set scan on define on
prompt Creating View Data for Direct Shipments Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_DRCT_SHPMTS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_DRCT_SHPMTS_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Inv Direct Shipments V','EXIDSV');
--Delete View Columns for EIS_XXWC_INV_DRCT_SHPMTS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_DRCT_SHPMTS_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_DRCT_SHPMTS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','PO_NUMBER',401,'Po Number','PO_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Po Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','ORDER_NUMBER',401,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','PO_COST',401,'Po Cost','PO_COST','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Po Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','RECEIVED_DATE',401,'Received Date','RECEIVED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Received Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','END_DATE',401,'End Date','END_DATE','','','','XXEIS_RS_ADMIN','DATE','','','End Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','SEGMENT_LOCATION',401,'Segment Location','SEGMENT_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','PERIOD_NAME',401,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','BRANCH_NUMBER',401,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','REQ_NUMBER',401,'Req Number','REQ_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Req Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','APPLICATION_ID',401,'Application Id','APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Application Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','CODE_COMBINATION_ID',401,'Code Combination Id','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Code Combination Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','HEADER_ID',401,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','LINE_ID',401,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','SET_OF_BOOKS_ID',401,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','LOCATION',401,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_DRCT_SHPMTS_V','ORGANIZATION_ID',401,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
--Inserting View Components for EIS_XXWC_INV_DRCT_SHPMTS_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_DRCT_SHPMTS_V','OE_ORDER_LINES',401,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_DRCT_SHPMTS_V','OE_ORDER_HEADERS',401,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_DRCT_SHPMTS_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_DRCT_SHPMTS_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Account Combinations','','');
--Inserting View Component Joins for EIS_XXWC_INV_DRCT_SHPMTS_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_DRCT_SHPMTS_V','OE_ORDER_LINES','OL',401,'EXIDSV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_DRCT_SHPMTS_V','OE_ORDER_HEADERS','OH',401,'EXIDSV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_DRCT_SHPMTS_V','GL_PERIOD_STATUSES','GPS',401,'EXIDSV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_DRCT_SHPMTS_V','GL_PERIOD_STATUSES','GPS',401,'EXIDSV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_DRCT_SHPMTS_V','GL_PERIOD_STATUSES','GPS',401,'EXIDSV.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_DRCT_SHPMTS_V','GL_CODE_COMBINATIONS_KFV','GCC',401,'EXIDSV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Direct Shipments Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Direct Shipments Report
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','EIS_INV_INVENTORY_ORGS_LOV1','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select  distinct per.period_name , per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     led.accounted_period_type = per.period_type
order by per.period_year desc,per.period_num asc','null','EIS INV PERIOD NAME','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Direct Shipments Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Direct Shipments Report
xxeis.eis_rs_utility.delete_report_rows( 'Direct Shipments Report' );
--Inserting Report - Direct Shipments Report
xxeis.eis_rs_ins.r( 401,'Direct Shipments Report','','This report provides the total listing of all direct / drop shipments (Location, PO Number, Order Number & Total Cost)','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_DRCT_SHPMTS_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Direct Shipments Report
xxeis.eis_rs_ins.rc( 'Direct Shipments Report',401,'ORDER_NUMBER','Order Number','Order Number','','','','','7','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_DRCT_SHPMTS_V','','');
xxeis.eis_rs_ins.rc( 'Direct Shipments Report',401,'PO_COST','Po Cost','Po Cost','','','','','6','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_DRCT_SHPMTS_V','','');
xxeis.eis_rs_ins.rc( 'Direct Shipments Report',401,'PO_NUMBER','Po Number','Po Number','','','','','4','N','','PAGE_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_DRCT_SHPMTS_V','','');
xxeis.eis_rs_ins.rc( 'Direct Shipments Report',401,'RECEIVED_DATE','Received Date','Received Date','','','','','5','N','','PAGE_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_DRCT_SHPMTS_V','','');
xxeis.eis_rs_ins.rc( 'Direct Shipments Report',401,'END_DATE','Date','End Date','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_DRCT_SHPMTS_V','','');
xxeis.eis_rs_ins.rc( 'Direct Shipments Report',401,'SEGMENT_LOCATION','Segment Location','Segment Location','','','','','3','N','','PAGE_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_DRCT_SHPMTS_V','','');
xxeis.eis_rs_ins.rc( 'Direct Shipments Report',401,'BRANCH_NUMBER','Location','Branch Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_DRCT_SHPMTS_V','','');
--Inserting Report Parameters - Direct Shipments Report
xxeis.eis_rs_ins.rp( 'Direct Shipments Report',401,'Fiscal Month','Fiscal Month','PERIOD_NAME','IN','EIS INV PERIOD NAME','','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Direct Shipments Report',401,'Organization','Organization','BRANCH_NUMBER','IN','EIS_INV_INVENTORY_ORGS_LOV1','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Direct Shipments Report
xxeis.eis_rs_ins.rcn( 'Direct Shipments Report',401,'PERIOD_NAME','IN',':Fiscal Month','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Direct Shipments Report',401,'BRANCH_NUMBER','IN',':Organization','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Direct Shipments Report
xxeis.eis_rs_ins.rs( 'Direct Shipments Report',401,'BRANCH_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Direct Shipments Report
--Inserting Report Templates - Direct Shipments Report
--Inserting Report Portals - Direct Shipments Report
--Inserting Report Dashboards - Direct Shipments Report
--Inserting Report Security - Direct Shipments Report
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','401','','50619',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','401','','50924',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','401','','51052',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','401','','50879',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','401','','50851',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','401','','50852',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','401','','50821',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','20005','','50880',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','','LB048272','',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','','10012196','',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','','MM050208','',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','','SO004816','',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','401','','20634',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Direct Shipments Report','401','','51029',401,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Inventory Listing Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_INV_LISTING_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_LISTING_V
Create or replace View XXEIS.EIS_XXWC_INV_LISTING_V
(LOCATION,BRANCH_NUMBER,SEGMENT_LOCATION,LOCATION_STATE,PART_NUMBER,MFG_NUMBER,DESCRIPTION,CONSIGNED,CAT_CLASS,CAT_CLASS_DESCRIPTION,UOM,SELLING_PRICE,AVERAGECOST,WEIGHT,ONHAND,FINAL_END_QTY,VENDOR_NAME,SHELF_LIFE,CONTAINER_TYPE,UN_NUMBER,FISCAL_MONTH,SALE_UNITS,BEGIN_INVENTORY,END_INVENTORY,COGS_AMOUNT,CAPROP65,PESTICIDE_FLAG,VOCNUMBER,PESTICIDE_FLAG_STATE,VOC_CATEGORY,VOC_SUB_CATEGORY,MSDS_NUMBER,ORMD,HAZARDOUS_MATERIAL_DESCRIPTION,PACKING_GROUP,HAZMAT_FLAG,HAZARD_CLASS,INVENTORY_ITEM_ID,INV_ORGANIZATION_ID,APPLICATION_ID,SET_OF_BOOKS_ID,CODE_COMBINATION_ID,ORG_ORGANIZATION_ID) AS 
SELECT SUBSTR(GCC.SEGMENT2,3) LOCATION,
      OOD.ORGANIZATION_CODE BRANCH_NUMBER,
      gcc.segment2 segment_location,
      hou.region_2 location_state,
      MSI.SEGMENT1 PART_NUMBER,
      XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.Get_Item_Crossref(msi.inventory_item_id,msi.organization_id) mfg_number,
      MSI.DESCRIPTION,
      DECODE (MSI.CONSIGNED_FLAG,1,'Y',2,'N') CONSIGNED,
      --MCV.SEGMENT2 CAT_CLASS,
      xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat_class,
      --mcv.description CAT_CLASS_DESCRIPTION,
      xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc(msi.inventory_item_id,msi.organization_id) CAT_CLASS_DESCRIPTION,
      msi.primary_uom_code uom,
      msi.list_price_per_unit selling_price,
      NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0) averagecost,
      msi.unit_weight weight,
      /*case when xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id) > 0 then 'Positive Onhand Items'
           when xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id) < 0 then 'Negative Onhand Items'
           else  'ALL Items' end oh_type,*/
      XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ONHAND_INV(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) ONHAND,
      nvl(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_FINAL_END_QTY(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,GPS.END_DATE),0) FINAL_END_QTY,
      XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_VENDOR_NAME(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) VENDOR_NAME,
      MSI.SHELF_LIFE_DAYS SHELF_LIFE,
      MSI.CONTAINER_TYPE_CODE CONTAINER_TYPE,
      PUN.UN_number UN_NUMBER,
      GPS.PERIOD_NAME FISCAL_MONTH,
      XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_MTD_SALES(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,GPS.START_DATE,GPS.END_DATE) SALE_UNITS,
      xxeis.eis_rs_xxwc_com_util_pkg.get_beg_inv(msi.inventory_item_id,msi.organization_id,gps.start_date) begin_inventory,
      xxeis.eis_rs_xxwc_com_util_pkg.get_end_inv(msi.inventory_item_id,msi.organization_id,gps.end_date) end_inventory,
      xxeis.eis_rs_xxwc_com_util_pkg.get_cogs_amt(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) cogs_amount,
      MSI.ATTRIBUTE1 CAPROP65,
      msi.attribute3 pesticide_flag,
      msi.attribute4 vocnumber,
      msi.attribute5 pesticide_flag_state,
      msi.attribute6 voc_category,
      msi.attribute7 voc_sub_category,
      msi.attribute8 msds_number,
      msi.attribute11 ORMD,
      msi.attribute17 hazardous_material_description,
      msi.attribute9 packing_group,
      MSI.HAZARDOUS_MATERIAL_FLAG HAZMAT_FLAG,
      PHC.HAZARD_CLASS HAZARD_CLASS,
      --primary keys
      MSI.INVENTORY_ITEM_ID,
      MSI.ORGANIZATION_ID INV_ORGANIZATION_ID,
      GPS.APPLICATION_ID,
      GPS.SET_OF_BOOKS_ID,
      gcc.code_combination_id,
      --MCV.CATEGORY_ID,
      OOD.ORGANIZATION_ID ORG_ORGANIZATION_ID
      --descr#flexfield#start
      --descr#flexfield#end
      --gl#accountff#start
      --gl#accountff#end
FROM MTL_SYSTEM_ITEMS_KFV MSI,
    gl_code_combinations_kfv gcc,
    ORG_ORGANIZATION_DEFINITIONS OOD,
    GL_PERIOD_STATUSES GPS,
    po_hazard_classes phc,
    po_un_numbers pun,
    mtl_parameters mtp,
    hr_organization_units_v hou   
  WHERE 1                 =1
  AND msi.organization_id =ood.organization_id
  AND GCC.CODE_COMBINaTION_ID =MSI.COST_OF_SALES_ACCOUNT
  AND GPS.APPLICATION_ID      =101
  AND GPS.SET_OF_BOOKS_ID     =OOD.SET_OF_BOOKS_ID
  AND TRUNC(sysdate) BETWEEN gps.start_date AND gps.end_date   
  --AND MSI.ATTRIBUTE_CATEGORY(+) ='WC'
  AND MSI.HAZARD_CLASS_ID    =PHC.HAZARD_CLASS_ID(+)
  and msi.un_number_id       = pun.un_number_id (+)
  and msi.organization_id=mtp.organization_id
  AND MTP.ORGANIZATION_ID(+)=HOU.ORGANIZATION_ID
    AND EXISTS
    (SELECT 1
    FROM xxeis.eis_org_access_v
    where organization_id = msi.organization_id
    )
/
set scan on define on
prompt Creating View Data for Inventory Listing Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_LISTING_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Rs Xxwc Inv Listing V','EXILV');
--Delete View Columns for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_LISTING_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SHELF_LIFE',401,'Shelf Life','SHELF_LIFE','','','','XXEIS_RS_ADMIN','NUMBER','','','Shelf Life');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ONHAND',401,'Onhand','ONHAND','','','','XXEIS_RS_ADMIN','NUMBER','','','Onhand');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Selling Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','UOM',401,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CAT_CLASS',401,'Cat Class','CAT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SEGMENT_LOCATION',401,'Segment Location','SEGMENT_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','LOCATION',401,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','FINAL_END_QTY',401,'Final End Qty','FINAL_END_QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Final End Qty');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','MFG_NUMBER',401,'Mfg Number','MFG_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mfg Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SALE_UNITS',401,'Sale Units','SALE_UNITS','','','','XXEIS_RS_ADMIN','NUMBER','','','Sale Units');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BRANCH_NUMBER',401,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CAT_CLASS_DESCRIPTION',401,'Cat Class Description','CAT_CLASS_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CONSIGNED',401,'Consigned','CONSIGNED','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Consigned');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','FISCAL_MONTH',401,'Fiscal Month','FISCAL_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fiscal Month');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','MSDS_NUMBER',401,'Msds Number','MSDS_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msds Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','VOC_SUB_CATEGORY',401,'Voc Sub Category','VOC_SUB_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Voc Sub Category');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','VOC_CATEGORY',401,'Voc Category','VOC_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Voc Category');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','PESTICIDE_FLAG_STATE',401,'Pesticide Flag State','PESTICIDE_FLAG_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pesticide Flag State');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','VOCNUMBER',401,'Vocnumber','VOCNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vocnumber');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','PESTICIDE_FLAG',401,'Pesticide Flag','PESTICIDE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pesticide Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CAPROP65',401,'Caprop65','CAPROP65','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Caprop65');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CONTAINER_TYPE',401,'Container Type','CONTAINER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Container Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','WEIGHT',401,'Weight','WEIGHT','','','','XXEIS_RS_ADMIN','NUMBER','','','Weight');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Averagecost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','BEGIN_INVENTORY',401,'Begin Inventory','BEGIN_INVENTORY','','','','XXEIS_RS_ADMIN','NUMBER','','','Begin Inventory');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','COGS_AMOUNT',401,'Cogs Amount','COGS_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Cogs Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','END_INVENTORY',401,'End Inventory','END_INVENTORY','','','','XXEIS_RS_ADMIN','NUMBER','','','End Inventory');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inv Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','HAZARDOUS_MATERIAL_DESCRIPTION',401,'Hazardous Material Description','HAZARDOUS_MATERIAL_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hazardous Material Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','ORMD',401,'Ormd','ORMD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ormd');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','PACKING_GROUP',401,'Packing Group','PACKING_GROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Packing Group');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','APPLICATION_ID',401,'Application Id','APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Application Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','CODE_COMBINATION_ID',401,'Code Combination Id','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Code Combination Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','SET_OF_BOOKS_ID',401,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','HAZMAT_FLAG',401,'Hazmat Flag','HAZMAT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hazmat Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','HAZARD_CLASS',401,'Hazard Class','HAZARD_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hazard Class');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','UN_NUMBER',401,'Un Number','UN_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Un Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_LISTING_V','LOCATION_STATE',401,'Location State','LOCATION_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location State');
--Inserting View Components for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Account Combinations','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Categories','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','1688889','Items','','');
--Inserting View Component Joins for EIS_XXWC_INV_LISTING_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_CODE_COMBINATIONS_KFV','GCC',401,'EXILV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','GL_PERIOD_STATUSES','GPS',401,'EXILV.FISCAL_MONTH','=','GPS.PERIOD_NAME(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_CATEGORIES','MCV',401,'EXILV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXILV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_LISTING_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXILV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Inventory Listing Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Inventory Listing Report
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','EIS_INV_INVENTORY_ORGS_LOV1','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT ''Negative Onhand Items'' onhand_type FROM dual
UNION 
SELECT ''Positive Onhand Items'' onhand_type FROM dual
UNION 
SELECT ''ALL Items'' onhand_type FROM dual','','INV ONHAND TYPE','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Inventory Listing Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory Listing Report
xxeis.eis_rs_utility.delete_report_rows( 'Inventory Listing Report' );
--Inserting Report - Inventory Listing Report
xxeis.eis_rs_ins.r( 401,'Inventory Listing Report','','Provide a listing of inventory related information ( Negative On Hand , HAZMAT list, Cat/Class list, Extended Description, Inventory Explosion, etc.)
Selection - Location, Prod, Ven, Cat/Class
Columns - stock level, negative avail, on hand, on order, turns, cat/class, descrip, extended description (Negative On Hand, HAZMAT list, Cat/Class list, Inventory Explosion  etc)
','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_LISTING_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Inventory Listing Report
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'CAT_CLASS','CatClass','Cat Class','','','','','7','N','','PAGE_FIELD','','','','5','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'DESCRIPTION','Description','Description','','','','','5','N','','PAGE_FIELD','','','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'ONHAND','Qty on Hand','Onhand','','','','','13','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'PART_NUMBER','Part','Part Number','','','','','4','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'SEGMENT_LOCATION','Segment Location','Segment Location','','','','','2','N','','PAGE_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'SELLING_PRICE','Selling Price','Selling Price','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'SHELF_LIFE','Shelf Life','Shelf Life','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'UOM','UOM','Uom','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','10','N','','PAGE_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'MFG_NUMBER','Mfg Number','Mfg Number','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'SALE_UNITS','Sale Units','Sale Units','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'CAT_CLASS_DESCRIPTION','Cat Class Description','Cat Class Description','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'CONSIGNED','Consigned','Consigned','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'FINAL END VALUE','Final End Value','Consigned','NUMBER','','','','17','Y','','','','','','','(EXILV.AVERAGECOST*EXILV.ONHAND)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'AVERAGECOST','Averagecost','Averagecost','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'LOCATION_STATE','Location State','Location State','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Listing Report',401,'BRANCH_NUMBER','Location','Branch Number','','','','','1','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_LISTING_V','','');
--Inserting Report Parameters - Inventory Listing Report
xxeis.eis_rs_ins.rp( 'Inventory Listing Report',401,'Organization','Organization','BRANCH_NUMBER','IN','EIS_INV_INVENTORY_ORGS_LOV1','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Listing Report',401,'Include All Items with a Positive On Hand','Include All Items with a Positive On Hand','','IN','INV ONHAND TYPE','''ALL Items''','VARCHAR2','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Inventory Listing Report
xxeis.eis_rs_ins.rcn( 'Inventory Listing Report',401,'BRANCH_NUMBER','IN',':Organization','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Inventory Listing Report',401,'OH_TYPE','IN',':Include All Items with a Positive On Hand','','','N','','N','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Inventory Listing Report',401,'','','','','and (decode(:Include All Items with a Positive On Hand ,''Positive Onhand Items'', EXILV.ONHAND) > 0
 or decode(:Include All Items with a Positive On Hand ,''Negative Onhand Items'', EXILV.ONHAND)  < 0
  or decode(:Include All Items with a Positive On Hand ,''ALL Items'', 1) =1 
)','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Inventory Listing Report
xxeis.eis_rs_ins.rs( 'Inventory Listing Report',401,'PART_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Inventory Listing Report
--Inserting Report Templates - Inventory Listing Report
--Inserting Report Portals - Inventory Listing Report
--Inserting Report Dashboards - Inventory Listing Report
--Inserting Report Security - Inventory Listing Report
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50619',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50924',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','51052',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50879',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50851',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50852',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','50821',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','20005','','50880',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Inventory Listing Report','401','','51029',401,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Specials Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_INV_SPECIAL_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_SPECIAL_V
Create or replace View XXEIS.EIS_XXWC_INV_SPECIAL_V
(ORGANIZATION_CODE,PART_NUMBER,DESCRIPTION,CAT,UOM,SELLING_PRICE,AVERAGECOST,GP,WEIGHT,ONHAND,EXTENDED_VALUE,VENDOR_NAME,VENDOR_NUMBER,BUYER_CODE,DATE_RECEIVED,START_DATE,END_DATE,OPEN_SALES_ORDERS,NO_OF_ITEM_PURCHASED,INVENTORY_ITEM_ID,INV_ORGANIZATION_ID,ORG_ORGANIZATION_ID,APPLICATION_ID,SET_OF_BOOKS_ID,PERIOD_NAME,AGENT_ID) AS 
SELECT ood.organization_code ,
  msi.segment1 part_number,
  msi.description,
  xxeis.eis_rs_xxwc_com_util_pkg.Get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,
  msi.primary_uom_code uom,
  --ol.unit_list_price selling_price,
  xxeis.eis_rs_xxwc_com_util_pkg.Get_list_Price(msi.inventory_item_id,msi.organization_id) selling_price,
  NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0) AVERAGECOST,
  --ROUND((((XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID))-(NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0)))*100)/(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID)),2) GP,
  CASE
    WHEN NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0) =0
    THEN 0
    ELSE ROUND(((NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0)-(NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0)))*100)/(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LIST_PRICE(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID)),2)
  END GP,
  MSI.UNIT_WEIGHT WEIGHT,
  MOQD.TRANSACTION_QUANTITY ONHAND,
  --xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id) ONHAND,
  (ROUND(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0),2)*(moqd.transaction_quantity)) extended_value,
  xxeis.eis_rs_xxwc_com_util_pkg.get_po_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.get_po_vendor_number(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) VENDOR_NUMBER,
  POA.AGENT_NAME BUYER_CODE,
  TRUNC(moqd.date_received) date_received,
  gps.start_date,
  gps.end_date,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_OPEN_SALES_ORDERS(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) OPEN_SALES_ORDERS,
  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.Get_item_purchased(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) no_of_item_purchased,
  --primary keys
  msi.inventory_item_id,
  msi.organization_id inv_organization_id,
  OOD.ORGANIZATION_ID ORG_ORGANIZATION_ID,
  GPS.APPLICATION_ID,
  GPS.SET_OF_BOOKS_ID,
  GPS.PERIOD_NAME,
  POA.AGENT_ID
  --descr#flexfield#start
  --descr#flexfield#end
  --gl#accountff#start
  --gl#accountff#end
FROM mtl_system_items_kfv msi,
  mtl_onhand_quantities_detail moqd,
  org_organization_definitions ood,
  po_agents_v poa,
  gl_period_statuses gps
WHERE 1                   =1
AND msi.organization_id   =ood.organization_id
AND msi.inventory_item_id =moqd.inventory_item_id
AND msi.organization_id   =moqd.organization_id
AND msi.buyer_id          =poa.agent_id(+)
AND msi.segment1 LIKE 'SP%'
AND gps.application_id =101
AND gps.set_of_books_id=ood.set_of_books_id
AND TRUNC(moqd.date_received) BETWEEN gps.start_date AND gps.end_date
and MOQD.TRANSACTION_QUANTITY <> 0
AND EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  where ORGANIZATION_ID = MSI.ORGANIZATION_ID
  )
/
set scan on define on
prompt Creating View Data for Specials Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_SPECIAL_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Rs Xxwc Inv Special V','EXISV');
--Delete View Columns for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_SPECIAL_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','BUYER_CODE',401,'Buyer Code','BUYER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','ONHAND',401,'Onhand','ONHAND','','','','XXEIS_RS_ADMIN','NUMBER','','','Onhand');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','WEIGHT',401,'Weight','WEIGHT','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Weight');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Averagecost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Selling Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','UOM',401,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','CAT',401,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','EXTENDED_VALUE',401,'Extended Value','EXTENDED_VALUE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Extended Value');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','GP',401,'Gp','GP','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Gp');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','DATE_RECEIVED',401,'Date Received','DATE_RECEIVED','','','','XXEIS_RS_ADMIN','DATE','','','Date Received');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','ORGANIZATION_CODE',401,'Organization Code','ORGANIZATION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','OPEN_SALES_ORDERS',401,'Open Sales Orders','OPEN_SALES_ORDERS','','','','XXEIS_RS_ADMIN','NUMBER','','','Open Sales Orders');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','AGENT_ID',401,'Agent Id','AGENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Agent Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inv Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','END_DATE',401,'End Date','END_DATE','','','','XXEIS_RS_ADMIN','DATE','','','End Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','START_DATE',401,'Start Date','START_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Start Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','APPLICATION_ID',401,'Application Id','APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Application Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','PERIOD_NAME',401,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','SET_OF_BOOKS_ID',401,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Set Of Books Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_SPECIAL_V','NO_OF_ITEM_PURCHASED',401,'No Of Item Purchased','NO_OF_ITEM_PURCHASED','','','','XXEIS_RS_ADMIN','NUMBER','','','No Of Item Purchased');
--Inserting View Components for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','PO_AGENTS_V',401,'PO_AGENTS','POA','POA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Buyers Table','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_SPECIAL_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Categories','','');
--Inserting View Component Joins for EIS_XXWC_INV_SPECIAL_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','PO_AGENTS_V','POA',401,'EXISV.AGENT_ID','=','POA.AGENT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','GL_PERIOD_STATUSES','GPS',401,'EXISV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','GL_PERIOD_STATUSES','GPS',401,'EXISV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','GL_PERIOD_STATUSES','GPS',401,'EXISV.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','MTL_CATEGORIES','MCV',401,'EXISV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXISV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_SPECIAL_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXISV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Specials Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Specials Report
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','EIS_INV_INVENTORY_ORGS_LOV1','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT ''Include Specials with Oh Hand > 7 days'' onhand_type FROM dual
UNION 
SELECT ''Include Specials with Oh Hand < 7 days'' onhand_type FROM dual
union 
SELECT ''ALL specials'' onhand_type FROM dual','','INV SPECIALONHAND TYPE','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Specials Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Specials Report
xxeis.eis_rs_utility.delete_report_rows( 'Specials Report' );
--Inserting Report - Specials Report
xxeis.eis_rs_ins.r( 401,'Specials Report','','report on all special parts ordered; special parts on hand with or without demand; report to flag special parts to become standard parts (i.e. they are used a lot)','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_SPECIAL_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','HTML,EXCEL,','N');
--Inserting Report Columns - Specials Report
xxeis.eis_rs_ins.rc( 'Specials Report',401,'AVERAGECOST','Avg Cost','Averagecost','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'BUYER_CODE','Buyer','Buyer Code','','','','','14','N','','PAGE_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'CAT','Cat Class','Cat','','','','','4','N','','PAGE_FIELD','','','','5','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'DESCRIPTION','Description','Description','','','','','3','N','','PAGE_FIELD','','','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'ONHAND','On Hand','Onhand','','','','','11','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'PART_NUMBER','Part Number','Part Number','','','','','2','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'SELLING_PRICE','Selling Price','Selling Price','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'UOM','UOM','Uom','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'WEIGHT','Weight','Weight','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'EXTENDED_VALUE','Extended Value','Extended Value','','','','','12','N','','DATA_FIELD','','SUM','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','9','N','','PAGE_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','','','8','N','','PAGE_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'GP','GP%','Gp','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'OPEN_SALES_ORDERS','Open Sales Orders','Open Sales Orders','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'ORGANIZATION_CODE','Org Number','Organization Code','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
xxeis.eis_rs_ins.rc( 'Specials Report',401,'NO_OF_ITEM_PURCHASED','No of times purchased','No Of Item Purchased','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_SPECIAL_V','','');
--Inserting Report Parameters - Specials Report
xxeis.eis_rs_ins.rp( 'Specials Report',401,'Start Date','Start Date','DATE_RECEIVED','>=','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Specials Report',401,'End Date','End Date','DATE_RECEIVED','<=','','','DATE','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Specials Report',401,'Organization','Organization','ORGANIZATION_CODE','IN','EIS_INV_INVENTORY_ORGS_LOV1','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Specials Report',401,'Select Items for report','Select Items for report','','IN','INV SPECIALONHAND TYPE','''ALL specials''','VARCHAR2','N','Y','4','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Specials Report
xxeis.eis_rs_ins.rcn( 'Specials Report',401,'DATE_RECEIVED','>=',':Start Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Specials Report',401,'DATE_RECEIVED','<=',':End Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Specials Report',401,'ORGANIZATION_CODE','IN',':Organization','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Specials Report',401,'','','','','and (decode(:Select Items for report ,''Include Specials with Oh Hand > 7 days'', EXISV.ONHAND) > 7
 or   decode(:Select Items for report ,''Include Specials with Oh Hand < 7 days'', EXISV.ONHAND) < 7
  or decode(:Select Items for report ,''ALL specials'', 1) =1 
)','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Specials Report
xxeis.eis_rs_ins.rs( 'Specials Report',401,'ORGANIZATION_CODE','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Specials Report',401,'PART_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Specials Report
xxeis.eis_rs_ins.rt( 'Specials Report',401,'Begin
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from (:Start Date);
xxeis.eis_rs_xxwc_com_util_pkg.set_date_to (:End Date);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Specials Report
--Inserting Report Portals - Specials Report
--Inserting Report Dashboards - Specials Report
--Inserting Report Security - Specials Report
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50619',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50924',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','51052',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50879',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50851',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50852',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','50821',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Specials Report','20005','','50880',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Specials Report','401','','51029',401,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Bin Location Data Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_INV_BIN_LOC_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_BIN_LOC_V
Create or replace View XXEIS.EIS_XXWC_INV_BIN_LOC_V
(ORGANIZATION,PART_NUMBER,DESCRIPTION,UOM,SELLING_PRICE,VENDOR_NAME,VENDOR_NUMBER,MIN_MINMAX_QUANTITY,MAX_MINMAX_QUANTITY,AVERAGECOST,WEIGHT,CAT,ONHAND,MTD_SALES,YTD_SALES,PRIMARY_BIN_LOC,ALTERNATE_BIN_LOC,BIN_LOC,LOCATION,INVENTORY_ITEM_ID,INV_ORGANIZATION_ID,ORG_ORGANIZATION_ID,APPLICATION_ID,SET_OF_BOOKS_ID) AS 
SELECT ood.organization_code ORGANIZATION,
    msi.segment1 part_number,
    msi.description,
    msi.primary_uom_code uom,
    msi.list_price_per_unit selling_price,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
    min_minmax_quantity ,
    max_minmax_quantity ,
    NVL(apps.Cst_Cost_Api.Get_Item_Cost(1,Msi.Inventory_Item_Id,Msi.Organization_Id),0) Averagecost,
    msi.unit_weight weight,
    xxeis.eis_rs_xxwc_com_util_pkg.Get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,
    xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id) onhand,
    xxeis.eis_rs_xxwc_com_util_pkg.get_mtd_sales(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) mtd_sales,
    xxeis.eis_rs_xxwc_com_util_pkg.get_ytd_sales(msi.inventory_item_id,msi.organization_id,gps.year_start_date) ytd_sales,
    xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc(msi.inventory_item_id,msi.organization_id) primary_bin_loc,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ALTERNATE_BIN_LOC(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) ALTERNATE_BIN_LOC,
    SUBSTR(SEGMENT1,3) BIN_LOC,
    ood.organization_name location,
    --primary keys
    msi.inventory_item_id,
    msi.organization_id inv_organization_id,
    ood.organization_id org_organization_id,
    gps.application_id,
    gps.set_of_books_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_system_items_kfv msi,
    org_organization_definitions ood,
    gl_period_statuses gps
  WHERE 1                 =1
  AND msi.organization_id =ood.organization_id
  AND application_id      =101
  AND gps.set_of_books_id =ood.set_of_books_id
  AND trunc(SYSDATE) BETWEEN gps.start_date AND gps.end_date
  and xxeis.eis_rs_xxwc_com_util_pkg.get_bin_loc_flag(msi.inventory_item_id,msi.organization_id)='Y'
 /* AND NOT EXISTS
    (SELECT 1
    FROM mtl_item_locations
    WHERE segment1 LIKE '1-%'
    AND inventory_item_id=msi.inventory_item_id
    AND organization_id  =msi.organization_id
    )*/
  AND EXISTS
    (SELECT 1
    FROM xxeis.eis_org_access_v
    where organization_id = msi.organization_id
    )
/
set scan on define on
prompt Creating View Data for Bin Location Data Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_BIN_LOC_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Rs Xxwc Inv Bin Loc V','EXIBLV');
--Delete View Columns for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_BIN_LOC_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','YTD_SALES',401,'Ytd Sales','YTD_SALES','','~,~2','','XXEIS_RS_ADMIN','NUMBER','DERIVED COLUMN','DERIVED COLUMN','Ytd Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MTD_SALES',401,'Mtd Sales','MTD_SALES','','~,~2','','XXEIS_RS_ADMIN','NUMBER','DERIVED COLUMN','DERIVED COLUMN','Mtd Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ONHAND',401,'Onhand','ONHAND','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','TRANSACTION_QUANTITY','Onhand');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~,~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Averagecost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MAX_MINMAX_QUANTITY',401,'Max Minmax Quantity','MAX_MINMAX_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','MAX_MINMAX_QUANTITY','Max Minmax Quantity');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MIN_MINMAX_QUANTITY',401,'Min Minmax Quantity','MIN_MINMAX_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','MIN_MINMAX_QUANTITY','Min Minmax Quantity');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','UOM',401,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','PRIMARY_UOM_CODE','Uom');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','DESCRIPTION','Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','SEGMENT1','Part Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ORGANIZATION',401,'Organization','ORGANIZATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ALTERNATE_BIN_LOC',401,'Alternate Bin Loc','ALTERNATE_BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Alternate Bin Loc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','BIN_LOC',401,'Bin Loc','BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin Loc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','LOCATION',401,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','WEIGHT',401,'Weight','WEIGHT','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','UNIT_WEIGHT','Weight');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','~~2','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','LIST_PRICE_PER_UNIT','Selling Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','CAT',401,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_CATEGORIES','SEGMENT2','Cat');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','INVENTORY_ITEM_ID','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','ORGANIZATION_ID','Inv Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_ID','Org Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','APPLICATION_ID',401,'Application Id','APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_PERIOD_STATUSES','APPLICATION_ID','Application Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','SET_OF_BOOKS_ID',401,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_PERIOD_STATUSES','SET_OF_BOOKS_ID','Set Of Books Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','PRIMARY_BIN_LOC',401,'Primary Bin Loc','PRIMARY_BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Primary Bin Loc');
--Inserting View Components for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_KFV','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Categories','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Code Combinations','','');
--Inserting View Component Joins for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIBLV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIBLV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','MTL_CATEGORIES','MCV',401,'EXIBLV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_CODE_COMBINATIONS_KFV','GCC',401,'EXIBLV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Bin Location Data Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Bin Location Data Report
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','EIS_INV_INVENTORY_ORGS_LOV1','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT distinct substr(SEGMENT1,3) bin_location FROM MTL_ITEM_LOCATIONS_KFV','','INV Bin Location','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT distinct substr(SEGMENT1,3) bin_location FROM MTL_ITEM_LOCATIONS_KFV','','INV Bin Location','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Bin Location Data Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Bin Location Data Report
xxeis.eis_rs_utility.delete_report_rows( 'Bin Location Data Report' );
--Inserting Report - Bin Location Data Report
xxeis.eis_rs_ins.r( 401,'Bin Location Data Report','','Items without Primary Bin Locations Assigned','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_BIN_LOC_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','HTML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Bin Location Data Report
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'AVERAGECOST','Average Cost','Averagecost','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'DESCRIPTION','Description','Description','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'MAX_MINMAX_QUANTITY','Max','Max Minmax Quantity','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'MIN_MINMAX_QUANTITY','Min','Min Minmax Quantity','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'MTD_SALES','MTD Units','Mtd Sales','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'ONHAND','Qty Onhand','Onhand','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'PART_NUMBER','Item Number','Part Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'UOM','UOM','Uom','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'YTD_SALES','YTD Units','Ytd Sales','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'VENDOR NUMBER-NAME','Vendor Number-Name','Ytd Sales','VARCHAR2','','','','4','Y','','','','','','','(EXIBLV.VENDOR_NUMBER||''-''||EXIBLV.VENDOR_NAME)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'ORGANIZATION','Organization','Organization','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'ALTERNATE_BIN_LOC','Alternate Bin Loc','Alternate Bin Loc','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'EXTENDED VALUE','Extended Value','Alternate Bin Loc','NUMBER','','','','8','Y','','','','','','','(EXIBLV.ONHAND*EXIBLV.AVERAGECOST)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'LOCATION','Location','Location','','','','','15','N','','ROW_FIELD','','','Location','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'PRIMARY_BIN_LOC','Primary Bin Loc','Primary Bin Loc','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'PRIMARY BIN','Primary bin count','Primary Bin Loc','NUMBER','','','','16','Y','','DATA_FIELD','','SUM','Number of Parts w/o Bin','2','decode(EXIBLV.primary_bin_loc,null,1,0)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
--Inserting Report Parameters - Bin Location Data Report
xxeis.eis_rs_ins.rp( 'Bin Location Data Report',401,'Organization','Organization','ORGANIZATION','IN','EIS_INV_INVENTORY_ORGS_LOV1','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location Data Report',401,'Starting Bin Location','Starting Bin Location','BIN_LOC','>=','INV Bin Location','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location Data Report',401,'Ending Bin Location','Ending Bin Location','BIN_LOC','<=','INV Bin Location','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Bin Location Data Report
xxeis.eis_rs_ins.rcn( 'Bin Location Data Report',401,'ORGANIZATION','IN',':Organization','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Bin Location Data Report
xxeis.eis_rs_ins.rs( 'Bin Location Data Report',401,'PART_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Bin Location Data Report
xxeis.eis_rs_ins.rt( 'Bin Location Data Report',401,'begin
xxeis.eis_rs_xxwc_com_util_pkg.g_start_bin:=:Starting Bin Location;
xxeis.eis_rs_xxwc_com_util_pkg.g_end_bin:=:Ending Bin Location;
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Bin Location Data Report
--Inserting Report Portals - Bin Location Data Report
--Inserting Report Dashboards - Bin Location Data Report
--Inserting Report Security - Bin Location Data Report
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50619',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50924',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','51052',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50879',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50851',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50852',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50821',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','20005','','50880',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','20634',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','','LB048272','',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','','10012196','',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','','MM050208','',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','','SO004816','',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','51029',401,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Hazmat Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_INV_HAZMAT_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_HAZMAT_V
Create or replace View XXEIS.EIS_XXWC_INV_HAZMAT_V
(LOCATION,PART_NUMBER,DESCRIPTION,CAPROP65,PESTICIDE_FLAG,VOCNUMBER,PESTICIDE_FLAG_STATE,VOC_CATEGORY,VOC_SUB_CATEGORY,MSDS_NUMBER,HAZMAT_FLAG,WEIGHT,UN_NUMBER,UN_DESCRIPTION,HAZARD_CLASS,HAZARDOUS_MATERIAL_DESCRIPTION,CONTAINER_TYPE,ORMD,PACKING_GROUP,COUNTRY_OF_ORIGIN,UOM,ONHAND,PERIOD_NAME,MINIMUM,MAXIMUM,INVENTORY_ITEM_ID,INV_ORGANIZATION_ID,ORG_ORGANIZATION_ID) AS 
SELECT ood.organization_code location,
  msi.segment1 part_number,
  msi.description,
  msi.attribute1 caprop65,
  msi.attribute3 pesticide_flag,
  msi.attribute4 vocnumber,
  msi.attribute5 pesticide_flag_state,
  msi.attribute6 voc_category,
  msi.attribute7 voc_sub_category,
  msi.attribute8 msds_number,
  msi.hazardous_material_flag hazmat_flag,
  msi.unit_weight weight,
  pun.un_number un_number,
  pun.description un_description,
  phc.hazard_class hazard_class,
  msi.attribute17 hazardous_material_description,
  msi.attribute18 container_type,
  msi.attribute11 ormd,
  msi.attribute9 packing_group,
  msi.attribute10 Country_of_Origin,
  msi.PRIMARY_UOM_CODE UOM,
  xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id) onhand,
  gps.period_name,
  msi.min_minmax_quantity minimum,
  msi.max_minmax_quantity maximum,
  --msi.hazard_class_id hazard_class_id,
  --primary keys
  msi.inventory_item_id,
  msi.organization_id inv_organization_id,
  ood.organization_id org_organization_id  
--descr#flexfield#start
--descr#flexfield#end
--gl#accountff#start
--gl#accountff#end
FROM MTL_SYSTEM_ITEMS_KFV MSI,
  ORG_ORGANIZATION_DEFINITIONS OOD,
  gl_period_statuses gps,
  po_hazard_classes phc,
  po_un_numbers pun
WHERE 1=1
and msi.organization_id    =ood.organization_id
AND MSI.ATTRIBUTE_CATEGORY='WC'
and msi.hazardous_material_flag='Y'
and msi.hazard_class_id    = phc.hazard_class_id
and msi.un_number_id       = pun.un_number_id (+)
and gps.application_id        =101
and gps.set_of_books_id       =ood.set_of_books_id
and trunc(SYSDATE) between gps.start_date and gps.end_date
AND EXISTS
    (select 1 from XXEIS.EIS_ORG_ACCESS_V where ORGANIZATION_ID = MSI.ORGANIZATION_ID
    )

/
set scan on define on
prompt Creating View Data for Hazmat Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_HAZMAT_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Rs Xxwc Inv Hazmat V','EXIHV');
--Delete View Columns for EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_HAZMAT_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MSDS_NUMBER',401,'Msds Number','MSDS_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Msds Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','VOC_SUB_CATEGORY',401,'Voc Sub Category','VOC_SUB_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Voc Sub Category');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','VOC_CATEGORY',401,'Voc Category','VOC_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Voc Category');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PESTICIDE_FLAG_STATE',401,'Pesticide Flag State','PESTICIDE_FLAG_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pesticide Flag State');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','VOCNUMBER',401,'Vocnumber','VOCNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vocnumber');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PESTICIDE_FLAG',401,'Pesticide Flag','PESTICIDE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pesticide Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','CAPROP65',401,'Caprop65','CAPROP65','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Caprop65');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','LOCATION',401,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','CONTAINER_TYPE',401,'Container Type','CONTAINER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Container Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','HAZARDOUS_MATERIAL_DESCRIPTION',401,'Hazardous Material Description','HAZARDOUS_MATERIAL_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hazardous Material Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','HAZARD_CLASS',401,'Hazard Class','HAZARD_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hazard Class');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MAXIMUM',401,'Maximum','MAXIMUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Maximum');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','MINIMUM',401,'Minimum','MINIMUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Minimum');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','ONHAND',401,'Onhand','ONHAND','','','','XXEIS_RS_ADMIN','NUMBER','','','Onhand');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','ORMD',401,'Ormd','ORMD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ormd');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PACKING_GROUP',401,'Packing Group','PACKING_GROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Packing Group');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','UN_DESCRIPTION',401,'Un Description','UN_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Un Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','UN_NUMBER',401,'Un Number','UN_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Un Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','WEIGHT',401,'Weight','WEIGHT','','','','XXEIS_RS_ADMIN','NUMBER','','','Weight');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inv Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','HAZMAT_FLAG',401,'Hazmat Flag','HAZMAT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Hazmat Flag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','PERIOD_NAME',401,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','COUNTRY_OF_ORIGIN',401,'Country Of Origin','COUNTRY_OF_ORIGIN','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Country Of Origin');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_HAZMAT_V','UOM',401,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom');
--Inserting View Components for EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_HAZMAT_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','');
--Inserting View Component Joins for EIS_XXWC_INV_HAZMAT_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_HAZMAT_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIHV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_HAZMAT_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIHV.INV_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Hazmat Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Hazmat Report
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','EIS_INV_INVENTORY_ORGS_LOV1','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct  attribute1 CA_Prop_65 from mtl_system_items_kfv where ATTRIBUTE_CATEGORY=''WC''','','INV CA Prop 65','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct attribute3 Pesticide_Flag from mtl_system_items_kfv where ATTRIBUTE_CATEGORY=''WC''','','INV Pesticide Flag','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select distinct attribute6 VOC_Category from mtl_system_items_kfv where ATTRIBUTE_CATEGORY=''WC''','','INV VOC Category','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Hazmat Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Hazmat Report
xxeis.eis_rs_utility.delete_report_rows( 'Hazmat Report' );
--Inserting Report - Hazmat Report
xxeis.eis_rs_ins.r( 401,'Hazmat Report','','Provide a listing of HazMat items: Chemical Report:  Inventory, UN #, ORMD, MSDS - Y/N; Chem Report:  VOC or insecticides by product; Chem Report:  Restricted Areas by product','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_HAZMAT_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,EXCEL,','N');
--Inserting Report Columns - Hazmat Report
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'CAPROP65','CA Prop 65','Caprop65','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'DESCRIPTION','Description','Description','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'LOCATION','Organization','Location','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'MSDS_NUMBER','MSDS Number','Msds Number','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'PART_NUMBER','Part Number','Part Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'PESTICIDE_FLAG','Pesticides Flag','Pesticide Flag','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'PESTICIDE_FLAG_STATE','Pesticide Flag State','Pesticide Flag State','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'VOCNUMBER','VOC G-L','Vocnumber','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'VOC_CATEGORY','VOC Category','Voc Category','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'VOC_SUB_CATEGORY','VOC Sub Category','Voc Sub Category','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'CONTAINER_TYPE','Container Type','Container Type','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'HAZARDOUS_MATERIAL_DESCRIPTION','Proper Shipping Name','Hazardous Material Description','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'HAZARD_CLASS','Hazard Class','Hazard Class','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'MAXIMUM','Max','Maximum','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'MINIMUM','Min','Minimum','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'ONHAND','Qty On Hand','Onhand','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'ORMD','Ormd','Ormd','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'PACKING_GROUP','Packing Group','Packing Group','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'UN_DESCRIPTION','Un Description','Un Description','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'UN_NUMBER','Un Number','Un Number','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'WEIGHT','Weight','Weight','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'HAZMAT_FLAG','Hazmat Flag','Hazmat Flag','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'COUNTRY_OF_ORIGIN','Country Of Origin','Country Of Origin','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
xxeis.eis_rs_ins.rc( 'Hazmat Report',401,'UOM','Uom','Uom','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_HAZMAT_V','','');
--Inserting Report Parameters - Hazmat Report
xxeis.eis_rs_ins.rp( 'Hazmat Report',401,'Organization','Organization','LOCATION','IN','EIS_INV_INVENTORY_ORGS_LOV1','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Hazmat Report',401,'CA Prop 65','CA Prop 65','CAPROP65','IN','INV CA Prop 65','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Hazmat Report',401,'Pesticide Flag','Pesticide Flag','PESTICIDE_FLAG','IN','INV Pesticide Flag','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Hazmat Report',401,'VOC Category','VOC Category','VOC_CATEGORY','IN','INV VOC Category','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Hazmat Report
xxeis.eis_rs_ins.rcn( 'Hazmat Report',401,'LOCATION','IN',':Organization','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Hazmat Report',401,'CAPROP65','IN',':CA Prop 65','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Hazmat Report',401,'PESTICIDE_FLAG','IN',':Pesticide Flag','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Hazmat Report',401,'VOC_CATEGORY','IN',':VOC Category','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Hazmat Report
xxeis.eis_rs_ins.rs( 'Hazmat Report',401,'PART_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Hazmat Report
--Inserting Report Templates - Hazmat Report
xxeis.eis_rs_ins.R_Tem( 'Hazmat Report','Hazmat Report','Provide a listing of HazMat items: Chemical Report:  Inventory, UN #, ORMD, MSDS - Y/N; Chem Report:  VOC or insecticides by product; Chem Report:  Restricted Areas by product','','','','','','','','','','','','XXEIS_RS_ADMIN');
--Inserting Report Portals - Hazmat Report
--Inserting Report Dashboards - Hazmat Report
--Inserting Report Security - Hazmat Report
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50619',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50924',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','51052',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50879',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50851',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50852',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','50821',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','20005','','50880',401,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Hazmat Report','401','','51029',401,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on


--Report Name            : Product Recall Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_PRODUCT_RECALL_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_PRODUCT_RECALL_V
Create or replace View XXEIS.EIS_XXWC_OM_PRODUCT_RECALL_V
(VENDOR_NAME,VENDOR_NUMBER,ONHAND_QTY,ORDERED_QTY,ITEM_NUMBER,ITEM_DESCRIPTION,SERIAL_NUMBER,LOT_NUMBER,ONHAND_LOCATION,CUSTOMER_SOLD_TO,SALES_ORDER_NUMBER,LINE_NUMBER,ORDERED_DATE,SOLD_FROM_WAREHOUSE,LINE_SHIPMENT,SOLD_ONHAND,HEADER_ID,LINE_ID,PARTY_ID,ORGANIZATION_ID,INVENTORY_ITEM_ID,MSI_ORGANIZATION_ID,DELIVERY_DETAIL_ID) AS 
SELECT xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_VENDOR_NUMBER(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) VENDOR_NUMBER,
    NULL ONHAND_QTY,
    NVL(ol.ordered_quantity,0) Ordered_Qty,
    Msi.Segment1 Item_Number ,
    MSI.DESCRIPTION ITEM_DESCRIPTION,
    --wdd.serial_number,
    CASE
      WHEN UPPER(wdd.source_header_type_name) LIKE 'STANDARD ORDER'
      THEN WDD.ATTRIBUTE1
      WHEN UPPER(wdd.source_header_type_name) IN( 'WC SHORT TERM RENTAL','WC LONG TERM RENTAL')
      THEN wdd.serial_number
    END serial_number,
    Wdd.Lot_Number,
    NULL onhand_location,
    NVL(hca.account_name,hzp.party_name) customer_sold_to,
    oh.order_number sales_order_number,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER
    ||'.'
    ||ol.option_number)) line_number,
    decode(otl.name, 'COUNTER LINE', nvl(trunc(SCHEDULE_SHIP_DATE),trunc(ol.creation_date)),NVL(TRUNC(ol.actual_shipment_date),trunc(ol.creation_date) ))ORDERED_DATE,
    mtp.Organization_Code Sold_From_Warehouse,
    ol.line_number
    ||'.'
    ||ol.shipment_number line_shipment,
    'sold' sold_onhand,
    ---Primary Keys
    oh.header_id ,
    ol.line_id ,
    hzp.party_id ,
    mtp.organization_id ,
    msi.inventory_item_id ,
    msi.organization_id msi_organization_id ,
    wdd.delivery_detail_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    hz_cust_accounts hca,
    hz_parties hzp,
    mtl_parameters mtp,
    hr_operating_units hrou,
    mtl_system_items_kfv msi,
    oe_transaction_types_vl ott,
    OE_TRANSACTION_TYPES_VL otl,
    wsh_delivery_details wdd
  WHERE ol.header_id            = oh.header_id
  AND oh.sold_to_org_id         = hca.cust_account_id(+)
  AND hca.party_id              = hzp.party_id(+)
  AND ol.ship_from_org_id       = mtp.organization_id(+)
  AND hrou.organization_id      = ol.sold_from_org_id(+)
  AND msi.organization_id       =ol.SHIP_from_org_id
  AND msi.inventory_item_id     =ol.inventory_item_id
  AND oh.order_type_id          = ott.transaction_type_id
  AND ol.line_type_id           = otl.transaction_type_id
  AND wdd.source_header_id(+)   = ol.header_id
  AND wdd.source_line_id(+)     = ol.line_id
  AND NVL(wdd.source_code,'OE') = 'OE'
  AND ol.flow_status_code       ='CLOSED'
  AND msi.item_type NOT        IN('NON-STOCK','RENTAL','AOC','PTO','RE_RENT')
  AND EXISTS
    (SELECT 1
    FROM XXEIS.EIS_ORG_ACCESS_V
    WHERE ORGANIZATION_ID = MSI.ORGANIZATION_ID
    )
UNION
  SELECT xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
    SUM(moqd.transaction_quantity) ONHAND_QTY,
    NULL Ordered_Qty,
    Msi.Segment1 Item_Number ,
    Msi.Description Item_Description,
    Mut.Serial_Number Serial_Number,
    mtln.lot_number Lot_Number,
    Mtp.Organization_Code Onhand_Location,
    NULL CUSTOMER_SOLD_TO,
    NULL sales_order_number,
    NULL LINE_NUMBER,
    NULL ordered_date,
    NULL Sold_From_Warehouse,
    -- MAX(Moqd.date_received) ordered_date,
    --Mtp.Organization_Code Sold_From_Warehouse,
    NULL line_shipment,
    'onhand' sold_onhand,
    ---Primary Keys
    NULL header_id ,
    NULL line_id ,
    NULL PARTY_ID ,
    NULL organization_id ,
    msi.inventory_item_id ,
    MSI.ORGANIZATION_ID MSI_ORGANIZATION_ID ,
    NULL DELIVERY_DETAIL_ID
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM Mtl_System_Items_Kfv Msi,
    Mtl_Onhand_Quantities_Detail Moqd ,
    Mtl_Unit_Transactions Mut,
    MTL_TRANSACTION_LOT_NUMBERS MTLN,
    mtl_parameters mtp,
    gl_period_statuses gps,
    org_organization_definitions ood
  WHERE 1                      =1
  AND Msi.Inventory_Item_Id    =Moqd.Inventory_Item_Id
  AND Msi.Organization_Id      =Moqd.Organization_Id
  AND Mut.Inventory_Item_Id(+) =Moqd.Inventory_Item_Id
  AND Mut.Organization_Id(+)   =Moqd.Organization_Id
  AND Mut.Transaction_Id (+)   =Moqd.Update_Transaction_Id
  AND Mtln.Inventory_Item_Id(+)=Moqd.Inventory_Item_Id
  AND Mtln.Organization_Id (+) =Moqd.Organization_Id
  AND Mtln.Transaction_Id (+)  =Moqd.Update_Transaction_Id
  AND mtp.organization_id      =msi.organization_id
  AND OOD.SET_OF_BOOKS_ID      =GPS.SET_OF_BOOKS_ID
  AND MSI.ORGANIZATION_ID      =OOD.ORGANIZATION_ID
  AND GPS.APPLICATION_ID       =101
  AND TRUNC(sysdate) BETWEEN TRUNC(GPS.START_DATE) AND TRUNC(GPS.END_DATE)
  AND msi.item_type NOT IN('NON-STOCK','RENTAL','AOC','PTO','RE_RENT')
  AND EXISTS
    (SELECT 1
    FROM XXEIS.EIS_ORG_ACCESS_V
    WHERE organization_id = msi.organization_id
    )
  GROUP BY xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) ,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) ,
    Msi.Segment1 ,
    Msi.Description ,
    Mut.Serial_Number ,
    mtln.lot_number ,
    Mtp.Organization_Code ,
    NULL,
    NULL ,
    NULL ,
    NULL ,
    NULL ,
    Mtp.Organization_Code ,
    NULL ,
    'onhand' ,
    NULL ,
    NULL ,
    NULL ,
    msi.inventory_item_id ,
    msi.organization_id ,
    NULL
/
set scan on define on
prompt Creating View Data for Product Recall Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_PRODUCT_RECALL_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Product Recall V','EXOPRV1');
--Delete View Columns for EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_PRODUCT_RECALL_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','SALES_ORDER_NUMBER',660,'Sales Order Number','SALES_ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','CUSTOMER_SOLD_TO',660,'Customer Sold To','CUSTOMER_SOLD_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Sold To');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ONHAND_LOCATION',660,'Onhand Location','ONHAND_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Onhand Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','SOLD_FROM_WAREHOUSE',660,'Sold From Warehouse','SOLD_FROM_WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sold From Warehouse');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','LOT_NUMBER',660,'Lot Number','LOT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Lot Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','SERIAL_NUMBER',660,'Serial Number','SERIAL_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Serial Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ONHAND_QTY',660,'Onhand Qty','ONHAND_QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Onhand Qty');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','SOLD_ONHAND',660,'Sold Onhand','SOLD_ONHAND','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sold Onhand');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','DELIVERY_DETAIL_ID',660,'Delivery Detail Id','DELIVERY_DETAIL_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Delivery Detail Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msi Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','LINE_SHIPMENT',660,'Line Shipment','LINE_SHIPMENT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Shipment');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_PRODUCT_RECALL_V','ORDERED_QTY',660,'Ordered Qty','ORDERED_QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Ordered Qty');
--Inserting View Components for EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_PRODUCT_RECALL_V','WSH_DELIVERY_DETAILS',660,'WSH_DELIVERY_DETAILS','WDD','WDD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Delivery Details','','');
--Inserting View Component Joins for EIS_XXWC_OM_PRODUCT_RECALL_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','OE_ORDER_HEADERS','OH',660,'EXOPRV1.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','OE_ORDER_LINES','OL',660,'EXOPRV1.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','HZ_PARTIES','HZP',660,'EXOPRV1.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_PARAMETERS','MTP',660,'EXOPRV1.ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOPRV1.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOPRV1.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_PRODUCT_RECALL_V','WSH_DELIVERY_DETAILS','WDD',660,'EXOPRV1.DELIVERY_DETAIL_ID','=','WDD.DELIVERY_DETAIL_ID(+)','','','','Y','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Product Recall Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Product Recall Report
xxeis.eis_rs_ins.lov( 660,'select CONCATENATED_SEGMENTS ITEM_NAME,DESCRIPTION ,ood.ORGANIZATION_NAME FROM  MTL_SYSTEM_ITEMS_KFV MSI ,org_organization_definitions ood WHERE  OOD.ORGANIZATION_ID=MSI.ORGANIZATION_ID and  exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = msi.organization_id)
','','OM ITEM NAME','This gives the item name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT LOT_NUMBER,HOU.NAME ORGANIZATION_NAME from mtl_lot_numbers ml ,hr_all_organization_units hou WHERE ML.ORGANIZATION_ID=HOU.ORGANIZATION_ID AND exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = ml.organization_id)
','','OM LOT NUMBER','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT LOT_NUMBER,HOU.NAME ORGANIZATION_NAME from mtl_lot_numbers ml ,hr_all_organization_units hou WHERE ML.ORGANIZATION_ID=HOU.ORGANIZATION_ID AND exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = ml.organization_id)
','','OM LOT NUMBER','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT serial_number,hou.organization_name current_organization FROM mtl_serial_numbers msn,org_organization_definitions hou WHERE HOU.ORGANIZATION_ID=MSN.CURRENT_ORGANIZATION_ID  and exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = hou.organization_id)
','','OM SERIAL NUMBER','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT serial_number,hou.organization_name current_organization FROM mtl_serial_numbers msn,org_organization_definitions hou WHERE HOU.ORGANIZATION_ID=MSN.CURRENT_ORGANIZATION_ID  and exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = hou.organization_id)
','','OM SERIAL NUMBER','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT VENDOR_NAME 
    FROM PO_VENDORS POV','','OM Vendor Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Product Recall Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Product Recall Report
xxeis.eis_rs_utility.delete_report_rows( 'Product Recall Report' );
--Inserting Report - Product Recall Report
xxeis.eis_rs_ins.r( 660,'Product Recall Report','','Provide visibility to products (generally tools) on-hand and sold to customers that have been Recalled by the Vendor.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_PRODUCT_RECALL_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Product Recall Report
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'CUSTOMER_SOLD_TO','Customer Sold To','Customer Sold To','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'ITEM_NUMBER','Item Number','Item Number','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'ONHAND_LOCATION','Onhand Location','Onhand Location','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'SALES_ORDER_NUMBER','Sales Order Number','Sales Order Number','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'SOLD_FROM_WAREHOUSE','Sold From Warehouse','Sold From Warehouse','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'LOT_NUMBER','Lot Number','Lot Number','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'ORDERED_DATE','Sale Date','Ordered Date','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'LINE_NUMBER','Line Number','Line Number','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'ONHAND_QTY','Onhand Qty','Onhand Qty','','','','','13','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
xxeis.eis_rs_ins.rc( 'Product Recall Report',660,'ORDERED_QTY','Ordered Qty','Ordered Qty','','','','','14','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PRODUCT_RECALL_V','','');
--Inserting Report Parameters - Product Recall Report
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Item Number','Item Number','ITEM_NUMBER','IN','OM ITEM NAME','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Vendor Name','Vendor Name','VENDOR_NAME','IN','OM Vendor Name LOV','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Lot Number From','Lot Number From','LOT_NUMBER','>=','OM LOT NUMBER','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Serial Number From','Serial Number From','SERIAL_NUMBER','>=','OM SERIAL NUMBER','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Sold Date From','Sold Date From','ORDERED_DATE','>=','','','DATE','N','Y','3','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Sold Date To','Sold Date To','ORDERED_DATE','<=','','','DATE','N','Y','4','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Serial Number To','Serial Number To','SERIAL_NUMBER','<=','OM SERIAL NUMBER','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Lot Number To','Lot Number To','LOT_NUMBER','<=','OM LOT NUMBER','','VARCHAR2','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Branch From','Branch From','SOLD_FROM_WAREHOUSE','>=','OM WAREHOUSE','','VARCHAR2','N','Y','9','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Product Recall Report',660,'Branch To','Branch To','SOLD_FROM_WAREHOUSE','<=','OM WAREHOUSE','','VARCHAR2','N','Y','10','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Product Recall Report
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'VENDOR_NAME','IN',':Vendor Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'ITEM_NUMBER','IN',':Item Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'NVL(ORDERED_DATE,:Sold Date From)','>=',':Sold Date From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'NVL(ORDERED_DATE,:Sold Date To)','<=',':Sold Date To','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'SERIAL_NUMBER','>=',':Serial Number From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'SERIAL_NUMBER','<=',':Serial Number To','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'LOT_NUMBER','>=',':Lot Number From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'LOT_NUMBER','<=',':Lot Number To','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'SOLD_FROM_WAREHOUSE','>=',':Branch From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Product Recall Report',660,'SOLD_FROM_WAREHOUSE','<=',':Branch To','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Product Recall Report
xxeis.eis_rs_ins.rs( 'Product Recall Report',660,'VENDOR_NAME','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Product Recall Report
--Inserting Report Templates - Product Recall Report
--Inserting Report Portals - Product Recall Report
--Inserting Report Dashboards - Product Recall Report
--Inserting Report Security - Product Recall Report
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50926',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50927',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50928',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50929',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50931',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50930',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','701','','50546',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Product Recall Report','','SO004816','',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Daily Flash Cash Report - for Sales Revenue as % to Plan
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_DAILY_FLASH_SALE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
Create or replace View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
(DATE1,BRANCH_LOCATION,SALE_AMT,SALE_COST,PAYMENT_TYPE,INVOICE_COUNT) AS 
SELECT TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from) date1,
    OOD.ORGANIZATION_CODE BRANCH_LOCATION,
    NVL(SUM(DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL(Ol.Unit_Selling_Price,0)),0) SALE_AMT,
    SUM (NVL(ol.ordered_quantity,0)                                            *NVL(ol.unit_cost,0))sale_cost,
    -- SUM(oep.payment_amount) cash_sales,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE PAYMENT_TYPE,
    COUNT( DISTINCT XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.get_dist_invoice_num(rct.customer_trx_id,rct.trx_number, otlh.name)) invoice_count
    ---Primary Keys
  FROM Ra_Customer_Trx Rct,
    ra_customer_trx_lines rctl,
    Oe_Order_Headers Oh,
    Oe_Order_Lines Ol,
    Org_Organization_Definitions Ood,
    OE_TRANSACTION_TYPES_VL otl,
    OE_TRANSACTION_TYPES_VL otlh
  WHERE 1                                             = 1
  AND TRUNC(oh.ordered_DATE)                          = TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
  AND TO_CHAR(oh.order_number)                        = TO_CHAR(rct.interface_header_attribute1)
  AND Ood.Organization_Id (+)                         = Ol.Ship_From_Org_Id
  AND TO_CHAR(rctl.interface_line_attribute6)         = TO_CHAR(Ol.Line_id)
  AND RCTL.INVENTORY_ITEM_ID                          = OL.INVENTORY_ITEM_ID
  AND OTL.TRANSACTION_TYPE_ID                         = Ol.LINE_TYPE_ID
  AND OTLH.TRANSACTION_TYPE_ID                        = OH.ORDER_TYPE_ID
  AND rctl.customer_trx_id                            = rct.customer_trx_id
  AND ol.header_id                                    = oh.header_id
  AND rctl.interface_line_context(+)                  = 'ORDER ENTRY'
  AND((xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type='CASH'
  AND (EXISTS
    (SELECT 1
    FROM oe_payments oep
    WHERE (( oep.payment_type_code IN ('CASH','CHECK', 'CREDIT_CARD')
    AND OEP.HEADER_ID               = OL.HEADER_ID ) )
    )
  OR (otl.order_category_code = 'RETURN'
  AND EXISTS
    (SELECT 1
    FROM XXWC_OM_CASH_REFUND_TBL XOC
    WHERE XOC.RETURN_HEADER_ID=OH.HEADER_ID
    ) ) ) )
  OR (xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type='CHARGE'
  AND ( (NOT EXISTS
    (SELECT 1 FROM oe_payments oep WHERE OEP.HEADER_ID = OL.HEADER_ID
    )
  AND otl.order_category_code <> 'RETURN' )
  OR(otl.order_category_code   = 'RETURN'
  AND NOT EXISTS
    (SELECT 1
    FROM XXWC_OM_CASH_REFUND_TBL XOC1
    WHERE XOC1.RETURN_HEADER_ID=OH.HEADER_ID
    ) ) ) ))
  GROUP BY TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from),
    OOD.ORGANIZATION_CODE,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE
/
set scan on define on
prompt Creating View Data for Daily Flash Cash Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Daily Flash Sale V','EXADFSV');
--Delete View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','DATE1',660,'Date1','DATE1','','','','XXEIS_RS_ADMIN','DATE','','','Date1');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_AMT',660,'Sale Amt','SALE_AMT','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Amt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_COST',660,'Sale Cost','SALE_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','PAYMENT_TYPE',660,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type');
--Inserting View Components for EIS_XXWC_AR_DAILY_FLASH_SALE_V
--Inserting View Component Joins for EIS_XXWC_AR_DAILY_FLASH_SALE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Cash Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Cash Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Cash Report - for Sales Revenue as % to Plan' );
--Inserting Report - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.r( 660,'Daily Flash Cash Report - for Sales Revenue as % to Plan','','The purpose of this extract is to provide Finance with a daily report of all cash sales by branch.  The report received on 2/28 will report the daily cash sales by branch for 2/27.

This report is to be processed daily and delivered via email to the defined distribution list.  This report will accompany the daily extracts, flash_charge and routing_info.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_DAILY_FLASH_SALE_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','Location','Branch Location','','','','','2','N','','ROW_FIELD','','','Location','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'DATE1','Date','Date1','','','','','1','N','','ROW_FIELD','','','Date','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'GROSS_PROFIT','GP$','Invoice Count','NUMBER','~~2','','','5','Y','','','','','','','(NVL(EXADFSV.SALE_AMT,0)-NVL(EXADFSV.sale_cost,0))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'SALE_AMT','Net Sales Dollars','Sale Amt','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
--Inserting Report Parameters - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rp( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Location','WC Branch Number','BRANCH_LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Date','Prior days date','DATE1','IN','','select trunc(sysdate-1) from dual','DATE','Y','Y','1','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rcn( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'DATE1','IN',':Date','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rs( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'DATE1','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rt( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_payment_type(''CASH'');
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Cash Report - for Sales Revenue as % to Plan
--Inserting Report Portals - Daily Flash Cash Report - for Sales Revenue as % to Plan
--Inserting Report Dashboards - Daily Flash Cash Report - for Sales Revenue as % to Plan
--Inserting Report Security - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','51030',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50638',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50622',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','401','','50941',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','21404',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','20678',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50846',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50845',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50847',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50848',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50849',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50944',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50871',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','SO004816','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50861',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Daily Flash Charge Report - for Sales Revenue as % to Plan
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_DAILY_FLASH_SALE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
Create or replace View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
(DATE1,BRANCH_LOCATION,SALE_AMT,SALE_COST,PAYMENT_TYPE,INVOICE_COUNT) AS 
SELECT TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from) date1,
    OOD.ORGANIZATION_CODE BRANCH_LOCATION,
    NVL(SUM(DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL(Ol.Unit_Selling_Price,0)),0) SALE_AMT,
    SUM (NVL(ol.ordered_quantity,0)                                            *NVL(ol.unit_cost,0))sale_cost,
    -- SUM(oep.payment_amount) cash_sales,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE PAYMENT_TYPE,
    COUNT( DISTINCT XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.get_dist_invoice_num(rct.customer_trx_id,rct.trx_number, otlh.name)) invoice_count
    ---Primary Keys
  FROM Ra_Customer_Trx Rct,
    ra_customer_trx_lines rctl,
    Oe_Order_Headers Oh,
    Oe_Order_Lines Ol,
    Org_Organization_Definitions Ood,
    OE_TRANSACTION_TYPES_VL otl,
    OE_TRANSACTION_TYPES_VL otlh
  WHERE 1                                             = 1
  AND TRUNC(oh.ordered_DATE)                          = TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
  AND TO_CHAR(oh.order_number)                        = TO_CHAR(rct.interface_header_attribute1)
  AND Ood.Organization_Id (+)                         = Ol.Ship_From_Org_Id
  AND TO_CHAR(rctl.interface_line_attribute6)         = TO_CHAR(Ol.Line_id)
  AND RCTL.INVENTORY_ITEM_ID                          = OL.INVENTORY_ITEM_ID
  AND OTL.TRANSACTION_TYPE_ID                         = Ol.LINE_TYPE_ID
  AND OTLH.TRANSACTION_TYPE_ID                        = OH.ORDER_TYPE_ID
  AND rctl.customer_trx_id                            = rct.customer_trx_id
  AND ol.header_id                                    = oh.header_id
  AND rctl.interface_line_context(+)                  = 'ORDER ENTRY'
  AND((xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type='CASH'
  AND (EXISTS
    (SELECT 1
    FROM oe_payments oep
    WHERE (( oep.payment_type_code IN ('CASH','CHECK', 'CREDIT_CARD')
    AND OEP.HEADER_ID               = OL.HEADER_ID ) )
    )
  OR (otl.order_category_code = 'RETURN'
  AND EXISTS
    (SELECT 1
    FROM XXWC_OM_CASH_REFUND_TBL XOC
    WHERE XOC.RETURN_HEADER_ID=OH.HEADER_ID
    ) ) ) )
  OR (xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type='CHARGE'
  AND ( (NOT EXISTS
    (SELECT 1 FROM oe_payments oep WHERE OEP.HEADER_ID = OL.HEADER_ID
    )
  AND otl.order_category_code <> 'RETURN' )
  OR(otl.order_category_code   = 'RETURN'
  AND NOT EXISTS
    (SELECT 1
    FROM XXWC_OM_CASH_REFUND_TBL XOC1
    WHERE XOC1.RETURN_HEADER_ID=OH.HEADER_ID
    ) ) ) ))
  GROUP BY TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from),
    OOD.ORGANIZATION_CODE,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE
/
set scan on define on
prompt Creating View Data for Daily Flash Charge Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Daily Flash Sale V','EXADFSV');
--Delete View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','DATE1',660,'Date1','DATE1','','','','XXEIS_RS_ADMIN','DATE','','','Date1');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_AMT',660,'Sale Amt','SALE_AMT','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Amt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_COST',660,'Sale Cost','SALE_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','PAYMENT_TYPE',660,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type');
--Inserting View Components for EIS_XXWC_AR_DAILY_FLASH_SALE_V
--Inserting View Component Joins for EIS_XXWC_AR_DAILY_FLASH_SALE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Charge Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Charge Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Charge Report - for Sales Revenue as % to Plan' );
--Inserting Report - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.r( 660,'Daily Flash Charge Report - for Sales Revenue as % to Plan','','The purpose of this extract is to provide Finance with a daily report of all cash sales by branch.  The report received on 2/28 will report the daily cash sales by branch for 2/27.

This report is to be processed daily and delivered via email to the defined distribution list.  This report will accompany the daily extracts, flash_charge and routing_info.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_DAILY_FLASH_SALE_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'GROSS_PROFIT','GP$','','NUMBER','~~2','','','5','Y','','DATA_FIELD','','SUM','','4','(nvl(EXADFSV.SALE_AMT,0)-nvl(EXADFSV.sale_cost,0))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','Location','Branch Location','','','','','2','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','','','','3','N','','DATA_FIELD','','COUNT','Invoice Count','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'SALE_AMT','Net Sales Dollars','Sale Amt','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'DATE1','Date','Date1','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
--Inserting Report Parameters - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rp( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'Location','WC Branch Number','BRANCH_LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'Date','Prior days date','DATE1','IN','','select trunc(sysdate-1) from dual','DATE','Y','Y','1','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rcn( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'DATE1','IN',':Date','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rs( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rt( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_payment_type(''CHARGE'');
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Charge Report - for Sales Revenue as % to Plan
--Inserting Report Portals - Daily Flash Charge Report - for Sales Revenue as % to Plan
--Inserting Report Dashboards - Daily Flash Charge Report - for Sales Revenue as % to Plan
--Inserting Report Security - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','51030',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50638',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50622',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','401','','50941',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','21404',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','20678',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50846',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50845',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50847',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50848',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50849',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50944',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50871',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','SO004816','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50861',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
Create or replace View XXEIS.EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
(CURRENT_FISCAL_MONTH,CURRENT_FISCAL_YEAR,DELIVERY_TYPE,BRANCH_LOCATION,INVOICE_COUNT,SALES) AS 
SELECT Gp.Period_Name Current_Fiscal_Month,
    GP.PERIOD_YEAR CURRENT_FISCAL_YEAR,
    xxeis.eis_rs_xxwc_com_util_pkg.get_shipping_mthd3(ol.shipping_method_code) delivery_type,
    mp.organization_code branch_location,
    COUNT( DISTINCT XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.get_dist_invoice_num(rct.customer_trx_id,rct.trx_number, otlh.name))  INVOICE_COUNT,
    NVL(SUM(DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL(Ol.Unit_Selling_Price,0)),0) sales
    -- SUM(oep.payment_amount) cash_sales,
    --   xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type payment_type
  FROM ra_customer_trx rct,
    Ra_Customer_Trx_Lines Rctl,
    Oe_Order_Headers Oh,
    Oe_Order_Lines Ol,
    Org_Organization_Definitions Mp,
    Gl_Periods Gp ,
    Gl_Sets_Of_Books Gsob,
    OE_TRANSACTION_TYPES_VL otl,
    OE_TRANSACTION_TYPES_VL otlh
  WHERE 1                            = 1
  AND TRUNC(oh.ordered_date)         = TRUNC(Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Date_From)
  AND RCTL.CUSTOMER_TRX_ID           = RCT.CUSTOMER_TRX_ID
  AND rctl.interface_line_attribute6 = TO_CHAR(Ol.Line_id)
  AND Rctl.Inventory_Item_Id         = Ol.Inventory_Item_Id
  AND OL.HEADER_ID                   = OH.HEADER_ID
  AND OTL.TRANSACTION_TYPE_ID        = Ol.LINE_TYPE_ID
  AND OTLH.TRANSACTION_TYPE_ID       = OH.ORDER_TYPE_ID
  AND TO_CHAR(Oh.Order_Number)       = Rct.Interface_Header_Attribute1
  AND Mp.Organization_Id (+)         = Ol.Ship_From_Org_Id
  AND Gsob.Set_Of_Books_Id           = Mp.Set_Of_Books_Id
  AND Gsob.Period_Set_Name           = Gp.Period_Set_Name
  AND TRUNC(oh.ordered_date) BETWEEN gp.start_date AND gp.end_date
    /*AND EXISTS
    (SELECT 1
    FROM oe_payments oep
    WHERE oep.header_id        = ol.header_id
    AND oep.payment_type_code IN ('CASH','CREDIT_CARD')
    )*/
  AND rctl.INTERFACE_LINE_CONTEXT(+) = 'ORDER ENTRY'
  GROUP BY GP.PERIOD_NAME,
    GP.PERIOD_YEAR,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_SHIPPING_MTHD3(OL.SHIPPING_METHOD_CODE),
    MP.ORGANIZATION_CODE
/
set scan on define on
prompt Creating View Data for Daily Flash Delivery Report  for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Flsh Sale By Del V','EXAFSBDV');
--Delete View Columns for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_FLSH_SALE_BY_DEL_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','CURRENT_FISCAL_MONTH',660,'Current Fiscal Month','CURRENT_FISCAL_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Current Fiscal Month');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','CURRENT_FISCAL_YEAR',660,'Current Fiscal Year','CURRENT_FISCAL_YEAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Current Fiscal Year');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','DELIVERY_TYPE',660,'Delivery Type','DELIVERY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Delivery Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','SALES',660,'Sales','SALES','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count');
--Inserting View Components for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
--Inserting View Component Joins for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Delivery Report  for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Delivery Report  for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan' );
--Inserting Report - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.r( 660,'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','The purpose of this extract is to provide Finance with a daily report of all sales by branch and aggregated by delivery type.  The report received on 2/28 will report the daily sales by branch and delivery type for 2/27.

This report is to be processed daily and delivered via email to the defined distribution list.  This report will accompany the daily extracts, flash_charge and flash_cash.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,EXCEL,','N');
--Inserting Report Columns - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','Location','Branch Location','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'DELIVERY_TYPE','Delivery Type','Delivery Type','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'SALES','Net Sales Dollars','Sales','','','','','6','N','','','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'CURRENT_FISCAL_MONTH','Fiscal Month','Current Fiscal Month','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'CURRENT_FISCAL_YEAR','Fiscal Year','Current Fiscal Year','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
--Inserting Report Parameters - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'Location','Location','BRANCH_LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'Date','Prior days date','','IN','','select trunc(sysdate-1) from dual','DATE','Y','Y','1','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rcn( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'','','','','--and EXAFSBDV.delivery_type is not null','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Inserting Report Triggers - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rt( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Inserting Report Portals - Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Inserting Report Dashboards - Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Inserting Report Security - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','SO004816','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','21623',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Invoice Pre-Register report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_INV_PRE_REG_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_INV_PRE_REG_V
Create or replace View XXEIS.EIS_XXWC_OM_INV_PRE_REG_V
(ORDER_NUMBER,ORDERED_DATE,CREATED_BY,ORDER_TYPE,ORDER_STATUS,CUSTOMER_NUMBER,CUSTOMER_NAME,GROSS_AMOUNT,DISCOUNT_AMT,TAX_VALUE,FREIGHT_AMT,AVERAGE_COST,PROFIT,SALESREP_NAME,SALESREP_NUMBER,WAREHOUSE,INV_AMOUNT,SALES,SALE_COST,HEADER_ID,PARTY_ID,ORGANIZATION_ID) AS 
SELECT oh.order_number,
    trunc(oh.ordered_date) ordered_date,
    PPF.FULL_NAME CREATED_BY,
    oth.Name Order_Type,
    NVL(flv.meaning, initcap(REPLACE(OH.FLOW_STATUS_CODE,'_',' '))) Order_Status,
    CUST_ACCT.ACCOUNT_NUMBER CUSTOMER_NUMBER,
    NVL(CUST_ACCT.ACCOUNT_NAME,PARTY.PARTY_NAME) CUSTOMER_NAME,
    SUM(NVL(ol.unit_selling_price,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity)) gross_amount,
    SUM(NVL(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DISCOUNT_AMT(OL.HEADER_ID,OL.LINE_ID),0)) DISCOUNT_AMT,
    SUM(NVL(DECODE(otl.order_category_code,'RETURN',(ol.tax_value*-1),ol.tax_value),0)) tax_value,
    SUM(NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_freight_amt(ol.header_id,ol.line_id),0)) freight_amt,
    SUM(ROUND(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0),2)) average_cost,
    SUM(((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (NVL(OL.UNIT_COST,0) * OL.ORDERED_QUANTITY))) PROFIT,
    rep.name SALESREP_NAME,
    Rep.Salesrep_Number,
    SHIP_FROM_ORG.ORGANIZATION_CODE WAREHOUSE,
    MAX(NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_invoice_amt (oh.header_id),0)) inv_amount,
    SUM(NVL(ol.unit_selling_price,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))SALES,
    SUM(NVL(ol.Unit_Cost,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))SALE_COST,
    ---Primary Keys
    oh.header_id ,
    party.party_id ,
    -- Rct.trx_number,
    ship_from_org.organization_id
    --    msi.inventory_item_id ,
    --    msi.organization_id msi_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org,
    hz_parties party,
    hz_cust_accounts cust_acct,
    oe_order_headers oh,
    oe_order_lines ol,
    Hr_ALL_ORGANIZATION_UNITS Hou,
    Ra_Salesreps Rep,
    oe_transaction_types_vl oth,
    Oe_Transaction_Types_Vl otl,
    MTL_SYSTEM_ITEMS_KFV MSI,
    fnd_lookup_values_vl flv,
    per_people_f ppf,
    fnd_user fu       
    -- ar_payment_schedules ps,
    --    ra_customer_trx rct
  WHERE Ol.Sold_To_Org_Id      = Cust_Acct.Cust_Account_Id(+)
  AND cust_acct.party_id       = party.party_id(+)
  AND oh.Ship_From_Org_Id      = Ship_From_Org.Organization_Id(+)
  AND Ol.Header_Id             = Oh.Header_Id
  AND msi.organization_id      = hou.organization_id
  AND oh.salesrep_id           = rep.salesrep_id(+)
  AND Msi.Inventory_Item_Id(+) = Ol.Inventory_Item_Id
  AND Msi.Organization_Id (+)  = Ol.Ship_From_Org_Id
  AND Oh.Order_Type_Id         = oth.Transaction_Type_Id
  AND oh.org_id                = oth.org_id
  AND ol.line_Type_Id          =Otl.Transaction_Type_Id
  AND ol.org_id                = otl.org_id
  AND flv.lookup_type(+)       ='FLOW_STATUS'
  AND FLV.LOOKUP_CODE(+)       = OH.FLOW_STATUS_CODE
    --  and oh.order_number=10003570
    --  AND TO_CHAR(OH.ORDER_NUMBER) = TO_CHAR(RCT.INTERFACE_HEADER_ATTRIBUTE1)
    -- AND PS.CUSTOMER_TRX_ID       =RCT.CUSTOMER_TRX_ID
    --AND PS.CLASS                <>'PMT'
  AND TRUNC(oh.ordered_date)           < TRUNC(sysdate)
  AND ol.flow_status_code             IN ('CLOSED' )
  and ol.invoice_interface_status_code = 'YES'
  and fu.user_id          =ol.created_by  
  AND FU.EMPLOYEE_ID      =PPF.PERSON_ID(+)  
  and trunc (ol.creation_date) between nvl (ppf.effective_start_date, trunc (ol.creation_date) ) and nvl (ppf.effective_end_date, trunc (ol.creation_date) )
  GROUP BY OH.ORDER_NUMBER,
    trunc(oh.ordered_date),
    PPF.FULL_NAME,
    OTH.NAME,
    NVL(FLV.MEANING, INITCAP(REPLACE(OH.FLOW_STATUS_CODE,'_',' '))),
    CUST_ACCT.ACCOUNT_NUMBER,
    NVL(CUST_ACCT.ACCOUNT_NAME,PARTY.PARTY_NAME),
    rep.name,
    REP.SALESREP_NUMBER,
    SHIP_FROM_ORG.ORGANIZATION_CODE,
    OH.HEADER_ID,
    PARTY.PARTY_ID,
    SHIP_FROM_ORG.ORGANIZATION_ID
  UNION ALL
  SELECT oh.order_number,
    trunc(oh.ordered_date) ordered_date,
    PPF.FULL_NAME CREATED_BY,
    oth.Name Order_Type,
    NVL(flv.meaning, initcap(REPLACE(OH.FLOW_STATUS_CODE,'_',' '))) Order_Status,
    CUST_ACCT.ACCOUNT_NUMBER CUSTOMER_NUMBER,
    NVL(cust_acct.account_name,party.party_name) customer_name,
    SUM(NVL(ol.unit_selling_price,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity)) gross_amount,
    SUM(NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_discount_amt(ol.header_id,ol.line_id),0)) discount_amt,
    SUM(NVL(DECODE(otl.order_category_code,'RETURN',(ol.tax_value*-1),ol.tax_value),0)) tax_value,
    SUM(NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_freight_amt(ol.header_id,ol.line_id),0)) freight_amt,
    SUM(ROUND(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0),2)) average_cost,
    SUM(((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY)- (NVL(OL.UNIT_COST,0) * OL.ORDERED_QUANTITY))) PROFIT,
    rep.name salesrep_name,
    Rep.Salesrep_Number,
    SHIP_FROM_ORG.ORGANIZATION_CODE WAREHOUSE,
    MAX(NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_invoice_amt (oh.header_id),0)) inv_amount,
    SUM(NVL(ol.unit_selling_price,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))SALES,
    SUM(NVL(ol.Unit_Cost,0) * DECODE(otl.order_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))SALE_COST,
    ---Primary Keys
    oh.header_id ,
    party.party_id ,
    -- Rct.trx_number,
    ship_from_org.organization_id
    --    msi.inventory_item_id ,
    --    msi.organization_id msi_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org,
    hz_parties party,
    hz_cust_accounts cust_acct,
    oe_order_headers oh,
    oe_order_lines ol,
    Hr_ALL_ORGANIZATION_UNITS Hou,
    Ra_Salesreps Rep,
    oe_transaction_types_vl oth,
    Oe_Transaction_Types_Vl otl,
    mtl_system_items_kfv msi,
    fnd_lookup_values_vl flv,
    per_people_f ppf,
    fnd_user fu           
  WHERE Ol.Sold_To_Org_Id      = Cust_Acct.Cust_Account_Id(+)
  AND cust_acct.party_id       = party.party_id(+)
  AND oh.Ship_From_Org_Id      = Ship_From_Org.Organization_Id(+)
  AND OL.HEADER_ID             = OH.HEADER_ID
  AND MSI.ORGANIZATION_ID      = HOU.ORGANIZATION_ID
  AND oh.salesrep_id           = rep.salesrep_id(+)
  AND Msi.Inventory_Item_Id(+) = Ol.Inventory_Item_Id
  AND Msi.Organization_Id (+)  = Ol.Ship_From_Org_Id
  AND OH.ORDER_TYPE_ID         = OTH.TRANSACTION_TYPE_ID
  AND oh.org_id                = oth.org_id
  AND OL.LINE_TYPE_ID          =OTL.TRANSACTION_TYPE_ID
  AND ol.org_id                = otl.org_id
  AND flv.lookup_type(+)       ='FLOW_STATUS'
  AND flv.lookup_code(+)       = oh.flow_status_code
  AND TRUNC(OH.ORDERED_DATE)   = TRUNC(SYSDATE)
    --  and oh.order_number=10003570
  AND ( ( OTH.NAME         ='COUNTER ORDER'
  AND OL.FLOW_STATUS_CODE IN ('FULFILLED','CLOSED' ))
  OR ( OTH.NAME           <>'COUNTER ORDER'
    --AND XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LINE_SHIP_CONFIRM_FLAG(OL.HEADER_ID,OL.LINE_ID)                       ='Y'
    --AND DECODE(OTL.ORDER_CATEGORY_CODE,'RETURN',XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LINE_RMA_FLAG(OL.LINE_ID),'Y')='Y'
  AND OL.FLOW_STATUS_CODE IN ('PRE-BILLING_ACCEPTANCE','CLOSED' ) )
  or (ol.flow_status_code in ('INVOICE_HOLD','CLOSED' )))
  and fu.user_id          =ol.created_by  
  AND FU.EMPLOYEE_ID      =PPF.PERSON_ID(+)  
  and trunc (ol.creation_date) between nvl (ppf.effective_start_date, trunc (ol.creation_date) ) and nvl (ppf.effective_end_date, trunc (ol.creation_date) ) 
  GROUP BY OH.ORDER_NUMBER,
    trunc(oh.ordered_date),
    PPF.FULL_NAME,
    OTH.NAME,
    NVL(FLV.MEANING, INITCAP(REPLACE(OH.FLOW_STATUS_CODE,'_',' '))),
    CUST_ACCT.ACCOUNT_NUMBER,
    NVL(CUST_ACCT.ACCOUNT_NAME,PARTY.PARTY_NAME),
    rep.name,
    REP.SALESREP_NUMBER,
    SHIP_FROM_ORG.ORGANIZATION_CODE,
    OH.HEADER_ID,
    PARTY.PARTY_ID,
    ship_from_org.organization_id
/
set scan on define on
prompt Creating View Data for Invoice Pre-Register report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_INV_PRE_REG_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Inv Pre Reg V','EXOIPRV');
--Delete View Columns for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_INV_PRE_REG_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','FREIGHT_AMT',660,'Freight Amt','FREIGHT_AMT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Freight Amt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','DISCOUNT_AMT',660,'Discount Amt','DISCOUNT_AMT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Amt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','TAX_VALUE',660,'Tax Value','TAX_VALUE','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Tax Value');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALESREP_NUMBER',660,'Salesrep Number','SALESREP_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','GROSS_AMOUNT',660,'Gross Amount','GROSS_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Gross Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_STATUS',660,'Order Status','ORDER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Warehouse');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Average Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PROFIT',660,'Profit','PROFIT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Profit');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','INV_AMOUNT',660,'Inv Amount','INV_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Inv Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALES',660,'Sales','SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','SALE_COST',660,'Sale Cost','SALE_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_INV_PRE_REG_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By');
--Inserting View Components for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','SHIP_FROM_ORG','SHIP_FROM_ORG','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','HZ_PARTIES',660,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','');
--Inserting View Component Joins for EIS_XXWC_OM_INV_PRE_REG_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_PARAMETERS','SHIP_FROM_ORG',660,'EXOIPRV.ORGANIZATION_ID','=','SHIP_FROM_ORG.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','HZ_PARTIES','PARTY',660,'EXOIPRV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_HEADERS','OH',660,'EXOIPRV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','OE_ORDER_LINES','OL',660,'EXOIPRV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIPRV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_INV_PRE_REG_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIPRV.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Invoice Pre-Register report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Invoice Pre-Register report
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Invoice Pre-Register report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Invoice Pre-Register report
xxeis.eis_rs_utility.delete_report_rows( 'Invoice Pre-Register report' );
--Inserting Report - Invoice Pre-Register report
xxeis.eis_rs_ins.r( 660,'Invoice Pre-Register report','','Real-time branch sales; current days sales  at a detail and summary level.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_INV_PRE_REG_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Invoice Pre-Register report
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','7','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'DISCOUNT_AMT','Discount Amt','Discount Amt','','','','','9','N','','DATA_FIELD','','SUM','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'FREIGHT_AMT','Freight Amt','Freight Amt','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'GROSS_AMOUNT','Gross Amount','Gross Amount','','','','','8','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','1','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_STATUS','Order Status','Order Status','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'SALESREP_NUMBER','Salesrep Number','Salesrep Number','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'TAX_VALUE','Sales Tax','Tax Value','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'AVERAGE_COST','Average Cost','Average Cost','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'PROFIT','Profit','Profit','','','','','14','N','','DATA_FIELD','','SUM','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'ORDER_TYPE','Order Type','Order Type','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'INV_AMOUNT','Inv Amount','Inv Amount','','','','','12','N','','DATA_FIELD','','SUM','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'SALESREP_NAME','Salesrep Name','Salesrep Name','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'GM_PER','GM%','Salesrep Name','NUMBER','','','','15','Y','','','','','','','case when (NVL(EXOIPRV.sales,0) !=0  AND NVL(EXOIPRV.sale_cost,0) !=0) then (((EXOIPRV.sales-EXOIPRV.sale_cost)/(EXOIPRV.sales))*100) WHEN NVL(EXOIPRV.sale_cost,0) =0  THEN 100 WHEN nvl(EXOIPRV.sales,0) != 0 THEN (((EXOIPRV.sales-EXOIPRV.sale_cost)/(EXOIPRV.sales))*100) else 0 end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
xxeis.eis_rs_ins.rc( 'Invoice Pre-Register report',660,'CREATED_BY','Created By','Created By','','','','','5','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_INV_PRE_REG_V','','');
--Inserting Report Parameters - Invoice Pre-Register report
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM WAREHOUSE','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Invoice Pre-Register report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','Y','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
--Inserting Report Conditions - Invoice Pre-Register report
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'WAREHOUSE','IN',':Warehouse','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'ORDERED_DATE','>=',':Ordered Date From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Invoice Pre-Register report',660,'ORDERED_DATE','<=',':Ordered Date To','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Invoice Pre-Register report
xxeis.eis_rs_ins.rs( 'Invoice Pre-Register report',660,'CREATED_BY','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Invoice Pre-Register report',660,'ORDER_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Invoice Pre-Register report
--Inserting Report Templates - Invoice Pre-Register report
--Inserting Report Portals - Invoice Pre-Register report
--Inserting Report Dashboards - Invoice Pre-Register report
--Inserting Report Security - Invoice Pre-Register report
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50926',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50927',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50928',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50929',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50931',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50930',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Invoice Pre-Register report','','SO004816','',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Orders on Hold Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_IGVP_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_IGVP_V
Create or replace View XXEIS.EIS_XXWC_OM_IGVP_V
(CUSTOMER_NUMBER,CUSTOMER_NAME,CREATED_BY,INVENTORY_ORGANIZATION,SALES_ORDER_NUMBER,SALES_ORDER_STATUS,ORDER_TYPE,SALES_ORDER_LINE_STATUS,SALES_ORDER_LINE_NUMBER,ORDER_HEADER_HOLD_NAME,ORDER_LINE_HOLD_NAME,EXTENDED_COST,PART_NUMBER,PART_DESCRIPTION,QUANTITY,UNIT_SELLING_PRICE,EXTENDED_PRICE,MARGIN_PER,UNIT_COST,GROSS_PERCENT_DOLLARS,GROSS_PERCENTAGE,ORDERED_DATE,ONHAND_LOCATION,AVERAGE_COST,MARGIN,UNIT_LIST_PRICE,HOLD_DATE,HEADER_ID,LINE_ID,PARTY_ID,INVENTORY_ITEM_ID,ORGANIZATION_ID) AS 
SELECT Hca.Account_Number Customer_Number,
    NVL( HCA.ACCOUNT_NAME,HZP.PARTY_NAME) CUSTOMER_NAME,
    PPF.FULL_NAME CREATED_BY,
    Mtp.Organization_Code Inventory_Organization,
    oh.order_number sales_order_number,
    NVL(flv_header_status.meaning, initcap(REPLACE(OH.FLOW_STATUS_CODE,'_',' '))) Sales_Order_Status,
    Ott.Name Order_Type,
    NVL(flv_line_status.meaning, initcap(REPLACE(xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_status(ol.line_id),'_',' '))) Sales_Order_Line_Status,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER
    ||'.'
    ||OL.OPTION_NUMBER)) Sales_Order_Line_Number,
    Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Header_Hold(Ol.Header_Id,Ol.Line_Id) Order_Header_Hold_Name,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LINE_HOLD(OL.HEADER_ID,OL.LINE_ID) ORDER_LINE_HOLD_NAME,
    nvl((NVL(ol.unit_cost,0)*OL.ORDERED_QUANTITY),0) EXTENDED_COST,
    MSI.SEGMENT1 PART_NUMBER ,
    NVL(Msi.Description,ol.user_item_description) Part_Description,
    ol.ordered_quantity quantity,
    NVL(OL.UNIT_SELLING_PRICE,0) UNIT_SELLING_PRICE ,
    nvl((NVL(OL.UNIT_SELLING_PRICE,0) * OL.ORDERED_QUANTITY),0) EXTENDED_PRICE,
    --OSOL.MARGIN_PERCENT           MARGIN_PER,
    (
    CASE
      WHEN NVL(OL.UNIT_COST,0) = 0
      THEN 100
      WHEN OL.UNIT_SELLING_PRICE =0
      THEN 0
      WHEN ol.ordered_quantity =0
      THEN 0
      ELSE ((ol.ordered_quantity*ol.unit_selling_price -ol.ordered_quantity*NVL(ol.Unit_Cost,0))/(ol.ordered_quantity*ol.unit_selling_price)*100)
    END) MARGIN_PER,
    NVL(OL.UNIT_COST,0) UNIT_COST,
    Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Sales_Profit(Ol.Header_Id) Gross_Percent_Dollars,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_SALES_PROFIT_PERCENTAGE(OL.HEADER_ID) GROSS_PERCENTAGE,
    /*SUM((
    CASE
    WHEN NVL(Ol.Unit_Cost,0) = 0
    THEN 100
    ELSE OSOL.MARGIN_PERCENT
    END)) GROSS_MARGIN_PERCENTAGE,*/
    --TRUNC(OH.ORDERED_DATE) ORDERED_DATE,
    trunc(oh.ordered_date) ordered_date,
    mtp.organization_code onhand_location,
    NVL(Apps.Cst_Cost_Api.Get_Item_Cost(1,Msi.Inventory_Item_Id,Msi.Organization_Id),0) AVERAGE_COST,
    (NVL(Ol.Unit_Selling_Price,0) - NVL(Ol.Unit_Cost,0)) Margin,
    NVL(ol.unit_list_price,0) unit_list_price,
    NVL(xxeis.eis_rs_xxwc_com_util_pkg.Get_header_hold_date(OH.HEADER_ID,ol.line_id),xxeis.eis_rs_xxwc_com_util_pkg.Get_line_hold_date(OH.HEADER_ID,ol.line_id)) hold_date,
    ---Primary Keys
    OH.HEADER_ID ,
    ol.line_id ,
    hzp.party_id ,
    msi.inventory_item_id ,
    msi.organization_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    Oe_Order_Lines Ol,
    Hz_Cust_Accounts Hca,
    Hz_Parties Hzp,
    Mtl_Parameters Mtp,
    Hr_All_Organization_Units Hrou,
    Mtl_System_Items_Kfv Msi,
    OE_TRANSACTION_TYPES_VL OTT,
    --   OE_SCH_ORDER_LINES_V OSOL,
    fnd_lookup_values_vl flv_header_status,
    FND_LOOKUP_VALUES_VL FLV_LINE_STATUS,
    PER_PEOPLE_F PPF,
    FND_USER FU
  WHERE ol.header_id    = oh.header_id
  AND oh.sold_to_org_id = hca.cust_account_id(+)
  AND HCA.PARTY_ID      = HZP.PARTY_ID(+)
    --AND OL.SHIP_FROM_ORG_ID                 = MTP.ORGANIZATION_ID(+)
  AND OL.SHIP_FROM_ORG_ID   = MSI.ORGANIZATION_ID
  AND MSI.ORGANIZATION_ID   =Mtp.organization_id
  AND Hrou.Organization_Id  = Msi.Organization_Id
  AND msi.organization_id   = ol.ship_from_org_id
  AND msi.inventory_item_id = ol.inventory_item_id
  AND OH.ORDER_TYPE_ID      = OTT.TRANSACTION_TYPE_ID
  AND FU.USER_ID            =OH.CREATED_BY
  and FU.EMPLOYEE_ID        =PPF.PERSON_ID(+)
  AND OL.FLOW_STATUS_CODE  <> 'CLOSED'
    -- AND ol.line_id                          = osol.line_id
  AND FLV_HEADER_STATUS.LOOKUP_TYPE(+)    ='FLOW_STATUS'
  AND FLV_HEADER_STATUS.LOOKUP_CODE(+)    = OH.FLOW_STATUS_CODE
  AND flv_line_status.lookup_type(+)      ='LINE_FLOW_STATUS'
  AND FLV_LINE_STATUS.LOOKUP_CODE(+)      = OL.FLOW_STATUS_CODE
  AND ( ol.invoice_interface_status_code IS NULL
  OR EXISTS
    (SELECT 1
    FROM oe_order_holds hold
    WHERE hold.header_id      =oh.header_id
    AND hold.hold_release_id IS NULL
    ) )
    --and oh.order_number='10000037'
  AND EXISTS
    (SELECT 1
    FROM Xxeis.Eis_Org_Access_V
    WHERE Organization_Id = Msi.Organization_Id
    )
/
set scan on define on
prompt Creating View Data for Orders on Hold Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_IGVP_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Igvp V','EXOIV');
--Delete View Columns for EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_IGVP_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORDER_LINE_HOLD_NAME',660,'Order Line Hold Name','ORDER_LINE_HOLD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Line Hold Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORDER_HEADER_HOLD_NAME',660,'Order Header Hold Name','ORDER_HEADER_HOLD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Header Hold Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','SALES_ORDER_LINE_NUMBER',660,'Sales Order Line Number','SALES_ORDER_LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order Line Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','SALES_ORDER_LINE_STATUS',660,'Sales Order Line Status','SALES_ORDER_LINE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order Line Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','SALES_ORDER_STATUS',660,'Sales Order Status','SALES_ORDER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','SALES_ORDER_NUMBER',660,'Sales Order Number','SALES_ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','INVENTORY_ORGANIZATION',660,'Inventory Organization','INVENTORY_ORGANIZATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Inventory Organization');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit Selling Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','QUANTITY',660,'Quantity','QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Quantity');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','PART_DESCRIPTION',660,'Part Description','PART_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','EXTENDED_PRICE',660,'Extended Price','EXTENDED_PRICE','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Extended Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MARGIN_PER',660,'Margin Per','MARGIN_PER','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Margin Per');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','UNIT_COST',660,'Unit Cost','UNIT_COST','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','GROSS_PERCENTAGE',660,'Gross Percentage','GROSS_PERCENTAGE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Gross Percentage');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','GROSS_PERCENT_DOLLARS',660,'Gross Percent Dollars','GROSS_PERCENT_DOLLARS','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Gross Percent Dollars');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Average Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ONHAND_LOCATION',660,'Onhand Location','ONHAND_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Onhand Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','MARGIN',660,'Margin','MARGIN','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Margin');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','UNIT_LIST_PRICE',660,'Unit List Price','UNIT_LIST_PRICE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit List Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','EXTENDED_COST',660,'Extended Cost','EXTENDED_COST','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Extended Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_IGVP_V','HOLD_DATE',660,'Hold Date','HOLD_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Hold Date');
--Inserting View Components for EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_IGVP_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','');
--Inserting View Component Joins for EIS_XXWC_OM_IGVP_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','OE_ORDER_HEADERS','OH',660,'EXOIV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','OE_ORDER_LINES','OL',660,'EXOIV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','HZ_PARTIES','HZP',660,'EXOIV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','MTL_PARAMETERS','MTP',660,'EXOIV.ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_IGVP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOIV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Orders on Hold Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Orders on Hold Report
xxeis.eis_rs_ins.lov( 660,'SELECT  NAME HOLD_NAME,DESCRIPTION HOLD_DESCRIPTION,HOLD_ID   FROM  OE_HOLD_DEFINITIONS','','OM HOLD NAME','This gives the hold name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Orders on Hold Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Orders on Hold Report
xxeis.eis_rs_utility.delete_report_rows( 'Orders on Hold Report' );
--Inserting Report - Orders on Hold Report
xxeis.eis_rs_ins.r( 660,'Orders on Hold Report','','Order Holds and Not Invoiced Report','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_IGVP_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Orders on Hold Report
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','2','N','','PAGE_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','1','N','','PAGE_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'INVENTORY_ORGANIZATION','Loc','Inventory Organization','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_HEADER_HOLD_NAME','Order Header Hold Name','Order Header Hold Name','','','','','12','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_LINE_HOLD_NAME','Order Line Hold Name','Order Line Hold Name','','','','','14','N','','ROW_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'PART_DESCRIPTION','Part Description','Part Description','','','','','17','N','','PAGE_FIELD','','','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'PART_NUMBER','Part Number','Part Number','','','','','16','N','','PAGE_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'QUANTITY','Quantity','Quantity','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'SALES_ORDER_LINE_NUMBER','Order Line Number','Sales Order Line Number','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'SALES_ORDER_LINE_STATUS','Order Line Status','Sales Order Line Status','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'SALES_ORDER_NUMBER','Sales Order Number','Sales Order Number','','','','','4','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'SALES_ORDER_STATUS','Order Header Status','Sales Order Status','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'EXTENDED_PRICE','Extended Price','Extended Price','','','','','20','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'MARGIN_PER','Margin%','Margin Per','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'UNIT_COST','Unit Cost','Unit Cost','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_HEADER_HOLD','Order Header Hold','Unit Cost','VARCHAR2','','','','11','Y','','','','','','','CASE WHEN EXOIV.ORDER_HEADER_HOLD_NAME IS NULL THEN ''No'' ELSE ''Yes'' END','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_LINE_HOLD','Order Line Hold','Unit Cost','VARCHAR2','','','','13','Y','','','','','','','CASE WHEN  EXOIV.ORDER_LINE_HOLD_NAME IS NULL THEN ''No'' ELSE ''Yes'' END','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDER_TYPE','Order Type','Order Type','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'CREATED_BY','Created By','Created By','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'EXTENDED_COST','Extended Cost','Extended Cost','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'HOLD_DATE','Hold Date','Hold Date','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report',660,'GM$','GM$','Ordered Date','NUMBER','~~2','','0','24','Y','','','','','','','(NVL(EXOIV.EXTENDED_PRICE,0)-NVL(EXOIV.EXTENDED_COST,0))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_IGVP_V','','');
--Inserting Report Parameters - Orders on Hold Report
xxeis.eis_rs_ins.rp( 'Orders on Hold Report',660,'Warehouse','Warehouse','INVENTORY_ORGANIZATION','IN','OM WAREHOUSE','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report',660,'Hold Name','Hold Name','ORDER_HEADER_HOLD_NAME','IN','OM HOLD NAME','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report',660,'Ordered Date From','Ordered Date To','ORDERED_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
--Inserting Report Conditions - Orders on Hold Report
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'INVENTORY_ORGANIZATION','IN',':Warehouse','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'ORDER_HEADER_HOLD_NAME','IN',':Hold Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'ORDERED_DATE','>=',':Ordered Date From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'ORDERED_DATE','<=',':Ordered Date To','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report',660,'','','','','AND (EXOIV.ORDER_HEADER_HOLD_NAME IS NOT NULL OR EXOIV.ORDER_LINE_HOLD_NAME IS NOT NULL)','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Orders on Hold Report
xxeis.eis_rs_ins.rs( 'Orders on Hold Report',660,'SALES_ORDER_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Orders on Hold Report
--Inserting Report Templates - Orders on Hold Report
--Inserting Report Portals - Orders on Hold Report
--Inserting Report Dashboards - Orders on Hold Report
--Inserting Report Security - Orders on Hold Report
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50926',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50927',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50928',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50929',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50931',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50930',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','701','','50546',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report','','SO004816','',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Open Sales Orders Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_OPEN_ORDERS_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_OPEN_ORDERS_V
Create or replace View XXEIS.EIS_XXWC_OM_OPEN_ORDERS_V
(WAREHOUSE,SALES_PERSON_NAME,CREATED_BY,ORDER_NUMBER,ORDERED_DATE,LINE_CREATION_DATE,ORDER_TYPE,QUOTE_NUMBER,ORDER_LINE_STATUS,ORDER_HEADER_STATUS,CUSTOMER_NUMBER,CUSTOMER_NAME,CUSTOMER_JOB_NAME,ORDER_AMOUNT,SHIPPING_METHOD,SHIP_TO_CITY,ZIP_CODE,SCHEDULE_SHIP_DATE,HEADER_STATUS,PAYMENT_TERMS,ORDER_HEADER_ID,ORDER_LINE_ID,CUST_ACCOUNT_ID,CUST_ACCT_SITE_ID,SALESREP_ID,PARTY_ID,TRANSACTION_TYPE_ID,MTP_ORGANIZATION_ID,HZCS_SHIP_TO_STE_ID,HZPS_SHP_TO_PRT_ID) AS 
SELECT mtp.organization_code warehouse,
    rep.name sales_person_name,
    ppf.full_name created_by,
    OH.ORDER_NUMBER ORDER_NUMBER,
    /* (OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER) LINE_NUMBER,*/
    TRUNC(OH.ORDERED_DATE) ORDERED_DATE,
    TRUNC(ol.creation_date) line_creation_date,
    OTT.NAME ORDER_TYPE,
    OH.QUOTE_NUMBER,
    xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_status(ol.line_id) order_line_status,
    flv_order_status.meaning order_header_status,
    --OL.FLOW_STATUS_CODE order_line_status,
    HCA.ACCOUNT_NUMBER CUSTOMER_NUMBER,
    NVL(hca.account_name,hzp.party_name) customer_name,
    hzcs_ship_to.location customer_job_name,
    ROUND((OL.UNIT_SELLING_PRICE * OL.ORDERED_QUANTITY),2) ORDER_AMOUNT,
    flv_ship_mtd.meaning shipping_method,
    hzl_ship_to.city ship_to_city,
    hzl_ship_to.postal_code zip_code,
    OL.SCHEDULE_SHIP_DATE SCHEDULE_SHIP_DATE,
    oh.flow_status_code header_status,
    rt.name Payment_terms,
    --Primary Keys Added
    Oh.Header_Id Order_Header_Id,
    Ol.Line_Id Order_Line_Id,
    hca.cust_account_id cust_account_id,
    Hcas_Ship_To.Cust_Acct_Site_Id Cust_Acct_Site_Id,
    Rep.Salesrep_Id Salesrep_Id,
    Hzp.Party_Id Party_Id,
    ott.transaction_type_id transaction_type_id,
    Mtp.Organization_Id Mtp_Organization_Id,
    Hzcs_Ship_To.Site_Use_Id Hzcs_Ship_To_Ste_Id,
    hzps_ship_to.party_site_id hzps_shp_to_prt_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    hz_cust_accounts hca,
    hz_parties hzp,
    oe_transaction_types_vl ott,
    ra_salesreps rep,
    mtl_parameters mtp,
    hz_cust_site_uses hzcs_ship_to,
    hz_cust_acct_sites hcas_ship_to,
    HZ_PARTY_SITES HZPS_SHIP_TO,
    hz_locations hzl_ship_to,
    hr_all_organization_units haou,
    PER_PEOPLE_F PPF,
    FND_USER FU,
    FND_LOOKUP_VALUES_VL FLV_SHIP_MTD,
    oe_lookups flv_order_status,
    ra_terms_vl rt
  WHERE ol.header_id                 = oh.header_id
  AND oh.sold_to_org_id              = hca.cust_account_id(+)
  AND hca.party_id                   = hzp.party_id(+)
  AND oh.order_type_id               = ott.transaction_type_id
  AND ol.ship_from_org_id            = mtp.organization_id(+)
  AND oh.ship_to_org_id              = hzcs_ship_to.site_use_id(+)
  AND hzcs_ship_to.cust_acct_site_id = hcas_ship_to.cust_acct_site_id(+)
  AND hcas_ship_to.party_site_id     = hzps_ship_to.party_site_id(+)
  AND hzl_ship_to.location_id(+)     = hzps_ship_to.location_id
  AND OH.SALESREP_ID                 = REP.SALESREP_ID(+)
  AND OL.FLOW_STATUS_CODE NOT       IN ('CLOSED','CANCELLED')
  AND MTP.ORGANIZATION_ID            = HAOU.ORGANIZATION_ID
    --AND OH.CREATED_BY                  = PPF.PERSON_ID
  AND FU.USER_ID                   =OH.CREATED_BY
  AND FU.EMPLOYEE_ID               =PPF.PERSON_ID(+)
  and FLV_SHIP_MTD.LOOKUP_TYPE(+)  ='SHIP_METHOD'
  and FLV_SHIP_MTD.LOOKUP_CODE(+)  = OL.SHIPPING_METHOD_CODE
  AND flv_order_status.lookup_type ='FLOW_STATUS'
  AND flv_order_status.lookup_code = Oh.flow_status_code
  AND oh.payment_term_id(+)        = rt.term_id
  and ott.name                    in('STANDARD ORDER','COUNTER ORDER')
  and ((ol.FLOW_STATUS_CODE = 'ENTERED'  and trunc (oh.creation_date) < trunc(sysdate)) or ol.FLOW_STATUS_CODE <> 'ENTERED' )
  and trunc (oh.creation_date) between nvl (ppf.effective_start_date, trunc (oh.creation_date) ) and nvl (ppf.effective_end_date, trunc (oh.creation_date) )
  and OH.TRANSACTION_PHASE_CODE <>'N'
/
set scan on define on
prompt Creating View Data for Open Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_OPEN_ORDERS_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Open Orders V','EXOOOV');
--Delete View Columns for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_OPEN_ORDERS_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_LINE_STATUS',660,'Order Line Status','ORDER_LINE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Line Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SCHEDULE_SHIP_DATE',660,'Schedule Ship Date','SCHEDULE_SHIP_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Schedule Ship Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Person Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ZIP_CODE',660,'Zip Code','ZIP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Zip Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SHIP_TO_CITY',660,'Ship To City','SHIP_TO_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To City');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Job Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Warehouse');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SHIPPING_METHOD',660,'Shipping Method','SHIPPING_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Shipping Method');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZPS_SHP_TO_PRT_ID',660,'Hzps Shp To Prt Id','HZPS_SHP_TO_PRT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Hzps Shp To Prt Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZCS_SHIP_TO_STE_ID',660,'Hzcs Ship To Ste Id','HZCS_SHIP_TO_STE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Hzcs Ship To Ste Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTP_ORGANIZATION_ID',660,'Mtp Organization Id','MTP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mtp Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','TRANSACTION_TYPE_ID',660,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Transaction Type Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','SALESREP_ID',660,'Salesrep Id','SALESREP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Salesrep Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUST_ACCT_SITE_ID',660,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Acct Site Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_LINE_ID',660,'Order Line Id','ORDER_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Line Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_HEADER_ID',660,'Order Header Id','ORDER_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','LINE_CREATION_DATE',660,'Line Creation Date','LINE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Line Creation Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','HEADER_STATUS',660,'Header Status','HEADER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Header Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','QUOTE_NUMBER',660,'Quote Number','QUOTE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Quote Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Header Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OPEN_ORDERS_V','PAYMENT_TERMS',660,'Payment Terms','PAYMENT_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Terms');
--Inserting View Components for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES',660,'HZ_CUST_ACCT_SITES_ALL','HCAS_SHIP_TO','HCAS_SHIP_TO','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores All Customer Account Sites Across All Opera','','');
--Inserting View Component Joins for EIS_XXWC_OM_OPEN_ORDERS_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCOUNTS','HCA',660,'EXOOOV.CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_PARTIES','HZP',660,'EXOOOV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','MTL_PARAMETERS','MTP',660,'EXOOOV.MTP_ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_OPEN_ORDERS_V','HZ_CUST_ACCT_SITES','HCAS_SHIP_TO',660,'EXOOOV.CUST_ACCT_SITE_ID','=','HCAS_SHIP_TO.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Open Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Open Sales Orders Report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS, HR_OPERATING_UNITS OU
WHERE RS.org_id = OU.organization_id
AND RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_vl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select meaning ship_method,description from FND_LOOKUP_VALUES_vl where lookup_type=''SHIP_METHOD''','','OM SHIP METHOD','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ppf.full_name  created_by, fu.user_name user_name
  FROM FND_USER fu, per_people_f ppf
 WHERE TRUNC(SYSDATE) BETWEEN fu.START_DATE AND NVL(fu.end_date, hr_general.end_of_time)
   and TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
   and fu.employee_id = ppf.person_id (+)
   and exists
          (select 1
             from fnd_user_resp_groups furg,
                  fnd_responsibility_vl fr
            where furg.responsibility_id   = fr.responsibility_id
              and fu.user_id = furg.user_id
              and fr.responsibility_name like ''XXEIS%'')','','Created By Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct meaning from oe_lookups oe, oe_order_headers oh where lookup_type like ''FLOW_STATUS''  and oh.flow_status_code = oe.lookup_code','','WC OM Order Header Status','Displays distinct header status','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct meaning from oe_lookups oe, oe_order_lines oh where lookup_type like ''LINE_FLOW_STATUS''  and oh.flow_status_code = oe.lookup_code','','WC OM Order Line Status','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'Select  name, description from ra_terms_vl','','WC OM Payment Terms','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT LOCATION FROM hz_cust_site_uses','','OM Customer Job Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Open Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Open Sales Orders Report
xxeis.eis_rs_utility.delete_report_rows( 'Open Sales Orders Report' );
--Inserting Report - Open Sales Orders Report
xxeis.eis_rs_ins.r( 660,'Open Sales Orders Report','','Open orders report by customer, by job, by salesperson, by created by, by shipping method, by promise date.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_OPEN_ORDERS_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Open Sales Orders Report
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CREATED_BY','Created By','Created By','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_AMOUNT','Order Amount','Order Amount','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_LINE_STATUS','Order Line Status','Order Line Status','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_TYPE','Order Type','Order Type','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SCHEDULE_SHIP_DATE','Schedule Ship Date','Schedule Ship Date','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SHIPPING_METHOD','Shipping Method','Shipping Method','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ZIP_CODE','Zip Code','Zip Code','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'WAREHOUSE','Warehouse','Warehouse','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SHIP_TO_CITY','Ship To City','Ship To City','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_JOB_NAME','Customer Job Name','Customer Job Name','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'QUOTE_NUMBER','Quote Number','Quote Number','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'ORDER_HEADER_STATUS','Order Header Status','Order Header Status','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
xxeis.eis_rs_ins.rc( 'Open Sales Orders Report',660,'PAYMENT_TERMS','Payment Terms','Payment Terms','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OPEN_ORDERS_V','','');
--Inserting Report Parameters - Open Sales Orders Report
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Created By','Created By','CREATED_BY','IN','Created By Lov','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Job Name','Job Name','CUSTOMER_JOB_NAME','IN','OM Customer Job Name LOV','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','N','Y','11','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Order Type','Order Type','ORDER_TYPE','IN','OM ORDER TYPE','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Sales Person','Sales Person','SALES_PERSON_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Schedule Ship Date From','Schedule Ship Date From','SCHEDULE_SHIP_DATE','>=','','','DATE','N','Y','9','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Shipping Method','Shipping Method','SHIPPING_METHOD','IN','OM SHIP METHOD','','VARCHAR2','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM WAREHOUSE','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','12','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Schedule Ship Date To','Schedule Ship Date To','SCHEDULE_SHIP_DATE','<=','','','DATE','N','Y','10','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Order Line Status','Order Line Status','ORDER_LINE_STATUS','IN','WC OM Order Line Status','','VARCHAR2','N','Y','13','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Order Header Status','Order Header Status','ORDER_HEADER_STATUS','IN','WC OM Order Header Status','','VARCHAR2','N','Y','14','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Sales Orders Report',660,'Payment Terms','Payment Terms','PAYMENT_TERMS','IN','WC OM Payment Terms','','VARCHAR2','N','Y','15','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Open Sales Orders Report
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'CREATED_BY','IN',':Created By','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'CUSTOMER_NAME','IN',':Customer Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'ORDER_TYPE','IN',':Order Type','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'SHIPPING_METHOD','IN',':Shipping Method','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'WAREHOUSE','IN',':Warehouse','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'TRUNC(ORDERED_DATE)','>=',':Ordered Date From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'TRUNC(SCHEDULE_SHIP_DATE)','>=',':Schedule Ship Date From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'TRUNC(ORDERED_DATE)','<=',':Ordered Date To','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'TRUNC(SCHEDULE_SHIP_DATE)','<=',':Schedule Ship Date To','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'CUSTOMER_JOB_NAME','IN',':Job Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'SALES_PERSON_NAME','IN',':Sales Person','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'ORDER_LINE_STATUS','IN',':Order Line Status','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'ORDER_HEADER_STATUS','IN',':Order Header Status','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Open Sales Orders Report',660,'PAYMENT_TERMS','IN',':Payment Terms','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Open Sales Orders Report
xxeis.eis_rs_ins.rs( 'Open Sales Orders Report',660,'WAREHOUSE','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Open Sales Orders Report',660,'ORDER_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Open Sales Orders Report
--Inserting Report Templates - Open Sales Orders Report
--Inserting Report Portals - Open Sales Orders Report
--Inserting Report Dashboards - Open Sales Orders Report
--Inserting Report Security - Open Sales Orders Report
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50926',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50927',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50928',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50929',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50931',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50930',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','701','','50546',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Open Sales Orders Report','','SO004816','',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Simpson Profile Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_SIMP_PROF_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_SIMP_PROF_V
Create or replace View XXEIS.EIS_XXWC_OM_SIMP_PROF_V
(LOCATION,BRANCH_NAME,MASTER_ACCOUNT_NUMBER,MASTER_ACCOUNT_NAME,CAT_CLASS_NUMBER,CAT_CLASS_NAME,SALESREP_NUMBER,SALESREP_NAME,SHIP_TO_SITE_NUMBER,SHIP_TO_LOCATION,SITE_SALESREP_NUMBER,SITE_SALESREP_NAME,SALES,EXTENDED_BRANCH_COST,ORDERED_DATE,ORDER_NUMBER,LINE_NUMBER,ITEM_NUMBER,HEADER_ID,LINE_ID,CUST_ACCOUNT_ID,PARTY_ID,SALESREP_ID,ORGANIZATION_ID,INVENTORY_ITEM_ID,MSI_ORGANIZATION_ID) AS 
SELECT OOD.ORGANIZATION_CODE LOCATION,
    ood.organization_name branch_name,
    hca_bill.account_number master_account_number,
    hca_bill.account_name master_account_name,
    mcv.segment2 cat_class_number,
    Xxeis.eis_rs_xxwc_com_util_pkg.Get_inv_cat_class_desc(msi.inventory_item_id,msi.organization_id) cat_class_name,
    rep.salesrep_number salesrep_number,
    rep.NAME salesrep_name,
    hzps_ship_to.party_site_number ship_to_site_number,
    hzcs_ship_to.location ship_to_location,
    rep_site.salesrep_number site_salesrep_number,
    rep_site.NAME site_salesrep_name,
    (ol.ordered_quantity  *NVL(ol.unit_selling_price,0)) sales ,
    ((OL.ORDERED_QUANTITY)*NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0)) EXTENDED_BRANCH_COST,
    TRUNC(OH.ORDERED_DATE) ORDERED_DATE,
    OH.ORDER_NUMBER,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER
    ||'.'
    ||OL.OPTION_NUMBER)) LINE_NUMBER,
    msi.segment1 item_number,
    ---Primary Keys
    oh.header_id,
    ol.line_id,
    hca_bill.cust_account_id,
    hzp_bill.party_id,
    rep.salesrep_id,
    ood.organization_id,
    msi.inventory_item_id,
    msi.organization_id msi_organization_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    hz_cust_accounts hca_bill,
    hz_parties hzp_bill,
    hz_cust_site_uses hzcs_bill_to,
    hz_cust_site_uses hzcs_ship_to,
    hz_cust_acct_sites hcas_ship_to,
    hz_party_sites hzps_ship_to,
    hz_locations hzl_ship_to,
    ra_salesreps rep,
    org_organization_definitions ood,
    mtl_system_items_kfv msi,
    mtl_categories_kfv mcv,
    ra_salesreps rep_site ,
    mtl_category_sets mcs,
    mtl_item_categories mic,
    fnd_id_flex_segments ffs ,
    fnd_flex_values_vl flex_val
  WHERE oh.header_id                   = ol.header_id
  AND oh.sold_to_org_id                = hca_bill.cust_account_id(+)
  AND hca_bill.party_id                = hzp_bill.party_id(+)
  AND oh.invoice_to_org_id             = hzcs_bill_to.site_use_id(+)
  AND Oh.Salesrep_Id                   = Rep.Salesrep_Id(+)
  AND Oh.Ship_To_Org_Id                = Hzcs_Ship_To.Site_Use_Id(+)
  AND Hzcs_Ship_To.Cust_Acct_Site_Id   = Hcas_Ship_To.Cust_Acct_Site_Id(+)
  AND Hcas_Ship_To.Party_Site_Id       = Hzps_Ship_To.Party_Site_Id(+)
  AND Hzl_Ship_To.Location_Id(+)       = Hzps_Ship_To.Location_Id
  AND Oh.Ship_From_Org_Id              = Ood.Organization_Id
  AND Ol.Inventory_Item_Id             = Msi.Inventory_Item_Id
  AND Ol.Ship_From_Org_Id              = Msi.Organization_Id
  AND Hzcs_Ship_To.Primary_Salesrep_Id = Rep_Site.Salesrep_Id(+)
  AND Mcs.Category_Set_Name            ='Inventory Category'
  AND Mcs.Structure_Id                 = Mcv.Structure_Id
  AND Mic.Inventory_Item_Id            = Msi.Inventory_Item_Id
  AND Mic.Organization_Id              = Msi.Organization_Id
  AND Mic.Category_Set_Id              = Mcs.Category_Set_Id
  AND Mic.Category_Id                  = Mcv.Category_Id
  AND flex_val.flex_value              = mcv.segment2
  AND ffs.flex_value_set_id            = flex_val.flex_value_set_id
  and ffs.id_flex_num                  =101
  AND exists (select 1
                from FND_FLEX_VALUE_SETS FS, FND_FLEX_VALUES_VL fv 
               where fs.flex_value_set_id = fv.flex_value_set_id 
                 and flex_value_set_name='XXEIS_SIMPSON_CUSTOMER'
                 AND fv.flex_value_meaning = hca_bill.account_number
             )   
  AND ffs.id_flex_code                 ='MCAT'
  AND ffs.application_column_name      ='SEGMENT2'
    -- and ffvs.flex_value_set_name         ='XXCUS CATEGORY CLASS'
  --AND upper(flex_val.description) LIKE 'SIMPSON%'
    --and Mcv.segment2 like '9000%'
    AND flex_val.flex_value between '9000' and '9030'
  and ol.flow_status_code='CLOSED'
/
set scan on define on
prompt Creating View Data for Simpson Profile Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_SIMP_PROF_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_SIMP_PROF_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Simp Prof V','EXOSPV');
--Delete View Columns for EIS_XXWC_OM_SIMP_PROF_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_SIMP_PROF_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_SIMP_PROF_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','EXTENDED_BRANCH_COST',660,'Extended Branch Cost','EXTENDED_BRANCH_COST','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Extended Branch Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','SALES',660,'Sales','SALES','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','SHIP_TO_LOCATION',660,'Ship To Location','SHIP_TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','SHIP_TO_SITE_NUMBER',660,'Ship To Site Number','SHIP_TO_SITE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Site Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','SALESREP_NUMBER',660,'Salesrep Number','SALESREP_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','MASTER_ACCOUNT_NAME',660,'Master Account Name','MASTER_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Account Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','MASTER_ACCOUNT_NUMBER',660,'Master Account Number','MASTER_ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Account Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','BRANCH_NAME',660,'Branch Name','BRANCH_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','LOCATION',660,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','CAT_CLASS_NAME',660,'Cat Class Name','CAT_CLASS_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','CAT_CLASS_NUMBER',660,'Cat Class Number','CAT_CLASS_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','SITE_SALESREP_NAME',660,'Site Salesrep Name','SITE_SALESREP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Salesrep Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','SITE_SALESREP_NUMBER',660,'Site Salesrep Number','SITE_SALESREP_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Salesrep Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msi Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_SIMP_PROF_V','SALESREP_ID',660,'Salesrep Id','SALESREP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Salesrep Id');
--Inserting View Components for EIS_XXWC_OM_SIMP_PROF_V
--Inserting View Component Joins for EIS_XXWC_OM_SIMP_PROF_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Simpson Profile Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Simpson Profile Report
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT MCV.SEGMENT2 cat_class,FLEX_VAL.DESCRIPTION cat_class_description 
from  Mtl_Categories_Kfv Mcv,
    MTL_CATEGORY_SETS MCS,
     FND_FLEX_VALUES_VL FLEX_VAL,
     fnd_flex_value_sets ffvs
    Where   Mcs.Category_Set_Name      =''Inventory Category''
  And Mcs.Structure_Id                 = Mcv.Structure_Id
  AND FLEX_VALUE                       = MCV.SEGMENT2
AND ffvs.flex_value_set_id=FLEX_VAL.flex_value_set_id
  AND FFVS.FLEX_VALUE_SET_NAME=''XXWC_CATEGORY_CLASS''
AND flex_val.flex_value BETWEEN ''9000'' AND ''9030''','','OM Simpson Inv Category','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT MCV.SEGMENT2 cat_class,FLEX_VAL.DESCRIPTION cat_class_description 
from  Mtl_Categories_Kfv Mcv,
    MTL_CATEGORY_SETS MCS,
     FND_FLEX_VALUES_VL FLEX_VAL,
     fnd_flex_value_sets ffvs
    Where   Mcs.Category_Set_Name      =''Inventory Category''
  And Mcs.Structure_Id                 = Mcv.Structure_Id
  AND FLEX_VALUE                       = MCV.SEGMENT2
AND ffvs.flex_value_set_id=FLEX_VAL.flex_value_set_id
  AND FFVS.FLEX_VALUE_SET_NAME=''XXWC_CATEGORY_CLASS''
AND flex_val.flex_value BETWEEN ''9000'' AND ''9030''','','OM Simpson Inv Category','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Simpson Profile Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Simpson Profile Report
xxeis.eis_rs_utility.delete_report_rows( 'Simpson Profile Report' );
--Inserting Report - Simpson Profile Report
xxeis.eis_rs_ins.r( 660,'Simpson Profile Report','','This report will display customer sales and vendor costs of Simpson products by Branch, Cat/Class, Master Account and Job Site Account.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_SIMP_PROF_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','','CSV,HTML,Html Summary,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Simpson Profile Report
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'BRANCH_NAME','Loc Name','Branch Name','','','','','2','N','','PAGE_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'EXTENDED_BRANCH_COST','Extended Branch Cost','Extended Branch Cost','','','','','17','N','','DATA_FIELD','','SUM','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'LOCATION','Loc','Location','','','','','1','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'MASTER_ACCOUNT_NAME','Master Account Name','Master Account Name','','','','','4','N','','ROW_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'MASTER_ACCOUNT_NUMBER','Master Account Number','Master Account Number','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'SALES','Extended Sales Price','Sales','','','','','16','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'SALESREP_NAME','Sales Rep Name','Salesrep Name','','','','','8','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'SALESREP_NUMBER','Sales Rep Number','Salesrep Number','','','','','7','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'SHIP_TO_LOCATION','Site-Location Name','Ship To Location','','','','','10','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'SHIP_TO_SITE_NUMBER','Site-Location Number','Ship To Site Number','','','','','9','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'CAT_CLASS_NAME','Cat Class Name','Cat Class Name','','','','','6','N','','PAGE_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'CAT_CLASS_NUMBER','Cat Class Number','Cat Class Number','','','','','5','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'SITE_SALESREP_NAME','Site-Location SalesrepName','Site Salesrep Name','','','','','12','N','','PAGE_FIELD','','','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'SITE_SALESREP_NUMBER','Site-Location SalesrepNumber','Site Salesrep Number','','','','','11','N','','PAGE_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'LINE_NUMBER','Line Number','Line Number','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
xxeis.eis_rs_ins.rc( 'Simpson Profile Report',660,'ITEM_NUMBER','Item Number','Item Number','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_SIMP_PROF_V','','');
--Inserting Report Parameters - Simpson Profile Report
xxeis.eis_rs_ins.rp( 'Simpson Profile Report',660,'From Date','From Date','ORDERED_DATE','>=','','','DATE','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Simpson Profile Report',660,'To Date','To Date','ORDERED_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Simpson Profile Report',660,'Cat Class From','Cat Class From','CAT_CLASS_NUMBER','>=','OM Simpson Inv Category','','VARCHAR2','Y','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Simpson Profile Report',660,'Cat Class To','Cat Class To','CAT_CLASS_NUMBER','<=','OM Simpson Inv Category','','VARCHAR2','Y','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Simpson Profile Report',660,'Customer Name','Customer Name','MASTER_ACCOUNT_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Simpson Profile Report
xxeis.eis_rs_ins.rcn( 'Simpson Profile Report',660,'ORDERED_DATE','>=',':From Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Simpson Profile Report',660,'ORDERED_DATE','<=',':To Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Simpson Profile Report',660,'CAT_CLASS_NUMBER','>=',':Cat Class From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Simpson Profile Report',660,'CAT_CLASS_NUMBER','<=',':Cat Class To','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Simpson Profile Report',660,'MASTER_ACCOUNT_NAME','IN',':Customer Name','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Simpson Profile Report
xxeis.eis_rs_ins.rs( 'Simpson Profile Report',660,'LOCATION','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Simpson Profile Report
--Inserting Report Templates - Simpson Profile Report
--Inserting Report Portals - Simpson Profile Report
--Inserting Report Dashboards - Simpson Profile Report
--Inserting Report Security - Simpson Profile Report
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50926',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50927',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50928',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50929',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50931',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50930',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','701','','50546',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Simpson Profile Report','','SO004816','',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Counter Sales Booked But Not Closed Order
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_COUNTER_ORDER_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_COUNTER_ORDER_V
Create or replace View XXEIS.EIS_XXWC_OM_COUNTER_ORDER_V
(ORDER_NUMBER,LINE_NUMBER,REASON,INVENTORY_ITEM,ITEM_DESCRIPTION,ORDERED_QUANTITY,ORDER_DATE,LINE_STATUS,WAREHOUSE,CREATED_BY,ONHAND_LOCATION,CUSTOMER_SOLD_TO,CREATION_DATE,HEADER_ID,LINE_ID,PARTY_ID,MTP_ORGANIZATION_ID,INVENTORY_ITEM_ID,ORGANIZATION_ID,TRANSACTION_TYPE_ID,LINE_TRANSACTION_TYPE_ID) AS 
SELECT OH.ORDER_NUMBER ,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER
    ||'.'
    ||OL.SHIPMENT_NUMBER
    ||'.'
    ||OL.OPTION_NUMBER)) LINE_NUMBER,
    nvl(xxeis.eis_rs_xxwc_com_util_pkg.Get_line_hold(OH.HEADER_ID,OL.LINE_ID),nvl(xxeis.eis_rs_xxwc_com_util_pkg.Get_header_hold(OH.HEADER_ID,OL.LINE_ID),  xxeis.eis_rs_xxwc_com_util_pkg.GET_LOT_STATUS(msi.inventory_item_id,msi.organization_id,ol.line_id)   )) REASON,
    msi.segment1 inventory_item ,
    Msi.Description Item_Description,
    ol.ordered_quantity ordered_quantity,
    TRUNC(OH.ORDERED_DATE) ORDER_DATE,
    xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_status(ol.line_id) LINE_STATUS,
    MTP.ORGANIZATION_CODE WAREHOUSE,
    nvl(ppf.full_name,FU.user_name) CREATED_BY,
    mtp.organization_code onhand_location,
    hca.account_NAME CUSTOMER_SOLD_TO,
    TRUNC(OL.CREATION_DATE) CREATION_DATE,
    ---Prrimary Keys
    OH.HEADER_ID ,
    OL.LINE_ID ,
    HZP.PARTY_ID ,
    MTP.ORGANIZATION_ID MTP_ORGANIZATION_ID ,
    MSI.INVENTORY_ITEM_ID ,
    MSI.ORGANIZATION_ID ,
    HEADER_ORDER_TYPE.TRANSACTION_TYPE_ID ,
    LINE_ORDER_TYPE.TRANSACTION_TYPE_ID LINE_TRANSACTION_TYPE_ID
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    hz_cust_accounts hca,
    hz_parties hzp,
    mtl_parameters mtp,
    Mtl_System_Items_Kfv Msi,
    Oe_Transaction_Types_Vl HEADER_ORDER_TYPE,
    OE_TRANSACTION_TYPES_VL LINE_ORDER_TYPE,
    PER_PEOPLE_F PPF,
    FND_USER FU,
    fnd_lookup_values_vl flv
  WHERE ol.header_id        = oh.header_id
  AND oh.sold_to_org_id     = hca.cust_account_id(+)
  AND hca.party_id          = hzp.party_id(+)
  AND ol.ship_from_org_id   = mtp.organization_id(+)
  AND msi.organization_id   = ol.ship_from_org_id
  AND msi.inventory_item_id = ol.inventory_item_id
  AND OH.ORDER_TYPE_ID      = HEADER_ORDER_TYPE.TRANSACTION_TYPE_ID
    --AND OH.CREATED_BY         = PPF.PERSON_ID
  AND FU.USER_ID     =OH.CREATED_BY
  AND FU.EMPLOYEE_ID =PPF.PERSON_ID(+)
  AND TRUNC (OH.CREATION_DATE) BETWEEN NVL (PPF.EFFECTIVE_START_DATE, TRUNC (OH.CREATION_DATE) ) AND NVL (PPF.EFFECTIVE_END_DATE, TRUNC (OH.CREATION_DATE) )
  AND upper(header_order_type.name) = 'COUNTER ORDER'
  AND Header_Order_Type.Org_Id      = Oh.Org_Id
  AND OL.LINE_TYPE_ID               = LINE_ORDER_TYPE.TRANSACTION_TYPE_ID
  AND upper(line_order_type.name)   = 'COUNTER LINE'
  AND Line_Order_Type.Org_Id        = Ol.Org_Id
  AND OL.FLOW_STATUS_CODE NOT      IN ('CLOSED','ENTERED','CANCELLED')
  and flv.lookup_type(+)            ='LINE_FLOW_STATUS'
  AND FLV.LOOKUP_CODE(+)            =OL.FLOW_STATUS_CODE
/
set scan on define on
prompt Creating View Data for Counter Sales Booked But Not Closed Order
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_COUNTER_ORDER_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_COUNTER_ORDER_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Counter Order V','EXOCOV');
--Delete View Columns for EIS_XXWC_OM_COUNTER_ORDER_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_COUNTER_ORDER_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_COUNTER_ORDER_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','LINE_STATUS',660,'Line Status','LINE_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Status');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Warehouse');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','ORDERED_QUANTITY',660,'Ordered Quantity','ORDERED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Ordered Quantity');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Order Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','INVENTORY_ITEM',660,'Inventory Item','INVENTORY_ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Inventory Item');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','CUSTOMER_SOLD_TO',660,'Customer Sold To','CUSTOMER_SOLD_TO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Sold To');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','ONHAND_LOCATION',660,'Onhand Location','ONHAND_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Onhand Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','LINE_TRANSACTION_TYPE_ID',660,'Line Transaction Type Id','LINE_TRANSACTION_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Transaction Type Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','MTP_ORGANIZATION_ID',660,'Mtp Organization Id','MTP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mtp Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','TRANSACTION_TYPE_ID',660,'Transaction Type Id','TRANSACTION_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Transaction Type Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_COUNTER_ORDER_V','REASON',660,'Reason','REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reason');
--Inserting View Components for EIS_XXWC_OM_COUNTER_ORDER_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_COUNTER_ORDER_V','OE_ORDER_HEADERS',660,'OE_ORDER_HEADERS_ALL','OH','OH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Headers All Stores Header Information For','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_COUNTER_ORDER_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_COUNTER_ORDER_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_COUNTER_ORDER_V','MTL_PARAMETERS',660,'MTL_PARAMETERS','MTP','MTP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Control Options And Defaults','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_COUNTER_ORDER_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','');
--Inserting View Component Joins for EIS_XXWC_OM_COUNTER_ORDER_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_COUNTER_ORDER_V','OE_ORDER_HEADERS','OH',660,'EXOCOV.HEADER_ID','=','OH.HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_COUNTER_ORDER_V','OE_ORDER_LINES','OL',660,'EXOCOV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_COUNTER_ORDER_V','HZ_PARTIES','HZP',660,'EXOCOV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_COUNTER_ORDER_V','MTL_PARAMETERS','MTP',660,'EXOCOV.MTP_ORGANIZATION_ID','=','MTP.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_COUNTER_ORDER_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOCOV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_COUNTER_ORDER_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOCOV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Counter Sales Booked But Not Closed Order
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Counter Sales Booked But Not Closed Order
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Counter Sales Booked But Not Closed Order
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Counter Sales Booked But Not Closed Order
xxeis.eis_rs_utility.delete_report_rows( 'Counter Sales Booked But Not Closed Order' );
--Inserting Report - Counter Sales Booked But Not Closed Order
xxeis.eis_rs_ins.r( 660,'Counter Sales Booked But Not Closed Order','','Provide visibility to counter sales create on previous days that are in a non-closed status. Counter lines will close the same day they are booked unless an issue related to Lot or Serial Controll exists.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_COUNTER_ORDER_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Counter Sales Booked But Not Closed Order
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'CREATED_BY','Created By','Created By','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'INVENTORY_ITEM','Inventory Item','Inventory Item','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'LINE_STATUS','Line Status','Line Status','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'ORDERED_QUANTITY','Ordered Quantity','Ordered Quantity','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'ORDER_DATE','Order Date','Order Date','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'ORDER_NUMBER','Order Number','Order Number','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'WAREHOUSE','Warehouse','Warehouse','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'LINE_NUMBER','Line Number','Line Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
xxeis.eis_rs_ins.rc( 'Counter Sales Booked But Not Closed Order',660,'REASON','Reason','Reason','','','','','10','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_COUNTER_ORDER_V','','');
--Inserting Report Parameters - Counter Sales Booked But Not Closed Order
xxeis.eis_rs_ins.rp( 'Counter Sales Booked But Not Closed Order',660,'Date From','Date From','CREATION_DATE','>=','','select sysdate-1 from dual','DATE','Y','Y','1','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Counter Sales Booked But Not Closed Order',660,'Date To','Date To','CREATION_DATE','<=','','select sysdate-1 from dual','DATE','Y','Y','2','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Counter Sales Booked But Not Closed Order',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM WAREHOUSE','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Counter Sales Booked But Not Closed Order
xxeis.eis_rs_ins.rcn( 'Counter Sales Booked But Not Closed Order',660,'WAREHOUSE','IN',':Warehouse','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Counter Sales Booked But Not Closed Order',660,'CREATION_DATE','>=',':Date From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Counter Sales Booked But Not Closed Order',660,'CREATION_DATE','<=',':Date To','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Counter Sales Booked But Not Closed Order
xxeis.eis_rs_ins.rs( 'Counter Sales Booked But Not Closed Order',660,'ORDER_NUMBER','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Counter Sales Booked But Not Closed Order',660,'LINE_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Counter Sales Booked But Not Closed Order
--Inserting Report Templates - Counter Sales Booked But Not Closed Order
--Inserting Report Portals - Counter Sales Booked But Not Closed Order
--Inserting Report Dashboards - Counter Sales Booked But Not Closed Order
--Inserting Report Security - Counter Sales Booked But Not Closed Order
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50926',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50927',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50928',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50929',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50931',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50930',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','701','','50546',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','','SO004816','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Counter Sales Booked But Not Closed Order','512','','50850',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Vendor Quote Batch Summary and Claim Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_OM_VENDOR_BATCH_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_VENDOR_BATCH_V
Create or replace View XXEIS.EIS_XXWC_OM_VENDOR_BATCH_V
(SALESPERSON_NAME,SALESPESON_NUMBER,MASTER_ACCOUNT_NAME,MASTER_ACCOUNT_NUMBER,JOB_NAME,REQUEST_NUMBER,JOB_NUMBER,VENDOR_NUMBER,VENDOR_NAME,VENDOR_QUOTE_NUMBER,INVOICE_NUMBER,INVOICE_DATE,PART_NUMBER,UOM,DESCRIPTION,PO_COST,LIST_PRICE,SPECIAL_COST,UNIT_CLAIM_VALUE,CLAIM_VALUE,QTY,REBATE,GL_CODING,GL_STRING,LOC,LOCATION,CREATED_BY,CREDIT_MEMO,REBEAT_CREDITMEMO_AMOUNT,CLAIM_NUMBER,CUSTOMER_TRX_ID,VENDOR_CLEARING_ACCOUNT,ORDER_NUMBER,LINE_NUMBER,CREATION_DATE,REQUEST_ID,BATCH_ID) AS 
SELECT ppf.full_name salesperson_name,
    jrs.salesrep_number salespeson_number,
    hcs.account_name master_account_name,
    hcs.account_number master_account_number,
    hcsu.location job_name ,
    xxeis.eis_rs_xxwc_com_util_pkg.get_ssd_req_number (oeh.header_id, oel.line_id, osbl.item_id) request_number,
    --       HPS.PARTY_SITE_NUMBER                   JOB_NUMBER,
    SUBSTR(hcsu.location,instr(hcsu.location,'-',1)+1) job_number,
    pv.segment1 vendor_number,
    pv.vendor_name vendor_name,
    osbl.agreement_number vendor_quote_number,
    NVL(rct.trx_number,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring(oeh.order_number,'TRXNUM',oel.line_id)) invoice_number,
    NVL(rct.trx_date,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring(oeh.order_number,'TRXDATE',oel.line_id)) invoice_date,
    msi.concatenated_segments part_number,
    msi.primary_unit_of_measure uom,
    msi.description description,
    osbl.list_price po_cost,
    osbl.list_price list_price,
    OSBL.AGREEMENT_PRICE SPECIAL_COST,
    NVL(osbl.list_price,0) - NVL(osbl.agreement_price,0) unit_claim_value,
    osbl.claim_amount claim_value,
    osbl.quantity_shipped qty,
    OSBL.CLAIM_AMOUNT REBATE,
    '400-900'
    ||'-'
    ||NVL(gcc.segment2,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring(oeh.order_number,'LOCATION',oel.line_id)) gl_coding,
    NVL(gcc.concatenated_segments,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring(oeh.order_number,'STRING',oel.line_id))gl_string,
    NVL(gcc.segment2,xxeis.eis_rs_xxwc_com_util_pkg.get_venquote_glstring(oeh.order_number,'LOCATION',oel.line_id))loc,
    ood.organization_code location,
    ppf1.full_name created_by,
    -- ap.invoice_num                                        credit_memo,
    --ap.invoice_amount                                     rebeat_creditmemo_amount,
    DECODE(ap.invoice_num,'',
    (SELECT ap.invoice_num
    FROM ap_invoices ap,
      ozf_claims_all oca1
    WHERE oca1.batch_id   =osbh.batch_id
    AND ap.reference_key1 = oca1.claim_id
    ), ap.invoice_num )credit_memo,
    DECODE(ap.invoice_amount,'',
    (SELECT ap.invoice_amount
    FROM ap_invoices ap,
      ozf_claims_all oca1
    WHERE oca1.batch_id   =osbh.batch_id
    AND ap.reference_key1 = oca1.claim_id
    ), ap.invoice_amount )rebeat_creditmemo_amount,
    --Oca.Claim_Number                                      Claim_Number,
    DECODE(oca.claim_number,'',
    (SELECT oca2.claim_number
    FROM ozf_claims_all oca2
    WHERE oca2.batch_id=osbh.batch_id
    ), oca.claim_number )Claim_Number,
    rct.customer_trx_id,
    gcck.concatenated_segments vendor_clearing_account,
    oeh.order_number,
    oel.line_number,
    TRUNC(OSBH.CREATION_DATE) CREATION_DATE,
    osbl.request_id,
    osbl.batch_id
  FROM ozf_sd_batch_headers_all osbh,
    ozf_sd_batch_lines_all osbl,
    ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    ra_cust_trx_line_gl_dist rctlgl,
    oe_order_headers oeh,
    oe_order_lines oel,
    po_vendors pv,
    po_vendor_sites pvs,
    mtl_system_items_b_kfv msi,
    org_organization_definitions ood,
    jtf_rs_salesreps jrs,
    PER_ALL_PEOPLE_F PPF,
    gl_code_combinations_kfv gcc,
    hz_parties hp,
    hz_cust_accounts hcs,
    hz_cust_site_uses hcsu,
    hz_cust_acct_sites hcas,
    hz_party_sites hps,
    per_all_people_f ppf1,
    fnd_user fu,
    -- RA_CUSTOMER_TRX               RCTC,
    --  Ar_Receivable_Applications    Arp,
    ozf_claims_all oca,
    ap_invoices ap,
    ozf_sys_parameters_all osp,
    GL_CODE_COMBINATIONS_KFV GCCk
  WHERE osbh.batch_id                 = osbl.batch_id
  AND rct.customer_trx_id (+)         = osbl.invoice_number
  AND rctl.customer_trx_line_id (+)   = osbl.invoice_line_number
  AND Rctlgl.Customer_Trx_Id (+)      = Rctl.Customer_Trx_Id
  AND Rctlgl.Customer_Trx_Line_Id (+) = Rctl.Customer_Trx_Line_Id
  AND Rctl.Line_Type (+)              ='LINE'
  AND Oeh.Header_Id                   = Oel.Header_Id
  AND Oeh.Header_Id                   = Osbl.Order_Header_Id
  AND Oel.Line_Id                     = Osbl.Order_Line_Id
  AND Oeh.Flow_Status_Code            ='CLOSED'
  AND Pv.Vendor_Id                    = Pvs.Vendor_Id
  AND Pv.Vendor_Id                    = Osbh.Vendor_Id
  AND Pvs.Vendor_Site_Id              = Osbh.Vendor_Site_Id
  AND Pvs.Org_Id                      = Osbh.Org_Id
  AND Msi.Inventory_Item_Id           = Osbl.Item_Id
  AND Msi.Organization_Id             = Osbl.Org_Id
  AND Ood.Organization_Id             = Osbl.Org_Id
    -- AND RCT.PRIMARY_SALESREP_ID           = JRS.SALESREP_ID(+)
  AND oeh.salesrep_id = jrs.salesrep_id(+)
    --AND Rct.Org_Id                        = Jrs.Org_Id(+)
  AND OEH.Org_Id        = Jrs.Org_Id(+)
  AND ppf.person_id (+) = jrs.person_id
    -- and nvl(trunc(OEH.ORDERED_DATE),sysdate) >= decode(ppf.effective_start_date,'',nvl(trunc(OEH.ORDERED_DATE),sysdate))
    -- and nvl(trunc(OEH.ORDERED_DATE),sysdate) <= decode(ppf.effective_start_date,'',NVL(trunc(OEH.ORDERED_DATE),sysdate))
  AND Hp.Party_Id                 = Hcs.Party_Id
  AND Hp.Party_Id                 = Osbl.Sold_To_Customer_Id
  AND Hcsu.Site_Use_Id            = Osbl.Ship_To_Org_Id
  AND Hcsu.Cust_Acct_Site_Id      = Hcas.Cust_Acct_Site_Id
  AND HCAS.PARTY_SITE_ID          = HPS.PARTY_SITE_ID
  AND GCC.CODE_COMBINATION_ID (+) = RCTLGL.CODE_COMBINATION_ID
  AND Fu.User_Id                  = Osbh.Created_By
  AND fu.employee_id              = ppf1.person_id
    -- and nvl(trunc(oeh.ordered_date),sysdate) >= decode(ppf1.effective_start_date,'',nvl(trunc(oeh.ordered_date),sysdate))
    -- and nvl(trunc(OEH.ORDERED_DATE),sysdate) <= decode(ppf1.effective_start_date,'',NVL(trunc(OEH.ORDERED_DATE),sysdate))
  AND Oca.Claim_Id (+) = Osbh.Claim_Id
    /* and( oca.claim_id                   = osbh.claim_id
    OR oca.batch_id  = osbh.batch_id)*/
  AND ap.reference_key1 (+) = oca.claim_id
  AND osp.org_id            = osbh.org_id
    -- and oeh.header_id=9264
  and OSP.GL_ID_DED_CLEARING = GCCK.CODE_COMBINATION_ID
/
set scan on define on
prompt Creating View Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_VENDOR_BATCH_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_VENDOR_BATCH_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Vendor Batch V','EXOVBV');
--Delete View Columns for EIS_XXWC_OM_VENDOR_BATCH_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_VENDOR_BATCH_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_VENDOR_BATCH_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','REQUEST_ID',660,'Request Id','REQUEST_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Request Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','CUSTOMER_TRX_ID',660,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','CREDIT_MEMO',660,'Credit Memo','CREDIT_MEMO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Credit Memo');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','LOC',660,'Loc','LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','REBATE',660,'Rebate','REBATE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Rebate');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','LIST_PRICE',660,'List Price','LIST_PRICE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','List Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','DESCRIPTION',660,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','UOM',660,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','VENDOR_QUOTE_NUMBER',660,'Vendor Quote Number','VENDOR_QUOTE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Quote Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','JOB_NUMBER',660,'Job Number','JOB_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','REQUEST_NUMBER',660,'Request Number','REQUEST_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Request Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','JOB_NAME',660,'Job Name','JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','MASTER_ACCOUNT_NUMBER',660,'Master Account Number','MASTER_ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Account Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','MASTER_ACCOUNT_NAME',660,'Master Account Name','MASTER_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Account Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','SALESPESON_NUMBER',660,'Salespeson Number','SALESPESON_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salespeson Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','SALESPERSON_NAME',660,'Salesperson Name','SALESPERSON_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesperson Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','GL_CODING',660,'Gl Coding','GL_CODING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Coding');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','LOCATION',660,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','PO_COST',660,'Po Cost','PO_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Po Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','REBEAT_CREDITMEMO_AMOUNT',660,'Rebeat Creditmemo Amount','REBEAT_CREDITMEMO_AMOUNT','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Rebeat Creditmemo Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','SPECIAL_COST',660,'Special Cost','SPECIAL_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Special Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','UNIT_CLAIM_VALUE',660,'Unit Claim Value','UNIT_CLAIM_VALUE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit Claim Value');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','CLAIM_NUMBER',660,'Claim Number','CLAIM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Claim Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','GL_STRING',660,'Gl String','GL_STRING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl String');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','VENDOR_CLEARING_ACCOUNT',660,'Vendor Clearing Account','VENDOR_CLEARING_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Clearing Account');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','CLAIM_VALUE',660,'Claim Value','CLAIM_VALUE','','','','XXEIS_RS_ADMIN','NUMBER','','','Claim Value');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','BATCH_ID',660,'Batch Id','BATCH_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Batch Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_BATCH_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Number');
--Inserting View Components for EIS_XXWC_OM_VENDOR_BATCH_V
--Inserting View Component Joins for EIS_XXWC_OM_VENDOR_BATCH_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT VENDOR_NAME 
    FROM PO_VENDORS POV','','OM Vendor Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct segment1 Vendor_Number from po_vendors','','OM Vendor Number LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_utility.delete_report_rows( 'Vendor Quote Batch Summary and Claim Report' );
--Inserting Report - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.r( 660,'Vendor Quote Batch Summary and Claim Report','','Provides order, credit customer and vendor details for all closed orders associated with a Vendor Quote.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_VENDOR_BATCH_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'CREATED_BY','Created By','Created By','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'CREDIT_MEMO','Credit Memo','Credit Memo','','','','','26','N','','ROW_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'DESCRIPTION','Description','Description','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'JOB_NAME','Job Name','Job Name','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'JOB_NUMBER','Site Number','Job Number','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LOC','Loc','Loc','','','','','25','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NAME','Master Account Name','Master Account Name','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NUMBER','Master Account Number','Master Account Number','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'PART_NUMBER','Part Number','Part Number','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'QTY','Qty','Qty','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'REBATE','Rebate','Rebate','','','','','22','N','','DATA_FIELD','','SUM','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'REQUEST_NUMBER','Request Number','Request Number','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SALESPERSON_NAME','Salesperson Name','Salesperson Name','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SALESPESON_NUMBER','Salespeson Number','Salespeson Number','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'UOM','Uom','Uom','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_QUOTE_NUMBER','Vendor Quote Number','Vendor Quote Number','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'GL_CODING','GL Coding','Gl Coding','','','','','24','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LOCATION','Location','Location','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'PO_COST','Po Cost','Po Cost','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'REBEAT_CREDITMEMO_AMOUNT','Rebate_Credit','Rebeat Creditmemo Amount','','','','','27','N','','DATA_FIELD','','SUM','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SPECIAL_COST','Special Cost','Special Cost','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'UNIT_CLAIM_VALUE','Unit Claim Value','Unit Claim Value','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'CLAIM_NUMBER','Claim Number','Claim Number','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'GL_STRING','GL String','Gl String','','','','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_CLEARING_ACCOUNT','Vendor Clearing Account','Vendor Clearing Account','','','','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_BATCH_V','','');
--Inserting Report Parameters - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Date Range From','Date Range From','CREATION_DATE','>=','','','DATE','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Vendor Name','Vendor Name','VENDOR_NAME','IN','OM Vendor Name LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Vendor Number','Vendor Number','VENDOR_NUMBER','IN','OM Vendor Number LOV','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Date Range To','Date Range To','CREATION_DATE','<=','','','DATE','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Bill To Customer','Bill To Customer','MASTER_ACCOUNT_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NAME','IN',':Vendor Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NUMBER','IN',':Vendor Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NAME','IN',':Bill To Customer','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'CREATION_DATE','>=',':Date Range From','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'CREATION_DATE','<=',':Date Range To','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Vendor Quote Batch Summary and Claim Report
--Inserting Report Triggers - Vendor Quote Batch Summary and Claim Report
--Inserting Report Templates - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.R_Tem( 'Vendor Quote Batch Summary and Claim Report','Vendor Quote Batch Summary and Claim Report','Seeded Template for Vendor Quote Batch Summary and Claim Report','','','','','','','','','','','','XXEIS_RS_ADMIN');
--Inserting Report Portals - Vendor Quote Batch Summary and Claim Report
--Inserting Report Dashboards - Vendor Quote Batch Summary and Claim Report
--Inserting Report Security - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','21623',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Outstanding Rental Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_OM_OUTSTG_RTL_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_OUTSTG_RTL_V
Create or replace View XXEIS.EIS_XXWC_OM_OUTSTG_RTL_V
(ORDER_NUMBER,EST_RENTAL_RETURN_DATE,CREATED_BY,LINE_NUMBER,ORDER_TYPE,CUSTOMER_NAME,CUSTOMER_JOB_NAME,SALES_PERSON_NAME,ITEM_NUMBER,ITEM_DESCRIPTION,QUANTITY_ON_RENT,RENTAL_START_DATE,RENTAL_LOCATION,DAYS_ON_RENT,HEADER_ID,LINE_ID,PARTY_ID,ORGANIZATION_ID,INVENTORY_ITEM_ID,MSI_ORGANIZATION_ID) AS 
SELECT OH.ORDER_NUMBER,
     decode(substr(ol1.ATTRIBUTE1,5,1),'/' ,to_char(to_date(substr(ol1.ATTRIBUTE1,1,10),'yyyy/mm/dd'),'DD-MON-YYYY'),ol1.ATTRIBUTE1) EST_RENTAL_RETURN_DATE, 
     PPF.FULL_NAME CREATED_BY,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER||'.'||OL.OPTION_NUMBER)) LINE_NUMBER, 
    ott.name order_type,
    hzp.party_name customer_name,
    hzcs_ship_to.location customer_job_name,
    rep.name sales_person_name,
    msi.segment1 item_number ,
    msi.description item_description,
    ol.ordered_quantity quantity_on_rent,
    xxeis.eis_rs_xxwc_com_util_pkg.get_rental_start_date(ol.Link_To_Line_Id) rental_start_date,
    mtp.organization_code rental_location,
    ( trunc(sysdate)- xxeis.eis_rs_xxwc_com_util_pkg.Get_rental_start_date(ol.Link_To_Line_Id)) DAYS_ON_RENT,
    ---Primary Keys
    oh.header_id,
    ol.line_id,
    hzp.party_id,
    mtp.organization_id,
    msi.inventory_item_id,
    Msi.Organization_Id Msi_Organization_Id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM OE_ORDER_LINES OL,
    oe_order_lines ol1,
    oe_order_headers oh,
    Oe_Transaction_Types_Vl Ott,
    Ra_Salesreps Rep,
    Hz_Cust_Accounts Hca,
    HZ_PARTIES HZP,
    FND_USER FU,
    PER_PEOPLE_F PPF,
    Mtl_System_Items_Kfv Msi,
    mtl_parameters mtp,
    Hz_Cust_Site_Uses Hzcs_Ship_To,
    Hz_Cust_Acct_Sites Hcas_Ship_To,
    hz_party_sites hzps_ship_to,
    hz_locations hzl_ship_to
  where ol.flow_status_code         in ('AWAITING_RETURN')
  AND OH.HEADER_ID                   = OL.HEADER_ID
  and ol.link_to_line_id             = ol1.line_id
  AND oh.order_type_id               = ott.transaction_type_id
  AND oh.salesrep_id                 = rep.salesrep_id(+)
  AND oh.sold_to_org_id              = hca.cust_account_id(+)
  AND HCA.PARTY_ID                   = HZP.PARTY_ID(+)
  AND FU.USER_ID     =Ol.CREATED_BY
  AND FU.EMPLOYEE_ID =PPF.PERSON_ID(+)
  AND TRUNC (OL.CREATION_DATE) BETWEEN NVL (PPF.EFFECTIVE_START_DATE, TRUNC (OL.CREATION_DATE) ) AND NVL (PPF.EFFECTIVE_END_DATE, TRUNC (OL.CREATION_DATE) )
  AND msi.organization_id            = ol.ship_from_org_id
  AND msi.inventory_item_id          = ol.inventory_item_id
  AND ol.ship_from_org_id            = mtp.organization_id(+)
  AND oh.ship_to_org_id              = hzcs_ship_to.site_use_id(+)
  AND hzcs_ship_to.cust_acct_site_id = hcas_ship_to.cust_acct_site_id(+)
  AND hcas_ship_to.party_site_id     = hzps_ship_to.party_site_id(+)
  AND HZL_SHIP_TO.LOCATION_ID(+)     = HZPS_SHIP_TO.LOCATION_ID
  AND OTT.NAME                      IN ('WC LONG TERM RENTAL', 'WC SHORT TERM RENTAL')
/
set scan on define on
prompt Creating View Data for Outstanding Rental Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_OUTSTG_RTL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_OUTSTG_RTL_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Outstg Rtl V','EXOORV');
--Delete View Columns for EIS_XXWC_OM_OUTSTG_RTL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_OUTSTG_RTL_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_OUTSTG_RTL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','RENTAL_LOCATION',660,'Rental Location','RENTAL_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rental Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','RENTAL_START_DATE',660,'Rental Start Date','RENTAL_START_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Rental Start Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','QUANTITY_ON_RENT',660,'Quantity On Rent','QUANTITY_ON_RENT','','','','XXEIS_RS_ADMIN','NUMBER','','','Quantity On Rent');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','SALES_PERSON_NAME',660,'Sales Person Name','SALES_PERSON_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Person Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','DAYS_ON_RENT',660,'Days On Rent','DAYS_ON_RENT','','','','XXEIS_RS_ADMIN','NUMBER','','','Days On Rent');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','CUSTOMER_JOB_NAME',660,'Customer Job Name','CUSTOMER_JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Job Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msi Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','EST_RENTAL_RETURN_DATE',660,'Est Rental Return Date','EST_RENTAL_RETURN_DATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Est Rental Return Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_OUTSTG_RTL_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By');
--Inserting View Components for EIS_XXWC_OM_OUTSTG_RTL_V
--Inserting View Component Joins for EIS_XXWC_OM_OUTSTG_RTL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Outstanding Rental Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Outstanding Rental Report
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT C.LOCATION  SHIP_TO_LOCATION ,A.ACCOUNT_NUMBER
CUSTOMER_NUMBER,A.ACCOUNT_NAME CUSTOMER_NAME ,HOU.NAME OPERATING_UNIT
FROM HZ_CUST_ACCOUNTS A,HZ_CUST_ACCT_SITES B,
HZ_CUST_SITE_USES C,
HR_OPERATING_UNITS HOU
 WHERE A.CUST_ACCOUNT_ID=B.CUST_ACCOUNT_ID
 AND B.CUST_ACCT_SITE_ID=C.CUST_ACCT_SITE_ID
 AND SITE_USE_CODE=''SHIP_TO''
 AND B.ORG_ID=HOU.ORGANIZATION_ID
','','OM SHIP TO LOCATION','This gives ship to locations','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Outstanding Rental Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Outstanding Rental Report
xxeis.eis_rs_utility.delete_report_rows( 'Outstanding Rental Report' );
--Inserting Report - Outstanding Rental Report
xxeis.eis_rs_ins.r( 660,'Outstanding Rental Report','','Identifies all Order Types Rental and returns Customer Name, Customer Project, Salesman, Part Numbers and Days on Rent to help support and track duration of project and rentals adverting overdue issues and credits.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_OUTSTG_RTL_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Outstanding Rental Report
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'ITEM_NUMBER','Item Number','Item Number','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'ORDER_TYPE','Order Type','Order Type','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'QUANTITY_ON_RENT','Quantity On Rent','Quantity On Rent','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'RENTAL_LOCATION','Rental Loc','Rental Location','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'RENTAL_START_DATE','Rental Start Date','Rental Start Date','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'SALES_PERSON_NAME','Sales Person Name','Sales Person Name','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'DAYS_ON_RENT','Days On Rent','Days On Rent','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'CUSTOMER_JOB_NAME','Job Site Name-Number','Customer Job Name','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'CREATED_BY','Created By','Created By','','','','','12','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
xxeis.eis_rs_ins.rc( 'Outstanding Rental Report',660,'EST_RENTAL_RETURN_DATE','Est Rental Return Date','Est Rental Return Date','','','','','13','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_OUTSTG_RTL_V','','');
--Inserting Report Parameters - Outstanding Rental Report
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Start Date','Start Date','RENTAL_START_DATE','>=','','','DATE','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'End Date','End Date','RENTAL_START_DATE','<=','','','DATE','N','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Warehouse','Warehouse','RENTAL_LOCATION','IN','OM WAREHOUSE','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Customer Name','Customer Name','CUSTOMER_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Outstanding Rental Report',660,'Customer Job Name','Customer Job Name','CUSTOMER_JOB_NAME','IN','OM SHIP TO LOCATION','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Outstanding Rental Report
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'TRUNC(RENTAL_START_DATE)','>=',':Start Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'TRUNC(RENTAL_START_DATE)','<=',':End Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'RENTAL_LOCATION','IN',':Warehouse','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'CUSTOMER_NAME','IN',':Customer Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Outstanding Rental Report',660,'CUSTOMER_JOB_NAME','IN',':Customer Job Name','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Outstanding Rental Report
xxeis.eis_rs_ins.rs( 'Outstanding Rental Report',660,'CUSTOMER_NAME','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Outstanding Rental Report',660,'CUSTOMER_JOB_NAME','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Outstanding Rental Report
--Inserting Report Templates - Outstanding Rental Report
xxeis.eis_rs_ins.R_Tem( 'Outstanding Rental Report','Outstanding Rental Report','Seeded template for Outstanding Rental Report','','','','','','','','','','','','XXEIS_RS_ADMIN');
--Inserting Report Portals - Outstanding Rental Report
--Inserting Report Dashboards - Outstanding Rental Report
--Inserting Report Security - Outstanding Rental Report
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50926',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50927',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50928',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50929',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50931',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50930',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Outstanding Rental Report','','SO004816','',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Back Ordered Inventory Movement Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_BACK_ORDER_INV_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_BACK_ORDER_INV_V
Create or replace View XXEIS.EIS_XXWC_OM_BACK_ORDER_INV_V
(ORDER_NUMBER,ACTION,LINE_NUMBER,ORDERED_ITEM,ITEM_DESCRIPTION,QUANTITY,ORDERED_DATE,SUB_INV,CREATED_BY,LAST_UPDATED_BY,WAREHOUSE,HEADER_ID,LINE_ID,INVENTORY_ITEM_ID,ORGANIZATION_ID,DELIVERY_DETAIL_ID) AS 
SELECT OH.ORDER_NUMBER ORDER_NUMBER,
    /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Full Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Full Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Hold Reserve'
    END action,*/
    'Full Cancellation / Hold Reserve' action,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER||'.'||OL.OPTION_NUMBER)) LINE_NUMBER,
    msi.segment1 ordered_item,
    msi.description item_description,
    ol.CANCELLED_QUANTITY quantity,
    oh.ordered_date ordered_date,
    OL.SUBINVENTORY SUB_INV,
    --hre.full_name created_by,
    nvl(ppf.full_name,fu.user_name) created_by,
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by(ol.line_id) last_updated_by,
    ood.organization_code warehouse,
    --Primary Keys
    oh.header_id,
    ol.line_id,
    msi.inventory_item_id,
    msi.organization_id,
    wdd.delivery_detail_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol ,
    mtl_system_items_kfv msi,
    --hr_employees hre,
    wsh_delivery_details wdd,
    oe_order_lines_history olh,
    org_organization_definitions ood,
    fnd_user fu,
    per_people_f ppf    
  WHERE oh.header_id          = ol.header_id
  AND ol.inventory_item_id    = msi.inventory_item_id
  and ol.ship_from_org_id     = msi.organization_id
  --AND oh.created_by           = hre.employee_id(+)
  and fu.user_id     =oh.created_by
  and fu.employee_id =ppf.person_id(+)
  and trunc (oh.ordered_date) between nvl (ppf.effective_start_date, trunc (oh.ordered_date) ) and nvl (ppf.effective_end_date, trunc (oh.ordered_date) )  
  AND ol.header_id            = wdd.source_header_id(+)
  AND ol.line_id              = wdd.source_line_id(+)
 -- AND wdd.released_status     = 'B'
  AND ol.header_id            = olh.header_id(+)
  AND OL.LINE_ID              = OLH.LINE_ID(+)
  AND ood.organization_id     = msi.organization_id
  AND oh.flow_status_code    <>'ENTERED'
  AND OL.FLOW_STATUS_CODE     ='CANCELLED'
  AND UPPER(OL.SUBINVENTORY) IN('RENTALHLD','GENERALHLD')
  AND trunc(OL.LAST_UPDATE_DATE) BETWEEN xxeis.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM AND xxeis.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
  --and ol.last_update_date <=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM
  --and ol.last_update_date >=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
  UNION
  SELECT OH.ORDER_NUMBER ORDER_NUMBER,
    /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Full Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Full Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Hold Reserve'
    END action,*/
    'Full Cancellation' action,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER||'.'||OL.OPTION_NUMBER)) line_number,
    msi.segment1 ordered_item,
    msi.description item_description,
    ol.CANCELLED_QUANTITY quantity,
    oh.ordered_date ordered_date,
    ol.subinventory sub_inv,
    --hre.full_name created_by,
    nvl(ppf.full_name,fu.user_name) created_by,    
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by(ol.line_id) last_updated_by,
    ood.organization_code warehouse,
    --Primary Keys
    oh.header_id,
    ol.line_id,
    msi.inventory_item_id,
    msi.organization_id,
    wdd.delivery_detail_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol ,
    mtl_system_items_kfv msi,
    --hr_employees hre,
    wsh_delivery_details wdd,
    oe_order_lines_history olh,
    org_organization_definitions ood,
    fnd_user fu,
    per_people_f ppf        
  WHERE oh.header_id              = ol.header_id
  AND ol.inventory_item_id        = msi.inventory_item_id
  and ol.ship_from_org_id         = msi.organization_id
  --AND oh.created_by               = hre.employee_id(+)
  and fu.user_id     =oh.created_by
  and fu.employee_id =ppf.person_id(+)
  and trunc (oh.ordered_date) between nvl (ppf.effective_start_date, trunc (oh.ordered_date) ) and nvl (ppf.effective_end_date, trunc (oh.ordered_date) )  
  AND ol.header_id                = wdd.source_header_id(+)
  AND ol.line_id                  = wdd.source_line_id(+)
--  AND wdd.released_status         = 'B'
  AND ol.header_id                = olh.header_id(+)
  AND OL.LINE_ID                  = OLH.LINE_ID(+)
  AND ood.organization_id         = msi.organization_id
  AND Oh.flow_status_code        <>'ENTERED'
  AND ol.flow_status_code         ='CANCELLED'
  AND UPPER(OL.SUBINVENTORY) IN('RENTAL','GENERAL')
  AND trunc(OL.LAST_UPDATE_DATE) BETWEEN EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM AND EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
  --and ol.last_update_date <=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM
  --and OL.LAST_UPDATE_DATE >=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
  UNION
  SELECT OH.ORDER_NUMBER ORDER_NUMBER,
    /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Full Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Full Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Hold Reserve'
    END action,*/
    'Partial Cancellation / Hold Reserve' action,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER||'.'||OL.OPTION_NUMBER)) line_number,
    msi.segment1 ordered_item,
    msi.description item_description,
    ol.CANCELLED_QUANTITY quantity,
    oh.ordered_date ordered_date,
    ol.subinventory sub_inv,
    --hre.full_name created_by,
    nvl(ppf.full_name,fu.user_name) created_by,    
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by(ol.line_id) last_updated_by,
    ood.organization_code warehouse,
    --Primary Keys
    oh.header_id,
    ol.line_id,
    msi.inventory_item_id,
    msi.organization_id,
    wdd.delivery_detail_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol ,
    mtl_system_items_kfv msi,
    --hr_employees hre,
    wsh_delivery_details wdd,
    oe_order_lines_history olh,
    org_organization_definitions ood,
    fnd_user fu,
    per_people_f ppf        
  WHERE oh.header_id          = ol.header_id
  AND ol.inventory_item_id    = msi.inventory_item_id
  and ol.ship_from_org_id     = msi.organization_id
  --AND oh.created_by           = hre.employee_id(+)
  and fu.user_id     =oh.created_by
  and fu.employee_id =ppf.person_id(+)
  and trunc (oh.ordered_date) between nvl (ppf.effective_start_date, trunc (oh.ordered_date) ) and nvl (ppf.effective_end_date, trunc (oh.ordered_date) )   
  AND ol.header_id            = wdd.source_header_id(+)
  AND ol.line_id              = wdd.source_line_id(+)
 -- AND wdd.released_status     = 'B'
  AND ol.header_id            = olh.header_id(+)
  AND OL.LINE_ID              = OLH.LINE_ID(+)
  AND ood.organization_id     = msi.organization_id
  AND oh.flow_status_code    <>'ENTERED'
  AND (OL.CANCELLED_QUANTITY <>0
  AND OL.ORDERED_QUANTITY    <>0)
  AND OLH.HIST_TYPE_CODE      = 'CANCELLATION'
  AND trunc(OLH.HIST_CREATION_DATE) BETWEEN EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM AND EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
    --And Olh.Hist_Creation_Date <=Eis_Rs_Xxwc_Com_Util_Pkg.Get_Date_From
    --and OLH.HIST_CREATION_DATE >=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
  AND UPPER(ol.subinventory) IN('RENTALHLD','GENERALHLD')
  UNION
  SELECT OH.ORDER_NUMBER ORDER_NUMBER,
    /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Full Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Full Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Hold Reserve'
    END action,*/
    'Partial Cancellation' action,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER||'.'||OL.OPTION_NUMBER)) line_number,
    msi.segment1 ordered_item,
    msi.description item_description,
    ol.CANCELLED_QUANTITY quantity,
    oh.ordered_date ordered_date,
    ol.subinventory sub_inv,
    --hre.full_name created_by,
    nvl(ppf.full_name,fu.user_name) created_by,    
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by(ol.line_id) last_updated_by,
    ood.organization_code warehouse,
    --Primary Keys
    oh.header_id,
    ol.line_id,
    msi.inventory_item_id,
    msi.organization_id,
    wdd.delivery_detail_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol ,
    mtl_system_items_kfv msi,
    --hr_employees hre,
    wsh_delivery_details wdd,
    oe_order_lines_history olh,
    org_organization_definitions ood,
    fnd_user fu,
    per_people_f ppf        
  WHERE oh.header_id          = ol.header_id
  AND ol.inventory_item_id    = msi.inventory_item_id
  and ol.ship_from_org_id     = msi.organization_id
  --AND oh.created_by           = hre.employee_id(+)
  and fu.user_id     =oh.created_by
  and fu.employee_id =ppf.person_id(+)
  and trunc (oh.ordered_date) between nvl (ppf.effective_start_date, trunc (oh.ordered_date) ) and nvl (ppf.effective_end_date, trunc (oh.ordered_date) )   
  AND ol.header_id            = wdd.source_header_id(+)
  AND ol.line_id              = wdd.source_line_id(+)
 -- AND wdd.released_status     = 'B'
  AND ol.header_id            = olh.header_id(+)
  AND OL.LINE_ID              = OLH.LINE_ID(+)
  AND ood.organization_id     = msi.organization_id
  AND oh.flow_status_code    <>'ENTERED'
  AND (ol.cancelled_quantity <>0
  AND ol.ordered_quantity    <>0)
  AND OLH.HIST_TYPE_CODE      = 'CANCELLATION'
  AND trunc(OLH.HIST_CREATION_DATE) BETWEEN EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM AND EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
    --and olh.HIST_CREATION_DATE <=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM
    --and OLH.HIST_CREATION_DATE >=EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
  AND UPPER(ol.subinventory) NOT IN('RENTAL','GENERAL')
  UNION
  SELECT OH.ORDER_NUMBER ORDER_NUMBER,
    /*CASE WHEN oh.flow_status_code  <>'ENTERED' AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Full Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code ='CANCELLED'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Full Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation / Hold Reserve'
    WHEN oh.flow_status_code <>'ENTERED'  AND (ol.cancelled_quantity <>0 AND ol.ordered_quantity <>0)
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory NOT IN('RENTAL','GENERAL')
    THEN 'Partial Cancellation'
    WHEN oh.flow_status_code <>'ENTERED'  AND ol.flow_status_code <>'AWAITING_SHIPPING'
    AND olh.hist_type_code = 'CANCELLATION'
    AND ol.subinventory IN('RENTAL','GENERAL')
    THEN 'Hold Reserve'
    END action,*/
    'Hold Reserve' action,
    DECODE (OL.OPTION_NUMBER,'',(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER),(OL.LINE_NUMBER||'.'||OL.SHIPMENT_NUMBER||'.'||OL.OPTION_NUMBER)) line_number,
    msi.segment1 ordered_item,
    MSI.DESCRIPTION ITEM_DESCRIPTION,
    ol.ordered_QUANTITY quantity,
    oh.ordered_date ordered_date,
    ol.subinventory sub_inv,
    --hre.full_name created_by,
    nvl(ppf.full_name,fu.user_name) created_by,    
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_updt_by(ol.line_id) last_updated_by,
    ood.organization_code warehouse,
    --Primary Keys
    oh.header_id,
    ol.line_id,
    msi.inventory_item_id,
    msi.organization_id,
    wdd.delivery_detail_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol ,
    mtl_system_items_kfv msi,
    --hr_employees hre,
    wsh_delivery_details wdd,
    oe_order_lines_history olh,
    org_organization_definitions ood,
    fnd_user fu,
    per_people_f ppf        
  WHERE oh.header_id          = ol.header_id
  AND ol.inventory_item_id    = msi.inventory_item_id
  and ol.ship_from_org_id     = msi.organization_id
  --AND oh.created_by           = hre.employee_id(+)
  and fu.user_id     =oh.created_by
  and fu.employee_id =ppf.person_id(+)
  and trunc (oh.ordered_date) between nvl (ppf.effective_start_date, trunc (oh.ordered_date) ) and nvl (ppf.effective_end_date, trunc (oh.ordered_date) )   
  AND ol.header_id            = wdd.source_header_id(+)
  AND ol.line_id              = wdd.source_line_id(+)
  --AND wdd.released_status     = 'B'
  AND ol.header_id            = olh.header_id(+)
  AND OL.LINE_ID              = OLH.LINE_ID(+)
  AND ood.organization_id     = msi.organization_id
  AND oh.flow_status_code    <>'ENTERED'
  AND ol.cancelled_quantity   =0
  AND ol.flow_status_code     = 'AWAITING_SHIPPING'
  AND UPPER(OL.SUBINVENTORY) IN('RENTALHLD','GENERALHLD')
/
set scan on define on
prompt Creating View Data for Back Ordered Inventory Movement Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_BACK_ORDER_INV_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_BACK_ORDER_INV_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Back Order Inv V','EXOBOIV');
--Delete View Columns for EIS_XXWC_OM_BACK_ORDER_INV_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_BACK_ORDER_INV_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_BACK_ORDER_INV_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','SUB_INV',660,'Sub Inv','SUB_INV','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sub Inv');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','QUANTITY',660,'Quantity','QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Quantity');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','ORDERED_ITEM',660,'Ordered Item','ORDERED_ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ordered Item');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Line Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','ACTION',660,'Action','ACTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Action');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','LAST_UPDATED_BY',660,'Last Updated By','LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Last Updated By');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','WAREHOUSE',660,'Warehouse','WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Warehouse');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','DELIVERY_DETAIL_ID',660,'Delivery Detail Id','DELIVERY_DETAIL_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Delivery Detail Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_BACK_ORDER_INV_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
--Inserting View Components for EIS_XXWC_OM_BACK_ORDER_INV_V
--Inserting View Component Joins for EIS_XXWC_OM_BACK_ORDER_INV_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Back Ordered Inventory Movement Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Back Ordered Inventory Movement Report
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT MSI.SECONDARY_INVENTORY_NAME,MSI.DESCRIPTION FROM MTL_SECONDARY_INVENTORIES MSI WHERE exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = MSI.organization_id)','','OM SUB INVENTORY','This gives the subinventory details','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Back Ordered Inventory Movement Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Back Ordered Inventory Movement Report
xxeis.eis_rs_utility.delete_report_rows( 'Back Ordered Inventory Movement Report' );
--Inserting Report - Back Ordered Inventory Movement Report
xxeis.eis_rs_ins.r( 660,'Back Ordered Inventory Movement Report','','Provide visibility to cancelled, partially cancelled, Hold/Reserve material that may still be physically located in the Staging area of the warehouse despite being electronically moved to the holding subinventory.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_BACK_ORDER_INV_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Back Ordered Inventory Movement Report
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'ACTION','Action','Action','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'CREATED_BY','Created By','Created By','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'LAST_UPDATED_BY','Last Updated By','Last Updated By','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'LINE_NUMBER','Line Number','Line Number','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'ORDERED_ITEM','Ordered Item','Ordered Item','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'QUANTITY','Quantity','Quantity','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'SUB_INV','Sub Inv','Sub Inv','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
xxeis.eis_rs_ins.rc( 'Back Ordered Inventory Movement Report',660,'WAREHOUSE','Warehouse','Warehouse','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_BACK_ORDER_INV_V','','');
--Inserting Report Parameters - Back Ordered Inventory Movement Report
xxeis.eis_rs_ins.rp( 'Back Ordered Inventory Movement Report',660,'Date From','Date From','','>=','','','DATE','Y','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Back Ordered Inventory Movement Report',660,'Date To','Date To','','<=','','','DATE','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Back Ordered Inventory Movement Report',660,'Warehouse','Warehouse','WAREHOUSE','IN','OM WAREHOUSE','','VARCHAR2','Y','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Back Ordered Inventory Movement Report',660,'Subinventory','Subinventory','SUB_INV','IN','OM SUB INVENTORY','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Back Ordered Inventory Movement Report
xxeis.eis_rs_ins.rcn( 'Back Ordered Inventory Movement Report',660,'WAREHOUSE','IN',':Warehouse','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Back Ordered Inventory Movement Report',660,'SUB_INV','IN',':Subinventory','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Back Ordered Inventory Movement Report
xxeis.eis_rs_ins.rs( 'Back Ordered Inventory Movement Report',660,'ORDER_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Back Ordered Inventory Movement Report
xxeis.eis_rs_ins.rt( 'Back Ordered Inventory Movement Report',660,'begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_from(:Date From);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_to(:Date To);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Back Ordered Inventory Movement Report
--Inserting Report Portals - Back Ordered Inventory Movement Report
--Inserting Report Dashboards - Back Ordered Inventory Movement Report
--Inserting Report Security - Back Ordered Inventory Movement Report
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','701','','50546',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','','SO004816','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Back Ordered Inventory Movement Report','512','','50850',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Lines Disposition Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_LINE_DISP_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_LINE_DISP_V
Create or replace View XXEIS.EIS_XXWC_OM_LINE_DISP_V
(ITEM_NUMBER,ITEM_DESCRIPTION,VELOCITY,BRANCH,PRICING_ZONE,QTY_OF_ITEMS_CANCELLED,QTY_OF_ITEMS_DISPOSITIONED,TIMES_LINE_CANCELLED,TIMES_LINE_DISPOSITIONED,MIN,MAX,TOTAL_SALES,BEGIN_INVENTORY,END_INVENTORY,COGS_AMOUNT,REORDER_POINT,ORGANIZATION_ID,INVENTORY_ITEM_ID) AS 
SELECT Item_Number,
    ITEM_DESCRIPTION,
    velocity,
    Branch,
    Pricing_Zone,
    SUM(Qty_Of_Items_Cancelled) Qty_Of_Items_Cancelled,
    SUM(Qty_Of_Items_Dispositioned) Qty_Of_Items_Dispositioned,
    SUM(Times_Line_Cancelled) Times_Line_Cancelled,
    SUM(Times_Line_Dispositioned) Times_Line_Dispositioned,
    MIN,
    MAX,
    SUM(TOTAL_SALES),
    Begin_Inventory,
    End_Inventory,
    Cogs_Amount,
    REORDER_POINT,
    ORGANIZATION_ID,
    inventory_item_id
    --    ,ORDER_NUMBER
    --  ,header_branch
  FROM
    (SELECT MSI.SEGMENT1 ITEM_NUMBER,
      msi.description item_description,
      ood.organization_code branch,
      oodl.organization_code line_warehouse,
      MP.ATTRIBUTE6 PRICING_ZONE,
      --(DECODE(OL.FLOW_STATUS_CODE ,'CANCELLED',OL.ORDERED_QUANTITY,0)) QTY_OF_ITEMS_CANCELLED,
      NVL(ol.cancelled_quantity,0) QTY_OF_ITEMS_CANCELLED,
      (DECODE(Ol.Flow_Status_Code ,'CANCELLED',0,DECODE(Ol.Ship_From_Org_Id , Oh.Ship_From_Org_Id,0,Ol.Ordered_Quantity))) Qty_Of_Items_Dispositioned,
      (DECODE(Ol.Flow_Status_Code ,'CANCELLED',1,0)) Times_Line_Cancelled,
      (DECODE(OL.FLOW_STATUS_CODE ,'CANCELLED',0,DECODE(OL.SHIP_FROM_ORG_ID , OH.SHIP_FROM_ORG_ID,0,1))) TIMES_LINE_DISPOSITIONED,
      XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_TOT_SALES_DLR(OL.LINE_ID) TOTAL_SALES,
      --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_TOT_PRIZINGZONE_SALES_DLR(ol.line_id,mp.attribute6) TOTAL_PRICE_SALES,
      xxeis.eis_rs_xxwc_com_util_pkg.Get_inv_vel_cat_class(msi.inventory_item_id,msi.organization_id) velocity,
      min_minmax_quantity MIN,
      Max_Minmax_Quantity MAX,
      xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_qty(msi.inventory_item_id,msi.organization_id,xxeis.eis_rs_xxwc_com_util_pkg.GET_DATE_FROM) BEGIN_INVENTORY,
      xxeis.eis_rs_xxwc_com_util_pkg.GET_ONHAND_qty(msi.inventory_item_id,msi.organization_id,xxeis.eis_rs_xxwc_com_util_pkg.GET_DATE_to) END_INVENTORY,
      Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Cogs_Amt(Msi.Inventory_Item_Id,Msi.Organization_Id,Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Date_From,Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Date_To) Cogs_Amount,
      -- Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.get_reason_code(ol.header_id,ol.line_id) reason_code,
      MIN_MINMAX_QUANTITY REORDER_POINT,
      mp.organization_code header_branch,
      ---Primary Keys
      msi.inventory_item_id ,
      MSI.ORGANIZATION_ID
      --      ,oh.order_number
      --hca.cust_account_id ,
      --hzp.party_id
      --descr#flexfield#start
      --descr#flexfield#end
      --gl#accountff#start
      --gl#accountff#end
    FROM mtl_system_items_kfv msi,
      Org_Organization_Definitions Ood,
      Oe_Order_Headers Oh,
      OE_ORDER_LINES OL,
      OE_ORDER_TYPES_V ot,
      Hz_Cust_Accounts Hca,
      Hz_Parties Hzp,
      mtl_parameters mp,
      Org_Organization_Definitions Oodl
    WHERE 1                   =1
    AND msi.organization_id   = ood.organization_id
    AND msi.organization_id   = Oh.ship_from_org_id
    AND msi.inventory_item_id = ol.inventory_item_id
    AND OL.HEADER_ID          = OH.HEADER_ID
    AND OH.ORDER_TYPE_ID      =OT.ORDER_TYPE_ID
    AND ot.name NOT          IN('REPAIR ORDER')
    AND oh.sold_to_org_id     = hca.cust_account_id(+)
    AND hca.party_id          = hzp.party_id
    AND mp.organization_id    = ood.organization_id
    AND Oodl.organization_id  =ol.Ship_From_Org_Id
      -- and mp.organization_id       = Oh.Ship_From_Org_Id
    AND ( ( Ol.Ship_From_Org_Id <> Oh.Ship_From_Org_Id
    AND ol.flow_status_code     IN ('CANCELLED','PRE-BILLING_ACCEPTANCE','CLOSED'))
    OR (ol.flow_status_code     IN ('CANCELLED')) )
      --AND ol.invoice_interface_status_code='YES'
      /* and exists (
      select 1 from OE_REASONS OER
      where OER.LINE_ID=OL.LINE_ID
      and reason_code in ('Discontinued Item',
      'Price',
      'Product Preference',
      'Supplier Preference',
      'Timing'    ) )*/
      -- To Exclude Repair and Rental Items
    AND NOT EXISTS
      (SELECT 1
      FROM OE_TRANSACTION_TYPES_VL OTT,
        MTL_ITEM_CATEGORIES_V MIC,
        MTL_CATEGORY_SETS MDCS
      WHERE OTT.TRANSACTION_TYPE_ID = OH.ORDER_TYPE_ID
      AND MSI.ORGANIZATION_ID       = MIC.ORGANIZATION_ID
      AND MSI.INVENTORY_ITEM_ID     = MIC.INVENTORY_ITEM_ID
      AND MIC.CATEGORY_SET_ID       = MDCS.CATEGORY_SET_ID
      AND MDCS.CATEGORY_SET_NAME    = 'Inventory Category'
      AND MIC.CATEGORY_CONCAT_SEGS IN ('99.99RT', '99.99RR')
      AND (UPPER(OTT.name)         IN('WC LONG TERM RENTAL','WC SHORT TERM RENTAL')
        -- OR UPPER(MSI.SEGMENT1) LIKE 'R%'
        )
      )
    AND TRUNC(ORDERED_DATE)>=XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM
    AND TRUNC(Ordered_Date)<=Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Date_To
      -- And Msi.Segment1='10112187'
    )
  GROUP BY ITEM_NUMBER,
    inventory_item_id,
    ITEM_DESCRIPTION,
    VELOCITY,
    BRANCH,
    ORGANIZATION_ID,
    PRICING_ZONE,
    MIN,
    MAX,
    BEGIN_INVENTORY,
    END_INVENTORY,
    COGS_AMOUNT,
    REORDER_POINT
/
set scan on define on
prompt Creating View Data for Lines Disposition Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_LINE_DISP_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Line Disp V','EXOLDV');
--Delete View Columns for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_LINE_DISP_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','MAX',660,'Max','MAX','','','','XXEIS_RS_ADMIN','NUMBER','','','Max');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','MIN',660,'Min','MIN','','','','XXEIS_RS_ADMIN','NUMBER','','','Min');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','TOTAL_SALES',660,'Total Sales','TOTAL_SALES','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Total Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','TIMES_LINE_DISPOSITIONED',660,'Times Line Dispositioned','TIMES_LINE_DISPOSITIONED','','','','XXEIS_RS_ADMIN','NUMBER','','','Times Line Dispositioned');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','TIMES_LINE_CANCELLED',660,'Times Line Cancelled','TIMES_LINE_CANCELLED','','','','XXEIS_RS_ADMIN','NUMBER','','','Times Line Cancelled');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','QTY_OF_ITEMS_DISPOSITIONED',660,'Qty Of Items Dispositioned','QTY_OF_ITEMS_DISPOSITIONED','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty Of Items Dispositioned');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','QTY_OF_ITEMS_CANCELLED',660,'Qty Of Items Cancelled','QTY_OF_ITEMS_CANCELLED','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty Of Items Cancelled');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','BRANCH',660,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','ITEM_NUMBER',660,'Item Number','ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','REORDER_POINT',660,'Reorder Point','REORDER_POINT','','','','XXEIS_RS_ADMIN','NUMBER','','','Reorder Point');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','PRICING_ZONE',660,'Pricing Zone','PRICING_ZONE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pricing Zone');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','VELOCITY',660,'Velocity','VELOCITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Velocity');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','BEGIN_INVENTORY',660,'Begin Inventory','BEGIN_INVENTORY','','','','XXEIS_RS_ADMIN','NUMBER','','','Begin Inventory');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','COGS_AMOUNT',660,'Cogs Amount','COGS_AMOUNT','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Cogs Amount');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','END_INVENTORY',660,'End Inventory','END_INVENTORY','','','','XXEIS_RS_ADMIN','NUMBER','','','End Inventory');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_LINE_DISP_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
--Inserting View Components for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','MTL_SYSTEM_ITEMS_KFV',660,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','OE_ORDER_LINES',660,'OE_ORDER_LINES_ALL','OL','OL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Oe Order Lines All Stores Information For All Orde','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','HZ_CUST_ACCOUNTS',660,'HZ_CUST_ACCOUNTS','HCA','HCA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_OM_LINE_DISP_V','HZ_PARTIES',660,'HZ_PARTIES','HZP','HZP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','');
--Inserting View Component Joins for EIS_XXWC_OM_LINE_DISP_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOLDV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','MTL_SYSTEM_ITEMS_KFV','MSI',660,'EXOLDV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','OE_ORDER_LINES','OL',660,'EXOLDV.LINE_ID','=','OL.LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','HZ_CUST_ACCOUNTS','HCA',660,'EXOLDV.CUST_ACCOUNT_ID','=','HCA.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_OM_LINE_DISP_V','HZ_PARTIES','HZP',660,'EXOLDV.PARTY_ID','=','HZP.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for Lines Disposition Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Lines Disposition Report
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )','','OM WAREHOUSE','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT MSI.SEGMENT1   ITEM_NUMBER,
MSI.DESCRIPTION  ITEM_DESCRIPTION from mtl_system_items_b msi','','OM Item Number LOV','Order Item numbers','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct attribute6 pricing_zone from mtl_parameters','','OM Pricing Zone','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select DESCRIPTION ITEM_DESCRIPTION from MTL_SYSTEM_ITEMS_KFV
where exists (Select 1 from XXEIS.EIS_ORG_ACCESS_V where organization_id = organization_id)

','','OM ITEM DESCRIPTION','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Lines Disposition Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Lines Disposition Report
xxeis.eis_rs_utility.delete_report_rows( 'Lines Disposition Report' );
--Inserting Report - Lines Disposition Report
xxeis.eis_rs_ins.r( 660,'Lines Disposition Report','','The purpose of this report is to list items that have been line dispositioned (line item fulfilled in a different branch) or cancelled.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_LINE_DISP_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Lines Disposition Report
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'ITEM_NUMBER','Item Number','Item Number','','','','','1','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'MAX','Max','Max','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'MIN','Min','Min','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'QTY_OF_ITEMS_CANCELLED','Qty Of Items Cancelled','Qty Of Items Cancelled','','','','','6','N','','DATA_FIELD','','SUM','','4','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'QTY_OF_ITEMS_DISPOSITIONED','Qty Of Items Dispositioned','Qty Of Items Dispositioned','','','','','4','N','','DATA_FIELD','','SUM','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TIMES_LINE_CANCELLED','Times Line Cancelled','Times Line Cancelled','','','','','5','N','','DATA_FIELD','','SUM','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TIMES_LINE_DISPOSITIONED','Times Line Dispositioned','Times Line Dispositioned','','','','','3','N','','DATA_FIELD','','SUM','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TOTAL_SALES','Total Location Sales','Total Sales','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TOTAL_TIMES_LINE_CANC_OR_DISP','Total Times Line Canc or Disp','Total Sales','NUMBER','','','','7','Y','','','','','','','(EXOLDV.TIMES_LINE_CANCELLED + EXOLDV.TIMES_LINE_DISPOSITIONED)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TOTAL_QTY_LINE_CANC_OR_DISP','Total Qty Line canc or Disp','Total Sales','NUMBER','','','','8','Y','','','','','','','(EXOLDV.QTY_OF_ITEMS_CANCELLED + EXOLDV.QTY_OF_ITEMS_DISPOSITIONED)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'REORDER_POINT','Reorder Point','Reorder Point','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'VELOCITY','Velocity','Velocity','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
xxeis.eis_rs_ins.rc( 'Lines Disposition Report',660,'TOTAL_PRICING_ZONE_SALES','Pricing Zone Sales','Velocity','NUMBER','~~2','','','10','Y','','','','','','','xxeis.EIS_RS_XXWC_COM_UTIL_PKG.GET_TOT_PRIZINGZONE_SALES_DLR(EXOLDV.INVENTORY_ITEM_ID,EXOLDV.ORGANIZATION_ID,EXOLDV.PRICING_ZONE)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_LINE_DISP_V','','');
--Inserting Report Parameters - Lines Disposition Report
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Location','Location','BRANCH','IN','OM WAREHOUSE','','VARCHAR2','Y','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Item Number','Item Number','ITEM_NUMBER','IN','OM Item Number LOV','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Pricing Zone','Pricing Zone','PRICING_ZONE','IN','OM Pricing Zone','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Item Description','Item Description','ITEM_DESCRIPTION','IN','OM ITEM DESCRIPTION','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Ordered Date From','Ordered Date From','','IN','','','DATE','Y','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Lines Disposition Report',660,'Ordered Date To','Ordered Date To','','IN','','','DATE','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
--Inserting Report Conditions - Lines Disposition Report
xxeis.eis_rs_ins.rcn( 'Lines Disposition Report',660,'ITEM_NUMBER','IN',':Item Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Lines Disposition Report',660,'BRANCH','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Lines Disposition Report',660,'PRICING_ZONE','IN',':Pricing Zone','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Lines Disposition Report',660,'ITEM_DESCRIPTION','IN',':Item Description','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Lines Disposition Report',660,'','','','','/*and EXOLDV.reason_code in (''Discontinued Item'', 
                         ''Price'', 
                         ''Product Preference'', 
                         ''Supplier Preference'', 
                         ''Timing''
                         )*/','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Lines Disposition Report
xxeis.eis_rs_ins.rs( 'Lines Disposition Report',660,'ITEM_NUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Lines Disposition Report
xxeis.eis_rs_ins.rt( 'Lines Disposition Report',660,'Begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_from(:Ordered Date From);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_date_to(:Ordered Date To);

end;
','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Lines Disposition Report
--Inserting Report Portals - Lines Disposition Report
--Inserting Report Dashboards - Lines Disposition Report
--Inserting Report Security - Lines Disposition Report
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50926',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50927',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50928',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50929',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50931',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50930',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','701','','50546',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Lines Disposition Report','','SO004816','',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Manual Price Changes on Sales Orders Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_MAN_PRICE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_MAN_PRICE_V
Create or replace View XXEIS.EIS_XXWC_OM_MAN_PRICE_V
(PRICING_ZONE,REGION,BRANCH,CUSTOMER,CUSTOMER_NUMBER,CREATED_BY,ORDER_NUMBER,ITEM,ITEM_DESCRIPTION,CAT_CLASS,CAT_CLASS_DESC,QUANTITY,LIST_PRICE,ORIGINAL_UNIT_SELLING_PRICE,FINAL_SELLING_PRICE,REASON_CODE,APPLICATION_METHOD,ORDERED_DATE,AVERAGE_COST,HEADER_ID,LINE_ID,LIST_LINE_TYPE_CODE,INVENTORY_ITEM_ID,MSI_ORGANIZATION_ID,ORGANIZATION_ID,CUST_ACCOUNT_ID) AS 
select MP.ATTRIBUTE6 PRICING_ZONE,
    mp.ATTRIBUTE9 REGION,
    ood.organization_code branch,
    Hca.Account_Name Customer,
    Hca.Account_Number Customer_Number,
    PPF.FULL_NAME CREATED_BY,
    OH.ORDER_NUMBER ORDER_NUMBER,
   /* (ol.line_number
    ||'.'
    ||ol.shipment_number) line_number,*/
    Msi.Segment1 Item,
    Msi.Description Item_Description,
    xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat_class,
    xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc(msi.inventory_item_id,msi.organization_id) cat_class_desc,
    ol.ordered_quantity quantity,
    Ol.Unit_List_Price List_Price,
    (ol.unit_list_price-xxeis.EIS_RS_XXWC_COM_UTIL_PKG.Get_Adj_Auto_Modifier_Amt(ol.header_id,ol.line_id)) original_unit_selling_price,
    ol.unit_selling_price final_selling_price,
    oel.meaning Reason_Code,
    apps.qp_qp_form_pricing_attr.get_meaning(adj_man.arithmetic_operator, 'ARITHMETIC_OPERATOR') application_method,
    TRUNC(oh.ordered_date) ordered_date,
    nvl(ol.unit_cost,0) average_cost,
    --(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0)) average_cost,
    ---Primary Keys
    oh.header_id,
    Ol.Line_Id,
    Adj_Man.List_Line_Type_Code,
    Msi.Inventory_Item_Id,
    Msi.Organization_Id Msi_Organization_Id,
    Ood.Organization_Id,
    Hca.Cust_Account_Id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_system_items_kfv msi,
    mtl_parameters mp,
    org_organization_definitions ood,
    hz_cust_accounts hca,
    Oe_Price_Adjustments_V Adj_Man,
    --  oe_price_adjustments_v adj_auto,
    PER_PEOPLE_F PPF,
    FND_USER FU,    
    oe_lookups oel
  WHERE 1                  = 1--oh.order_number  ='10001157'
  AND ol.ship_from_org_id  = ood.organization_id
  AND oh.sold_to_org_id    = hca.cust_account_id
  AND oh.header_id         = ol.header_id
  AND Ol.Ship_From_Org_Id  = Msi.Organization_Id
  AND ol.inventory_item_id = msi.inventory_item_id
  AND Msi.Organization_Id  = Mp.Organization_Id
  AND Ol.Header_Id         = Adj_Man.Header_Id
  AND Ol.Line_Id           = Adj_Man.Line_Id
    -- AND Ol.Header_Id                    = NVL(adj_Auto.Header_Id,Ol.Header_Id)
    -- AND Ol.Line_Id                      = NVL(Adj_Auto.Line_Id,Ol.Line_Id)
    -- AND NVL(adj_auto.automatic_flag,'Y')='Y'
  --AND Ol.Created_By = Ppf.Person_Id
  AND FU.USER_ID          =Ol.CREATED_BY
  AND FU.EMPLOYEE_ID      =PPF.PERSON_ID(+)      
  AND TRUNC (OL.CREATION_DATE) BETWEEN NVL (PPF.EFFECTIVE_START_DATE, TRUNC (OL.CREATION_DATE) ) AND NVL (PPF.EFFECTIVE_END_DATE, TRUNC (OL.CREATION_DATE) )
    --AND oh.order_number='10001303'
  --AND ( adj_man.adjustment_name         IN ('Line Discount Amount','NEW PRICE_LINE_DISC','LINE DISCOUNT PERCENT'))
  AND upper(adj_man.adjustment_name) IN ('AMOUNT_LINE_DISCOUNT','NEW PRICE','PERCENT_LINE_DISCOUNT')
  AND OEL.LOOKUP_CODE(+)                 =ADJ_MAN.CHANGE_REASON_CODE
  and NVL(OEL.LOOKUP_TYPE,'CHANGE_CODE') ='CHANGE_CODE'
/
set scan on define on
prompt Creating View Data for Manual Price Changes on Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_MAN_PRICE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_MAN_PRICE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Man Price V','EXOMPV');
--Delete View Columns for EIS_XXWC_OM_MAN_PRICE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_MAN_PRICE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_MAN_PRICE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','FINAL_SELLING_PRICE',660,'Final Selling Price','FINAL_SELLING_PRICE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Final Selling Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','LIST_PRICE',660,'List Price','LIST_PRICE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','List Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','QUANTITY',660,'Quantity','QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Quantity');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ITEM',660,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CUSTOMER',660,'Customer','CUSTOMER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','BRANCH',660,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','PRICING_ZONE',660,'Pricing Zone','PRICING_ZONE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pricing Zone');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','APPLICATION_METHOD',660,'Application Method','APPLICATION_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Application Method');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ORIGINAL_UNIT_SELLING_PRICE',660,'Original Unit Selling Price','ORIGINAL_UNIT_SELLING_PRICE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Original Unit Selling Price');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','REASON_CODE',660,'Reason Code','REASON_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reason Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CAT_CLASS',660,'Cat Class','CAT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CAT_CLASS_DESC',660,'Cat Class Desc','CAT_CLASS_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class Desc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','LIST_LINE_TYPE_CODE',660,'List Line Type Code','LIST_LINE_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','List Line Type Code');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Average Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msi Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','REGION',660,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region');
--Inserting View Components for EIS_XXWC_OM_MAN_PRICE_V
--Inserting View Component Joins for EIS_XXWC_OM_MAN_PRICE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Manual Price Changes on Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct attribute6 pricing_zone from mtl_parameters','','OM Pricing Zone','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'  Select Mcv.segment2 ,concatenated_segments,mcv.Description  
  From Mtl_Categories_Kfv Mcv,
       Mtl_Category_Sets Mcs
  Where  Mcs.Structure_Id                 =Mcv.Structure_Id
  AND Mcs.Category_Set_Name            =''Inventory Category''','','OM CAT CLASS','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ppf.full_name  created_by, fu.user_name user_name
  FROM FND_USER fu, per_people_f ppf
 WHERE TRUNC(SYSDATE) BETWEEN fu.START_DATE AND NVL(fu.end_date, hr_general.end_of_time)
   and TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
   and fu.employee_id = ppf.person_id (+)
   and exists
          (select 1
             from fnd_user_resp_groups furg,
                  fnd_responsibility_vl fr
            where furg.responsibility_id   = fr.responsibility_id
              and fu.user_id = furg.user_id
              and fr.responsibility_name like ''XXEIS%'')','','Created By Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT Flex_Val.description 
from  Mtl_Categories_Kfv Mcv,
    MTL_CATEGORY_SETS MCS,
    Mtl_Item_Categories Mic,
     Fnd_Flex_Values_Vl Flex_Val
    Where   Mcs.Category_Set_Name      =''Inventory Category''
  And Mcs.Structure_Id                 = Mcv.Structure_Id
  AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
  And Mic.Category_Id                  = Mcv.Category_Id 
  And Flex_Value                       = Mcv.SEGMENT2
  and flex_value_Set_id=1015058','','OM CAT CLASS DESC','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct organization_code org_code,organization_name org_name from org_organization_definitions','','Branch Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Manual Price Changes on Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_utility.delete_report_rows( 'Manual Price Changes on Sales Orders Report' );
--Inserting Report - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.r( 660,'Manual Price Changes on Sales Orders Report','','Provides visibility to order lines where the unit selling price was manually overridden.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_MAN_PRICE_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'BRANCH','Branch','Branch','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CREATED_BY','Created By','Created By','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER','Customer Name','Customer','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'FINAL_SELLING_PRICE','Final Selling Price','Final Selling Price','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ITEM','Item','Item','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'LIST_PRICE','List Price','List Price','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'PRICING_ZONE','Pricing Zone','Pricing Zone','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'QUANTITY','Quantity','Quantity','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'APPLICATION_METHOD','Application Method','Application Method','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ORIGINAL_UNIT_SELLING_PRICE','Original Unit Selling Price','Original Unit Selling Price','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'REASON_CODE','Reason Code','Reason Code','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ORIGINAL GM%','Original GM%','Reason Code','NUMBER','','','','15','Y','','','','','','','case when EXOMPV.ORIGINAL_UNIT_SELLING_PRICE > 0 then ((((EXOMPV.ORIGINAL_UNIT_SELLING_PRICE-EXOMPV.AVERAGE_COST))/(EXOMPV.ORIGINAL_UNIT_SELLING_PRICE))*100) else 0 end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'FINAL SELLING PRICE GM%','Final Selling Price GM%','Reason Code','NUMBER','','','','17','Y','','','','','','','case when EXOMPV.FINAL_SELLING_PRICE>0 then ((((EXOMPV.FINAL_SELLING_PRICE-EXOMPV.AVERAGE_COST))/(EXOMPV.FINAL_SELLING_PRICE))*100) else 0 end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'DOLLARS LOST','Dollars Lost','Reason Code','NUMBER','','','','19','Y','','','','','','','(EXOMPV.ORIGINAL_UNIT_SELLING_PRICE-EXOMPV.FINAL_SELLING_PRICE)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'GM% LOST','GM% Lost','Reason Code','NUMBER','','','','20','Y','','','','','','','(case when EXOMPV.ORIGINAL_UNIT_SELLING_PRICE > 0 then ((((EXOMPV.ORIGINAL_UNIT_SELLING_PRICE-EXOMPV.AVERAGE_COST))/(EXOMPV.ORIGINAL_UNIT_SELLING_PRICE))*100) else 0 end -case when EXOMPV.FINAL_SELLING_PRICE>0 then ((((EXOMPV.FINAL_SELLING_PRICE-EXOMPV.AVERAGE_COST))/(EXOMPV.FINAL_SELLING_PRICE))*100) else 0 end)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CAT_CLASS','Cat Class','Cat Class','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CAT_CLASS_DESC','Cat Class Desc','Cat Class Desc','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'REGION','Region','Region','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
--Inserting Report Parameters - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Pricing Zone','Pricing Zone','PRICING_ZONE','IN','OM Pricing Zone','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Customer Name','Customer Name','CUSTOMER','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'From Date','From Date','ORDERED_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'To Date','To Date','ORDERED_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Created By','Created By','CREATED_BY','IN','Created By Lov','','VARCHAR2','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Cat Class','Cat Class','CAT_CLASS','IN','OM CAT CLASS','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'cat Class Desc','cat Class Desc','CAT_CLASS_DESC','IN','OM CAT CLASS DESC','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Branch','Branch','BRANCH','IN','Branch Lov','','VARCHAR2','N','Y','9','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','10','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'PRICING_ZONE','IN',':Pricing Zone','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER','IN',':Customer Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'ORDERED_DATE','>=',':From Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'ORDERED_DATE','<=',':To Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CREATED_BY','IN',':Created By','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CAT_CLASS','IN',':Cat Class','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CAT_CLASS_DESC','IN',':cat Class Desc','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'BRANCH','IN',':Branch','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'REGION','IN',':Region','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rs( 'Manual Price Changes on Sales Orders Report',660,'PRICING_ZONE','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Manual Price Changes on Sales Orders Report',660,'CREATED_BY','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Manual Price Changes on Sales Orders Report
--Inserting Report Templates - Manual Price Changes on Sales Orders Report
--Inserting Report Portals - Manual Price Changes on Sales Orders Report
--Inserting Report Dashboards - Manual Price Changes on Sales Orders Report
--Inserting Report Security - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50926',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50927',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50928',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50929',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50931',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50930',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','21623',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','701','','50546',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50861',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','SO004816','',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on


--Report Name            : White Cap End of Month Commissions Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_TBD_OUTBND_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_TBD_OUTBND_V
Create or replace View XXEIS.EIS_XXWC_AR_TBD_OUTBND_V
(BUSINESSDATE,REPORT_TYPE,ORDER_NUMBER,INVOICENUMBER,LINENO,DESCRIPTION,QTY,UNITPRICE,EXTSALE,MASTERNUMBER,MASTERNAME,JOBNUMBER,CUSTOMERNAME,SALESREPNUMBER,SALESREPNAME,LOC,MP_ORGANIZATION_ID,REGIONNAME,PARTNO,CATCLASS,INVOICE_SOURCE,SHIPPED_QUANTITY,DIRECTFLAG,AVERAGECOST,TRADER,SPECIAL_COST,OPERATING_UNIT,ORG_ID,INVENTORY_ITEM_ID,ORGANIZATION_ID,CUSTOMER_TRX_ID,PARTY_ID,PERIOD_NAME,MFGADJUST) AS 
SELECT GD.GL_DATE BUSINESSDATE,
    CASE
      WHEN (mic.CATEGORY_CONCAT_SEGS IN ('99.99RT', '99.99RR')
      AND (OEH.NAME                  IN('WC LONG TERM RENTAL','WC SHORT TERM RENTAL')))
        --OR upper(MSI.segment1) LIKE 'R%')
      THEN 'Rental'
      WHEN mic.SEGMENT1 IN ('BB', 'BS', 'DC', 'EC', 'EP', 'FA', 'FC', 'JS', 'ME','ML',' MR', 'NS', 'PS', 'RE', 'RL', 'RP', 'SA','SC','SH','TH')
      THEN 'Intangibles'
      WHEN mc.SEGMENT1 IN ('GC', 'MD', 'PC', 'PD', 'PR', 'RR','RV','ST','XX')
      THEN 'Non Comm.'
      WHEN (mic.category_concat_segs IN ('FB.MODL', 'FB.SUBA','FB.OPTC','RF.OSPI','RF.SERV'))
      OR mic.category_concat_segs    IN('60.6021','60.6022','60.6035')
      THEN 'FabRebar'
      WHEN ((OL.ATTRIBUTE3 ='Y'
      AND oel.name LIKE '%BILL%ONLY%')
      OR (OL.ATTRIBUTE15 ='Y'
      AND OEL.NAME LIKE '%BILL%ONLY%') )
      THEN 'Blow and Expired'
      ELSE 'Product'
    END report_type,
    oh.order_number order_number,
    CT.TRX_NUMBER INVOICENUMBER,
    CTL.LINE_NUMBER LINENO,
    ctl.description description,
    NVL(CTL.QUANTITY_ORDERED,0) QTY,
    NVL(ctl.unit_selling_price,0) unitprice,
    DECODE(NVL(ctl.unit_selling_price,0),0,0,(NVL(CTL.QUANTITY_ORDERED,0)*(NVL(ctl.unit_selling_price,0)+ xxeis.eis_rs_xxwc_com_util_pkg.Get_MfgAdjust(ol.header_id,ol.line_id)))) ExtSale,
    HCA.ACCOUNT_NUMBER MASTERNUMBER,
    HCA.ACCOUNT_NAME MASTERNAME,
    HPS.PARTY_SITE_NUMBER JOBNUMBER,
    HCSU.LOCATION CUSTOMERNAME,
    JRS.SALESREP_NUMBER SALESREPNUMBER,
    JRSE.SOURCE_NAME SALESREPNAME,
    --RA.SALESREP_NUMBER SALESREPNUMBER,
    --RA.NAME SALESREPNAME,
    mp.organization_code loc,
    MP.ORGANIZATION_ID MP_ORGANIZATION_ID,
    MP.ATTRIBUTE9 REGIONNAME,
    MSI.SEGMENT1 PARTNO,
    mc.SEGMENT2 CATCLASS,
    BS.NAME INVOICE_SOURCE,
    NVL(ol.shipped_quantity,0) SHIPPED_QUANTITY,
    DECODE(Ol.Source_Type_Code,'INTERNAL',0,'EXTERNAL',1,Ol.Source_Type_Code) Directflag,
    --NVL(APPS.CST_COST_API.GET_ITEM_COST(1,MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID),0) AVERAGECOST,
    /*  CASE
    WHEN Mic.Segment1      IN ('BB', 'BS', 'DC', 'EC', 'EP', 'FA', 'FC', 'JS', 'ME',' ML',' MR', 'NS', 'PS', 'RE', 'RL', 'RP', 'SA','SC','SH','TH')
    AND Upper(Mic.Segment2) ='EC10'
    THEN to_number(Ol.Attribute8)
    WHEN NVL(ctl.unit_selling_price,0) = 0
    THEN 0
    ELSE NVL(Apps.Cst_Cost_Api.Get_Item_Cost(1,Msi.Inventory_Item_Id,Msi.Organization_Id),0)
    end averagecost,*/
    CASE
      WHEN Mic.Segment1      IN ('BB', 'BS', 'DC', 'EC', 'EP', 'FA', 'FC', 'JS', 'ME',' ML',' MR', 'NS', 'PS', 'RE', 'RL', 'RP', 'SA','SC','SH','TH')
      AND Upper(Mic.Segment2) ='EC10'
      THEN to_number(Ol.Attribute8)
      ELSE DECODE(NVL(ol.unit_cost,0),0,nvl(xxeis.eis_rs_xxwc_com_util_pkg.get_average_cost(OL.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID,ol.fulfillment_date, ol.line_id),0),ol.unit_cost)
    END AVERAGECOST,
    DECODE(xxeis.eis_rs_xxwc_com_util_pkg.Get_trader(ol.header_id,ol.line_id),0,'-','Trader') Trader,
    NVL(ROUND(xxeis.eis_rs_xxwc_com_util_pkg.Get_Special_cost(ol.header_id,ol.line_id,ol.inventory_item_id),2),0) SPECIAL_COST,
    HOU.NAME OPERATING_UNIT,
    HOU.ORGANIZATION_ID org_id,
    msi.inventory_item_id,
    MSI.ORGANIZATION_ID,
    CT.CUSTOMER_TRX_ID,
    HP.PARTY_ID,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PERIOD_NAME(GD.SET_OF_BOOKS_ID,GD.GL_DATE)PERIOD_NAME,
    xxeis.eis_rs_xxwc_com_util_pkg.Get_MfgAdjust(ol.header_id,ol.line_id) MfgAdjust
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM RA_CUSTOMER_TRX CT,
    RA_CUSTOMER_TRX_LINES CTL,
    RA_CUST_TRX_LINE_GL_DIST GD,
    HZ_PARTIES HP,
    HZ_CUST_ACCOUNTS HCA,
    HZ_PARTY_SITES HPS,
    hz_cust_acct_sites hcas,
    HZ_CUST_SITE_USES HCSU,
    JTF_RS_SALESREPS JRS,
    JTF_RS_RESOURCE_EXTNS JRSE,
    -- RA_SALESREPS_ALL RA,
    MTL_PARAMETERS MP,
    MTL_SYSTEM_ITEMS_KFV MSI,
    MTL_ITEM_CATEGORIES_V mic,
    mtl_categories_v mc,
    OE_ORDER_LINES OL,
    OE_ORDER_HEADERS OH,
    HR_OPERATING_UNITS HOU,
    MTL_CATEGORY_SETS MDCS,
    RA_BATCH_SOURCES BS,
    OE_TRANSACTION_TYPES_VL OEL,
    OE_TRANSACTION_TYPES_VL OEH
  WHERE 1                    = 1
  AND CT.CUSTOMER_TRX_ID     = CTL.CUSTOMER_TRX_ID
  AND CT.CUSTOMER_TRX_ID     = GD.CUSTOMER_TRX_ID
  AND GD.ACCOUNT_CLASS       = 'REC'
  AND CT.BILL_TO_CUSTOMER_ID = HCA.CUST_ACCOUNT_ID
  AND HCA.PARTY_ID           = HP.PARTY_ID(+)
  AND HP.PARTY_ID            = HPS.PARTY_ID
  AND HCAS.PARTY_SITE_ID     = HPS.PARTY_SITE_ID
  AND HCSU.CUST_ACCT_SITE_ID = HCAS.CUST_ACCT_SITE_ID
  AND HCSU.site_use_id       = ct.bill_to_site_use_id
  AND HCA.CUST_ACCOUNT_ID    = HCAS.CUST_ACCOUNT_ID
    -- AND Oh.Salesrep_Id         = Ra.Salesrep_Id(+)
    -- and Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Grossmgr_Rpt_Sale_Rep_Flag(RA.Salesrep_Number)='Y'
  AND CT.PRIMARY_SALESREP_ID     = JRS.SALESREP_ID
  AND CT.ORG_ID                  = JRS.ORG_ID
  AND JRS.RESOURCE_ID            = JRSE.RESOURCE_ID
  AND CTL.INTERFACE_LINE_CONTEXT = 'ORDER ENTRY'
  AND TO_CHAR(oh.order_number)   = CT.interface_header_attribute1
  AND TO_CHAR(OL.LINE_ID)        = CTL.INTERFACE_LINE_ATTRIBUTE6
  AND OH.HEADER_ID               = OL.HEADER_ID
  AND MSI.INVENTORY_ITEM_ID      = OL.INVENTORY_ITEM_ID
  AND MSI.ORGANIZATION_ID        = OL.SHIP_FROM_ORG_ID
  AND mic.category_id            = mc.category_id
  AND MSI.ORGANIZATION_ID        = mic.ORGANIZATION_ID
  AND MSI.INVENTORY_ITEM_ID      = mic.INVENTORY_ITEM_ID
  AND CT.ORG_ID                  = HOU.ORGANIZATION_ID
  AND msi.ORGANIZATION_ID        = MP.ORGANIZATION_ID
  AND mic.category_set_id        = mdcs.category_set_id
  AND CTL.EXTENDED_AMOUNT       <> 0
  AND MDCS.CATEGORY_SET_NAME     = 'Inventory Category'
  AND CT.BATCH_SOURCE_ID         = BS.BATCH_SOURCE_ID
  AND CT.ORG_ID                  = BS.ORG_ID
  AND MC.STRUCTURE_NAME          ='Item Categories'
  AND OEL.TRANSACTION_TYPE_ID    = OL.LINE_TYPE_ID
  AND OEH.TRANSACTION_TYPE_ID    = OH.ORDER_TYPE_ID
  AND Oeh.Org_Id                 = Oh.Org_Id
  AND oel.org_id                 = ol.org_id
/
set scan on define on
prompt Creating View Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_TBD_OUTBND_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Ar Tbd Outbound V','EXATOV');
--Delete View Columns for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_TBD_OUTBND_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','AVERAGECOST',222,'Averagecost','AVERAGECOST','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Averagecost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','DIRECTFLAG',222,'Directflag','DIRECTFLAG','','','','XXEIS_RS_ADMIN','NUMBER','','','Directflag');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CATCLASS',222,'Catclass','CATCLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Catclass');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PARTNO',222,'Partno','PARTNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Partno');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','REGIONNAME',222,'Regionname','REGIONNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Regionname');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LOC',222,'Loc','LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREPNAME',222,'Salesrepname','SALESREPNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrepname');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SALESREPNUMBER',222,'Salesrepnumber','SALESREPNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrepnumber');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CUSTOMERNAME',222,'Customername','CUSTOMERNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customername');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','JOBNUMBER',222,'Jobnumber','JOBNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Jobnumber');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MASTERNAME',222,'Mastername','MASTERNAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Mastername');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MASTERNUMBER',222,'Masternumber','MASTERNUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Masternumber');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','EXTSALE',222,'Extsale','EXTSALE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Extsale');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','UNITPRICE',222,'Unitprice','UNITPRICE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Unitprice');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','QTY',222,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','DESCRIPTION',222,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','LINENO',222,'Lineno','LINENO','','','','XXEIS_RS_ADMIN','NUMBER','','','Lineno');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVOICENUMBER',222,'Invoicenumber','INVOICENUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoicenumber');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','BUSINESSDATE',222,'Businessdate','BUSINESSDATE','','','','XXEIS_RS_ADMIN','DATE','','','Businessdate');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','TRADER',222,'Trader','TRADER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trader');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVOICE_SOURCE',222,'Invoice Source','INVOICE_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Source');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','REPORT_TYPE',222,'Report Type','REPORT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Report Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PERIOD_NAME',222,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SHIPPED_QUANTITY',222,'Shipped Quantity','SHIPPED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Shipped Quantity');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','INVENTORY_ITEM_ID',222,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','ORG_ID',222,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MP_ORGANIZATION_ID',222,'Mp Organization Id','MP_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Mp Organization Id');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','SPECIAL_COST',222,'Special Cost','SPECIAL_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Special Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_TBD_OUTBND_V','MFGADJUST',222,'Mfgadjust','MFGADJUST','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Mfgadjust');
--Inserting View Components for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','RA_CUSTOMER_TRX',222,'RA_CUSTOMER_TRX_ALL','CT','CT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Header-Level Information About Invoices, Debit Mem','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV',222,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_AR_TBD_OUTBND_V','HZ_PARTIES',222,'HZ_PARTIES','HP','HP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Party Information','','');
--Inserting View Component Joins for EIS_XXWC_AR_TBD_OUTBND_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','RA_CUSTOMER_TRX','CT',222,'EXATOV.CUSTOMER_TRX_ID','=','CT.CUSTOMER_TRX_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXATOV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXATOV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_AR_TBD_OUTBND_V','HZ_PARTIES','HP',222,'EXATOV.PARTY_ID','=','HP.PARTY_ID(+)','','','','','XXEIS_RS_ADMIN');
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.lov( 222,'select distinct per.period_name ,  per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type
order by per.period_num asc,
per.period_year desc','','AR_PERIOD_NAMES','AR_PERIOD_NAMES','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'select DISTINCT NAME from RA_BATCH_SOURCES_ALL','','AR Batch Source Name LOV','Displays Batch Sources information','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap End of Month Commissions Report
xxeis.eis_rs_utility.delete_report_rows( 'White Cap End of Month Commissions Report' );
--Inserting Report - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.r( 222,'White Cap End of Month Commissions Report','','The purpose of this report is to extract the necessary information on posted sales to enable the accurate calculation of commissions to White Cap Sales Representatives for a defined period of time (e.g. one month). Note:  The parameter GL Period to Report correlates to the range of dates that will be reported (e.g. select APR-20yy) will report all transactions within the respective GL period. 

','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_TBD_OUTBND_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'AVERAGECOST','AverageCost','Averagecost','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'BUSINESSDATE','BusinessDate','Businessdate','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'CATCLASS','CatClass','Catclass','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'CUSTOMERNAME','CustomerName','Customername','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'DESCRIPTION','Description','Description','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'DIRECTFLAG','DirectFlag','Directflag','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'INVOICENUMBER','InvoiceNumber','Invoicenumber','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'JOBNUMBER','JobNumber','Jobnumber','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'LINENO','LineNo','Lineno','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'LOC','Loc','Loc','','','','','8','N','','COL_FIELD','','','Branch Id','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'MASTERNAME','MasterName','Mastername','','','','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'MASTERNUMBER','MasterNumber','Masternumber','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'PARTNO','PartNo','Partno','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'QTY','QTY','Qty','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'REGIONNAME','RegionName','Regionname','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREPNAME','SalesRepName','Salesrepname','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREPNUMBER','SalesrepNumber','Salesrepnumber','','','','','6','N','','ROW_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'UNITPRICE','UnitPrice','Unitprice','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'TRADER','Trader','Trader','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'REPORT_TYPE','ReportTypeName','Report Type','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'EXT_AVG_COST','ExtAvgCost','Report Type','NUMBER','','','','19','Y','','','','','','','(EXATOV.AVERAGECOST * EXATOV.QTY)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GM_AVG_COST','GMAvgCost','Report Type','NUMBER','','','','21','Y','','','','','','','decode(EXATOV.ExtSale ,0,0,((EXATOV.ExtSale - (EXATOV.AVERAGECOST * EXATOV.QTY)) / EXATOV.ExtSale))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'EXT_SPECIAL_COST','ExtSpecialCost','Report Type','NUMBER','','','','22','Y','','','','','','','(EXATOV.SPECIAL_COST * EXATOV.QTY)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GPS_SPECIAL_COST','GPSpecialCost','Report Type','NUMBER','','','','23','Y','','','','','','','DECODE((EXATOV.SPECIAL_COST * EXATOV.QTY),0,0,(EXATOV.ExtSale - (EXATOV.SPECIAL_COST * EXATOV.QTY)))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GMS_SPECIAL_COST','GMSpecialCost','Report Type','NUMBER','','','','24','Y','','','','','','','DECODE((EXATOV.SPECIAL_COST * EXATOV.QTY),0,0,((EXATOV.ExtSale - (EXATOV.SPECIAL_COST * EXATOV.QTY))/(EXATOV.ExtSale)))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'GP_AVG_COST','GPAvgCost','Report Type','NUMBER','','','','20','Y','','','','','','','(EXATOV.ExtSale - (EXATOV.AVERAGECOST * EXATOV.QTY))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'MFGADJUST','Mfgadjust','Mfgadjust','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'EXTSALE','Extsale','Extsale','','','','','17','N','','DATA_FIELD','','SUM','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
xxeis.eis_rs_ins.rc( 'White Cap End of Month Commissions Report',222,'ORDER_NUMBER','Order Number','Order Number','','','','','29','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_TBD_OUTBND_V','','');
--Inserting Report Parameters - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','select name from hr_operating_units where organization_id=fnd_profile.value(''ORG_ID'')','VARCHAR2','Y','Y','1','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'GL Period to Report','GL Period to Report','PERIOD_NAME','IN','AR_PERIOD_NAMES','select gps.period_name from gl_period_statuses gps where gps.application_id = 101  and gps.set_of_books_id = fnd_profile.value(''GL_SET_OF_BKS_ID'') and trunc(sysdate) between gps.start_date and gps.end_date','VARCHAR2','Y','Y','3','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Quantity Shipped','Order Line Quantity Shipped','SHIPPED_QUANTITY','IN','','','NUMERIC','N','N','4','','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Extended Amount','Order Line Extended Amount','EXTSALE','IN','','','NUMERIC','N','N','5','','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Extended Cost','Order Line Extended Cost','AVERAGECOST','IN','','','NUMERIC','N','N','6','','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap End of Month Commissions Report',222,'Invoice Source','Invoice Source','INVOICE_SOURCE','IN','AR Batch Source Name LOV','''ORDER MANAGEMENT'',''STANDARD OM SOURCE'',''REPAIR OM SOURCE'',''WC MANUAL''','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'SHIPPED_QUANTITY','IN',':Order Line Quantity Shipped','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'EXTSALE','IN',':Order Line Extended Amount','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'AVERAGECOST','IN',':Order Line Extended Cost','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'INVOICE_SOURCE','IN',':Invoice Source','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'OPERATING_UNIT','IN',':Operating Unit','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap End of Month Commissions Report',222,'PERIOD_NAME','IN',':GL Period to Report','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rs( 'White Cap End of Month Commissions Report',222,'REPORT_TYPE','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'White Cap End of Month Commissions Report',222,'SALESREPNUMBER','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - White Cap End of Month Commissions Report
--Inserting Report Templates - White Cap End of Month Commissions Report
--Inserting Report Portals - White Cap End of Month Commissions Report
--Inserting Report Dashboards - White Cap End of Month Commissions Report
--Inserting Report Security - White Cap End of Month Commissions Report
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50920',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50919',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50921',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50922',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50923',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','51030',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50638',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50622',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','20678',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','401','','50941',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','21404',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50846',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50845',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50847',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50848',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50849',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50944',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','222','','50871',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','20005','','50880',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','LC053655','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','10010432','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','RB054040','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','RV003897','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','SS084202','',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'White Cap End of Month Commissions Report','','SO004816','',222,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
