grant all on mrp.MRP_SR_ASSIGNMENTS to xxeis;

create or replace synonym XXEIS.MRP_SR_ASSIGNMENTS for MRP.MRP_SR_ASSIGNMENTS;

grant all on APPS.OE_PRICE_ADJUSTMENTS_V to xxeis;

create or replace synonym XXEIS.OE_PRICE_ADJUSTMENTS_V for APPS.OE_PRICE_ADJUSTMENTS_V;

grant all on OZF.OZF_SD_BATCH_LINES_ALL to xxeis;

create or replace synonym XXEIS.OZF_SD_BATCH_LINES_ALL for OZF.OZF_SD_BATCH_LINES_ALL;

grant all on OZF.OZF_SD_REQUEST_LINES_ALL to xxeis;

create or replace synonym XXEIS.OZF_SD_REQUEST_LINES_ALL for OZF.OZF_SD_REQUEST_LINES_ALL;

grant all on OZF.OZF_SD_REQUEST_HEADERS_ALL_B to xxeis;

create or replace synonym XXEIS.OZF_SD_REQUEST_HEADERS_ALL_B for OZF.OZF_SD_REQUEST_HEADERS_ALL_B;

grant all on APPS.ZX_LINES_V to xxeis;

create or replace synonym XXEIS.ZX_LINES_V for APPS.ZX_LINES_V;

grant all on ONT.OE_PAYMENTS to xxeis;

create or replace synonym XXEIS.OE_PAYMENTS for ONT.OE_PAYMENTS;

grant all on ONT.OE_REASONS to xxeis;

create or replace synonym XXEIS.OE_REASONS for ONT.OE_REASONS;

grant execute on apps.CST_COST_API to xxeis;

create or replace synonym XXEIS.CST_COST_API for apps.CST_COST_API;

grant all on APPS.CST_ITEM_COST_TYPE_V to xxeis;

create or replace synonym XXEIS.CST_ITEM_COST_TYPE_V for APPS.CST_ITEM_COST_TYPE_V;

grant all on IBY.IBY_CREDITCARD to xxeis;

create or replace synonym XXEIS.IBY_CREDITCARD for IBY.IBY_CREDITCARD;

grant all on APPS.OE_SCH_ORDER_LINES_V to xxeis;

create or replace synonym XXEIS.OE_SCH_ORDER_LINES_V for APPS.OE_SCH_ORDER_LINES_V;

grant all on APPS.IBY_TRXN_EXTENSIONS_V to xxeis;

create or replace synonym XXEIS.IBY_TRXN_EXTENSIONS_V for APPS.IBY_TRXN_EXTENSIONS_V;

grant execute on apps.QP_QP_FORM_PRICING_ATTR to xxeis;

create or replace synonym XXEIS.QP_QP_FORM_PRICING_ATTR for apps.QP_QP_FORM_PRICING_ATTR;

grant all on APPS.JTF_RS_DEFRESOURCES_V to xxeis;

CREATE OR REPLACE SYNONYM XXEIS.JTF_RS_DEFRESOURCES_V FOR APPS.JTF_RS_DEFRESOURCES_V;

grant all on INV.MTL_RESERVATIONS to xxeis;

create or replace synonym XXEIS.MTL_RESERVATIONS for INV.MTL_RESERVATIONS;

grant all on APPS.JTF_RS_DEFRESOURCES_V to xxeis;

create or replace synonym XXEIS.JTF_RS_DEFRESOURCES_V for APPS.JTF_RS_DEFRESOURCES_V;

grant all on AP.AP_INVOICE_LINES_ALL to xxeis;

create or replace synonym XXEIS.AP_INVOICE_LINES_ALL for AP.AP_INVOICE_LINES_ALL;

grant all on ZX.ZX_LINES to xxeis;

create or replace synonym XXEIS.ZX_LINES for ZX.ZX_LINES;

grant all on OZF.OZF_SD_BATCH_HEADERS_ALL to xxeis;

create or replace synonym XXEIS.OZF_SD_BATCH_HEADERS_ALL for OZF.OZF_SD_BATCH_HEADERS_ALL;

grant all on INV.MTL_DEMAND_HISTORIES to xxeis;

create or replace synonym xxeis.MTL_DEMAND_HISTORIES for inv.MTL_DEMAND_HISTORIES;

grant all on OZF.OZF_SYS_PARAMETERS_ALL to xxeis;

create or replace synonym xxeis.OZF_SYS_PARAMETERS_ALL for OZF.OZF_SYS_PARAMETERS_ALL;

grant all on OZF.OZF_CLAIMS_ALL to xxeis;

create or replace synonym xxeis.OZF_CLAIMS_ALL for OZF.OZF_CLAIMS_ALL;