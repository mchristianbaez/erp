--Report Name            : HDS Payment Register - Details Remittance
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_AP_PAYMENT_REG_REMITTANCE
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_AP_PAYMENT_REG_REMITTANCE
xxeis.eis_rsc_ins.v( 'EIS_AP_PAYMENT_REG_REMITTANCE',200,'','','','','XXEIS_RS_ADMIN','XXEIS','EIS_AP_PAYMENT_REG_REMITTANCE','EAPRR','','','VIEW','US','','');
--Delete Object Columns for EIS_AP_PAYMENT_REG_REMITTANCE
xxeis.eis_rsc_utility.delete_view_rows('EIS_AP_PAYMENT_REG_REMITTANCE',200,FALSE);
--Inserting Object Columns for EIS_AP_PAYMENT_REG_REMITTANCE
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','AMOUNT_PAID',200,'Amount Paid','AMOUNT_PAID','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','DISCOUNT_TAKEN',200,'Discount Taken','DISCOUNT_TAKEN','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','EXT_BANK_ACCOUNT_IBAN_NUMBER',200,'Ext Bank Account Iban Number','EXT_BANK_ACCOUNT_IBAN_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ext Bank Account Iban Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','EXT_BANK_BRANCH_NAME',200,'Ext Bank Branch Name','EXT_BANK_BRANCH_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ext Bank Branch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','EXT_BANK_ACCOUNT_NUMBER',200,'Ext Bank Account Number','EXT_BANK_ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ext Bank Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','EXT_BANK_ACCOUNT_NAME',200,'Ext Bank Account Name','EXT_BANK_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ext Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','DISTRIBUTION_AMOUNT',200,'Distribution Amount','DISTRIBUTION_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Distribution Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_AMOUNT',200,'Invoice Amount','INVOICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_NUMBER',200,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','SEGMENT1',200,'Segment1','SEGMENT1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','VENDOR_NAME',200,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CHECK_AMOUNT',200,'Check Amount','CHECK_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Check Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYMENT_DATE',200,'Payment Date','PAYMENT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Payment Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CHECK_NUMBER',200,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_DATE',200,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYMENT_TYPE',200,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','ZIP',200,'Zip','ZIP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','STATE',200,'State','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CITY',200,'City','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','City','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','ADDRESS_LINE2',200,'Address Line2','ADDRESS_LINE2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','ADDRESS_LINE1',200,'Address Line1','ADDRESS_LINE1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CHECKRUN_NAME',200,'Checkrun Name','CHECKRUN_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Checkrun Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','BANK_ACCOUNT_NAME',200,'Bank Account Name','BANK_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CHECK_CREATED_BY',200,'Check Created By','CHECK_CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','SOURCE',200,'Source','SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INT_BANK_ACCOUNT_NUMBER',200,'Int Bank Account Number','INT_BANK_ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Int Bank Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INT_BANK_ACCOUNT_NAME',200,'Int Bank Account Name','INT_BANK_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Int Bank Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INT_BANK_BRANCH_NAME',200,'Int Bank Branch Name','INT_BANK_BRANCH_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Int Bank Branch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','EXT_BANK_BRANCH_NUM',200,'Ext Bank Branch Num','EXT_BANK_BRANCH_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ext Bank Branch Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYMENT_METHOD_CODE',200,'Payment Method Code','PAYMENT_METHOD_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INT_BANK_NAME',200,'Int Bank Name','INT_BANK_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Int Bank Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PERIOD_NAME',200,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','GL_ACCOUNT',200,'Gl Account','GL_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','SUPPLIER_NUMBER',200,'Supplier Number','SUPPLIER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','OPERATING_UNIT',200,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','VENDOR_SITE_ID',200,'Vendor Site Id','VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','VENDOR_ID',200,'Vendor Id','VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','ORGANIZATION_ID',200,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_HEADER_ID',200,'Po Header Id','PO_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Po Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_LINE_ID',200,'Po Line Id','PO_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Po Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','LINE_LOCATION_ID',200,'Line Location Id','LINE_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_DISTRIBUTION_ID',200,'Po Distribution Id','PO_DISTRIBUTION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Po Distribution Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_LINE_NUMBER',200,'Invoice Line Number','INVOICE_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_ID',200,'Invoice Id','INVOICE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CHECK_ID',200,'Check Id','CHECK_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_PAYMENT_ID',200,'Invoice Payment Id','INVOICE_PAYMENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Payment Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','APPROVAL_DESCRIPTION',200,'Approval Description','APPROVAL_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approval Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','APPROVAL_STATUS',200,'Approval Status','APPROVAL_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approval Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','APPROVED_FLAG',200,'Approved Flag','APPROVED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Approved Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_STATUS',200,'Po Status','PO_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Po Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_DATE',200,'Po Date','PO_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Po Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_RECEIVED_DATE',200,'Invoice Received Date','INVOICE_RECEIVED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Received Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','GOODS_RECEIVED_DATE',200,'Goods Received Date','GOODS_RECEIVED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Goods Received Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','FREIGHT_AMOUNT',200,'Freight Amount','FREIGHT_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Freight Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYMENT_STATUS_FLAG',200,'Payment Status Flag','PAYMENT_STATUS_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Status Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAY_GROUP_LOOKUP_CODE',200,'Pay Group Lookup Code','PAY_GROUP_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Group Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','TAX_AMOUNT',200,'Tax Amount','TAX_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Tax Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','AMOUNT_APPLICABLE_TO_DISCOUNT',200,'Amount Applicable To Discount','AMOUNT_APPLICABLE_TO_DISCOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount Applicable To Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_TYPE_LOOKUP_CODE',200,'Invoice Type Lookup Code','INVOICE_TYPE_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','RELEASED_DATE',200,'Released Date','RELEASED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Released Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','STOPPED_DATE',200,'Stopped Date','STOPPED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Stopped Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POSITIVE_PAY_STATUS_CODE',200,'Positive Pay Status Code','POSITIVE_PAY_STATUS_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Positive Pay Status Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','FUTURE_PAY_DUE_DATE',200,'Future Pay Due Date','FUTURE_PAY_DUE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Future Pay Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','VOID_DATE',200,'Void Date','VOID_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Void Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CLEARED_DATE',200,'Cleared Date','CLEARED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Cleared Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CLEARED_AMOUNT',200,'Cleared Amount','CLEARED_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Cleared Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CHECK_VOUCHER_NUM',200,'Check Voucher Num','CHECK_VOUCHER_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Check Voucher Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','STATUS_LOOKUP_CODE',200,'Status Lookup Code','STATUS_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Status Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','COUNTRY',200,'Country','COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','COUNTY',200,'County','COUNTY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','County','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','ADDRESS_LINE4',200,'Address Line4','ADDRESS_LINE4','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','ADDRESS_LINE3',200,'Address Line3','ADDRESS_LINE3','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','REVERSAL_INV_PMT_ID',200,'Reversal Inv Pmt Id','REVERSAL_INV_PMT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Reversal Inv Pmt Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','REVERSAL_FLAG',200,'Reversal Flag','REVERSAL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reversal Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_PAYMENT_TYPE',200,'Invoice Payment Type','INVOICE_PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Payment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','ASSETS_ADDITION_FLAG',200,'Assets Addition Flag','ASSETS_ADDITION_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Assets Addition Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYMENT_BASE_AMOUNT',200,'Payment Base Amount','PAYMENT_BASE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Payment Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_BASE_AMOUNT',200,'Invoice Base Amount','INVOICE_BASE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','DISCOUNT_LOST',200,'Discount Lost','DISCOUNT_LOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Lost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYEE_NAME',200,'Payee Name','PAYEE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payee Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYMENT_STATUS',200,'Payment Status','PAYMENT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INV_PAYEE_SUPPLIER_SITE_NAME',200,'Inv Payee Supplier Site Name','INV_PAYEE_SUPPLIER_SITE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Inv Payee Supplier Site Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYEE_LE_REGISTRATION_NUM',200,'Payee Le Registration Num','PAYEE_LE_REGISTRATION_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payee Le Registration Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYEE_PARTY_NAME',200,'Payee Party Name','PAYEE_PARTY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payee Party Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYER_LE_REGISTRATION_NUM',200,'Payer Le Registration Num','PAYER_LE_REGISTRATION_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payer Le Registration Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','EXT_EFT_SWIFT_CODE',200,'Ext Eft Swift Code','EXT_EFT_SWIFT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ext Eft Swift Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POSTED_FLAG',200,'Posted Flag','POSTED_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Posted Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYMENT_NUM',200,'Payment Num','PAYMENT_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','ACCOUNTING_DATE',200,'Accounting Date','ACCOUNTING_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','GL_CCID',200,'Gl Ccid','GL_CCID','','','','XXEIS_RS_ADMIN','NUMBER','','','Gl Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','GL_PRODUCT',200,'Gl Product','GL_PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_DESCRIPTION',200,'Invoice Description','INVOICE_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','BASE_AMOUNT',200,'Base Amount','BASE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Base Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','DISTRIBUTION_LINE_NUMBER',200,'Distribution Line Number','DISTRIBUTION_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Distribution Line Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PAYMENT_CURRENCY_CODE',200,'Payment Currency Code','PAYMENT_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','INVOICE_CURRENCY_CODE',200,'Invoice Currency Code','INVOICE_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','VENDOR_SITE_CODE',200,'Vendor Site Code','VENDOR_SITE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','LEDGER_NAME',200,'Ledger Name','LEDGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ledger Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_NUMBER',200,'Po Number','PO_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#162#Documentum_PO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: Documentum PO Number Context: 162','API#162#Documentum_PO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Api#162#Documentum Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#163#BizTalk_ID_Frt_Bizta',200,'Descriptive flexfield (DFF): Invoice Column Name: BizTalk ID-Frt Biztalk Context: 163','API#163#BizTalk_ID_Frt_Bizta','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE1','Api#163#Biztalk Id-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#163#Invoice_Type',200,'Descriptive flexfield (DFF): Invoice Column Name: Invoice Type Context: 163','API#163#Invoice_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE11','Api#163#Invoice Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#163#DCTM_Image_Link',200,'Descriptive flexfield (DFF): Invoice Column Name: DCTM Image Link Context: 163','API#163#DCTM_Image_Link','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE14','Api#163#Dctm Image Link','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#163#R11_Invoice_ID',200,'Descriptive flexfield (DFF): Invoice Column Name: R11 Invoice ID Context: 163','API#163#R11_Invoice_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE15','Api#163#R11 Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#163#Branch_Frt_Biztalk',200,'Descriptive flexfield (DFF): Invoice Column Name: Branch-Frt Biztalk Context: 163','API#163#Branch_Frt_Biztalk','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Api#163#Branch-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#163#POS_System_Code_Frt_',200,'Descriptive flexfield (DFF): Invoice Column Name: POS System Code-Frt Biztalk Context: 163','API#163#POS_System_Code_Frt_','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE8','Api#163#Pos System Code-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#166#Case_Identifier',200,'Descriptive flexfield (DFF): Invoice Column Name: Case Identifier Context: 166','API#166#Case_Identifier','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE1','Api#166#Case Identifier','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#166#R11_Invoice_ID',200,'Descriptive flexfield (DFF): Invoice Column Name: R11 Invoice ID Context: 166','API#166#R11_Invoice_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE15','Api#166#R11 Invoice Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#166#Deduction_Code',200,'Descriptive flexfield (DFF): Invoice Column Name: Deduction Code Context: 166','API#166#Deduction_Code','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE2','Api#166#Deduction Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#166#Employee',200,'Descriptive flexfield (DFF): Invoice Column Name: Employee Context: 166','API#166#Employee','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Api#166#Employee','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#166#Description',200,'Descriptive flexfield (DFF): Invoice Column Name: Description Context: 166','API#166#Description','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Api#166#Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','APID#163#PO_Number_Frt_Bizta',200,'Descriptive flexfield (DFF): Invoice Distribution Column Name: PO Number-Frt Biztalk Context: 163','APID#163#PO_Number_Frt_Bizta','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICE_DISTRIBUTIONS_ALL','ATTRIBUTE1','Apid#163#Po Number-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','APID#163#Branch_Frt_Biztalk',200,'Descriptive flexfield (DFF): Invoice Distribution Column Name: Branch-Frt Biztalk Context: 163','APID#163#Branch_Frt_Biztalk','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICE_DISTRIBUTIONS_ALL','ATTRIBUTE2','Apid#163#Branch-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','APID#163#Bill_of_Lading_Frt_',200,'Descriptive flexfield (DFF): Invoice Distribution Column Name: Bill of Lading-Frt Biztalk Context: 163','APID#163#Bill_of_Lading_Frt_','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICE_DISTRIBUTIONS_ALL','ATTRIBUTE3','Apid#163#Bill Of Lading-Frt Biztalk','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Need_By_Date',200,'Descriptive flexfield (DFF): PO Headers Column Name: Need-By Date Context: Standard Purchase Order','POH#StandardP#Need_By_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE1','Poh#Standard Purchase Order#Need-By Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#FOB_TERMS_Tab',200,'Descriptive flexfield (DFF): PO Headers Column Name: FOB (TERMS Tab) Context: Standard Purchase Order','POH#StandardP#FOB_TERMS_Tab','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE4','Poh#Standard Purchase Order#Fob Terms Tab','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#163#SO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: SO Number Context: 163','API#163#SO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Api#163#So Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#163#PO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: PO Number Context: 163','API#163#PO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Api#163#Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#163#BOL',200,'Descriptive flexfield (DFF): Invoice Column Name: BOL Context: 163','API#163#BOL','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE6','Api#163#Bol','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#167#SO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: SO Number Context: 167','API#167#SO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE3','Api#167#So Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#167#PO_Number',200,'Descriptive flexfield (DFF): Invoice Column Name: PO Number Context: 167','API#167#PO_Number','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE4','Api#167#Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#167#Canada_Tax_Type',200,'Descriptive flexfield (DFF): Invoice Column Name: Canada Tax Type Context: 167','API#167#Canada_Tax_Type','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE5','Api#167#Canada Tax Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','API#167#BOL',200,'Descriptive flexfield (DFF): Invoice Column Name: BOL Context: 167','API#167#BOL','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_INVOICES_ALL','ATTRIBUTE6','Api#167#Bol','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Note_To_AP',200,'Descriptive flexfield (DFF): PO Headers Column Name: Note To AP Context: Standard Purchase Order','POH#StandardP#Note_To_AP','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE10','Poh#Standard Purchase Order#Note To Ap','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Discount_Valid',200,'Descriptive flexfield (DFF): PO Headers Column Name: Discount Validated ? Context: Standard Purchase Order','POH#StandardP#Discount_Valid','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE11','Poh#Standard Purchase Order#Discount Validated ?','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Requested_Deli',200,'Descriptive flexfield (DFF): PO Headers Column Name: Requested Delivery Date Context: Standard Purchase Order','POH#StandardP#Requested_Deli','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE12','Poh#Standard Purchase Order#Requested Delivery Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Drop_Ship_EDI_',200,'Descriptive flexfield (DFF): PO Headers Column Name: Drop Ship EDI Inidicator Context: Standard Purchase Order','POH#StandardP#Drop_Ship_EDI_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE2','Poh#Standard Purchase Order#Drop Ship Edi Inidicator','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Drop_Ship_Cont',200,'Descriptive flexfield (DFF): PO Headers Column Name: Drop Ship Contact Phone Number Context: Standard Purchase Order','POH#StandardP#Drop_Ship_Cont','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE3','Poh#Standard Purchase Order#Drop Ship Contact Phone Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Freight_Accoun',200,'Descriptive flexfield (DFF): PO Headers Column Name: Freight Account Number Context: Standard Purchase Order','POH#StandardP#Freight_Accoun','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE5','Poh#Standard Purchase Order#Freight Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Promised_Date',200,'Descriptive flexfield (DFF): PO Headers Column Name: Promised Date Context: Standard Purchase Order','POH#StandardP#Promised_Date','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE6','Poh#Standard Purchase Order#Promised Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Header_Discoun',200,'Descriptive flexfield (DFF): PO Headers Column Name: Header Discount Type Context: Standard Purchase Order','POH#StandardP#Header_Discoun','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE7','Poh#Standard Purchase Order#Header Discount Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Header_Discoun1',200,'Descriptive flexfield (DFF): PO Headers Column Name: Header Discount Value Context: Standard Purchase Order','POH#StandardP#Header_Discoun1','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE8','Poh#Standard Purchase Order#Header Discount Value','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POH#StandardP#Line_Discount_',200,'Descriptive flexfield (DFF): PO Headers Column Name: Line Discount Percent Context: Standard Purchase Order','POH#StandardP#Line_Discount_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ATTRIBUTE9','Poh#Standard Purchase Order#Line Discount Percent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POL#Apply__Remove_Discount_',200,'Descriptive flexfield (DFF): PO Lines Column Name: Apply / Remove Discount ?','POL#Apply__Remove_Discount_','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','ATTRIBUTE1','Pol#Apply / Remove Discount ?','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POL#Discount_Applied_Flag',200,'Descriptive flexfield (DFF): PO Lines Column Name: Discount Applied Flag','POL#Discount_Applied_Flag','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','ATTRIBUTE2','Pol#Discount Applied Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','POL#Original_Line_Cost',200,'Descriptive flexfield (DFF): PO Lines Column Name: Original Line Cost','POL#Original_Line_Cost','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','ATTRIBUTE3','Pol#Original Line Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_AP_PAYMENT_REG_REMITTANCE','CH_PAYMENT_TYPE',200,'Ch Payment Type','CH_PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ch Payment Type','','','','');
--Inserting Object Components for EIS_AP_PAYMENT_REG_REMITTANCE
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_INVOICE_PAYMENTS_ALL',200,'AP_INVOICE_PAYMENTS_ALL','AIP','AIP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Invoice Payment Records','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_CHECKS_ALL',200,'AP_CHECKS_ALL','CH','CH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Supplier Payment Data','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_INVOICES_ALL',200,'AP_INVOICES_ALL','API','API','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Detailed Invoice Records','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_INVOICE_DISTRIBUTIONS_ALL',200,'AP_INVOICE_DISTRIBUTIONS_ALL','APID','APID','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Invoice Distribution Line Information','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_DISTRIBUTIONS_ALL',200,'PO_DISTRIBUTIONS_ALL','POD','POD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchase Order Distributions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_LINE_LOCATIONS_ALL',200,'PO_LINE_LOCATIONS_ALL','POLL','POLL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Document Shipment Schedules (For Purchase Orders, ','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_LINES_ALL',200,'PO_LINES_ALL','POL','POL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchase Document Lines (For Purchase Orders, Purc','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_HEADERS_ALL',200,'PO_HEADERS_ALL','POH','POH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Document Headers (For Purchase Orders, Purchase Ag','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_SUPPLIERS',200,'AP_SUPPLIERS','PV','PV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ap Suppliers Stores Information About Your Supplie','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_SUPPLIER_SITES_ALL',200,'AP_SUPPLIER_SITES_ALL','PVS','PVS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Ap Supplier Sites All Stores Information About You','','','','','ASSA','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_AP_PAYMENT_REG_REMITTANCE','HR_ORGANIZATION_UNITS',200,'HR_ALL_ORGANIZATION_UNITS','HOU','HOU','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','HR_ALL_ORGANIZATION_UNITS','N','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_AP_PAYMENT_REG_REMITTANCE
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_INVOICE_PAYMENTS_ALL','AIP',200,'EAPRR.INVOICE_PAYMENT_ID','=','AIP.INVOICE_PAYMENT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_CHECKS_ALL','CH',200,'EAPRR.CHECK_ID','=','CH.CHECK_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_INVOICES_ALL','API',200,'EAPRR.INVOICE_ID','=','API.INVOICE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_INVOICE_DISTRIBUTIONS_ALL','APID',200,'EAPRR.INVOICE_ID','=','APID.INVOICE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_INVOICE_DISTRIBUTIONS_ALL','APID',200,'EAPRR.INVOICE_LINE_NUMBER','=','APID.INVOICE_LINE_NUMBER(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_INVOICE_DISTRIBUTIONS_ALL','APID',200,'EAPRR.DISTRIBUTION_LINE_NUMBER','=','APID.DISTRIBUTION_LINE_NUMBER(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_DISTRIBUTIONS_ALL','POD',200,'EAPRR.PO_DISTRIBUTION_ID','=','POD.PO_DISTRIBUTION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_LINE_LOCATIONS_ALL','POLL',200,'EAPRR.LINE_LOCATION_ID','=','POLL.LINE_LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_LINES_ALL','POL',200,'EAPRR.PO_LINE_ID','=','POL.PO_LINE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','PO_HEADERS_ALL','POH',200,'EAPRR.PO_HEADER_ID','=','POH.PO_HEADER_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_SUPPLIERS','PV',200,'EAPRR.VENDOR_ID','=','PV.VENDOR_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','AP_SUPPLIER_SITES_ALL','PVS',200,'EAPRR.VENDOR_SITE_ID','=','PVS.VENDOR_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_AP_PAYMENT_REG_REMITTANCE','HR_ORGANIZATION_UNITS','HOU',200,'EAPRR.ORGANIZATION_ID','=','HOU.ORGANIZATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Payment Register - Details Remittance
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Payment Register - Details Remittance
xxeis.eis_rsc_ins.lov( 200,'select distinct CHECKRUN_NAME from AP_CHECKS','','EIS_AP_CHECKRUN_NAME_LOV','List of Values for Checkrun Name','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select CHECK_NUMBER from AP_CHECKS','','EIS_AP_CHECK_NUMBER_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 200,'select distinct payment_method_code from iby.IBY_PAYMENTS_ALL','','EIS_AP_Payment_Method_Code','List of Values for Payment Method Code','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS Payment Register - Details Remittance
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Payment Register - Details Remittance
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Payment Register - Details Remittance' );
--Inserting Report - HDS Payment Register - Details Remittance
xxeis.eis_rsc_ins.r( 200,'HDS Payment Register - Details Remittance','','','','','','XXEIS_RS_ADMIN','EIS_AP_PAYMENT_REG_REMITTANCE','Y','','','XXEIS_RS_ADMIN','','Y','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','','','','','','','','','US','','','','');
--Inserting Report Columns - HDS Payment Register - Details Remittance
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'ADDRESS_LINE1','Address Line1','Address Line1','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'ADDRESS_LINE2','Address Line2','Address Line2','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'CHECKRUN_NAME','Checkrun Name','Checkrun Name','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'CHECK_AMOUNT','Check Amount','Check Amount','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'CHECK_NUMBER','Check Number','Check Number','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'CITY','City','City','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'DISCOUNT_TAKEN','Discount Taken','Discount Taken','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'EXT_BANK_ACCOUNT_IBAN_NUMBER','Ext Bank Account Iban Number','Ext Bank Account Iban Number','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'EXT_BANK_ACCOUNT_NAME','Ext Bank Account Name','Ext Bank Account Name','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'EXT_BANK_ACCOUNT_NUMBER','Ext Bank Account Number','Ext Bank Account Number','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'EXT_BANK_BRANCH_NUM','Ext Bank Branch Num','Ext Bank Branch Num','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'INT_BANK_ACCOUNT_NAME','Int Bank Account Name','Int Bank Account Name','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'INT_BANK_ACCOUNT_NUMBER','Int Bank Account Number','Int Bank Account Number','','','','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'INT_BANK_BRANCH_NAME','Int Bank Branch Name','Int Bank Branch Name','','','','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'INT_BANK_NAME','Int Bank Name','Int Bank Name','','','','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'INVOICE_AMOUNT','Invoice Amount','Invoice Amount','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'PAYMENT_DATE','Payment Date','Payment Date','','','','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'PAYMENT_METHOD_CODE','Payment Method Code','Payment Method Code','','','','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'STATE','State','State','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'SUPPLIER_NUMBER','Supplier Number','Supplier Number','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'ZIP','Zip','Zip','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'AMOUNT_PAID','Amount Paid','Amount Paid','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'DISTRIBUTION_AMOUNT','Distribution Amount','Distribution Amount','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS Payment Register - Details Remittance',200,'CHECK_CREATED_BY','Check Created By','Check Created By','','','','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_AP_PAYMENT_REG_REMITTANCE','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Payment Register - Details Remittance
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance',200,'Payment Date From','Payment Date From','PAYMENT_DATE','>=','','','DATE','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','','EIS_AP_PAYMENT_REG_REMITTANCE','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance',200,'Payment Date To','Payment Date To','PAYMENT_DATE','<=','','','DATE','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','','EIS_AP_PAYMENT_REG_REMITTANCE','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance',200,'Check Number','Check Number','CHECK_NUMBER','IN','EIS_AP_CHECK_NUMBER_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REG_REMITTANCE','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance',200,'Checkrun Name','Checkrun Name','CHECKRUN_NAME','IN','EIS_AP_CHECKRUN_NAME_LOV','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REG_REMITTANCE','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS Payment Register - Details Remittance',200,'Payment Type','Payment Type','PAYMENT_METHOD_CODE','IN','EIS_AP_Payment_Method_Code','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_AP_PAYMENT_REG_REMITTANCE','','','US','');
--Inserting Dependent Parameters - HDS Payment Register - Details Remittance
--Inserting Report Conditions - HDS Payment Register - Details Remittance
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance',200,'CHECKRUN_NAME IN :Checkrun Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECKRUN_NAME','','Checkrun Name','','','','','EIS_AP_PAYMENT_REG_REMITTANCE','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details Remittance','CHECKRUN_NAME IN :Checkrun Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance',200,'CHECK_NUMBER IN :Check Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_NUMBER','','Check Number','','','','','EIS_AP_PAYMENT_REG_REMITTANCE','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details Remittance','CHECK_NUMBER IN :Check Number ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance',200,'EAPRR.PAYMENT_DATE >= :Payment Date From ','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','EIS_AP_PAYMENT_REG_REMITTANCE','','','','','','GREATER_THAN_EQUALS','Y','Y','TRUNC(EAPRR.PAYMENT_DATE)',':Payment Date From','','','1',200,'HDS Payment Register - Details Remittance','EAPRR.PAYMENT_DATE >= :Payment Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance',200,'PAYMENT_METHOD_CODE = ''WIRE'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_METHOD_CODE','','','','','','','EIS_AP_PAYMENT_REG_REMITTANCE','','','','','','EQUALS','Y','N','','''WIRE''','','','1',200,'HDS Payment Register - Details Remittance','PAYMENT_METHOD_CODE = ''WIRE'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance',200,'PAYMENT_METHOD_CODE IN :Payment Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAYMENT_METHOD_CODE','','Payment Type','','','','','EIS_AP_PAYMENT_REG_REMITTANCE','','','','','','IN','Y','Y','','','','','1',200,'HDS Payment Register - Details Remittance','PAYMENT_METHOD_CODE IN :Payment Type ');
xxeis.eis_rsc_ins.rcnh( 'HDS Payment Register - Details Remittance',200,'EAPRR.PAYMENT_DATE <= Payment Date To','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','EIS_AP_PAYMENT_REG_REMITTANCE','','','','','','LESS_THAN_EQUALS','Y','Y','TRUNC(EAPRR.PAYMENT_DATE)',':Payment Date To','','','1',200,'HDS Payment Register - Details Remittance','EAPRR.PAYMENT_DATE <= Payment Date To');
--Inserting Report Sorts - HDS Payment Register - Details Remittance
xxeis.eis_rsc_ins.rs( 'HDS Payment Register - Details Remittance',200,'CHECK_NUMBER','ASC','XXEIS_RS_ADMIN','1','');
--Inserting Report Triggers - HDS Payment Register - Details Remittance
--inserting report templates - HDS Payment Register - Details Remittance
--Inserting Report Portals - HDS Payment Register - Details Remittance
--inserting report dashboards - HDS Payment Register - Details Remittance
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Payment Register - Details Remittance','200','EIS_AP_PAYMENT_REG_REMITTANCE','EIS_AP_PAYMENT_REG_REMITTANCE','N','');
--inserting report security - HDS Payment Register - Details Remittance
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_ADMIN_US_GSCIWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_ADMIN_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_DISBUREMTS_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_OIE_USER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','XXWC_PAY_NO_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','XXWC_PAY_W_CALENDAR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','XXWC_PAY_DISBURSE',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','XXWC_PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','XXWC_PAY_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','XXWC_PAY_VENDOR_MSTR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_MGR_NOSUP_US_IWO',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_PYABLS_MNGR_CAN',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_PYBLS_MNGR',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Payment Register - Details Remittance','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Payment Register - Details Remittance
--Inserting Report   Version details- HDS Payment Register - Details Remittance
xxeis.eis_rsc_ins.rv( 'HDS Payment Register - Details Remittance','','HDS Payment Register - Details Remittance','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
