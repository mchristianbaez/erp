--Report Name            : WC - RTV DFF
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_646484_MIJYJV_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_646484_MIJYJV_V
xxeis.eis_rsc_ins.v( 'XXEIS_646484_MIJYJV_V',200,'Paste SQL View for WC - RTV DFF','1.0','','','10012196','APPS','WC - RTV DFF View','X6MV1','','','VIEW','US','','');
--Delete Object Columns for XXEIS_646484_MIJYJV_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_646484_MIJYJV_V',200,FALSE);
--Inserting Object Columns for XXEIS_646484_MIJYJV_V
xxeis.eis_rsc_ins.vc( 'XXEIS_646484_MIJYJV_V','INVOICE_TYPE_LOOKUP_CODE',200,'','','','','','10012196','VARCHAR2','','','Invoice Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_646484_MIJYJV_V','INVOICE_NUM',200,'','','','','','10012196','VARCHAR2','','','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_646484_MIJYJV_V','ATTRIBUTE1',200,'','','','','','10012196','VARCHAR2','','','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_646484_MIJYJV_V','AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_646484_MIJYJV_V','ACCOUNTING_DATE',200,'','','','','','10012196','DATE','','','Accounting Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_646484_MIJYJV_V','SEGMENT1',200,'','','','','','10012196','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_646484_MIJYJV_V','VENDOR_NAME',200,'','','','','','10012196','VARCHAR2','','','Vendor Name','','','','US');
--Inserting Object Components for XXEIS_646484_MIJYJV_V
--Inserting Object Component Joins for XXEIS_646484_MIJYJV_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC - RTV DFF
prompt Creating Report Data for WC - RTV DFF
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC - RTV DFF
xxeis.eis_rsc_utility.delete_report_rows( 'WC - RTV DFF' );
--Inserting Report - WC - RTV DFF
xxeis.eis_rsc_ins.r( 200,'WC - RTV DFF','','Invoice types are: STANDARD, DEBIT, or CREDIT','','','','10012196','XXEIS_646484_MIJYJV_V','Y','','SELECT AIA.INVOICE_TYPE_LOOKUP_CODE
, AIA.INVOICE_NUM
, AILA.ATTRIBUTE1
, AILA.AMOUNT
, AILA.ACCOUNTING_DATE
, ASS.SEGMENT1
, ASS.VENDOR_NAME
FROM AP.AP_INVOICES_ALL AIA
, AP.AP_INVOICE_LINES_ALL AILA
, AP.AP_SUPPLIERS ASS
WHERE AIA.INVOICE_ID = AILA.INVOICE_ID
AND AIA.VENDOR_ID = ASS.VENDOR_ID
AND AIA.ORG_ID = 162
','10012196','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','','','','','','','','APPS','US','','','','');
--Inserting Report Columns - WC - RTV DFF
xxeis.eis_rsc_ins.rc( 'WC - RTV DFF',200,'ACCOUNTING_DATE','Accounting Date','','','','default','','6','N','','','','','','','','10012196','N','N','','XXEIS_646484_MIJYJV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - RTV DFF',200,'SEGMENT1','Supplier Num','','','','default','','2','N','','','','','','','','10012196','N','N','','XXEIS_646484_MIJYJV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - RTV DFF',200,'AMOUNT','Amount','','','~T~D~0','default','','5','N','','','','','','','','10012196','N','N','','XXEIS_646484_MIJYJV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'WC - RTV DFF',200,'INVOICE_NUM','Invoice Num','','','','default','','4','N','','','','','','','','10012196','N','N','','XXEIS_646484_MIJYJV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - RTV DFF',200,'INVOICE_TYPE_LOOKUP_CODE','Invoice Type','','','','default','','1','N','','','','','','','','10012196','N','N','','XXEIS_646484_MIJYJV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - RTV DFF',200,'ATTRIBUTE1','RTV Transaction Num','','','','default','','7','N','','','','','','','','10012196','N','N','','XXEIS_646484_MIJYJV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'WC - RTV DFF',200,'VENDOR_NAME','Vendor Name','','','','default','','3','N','','','','','','','','10012196','N','N','','XXEIS_646484_MIJYJV_V','','','GROUP_BY','US','');
--Inserting Report Parameters - WC - RTV DFF
xxeis.eis_rsc_ins.rp( 'WC - RTV DFF',200,'RTV Transaction ID From','','ATTRIBUTE1','>=','','','VARCHAR2','N','Y','1','','Y','CONSTANT','10012196','Y','N','','','','XXEIS_646484_MIJYJV_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - RTV DFF',200,'Invoice Number','','INVOICE_NUM','IN','','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_646484_MIJYJV_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - RTV DFF',200,'Invoice Type','','INVOICE_TYPE_LOOKUP_CODE','IN','','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_646484_MIJYJV_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - RTV DFF',200,'Supplier Number','','SEGMENT1','IN','','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_646484_MIJYJV_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC - RTV DFF',200,'RTV Transaction ID To','','ATTRIBUTE1','<=','','','VARCHAR2','N','Y','2','','Y','CONSTANT','10012196','Y','N','','','','XXEIS_646484_MIJYJV_V','','','US','');
--Inserting Dependent Parameters - WC - RTV DFF
--Inserting Report Conditions - WC - RTV DFF
xxeis.eis_rsc_ins.rcnh( 'WC - RTV DFF',200,'ATTRIBUTE1 >= :RTV Transaction ID From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ATTRIBUTE1','','RTV Transaction ID From','','','','','XXEIS_646484_MIJYJV_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'WC - RTV DFF','ATTRIBUTE1 >= :RTV Transaction ID From ');
xxeis.eis_rsc_ins.rcnh( 'WC - RTV DFF',200,'ATTRIBUTE1 <= :RTV Transaction ID To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ATTRIBUTE1','','RTV Transaction ID To','','','','','XXEIS_646484_MIJYJV_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'WC - RTV DFF','ATTRIBUTE1 <= :RTV Transaction ID To ');
xxeis.eis_rsc_ins.rcnh( 'WC - RTV DFF',200,'INVOICE_NUM IN :Invoice Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_NUM','','Invoice Number','','','','','XXEIS_646484_MIJYJV_V','','','','','','IN','Y','Y','','','','','1',200,'WC - RTV DFF','INVOICE_NUM IN :Invoice Number ');
xxeis.eis_rsc_ins.rcnh( 'WC - RTV DFF',200,'INVOICE_TYPE_LOOKUP_CODE IN :Invoice Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_TYPE_LOOKUP_CODE','','Invoice Type','','','','','XXEIS_646484_MIJYJV_V','','','','','','IN','Y','Y','','','','','1',200,'WC - RTV DFF','INVOICE_TYPE_LOOKUP_CODE IN :Invoice Type ');
xxeis.eis_rsc_ins.rcnh( 'WC - RTV DFF',200,'SEGMENT1 IN :Supplier Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT1','','Supplier Number','','','','','XXEIS_646484_MIJYJV_V','','','','','','IN','Y','Y','','','','','1',200,'WC - RTV DFF','SEGMENT1 IN :Supplier Number ');
--Inserting Report Sorts - WC - RTV DFF
--Inserting Report Triggers - WC - RTV DFF
--inserting report templates - WC - RTV DFF
--Inserting Report Portals - WC - RTV DFF
--inserting report dashboards - WC - RTV DFF
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC - RTV DFF','200','XXEIS_646484_MIJYJV_V','XXEIS_646484_MIJYJV_V','N','');
--inserting report security - WC - RTV DFF
xxeis.eis_rsc_ins.rsec( 'WC - RTV DFF','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - RTV DFF','200','','XXWC_PAY_VENDOR_MSTR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - RTV DFF','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - RTV DFF','200','','XXWC_PAY_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - RTV DFF','200','','XXWC_PAYABLES_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - RTV DFF','200','','XXWC_PAY_DISBURSE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - RTV DFF','200','','XXWC_PAY_W_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - RTV DFF','200','','XXWC_PAY_NO_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'WC - RTV DFF','','BS006141','',200,'10012196','','','');
--Inserting Report Pivots - WC - RTV DFF
--Inserting Report   Version details- WC - RTV DFF
xxeis.eis_rsc_ins.rv( 'WC - RTV DFF','','WC - RTV DFF','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
