--Report Name            : Manual Price Changes on Sales Orders Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_MAN_PRICE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_MAN_PRICE_V
Create or replace View XXEIS.EIS_XXWC_OM_MAN_PRICE_V
(PRICING_ZONE,REGION,BRANCH,CUSTOMER,CUSTOMER_NUMBER,CREATED_BY,ORDER_NUMBER,ITEM,ITEM_DESCRIPTION,CAT_CLASS,CAT_CLASS_DESC,QUANTITY,LIST_PRICE,ORIGINAL_UNIT_SELLING_PRICE,FINAL_SELLING_PRICE,REASON_CODE,APPLICATION_METHOD,ORDERED_DATE,AVERAGE_COST,MODIFIER_TYPE,HEADER_ID,LINE_ID,LIST_LINE_TYPE_CODE,INVENTORY_ITEM_ID,MSI_ORGANIZATION_ID,ORGANIZATION_ID,CUST_ACCOUNT_ID) AS 
SELECT MP.ATTRIBUTE6 PRICING_ZONE,
    mp.ATTRIBUTE9 REGION,
    ood.organization_code branch,
    Hca.Account_Name Customer,
    Hca.Account_Number Customer_Number,
    PPF.FULL_NAME CREATED_BY,
    OH.ORDER_NUMBER ORDER_NUMBER,
    /* (ol.line_number
    ||'.'
    ||ol.shipment_number) line_number,*/
    Msi.Segment1 Item,
    Msi.Description Item_Description,
    xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat_class,
    xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc(msi.inventory_item_id,msi.organization_id) cat_class_desc,
    ol.ordered_quantity quantity,
    Ol.Unit_List_Price List_Price,
    (ol.unit_list_price-xxeis.EIS_RS_XXWC_COM_UTIL_PKG.Get_Adj_Auto_Modifier_Amt(ol.header_id,ol.line_id)) original_unit_selling_price,
    ol.unit_selling_price final_selling_price,
    oel.meaning Reason_Code,
    /* CASE
    WHEN upper(adj_man.adjustment_name) IN ('AMOUNT_LINE_DISCOUNT','NEW PRICE','PERCENT_LINE_DISCOUNT')
    THEN apps.qp_qp_form_pricing_attr.get_meaning(adj_man.arithmetic_operator, 'ARITHMETIC_OPERATOR')
    ELSE 'New Price'
    END*/
    apps.qp_qp_form_pricing_attr.get_meaning(adj_man.arithmetic_operator, 'ARITHMETIC_OPERATOR') application_method,
    TRUNC(oh.ordered_date) ordered_date,
    NVL(ol.unit_cost,0) average_cost,
    qph.attribute10  modifier_type,
    --NVL(qph.attribute10, 'Line Override') modifier_type,
    --(NVL(apps.cst_cost_api.get_item_cost(1,msi.inventory_item_id,msi.organization_id),0)) average_cost,
    ---Primary Keys
    oh.header_id,
    Ol.Line_Id,
    Adj_Man.List_Line_Type_Code,
    Msi.Inventory_Item_Id,
    Msi.Organization_Id Msi_Organization_Id,
    Ood.Organization_Id,
    Hca.Cust_Account_Id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM oe_order_headers oh,
    oe_order_lines ol,
    mtl_system_items_kfv msi,
    mtl_parameters mp,
    org_organization_definitions ood,
    hz_cust_accounts hca,
    Oe_Price_Adjustments_V Adj_Man,
    --  oe_price_adjustments_v adj_auto,
    PER_PEOPLE_F PPF,
    FND_USER FU,
    oe_lookups oel,
    QP_LIST_HEADERS_VL qph
  WHERE 1                            = 1--oh.order_number  ='10001157'
  AND ol.ship_from_org_id            = ood.organization_id
  AND oh.sold_to_org_id              = hca.cust_account_id
  AND oh.header_id                   = ol.header_id
  AND Ol.Ship_From_Org_Id            = Msi.Organization_Id
  AND ol.inventory_item_id           = msi.inventory_item_id
  AND Msi.Organization_Id            = Mp.Organization_Id
  AND Ol.Header_Id                   = Adj_Man.Header_Id(+)
  AND Ol.Line_Id                     = Adj_Man.Line_Id(+)
  AND adj_man.list_header_id         = qph.list_header_id(+)
  AND adj_man.list_line_type_code(+) = 'DIS'
    -- AND Ol.Header_Id                    = NVL(adj_Auto.Header_Id,Ol.Header_Id)
    -- AND Ol.Line_Id                      = NVL(Adj_Auto.Line_Id,Ol.Line_Id)
    -- AND NVL(adj_auto.automatic_flag,'Y')='Y'
    --AND Ol.Created_By = Ppf.Person_Id
  AND FU.USER_ID     =Ol.CREATED_BY
  AND FU.EMPLOYEE_ID =PPF.PERSON_ID(+)
  AND TRUNC (OL.CREATION_DATE) BETWEEN NVL (PPF.EFFECTIVE_START_DATE, TRUNC (OL.CREATION_DATE) ) AND NVL (PPF.EFFECTIVE_END_DATE, TRUNC (OL.CREATION_DATE) )
    --AND oh.order_number='10001303'
    --AND ( adj_man.adjustment_name         IN ('Line Discount Amount','NEW PRICE_LINE_DISC','LINE DISCOUNT PERCENT'))
  AND (upper(adj_man.adjustment_name) IN ('AMOUNT_LINE_DISCOUNT','NEW PRICE','PERCENT_LINE_DISCOUNT','NEW_PRICE_DISCOUNT'))
    /*OR(  adj_man.automatic_flag='N' /*((SELECT SUM(oe1.adjusted_amount)
    FROM oe_price_adjustments_v oe1
    WHERE oe1.list_line_type_code        ='DIS'
    AND ol.line_id                       = oe1.line_id) + nvl(ol.unit_list_price,0) ) <> NVL(ol.unit_selling_price,0)
    )
    )*/
  AND OEL.LOOKUP_CODE(+)                 =ADJ_MAN.CHANGE_REASON_CODE
  AND NVL(OEL.LOOKUP_TYPE,'CHANGE_CODE') ='CHANGE_CODE'
/
set scan on define on
prompt Creating View Data for Manual Price Changes on Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_MAN_PRICE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_MAN_PRICE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Man Price V','EXOMPV','','');
--Delete View Columns for EIS_XXWC_OM_MAN_PRICE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_MAN_PRICE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_MAN_PRICE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','FINAL_SELLING_PRICE',660,'Final Selling Price','FINAL_SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Final Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','LIST_PRICE',660,'List Price','LIST_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','List Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','QUANTITY',660,'Quantity','QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ITEM_DESCRIPTION',660,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ITEM',660,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CUSTOMER',660,'Customer','CUSTOMER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','BRANCH',660,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','PRICING_ZONE',660,'Pricing Zone','PRICING_ZONE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pricing Zone','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','APPLICATION_METHOD',660,'Application Method','APPLICATION_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Application Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ORIGINAL_UNIT_SELLING_PRICE',660,'Original Unit Selling Price','ORIGINAL_UNIT_SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Original Unit Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','REASON_CODE',660,'Reason Code','REASON_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Reason Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CAT_CLASS',660,'Cat Class','CAT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CAT_CLASS_DESC',660,'Cat Class Desc','CAT_CLASS_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','REGION',660,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','LINE_ID',660,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','LIST_LINE_TYPE_CODE',660,'List Line Type Code','LIST_LINE_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','List Line Type Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_MAN_PRICE_V','MODIFIER_TYPE',660,'Modifier Type','MODIFIER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Modifier Type','','','');
--Inserting View Components for EIS_XXWC_OM_MAN_PRICE_V
--Inserting View Component Joins for EIS_XXWC_OM_MAN_PRICE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Manual Price Changes on Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','OM CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ppf.full_name  created_by, fu.user_name user_name
  FROM FND_USER fu, per_people_f ppf
 WHERE TRUNC(SYSDATE) BETWEEN fu.START_DATE AND NVL(fu.end_date, hr_general.end_of_time)
   and TRUNC(SYSDATE) BETWEEN ppf.effective_start_date AND ppf.effective_end_date
   and fu.employee_id = ppf.person_id (+)
   and exists
          (select 1
             from fnd_user_resp_groups furg,
                  fnd_responsibility_vl fr
            where furg.responsibility_id   = fr.responsibility_id
              and fu.user_id = furg.user_id
              and fr.responsibility_name like ''XXEIS%'')','','Created By Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct attribute6 pricing_zone from mtl_parameters','','OM Pricing Zone','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'  Select Mcv.segment2 ,concatenated_segments,mcv.Description  
  From Mtl_Categories_Kfv Mcv,
       Mtl_Category_Sets Mcs
  Where  Mcs.Structure_Id                 =Mcv.Structure_Id
  AND Mcs.Category_Set_Name            =''Inventory Category''','','OM CAT CLASS','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT Flex_Val.description 
from  Mtl_Categories_Kfv Mcv,
    MTL_CATEGORY_SETS MCS,
    Mtl_Item_Categories Mic,
     Fnd_Flex_Values_Vl Flex_Val
    Where   Mcs.Category_Set_Name      =''Inventory Category''
  And Mcs.Structure_Id                 = Mcv.Structure_Id
  AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
  And Mic.Category_Id                  = Mcv.Category_Id 
  And Flex_Value                       = Mcv.SEGMENT2
  and flex_value_Set_id=1015058','','OM CAT CLASS DESC','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct organization_code org_code,organization_name org_name from org_organization_definitions','','Branch Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Manual Price Changes on Sales Orders Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_utility.delete_report_rows( 'Manual Price Changes on Sales Orders Report' );
--Inserting Report - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.r( 660,'Manual Price Changes on Sales Orders Report','','Provides visibility to order lines where the unit selling price was manually overridden.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_MAN_PRICE_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'BRANCH','Branch','Branch','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CREATED_BY','Created By','Created By','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER','Customer Name','Customer','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'FINAL_SELLING_PRICE','Final Selling Price','Final Selling Price','','~~~','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ITEM','Item','Item','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'LIST_PRICE','List Price','List Price','','~~~','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'PRICING_ZONE','Pricing Zone','Pricing Zone','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'QUANTITY','Quantity','Quantity','','~T~D~2','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'APPLICATION_METHOD','Application Method','Application Method','','','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ORIGINAL_UNIT_SELLING_PRICE','Original Unit Selling Price','Original Unit Selling Price','','~~~','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'REASON_CODE','Reason Code','Reason Code','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'ORIGINAL GM%','Original GM%','Reason Code','VARCHAR2','','right','','15','Y','','','','','','','round(case when EXOMPV.ORIGINAL_UNIT_SELLING_PRICE > 0 then ((((EXOMPV.ORIGINAL_UNIT_SELLING_PRICE-EXOMPV.AVERAGE_COST))/(EXOMPV.ORIGINAL_UNIT_SELLING_PRICE))*100) else 0 end,2)||''%''','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'FINAL SELLING PRICE GM%','Final Selling Price GM%','Reason Code','NUMBER','~~~','default','','17','Y','','','','','','','case when EXOMPV.FINAL_SELLING_PRICE>0 then ((((EXOMPV.FINAL_SELLING_PRICE-EXOMPV.AVERAGE_COST))/(EXOMPV.FINAL_SELLING_PRICE))*100) else 0 end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'DOLLARS LOST','Dollars Lost','Reason Code','NUMBER','~~~','default','','20','Y','','','','','','','(EXOMPV.ORIGINAL_UNIT_SELLING_PRICE-EXOMPV.FINAL_SELLING_PRICE)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'GM% LOST','GM% Lost','Reason Code','NUMBER','~~~','default','','21','Y','','','','','','','(case when EXOMPV.ORIGINAL_UNIT_SELLING_PRICE > 0 then ((((EXOMPV.ORIGINAL_UNIT_SELLING_PRICE-EXOMPV.AVERAGE_COST))/(EXOMPV.ORIGINAL_UNIT_SELLING_PRICE))*100) else 0 end -case when EXOMPV.FINAL_SELLING_PRICE>0 then ((((EXOMPV.FINAL_SELLING_PRICE-EXOMPV.AVERAGE_COST))/(EXOMPV.FINAL_SELLING_PRICE))*100) else 0 end)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CAT_CLASS','Cat Class','Cat Class','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'CAT_CLASS_DESC','Cat Class Desc','Cat Class Desc','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'REGION','Region','Region','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
xxeis.eis_rs_ins.rc( 'Manual Price Changes on Sales Orders Report',660,'MODIFIER_TYPE','White Cap Modifier Type','Modifier Type','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_MAN_PRICE_V','','');
--Inserting Report Parameters - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Pricing Zone','Pricing Zone','PRICING_ZONE','IN','OM Pricing Zone','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Customer Name','Customer Name','CUSTOMER','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','OM CUSTOMER NUMBER','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'From Date','From Date','ORDERED_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'To Date','To Date','ORDERED_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Created By','Created By','CREATED_BY','IN','Created By Lov','','VARCHAR2','N','Y','8','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Cat Class','Cat Class','CAT_CLASS','IN','OM CAT CLASS','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'cat Class Desc','cat Class Desc','CAT_CLASS_DESC','IN','OM CAT CLASS DESC','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Branch','Branch','BRANCH','IN','Branch Lov','','VARCHAR2','N','Y','9','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Manual Price Changes on Sales Orders Report',660,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','10','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'PRICING_ZONE','IN',':Pricing Zone','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER','IN',':Customer Name','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'ORDERED_DATE','>=',':From Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'ORDERED_DATE','<=',':To Date','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CREATED_BY','IN',':Created By','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CAT_CLASS','IN',':Cat Class','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'CAT_CLASS_DESC','IN',':cat Class Desc','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'BRANCH','IN',':Branch','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Manual Price Changes on Sales Orders Report',660,'REGION','IN',':Region','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rs( 'Manual Price Changes on Sales Orders Report',660,'PRICING_ZONE','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Manual Price Changes on Sales Orders Report',660,'CUSTOMER','ASC','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rs( 'Manual Price Changes on Sales Orders Report',660,'CREATED_BY','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Manual Price Changes on Sales Orders Report
--Inserting Report Templates - Manual Price Changes on Sales Orders Report
--Inserting Report Portals - Manual Price Changes on Sales Orders Report
--Inserting Report Dashboards - Manual Price Changes on Sales Orders Report
--Inserting Report Security - Manual Price Changes on Sales Orders Report
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50926',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50927',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50928',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50929',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50931',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50930',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','LC053655','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','10010432','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','RB054040','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','RV003897','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','SS084202','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','','SO004816','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50870',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50871',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','660','','50869',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Manual Price Changes on Sales Orders Report','20005','','50900',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Manual Price Changes on Sales Orders Report
END;
/
set scan on define on
