--Report Name            : New Item Report DataPull - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for New Item Report DataPull - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_NEW_ITEM_DATA_RPT_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V',401,'','','','','SA059956','XXEIS','Eis Xxwc New Item Data Rpt V','EXNIDRV','','');
--Delete View Columns for EIS_XXWC_NEW_ITEM_DATA_RPT_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_NEW_ITEM_DATA_RPT_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_NEW_ITEM_DATA_RPT_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','ITEM_CREATED_BY',401,'Item Created By','ITEM_CREATED_BY','','','','SA059956','VARCHAR2','','','Item Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','CREATION_DATE',401,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','WEIGHT_UOM',401,'Weight Uom','WEIGHT_UOM','','','','SA059956','VARCHAR2','','','Weight Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','WEIGHT',401,'Weight','WEIGHT','','','','SA059956','NUMBER','','','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','SHELF_LIFE_DAYS',401,'Shelf Life Days','SHELF_LIFE_DAYS','','','','SA059956','NUMBER','','','Shelf Life Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','COUNTRY_OF_ORIGIN',401,'Country Of Origin','COUNTRY_OF_ORIGIN','','','','SA059956','VARCHAR2','','','Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','HAZMAT_FLAG',401,'Hazmat Flag','HAZMAT_FLAG','','','','SA059956','VARCHAR2','','','Hazmat Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','SELLING_UOM',401,'Selling Uom','SELLING_UOM','','','','SA059956','VARCHAR2','','','Selling Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','SUGGESTED_RETAIL_PRICE',401,'Suggested Retail Price','SUGGESTED_RETAIL_PRICE','','','','SA059956','VARCHAR2','','','Suggested Retail Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','XPAR',401,'Xpar','XPAR','','','','SA059956','VARCHAR2','','','Xpar','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','TIME_SENSITIVE',401,'Time Sensitive','TIME_SENSITIVE','','','','SA059956','VARCHAR2','','','Time Sensitive','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','CATMGT_SUBCATEGORY_DESC',401,'Catmgt Subcategory Desc','CATMGT_SUBCATEGORY_DESC','','','','SA059956','VARCHAR2','','','Catmgt Subcategory Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','CATMGT_CATEGORY_DESC',401,'Catmgt Category Desc','CATMGT_CATEGORY_DESC','','','','SA059956','VARCHAR2','','','Catmgt Category Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','CATEGORY_CLASS',401,'Category Class','CATEGORY_CLASS','','','','SA059956','VARCHAR2','','','Category Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','PO_COST',401,'Po Cost','PO_COST','','','','SA059956','NUMBER','','','Po Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','VENDOR_TIER',401,'Vendor Tier','VENDOR_TIER','','','','SA059956','VARCHAR2','','','Vendor Tier','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','MASTER_VENDOR_#',401,'Master Vendor #','MASTER_VENDOR_#','','','','SA059956','VARCHAR2','','','Master Vendor #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','VENDOR_PART_#',401,'Vendor Part #','VENDOR_PART_#','','','','SA059956','VARCHAR2','','','Vendor Part #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','PRIMARY_UOM',401,'Primary Uom','PRIMARY_UOM','','','','SA059956','VARCHAR2','','','Primary Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','ITEM_LEVEL',401,'Item Level','ITEM_LEVEL','','','','SA059956','VARCHAR2','','','Item Level','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','SA059956','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','ITEM_NUM',401,'Item Num','ITEM_NUM','','','','SA059956','VARCHAR2','','','Item Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','ITEM_STATUS',401,'Item Status','ITEM_STATUS','','','','SA059956','VARCHAR2','','','Item Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','ITEM_TYPE',401,'Item Type','ITEM_TYPE','','','','SA059956','VARCHAR2','','','Item Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','LOCATION_NAME',401,'Location Name','LOCATION_NAME','','','','SA059956','VARCHAR2','','','Location Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','APPROVAL_DATE',401,'Approval Date','APPROVAL_DATE','','','','SA059956','DATE','','','Approval Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','APPROVAL_STATUS',401,'Approval Status','APPROVAL_STATUS','','','','SA059956','VARCHAR2','','','Approval Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','APPROVAL_STATUS_TYPE',401,'Approval Status Type','APPROVAL_STATUS_TYPE','','','','SA059956','NUMBER','','','Approval Status Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','CHANGE_ID',401,'Change Id','CHANGE_ID','','','','SA059956','NUMBER','','','Change Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','CHANGE_ORDER_NUMBER',401,'Change Order Number','CHANGE_ORDER_NUMBER','','','','SA059956','VARCHAR2','','','Change Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','STATUS_NAME',401,'Status Name','STATUS_NAME','','','','SA059956','VARCHAR2','','','Status Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_NEW_ITEM_DATA_RPT_V','STATUS_TYPE',401,'Status Type','STATUS_TYPE','','','','SA059956','NUMBER','','','Status Type','','','');
--Inserting View Components for EIS_XXWC_NEW_ITEM_DATA_RPT_V
--Inserting View Component Joins for EIS_XXWC_NEW_ITEM_DATA_RPT_V
END;
/
set scan on define on
prompt Creating Report Data for New Item Report DataPull - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - New Item Report DataPull - WC
xxeis.eis_rs_utility.delete_report_rows( 'New Item Report DataPull - WC' );
--Inserting Report - New Item Report DataPull - WC
xxeis.eis_rs_ins.r( 401,'New Item Report DataPull - WC','','The  purpose of this report is to provide a listing of new items(Daily) built into the system to determine if any further action is needed by EH&S, Safety, Pricing, and Category Managers.','','','','SA059956','EIS_XXWC_NEW_ITEM_DATA_RPT_V','Y','','','SA059956','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - New Item Report DataPull - WC
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'CATEGORY_CLASS','Category Class','Category Class','','','','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'CATMGT_CATEGORY_DESC','Catmgt Category Desc','Catmgt Category Desc','','','','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'CATMGT_SUBCATEGORY_DESC','Catmgt Subcategory Desc','Catmgt Subcategory Desc','','','','','14','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'COUNTRY_OF_ORIGIN','Country Of Origin','Country Of Origin','','','','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'CREATION_DATE','Creation Date','Creation Date','','','','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'DESCRIPTION','Description','Description','','','','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'HAZMAT_FLAG','Hazmat Flag','Hazmat Flag','','','','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'ITEM_CREATED_BY','Item Created By','Item Created By','','','','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'ITEM_LEVEL','Item Level','Item Level','','','','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'ITEM_NUM','Item Num','Item Num','','','','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'ITEM_STATUS','Item Status','Item Status','','','','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'ITEM_TYPE','Item Type','Item Type','','','','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'MASTER_VENDOR_#','Master Vendor #','Master Vendor #','','','','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'PO_COST','Po Cost','Po Cost','','','','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'PRIMARY_UOM','Primary Uom','Primary Uom','','','','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'SELLING_UOM','Selling Uom','Selling Uom','','','','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'SHELF_LIFE_DAYS','Shelf Life Days','Shelf Life Days','','','','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'SUGGESTED_RETAIL_PRICE','Suggested Retail Price','Suggested Retail Price','','','','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'TIME_SENSITIVE','Time Sensitive','Time Sensitive','','','','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'VENDOR_NAME','Vendor Name','Vendor Name','','','','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'VENDOR_PART_#','Vendor Part #','Vendor Part #','','','','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'VENDOR_TIER','Vendor Tier','Vendor Tier','','','','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'WEIGHT','Weight','Weight','','','','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'WEIGHT_UOM','Weight Uom','Weight Uom','','','','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'XPAR','Xpar','Xpar','','','','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'LOCATION_NAME','Location Name','Location Name','','','','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
xxeis.eis_rs_ins.rc( 'New Item Report DataPull - WC',401,'APPROVAL_DATE','Approval Date','Approval Date','','','','','27','N','','','','','','','','SA059956','N','N','','EIS_XXWC_NEW_ITEM_DATA_RPT_V','','');
--Inserting Report Parameters - New Item Report DataPull - WC
--Inserting Report Conditions - New Item Report DataPull - WC
--Inserting Report Sorts - New Item Report DataPull - WC
xxeis.eis_rs_ins.rs( 'New Item Report DataPull - WC',401,'ITEM_TYPE','ASC','SA059956','1','');
xxeis.eis_rs_ins.rs( 'New Item Report DataPull - WC',401,'CREATION_DATE','ASC','SA059956','2','');
xxeis.eis_rs_ins.rs( 'New Item Report DataPull - WC',401,'ITEM_NUM','ASC','SA059956','3','');
--Inserting Report Triggers - New Item Report DataPull - WC
--Inserting Report Templates - New Item Report DataPull - WC
--Inserting Report Portals - New Item Report DataPull - WC
--Inserting Report Dashboards - New Item Report DataPull - WC
--Inserting Report Security - New Item Report DataPull - WC
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC','20005','','50900',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC','401','','50884',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC','401','','50883',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC','661','','50891',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC','20005','','51713',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC','401','','50990',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC','20005','','51711',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC','201','','50892',401,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'New Item Report DataPull - WC','20005','','50897',401,'SA059956','','');
--Inserting Report Pivots - New Item Report DataPull - WC
END;
/
set scan on define on
