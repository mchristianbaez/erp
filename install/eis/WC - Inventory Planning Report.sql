--Report Name            : WC - Inventory Planning Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_451485_HVBVZL_V
--Connecting to schema APPS
accept SID prompt "Please enter database ( connect string )  : "
accept apps_password prompt "Please enter APPS password : " hide
connect APPS/@
set scan off define off
prompt Creating View APPS.XXEIS_451485_HVBVZL_V
Create or replace View APPS.XXEIS_451485_HVBVZL_V
 AS 
select "ITEM_SEGMENTS","DESCRIPTION","ERROR","SORTEE","SUBINVENTORY_CODE","MIN_QTY","MAX_QTY","ONHAND_QTY","SUPPLY_QTY","DEMAND_QTY","TOT_AVAIL_QTY","MIN_ORD_QTY","MAX_ORD_QTY","FIX_MULT","REORD_QTY","CREATION_DATE","REQUEST_ID","ORGANIZATION_CODE","INVENTORY_ITEM_ID","ORGANIZATION_ID","INVENTORY_PLANNING_CODE","SAFETY_STOCK","REORDER_POINT","LEAD_TIME_DEMAND","AMU","RESERVE_STOCK","EOQ","PROCESS_LEAD_TIME","LEAD_TIME","REVIEW_TIME" from XXWC.XXWC_INV_MIN_MAX_TEMP/
set scan on define on
prompt Creating View Data for WC - Inventory Planning Report
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_451485_HVBVZL_V
xxeis.eis_rs_ins.v( 'XXEIS_451485_HVBVZL_V',401,'Paste SQL View for Inventory Planning Report','1.0','','','HT038687','APPS','Inventory Planning Report View','X4HV','','');
--Delete View Columns for XXEIS_451485_HVBVZL_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_451485_HVBVZL_V',401,FALSE);
--Inserting View Columns for XXEIS_451485_HVBVZL_V
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','ITEM_SEGMENTS',401,'','','','','','HT038687','VARCHAR2','','','Item Segments','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','DESCRIPTION',401,'','','','','','HT038687','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','MIN_QTY',401,'','','','','','HT038687','NUMBER','','','Min Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','MAX_QTY',401,'','','','','','HT038687','NUMBER','','','Max Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','ONHAND_QTY',401,'','','','','','HT038687','NUMBER','','','Onhand Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','SUPPLY_QTY',401,'','','','','','HT038687','NUMBER','','','Supply Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','DEMAND_QTY',401,'','','','','','HT038687','NUMBER','','','Demand Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','TOT_AVAIL_QTY',401,'','','','','','HT038687','NUMBER','','','Tot Avail Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','FIX_MULT',401,'','','','','','HT038687','NUMBER','','','Fix Mult','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','REORD_QTY',401,'','','','','','HT038687','NUMBER','','','Reord Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','CREATION_DATE',401,'','','','','','HT038687','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','ORGANIZATION_CODE',401,'','','','','','HT038687','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','INVENTORY_PLANNING_CODE',401,'','','','','','HT038687','NUMBER','','','Inventory Planning Code','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','SAFETY_STOCK',401,'','','','','','HT038687','NUMBER','','','Safety Stock','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','REORDER_POINT',401,'','','','','','HT038687','NUMBER','','','Reorder Point','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','AMU',401,'','','','','','HT038687','NUMBER','','','Amu','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','ERROR',401,'','','','','','HT038687','VARCHAR2','','','Error','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','SORTEE',401,'','','','','','HT038687','VARCHAR2','','','Sortee','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','SUBINVENTORY_CODE',401,'','','','','','HT038687','VARCHAR2','','','Subinventory Code','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','MIN_ORD_QTY',401,'','','','','','HT038687','NUMBER','','','Min Ord Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','MAX_ORD_QTY',401,'','','','','','HT038687','NUMBER','','','Max Ord Qty','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','REQUEST_ID',401,'','','','','','HT038687','NUMBER','','','Request Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','INVENTORY_ITEM_ID',401,'','','','','','HT038687','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','ORGANIZATION_ID',401,'','','','','','HT038687','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','LEAD_TIME_DEMAND',401,'','','','','','HT038687','NUMBER','','','Lead Time Demand','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','RESERVE_STOCK',401,'','','','','','HT038687','NUMBER','','','Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','EOQ',401,'','','','','','HT038687','NUMBER','','','Eoq','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','PROCESS_LEAD_TIME',401,'','','','','','HT038687','NUMBER','','','Process Lead Time','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','LEAD_TIME',401,'','','','','','HT038687','NUMBER','','','Lead Time','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_451485_HVBVZL_V','REVIEW_TIME',401,'','','','','','HT038687','NUMBER','','','Review Time','','','');
--Inserting View Components for XXEIS_451485_HVBVZL_V
--Inserting View Component Joins for XXEIS_451485_HVBVZL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for WC - Inventory Planning Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - WC - Inventory Planning Report
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code, organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','EIS XXWC INV ORGS','XXWC list of inventort orgs','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select ''1'' code, ''Re-order Point'' description
  from dual
  union select ''2'' code, ''Min-Max'' description
  from dual
  union select ''6'' code, ''Not Planned'' description
  from dual','','EIS_INV_PLANNING_CODE','Static LOV for Inv Planning Code.
Possible Values: 
1 - Reorder Point, 
2 - Min-Max
6 - Not Planned
','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for WC - Inventory Planning Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC - Inventory Planning Report
xxeis.eis_rs_utility.delete_report_rows( 'WC - Inventory Planning Report' );
--Inserting Report - WC - Inventory Planning Report
xxeis.eis_rs_ins.r( 401,'WC - Inventory Planning Report','','Inventory Min Max / Re-order Point Planning Report','','','','HT038687','XXEIS_451485_HVBVZL_V','Y','','select * from XXWC.XXWC_INV_MIN_MAX_TEMP
','HT038687','','N','White Cap Reports','','EXCEL,','N');
--Inserting Report Columns - WC - Inventory Planning Report
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'ITEM_SEGMENTS','Item Segments','','','','default','','2','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'DESCRIPTION','Description','','','','default','','4','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'MIN_QTY','Min Qty','','','~~~','default','','6','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'MAX_QTY','Max Qty','','','~~~','default','','7','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'ONHAND_QTY','Onhand Qty','','','~~~','default','','8','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'SUPPLY_QTY','Supply Qty','','','~~~','default','','9','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'DEMAND_QTY','Demand Qty','','','~~~','default','','10','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'TOT_AVAIL_QTY','Tot Avail Qty','','','~~~','default','','11','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'FIX_MULT','Fix Mult','','','~~~','default','','13','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'REORD_QTY','Reord Qty','','','~~~','default','','12','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'CREATION_DATE','Creation Date','','','','default','','3','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'ORGANIZATION_CODE','Organization Code','','','','default','','1','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'INVENTORY_PLANNING_CODE','Inventory Planning Code','','','~~~','default','','5','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'SAFETY_STOCK','Safety Stock','','','~~~','default','','15','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'REORDER_POINT','Reorder Point','','','~~~','default','','14','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
xxeis.eis_rs_ins.rc( 'WC - Inventory Planning Report',401,'AMU','Amu','','','~~~','default','','16','N','','','','','','','','HT038687','N','N','','XXEIS_451485_HVBVZL_V','','');
--Inserting Report Parameters - WC - Inventory Planning Report
xxeis.eis_rs_ins.rp( 'WC - Inventory Planning Report',401,'Creation Date From','','CREATION_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','HT038687','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC - Inventory Planning Report',401,'Inventory Planning Code (1-Reorder Point, 2-Min-Max,6-Not Planned)','Inventory_Planning_Code (1-Reorder Point, 2-Min-Max,6-Not Planned)','INVENTORY_PLANNING_CODE','IN','EIS_INV_PLANNING_CODE','','NUMERIC','N','Y','4','','Y','CONSTANT','HT038687','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC - Inventory Planning Report',401,'Organization Code','','ORGANIZATION_CODE','IN','EIS XXWC INV ORGS','','VARCHAR2','Y','Y','1','','Y','CONSTANT','HT038687','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC - Inventory Planning Report',401,'Creation Date To','','CREATION_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','HT038687','Y','N','','','');
--Inserting Report Conditions - WC - Inventory Planning Report
xxeis.eis_rs_ins.rcn( 'WC - Inventory Planning Report',401,'ORGANIZATION_CODE','IN',':Organization Code','','','Y','1','Y','HT038687');
xxeis.eis_rs_ins.rcn( 'WC - Inventory Planning Report',401,'INVENTORY_PLANNING_CODE','IN',':Inventory Planning Code (1-Reorder Point, 2-Min-Max,6-Not Planned)','','','Y','4','Y','HT038687');
xxeis.eis_rs_ins.rcn( 'WC - Inventory Planning Report',401,'CREATION_DATE','>=',':Creation Date From','','','Y','2','Y','HT038687');
xxeis.eis_rs_ins.rcn( 'WC - Inventory Planning Report',401,'CREATION_DATE','<=',':Creation Date To','','','Y','3','Y','HT038687');
--Inserting Report Sorts - WC - Inventory Planning Report
xxeis.eis_rs_ins.rs( 'WC - Inventory Planning Report',401,'CREATION_DATE','ASC','HT038687','','');
xxeis.eis_rs_ins.rs( 'WC - Inventory Planning Report',401,'ORGANIZATION_CODE','ASC','HT038687','','');
xxeis.eis_rs_ins.rs( 'WC - Inventory Planning Report',401,'ITEM_SEGMENTS','ASC','HT038687','','');
--Inserting Report Triggers - WC - Inventory Planning Report
--Inserting Report Templates - WC - Inventory Planning Report
--Inserting Report Portals - WC - Inventory Planning Report
--Inserting Report Dashboards - WC - Inventory Planning Report
--Inserting Report Security - WC - Inventory Planning Report
xxeis.eis_rs_ins.rsec( 'WC - Inventory Planning Report','201','','50921',401,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Planning Report','401','','50981',401,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC - Inventory Planning Report','20005','','50900',401,'HT038687','','');
--Inserting Report Pivots - WC - Inventory Planning Report
END;
/
set scan on define on
