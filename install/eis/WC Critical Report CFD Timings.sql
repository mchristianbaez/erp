--Report Name            : WC Critical Report CFD Timings
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for WC Critical Report CFD Timings
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_FND_CONC_REQ_DET_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_FND_CONC_REQ_DET_V',85000,'','','','','MR020532','XXEIS','Eis Xxwc Fnd Conc Req Det V','EXFCRDV','','');
--Delete View Columns for EIS_XXWC_FND_CONC_REQ_DET_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_FND_CONC_REQ_DET_V',85000,FALSE);
--Inserting View Columns for EIS_XXWC_FND_CONC_REQ_DET_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','CONCURRENT_MANAGER',85000,'Concurrent Manager','CONCURRENT_MANAGER','','','','MR020532','VARCHAR2','','','Concurrent Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','PRINTER',85000,'Printer','PRINTER','','','','MR020532','VARCHAR2','','','Printer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','ARGUMENT_TEXT',85000,'Argument Text','ARGUMENT_TEXT','','','','MR020532','VARCHAR2','','','Argument Text','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','STATUS_CODE',85000,'Status Code','STATUS_CODE','','','','MR020532','VARCHAR2','','','Status Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','PHASE_CODE',85000,'Phase Code','PHASE_CODE','','','','MR020532','VARCHAR2','','','Phase Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','USER_NAME',85000,'User Name','USER_NAME','','','','MR020532','VARCHAR2','','','User Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','ACTUAL_COMPLETION_DATE',85000,'Actual Completion Date','ACTUAL_COMPLETION_DATE','','','','MR020532','VARCHAR2','','','Actual Completion Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','ACTUAL_START_DATE',85000,'Actual Start Date','ACTUAL_START_DATE','','','','MR020532','VARCHAR2','','','Actual Start Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','REQUEST_DATE',85000,'Request Date','REQUEST_DATE','','','','MR020532','VARCHAR2','','','Request Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','END_TO_END',85000,'End To End','END_TO_END','','','','MR020532','NUMBER','','','End To End','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','RUNNING_TIME',85000,'Running Time','RUNNING_TIME','','','','MR020532','NUMBER','','','Running Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','PENDING_NORMAL_TIME',85000,'Pending Normal Time','PENDING_NORMAL_TIME','','','','MR020532','NUMBER','','','Pending Normal Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','REQUEST_ID',85000,'Request Id','REQUEST_ID','','','','MR020532','NUMBER','','','Request Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_FND_CONC_REQ_DET_V','USER_CONC_PROG_NAME',85000,'User Conc Prog Name','USER_CONC_PROG_NAME','','','','MR020532','VARCHAR2','','','User Conc Prog Name','','','');
--Inserting View Components for EIS_XXWC_FND_CONC_REQ_DET_V
--Inserting View Component Joins for EIS_XXWC_FND_CONC_REQ_DET_V
END;
/
set scan on define on
prompt Creating Report Data for WC Critical Report CFD Timings
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC Critical Report CFD Timings
xxeis.eis_rs_utility.delete_report_rows( 'WC Critical Report CFD Timings' );
--Inserting Report - WC Critical Report CFD Timings
xxeis.eis_rs_ins.r( 85000,'WC Critical Report CFD Timings','','This report will be used to extract the CFD timings after PROD migration which has impacted the CFD�s
Business Risk: This helps us to make sure the reports are running within the time frame
Requirement - Need to the ability to pull the CFD timings for the Critical CFD�s like
a.	XXWC OM Sales Receipt Report
b.	XXWC OM Customer Sales Receipt Report
c.	XXWC OM Pick Slip
d.	XXWC OM Packing Slip','','','','MR020532','EIS_XXWC_FND_CONC_REQ_DET_V','Y','','','MR020532','','N','WC Knowledge Management Reports','PDF,','CSV,EXCEL,','N');
--Inserting Report Columns - WC Critical Report CFD Timings
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'ACTUAL_COMPLETION_DATE','Actual Completion Date','Actual Completion Date','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'ACTUAL_START_DATE','Actual Start Date','Actual Start Date','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'ARGUMENT_TEXT','Argument Text','Argument Text','','','default','','12','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'CONCURRENT_MANAGER','Concurrent Manager','Concurrent Manager','','','default','','14','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'END_TO_END','End To End','End To End','','~~~','default','','5','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'PENDING_NORMAL_TIME','Pending Normal Time','Pending Normal Time','','~~~','default','','3','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'PHASE_CODE','Phase Code','Phase Code','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'PRINTER','Printer','Printer','','','default','','13','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'REQUEST_DATE','Request Date','Request Date','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'REQUEST_ID','Request Id','Request Id','','~~~','default','','2','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'RUNNING_TIME','Running Time','Running Time','','~~~','default','','4','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'STATUS_CODE','Status Code','Status Code','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'USER_CONC_PROG_NAME','User Concurrent Program Name','User Conc Prog Name','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
xxeis.eis_rs_ins.rc( 'WC Critical Report CFD Timings',85000,'USER_NAME','User Name','User Name','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_XXWC_FND_CONC_REQ_DET_V','','');
--Inserting Report Parameters - WC Critical Report CFD Timings
--Inserting Report Conditions - WC Critical Report CFD Timings
--Inserting Report Sorts - WC Critical Report CFD Timings
--Inserting Report Triggers - WC Critical Report CFD Timings
--Inserting Report Templates - WC Critical Report CFD Timings
--Inserting Report Portals - WC Critical Report CFD Timings
--Inserting Report Dashboards - WC Critical Report CFD Timings
--Inserting Report Security - WC Critical Report CFD Timings
xxeis.eis_rs_ins.rsec( 'WC Critical Report CFD Timings','20005','','50900',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC Critical Report CFD Timings','660','','51045',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC Critical Report CFD Timings','660','','50901',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC Critical Report CFD Timings','660','','51025',85000,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'WC Critical Report CFD Timings','660','','50860',85000,'MR020532','','');
--Inserting Report Pivots - WC Critical Report CFD Timings
END;
/
set scan on define on
