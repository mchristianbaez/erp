--Report Name            : Promotional Tracking Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_OM_PROM_TRAC_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_OM_PROM_TRAC_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_OM_PROM_TRAC_V',660,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Om Prom Trac V','EXOPTV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_OM_PROM_TRAC_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_OM_PROM_TRAC_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_OM_PROM_TRAC_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','ANONYMOUS','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','ANONYMOUS','NUMBER','','','Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','MODIFIER_DESC',660,'Modifier Desc','MODIFIER_DESC','','','','ANONYMOUS','VARCHAR2','','','Modifier Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','MODIFIER_NAME',660,'Modifier Name','MODIFIER_NAME','','','','ANONYMOUS','VARCHAR2','','','Modifier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','MODIFIER_TYPE',660,'Modifier Type','MODIFIER_TYPE','','','','ANONYMOUS','VARCHAR2','','','Modifier Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','ANONYMOUS','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','ANONYMOUS','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','ORDER_LINE',660,'Order Line','ORDER_LINE','','','','ANONYMOUS','VARCHAR2','','','Order Line','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Unit Selling Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','UOM',660,'Uom','UOM','','','','ANONYMOUS','VARCHAR2','','','Uom','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','QTY',660,'Qty','QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','ITEM_DESC',660,'Item Desc','ITEM_DESC','','','','ANONYMOUS','VARCHAR2','','','Item Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','ITEM',660,'Item','ITEM','','','','ANONYMOUS','VARCHAR2','','','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','PAYMENT_DESC',660,'Payment Desc','PAYMENT_DESC','','','','ANONYMOUS','VARCHAR2','','','Payment Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','PAYMENT_TERM',660,'Payment Term','PAYMENT_TERM','','','','ANONYMOUS','VARCHAR2','','','Payment Term','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','SALE_COST',660,'Sale Cost','SALE_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Sale Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','SALES',660,'Sales','SALES','','','','ANONYMOUS','NUMBER','','','Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','INV_AMOUNT',660,'Inv Amount','INV_AMOUNT','','~T~D~2','','ANONYMOUS','NUMBER','','','Inv Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','LOCATION_NAME',660,'Location Name','LOCATION_NAME','','','','ANONYMOUS','VARCHAR2','','','Location Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','LOCATION',660,'Location','LOCATION','','','','ANONYMOUS','VARCHAR2','','','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','SALESREP_NUMBER',660,'Salesrep Number','SALESREP_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Salesrep Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','TRX_NUMBER',660,'Trx Number','TRX_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Trx Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','ANONYMOUS','VARCHAR2','','','Salesrep Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','PROFIT',660,'Profit','PROFIT','','','','ANONYMOUS','NUMBER','','','Profit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','EXTENDED_COST',660,'Extended Cost','EXTENDED_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Extended Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Average Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','FREIGHT_AMT',660,'Freight Amt','FREIGHT_AMT','','~T~D~2','','ANONYMOUS','NUMBER','','','Freight Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','TAX_VALUE',660,'Tax Value','TAX_VALUE','','~T~D~2','','ANONYMOUS','NUMBER','','','Tax Value','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','DISCOUNT_AMT',660,'Discount Amt','DISCOUNT_AMT','','~T~D~2','','ANONYMOUS','NUMBER','','','Discount Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','EXTENDED_SALE',660,'Extended Sale','EXTENDED_SALE','','','','ANONYMOUS','NUMBER','','','Extended Sale','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','SHIP_QTY',660,'Ship Qty','SHIP_QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Ship Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','ANONYMOUS','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','ORDER_STATUS',660,'Order Status','ORDER_STATUS','','','','ANONYMOUS','VARCHAR2','','','Order Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','ANONYMOUS','VARCHAR2','','','Order Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','CREATED_BY',660,'Created By','CREATED_BY','','','','ANONYMOUS','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','ANONYMOUS','DATE','','','Ordered Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','CODE_COMBINATION_ID',660,'Code Combination Id','CODE_COMBINATION_ID','','','','ANONYMOUS','NUMBER','','','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','CUSTOMER_TRX_ID',660,'Customer Trx Id','CUSTOMER_TRX_ID','','','','ANONYMOUS','NUMBER','','','Customer Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','CUSTOMER_TRX_LINE_ID',660,'Customer Trx Line Id','CUSTOMER_TRX_LINE_ID','','','','ANONYMOUS','NUMBER','','','Customer Trx Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','GL_CODING',660,'Gl Coding','GL_CODING','','','','ANONYMOUS','VARCHAR2','','','Gl Coding','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','GL_STRING',660,'Gl String','GL_STRING','','','','ANONYMOUS','VARCHAR2','','','Gl String','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','LINE_ID',660,'Line Id','LINE_ID','','','','ANONYMOUS','NUMBER','','','Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','LOC',660,'Loc','LOC','','','','ANONYMOUS','VARCHAR2','','','Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','MSI_ORGANIZATION_ID',660,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Msi Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_OM_PROM_TRAC_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Order Number','','','','US');
--Inserting Object Components for EIS_XXWC_OM_PROM_TRAC_V
--Inserting Object Component Joins for EIS_XXWC_OM_PROM_TRAC_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Promotional Tracking Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Promotional Tracking Report
xxeis.eis_rsc_ins.lov( 660,'select distinct pov.segment1 vendor_number, pov.vendor_name vendor_name  from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC VENDOR NUMBER','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct  pov.vendor_name , pov.segment1 vendor_number from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC Vendor Name','','ANONYMOUS',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT NAME  MODIFIER_NAME , DESCRIPTION MODIFIER_DESCRIPTION FROM  QP_SECU_LIST_HEADERS_VL D
WHERE  VIEW_FLAG = ''Y''','','XXWC Modifer Name','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT  DESCRIPTION MODIFIER_DESCRIPTION , NAME  MODIFIER_NAME FROM  QP_SECU_LIST_HEADERS_VL D
WHERE  VIEW_FLAG = ''Y''','','XXWC Modifier Desc','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'select distinct attribute10 Modifier_type from qp_list_headers_vl','','XXWC Modifier Type','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Promotional Tracking Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Promotional Tracking Report
xxeis.eis_rsc_utility.delete_report_rows( 'Promotional Tracking Report' );
--Inserting Report - Promotional Tracking Report
xxeis.eis_rsc_ins.r( 660,'Promotional Tracking Report','','This report provides visibility into all sales associated with supplier and  White Cap sponsored promotions, but  is defaulted to supplier-sponsored promotions for the purpose of managing promotional reimbursements.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_PROM_TRAC_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Promotional Tracking Report
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'AVERAGE_COST','Avg Cost','Average Cost','','~T~D~2','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'EXTENDED_COST','Extended Avg Cost','Extended Cost','','~T~D~2','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'EXTENDED_SALE','Extended Sale','Extended Sale','','~T~D~2','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'ITEM','Product Number','Item','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'ITEM_DESC','Product Description','Item Desc','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'LOCATION','Location','Location','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'LOCATION_NAME','Location Name','Location Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'MODIFIER_DESC','Promo Description','Modifier Desc','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'MODIFIER_NAME','Promo Code','Modifier Name','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'MODIFIER_TYPE','Modifier Type','Modifier Type','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'SHIP_QTY','QTY Shipped','Ship Qty','','~T~D~0','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'TRX_NUMBER','Invoice Number','Trx Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'UNIT_SELLING_PRICE','Sales Per Unit','Unit Selling Price','','~T~D~2','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'VENDOR_NAME','Supplier','Vendor Name','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'GL_CODING','Gl Coding','Gl Coding','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'GL_STRING','Gl String','Gl String','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Promotional Tracking Report',660,'ORDER_NUMBER','Order Number','Order Number','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_PROM_TRAC_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Promotional Tracking Report
xxeis.eis_rsc_ins.rp( 'Promotional Tracking Report',660,'Invoice From Date','Invoice From Date','INVOICE_DATE','>=','','','DATE','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_PROM_TRAC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Promotional Tracking Report',660,'Invoice To Date','Invoice To Date','INVOICE_DATE','<=','','','DATE','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_PROM_TRAC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Promotional Tracking Report',660,'Supplier Name','Supplier Name','VENDOR_NAME','IN','XXWC Vendor Name','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_PROM_TRAC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Promotional Tracking Report',660,'Supplier Number','Supplier Number','VENDOR_NUMBER','IN','XXWC VENDOR NUMBER','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_PROM_TRAC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Promotional Tracking Report',660,'Promo Code','Promo Code','MODIFIER_NAME','IN','XXWC Modifer Name','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_PROM_TRAC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Promotional Tracking Report',660,'Promo Description','Promo Description','MODIFIER_DESC','IN','XXWC Modifier Desc','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_PROM_TRAC_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Promotional Tracking Report',660,'White Cap Modifier Type','White Cap Modifier Type','MODIFIER_TYPE','IN','XXWC Modifier Type','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_OM_PROM_TRAC_V','','','US','');
--Inserting Dependent Parameters - Promotional Tracking Report
--Inserting Report Conditions - Promotional Tracking Report
xxeis.eis_rsc_ins.rcnh( 'Promotional Tracking Report',660,'INVOICE_DATE >= :Invoice From Date ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_DATE','','Invoice From Date','','','','','EIS_XXWC_OM_PROM_TRAC_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',660,'Promotional Tracking Report','INVOICE_DATE >= :Invoice From Date ');
xxeis.eis_rsc_ins.rcnh( 'Promotional Tracking Report',660,'INVOICE_DATE <= :Invoice To Date ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_DATE','','Invoice To Date','','','','','EIS_XXWC_OM_PROM_TRAC_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',660,'Promotional Tracking Report','INVOICE_DATE <= :Invoice To Date ');
xxeis.eis_rsc_ins.rcnh( 'Promotional Tracking Report',660,'MODIFIER_DESC IN :Promo Description ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','MODIFIER_DESC','','Promo Description','','','','','EIS_XXWC_OM_PROM_TRAC_V','','','','','','IN','Y','Y','','','','','1',660,'Promotional Tracking Report','MODIFIER_DESC IN :Promo Description ');
xxeis.eis_rsc_ins.rcnh( 'Promotional Tracking Report',660,'MODIFIER_NAME IN :Promo Code ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','MODIFIER_NAME','','Promo Code','','','','','EIS_XXWC_OM_PROM_TRAC_V','','','','','','IN','Y','Y','','','','','1',660,'Promotional Tracking Report','MODIFIER_NAME IN :Promo Code ');
xxeis.eis_rsc_ins.rcnh( 'Promotional Tracking Report',660,'MODIFIER_TYPE IN :White Cap Modifier Type ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','MODIFIER_TYPE','','White Cap Modifier Type','','','','','EIS_XXWC_OM_PROM_TRAC_V','','','','','','IN','Y','Y','','','','','1',660,'Promotional Tracking Report','MODIFIER_TYPE IN :White Cap Modifier Type ');
xxeis.eis_rsc_ins.rcnh( 'Promotional Tracking Report',660,'VENDOR_NAME IN :Supplier Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Supplier Name','','','','','EIS_XXWC_OM_PROM_TRAC_V','','','','','','IN','Y','Y','','','','','1',660,'Promotional Tracking Report','VENDOR_NAME IN :Supplier Name ');
xxeis.eis_rsc_ins.rcnh( 'Promotional Tracking Report',660,'VENDOR_NUMBER IN :Supplier Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NUMBER','','Supplier Number','','','','','EIS_XXWC_OM_PROM_TRAC_V','','','','','','IN','Y','Y','','','','','1',660,'Promotional Tracking Report','VENDOR_NUMBER IN :Supplier Number ');
--Inserting Report Sorts - Promotional Tracking Report
--Inserting Report Triggers - Promotional Tracking Report
--inserting report templates - Promotional Tracking Report
--Inserting Report Portals - Promotional Tracking Report
--inserting report dashboards - Promotional Tracking Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Promotional Tracking Report','660','EIS_XXWC_OM_PROM_TRAC_V','EIS_XXWC_OM_PROM_TRAC_V','N','');
--inserting report security - Promotional Tracking Report
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','XXWC_RENTAL_OM_PRICING_SUPER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','XXWC_ORDER_MGMT_READ_SHIPPING',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','XXWC_ORDER_MGMT_READ_ONLY_COST',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','XXWC_ORDER_MGMT_READ_ONLY',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','660','','',660,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Promotional Tracking Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',660,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - Promotional Tracking Report
--Inserting Report   Version details- Promotional Tracking Report
xxeis.eis_rsc_ins.rv( 'Promotional Tracking Report','','Promotional Tracking Report','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
