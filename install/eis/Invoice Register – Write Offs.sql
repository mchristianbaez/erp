--Report Name            : Invoice Register - Write Offs
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_AR_INVOICE_REGISTER_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_INVOICE_REGISTER_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Invoice Register V','EXAIRV','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_INVOICE_REGISTER_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SHIP_VIA',222,'Ship Via','SHIP_VIA','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Via','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SHIP_TO_POSTAL_CODE',222,'Ship To Postal Code','SHIP_TO_POSTAL_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Postal Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SHIP_TO_STATE',222,'Ship To State','SHIP_TO_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To State','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SHIP_TO_CITY',222,'Ship To City','SHIP_TO_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To City','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','POSTAL_CODE',222,'Postal Code','POSTAL_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Postal Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','STATE',222,'State','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CITY',222,'City','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','City','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL_ACCOUNT',222,'Gl Account','GL_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','DISCOUNT_AMT',222,'Discount Amt','DISCOUNT_AMT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Discount Amt','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','INVOICE_AMOUNT',222,'Invoice Amount','INVOICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','FISCAL_MONTH',222,'Fiscal Month','FISCAL_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fiscal Month','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','FISCAL_YEAR',222,'Fiscal Year','FISCAL_YEAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Fiscal Year','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','AMOUNT_APPLIED',222,'Amount Applied','AMOUNT_APPLIED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Amount Applied','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','RECEIPT_DATE',222,'Receipt Date','RECEIPT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Receipt Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','WRITEOFF_AMOUNT',222,'Writeoff Amount','WRITEOFF_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Writeoff Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','LOCATION',222,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ACTIVITY_NAME',222,'Activity Name','ACTIVITY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Activity Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJ_TYPE_CODE',222,'Adj Type Code','ADJ_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adj Type Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ACCOUNT',222,'Account','ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_ACCTD_AMOUNT',222,'Adjustment Acctd Amount','ADJUSTMENT_ACCTD_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Adjustment Acctd Amount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_DATE',222,'Adjustment Date','ADJUSTMENT_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Adjustment Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_GL_DATE',222,'Adjustment Gl Date','ADJUSTMENT_GL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Adjustment Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_ID',222,'Adjustment Id','ADJUSTMENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Adjustment Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_NUM',222,'Adjustment Num','ADJUSTMENT_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adjustment Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJUSTMENT_POSTABLE',222,'Adjustment Postable','ADJUSTMENT_POSTABLE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adjustment Postable','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJ_TYPE',222,'Adj Type','ADJ_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adj Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ADJ_TYPE_MEANING',222,'Adj Type Meaning','ADJ_TYPE_MEANING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Adj Type Meaning','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','BRANCH_DESCR',222,'Branch Descr','BRANCH_DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','COA_ID',222,'Coa Id','COA_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Coa Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','COMPANY_NAME',222,'Company Name','COMPANY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Company Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','COUNTRY',222,'Country','COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Country','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CURRENCY_CODE',222,'Currency Code','CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Currency Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_CATEGORY_CODE',222,'Customer Category Code','CUSTOMER_CATEGORY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Category Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_KEY',222,'Customer Key','CUSTOMER_KEY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Key','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUSTOMER_TYPE',222,'Customer Type','CUSTOMER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCT_ORIG_SYS_REF',222,'Cust Acct Orig Sys Ref','CUST_ACCT_ORIG_SYS_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Acct Orig Sys Ref','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCT_SITES_ORIG_SYS_REF',222,'Cust Acct Sites Orig Sys Ref','CUST_ACCT_SITES_ORIG_SYS_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Acct Sites Orig Sys Ref','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCT_SITE_ID',222,'Cust Acct Site Id','CUST_ACCT_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Acct Site Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_ACCT_STATUS',222,'Cust Acct Status','CUST_ACCT_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Acct Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_SITE_USES_STATUS',222,'Cust Site Uses Status','CUST_SITE_USES_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cust Site Uses Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CUST_TRX_TYPE_ID',222,'Cust Trx Type Id','CUST_TRX_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Trx Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','DUNS_NUMBER',222,'Duns Number','DUNS_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Duns Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','D_OR_I',222,'D Or I','D_OR_I','','','','XXEIS_RS_ADMIN','VARCHAR2','','','D Or I','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','FUNCTIONAL_CURRENCY',222,'Functional Currency','FUNCTIONAL_CURRENCY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Functional Currency','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','FUNCTIONAL_CURRENCY_PRECISION',222,'Functional Currency Precision','FUNCTIONAL_CURRENCY_PRECISION','','','','XXEIS_RS_ADMIN','NUMBER','','','Functional Currency Precision','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HEADER_ID',222,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','KEY_ACCOUNT_FLAG',222,'Key Account Flag','KEY_ACCOUNT_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Key Account Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','LANGUAGE',222,'Language','LANGUAGE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Language','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','LEDGER_ID',222,'Ledger Id','LEDGER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Ledger Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','ORG_ID',222,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_NAME',222,'Party Name','PARTY_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_NUMBER',222,'Party Number','PARTY_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_ORIG_SYS_REF',222,'Party Orig Sys Ref','PARTY_ORIG_SYS_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Orig Sys Ref','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PARTY_TYPE',222,'Party Type','PARTY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PAYMENT_SCHEDULE_ID',222,'Payment Schedule Id','PAYMENT_SCHEDULE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Payment Schedule Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_FIRST_NAME',222,'Person First Name','PERSON_FIRST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person First Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_LAST_NAME',222,'Person Last Name','PERSON_LAST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Last Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_MIDDLE_NAME',222,'Person Middle Name','PERSON_MIDDLE_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Middle Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_NAME_SUFFIX',222,'Person Name Suffix','PERSON_NAME_SUFFIX','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Name Suffix','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PERSON_TITLE',222,'Person Title','PERSON_TITLE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Person Title','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PRIMARY_FLAG',222,'Primary Flag','PRIMARY_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Primary Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','PROVINCE',222,'Province','PROVINCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Province','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','RECEIVABLES_TRX_ID',222,'Receivables Trx Id','RECEIVABLES_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Receivables Trx Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SITE_USE_CODE',222,'Site Use Code','SITE_USE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Use Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SITE_USE_ID',222,'Site Use Id','SITE_USE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Site Use Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','SITE_USE_ORIG_SYS_REF',222,'Site Use Orig Sys Ref','SITE_USE_ORIG_SYS_REF','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Use Orig Sys Ref','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','STATUS',222,'Status','STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TAX_CODE',222,'Tax Code','TAX_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TAX_REFERENCE',222,'Tax Reference','TAX_REFERENCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Tax Reference','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TP_HEADER_ID',222,'Tp Header Id','TP_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Tp Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_COMMENTS',222,'Trx Comments','TRX_COMMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Comments','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_CURRENCY_CODE',222,'Trx Currency Code','TRX_CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Currency Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_DUE_DATE',222,'Trx Due Date','TRX_DUE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Trx Due Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_GL_DATE',222,'Trx Gl Date','TRX_GL_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Trx Gl Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_TYPE',222,'Trx Type','TRX_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','TRX_TYPE_CLASS',222,'Trx Type Class','TRX_TYPE_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Trx Type Class','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#ACCOUNT',222,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GL#Account','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#ACCOUNT#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#ACCOUNT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GL#Account Descr','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#COST_CENTER',222,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#COST_CENTER','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GL#Cost Center','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#COST_CENTER#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#COST_CENTER#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GL#Cost Center Descr','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#FURTURE_USE',222,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#FURTURE_USE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GL#Furture Use','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#FURTURE_USE#DESCR',222,'Accounting Flexfield : Segment ''Furture Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#FURTURE_USE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GL#Furture Use Descr','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#FUTURE_USE_2',222,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#FUTURE_USE_2','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GL#Future Use 2','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#FUTURE_USE_2#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#FUTURE_USE_2#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GL#Future Use 2 Descr','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#LOCATION',222,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GL#Location','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#LOCATION#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#LOCATION#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GL#Location Descr','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#PRODUCT',222,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GL#Product','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#PRODUCT#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#PRODUCT#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GL#Product Descr','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#PROJECT_CODE',222,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#PROJECT_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GL#Project Code','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL#50328#PROJECT_CODE#DESCR',222,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GL#50328#PROJECT_CODE#DESCR','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GL#Project Code Descr','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_INVOICE_REGISTER_V','CODE_COMBINATION_ID',222,'Code Combination Id','CODE_COMBINATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Code Combination Id','','','','US','');
--Inserting Object Components for EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_ACCOUNTS',222,'HZ_CUST_ACCOUNTS','CUST','CUST','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Information About Customer Accounts.','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_PARTIES',222,'HZ_PARTIES','PARTY','PARTY','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Information About Parties Such As Organizations, P','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_SITE_USES',222,'HZ_CUST_SITE_USES_ALL','SITE_USES','SITE_USES','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores Business Purposes Assigned To Customer Acco','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_ACCT_SITES',222,'HZ_CUST_ACCT_SITES_ALL','ACCT_SITE','ACCT_SITE','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Stores All Customer Account Sites Across All Opera','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL_CODE_COMBINATIONS',222,'GL_CODE_COMBINATIONS','GL','GL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Receivable Activity Account','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_AR_INVOICE_REGISTER_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_ACCOUNTS','CUST',222,'EXAIRV.CUST_ACCOUNT_ID','=','CUST.CUST_ACCOUNT_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_PARTIES','PARTY',222,'EXAIRV.PARTY_ID','=','PARTY.PARTY_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_SITE_USES','SITE_USES',222,'EXAIRV.SITE_USE_ID','=','SITE_USES.SITE_USE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','HZ_CUST_ACCT_SITES','ACCT_SITE',222,'EXAIRV.CUST_ACCT_SITE_ID','=','ACCT_SITE.CUST_ACCT_SITE_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_INVOICE_REGISTER_V','GL_CODE_COMBINATIONS','GL',222,'EXAIRV.CODE_COMBINATION_ID','=','GL.CODE_COMBINATION_ID','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for Invoice Register - Write Offs
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Invoice Register - Write Offs
xxeis.eis_rsc_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
    where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT  CONCATENATED_SEGMENTS
  FROM   gl_code_combinations_kfv glcc
 WHERE  chart_of_accounts_id IN (
                SELECT led.chart_of_accounts_id
                  from gl_ledgers led
                 where gl_security_pkg.validate_access(led.ledger_id) = ''TRUE'')
   and nvl(summary_flag,''N'') = upper(''N'')
order by CONCATENATED_SEGMENTS','','EIS_GLACCOUNT_CONCAT_LOV','EIS_GLACCOUNT_CONCAT_LOV','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct trx_number
from ra_customer_trx_all','null','INVOICES','List of All AR Invoices','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_NAME
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_NAME','','Fiscal Month','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct name from AR_RECEIVABLES_TRX_all','','XXWC Receivables Activities','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select  lookup_code, meaning from ar_lookups ladjtype where ladjtype.lookup_type  = ''ADJUSTMENT_TYPE''','','AR Adjustment Types','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for Invoice Register - Write Offs
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Invoice Register - Write Offs
xxeis.eis_rsc_utility.delete_report_rows( 'Invoice Register - Write Offs',222 );
--Inserting Report - Invoice Register - Write Offs
xxeis.eis_rsc_ins.r( 222,'Invoice Register - Write Offs','','Provide the detail of all invoices transacted for a certain period.','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_INVOICE_REGISTER_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','','','','','','','','','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - Invoice Register - Write Offs
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'CITY','City','City','','','','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'DISCOUNT_AMT','Discount Amt','Discount Amt','','','','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'GL_ACCOUNT','Gl Account','Gl Account','','','','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'INVOICE_AMOUNT','Invoice Amount','Invoice Amount','','','','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'POSTAL_CODE','Postal Code','Postal Code','','','','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'SHIP_TO_CITY','Ship To City','Ship To City','','','','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'SHIP_TO_POSTAL_CODE','Ship To Postal Code','Ship To Postal Code','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'SHIP_TO_STATE','Ship To State','Ship To State','','','','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'SHIP_VIA','Ship Via','Ship Via','','','','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'STATE','State','State','','','','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'AMOUNT_APPLIED','Amount Applied','Amount Applied','','','','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'RECEIPT_DATE','Receipt Date','Receipt Date','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'WRITEOFF_AMOUNT','Writeoff Amount','Writeoff Amount','','','','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','SUM','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'LOCATION','Location','Location','','','','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'ORDER_NUMBER','Order Number','Order Number','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
xxeis.eis_rsc_ins.rc( 'Invoice Register - Write Offs',222,'ACTIVITY_NAME','Activity Name','Activity Name','','','','','22','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','GROUP_BY','US','','');
--Inserting Report Parameters - Invoice Register - Write Offs
xxeis.eis_rsc_ins.rp( 'Invoice Register - Write Offs',222,'Customer Name','Customer Name','CUSTOMER_NAME','IN','Customer Name','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Invoice Register - Write Offs',222,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Invoice Register - Write Offs',222,'Fiscal Month','Fiscal Month','FISCAL_MONTH','IN','Fiscal Month','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Invoice Register - Write Offs',222,'Invoice Number','Invoice Number','INVOICE_NUMBER','IN','INVOICES','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Invoice Register - Write Offs',222,'Receivable Activity','Receivable Activity','ACTIVITY_NAME','IN','XXWC Receivables Activities','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Invoice Register - Write Offs',222,'Receivables Activity Account','GL Account','ACCOUNT','IN','EIS_GLACCOUNT_CONCAT_LOV','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','Y','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Invoice Register - Write Offs',222,'Adjustment Type','Adjustment Type','ADJ_TYPE_CODE','IN','AR Adjustment Types','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','US','');
--Inserting Dependent Parameters - Invoice Register - Write Offs
--Inserting Report Conditions - Invoice Register - Write Offs
xxeis.eis_rsc_ins.rcnh( 'Invoice Register - Write Offs',222,'ACCOUNT IN :Receivables Activity Account ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT','','Receivables Activity Account','','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','','','','IN','Y','Y','','','','','1',222,'Invoice Register - Write Offs','ACCOUNT IN :Receivables Activity Account ');
xxeis.eis_rsc_ins.rcnh( 'Invoice Register - Write Offs',222,'ACTIVITY_NAME IN :Receivable Activity ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','ACTIVITY_NAME','','Receivable Activity','','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','','','','IN','Y','Y','','','','','1',222,'Invoice Register - Write Offs','ACTIVITY_NAME IN :Receivable Activity ');
xxeis.eis_rsc_ins.rcnh( 'Invoice Register - Write Offs',222,'ADJ_TYPE_CODE IN :Adjustment Type ','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','ADJ_TYPE_CODE','','Adjustment Type','','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','','','','IN','Y','Y','','','','','1',222,'Invoice Register - Write Offs','ADJ_TYPE_CODE IN :Adjustment Type ');
xxeis.eis_rsc_ins.rcnh( 'Invoice Register - Write Offs',222,'CUSTOMER_NAME IN :Customer Name ','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NAME','','Customer Name','','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','','','','IN','Y','Y','','','','','1',222,'Invoice Register - Write Offs','CUSTOMER_NAME IN :Customer Name ');
xxeis.eis_rsc_ins.rcnh( 'Invoice Register - Write Offs',222,'CUSTOMER_NUMBER IN :Customer Number ','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','','','','IN','Y','Y','','','','','1',222,'Invoice Register - Write Offs','CUSTOMER_NUMBER IN :Customer Number ');
xxeis.eis_rsc_ins.rcnh( 'Invoice Register - Write Offs',222,'FISCAL_MONTH IN :Fiscal Month ','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','FISCAL_MONTH','','Fiscal Month','','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','','','','IN','Y','Y','','','','','1',222,'Invoice Register - Write Offs','FISCAL_MONTH IN :Fiscal Month ');
xxeis.eis_rsc_ins.rcnh( 'Invoice Register - Write Offs',222,'INVOICE_NUMBER IN :Invoice Number ','SIMPLE','','','Y','','7');
xxeis.eis_rsc_ins.rcnd( '','','INVOICE_NUMBER','','Invoice Number','','','','','EIS_XXWC_AR_INVOICE_REGISTER_V','','','','','','IN','Y','Y','','','','','1',222,'Invoice Register - Write Offs','INVOICE_NUMBER IN :Invoice Number ');
--Inserting Report Sorts - Invoice Register - Write Offs
xxeis.eis_rsc_ins.rs( 'Invoice Register - Write Offs',222,'FISCAL_MONTH','ASC','XXEIS_RS_ADMIN','1','');
--Inserting Report Triggers - Invoice Register - Write Offs
--inserting report templates - Invoice Register - Write Offs
xxeis.eis_rsc_ins.r_tem( 'Invoice Register - Write Offs','Invoice Register','Seeded Template for Invoice Register','','','','','','','','','','','Invoice Register - Write Offs.rtf','XXEIS_RS_ADMIN','X','','','Y','Y','N','N');
--Inserting Report Portals - Invoice Register - Write Offs
--inserting report dashboards - Invoice Register - Write Offs
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Invoice Register - Write Offs','222','EIS_XXWC_AR_INVOICE_REGISTER_V','EIS_XXWC_AR_INVOICE_REGISTER_V','N','');
--inserting report security - Invoice Register - Write Offs
xxeis.eis_rsc_ins.rsec( 'Invoice Register - Write Offs','222','','RECEIVABLES INQUIRY',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Invoice Register - Write Offs','222','','RECEIVABLES_MANAGER',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Invoice Register - Write Offs','20005','','XXWC_VIEW_ALL_EIS_REPORTS',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Invoice Register - Write Offs','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Invoice Register - Write Offs','101','','XXCUS_GL_INQUIRY',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Invoice Register - Write Offs','222','','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Invoice Register - Write Offs','1','','',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Invoice Register - Write Offs','401','','',222,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - Invoice Register - Write Offs
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- Invoice Register - Write Offs
xxeis.eis_rsc_ins.rv( 'Invoice Register - Write Offs','','Invoice Register - Write Offs','SA059956','22-OCT-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
