--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_XXWC_AR_CUST_ACCT_RECON_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_CUST_ACCT_RECON_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_CUST_ACCT_RECON_V',222,'','','','','SA059956','XXEIS','Eis Xxwc Ar Cust Acct Recon V','EXACARV1','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_AR_CUST_ACCT_RECON_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_CUST_ACCT_RECON_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_CUST_ACCT_RECON_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','ON_ACCOUNT_AMOUNT',222,'On Account Amount','ON_ACCOUNT_AMOUNT','','','','SA059956','NUMBER','','','On Account Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','PREPAYMENT_AMOUNT',222,'Prepayment Amount','PREPAYMENT_AMOUNT','','','','SA059956','NUMBER','','','Prepayment Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','UNAPPLIED_AMOUNT',222,'Unapplied Amount','UNAPPLIED_AMOUNT','','','','SA059956','NUMBER','','','Unapplied Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','RECEIPT_AMOUNT',222,'Receipt Amount','RECEIPT_AMOUNT','','','','SA059956','NUMBER','','','Receipt Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','DISCREPANCY_CODE',222,'Discrepancy Code','DISCREPANCY_CODE','','','','SA059956','VARCHAR2','','','Discrepancy Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','RECEIPT_COMMENTS',222,'Receipt Comments','RECEIPT_COMMENTS','','','','SA059956','VARCHAR2','','','Receipt Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','APPLIED_DATE',222,'Applied Date','APPLIED_DATE','','','','SA059956','DATE','','','Applied Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','DEPOSIT_DATE',222,'Deposit Date','DEPOSIT_DATE','','','','SA059956','DATE','','','Deposit Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','UNEARNED_DISCOUNT',222,'Unearned Discount','UNEARNED_DISCOUNT','','','','SA059956','NUMBER','','','Unearned Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','REFERENCE',222,'Reference','REFERENCE','','','','SA059956','VARCHAR2','','','Reference','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','CREATED_BY',222,'Created By','CREATED_BY','','','','SA059956','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','ACTIVITY_DATE',222,'Activity Date','ACTIVITY_DATE','','','','SA059956','DATE','','','Activity Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','STATE',222,'State','STATE','','','','SA059956','VARCHAR2','','','State','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','APPLIED_TRX_TYPE',222,'Applied Trx Type','APPLIED_TRX_TYPE','','','','SA059956','VARCHAR2','','','Applied Trx Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','APPLIED_AMOUNT',222,'Applied Amount','APPLIED_AMOUNT','','','','SA059956','NUMBER','','','Applied Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','APPLIED_TRX_NUMBER',222,'Applied Trx Number','APPLIED_TRX_NUMBER','','','','SA059956','VARCHAR2','','','Applied Trx Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','PAYMENT_METHOD',222,'Payment Method','PAYMENT_METHOD','','','','SA059956','VARCHAR2','','','Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','TRX_PAYMENT_TERMS',222,'Trx Payment Terms','TRX_PAYMENT_TERMS','','','','SA059956','VARCHAR2','','','Trx Payment Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','DAYS_LATE',222,'Days Late','DAYS_LATE','','','','SA059956','NUMBER','','','Days Late','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','EARNED_DISCOUNT',222,'Earned Discount','EARNED_DISCOUNT','','','','SA059956','NUMBER','','','Earned Discount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','TAX_BALANCE_DUE',222,'Tax Balance Due','TAX_BALANCE_DUE','','','','SA059956','NUMBER','','','Tax Balance Due','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','TRANSACTION_BALANCE_DUE',222,'Transaction Balance Due','TRANSACTION_BALANCE_DUE','','','','SA059956','NUMBER','','','Transaction Balance Due','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','TRX_AMOUNT_DUE_ORIGINAL',222,'Trx Amount Due Original','TRX_AMOUNT_DUE_ORIGINAL','','','','SA059956','NUMBER','','','Trx Amount Due Original','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','RECEIPT_DATE',222,'Receipt Date','RECEIPT_DATE','','','','SA059956','DATE','','','Receipt Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','RECEIPT_NUMBER',222,'Receipt Number','RECEIPT_NUMBER','','','','SA059956','VARCHAR2','','','Receipt Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','PO_NUMBER',222,'Po Number','PO_NUMBER','','','','SA059956','VARCHAR2','','','Po Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','SA059956','DATE','','','Trx Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','TRANSACTION_NUMBER',222,'Transaction Number','TRANSACTION_NUMBER','','','','SA059956','VARCHAR2','','','Transaction Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','LOCATION',222,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','PARTY_SITE_NUMBER',222,'Party Site Number','PARTY_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Party Site Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','ACCOUNT_STATUS',222,'Account Status','ACCOUNT_STATUS','','','','SA059956','VARCHAR2','','','Account Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','PAYMENT_TERMS',222,'Payment Terms','PAYMENT_TERMS','','','','SA059956','VARCHAR2','','','Payment Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','ACCOUNT_MANAGER',222,'Account Manager','ACCOUNT_MANAGER','','','','SA059956','VARCHAR2','','','Account Manager','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','CREDIT_MANAGER',222,'Credit Manager','CREDIT_MANAGER','','','','SA059956','VARCHAR2','','','Credit Manager','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','PHONE_NUMBER',222,'Phone Number','PHONE_NUMBER','','','','SA059956','VARCHAR2','','','Phone Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_ACCT_RECON_V','REPORT_VIEW',222,'Report View','REPORT_VIEW','','','','SA059956','VARCHAR2','','','Report View','','','','');
--Inserting Object Components for EIS_XXWC_AR_CUST_ACCT_RECON_V
--Inserting Object Component Joins for EIS_XXWC_AR_CUST_ACCT_RECON_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for Customer Account Reconciliation report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Customer Account Reconciliation report
xxeis.eis_rsc_ins.lov( 222,'SELECT NVL(hca.account_name, hp.party_name) customer_name,hca.account_number customer_number
FROM hz_cust_accounts hca,
  hz_parties hp
where hca.party_id=hp.party_id
ORDER BY 1','','XXWC Customer Name LOV','Display List of Values for Customer Name','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT hca.account_number customer_number,NVL(hca.account_name, hp.party_name) customer_name
FROM hz_cust_accounts hca,
  hz_parties hp
where hca.party_id=hp.party_id
ORDER BY lpad(hca.account_number,10)','','XXWC Customer Number LOV','Displays List of Values for Customer Number','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT ''Transaction'' report_view FROM dual
UNION ALL
SELECT ''Receipt'' report_view FROM dual','','XXWC AR Report View LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT HPS.PARTY_SITE_NUMBER,''Transaction'' REPORT_VIEW FROM APPS.HZ_PARTY_SITES HPS
order by lpad(HPS.PARTY_SITE_NUMBER,10)','','XXWC Party Site Number LOV','','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT acr.receipt_number,
  HCA.ACCOUNT_NUMBER CUSTOMER_NUMBER
FROM ar_cash_receipts acr,
  HZ_CUST_ACCOUNTS HCA
WHERE acr.pay_from_customer = hca.cust_account_id
order by lpad(acr.receipt_number,10)','','XXWC Customer Receipt Number LOV','This lov lists all customer related receipts','SA059956',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT  rct.trx_number,
  HCA.ACCOUNT_NUMBER CUSTOMER_NUMBER
FROM apps.ar_payment_schedules aps,
  apps.ra_customer_trx RCT,
  apps.HZ_CUST_ACCOUNTS HCA
WHERE APS.CUSTOMER_TRX_ID = RCT.CUSTOMER_TRX_ID
AND APS.ORG_ID            = RCT.ORG_ID
AND APS.CUSTOMER_ID       = HCA.CUST_ACCOUNT_ID
order by lpad(rct.trx_number,10)','','XXWC Customer Trx Number LOV','This lov lists all customer related transaction numbers','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for Customer Account Reconciliation report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Customer Account Reconciliation report
xxeis.eis_rsc_utility.delete_report_rows( 'Customer Account Reconciliation report' );
--Inserting Report - Customer Account Reconciliation report
xxeis.eis_rsc_ins.r( 222,'Customer Account Reconciliation report','','Customer Account Reconciliation report','','','','SA059956','EIS_XXWC_AR_CUST_ACCT_RECON_V','Y','','','SA059956','','N','White Cap Reports','EXCEL,','','N','','','','','','N','','US','','','','');
--Inserting Report Columns - Customer Account Reconciliation report
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'ACCOUNT_MANAGER','Account Manager','Account Manager','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'ACCOUNT_STATUS','Account Status','Account Status','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'ACTIVITY_DATE','Activity Date','Activity Date','','','default','','27','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'APPLIED_AMOUNT','Applied Amount','Applied Amount','','~T~D~2','default','','24','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'APPLIED_DATE','Applied Date','Applied Date','','','default','','32','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'APPLIED_TRX_NUMBER','Applied Trx Number','Applied Trx Number','','','default','','23','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'APPLIED_TRX_TYPE','Applied Trx Type','Applied Trx Type','','','default','','25','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'CREATED_BY','Created By','Created By','','','default','','28','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'CREDIT_MANAGER','Credit Manager','Credit Manager','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'DAYS_LATE','Days Late','Days Late','','~~~','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'DEPOSIT_DATE','Deposit Date','Deposit Date','','','default','','31','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'DISCREPANCY_CODE','Discrepancy Code','Discrepancy Code','','','default','','34','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'EARNED_DISCOUNT','Earned Discount','Earned Discount','','~T~D~2','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'LOCATION','Location','Location','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'ON_ACCOUNT_AMOUNT','On Account Amount','On Account Amount','','~T~D~2','default','','38','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'PARTY_SITE_NUMBER','Party Site Number','Party Site Number','','','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'PAYMENT_METHOD','Payment Method','Payment Method','','','default','','22','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'PAYMENT_TERMS','Payment Terms','Payment Terms','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'PHONE_NUMBER','Phone Number','Phone Number','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'PO_NUMBER','Po Number','Po Number','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'PREPAYMENT_AMOUNT','Prepayment Amount','Prepayment Amount','','~T~D~2','default','','37','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'RECEIPT_AMOUNT','Receipt Amount','Receipt Amount','','~T~D~2','default','','35','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'RECEIPT_COMMENTS','Receipt Comments','Receipt Comments','','','default','','33','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'RECEIPT_DATE','Receipt Date','Receipt Date','','','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'RECEIPT_NUMBER','Receipt Number','Receipt Number','','','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'REFERENCE','Reference','Reference','','','default','','29','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'STATE','State','State','','','default','','26','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'TAX_BALANCE_DUE','Tax Balance Due','Tax Balance Due','','~T~D~2','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'TRANSACTION_BALANCE_DUE','Transaction Balance Due','Transaction Balance Due','','~T~D~2','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'TRANSACTION_NUMBER','Transaction Number','Transaction Number','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'TRX_AMOUNT_DUE_ORIGINAL','Trx Amount Due Original','Trx Amount Due Original','','~T~D~2','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'TRX_DATE','Trx Date','Trx Date','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'TRX_PAYMENT_TERMS','Trx Payment Terms','Trx Payment Terms','','','default','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'UNAPPLIED_AMOUNT','Unapplied Amount','Unapplied Amount','','~T~D~2','default','','36','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'UNEARNED_DISCOUNT','Unearned Discount','Unearned Discount','','~T~D~2','default','','30','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Reconciliation report',222,'REPORT_VIEW','Report View','Report View','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','','US','');
--Inserting Report Parameters - Customer Account Reconciliation report
xxeis.eis_rsc_ins.rp( 'Customer Account Reconciliation report',222,'Customer Account Number','Customer Account Number','','','XXWC Customer Number LOV','','VARCHAR2','Y','Y','2','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Reconciliation report',222,'Customer Account Name','Customer Account Name','','','XXWC Customer Name LOV','','VARCHAR2','N','Y','3','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Reconciliation report',222,'Start Date','Start Date','','','','select add_months(trunc(sysdate),-3) start_date from dual','DATE','Y','Y','5','N','N','SQL','SA059956','Y','N','','Start Date','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Reconciliation report',222,'End Date','End Date','','','','','DATE','N','Y','6','N','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Reconciliation report',222,'Transaction Number','Transaction Number','','','XXWC Customer Trx Number LOV','','VARCHAR2','N','Y','7','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Reconciliation report',222,'Receipt Number','Receipt Number','','','XXWC Customer Receipt Number LOV','','VARCHAR2','N','Y','8','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Reconciliation report',222,'Report View','Report View','','','XXWC AR Report View LOV','''Transaction''','VARCHAR2','Y','Y','1','N','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Reconciliation report',222,'Party Site Number','Party Site Number','','','XXWC Party Site Number LOV','','VARCHAR2','N','Y','4','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_ACCT_RECON_V','','','US','');
--Inserting Dependent Parameters - Customer Account Reconciliation report
xxeis.eis_rsc_ins.rdp( 'Customer Account Reconciliation report',222,'CUSTOMER_NUMBER','Transaction Number','Customer Account Number','IN','N','');
xxeis.eis_rsc_ins.rdp( 'Customer Account Reconciliation report',222,'CUSTOMER_NUMBER','Receipt Number','Customer Account Number','IN','N','');
xxeis.eis_rsc_ins.rdp( 'Customer Account Reconciliation report',222,'CUSTOMER_NUMBER','Customer Account Name','Customer Account Number','IN','N','');
xxeis.eis_rsc_ins.rdp( 'Customer Account Reconciliation report',222,'CUSTOMER_NAME','Customer Account Number','Customer Account Name','IN','N','');
--Inserting Report Conditions - Customer Account Reconciliation report
--Inserting Report Sorts - Customer Account Reconciliation report
--Inserting Report Triggers - Customer Account Reconciliation report
xxeis.eis_rsc_ins.rt( 'Customer Account Reconciliation report',222,'BEGIN
xxeis.EIS_XXWC_CUST_ACC_RECON_PKG.CUST_ACCT_RECON_PROC(
P_REPORT_VIEW		=> :Report View ,
P_CUST_ACC_NUMBER 	=> :Customer Account Number,
P_CUST_ACC_NAME   	=> :Customer Account Name,
P_PARTY_SITE_NUM  	=> :Party Site Number,
P_START_DATE      	=> :Start Date,
P_END_DATE        	=> :End Date,
P_TRX_NUMBER      	=> :Transaction Number,
p_Receipt_Number  	=> :Receipt Number);
END;','B','Y','SA059956','BQ');
--inserting report templates - Customer Account Reconciliation report
xxeis.eis_rsc_ins.r_tem( 'Customer Account Reconciliation report','Customer Account Reconciliation report','Customer Account Reconciliation report','','','','','','','','','','','Customer Account Reconciliation report.rtf','SA059956','X','','','Y','Y','Y','N');
--Inserting Report Portals - Customer Account Reconciliation report
--inserting report dashboards - Customer Account Reconciliation report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Customer Account Reconciliation report','222','EIS_XXWC_AR_CUST_ACCT_RECON_V','EIS_XXWC_AR_CUST_ACCT_RECON_V','N','');
--inserting report security - Customer Account Reconciliation report
xxeis.eis_rsc_ins.rsec( 'Customer Account Reconciliation report','222','','XXWC_CRE_CREDIT_COLL_MGR_NOREC',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Reconciliation report','222','','XXWC_CRE_CREDIT_COLL_MGR',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Reconciliation report','222','','XXWC_CRE_ASSOC_CUST_MAINT',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Reconciliation report','222','','XXWC_CRE_ASSOC_COLLECTIONS',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Reconciliation report','222','','XXWC_CRE_ASSOC_CASH_APP_MGR',222,'SA059956','','','');
--Inserting Report Pivots - Customer Account Reconciliation report
--Inserting Report   Version details- Customer Account Reconciliation report
xxeis.eis_rsc_ins.rv( 'Customer Account Reconciliation report','','Customer Account Reconciliation report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
