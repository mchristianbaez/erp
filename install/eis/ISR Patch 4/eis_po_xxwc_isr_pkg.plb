create or replace
PACKAGE BODY  eis_po_xxwc_isr_pkg AS

PROCEDURE write_log (  p_text IN VARCHAR2
                     )
Is
BEGIN
     IF nvl(G_log_mode ,'FND')='FND' THEN
        fnd_file.put_line(fnd_file.LOG,p_text);
     ELSE
         dbms_output.put_line(p_text);
     END IF;    
END;



PROCEDURE MAIN IS

CURSOR c_item_cur 
                is (
                    SELECT * 
                    from xxeis.eis_xxwc_po_isr_tab_v
                    );
type period_tab is table of varchar2(200)  index by binary_integer; 
period_name_tab                period_tab;
l_yes_no                       VARCHAR2(10);
l_current_period_year          VARCHAR2(20);
l_current_effective_period_num VARCHAR2(240);
l_period_name                  varchar2(100);
l_store_period_column_name     varchar2(240);
l_other_period_column_name     varchar2(240);
l_update_str                   VARCHAR2(10000);    
lv_program_location            varchar2(4000);   
l_period_count                 NUMBER;
l_item_cost                    NUMBER;
l_avail_d                      NUMBER;
l_on_ord                       NUMBER;
l_avail2                       NUMBER;


BEGIN
 
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_tab';
  FOR c_item_rec IN c_item_cur
  loop
       l_item_cost := nvl(C_ITEM_REC.bpa_cost,c_item_rec.item_cost);
       l_avail_d   := c_item_rec.avail *  c_item_rec.aver_cost;
       l_on_ord    := c_item_rec.supply - c_item_rec.on_ord;
       l_avail2     := c_item_rec.qoh - c_item_rec.demand;
       
        INSERT
        INTO xxeis.eis_xxwc_po_isr_tab
          (
            org,
            pre,
            item_number,
            vendor_num,
            vendor_name,
            SOURCE,
            st,
            description,
            cat,
            pplt,
            plt,
            uom,
            cl,
            stk_flag,
            pm,
            minn,
            maxn,
            amu,
            mf_flag,
            hit6_store_sales,
            hit6_other_inv_sales,
            aver_cost,
            ITEM_COST,
            bpa_cost,
            bpa,
            QOH,
            on_ord,
            available,
            availabledollar,
            one_store_sale,
            six_store_sale,
            twelve_store_sale,
            one_other_inv_sale,
            six_other_inv_sale,
            twelve_other_inv_sale,
            bin_loc,
            mc,
            fi_flag,
            freeze_date,
            res,
            thirteen_wk_avg_inv,
            thirteen_wk_an_cogs,
            turns,
            buyer,
            ts,
            jan_store_sale,
            feb_store_sale,
            mar_store_sale,
            apr_store_sale,
            may_store_sale,
            jun_store_sale,
            jul_store_sale,
            aug_store_sale,
            sep_store_sale,
            oct_store_sale,
            nov_store_sale,
            dec_store_sale,
            jan_other_inv_sale,
            feb_other_inv_sale,
            mar_other_inv_sale,
            apr_other_inv_sale,
            may_other_inv_sale,
            jun_other_inv_sale,
            jul_other_inv_sale,
            aug_other_inv_sale,
            sep_other_inv_sale,
            oct_other_inv_sale,
            nov_other_inv_sale,
            dec_other_inv_sale,
            hit4_store_sales,
            hit4_other_inv_sales,
            So,
            inventory_item_id,
            organization_id,
            set_of_books_id,
            org_name,
            district,  
            region,
            inv_cat_seg1,
              WT,
            ss,--saftey_stk
            fml,
           open_req,
           sourcing_rule,
           clt,
           avail2,
           int_req,
           dir_req,
           demand
          )
          VALUES
          (
            c_item_rec.org,--org
            c_item_rec.pre ,--pre
            c_item_rec.item_number ,--item_number
            c_item_rec.vendor_number ,--vendor_num
            c_item_rec.vendor_name,--vendor_name
            c_item_rec.SOURCE,--source
            c_item_rec.st ,--st
            c_item_rec.description ,--description
            c_item_rec.cat_class ,--cat
            c_item_rec.pplt ,--pplt
            c_item_rec.plt ,--plt
            c_item_rec.uom,--uom
            c_item_rec.cl ,--cl
            c_item_rec.stk ,--stk_flag
            c_item_rec.pm ,--pm 
            c_item_rec.MIN ,--min
            c_item_rec.MAX ,--max
            c_item_rec.amu ,--amu
            c_item_rec.mf_flag ,--mf_flag
            NULL,--hit6_store_sales
            NULL ,--hit6_other_inv_sales ,
            C_ITEM_REC.AVER_COST ,--aver_cost            
            l_item_cost,
            --C_ITEM_REC.ITEM_COST ,--item_cost
            C_ITEM_REC.bpa_cost,
            c_item_rec.bpa ,--bpa
            C_ITEM_REC.QOH ,--oh_qty
            l_on_ord,
            --C_ITEM_REC.on_ord ,--on_ord
            C_ITEM_REC.AVAIL ,--available
            l_avail_d,--availabledollar
            NULL ,--one_store_sale
            NULL ,--six_store_sale
            NULL ,--twelve_store_sale
            NULL ,--one_other_inv_sale,
            NULL ,--six_other_inv_sale,
            NULL ,--twelve_other_inv_sale,
            c_item_rec.bin_loc ,--bin_loc,
            c_item_rec.mc ,--mc,
            c_item_rec.fi,--fi
            c_item_rec.freeze_date ,--freeze_date,
            c_item_rec.res,--reserve_stock
            c_item_rec.thirteen_wk_avg_inv,--thirteen_wk_avg_inv
            c_item_rec.thirteen_wk_an_cogs,--thirteen_wk_an_cogs
            c_item_rec.turns,--turns
            c_item_rec.buyer,--buyer
            c_item_rec.ts,--ts
            NULL ,--jan_store_sale,
            NULL ,--feb_store_sale,
            NULL ,--mar_store_sale,
            NULL ,--apr_store_sale,
            NULL ,--may_store_sale,
            NULL ,--jun_store_sale,
            NULL ,--jul_store_sale,
            NULL ,--aug_store_sale,
            NULL ,--sep_store_sale,
            NULL ,--oct_store_sale,
            NULL ,--nov_store_sale,
            NULL ,--dec_store_sale,
            NULL ,--jan_other_inv_sale,
            NULL ,--feb_other_inv_sale,
            NULL ,--mar_other_inv_sale,
            NULL ,--apr_other_inv_sale,
            NULL ,--may_other_inv_sale,
            NULL ,--jun_other_inv_sale,
            NULL ,--jul_other_inv_sale,
            NULL ,--aug_other_inv_sale,
            NULL ,--sep_other_inv_sale,
            NULL ,--oct_other_inv_sale,
            NULL ,--nov_other_inv_sale,
            NULL ,--dec_other_inv_sale,
            NULL ,--hit4_store_sales,
            null ,--hit4_other_inv_sales,
            c_item_rec.So ,--ss
            c_item_rec.inventory_item_id,
            c_item_rec.organization_id,
            c_item_rec.set_of_books_id,
            c_item_rec.organization_name,
            c_item_rec.district,
            c_item_rec.region,
            c_item_rec.inv_cat_seg1,
            C_ITEM_REC.WT,
            c_item_rec.ss,--saftey_stk
            c_item_rec.fml,
           c_item_rec.open_req,
           c_item_rec.sourcing_rule,
           c_item_rec.clt,
           l_avail2,
           --c_item_rec.avail2,
           c_item_rec.int_req,
           c_item_rec.dir_req,
           c_item_rec.demand
           ) ;
      END loop;
      COMMIT;
  
  FOR c_distinct_sob_id IN(SELECT DISTINCT set_of_books_id FROM xxeis.eis_xxwc_po_isr_tab)
  loop
         l_period_count:=1;          
         period_name_tab.DELETE;
         --get current period and year
         lv_program_location :='Getting current period and year';         
         SELECT period_name,
                 period_year,
                 effective_period_num 
         INTO   l_period_name,
                 l_current_period_year,
                 l_current_effective_period_num
         FROM gl_period_statuses 
         where application_id=101
         and trunc(sysdate) between start_date and end_date
         and adjustment_period_flag  <>'Y'
         and set_of_books_id         = c_distinct_sob_id.set_of_books_id;
         l_current_effective_period_num:=l_current_effective_period_num-1;
          -- Added to get the previous period
         BEGIN
                               SELECT period_name 
                                INTO period_name_tab(l_period_count)
                                FROM gl_period_statuses
                                WHERE application_id=101
                                AND effective_period_num    = l_current_effective_period_num
                                AND period_year             = l_current_period_year
                                AND adjustment_period_flag  <>'Y'
                                AND set_of_books_id         = c_distinct_sob_id.set_of_books_id;
                     
                                 dbms_output.put_line('period_name_tab(l_period_count)='||period_name_tab(l_period_count));
                                 fnd_file.put_line(fnd_file.log,'period_name_tab(l_period_count)='||period_name_tab(l_period_count));
                                 fnd_file.put_line(fnd_file.log,'l_current_effective_period_num is:'||l_current_effective_period_num);
                                 fnd_file.put_line(fnd_file.log,'l_current_period_year is:'||l_current_period_year);
                                 lv_program_location :=' Next period and year is='||period_name_tab(l_period_count)||','||l_current_period_year;
                                 write_log(lv_program_location);
                        exception WHEN others THEN
                        l_period_count:=l_period_count-1;
                        fnd_file.put_line(fnd_file.log,'Inside Exception ='||l_period_count);
                        exit;
          END;
         
         --period_name_tab(l_period_count):=l_period_name;
         lv_program_location :=' current period and year is='||l_period_name||','||l_current_period_year;
         write_log(lv_program_location);

              --store next period names for existing year in array list
                  loop
                      l_period_count:=l_period_count+1;
                      l_current_effective_period_num:=l_current_effective_period_num-1;
                       BEGIN
                               SELECT period_name 
                                INTO period_name_tab(l_period_count)
                                FROM gl_period_statuses
                                WHERE application_id=101
                                AND effective_period_num    = l_current_effective_period_num
                                AND period_year             = l_current_period_year
                                AND adjustment_period_flag  <>'Y'
                                AND set_of_books_id         = c_distinct_sob_id.set_of_books_id;
                     
                                 dbms_output.put_line('period_name_tab(l_period_count)='||period_name_tab(l_period_count));
                                 fnd_file.put_line(fnd_file.log,'period_name_tab(l_period_count)='||period_name_tab(l_period_count));
                                 fnd_file.put_line(fnd_file.log,'l_current_effective_period_num is:'||l_current_effective_period_num);
                                 fnd_file.put_line(fnd_file.log,'l_current_period_year is:'||l_current_period_year);
                                 lv_program_location :=' Next period and year is='||period_name_tab(l_period_count)||','||l_current_period_year;
                                 write_log(lv_program_location);
                        exception WHEN others THEN
                        l_period_count:=l_period_count-1;
                        fnd_file.put_line(fnd_file.log,'Inside Exception ='||l_period_count);
                        exit;
                        END;
                    exit WHEN l_period_count=11;
                    END loop;
                    
                    
              dbms_output.put_line('l_period_count before change in year ='||l_period_count);  
              --when there is change in year then store next period names for prevous year in array list
              FOR c_period_name IN (SELECT period_name 
                                    FROM (
                                           SELECT period_name
                                           FROM   gl_period_statuses
                                            WHERE application_id          = 101
                                            AND   period_year             = (l_current_period_year-1)
                                            AND   set_of_books_id         = c_distinct_sob_id.set_of_books_id
                                            AND   adjustment_period_flag  <>'Y'
                                            ORDER BY effective_period_num DESC
                                           )
                                      WHERE ROWNUM<=(12-l_period_count)
                                      )
              loop
                  dbms_output.put_line('l_period_count='||l_period_count);  
                  l_period_count:=l_period_count+1;
                  period_name_tab(l_period_count):=c_period_name.period_name;
                  lv_program_location :=' Next period and year is='||period_name_tab(l_period_count)||','||(l_current_period_year-1);
                  write_log(lv_program_location);
             END loop;
      
  END loop;
  
 
  FOR period_name_cnt IN 1..12
  loop
     lv_program_location :=' getting update period name for ='||period_name_tab(period_name_cnt);
     write_log(lv_program_location);
    IF upper(period_name_tab(period_name_cnt)) LIKE  '%JAN%' THEN
       l_store_period_column_name:='jan_store_sale';
       l_other_period_column_name :='jan_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%FEB%' THEN
       l_store_period_column_name:='feb_store_sale';
       l_other_period_column_name :='feb_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%MAR%' THEN
       l_store_period_column_name:='mar_store_sale';
       l_other_period_column_name :='mar_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%APR%' THEN
       l_store_period_column_name:='apr_store_sale';
       l_other_period_column_name :='apr_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%MAY%' THEN
       l_store_period_column_name:='may_store_sale';
       l_other_period_column_name :='may_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%JUN%' THEN
       l_store_period_column_name:='jun_store_sale';
       l_other_period_column_name :='jun_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%JUL%' THEN
       l_store_period_column_name:='jul_store_sale';
       l_other_period_column_name :='jul_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%AUG%' THEN
       l_store_period_column_name:='aug_store_sale';
       l_other_period_column_name :='aug_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%SEP%' THEN
       l_store_period_column_name:='sep_store_sale';
       l_other_period_column_name :='sep_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%OCT%' THEN
       l_store_period_column_name:='oct_store_sale';
       l_other_period_column_name :='oct_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%NOV%' THEN
       l_store_period_column_name:='nov_store_sale';
       l_other_period_column_name :='nov_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%DEC%' THEN
       l_store_period_column_name:='dec_store_sale';
       l_other_period_column_name :='dec_other_inv_sale';
   END IF;
     
   l_update_str:=
                'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set '||l_store_period_column_name||' =(select sum(mdh.sales_order_demand)
                from   mtl_demand_histories mdh,
                       gl_period_statuses    gps,
                       org_organization_definitions   ood 
                where 1=1
                and mdh.inventory_item_id       =tab.inventory_item_id
                and mdh.organization_id     = tab.organization_id
                and gps.application_id  =101
                and trunc(period_start_date) between gps.start_date and gps.end_date
                and ood.set_of_books_id=gps.set_of_books_id
                and ood.organization_id=mdh.organization_id 
                and gps.period_name = '''||period_name_tab(period_name_cnt)||''' )';
     lv_program_location :=' update sql query ='||l_update_str;
     write_log(lv_program_location);  
     BEGIN
        EXECUTE IMMEDIATE l_update_str; COMMIT;
     exception WHEN others THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
          
      ---DC mode calcualations     
     l_update_str:=
      'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set '||l_other_period_column_name||' =(select sum(mdh.sales_order_demand)
      from   mtl_demand_histories mdh,
             gl_period_statuses    gps,
             org_organization_definitions   ood,
             mtl_system_items_kfv msi
      where 1=1
      and msi.inventory_item_id       =tab.inventory_item_id
      and msi.source_organization_id     = tab.organization_id
      AND msi.source_organization_id <> msi.organization_id
      and gps.application_id  =101
      and trunc(period_start_date) between gps.start_date and gps.end_date
      and ood.set_of_books_id=gps.set_of_books_id
      and ood.organization_id=mdh.organization_id 
      and msi.organization_id= mdh.organization_id
      and   msi.inventory_item_id=mdh.inventory_item_id
      and gps.period_name = '''||period_name_tab(period_name_cnt)||''' )';
      lv_program_location :=' update sql query ='||l_update_str;
      write_log(lv_program_location);
       
     BEGIN
        EXECUTE IMMEDIATE l_update_str;COMMIT;
     exception WHEN others THEN
      lv_program_location :=' Exception in updating the other sales column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;

  END loop;
  
  
  BEGIN
       l_update_str:= 'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set HIT4_STORE_SALES  =(select count(distinct oeh.header_id)
      from   oe_order_headers_all      oeh,
             oe_order_lines_all        oel,
             gl_period_statuses    gps,
             org_organization_definitions   ood 
      where 1=1
      and oel.inventory_item_id       = tab.inventory_item_id
      and oel.ship_from_org_id        = tab.organization_id
      and oel.header_id               = oeh.header_id
      and  oel.Flow_Status_Code       = ''CLOSED''
      and oel.line_category_code      <> ''RETURN''
      AND  oel.invoice_interface_status_code = ''YES''
      and gps.application_id  =101
      and trunc(oeh.ordered_date) between gps.start_date and gps.end_date
      and not exists (select 1 from oe_order_lines_all olr
                          where olr.line_category_code = ''RETURN''
                          and   olr.return_context     = ''ORDER''
                          and   olr.flow_status_code = ''CLOSED''
                          and   olr.invoice_interface_status_code = ''YES''
                          and   olr.reference_header_id = oel.header_id 
                          and   olr.reference_line_id   = oel.line_id 
                          and  (nvl(olr.ordered_quantity,0) - nvl(olr.cancelled_quantity,0))=(nvl(oel.ordered_quantity,0) - nvl(oel.cancelled_quantity,0))  
                          )
      and ood.set_of_books_id         = gps.set_of_books_id
      and ood.organization_id         = oel.ship_from_org_id       
      and gps.effective_period_num >=(select effective_period_num 
                                      from gl_period_statuses gps1
                                      where application_id=101
                                      and  set_of_books_id=gps.set_of_books_id
                                      and period_name ='''||period_name_tab(4)||''' )'||
     ' and gps.effective_period_num <= (select effective_period_num 
                                       from gl_period_statuses gps1
                                       where application_id=101
                                       and  set_of_books_id=gps.set_of_books_id
                                       and period_name ='''||period_name_tab(1)||''') )';
     dbms_output.put_line('L_UPDATE_STR='||l_update_str);
     fnd_file.put_line(fnd_file.LOG,'L_UPDATE_STR='||l_update_str);
     lv_program_location :=' update sql query ='||l_update_str;
      EXECUTE IMMEDIATE l_update_str;    COMMIT;
   exception WHEN others THEN
    lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
    write_log(lv_program_location);
     END;
     
   ---DC mode calcualations
     BEGIN
       l_update_str:= 'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set HIT4_OTHER_INV_SALES  =(select count(distinct oeh.header_id)
      from   oe_order_headers_all      oeh,
             oe_order_lines_all        oel,
             gl_period_statuses    gps,
             org_organization_definitions   ood ,
             mtl_system_items_kfv msi
      where 1=1
      and  oel.header_id             = oeh.header_id
      and  oel.Flow_Status_Code      = ''CLOSED''
      and oel.line_category_code      <> ''RETURN''
      AND  oel.invoice_interface_status_code = ''YES''
      and tab.inventory_item_id      = msi.inventory_item_id
      and tab.organization_id        = msi.source_organization_id      
      and gps.application_id         = 101
      and trunc(oeh.ordered_date) between gps.start_date and gps.end_date
      and not exists (select 1 from oe_order_lines_all olr
                          where olr.line_category_code = ''RETURN''
                          and   olr.return_context     = ''ORDER''
                          and   olr.flow_status_code = ''CLOSED''
                          and   olr.invoice_interface_status_code = ''YES''
                          and   olr.reference_header_id = oel.header_id 
                          and   olr.reference_line_id   = oel.line_id 
                          and  (nvl(olr.ordered_quantity,0) - nvl(olr.cancelled_quantity,0))=(nvl(oel.ordered_quantity,0) - nvl(oel.cancelled_quantity,0))  
                          )
      and ood.set_of_books_id        = gps.set_of_books_id
      and ood.organization_id        = oel.ship_from_org_id 
      and msi.organization_id        = oel.ship_from_org_id
      and msi.inventory_item_id      = oel.inventory_item_id      
      and gps.effective_period_num >=(select effective_period_num 
                                     from gl_period_statuses gps1
                                     where application_id=101
                                     and  set_of_books_id=gps.set_of_books_id
                                     and period_name ='''||period_name_tab(4)||''' )'||
     ' and gps.effective_period_num <= (select effective_period_num 
                                     from gl_period_statuses gps1
                                     where application_id=101
                                     and  set_of_books_id=gps.set_of_books_id
                                     and period_name ='''||period_name_tab(1)||''' ))';     
     lv_program_location :=' update sql query ='||l_update_str;
     write_log(lv_program_location);
        EXECUTE IMMEDIATE l_update_str;    COMMIT;
   exception WHEN others THEN
    lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
    write_log(lv_program_location);   
     END;
     

       
          begin
       l_update_str:= 'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set HIT6_STORE_SALES  =(select count(distinct oeh.header_id)
      from   oe_order_headers_all      oeh,
             oe_order_lines_all        oel,
             gl_period_statuses    gps,
             org_organization_definitions   ood 
      where 1=1
      and oel.header_id              = oeh.header_id
      and  oel.Flow_Status_Code      = ''CLOSED''
      and  oel.line_category_code      <> ''RETURN''
      AND  oel.invoice_interface_status_code = ''YES''
      and oel.inventory_item_id      = tab.inventory_item_id
      and oel.ship_from_org_id       = tab.organization_id
      and gps.application_id         = 101
      and trunc(oeh.ordered_date) between gps.start_date and gps.end_date
            and not exists (select 1 from oe_order_lines_all olr
                          where olr.line_category_code = ''RETURN''
                          and   olr.return_context     = ''ORDER''
                          and   olr.flow_status_code = ''CLOSED''
                          and   olr.invoice_interface_status_code = ''YES''
                          and   olr.reference_header_id = oel.header_id 
                          and   olr.reference_line_id   = oel.line_id 
                          and  (nvl(olr.ordered_quantity,0) - nvl(olr.cancelled_quantity,0))=(nvl(oel.ordered_quantity,0) - nvl(oel.cancelled_quantity,0))  
                          )
      and ood.set_of_books_id       = gps.set_of_books_id
      and ood.organization_id       = oel.ship_from_org_id 
      and gps.effective_period_num >=(select effective_period_num 
                                      from gl_period_statuses gps1
                                      where application_id=101
                                      and  set_of_books_id=gps.set_of_books_id
                                      and period_name ='''||period_name_tab(6)||''' )'||
     ' and gps.effective_period_num <= (select effective_period_num 
                                       from gl_period_statuses gps1
                                       where application_id=101
                                       and  set_of_books_id=gps.set_of_books_id
                                       and period_name ='''||period_name_tab(1)||''') )';     
     lv_program_location :=' update sql query ='||l_update_str;
     write_log(lv_program_location);
        EXECUTE IMMEDIATE l_update_str;    COMMIT;
   exception WHEN others THEN
   lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
   write_log(lv_program_location);
     END;
     
         ---DC mode calcualations  
         
       BEGIN
       l_update_str:= 'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set HIT6_OTHER_INV_SALES  =(select count(distinct oeh.header_id)
      from   oe_order_headers_all      oeh,
             oe_order_lines_all        oel,
             gl_period_statuses    gps,
             org_organization_definitions   ood ,
             mtl_system_items_kfv msi
      where 1=1
      and  oel.header_id              = oeh.header_id
      and  oel.Flow_Status_Code       = ''CLOSED''
      and oel.line_category_code      <> ''RETURN''
      AND  oel.invoice_interface_status_code = ''YES''
      and tab.inventory_item_id       = msi.inventory_item_id
      and tab.organization_id         = msi.source_organization_id
      and gps.application_id  =101
      and trunc(oeh.ordered_date) between gps.start_date and gps.end_date
      and not exists (select 1 from oe_order_lines_all olr
                          where olr.line_category_code = ''RETURN''
                          and   olr.return_context     = ''ORDER''
                          and   olr.flow_status_code = ''CLOSED''
                          and   olr.invoice_interface_status_code = ''YES''
                          and   olr.reference_header_id = oel.header_id 
                          and   olr.reference_line_id   = oel.line_id 
                          and  (nvl(olr.ordered_quantity,0) - nvl(olr.cancelled_quantity,0))=(nvl(oel.ordered_quantity,0) - nvl(oel.cancelled_quantity,0))  
                          )
      and ood.set_of_books_id        = gps.set_of_books_id
      and ood.organization_id        = oel.ship_from_org_id 
      and msi.organization_id        = oel.ship_from_org_id
      and msi.inventory_item_id      = oel.inventory_item_id
      and gps.effective_period_num >=( select effective_period_num 
                                       from gl_period_statuses gps1
                                       where application_id=101
                                       and  set_of_books_id=gps.set_of_books_id
                                       and period_name ='''||period_name_tab(6)||''' )'||
     ' and gps.effective_period_num <=(select effective_period_num 
                                       from gl_period_statuses gps1
                                       where application_id=101
                                       and  set_of_books_id=gps.set_of_books_id
                                       and period_name ='''||period_name_tab(1)||''') )';
 
     lv_program_location :=' update sql query ='||l_update_str;
     write_log(lv_program_location);
        EXECUTE IMMEDIATE l_update_str;    COMMIT;
   exception WHEN others THEN
   lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
   write_log(lv_program_location);
     END;
     
  BEGIN
      UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET one_store_sale =(SELECT sum(ol.ordered_quantity)
      FROM   oe_order_lines_all ol,
             OE_ORDER_HEADERS_ALL OH
      where oh.header_id          = ol.header_id
      and inventory_item_id       = tab.inventory_item_id
      and ol.ship_from_org_id     = tab.organization_id
      and  ol.flow_status_code    = 'CLOSED'
      and ol.line_category_code   <> 'RETURN'
      and ol.invoice_interface_status_code = 'YES' 
      and not exists (select 1 from oe_order_lines_all olr
                          where olr.line_category_code = 'RETURN'
                          and   olr.return_context     = 'ORDER'
                          and   olr.flow_status_code = 'CLOSED'
                          and   olr.invoice_interface_status_code = 'YES' 
                          and   olr.reference_header_id = ol.header_id 
                          and   olr.reference_line_id   = ol.line_id 
                          and  (nvl(olr.ordered_quantity,0) - nvl(olr.cancelled_quantity,0))=(nvl(ol.ordered_quantity,0) - nvl(ol.cancelled_quantity,0))  
                          )
      and trunc(oh.ordered_date) between (trunc(sysdate)-30) and  trunc(sysdate));

      exception WHEN others THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
     
       ---DC mode calcualations  
         BEGIN
      UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET one_other_inv_sale =(SELECT sum(ol.ordered_quantity)
      FROM   oe_order_lines_all ol,
             oe_order_headers_all oh,
             mtl_system_items_kfv msi
      where oh.header_id              = ol.header_id
      and msi.inventory_item_id       = tab.inventory_item_id
      and msi.source_organization_id  = tab.organization_id
      and msi.organization_id         = ol.ship_from_org_id
      and msi.inventory_item_id       = ol.inventory_item_id
      and  ol.flow_status_code       = 'CLOSED'
      and ol.line_category_code   <> 'RETURN'
      and ol.invoice_interface_status_code = 'YES' 
      and not exists (select 1 from oe_order_lines_all olr
                          where olr.line_category_code = 'RETURN'
                          and   olr.return_context     = 'ORDER'
                          and   olr.flow_status_code = 'CLOSED'
                          and   olr.invoice_interface_status_code = 'YES' 
                          and   olr.reference_header_id = ol.header_id 
                          and   olr.reference_line_id   = ol.line_id 
                          and  (nvl(olr.ordered_quantity,0) - nvl(olr.cancelled_quantity,0))=(nvl(ol.ordered_quantity,0) - nvl(ol.cancelled_quantity,0))  
                          )
      AND trunc(oh.ordered_date) BETWEEN (trunc(SYSDATE)-30) AND  trunc(SYSDATE));

      exception WHEN others THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
     
     
       BEGIN
      UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET six_store_sale =(SELECT sum(ol.ordered_quantity)
      from   oe_order_lines_all ol,
             oe_order_headers_all oh
      where oh.header_id          = ol.header_id
      and inventory_item_id       = tab.inventory_item_id
      and ol.ship_from_org_id     = tab.organization_id
      and  ol.flow_status_code   = 'CLOSED'
      and ol.line_category_code   <> 'RETURN'
      and ol.invoice_interface_status_code = 'YES' 
      and not exists (select 1 from oe_order_lines_all olr
                          where olr.line_category_code = 'RETURN'
                          and   olr.return_context     = 'ORDER'
                          and   olr.flow_status_code = 'CLOSED'
                          and   olr.invoice_interface_status_code = 'YES' 
                          and   olr.reference_header_id = ol.header_id 
                          and   olr.reference_line_id   = ol.line_id 
                          and  (nvl(olr.ordered_quantity,0) - nvl(olr.cancelled_quantity,0))=(nvl(ol.ordered_quantity,0) - nvl(ol.cancelled_quantity,0))  
                          )
      AND trunc(oh.ordered_date) BETWEEN (trunc(SYSDATE)-182) AND trunc(SYSDATE));

      exception WHEN others THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
  
  
     ---DC mode calcualations 
     
      BEGIN
          UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET six_other_inv_sale =(SELECT sum(ol.ordered_quantity)
          FROM   oe_order_lines_all ol,
                 oe_order_headers_all oh,
                 mtl_system_items_kfv msi
          where oh.header_id              = ol.header_id
          and msi.inventory_item_id       = tab.inventory_item_id
          and msi.source_organization_id  = tab.organization_id 
          and msi.organization_id         = ol.ship_from_org_id
          and msi.inventory_item_id       = ol.inventory_item_id
          and  ol.flow_status_code        = 'CLOSED'
          and ol.line_category_code   <> 'RETURN'
          AND ol.invoice_interface_status_code = 'YES' 
          AND trunc(oh.ordered_date) BETWEEN (trunc(SYSDATE)-182) AND trunc(SYSDATE));

      EXCEPTION WHEN OTHERS THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
  
      BEGIN
          UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET twelve_store_sale =(SELECT sum(ol.ordered_quantity)
          from   oe_order_lines_all ol,
                 oe_order_headers_all oh
          where oh.header_id           = ol.header_id
          and inventory_item_id        = tab.inventory_item_id
          and ol.ship_from_org_id      = tab.organization_id
          and  ol.flow_status_code     = 'CLOSED'
          and ol.line_category_code   <> 'RETURN'
          and not exists (select 1 from oe_order_lines_all olr
                          where olr.line_category_code = 'RETURN'
                          and   olr.return_context     = 'ORDER'
                          and   olr.flow_status_code = 'CLOSED'
                          and   olr.invoice_interface_status_code = 'YES' 
                          and   olr.reference_header_id = ol.header_id 
                          and   olr.reference_line_id   = ol.line_id 
                          and  (nvl(olr.ordered_quantity,0) - nvl(olr.cancelled_quantity,0))=(nvl(ol.ordered_quantity,0) - nvl(ol.cancelled_quantity,0))  
                          )
          AND trunc(oh.ordered_date) BETWEEN (trunc(SYSDATE)-365) AND trunc(SYSDATE)
          );
      EXCEPTION when OTHERS then
       lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
       write_log(lv_program_location);
     END;
     ---DC mode calcualations 
   
   
      BEGIN
          UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET twelve_other_inv_sale =(SELECT sum(ol.ordered_quantity)
          FROM   oe_order_lines_all ol,
                 oe_order_headers_all oh,
                 mtl_system_items_kfv msi
          where oh.header_id              = ol.header_id
          and msi.inventory_item_id       = tab.inventory_item_id
          and msi.source_organization_id  = tab.organization_id
          and msi.organization_id         = ol.ship_from_org_id
          and msi.inventory_item_id       = ol.inventory_item_id
          and ol.flow_status_code         = 'CLOSED'
          and ol.line_category_code   <> 'RETURN'
          and ol.invoice_interface_status_code = 'YES' 
          and not exists (select 1 from oe_order_lines_all olr
                          where olr.line_category_code = 'RETURN'
                          and   olr.return_context     = 'ORDER'
                          and   olr.flow_status_code = 'CLOSED'
                          and   olr.invoice_interface_status_code = 'YES' 
                          and   olr.reference_header_id = ol.header_id 
                          and   olr.reference_line_id   = ol.line_id 
                          and  (nvl(olr.ordered_quantity,0) - nvl(olr.cancelled_quantity,0))=(nvl(ol.ordered_quantity,0) - nvl(ol.cancelled_quantity,0))  
                          )
          AND trunc(oh.ordered_date) BETWEEN (trunc(SYSDATE)-365) AND trunc(SYSDATE)
      );
      EXCEPTION WHEN OTHERS THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
     
     
  END  ;--MAIN
  
 PROCEDURE ISR_RPT_PROC
                     (p_process_id           IN NUMBER,
                      p_region               IN VARCHAR2,
                      p_district             IN VARCHAR2,
                      p_location             IN VARCHAR2,
                      p_dc_mode              IN VARCHAR2,
                      p_tool_repair          IN VARCHAR2,
                      p_time_sensitive       IN VARCHAR2,   
                      p_stk_items_with_hit4  IN VARCHAR2,
                      p_Report_Condition     in varchar2,
                      p_report_criteria      in varchar2,
                      p_report_criteria_val  in varchar2,
                      p_start_bin_loc        in varchar2,
                      p_end_bin_loc          in varchar2,
                      p_vendor               in varchar2,
                      p_item                 in varchar2,
                      p_cat_class            in varchar2 ,
                      P_ORG_LIST             IN VARCHAR2,
                      P_ITEM_LIST            IN VARCHAR2,
                      p_supplier_list        in varchar2,
                      P_CAT_CLASS_LIST       in varchar2,
                      P_SOURCE_LIST          in varchar2
                      )
 IS
 l_query                varchar2(32000);
 L_CONDITION_STR        VARCHAR2(32000);
 LV_PROGRAM_LOCATION    VARCHAR2(2000);
 L_REF_CURSOR           CURSOR_TYPE;
 L_INSERT_RECD_CNT      NUMBER;
 L_ORG_LIST_VLAUES      VARCHAR2(32000);
 L_SUPPLIER_EXISTS      VARCHAR2(32767);
 L_ORG_EXISTS           VARCHAR2(32000);
 l_item_exists          varchar2(32000);
 L_CATCLASS_EXISTS      VARCHAR2(32000);  
 L_source_exists          VARCHAR2(32000); 
 L_LLEGTH NUMBER;
 

 BEGIN
       if p_region is not null then
           l_condition_str:= l_condition_str||' and REGION in ('||xxeis.eis_rs_utility.get_param_values(p_region)||' )';
       END IF;
       if p_district is not null then
          l_condition_str:= l_condition_str||' and DISTRICT in ('||xxeis.eis_rs_utility.get_param_values(p_district)||' )';
       END IF;
       if p_location is not null then
          L_CONDITION_STR:= L_CONDITION_STR||' and ORG in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_LOCATION)||' )';
        /*  l_location_exists:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''ORG''
                                     and list_values like '''||'%'||p_location||'%'||'''
                                     )';
      ELSE
       l_location_exists:='and 1=1';*/
       end if;
       if p_vendor is not null then
          L_CONDITION_STR:= L_CONDITION_STR||' and VENDOR_NAME in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_VENDOR)||' )';
      /* l_vendor_exists :='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''VENDOR''
                                     and list_values like '''||'%'||p_vendor||'%'||'''
                                     )';
        FND_FILE.PUT_LINE(FND_FILE.LOG,'l_vendor_exists is '||L_VENDOR_EXISTS);
        ELSE
        l_vendor_exists:='and 1=1';*/
       END IF;
       if p_item is not null then
          L_CONDITION_STR:= L_CONDITION_STR||' and msi.concatenated_segments in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ITEM)||' )';
      /* L_ITEM_EXISTS:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''ITEM''
                                     and list_values like '''||'%'||p_item||'%'||'''
                                     )';
       ELSE
        L_ITEM_EXISTS:='and 1=1';*/
      
       end if;
       if p_cat_class is not null then
          l_condition_str:= l_condition_str||' and CAT in ('||xxeis.eis_rs_utility.get_param_values(p_cat_class)||' )';
       END IF;       
     /*  If P_Dc_Mode Is Not Null
       l_condition_str:= l_condition_str:=||'and ORG_NAME ='P_Location;
       Then
       End If;*/
       if (P_TOOL_REPAIR ='Tool Repair Only' or P_TOOL_REPAIR like 'Tool%') then             
         l_condition_str:= l_condition_str||' and CAT = ''TH01''';
       end if;
       
       if p_tool_repair ='Exclude' then             
         l_condition_str:= l_condition_str||' and CAT <> ''TH01''';
       END IF;
       
       if p_report_condition='All items' then
           l_condition_str:= l_condition_str || ' and 1=1 ';
       end if;    
       
       if p_report_condition='All items on hand' then
            l_condition_str:= l_condition_str||' and QOH > 0 ';
       end if;    
       
        if (p_report_condition='Non-stock on hand' or p_report_condition like 'Non-%')  then
          l_condition_str:= l_condition_str||' and STK_FLAG  =''N'''||' and QOH > 0 ';
       end if;    
       
       if (p_report_condition='Non stock only' or (p_report_condition like 'Non%' and p_report_condition not like 'Non-%')) then
            l_condition_str:= l_condition_str||' and STK_FLAG =''N'' ';
       end if;        
       
       if p_report_condition='Active large' then
            L_CONDITION_STR:= L_CONDITION_STR||' and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or twelve_sales > 0 or on_ord > 0 or Available > 0 or Available < 0)';
       end if;       
       
       if p_report_condition='Active small' then
          l_condition_str:= l_condition_str||' AND (( STK_FLAG =''Y'' OR (STK_FLAG =''N'' and (QOH > 0 or on_ord > 0 or open_req > 0 or  int_req > 0 or demand > 0 or dir_req > 0 or Available > 0 )))'
                                            ||   ' AND ((NOT EXISTS (select 1 from dual where REGEXP_LIKE(substr(item_number,1,1),''[a-zA-Z]''))) or item_number like ''SP%''))';
          ---l_condition_str:= l_condition_str||' and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or on_ord > 0 or Available > 0 or Available < 0)';
       end if;  
       
       if p_report_condition='Stock items only' then
          l_condition_str:= l_condition_str||' and STK_FLAG =''Y''';
       end if;       
       
       if p_report_condition='Stock items with 0/0 min/max' then
          l_condition_str:= l_condition_str||' and STK_FLAG =''Y'''||' and MINN=0 and MAXN=0';
       end if;          
       
      if (p_time_sensitive ='Time Sensitive Only' or p_time_sensitive like 'Time%') then
          l_condition_str:= l_condition_str||' and TS > 0 and TS < 4000';
       END IF;
       
        IF p_stk_items_with_hit4 IS NOT NULL  THEN
          l_condition_str:= l_condition_str||' and STK_FLAG =''Y'''|| ' and hit4_sales >'||p_stk_items_with_hit4;
       END IF;      
       
       IF P_ORG_LIST IS NOT NULL THEN 
         L_ORG_EXISTS:= ' AND EXISTS (SELECT 1  FROM 
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME='''||P_ORG_LIST||'''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                         TAB.org, 1, 1)<>0) ';
          ELSE
       
       L_ORG_EXISTS :=' and 1=1 ';
       
       END IF;
    
         IF P_SUPPLIER_LIST IS NOT NULL THEN 
       L_SUPPLIER_EXISTS :=' AND EXISTS (SELECT 1  FROM 
                            APPS.XXWC_PARAM_LIST
                            WHERE LIST_NAME='''||P_SUPPLIER_LIST||'''
                            AND DBMS_LOB.INSTR(LIST_VALUES,
                            TAB.vendor_name, 1, 1)<>0) ';
       ELSE
       L_SUPPLIER_EXISTS :=' and 1=1 ';
       END IF;
       IF P_ITEM_LIST IS NOT NULL THEN 
        L_ITEM_EXISTS:=' AND EXISTS (SELECT 1  FROM 
                        APPS.XXWC_PARAM_LIST
                        WHERE LIST_NAME='''||P_ITEM_LIST||'''
                        AND DBMS_LOB.INSTR(LIST_VALUES,
                        TAB.item_number, 1, 1)<>0) ';
         ELSE
        L_ITEM_EXISTS :=' and 1=1 ';
        end if;
        
       IF P_CAT_CLASS_LIST IS NOT NULL THEN 
         L_CATCLASS_EXISTS:= ' AND EXISTS (SELECT 1  FROM 
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME='''||P_CAT_CLASS_LIST||'''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                         TAB.cat, 1, 1)<>0) ';
          ELSE
       
       L_CATCLASS_EXISTS :=' and 1=1 ';
       
       end if;        
       
      IF P_SOURCE_LIST IS NOT NULL THEN 
         L_source_exists:= ' AND EXISTS (SELECT 1  FROM 
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME='''||P_SOURCE_LIST||'''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                         TAB.source, 1, 1)<>0) ';
          ELSE
       
       L_source_exists :=' and 1=1 ';
       
       END IF;               
      
    
    
    
    if p_report_criteria is not null and p_report_criteria_val is not null  then
          if (p_report_criteria='Vendor Number' or p_report_criteria like 'Vendor%')  then
             l_condition_str:= l_condition_str||' and upper(VENDOR_NUM) like '''||upper(p_report_criteria_val)||''''; 
             Fnd_File.Put_Line(Fnd_File.Log,l_condition_str);
          end if;
          if (p_report_criteria='First 3 digits of Item' or p_report_criteria like 'First%')  then
             l_condition_str:= l_condition_str||' and upper(PRE) = '''||upper(p_report_criteria_val)||''''; 
          end if;
          if (p_report_criteria='Item number' or p_report_criteria like 'Item%')   then
            l_condition_str:= l_condition_str||' and upper(ITEM_NUMBER) = '''||upper(p_report_criteria_val)||''''; 
          end if;   
          if p_report_criteria='Source' and p_report_criteria_val is not null  then
             L_Condition_Str:= L_Condition_Str||' and upper(SOURCE) = '''||upper(p_report_criteria_val)||''''; 
            NULL;
          END IF; 
          IF p_report_criteria='External source vendor'   then
             -- L_Condition_Str:= L_Condition_Str||' and ITEM_NUMBER = '''||P_Report_Criteria_Val||''; 
          null;
          end if;   
          if (p_report_criteria='2 Digit Cat' or p_report_criteria like '2%')  then
             l_condition_str:= l_condition_str||' and upper(inv_cat_seg1) = '''||upper(p_report_criteria_val)||''''; 
            null;
          END IF;  
          if (p_report_criteria='4 Digit Cat Class' or p_report_criteria like '4%')   then
             l_condition_str:= l_condition_str||' and upper(CAT) = '''||upper(p_report_criteria_val)||''''; 
          END IF;      
          if (P_REPORT_CRITERIA='Default buyer' or P_REPORT_CRITERIA like 'Default%')  then
             l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(p_report_criteria_val)||''''; --commented by santosh
            -- l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(replace(p_report_criteria_val,''''))||''''; --commented by santosh
--             L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(P_REPORT_CRITERIA_VAL)||''')'; -- added by santosh
      --       L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(replace(replace(P_REPORT_CRITERIA_VAL,'''',''),',',', '))||''')'; -- added by santosh

             --l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_report_criteria_val))||''''; 
          end if;              
       END IF;  
       if p_start_bin_loc is not null  then
         l_condition_str:= l_condition_str||' and upper(Bin_Loc) >= '''||upper(p_start_bin_loc)||''''; 
       END IF;
       if p_end_bin_loc is not null   then
         l_condition_str:= l_condition_str||' and upper(Bin_Loc) <= '''||upper(p_end_bin_loc)||''''; 
       END IF;         
      
      
    /*   IF p_all_items ='Yes'   THEN
       l_condition_str:= ' and 1=1';
       END IF;
*/


  l_query:= 'select tab.* 
             from 
             xxeis.EIS_XXWC_PO_ISR_V tab,
             mtl_system_items_kfv msi
             where 1=1 
             and msi.inventory_item_id = tab.inventory_item_id
             and msi.organization_id   = tab.organization_id  '||
             L_CONDITION_STR||L_ORG_EXISTS||L_SUPPLIER_EXISTS||L_ITEM_EXISTS||L_CATCLASS_EXISTS||L_SOURCE_EXISTS;
            lv_program_location:='The main query is ' ||l_query;
            write_log(lv_program_location);
            l_insert_recd_cnt:=1;
   OPEN l_ref_cursor FOR l_query;
   LOOP
        FETCH l_ref_cursor  INTO g_view_record;
        Exit When L_Ref_Cursor%Notfound; 
        G_View_Tab(L_Insert_Recd_Cnt).Org:=                   G_View_Record.Org ;                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt).Pre:=                   G_View_Record.Pre ;                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt).ITEM_NUMBER:=           g_view_record.ITEM_NUMBER ;                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt).Vendor_Num:=            G_View_Record.Vendor_Num  ;                                                                                                                                                                    
        G_View_Tab(L_Insert_Recd_Cnt ).VENDOR_NAME:=          g_view_record.VENDOR_NAME ;                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).Source:=               G_View_Record.Source ;                                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).St:=                   G_View_Record.St      ;                                                                                                                                                                  
        G_View_Tab(L_Insert_Recd_Cnt ).Description:=          G_View_Record.Description  ;                                                                                                                                                                                
        G_View_Tab(L_Insert_Recd_Cnt ).Cat:=                  G_View_Record.Cat     ;                                                                                                                                                                                       
        G_View_Tab(L_Insert_Recd_Cnt ).Pplt:=                 G_View_Record.Pplt  ;                                                                                                                                                                                         
        G_View_Tab(L_Insert_Recd_Cnt ).Plt:=                  G_View_Record.Plt   ;                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Uom:=                  G_View_Record.Uom     ;                                                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).Cl:=                   G_View_Record.Cl     ;                                                                                                                                                                                                
        G_View_Tab(L_Insert_Recd_Cnt ).Stk_Flag:=             G_View_Record.Stk_Flag  ;                                                                                                                                                                                                     
        G_View_Tab(L_Insert_Recd_Cnt ).Pm:=                   G_View_Record.Pm      ;                                                                                                                                                                                                
        G_View_Tab(L_Insert_Recd_Cnt ).Minn:=                 G_View_Record.Minn    ;                                                                                                                                                                                                          
        G_View_Tab(L_Insert_Recd_Cnt ).Maxn:=                 G_View_Record.Maxn  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Amu:=                  G_View_Record.Amu     ;                                                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).Mf_Flag:=              G_View_Record.Mf_Flag    ;                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Hit6_Sales:=           G_View_Record.Hit6_Sales     ;                                                                                                                                                                                                         
        G_View_Tab(L_Insert_Recd_Cnt ).Aver_Cost:=            G_View_Record.Aver_Cost   ;                                                                                                                                                                                                           
        G_View_Tab(L_Insert_Recd_Cnt ).Item_Cost:=            G_View_Record.Item_Cost;                                                                                                                                                                                                              
        G_VIEW_TAB(L_INSERT_RECD_CNT ).BPA:=                  G_VIEW_RECORD.BPA;   
        G_View_Tab(L_Insert_Recd_Cnt ).Bpa_cost:=             G_View_Record.Bpa_cost; 
        G_View_Tab(L_Insert_Recd_Cnt ).Qoh:=                  G_View_Record.Qoh;                                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Available:=            G_View_Record.Available;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Availabledollar:=      G_View_Record.Availabledollar;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Jan_Sales:=            G_View_Record.Jan_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Feb_Sales:=            G_View_Record.Feb_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Mar_Sales:=            G_View_Record.Mar_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Apr_Sales:=            G_View_Record.Apr_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).May_Sales:=            G_View_Record.May_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).June_Sales:=           G_View_Record.June_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Jul_Sales:=            G_View_Record.Jul_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Aug_Sales:=            G_View_Record.Aug_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Sep_Sales:=            G_View_Record.Sep_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Oct_Sales:=            G_View_Record.Oct_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Nov_Sales:=            G_View_Record.Nov_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Dec_Sales:=            G_View_Record.Dec_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Hit4_Sales:=           G_View_Record.Hit4_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).One_Sales:=            G_View_Record.One_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Six_Sales:=            G_View_Record.Six_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Twelve_Sales:=         G_View_Record.Twelve_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Bin_Loc:=              G_View_Record.Bin_Loc;                                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt ).Mc:=                   G_View_Record.Mc;                                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt ).Fi_Flag:=              G_View_Record.Fi_Flag;                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Freeze_Date:=          G_View_Record.Freeze_Date;                                                                                                                                                                                         
        G_View_Tab(L_Insert_Recd_Cnt ).Res:=                  G_View_Record.Res;                                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt ).Thirteen_Wk_Avg_Inv:=  G_View_Record.Thirteen_Wk_Avg_Inv  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).THIRTEEN_WK_AN_COGS:=  g_view_record.THIRTEEN_WK_AN_COGS ;                                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Turns:=                G_View_Record.Turns  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Buyer:=                G_View_Record.Buyer  ;                                                                                                                                                                                                    
        G_VIEW_TAB(L_INSERT_RECD_CNT ).TS:=                   G_VIEW_RECORD.TS   ;                                                                                                                                                                                                           
        G_VIEW_TAB(L_INSERT_RECD_CNT ).SO:=                   G_VIEW_RECORD.SO  ;  
        g_view_tab(l_insert_recd_cnt ).sourcing_rule:=        g_view_record.sourcing_rule  ;  
        g_View_Tab(L_Insert_Recd_Cnt ).clt:=                  g_View_Record.clt  ; 
        G_View_Tab(L_Insert_Recd_Cnt ).INVENTORY_ITEM_ID:=    g_view_record.INVENTORY_ITEM_ID  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Organization_Id:=      G_View_Record.Organization_Id   ;                                                                                                                                                                                                           
        g_view_tab(l_insert_recd_cnt ).set_of_books_id:=      g_view_record.set_of_books_id  ;
        G_View_Tab(L_Insert_Recd_Cnt ).on_ord:=               g_view_record.on_ord ;
        G_View_Tab(L_Insert_Recd_Cnt ).ORG_NAME:=             g_view_record.ORG_NAME   ;                                                                                                                                                                                          
        G_View_Tab(L_Insert_Recd_Cnt ).District:=             G_View_Record.District ;                                                                                                                                                                                          
        g_view_tab(l_insert_recd_cnt ).region:=               g_view_record.region ;          
        G_View_Tab(L_Insert_Recd_Cnt ).wt:=             g_view_record.wt   ;                                                                                                                                                                                          
        g_view_tab(l_insert_recd_cnt ).ss:=             g_view_record.ss ;                                                                                                                                                                                          
        g_view_tab(l_insert_recd_cnt ).fml:=               g_view_record.fml ;  
        g_view_tab(l_insert_recd_cnt ).open_req:=               g_view_record.open_req ;          
        
        g_view_tab(l_insert_recd_cnt ).common_output_id:=xxeis.eis_rs_common_outputs_s.nextval;
        g_view_tab(l_insert_recd_cnt ).process_id:=p_process_id;
        g_view_tab(l_insert_recd_cnt ).avail2:=G_View_Record.avail2;
        g_view_tab(l_insert_recd_cnt ).int_req:=G_View_Record.int_req;
        g_view_tab(l_insert_recd_cnt ).dir_req:=G_View_Record.dir_req;
        g_view_tab(l_insert_recd_cnt ).demand:=G_View_Record.demand;
        l_insert_recd_cnt:=l_insert_recd_cnt+1;       
   END loop;
   CLOSE l_ref_cursor; 
   begin
  
         fnd_file.put_line(fnd_file.log,'l_insert_recd_cnt' ||l_insert_recd_cnt);
         fnd_file.put_line(fnd_file.log,'first common_output_id' ||g_view_tab(1).common_output_id);
     --    Fnd_File.Put_Line(Fnd_File.Log,'last common_output_id' ||G_View_Tab(L_Insert_Recd_Cnt).Common_Output_Id);
         IF l_insert_recd_cnt >= 1  then
            forall i IN g_view_tab.FIRST .. g_view_tab.LAST 
              INSERT INTO xxeis.eis_xxwc_po_isr_rpt_v
                    values g_view_tab (i);
         END IF;      
         commit;
         
         g_view_tab.DELETE;
    exception
         when others
         then
           fnd_file.put_line(fnd_file.log,'Inside Exception==>'||substr(sqlerrm,1,240));
      END;

  END;
  
 
End;
/