--Report Name            : Bin Location Data Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_INV_BIN_LOC_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_BIN_LOC_V
Create or replace View XXEIS.EIS_XXWC_INV_BIN_LOC_V
(ORGANIZATION,PART_NUMBER,DESCRIPTION,UOM,SELLING_PRICE,VENDOR_NAME,VENDOR_NUMBER,MIN_MINMAX_QUANTITY,MAX_MINMAX_QUANTITY,AVERAGECOST,WEIGHT,CAT,ONHAND,MTD_SALES,YTD_SALES,PRIMARY_BIN_LOC,ALTERNATE_BIN_LOC,BIN_LOC,LOCATION,STK,INVENTORY_ITEM_ID,INV_ORGANIZATION_ID,ORG_ORGANIZATION_ID,APPLICATION_ID,SET_OF_BOOKS_ID) AS 
SELECT ood.organization_code ORGANIZATION,
    msi.segment1 part_number,
    msi.description,
    msi.primary_uom_code uom,
    msi.list_price_per_unit selling_price,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor_name,
    xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_number(msi.inventory_item_id,msi.organization_id) vendor_number,
    min_minmax_quantity ,
    max_minmax_quantity ,
    NVL(apps.Cst_Cost_Api.Get_Item_Cost(1,Msi.Inventory_Item_Id,Msi.Organization_Id),0) Averagecost,
    msi.unit_weight weight,
    xxeis.eis_rs_xxwc_com_util_pkg.Get_inv_cat_class(msi.inventory_item_id,msi.organization_id) cat,
    xxeis.eis_rs_xxwc_com_util_pkg.get_onhand_inv(msi.inventory_item_id,msi.organization_id) onhand,
    xxeis.eis_rs_xxwc_com_util_pkg.get_mtd_sales(msi.inventory_item_id,msi.organization_id,gps.start_date,gps.end_date) mtd_sales,
    xxeis.eis_rs_xxwc_com_util_pkg.get_ytd_sales(msi.inventory_item_id,msi.organization_id,gps.year_start_date) ytd_sales,
    xxeis.eis_rs_xxwc_com_util_pkg.get_primary_bin_loc(msi.inventory_item_id,msi.organization_id) primary_bin_loc,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ALTERNATE_BIN_LOC(MSI.INVENTORY_ITEM_ID,MSI.ORGANIZATION_ID) ALTERNATE_BIN_LOC,
    SUBSTR(msi.SEGMENT1,3) BIN_LOC,
    ood.organization_name location,
    CASE
      WHEN (mcvs.segment1 IN ('1', '2', '3', '4', '5', '6', '7', '8', '9', 'C','B'))
      THEN 'Y'
      WHEN ( mcvs.segment1    IN ('E')
      AND (min_minmax_quantity = 0
      AND max_minmax_quantity  = 0))
      THEN 'N'
      WHEN (mcvs.segment1     IN ('E')
      AND (min_minmax_quantity > 0
      AND max_minmax_quantity  > 0))
      THEN 'Y'
      WHEN (mcvs.segment1 IN ('N', 'Z'))
        --AND ITEM_TYPE                                                                                           ='NON-STOCK')
      THEN 'N'
      ELSE 'N'
    END stk,
    --primary keys
    msi.inventory_item_id,
    msi.organization_id inv_organization_id,
    ood.organization_id org_organization_id,
    gps.application_id,
    gps.set_of_books_id
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_system_items_kfv msi,
    org_organization_definitions ood,
    gl_period_statuses gps,
    Mtl_Categories_Kfv mcvs,
    Mtl_Item_Categories mics
  WHERE 1                 =1
  AND msi.organization_id =ood.organization_id
  AND application_id      =101
  AND gps.set_of_books_id =ood.set_of_books_id
  AND TRUNC(SYSDATE) BETWEEN gps.start_date AND gps.end_date
  AND msi.organization_id                                                                       = mics.organization_id(+)
  AND msi.inventory_item_id            = mics.inventory_item_id(+)
  AND mics.Category_Id                                                                          = mcvs.Category_Id(+)
  AND mcvs.Structure_Id(+)                                                                      = 50410
  AND mics.category_set_id(+)                                                                   = 1100000044
  AND xxeis.eis_rs_xxwc_com_util_pkg.get_bin_loc_flag(msi.inventory_item_id,msi.organization_id)='Y'
    /* AND NOT EXISTS
    (SELECT 1
    FROM mtl_item_locations
    WHERE segment1 LIKE '1-%'
    AND inventory_item_id=msi.inventory_item_id
    AND organization_id  =msi.organization_id
    )*/
  AND EXISTS
    (SELECT 1
    FROM xxeis.eis_org_access_v
    WHERE organization_id = msi.organization_id
    )
/
set scan on define on
prompt Creating View Data for Bin Location Data Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_BIN_LOC_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Rs Xxwc Inv Bin Loc V','EXIBLV','','');
--Delete View Columns for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_BIN_LOC_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','YTD_SALES',401,'Ytd Sales','YTD_SALES','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','DERIVED COLUMN','DERIVED COLUMN','Ytd Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MTD_SALES',401,'Mtd Sales','MTD_SALES','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','DERIVED COLUMN','DERIVED COLUMN','Mtd Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ONHAND',401,'Onhand','ONHAND','','','','XXEIS_RS_ADMIN','NUMBER','MTL_ONHAND_QUANTITIES_DETAIL','TRANSACTION_QUANTITY','Onhand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','AVERAGECOST',401,'Averagecost','AVERAGECOST','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Averagecost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MAX_MINMAX_QUANTITY',401,'Max Minmax Quantity','MAX_MINMAX_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','MAX_MINMAX_QUANTITY','Max Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','MIN_MINMAX_QUANTITY',401,'Min Minmax Quantity','MIN_MINMAX_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','MIN_MINMAX_QUANTITY','Min Minmax Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','UOM',401,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','PRIMARY_UOM_CODE','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','DESCRIPTION','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_KFV','SEGMENT1','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ORGANIZATION',401,'Organization','ORGANIZATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Organization','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ALTERNATE_BIN_LOC',401,'Alternate Bin Loc','ALTERNATE_BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Alternate Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','BIN_LOC',401,'Bin Loc','BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','LOCATION',401,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','PRIMARY_BIN_LOC',401,'Primary Bin Loc','PRIMARY_BIN_LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Primary Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','WEIGHT',401,'Weight','WEIGHT','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','UNIT_WEIGHT','Weight','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','VENDOR_NUMBER',401,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','VENDOR_NAME',401,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','DERIVED COLUMN','DERIVED COLUMN','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','SELLING_PRICE',401,'Selling Price','SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','LIST_PRICE_PER_UNIT','Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','CAT',401,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_CATEGORIES','SEGMENT2','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','INVENTORY_ITEM_ID','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','INV_ORGANIZATION_ID',401,'Inv Organization Id','INV_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_KFV','ORGANIZATION_ID','Inv Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','ORG_ORGANIZATION_ID',401,'Org Organization Id','ORG_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','ORG_ORGANIZATION_DEFINITIONS','ORGANIZATION_ID','Org Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','APPLICATION_ID',401,'Application Id','APPLICATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_PERIOD_STATUSES','APPLICATION_ID','Application Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_BIN_LOC_V','SET_OF_BOOKS_ID',401,'Set Of Books Id','SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_PERIOD_STATUSES','SET_OF_BOOKS_ID','Set Of Books Id','','','');
--Inserting View Components for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV',401,'MTL_SYSTEM_ITEMS_KFV','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Item Definitions','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','MTL_CATEGORIES',401,'MTL_CATEGORIES_B','MCV','MCV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Categories','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES',401,'GL_PERIOD_STATUSES','GPS','GPS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Calendar Period Statuses','','','','');
xxeis.eis_rs_ins.vcomp( 'EIS_XXWC_INV_BIN_LOC_V','GL_CODE_COMBINATIONS_KFV',401,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','GL Code Combinations','','','','');
--Inserting View Component Joins for EIS_XXWC_INV_BIN_LOC_V
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIBLV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','MTL_SYSTEM_ITEMS_KFV','MSI',401,'EXIBLV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','MTL_CATEGORIES','MCV',401,'EXIBLV.CATEGORY_ID','=','MCV.CATEGORY_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_PERIOD_STATUSES','GPS',401,'EXIBLV.PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','','XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.vcj( 'EIS_XXWC_INV_BIN_LOC_V','GL_CODE_COMBINATIONS_KFV','GCC',401,'EXIBLV.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN','','');
END;
/
set scan on define on
prompt Creating Report LOV Data for Bin Location Data Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Bin Location Data Report
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code,organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','XXWC INV ORGANIZATIONS LOV','List of All Inventory Orgs under a given operating unit.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT distinct substr(SEGMENT1,3) bin_location FROM MTL_ITEM_LOCATIONS_KFV','','INV Bin Location','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT distinct substr(SEGMENT1,3) bin_location FROM MTL_ITEM_LOCATIONS_KFV','','INV Bin Location','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Bin Location Data Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Bin Location Data Report
xxeis.eis_rs_utility.delete_report_rows( 'Bin Location Data Report' );
--Inserting Report - Bin Location Data Report
xxeis.eis_rs_ins.r( 401,'Bin Location Data Report','','Items without Primary Bin Locations Assigned','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_BIN_LOC_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Bin Location Data Report
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'AVERAGECOST','Average Cost','Averagecost','','~T~D~2','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'DESCRIPTION','Description','Description','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'MAX_MINMAX_QUANTITY','Max','Max Minmax Quantity','','~~~','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'MIN_MINMAX_QUANTITY','Min','Min Minmax Quantity','','~~~','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'MTD_SALES','MTD Units','Mtd Sales','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'ONHAND','Qty Onhand','Onhand','','~~~','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'PART_NUMBER','Item Number','Part Number','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'UOM','UOM','Uom','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'YTD_SALES','YTD Units','Ytd Sales','','~T~D~2','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'VENDOR NUMBER-NAME','Vendor Number-Name','Ytd Sales','VARCHAR2','','default','','4','Y','','','','','','','(EXIBLV.VENDOR_NUMBER||''-''||EXIBLV.VENDOR_NAME)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'ORGANIZATION','Organization','Organization','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'ALTERNATE_BIN_LOC','Alternate Bin Loc','Alternate Bin Loc','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'EXTENDED VALUE','Extended Value','Alternate Bin Loc','NUMBER','~T~D~2','default','','8','Y','','','','','','','(EXIBLV.ONHAND*EXIBLV.AVERAGECOST)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'LOCATION','Location','Location','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'PRIMARY_BIN_LOC','Primary Bin Loc','Primary Bin Loc','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
xxeis.eis_rs_ins.rc( 'Bin Location Data Report',401,'PRIMARY BIN','Primary bin count','Primary Bin Loc','NUMBER','~~~','default','','16','Y','','','','','','','decode(EXIBLV.primary_bin_loc,null,1,0)','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_BIN_LOC_V','','');
--Inserting Report Parameters - Bin Location Data Report
xxeis.eis_rs_ins.rp( 'Bin Location Data Report',401,'Organization','Organization','ORGANIZATION','IN','XXWC INV ORGANIZATIONS LOV','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location Data Report',401,'Starting Bin Location','Starting Bin Location','BIN_LOC','>=','INV Bin Location','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Bin Location Data Report',401,'Ending Bin Location','Ending Bin Location','BIN_LOC','<=','INV Bin Location','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Bin Location Data Report
xxeis.eis_rs_ins.rcn( 'Bin Location Data Report',401,'ORGANIZATION','IN',':Organization','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Bin Location Data Report',401,'','','','','AND (nvl(EXIBLV.ONHAND,0) <> 0 or EXIBLV.stk =''Y'')','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Bin Location Data Report
xxeis.eis_rs_ins.rs( 'Bin Location Data Report',401,'PART_NUMBER','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Bin Location Data Report
xxeis.eis_rs_ins.rt( 'Bin Location Data Report',401,'begin
xxeis.eis_rs_xxwc_com_util_pkg.g_start_bin:=:Starting Bin Location;
xxeis.eis_rs_xxwc_com_util_pkg.g_end_bin:=:Ending Bin Location;
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Bin Location Data Report
--Inserting Report Portals - Bin Location Data Report
--Inserting Report Dashboards - Bin Location Data Report
--Inserting Report Security - Bin Location Data Report
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50619',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50924',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','51052',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50879',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50851',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50852',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50821',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','20005','','50880',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','20634',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','','LB048272','',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','','10012196','',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','','MM050208','',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','','SO004816','',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','51029',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50882',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50883',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50981',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50855',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50884',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','20005','','50900',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50895',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50865',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50864',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50849',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','660','','50871',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50863',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50862',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','51104',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50846',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50845',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Bin Location Data Report','401','','50844',401,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Bin Location Data Report
xxeis.eis_rs_ins.rpivot( 'Bin Location Data Report',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location Data Report',401,'Pivot','LOCATION','ROW_FIELD','','Location','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Bin Location Data Report',401,'Pivot','PRIMARY BIN','DATA_FIELD','SUM','Number of Parts w/o Bin','2','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
