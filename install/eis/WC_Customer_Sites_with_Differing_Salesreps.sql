--Report Name            : WC Customer Sites with Differing Salesreps
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for WC Customer Sites with Differing Salesreps
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_CUSTSITES_DIFF_SALESREPS
xxeis.eis_rs_ins.v( 'XXWC_CUSTSITES_DIFF_SALESREPS',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Xxwc Custsites Diff Salesreps','XCDS','','');
--Delete View Columns for XXWC_CUSTSITES_DIFF_SALESREPS
xxeis.eis_rs_utility.delete_view_rows('XXWC_CUSTSITES_DIFF_SALESREPS',222,FALSE);
--Inserting View Columns for XXWC_CUSTSITES_DIFF_SALESREPS
xxeis.eis_rs_ins.vc( 'XXWC_CUSTSITES_DIFF_SALESREPS','PRIMARY_SALESREP_ID',222,'Primary Salesrep Id','PRIMARY_SALESREP_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Primary Salesrep Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTSITES_DIFF_SALESREPS','SALESREP',222,'Salesrep','SALESREP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTSITES_DIFF_SALESREPS','LOCATION',222,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTSITES_DIFF_SALESREPS','SITE_USE_CODE',222,'Site Use Code','SITE_USE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Use Code','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTSITES_DIFF_SALESREPS','PARTY_SITE_NUMBER',222,'Party Site Number','PARTY_SITE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTSITES_DIFF_SALESREPS','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_CUSTSITES_DIFF_SALESREPS','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
--Inserting View Components for XXWC_CUSTSITES_DIFF_SALESREPS
--Inserting View Component Joins for XXWC_CUSTSITES_DIFF_SALESREPS
END;
/
set scan on define on
prompt Creating Report Data for WC Customer Sites with Differing Salesreps
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC Customer Sites with Differing Salesreps
xxeis.eis_rs_utility.delete_report_rows( 'WC Customer Sites with Differing Salesreps' );
--Inserting Report - WC Customer Sites with Differing Salesreps
xxeis.eis_rs_ins.r( 222,'WC Customer Sites with Differing Salesreps','','This report shows all customer sites that have a different sales rep on the bill-to than on the ship-to','','','','XXEIS_RS_ADMIN','XXWC_CUSTSITES_DIFF_SALESREPS','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - WC Customer Sites with Differing Salesreps
xxeis.eis_rs_ins.rc( 'WC Customer Sites with Differing Salesreps',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTSITES_DIFF_SALESREPS','','');
xxeis.eis_rs_ins.rc( 'WC Customer Sites with Differing Salesreps',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTSITES_DIFF_SALESREPS','','');
xxeis.eis_rs_ins.rc( 'WC Customer Sites with Differing Salesreps',222,'LOCATION','Location','Location','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTSITES_DIFF_SALESREPS','','');
xxeis.eis_rs_ins.rc( 'WC Customer Sites with Differing Salesreps',222,'PARTY_SITE_NUMBER','Party Site Number','Party Site Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTSITES_DIFF_SALESREPS','','');
xxeis.eis_rs_ins.rc( 'WC Customer Sites with Differing Salesreps',222,'PRIMARY_SALESREP_ID','Primary Salesrep Id','Primary Salesrep Id','','~~~','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTSITES_DIFF_SALESREPS','','');
xxeis.eis_rs_ins.rc( 'WC Customer Sites with Differing Salesreps',222,'SALESREP','Salesrep','Salesrep','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTSITES_DIFF_SALESREPS','','');
xxeis.eis_rs_ins.rc( 'WC Customer Sites with Differing Salesreps',222,'SITE_USE_CODE','Site Use Code','Site Use Code','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','XXWC_CUSTSITES_DIFF_SALESREPS','','');
--Inserting Report Parameters - WC Customer Sites with Differing Salesreps
--Inserting Report Conditions - WC Customer Sites with Differing Salesreps
--Inserting Report Sorts - WC Customer Sites with Differing Salesreps
--Inserting Report Triggers - WC Customer Sites with Differing Salesreps
--Inserting Report Templates - WC Customer Sites with Differing Salesreps
--Inserting Report Portals - WC Customer Sites with Differing Salesreps
--Inserting Report Dashboards - WC Customer Sites with Differing Salesreps
--Inserting Report Security - WC Customer Sites with Differing Salesreps
xxeis.eis_rs_ins.rsec( 'WC Customer Sites with Differing Salesreps','222','','50992',222,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - WC Customer Sites with Differing Salesreps
END;
/
set scan on define on
