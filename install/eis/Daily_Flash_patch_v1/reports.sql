--Report Name            : Daily Flash Cash Report - for Sales Revenue as % to Plan
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_DAILY_FLASH_SALE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
Create or replace View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
(DATE1,BRANCH_LOCATION,SALE_AMT,SALE_COST,PAYMENT_TYPE,INVOICE_COUNT) AS 
SELECT TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from) date1,
    OOD.ORGANIZATION_CODE BRANCH_LOCATION,
    SUM(OL.ORDERED_QUANTITY *NVL(OL.UNIT_SELLING_PRICE,0)+NVL(OL.TAX_VALUE,0)) SALE_AMT,
    sum (ol.ordered_quantity*nvl(ol.unit_cost,0)+nvl(ol.tax_value,0))sale_cost,
   -- SUM(oep.payment_amount) cash_sales,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE PAYMENT_TYPE,
    COUNT( DISTINCT rct.trx_number) invoice_count
    ---Primary Keys
  FROM Ra_Customer_Trx Rct,
    ra_customer_trx_lines rctl,
    Oe_Order_Headers Oh,
    Oe_Order_Lines Ol,
    Org_Organization_Definitions Ood   
  WHERE 1                                            = 1
  AND TRUNC(oh.ordered_DATE)                         = TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
  AND TO_CHAR(oh.order_number)                       = TO_CHAR(rct.interface_header_attribute1)
  AND Ood.Organization_Id (+)                        = Ol.Ship_From_Org_Id
  AND TO_CHAR(rctl.interface_line_attribute6)        = TO_CHAR(Ol.Line_id)
  AND RCTL.INVENTORY_ITEM_ID                         = OL.INVENTORY_ITEM_ID
  AND rctl.customer_trx_id                           = rct.customer_trx_id
  AND ol.header_id                                   = oh.header_id
  and rctl.interface_line_context(+)                 = 'ORDER ENTRY'
  and exists (select 1 from  oe_payments oep
  where  xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type=oep.payment_type_code
    AND OEP.HEADER_ID                                  = OL.HEADER_ID)
  group by TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from), OOD.ORGANIZATION_CODE, XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE/
set scan on define on
prompt Creating View Data for Daily Flash Cash Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Daily Flash Sale V','EXADFSV');
--Delete View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','DATE1',660,'Date1','DATE1','','','','XXEIS_RS_ADMIN','DATE','','','Date1');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_AMT',660,'Sale Amt','SALE_AMT','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Amt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_COST',660,'Sale Cost','SALE_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','PAYMENT_TYPE',660,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type');
--Inserting View Components for EIS_XXWC_AR_DAILY_FLASH_SALE_V
--Inserting View Component Joins for EIS_XXWC_AR_DAILY_FLASH_SALE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Cash Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Cash Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Cash Report - for Sales Revenue as % to Plan' );
--Inserting Report - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.r( 660,'Daily Flash Cash Report - for Sales Revenue as % to Plan','','The purpose of this extract is to provide Finance with a daily report of all cash sales by branch.  The report received on 2/28 will report the daily cash sales by branch for 2/27.

This report is to be processed daily and delivered via email to the defined distribution list.  This report will accompany the daily extracts, flash_charge and routing_info.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_DAILY_FLASH_SALE_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','Location','Branch Location','','','','','2','N','','ROW_FIELD','','','Location','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'DATE1','Date','Date1','','','','','1','N','','ROW_FIELD','','','Date','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'GROSS_PROFIT','GP$','Invoice Count','NUMBER','','','','5','Y','','','','','','','(NVL(EXADFSV.SALE_AMT,0)-NVL(EXADFSV.sale_cost,0))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'SALE_AMT','Net Sales Dollars','Sale Amt','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
--Inserting Report Parameters - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rp( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Location','WC Branch Number','BRANCH_LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'Date','Prior days date','DATE1','IN','','select trunc(sysdate-1) from dual','DATE','Y','Y','1','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rcn( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'DATE1','IN',':Date','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rs( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'DATE1','ASC','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rs( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rt( 'Daily Flash Cash Report - for Sales Revenue as % to Plan',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_payment_type(''CASH'');
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Cash Report - for Sales Revenue as % to Plan
--Inserting Report Portals - Daily Flash Cash Report - for Sales Revenue as % to Plan
--Inserting Report Dashboards - Daily Flash Cash Report - for Sales Revenue as % to Plan
--Inserting Report Security - Daily Flash Cash Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','51030',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50638',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50622',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','401','','50941',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','21404',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','20678',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50846',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50845',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50847',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50848',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50849',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50944',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50871',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','','SO004816','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Cash Report - for Sales Revenue as % to Plan','660','','50861',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Daily Flash Charge Report - for Sales Revenue as % to Plan
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_DAILY_FLASH_SALE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
Create or replace View XXEIS.EIS_XXWC_AR_DAILY_FLASH_SALE_V
(DATE1,BRANCH_LOCATION,SALE_AMT,SALE_COST,PAYMENT_TYPE,INVOICE_COUNT) AS 
SELECT TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from) date1,
    OOD.ORGANIZATION_CODE BRANCH_LOCATION,
    SUM(OL.ORDERED_QUANTITY *NVL(OL.UNIT_SELLING_PRICE,0)+NVL(OL.TAX_VALUE,0)) SALE_AMT,
    sum (ol.ordered_quantity*nvl(ol.unit_cost,0)+nvl(ol.tax_value,0))sale_cost,
   -- SUM(oep.payment_amount) cash_sales,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE PAYMENT_TYPE,
    COUNT( DISTINCT rct.trx_number) invoice_count
    ---Primary Keys
  FROM Ra_Customer_Trx Rct,
    ra_customer_trx_lines rctl,
    Oe_Order_Headers Oh,
    Oe_Order_Lines Ol,
    Org_Organization_Definitions Ood   
  WHERE 1                                            = 1
  AND TRUNC(oh.ordered_DATE)                         = TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
  AND TO_CHAR(oh.order_number)                       = TO_CHAR(rct.interface_header_attribute1)
  AND Ood.Organization_Id (+)                        = Ol.Ship_From_Org_Id
  AND TO_CHAR(rctl.interface_line_attribute6)        = TO_CHAR(Ol.Line_id)
  AND RCTL.INVENTORY_ITEM_ID                         = OL.INVENTORY_ITEM_ID
  AND rctl.customer_trx_id                           = rct.customer_trx_id
  AND ol.header_id                                   = oh.header_id
  and rctl.interface_line_context(+)                 = 'ORDER ENTRY'
  and exists (select 1 from  oe_payments oep
  where  xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type=oep.payment_type_code
    AND OEP.HEADER_ID                                  = OL.HEADER_ID)
  group by TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from), OOD.ORGANIZATION_CODE, XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_TYPE/
set scan on define on
prompt Creating View Data for Daily Flash Charge Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Daily Flash Sale V','EXADFSV');
--Delete View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_DAILY_FLASH_SALE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_DAILY_FLASH_SALE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','DATE1',660,'Date1','DATE1','','','','XXEIS_RS_ADMIN','DATE','','','Date1');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_AMT',660,'Sale Amt','SALE_AMT','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Amt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','SALE_COST',660,'Sale Cost','SALE_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Sale Cost');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_DAILY_FLASH_SALE_V','PAYMENT_TYPE',660,'Payment Type','PAYMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type');
--Inserting View Components for EIS_XXWC_AR_DAILY_FLASH_SALE_V
--Inserting View Component Joins for EIS_XXWC_AR_DAILY_FLASH_SALE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Charge Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Charge Report - for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Charge Report - for Sales Revenue as % to Plan' );
--Inserting Report - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.r( 660,'Daily Flash Charge Report - for Sales Revenue as % to Plan','','The purpose of this extract is to provide Finance with a daily report of all cash sales by branch.  The report received on 2/28 will report the daily cash sales by branch for 2/27.

This report is to be processed daily and delivered via email to the defined distribution list.  This report will accompany the daily extracts, flash_charge and routing_info.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_DAILY_FLASH_SALE_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'GROSS_PROFIT','GP$','','NUMBER','','','','5','Y','','DATA_FIELD','','SUM','','4','(nvl(EXADFSV.SALE_AMT,0)-nvl(EXADFSV.sale_cost,0))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','Location','Branch Location','','','','','2','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','','','','3','N','','DATA_FIELD','','COUNT','Invoice Count','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'SALE_AMT','Net Sales Dollars','Sale Amt','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'DATE1','Date','Date1','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_DAILY_FLASH_SALE_V','','');
--Inserting Report Parameters - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rp( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'Location','WC Branch Number','BRANCH_LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'Date','Prior days date','DATE1','IN','','select trunc(sysdate-1) from dual','DATE','Y','Y','1','','Y','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rcn( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'DATE1','IN',':Date','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rs( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rt( 'Daily Flash Charge Report - for Sales Revenue as % to Plan',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.set_payment_type(''CREDIT_CARD'');
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Charge Report - for Sales Revenue as % to Plan
--Inserting Report Portals - Daily Flash Charge Report - for Sales Revenue as % to Plan
--Inserting Report Dashboards - Daily Flash Charge Report - for Sales Revenue as % to Plan
--Inserting Report Security - Daily Flash Charge Report - for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','51030',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50638',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50622',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','401','','50941',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','21404',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','20678',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50846',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50845',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50847',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50848',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50849',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50944',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50871',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','','SO004816','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50856',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50857',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50858',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50859',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50860',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Charge Report - for Sales Revenue as % to Plan','660','','50861',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
--Report Name            : Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
Create or replace View XXEIS.EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
(CURRENT_FISCAL_MONTH,CURRENT_FISCAL_YEAR,DELIVERY_TYPE,BRANCH_LOCATION,INVOICE_COUNT,SALES) AS 
SELECT Gp.Period_Name Current_Fiscal_Month,
    GP.PERIOD_YEAR CURRENT_FISCAL_YEAR,
    xxeis.eis_rs_xxwc_com_util_pkg.get_shipping_mthd3(ol.shipping_method_code) delivery_type,
    mp.organization_code branch_location,
    COUNT( DISTINCT RCT.TRX_NUMBER) INVOICE_COUNT,
    sum(nvl(ol.ordered_quantity,0) * nvl(ol.unit_selling_price,0)+nvl(ol.tax_value,0)) sales
   -- SUM(oep.payment_amount) cash_sales,
 --   xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type payment_type
  FROM ra_customer_trx rct,
    Ra_Customer_Trx_Lines Rctl,
    Oe_Order_Headers Oh,
    Oe_Order_Lines Ol,
    Org_Organization_Definitions Mp,
    Gl_Periods Gp ,
    Gl_Sets_Of_Books Gsob   
  WHERE 1                            = 1
  AND TRUNC(oh.ordered_date)         = TRUNC(Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Date_From)
  AND RCTL.CUSTOMER_TRX_ID           = RCT.CUSTOMER_TRX_ID
  AND rctl.interface_line_attribute6 = TO_CHAR(Ol.Line_id)
  AND Rctl.Inventory_Item_Id         = Ol.Inventory_Item_Id
  AND OL.HEADER_ID                   = OH.HEADER_ID
  AND TO_CHAR(Oh.Order_Number)       = Rct.Interface_Header_Attribute1
  AND Mp.Organization_Id (+)         = Ol.Ship_From_Org_Id
  AND Gsob.Set_Of_Books_Id           = Mp.Set_Of_Books_Id
  AND Gsob.Period_Set_Name           = Gp.Period_Set_Name
  and trunc(oh.ordered_date) between gp.start_date and gp.end_date
  and exists(select 1 from  oe_payments oep
  where oep.header_id                  = ol.header_id
  AND oep.payment_type_code         IN ('CASH','CREDIT_CARD'))
  AND rctl.INTERFACE_LINE_CONTEXT(+) = 'ORDER ENTRY'  
  group by GP.PERIOD_NAME, GP.PERIOD_YEAR, XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_SHIPPING_MTHD3(OL.SHIPPING_METHOD_CODE), MP.ORGANIZATION_CODE/
set scan on define on
prompt Creating View Data for Daily Flash Delivery Report  for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Flsh Sale By Del V','EXAFSBDV');
--Delete View Columns for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_FLSH_SALE_BY_DEL_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','CURRENT_FISCAL_MONTH',660,'Current Fiscal Month','CURRENT_FISCAL_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Current Fiscal Month');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','CURRENT_FISCAL_YEAR',660,'Current Fiscal Year','CURRENT_FISCAL_YEAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Current Fiscal Year');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','DELIVERY_TYPE',660,'Delivery Type','DELIVERY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Delivery Type');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','SALES',660,'Sales','SALES','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Sales');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count');
--Inserting View Components for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
--Inserting View Component Joins for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Delivery Report  for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Delivery Report  for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan' );
--Inserting Report - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.r( 660,'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','The purpose of this extract is to provide Finance with a daily report of all sales by branch and aggregated by delivery type.  The report received on 2/28 will report the daily sales by branch and delivery type for 2/27.

This report is to be processed daily and delivered via email to the defined distribution list.  This report will accompany the daily extracts, flash_charge and flash_cash.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,EXCEL,','N');
--Inserting Report Columns - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','Location','Branch Location','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'DELIVERY_TYPE','Delivery Type','Delivery Type','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'SALES','Net Sales Dollars','Sales','','','','','6','N','','','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'CURRENT_FISCAL_MONTH','Fiscal Month','Current Fiscal Month','','','','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'CURRENT_FISCAL_YEAR','Fiscal Year','Current Fiscal Year','','','','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
--Inserting Report Parameters - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'Location','Location','BRANCH_LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'Date','Prior days date','','IN','','select trunc(sysdate-1) from dual','DATE','Y','Y','1','','N','SQL','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rcn( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','IN',':Location','','','Y','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'','','','','--and EXAFSBDV.delivery_type is not null','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Inserting Report Triggers - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rt( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan',660,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Date);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Inserting Report Portals - Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Inserting Report Dashboards - Daily Flash Delivery Report  for Sales Revenue as % to Plan
--Inserting Report Security - Daily Flash Delivery Report  for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','51030',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50638',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50622',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','401','','50941',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','21404',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','20678',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50846',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50845',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50847',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50848',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50849',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50944',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','660','','50871',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','20005','','50880',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','LC053655','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','10010432','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','RB054040','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','RV003897','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','SS084202','',660,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report  for Sales Revenue as % to Plan','','SO004816','',660,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
