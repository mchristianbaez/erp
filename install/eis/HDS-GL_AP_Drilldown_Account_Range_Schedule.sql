--Report Name            : HDS-GL_AP_Drilldown_Account_Range_Schedule
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXHDS_GL_SL_180_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.v( 'EIS_XXHDS_GL_SL_180_V',101,'','','','','MT063505','XXEIS','Eis Xxhds Gl Sl 180 V','EXGS_V','','','VIEW','US','','','');
--Delete Object Columns for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXHDS_GL_SL_180_V',101,FALSE);
--Inserting Object Columns for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#ACCOUNT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT#DESCR','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account Descr','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT5',101,'Segment5','SEGMENT5','','','','MT063505','VARCHAR2','','','Segment5','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT4',101,'Segment4','SEGMENT4','','','','MT063505','VARCHAR2','','','Segment4','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT3',101,'Segment3','SEGMENT3','','','','MT063505','VARCHAR2','','','Segment3','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT2',101,'Segment2','SEGMENT2','','','','MT063505','VARCHAR2','','','Segment2','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT1',101,'Segment1','SEGMENT1','','','','MT063505','VARCHAR2','','','Segment1','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','MT063505','VARCHAR2','','','Batch Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','NAME',101,'Name','NAME','','','','MT063505','VARCHAR2','','','Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','TYPE',101,'Type','TYPE','','','','MT063505','VARCHAR2','','','Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','MT063505','VARCHAR2','','','Period Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','CURRENCY_CODE',101,'Currency Code','CURRENCY_CODE','','','','MT063505','VARCHAR2','','','Currency Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','TRANSACTION_NUM',101,'Transaction Num','TRANSACTION_NUM','','','','MT063505','VARCHAR2','','','Transaction Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','~T~D~2','','MT063505','NUMBER','','','Sla Dist Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','~T~D~2','','MT063505','NUMBER','','','Sla Dist Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','~T~D~2','','MT063505','NUMBER','','','Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','~T~D~2','','MT063505','NUMBER','','','Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','MT063505','VARCHAR2','','','Customer Or Vendor','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','MT063505','NUMBER','','','Gl Sl Link Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','MT063505','VARCHAR2','','','Line Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ENTRY',101,'Entry','ENTRY','','','','MT063505','VARCHAR2','','','Entry','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','MT063505','DATE','','','Acc Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','MT063505','NUMBER','','','Je Line Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','MT063505','VARCHAR2','','','Je Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SOURCE',101,'Source','SOURCE','','','','MT063505','VARCHAR2','','','Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','~T~D~2','','MT063505','NUMBER','','','Sla Dist Accounted Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','~T~D~2','','MT063505','NUMBER','','','Sla Line Entered Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','~T~D~2','','MT063505','NUMBER','','','Sla Line Accounted Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','~T~D~2','','MT063505','NUMBER','','','Sla Dist Entered Net','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ASSOCIATE_NUM',101,'Associate Num','ASSOCIATE_NUM','','','','MT063505','VARCHAR2','','','Associate Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#FUTURE_USE',101,'Gcc#50368#Future Use','GCC#50368#FUTURE_USE','','','','MT063505','VARCHAR2','','','Gcc#50368#Future Use','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DIVISION#DESCR',101,'Gcc#50368#Division#Descr','GCC#50368#DIVISION#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Division#Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DIVISION',101,'Gcc#50368#Division','GCC#50368#DIVISION','','','','MT063505','VARCHAR2','','','Gcc#50368#Division','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DEPARTMENT#DESCR',101,'Gcc#50368#Department#Descr','GCC#50368#DEPARTMENT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Department#Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DEPARTMENT',101,'Gcc#50368#Department','GCC#50368#DEPARTMENT','','','','MT063505','VARCHAR2','','','Gcc#50368#Department','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#ACCOUNT#DESCR',101,'Gcc#50368#Account#Descr','GCC#50368#ACCOUNT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Account#Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#ACCOUNT',101,'Gcc#50368#Account','GCC#50368#ACCOUNT','','','','MT063505','VARCHAR2','','','Gcc#50368#Account','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PROJECT_CODE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE#DESCR','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code Descr','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PROJECT_CODE',101,'Accounting Flexfield (KFF): Segment ''Project Code'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PROJECT_CODE','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT5','GCC#Project Code','50328','1014551','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PRODUCT#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT#DESCR','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product Descr','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PRODUCT',101,'Accounting Flexfield (KFF): Segment ''Product'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#PRODUCT','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT1','GCC#Product','50328','1014547','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#LOCATION#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION#DESCR','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location Descr','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#LOCATION',101,'Accounting Flexfield (KFF): Segment ''Location'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#LOCATION','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT2','GCC#Location','50328','1014548','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FUTURE_USE_2#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2#DESCR','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2 Descr','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FUTURE_USE_2',101,'Accounting Flexfield (KFF): Segment ''Future Use 2'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE_2','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT7','GCC#Future Use 2','50328','1014948','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#COST_CENTER#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER#DESCR','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center Descr','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#COST_CENTER',101,'Accounting Flexfield (KFF): Segment ''Cost Center'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#COST_CENTER','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT3','GCC#Cost Center','50328','1014549','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#ACCOUNT',101,'Accounting Flexfield (KFF): Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#ACCOUNT','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT4','GCC#Account','50328','1014550','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#BRANCH',101,'Gcc#Branch','GCC#BRANCH','','','','MT063505','VARCHAR2','','','Gcc#Branch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','MT063505','NUMBER','','','Code Combination Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','EFFECTIVE_PERIOD_NUM',101,'Effective Period Num','EFFECTIVE_PERIOD_NUM','','','','MT063505','NUMBER','','','Effective Period Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','MT063505','VARCHAR2','','','Gl Account String','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','~T~D~2','','MT063505','NUMBER','','','Sla Dist Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','~T~D~2','','MT063505','NUMBER','','','Sla Dist Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','~T~D~2','','MT063505','NUMBER','','','Sla Line Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','~T~D~2','','MT063505','NUMBER','','','Sla Line Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','~T~D~2','','MT063505','NUMBER','','','Sla Line Accounted Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','~T~D~2','','MT063505','NUMBER','','','Sla Line Accounted Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','~T~D~2','','MT063505','NUMBER','','','Entered Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','~T~D~2','','MT063505','NUMBER','','','Entered Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','H_SEQ_ID',101,'H Seq Id','H_SEQ_ID','','','','MT063505','NUMBER','','','H Seq Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEQ_NUM',101,'Seq Num','SEQ_NUM','','','','MT063505','NUMBER','','','Seq Num','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEQ_ID',101,'Seq Id','SEQ_ID','','','','MT063505','NUMBER','','','Seq Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','~T~D~2','','MT063505','NUMBER','','','Line Ent Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','~T~D~2','','MT063505','NUMBER','','','Line Ent Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','~T~D~2','','MT063505','NUMBER','','','Line Acctd Cr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','~T~D~2','','MT063505','NUMBER','','','Line Acctd Dr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LLINE',101,'Lline','LLINE','','','','MT063505','NUMBER','','','Lline','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','HNUMBER',101,'Hnumber','HNUMBER','','','','MT063505','NUMBER','','','Hnumber','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LSEQUENCE',101,'Lsequence','LSEQUENCE','','','','MT063505','VARCHAR2','','','Lsequence','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','MT063505','NUMBER','','','Je Header Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#SUBACCOUNT#DESCR',101,'Gcc#50368#Subaccount#Descr','GCC#50368#SUBACCOUNT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Subaccount#Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#SUBACCOUNT',101,'Gcc#50368#Subaccount','GCC#50368#SUBACCOUNT','','','','MT063505','VARCHAR2','','','Gcc#50368#Subaccount','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#PRODUCT#DESCR',101,'Gcc#50368#Product#Descr','GCC#50368#PRODUCT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Product#Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#PRODUCT',101,'Gcc#50368#Product','GCC#50368#PRODUCT','','','','MT063505','VARCHAR2','','','Gcc#50368#Product','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#FUTURE_USE#DESCR',101,'Gcc#50368#Future Use#Descr','GCC#50368#FUTURE_USE#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Future Use#Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','BATCH',101,'Batch','BATCH','','','','MT063505','NUMBER','','','Batch','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FURTURE_USE',101,'Gcc#50328#Furture Use','GCC#50328#FURTURE_USE','','','','MT063505','VARCHAR2','','','Gcc#50328#Furture Use','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FURTURE_USE#DESCR',101,'Gcc#50328#Furture Use#Descr','GCC#50328#FURTURE_USE#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Furture Use#Descr','','','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FUTURE_USE',101,'Accounting Flexfield (KFF): Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use','50328','1014552','','US','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FUTURE_USE#DESCR',101,'Accounting Flexfield (KFF) : Segment ''Future Use'' under chart of accounts ''XXCUS Accounting Flexfield''','GCC#50328#FUTURE_USE#DESCR','','','','MT063505','VARCHAR2','GL_CODE_COMBINATIONS','SEGMENT6','GCC#Future Use Descr','50328','1014552','','US','');
--Inserting Object Components for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_JE_LINES',101,'GL_JE_LINES','GJL','GJL','MT063505','MT063505','-1','Journal Entry Lines','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_JE_HEADERS',101,'GL_JE_HEADERS','GJH','GJH','MT063505','MT063505','-1','Journal Entry Headers','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_CODE_COMBINATIONS',101,'GL_CODE_COMBINATIONS','GCC','GCC','MT063505','MT063505','-1','Account Combinations','Y','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_LEDGERS',101,'GL_LEDGERS','GL','GL','MT063505','MT063505','-1','Ledger Definition','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES',101,'GL_PERIOD_STATUSES','GPS','GPS','MT063505','MT063505','-1','Calendar Period Statuses','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_JE_LINES','GJL',101,'EXGS_V.JE_HEADER_ID','=','GJL.JE_HEADER_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_JE_LINES','GJL',101,'EXGS_V.JE_LINE_NUM','=','GJL.JE_LINE_NUM(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_JE_HEADERS','GJH',101,'EXGS_V.JE_HEADER_ID','=','GJH.JE_HEADER_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_CODE_COMBINATIONS','GCC',101,'EXGS_V.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','1','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_LEDGERS','GL',101,'EXGS_V.NAME','=','GL.NAME(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES','GPS',101,'EXGS_V.GPS_APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES','GPS',101,'EXGS_V.GPS_SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES','GPS',101,'EXGS_V.GPS_PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','Y','MT063505');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS-GL_AP_Drilldown_Account_Range_Schedule
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS-GL_AP_Drilldown_Account_Range_Schedule
xxeis.eis_rsc_ins.lov( 101,'SELECT distinct user_je_source_name FROM gl_je_sources','','EIS_GL_JE_SOURCES_LOV','LOV of all Available Journal Sources','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type
				FROM
					fnd_flex_value_sets ffvs ,
					fnd_flex_values ffv,
					fnd_flex_values_tl ffvtl
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'')
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
				AND ffv.enabled_flag = upper(''Y'')
				AND ffv.summary_flag in (''Y'',''N'')
				/* AND ffv.summary_flag <> upper(''Y'') */
				AND ffvtl.LANGUAGE = USERENV(''LANG'')
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT1'' , ffv.flex_value)=''TRUE''
				order by ffv.flex_value	','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type
				FROM
					fnd_flex_value_sets ffvs ,
					fnd_flex_values ffv,
					fnd_flex_values_tl ffvtl
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'')
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID
				AND ffv.enabled_flag = upper(''Y'')
				AND ffv.summary_flag in (''Y'',''N'')
				/* AND ffv.summary_flag <> upper(''Y'') */
				AND ffvtl.LANGUAGE = USERENV(''LANG'')
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT2'' , ffv.flex_value)=''TRUE''
				order by ffv.flex_value	','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_COSTCENTER'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT3'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_COSTCENTER','XXCUS_GL_COSTCENTER','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT4'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PROJECT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				/* AND ffv.summary_flag <> upper(''Y'') */ 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT5'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_PROJECT','XXCUS_GL_PROJECT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','select name,period_set_name,currency_code,chart_of_accounts_id from gl_sets_of_books','','LEDGER NAME','set of books ledger name from gl_sets_of_books','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 101,'select distinct currency_code from gl_je_headers','','XXHDS GL Currency Code LOV','','MT063505',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS-GL_AP_Drilldown_Account_Range_Schedule
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS-GL_AP_Drilldown_Account_Range_Schedule
xxeis.eis_rsc_utility.delete_report_rows( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101 );
--Inserting Report - HDS-GL_AP_Drilldown_Account_Range_Schedule
xxeis.eis_rsc_ins.r( 101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','','This Report is copied from HDS-GL_AP_Drilldown_Account_Range.Includes GL activity and details from subledger for AP data generated in the subledger.
Changes requested via ESMS 286481 and 293265 to add Ledger as a parameter and correct duplicate data updated 5/20/2016.','','','','PS066716','EIS_XXHDS_GL_SL_180_V','Y','','','PS066716','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','N','','US','','HDS-GL_AP_Drilldown_Account_Range','','','','','','','','','','','','','','');
--Inserting Report Columns - HDS-GL_AP_Drilldown_Account_Range_Schedule
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'JE_LINE_NUM','Je Line Num','Je Line Num','','~~~','default','','8','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'ACCOUNTED_CR','Total Accounted Debit','Accounted Cr','','~T~D~2','default','','23','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'ACCOUNTED_DR','Total Accounted Credit','Accounted Dr','','~T~D~2','default','','24','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'ACC_DATE','Acc Date','Acc Date','','','default','','5','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','6','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'CURRENCY_CODE','Currency Code','Currency Code','','','default','','2','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'CUSTOMER_OR_VENDOR','Vendor Name','Customer Or Vendor','','','default','','17','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'ENTRY','Description','Entry','','','default','','7','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'GCC#50328#ACCOUNT#DESCR','Account Description','Accounting Flexfield (KFF) : Segment ''Account'' under chart of accounts ''XXCUS Accounting Flexfield''','','','default','','14','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'GL_SL_LINK_ID','Subledger Link ID','Gl Sl Link Id','','~~~','default','','18','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'JE_CATEGORY','Je Category','Je Category','','','default','','3','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'LINE_DESCR','Line Descr','Line Descr','','','default','','9','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'PERIOD_NAME','Period Name','Period Name','','','default','','4','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SOURCE','Source','Source','','','default','','1','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'TRANSACTION_NUM','Invoice Number','Transaction Num','','','default','','16','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'TYPE','Line Type','Type','','','default','','19','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SLA_DIST_ACCOUNTED_CR','Subledger Line Credit','Sla Dist Accounted Cr','','~T~D~2','default','','21','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SLA_DIST_ACCOUNTED_DR','Subledger Line Debit','Sla Dist Accounted Dr','','~T~D~2','default','','20','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'ACCOUNTED_NET','Subledger_Net','Sla Dist Accounted Dr','NUMBER','~T~D~2','default','','22','Y','Y','','','','','','SLA_DIST_ACCOUNTED_NET*-1','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SEGMENT4','Account','Segment4','','','default','','13','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SEGMENT1','Product','Segment1','','','default','','10','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SEGMENT2','Location','Segment2','','','default','','11','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SEGMENT3','Cost Center','Segment3','','','default','','12','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SEGMENT5','Project Code','Segment5','','','default','','15','N','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'NAME','Name','Name','','','default','','25','','Y','','','','','','','PS066716','N','N','','EIS_XXHDS_GL_SL_180_V','','','','US','','');
--Inserting Report Parameters - HDS-GL_AP_Drilldown_Account_Range_Schedule
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Source','Source','SOURCE','IN','EIS_GL_JE_SOURCES_LOV','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Product','Product','SEGMENT1','IN','XXCUS_GL_PRODUCT','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Location','Location','SEGMENT2','IN','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Cost Center','Cost Center','SEGMENT3','IN','XXCUS_GL_COSTCENTER','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Account From','Account From','SEGMENT4','>=','XXCUS_GL_ACCOUNT','','VARCHAR2','N','N','7','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Currency Code','Currency Code','CURRENCY_CODE','IN','XXHDS GL Currency Code LOV','','VARCHAR2','N','Y','10','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Project Code','Project Code','SEGMENT5','IN','XXCUS_GL_PROJECT','','VARCHAR2','N','Y','9','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Account To','Account To','SEGMENT4','<=','XXCUS_GL_ACCOUNT','','VARCHAR2','N','N','8','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Period Date','Period Date','PERIOD_NAME','IN','','','DATE','Y','Y','2','Y','N','CURRENT_DATE','PS066716','Y','N','','Start Date','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'Ledger Name','Ledger Name','NAME','IN','LEDGER NAME','''HD Supply USD''','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','PS066716','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
--Inserting Dependent Parameters - HDS-GL_AP_Drilldown_Account_Range_Schedule
--Inserting Report Conditions - HDS-GL_AP_Drilldown_Account_Range_Schedule
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'CURRENCY_CODE IN :Currency Code ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','CURRENCY_CODE','','Currency Code','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','CURRENCY_CODE IN :Currency Code ');
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'NAME IN :Ledger Name ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','NAME','','Ledger Name','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','NAME IN :Ledger Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SEGMENT1 IN :Product ','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT1','','Product','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','SEGMENT1 IN :Product ');
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SEGMENT2 IN :Location ','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT2','','Location','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','SEGMENT2 IN :Location ');
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SEGMENT3 IN :Cost Center ','SIMPLE','','','Y','','6');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT3','','Cost Center','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','SEGMENT3 IN :Cost Center ');
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SEGMENT5 IN :Project Code ','SIMPLE','','','Y','','7');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT5','','Project Code','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','SEGMENT5 IN :Project Code ');
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'SOURCE IN :Source ','SIMPLE','','','Y','','8');
xxeis.eis_rsc_ins.rcnd( '','','SOURCE','','Source','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','SOURCE IN :Source ');
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'EXGS_V.SEGMENT4 >= Account From','SIMPLE','','','Y','','9');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT4','','Account From','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','EXGS_V.SEGMENT4 >= Account From');
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'EXGS_V.SEGMENT4 >= Account To','SIMPLE','','','Y','','10');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT4','','Account To','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','EXGS_V.SEGMENT4 >= Account To');
xxeis.eis_rsc_ins.rcnh( 'HDS-GL_AP_Drilldown_Account_Range_Schedule',101,'FreeText','FREE_TEXT','','','Y','','11');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','Y','','','','and EXGS_V.PERIOD_NAME in (SELECT gp.period_name FROM gl_periods gp
WHERE gp.period_set_name=''4-4-QTR''
AND gp.year_start_date   IN
  (SELECT year_start_date
  FROM gl_periods gp1
  WHERE gp1.period_set_name=''4-4-QTR''
  AND trunc(to_date(:Period Date)) BETWEEN gp1.start_date AND gp1.end_date
  )
  and gp.start_date <=trunc(to_date(:Period Date)))
and (EXGS_V.SEGMENT4 in (''184100'',''809500'') or (EXGS_V.SEGMENT4 between ''201000'' and ''201999'') or(EXGS_V.SEGMENT4 between ''252000'' and ''252999'')
or(EXGS_V.SEGMENT4 between ''261000'' and ''263999'') or(EXGS_V.SEGMENT4 between ''801000'' and ''801999'') 
or(EXGS_V.SEGMENT4 between ''805000'' and ''805999'') 
or(EXGS_V.SEGMENT4 between ''806000'' and ''806999'') )','1',101,'HDS-GL_AP_Drilldown_Account_Range_Schedule','FreeText');
--Inserting Report Sorts - HDS-GL_AP_Drilldown_Account_Range_Schedule
--Inserting Report Triggers - HDS-GL_AP_Drilldown_Account_Range_Schedule
--inserting report templates - HDS-GL_AP_Drilldown_Account_Range_Schedule
--Inserting Report Portals - HDS-GL_AP_Drilldown_Account_Range_Schedule
--inserting report dashboards - HDS-GL_AP_Drilldown_Account_Range_Schedule
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS-GL_AP_Drilldown_Account_Range_Schedule','101','EIS_XXHDS_GL_SL_180_V','EIS_XXHDS_GL_SL_180_V','N','');
--inserting report security - HDS-GL_AP_Drilldown_Account_Range_Schedule
--Inserting Report Pivots - HDS-GL_AP_Drilldown_Account_Range_Schedule
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- HDS-GL_AP_Drilldown_Account_Range_Schedule
xxeis.eis_rsc_ins.rv( 'HDS-GL_AP_Drilldown_Account_Range_Schedule','','HDS-GL_AP_Drilldown_Account_Range_Schedule','PS066716','13-MAR-2018');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END; 
/
