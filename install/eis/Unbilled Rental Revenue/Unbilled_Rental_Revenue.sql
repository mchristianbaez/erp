--Report Name            : Unbilled Rental Revenue
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_INV_UNINVODR_DTL_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_INV_UNINVODR_DTL_V
Create or replace View XXEIS.EIS_XXWC_INV_UNINVODR_DTL_V
(PERIOD_NAME,LOCATION,SEGMENT_NUMBER,CUSTOMER_NUMBER,CUSTOMER_NAME,PART_NUMBER,DESCRIPTION,LAST_INVOICE_NUM,LAST_ORDER_NUM,SALESREP_ID,ORDER_TYPE,ORDERED_QUANTITY,NEW_SELLING_PRICE,LAST_BILLED_DATE,TOTAL_DAYS,UNBILLED_RENTAL_DAYS,UNBILLED_RENTAL_AMOUNT) AS 
SELECT gps.period_name ,
    ship_from_org.organization_code location,
    gcc.segment2 segment_number,
    CUST_ACCT.ACCOUNT_NUMBER CUSTOMER_NUMBER,
    NVL(CUST_ACCT.account_name,party.party_name) customer_name,
    msi.segment1 part_number,
    Msi.Description Description,
    Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Last_Invoice_Num(Ol.Inventory_Item_Id,Ol.Sold_To_Org_Id, ol.header_id) Last_Invoice_Num,
    oh.order_number LAST_ORDER_NUM,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LAST_ORDER_NUM(OL.INVENTORY_ITEM_ID,OL.SOLD_TO_ORG_ID) LAST_ORDER_NUM,
    JSR.name SALESREP_ID,
    OH.ATTRIBUTE1 order_type,
    ol1.SHIPPED_QUANTITY ORDERED_QUANTITY,
    ol1.UNIT_LIST_PRICE new_selling_price,
    xxeis.eis_rs_xxwc_com_util_pkg.get_last_billed_date(ol.inventory_item_id,ol.sold_to_org_id,ol.header_id) last_billed_date,
    ((TRUNC(GPS.END_DATE)-TRUNC(NVL(to_date(OL1.attribute12, 'YYYY/MM/DD HH24:MI:SS'),OL1.ACTUAL_SHIPMENT_DATE)))+1) TOTAL_DAYS,
    xxeis.eis_rs_xxwc_com_util_pkg.get_rental_long_bill_days(NVL(to_date(OL1.attribute12, 'YYYY/MM/DD HH24:MI:SS'),OL1.ACTUAL_SHIPMENT_DATE),GPS.END_DATE) UNBILLED_RENTAL_DAYS,
    CASE
      WHEN OL.ATTRIBUTE4 IS NULL
      THEN ((ol1.SHIPPED_QUANTITY) * xxeis.eis_rs_xxwc_com_util_pkg.get_rental_long_bill_days(NVL(to_date(OL1.attribute12, 'YYYY/MM/DD HH24:MI:SS'),OL1.ACTUAL_SHIPMENT_DATE),GPS.END_DATE)*(ol1.UNIT_LIST_PRICE/28) )
      ELSE (ol1.SHIPPED_QUANTITY   * (OL.ATTRIBUTE4 /28)*xxeis.eis_rs_xxwc_com_util_pkg.get_rental_long_bill_days(NVL(to_date(OL1.attribute12, 'YYYY/MM/DD HH24:MI:SS'),OL1.ACTUAL_SHIPMENT_DATE),GPS.END_DATE) )
    END unbilled_rental_amount
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org,
    hz_parties party,
    hz_cust_accounts cust_acct,
    oe_order_headers oh,
    OE_ORDER_LINES OL,
    OE_TRANSACTION_TYPES_VL OTL,
    oe_order_lines ol1,
    hr_operating_units hou,
    mtl_system_items_kfv msi,
    gl_code_combinations_kfv gcc,
    GL_PERIOD_STATUSES GPS,
    --    OE_ORDER_TYPES_V OT,
    /*
    QP_LIST_HEADERS PH,*/
    Ra_Salesreps JSR
    -- QP_LIST_LINES PL
    --          QP_LIST_LINES_V QLLV
  WHERE 1                           = 1
  AND ol.sold_to_org_id             = cust_acct.cust_account_id
  AND cust_acct.party_id            = party.party_id
  AND ol.ship_from_org_id           = ship_from_org.organization_id
  AND OL.HEADER_ID                  = OH.HEADER_ID
  AND OL.LINE_TYPE_ID               = OTL.TRANSACTION_TYPE_ID
  AND OL.ORG_ID                     = OTL.ORG_ID
  AND OL.LINK_TO_LINE_ID            = OL1.LINE_ID
  AND ol.flow_status_code           ='AWAITING_RETURN'
  AND OTL.NAME                      ='RETURN WITH RECEIPT'
  AND OH.ATTRIBUTE1                 ='Long Term'
  AND oh.flow_status_code          <>'CLOSED'
  AND NVL(OL1.ATTRIBUTE2,'NOT IN') <> '28 Day ReRental Billing'
    --  and OH.ORDER_TYPE_ID = OT.ORDER_TYPE_ID
    --  and ot.name ='WC LONG TERM RENTAL'
  AND ol.inventory_item_id                    = msi.inventory_item_id
  AND msi.organization_id                     = ol.ship_from_org_id
  AND oh.org_id                               = hou.organization_id
  AND gcc.code_combination_id                 = ship_from_org.cost_of_sales_account
  AND gps.application_id                      =101
  AND GPS.SET_OF_BOOKS_ID                     =HOU.SET_OF_BOOKS_ID
  AND gps.end_date                            >TRUNC(NVL(to_date(OL1.attribute12, 'YYYY/MM/DD HH24:MI:SS'),OL1.ACTUAL_SHIPMENT_DATE))
  AND (TRUNC(GPS.END_DATE)-TRUNC(NVL(to_date(OL1.attribute12, 'YYYY/MM/DD HH24:MI:SS'),OL1.ACTUAL_SHIPMENT_DATE))) > 28
  AND JSR.SALESREP_ID (+)                     = Oh.SALESREP_ID
  AND JSR.ORG_ID (+)                          = Oh.ORG_ID
  UNION ALL
  SELECT gps.period_name ,
    ship_from_org.organization_code location,
    gcc.segment2 segment_number,
    CUST_ACCT.ACCOUNT_NUMBER CUSTOMER_NUMBER,
    NVL(CUST_ACCT.account_name,party.party_name) customer_name,
    msi.segment1 part_number,
    Msi.Description Description,
    Xxeis.Eis_Rs_Xxwc_Com_Util_Pkg.Get_Last_Invoice_Num(Ol.Inventory_Item_Id,Ol.Sold_To_Org_Id, ol.header_id) Last_Invoice_Num,
    oh.order_number LAST_ORDER_NUM,
    --XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LAST_ORDER_NUM(OL.INVENTORY_ITEM_ID,OL.SOLD_TO_ORG_ID) LAST_ORDER_NUM,
    JSR.name SALESREP_ID,
    OH.ATTRIBUTE1 ORDER_TYPE,
    -- SUM(OL.ORDERED_QUANTITY) ORDERED_QUANTITY,
    ol1.SHIPPED_QUANTITY ORDERED_QUANTITY,
    ol1.UNIT_LIST_PRICE new_selling_price,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_new_selling_price(ol.inventory_item_id,ol.ship_from_org_id,ol.sold_to_org_id) new_selling_price,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_LAST_BILLED_DATE(OL.INVENTORY_ITEM_ID,OL.SOLD_TO_ORG_ID,ol.header_id) LAST_BILLED_DATE,
    ((TRUNC(GPS.END_DATE)-TRUNC(OL1.ACTUAL_SHIPMENT_DATE))+1) TOTAL_DAYS,
    (XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_RENTAL_BILL_DAYS(OL1.ACTUAL_SHIPMENT_DATE,GPS.START_DATE,GPS.END_DATE)) UNBILLED_RENTAL_DAYS,
    (ol1.SHIPPED_QUANTITY * XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ACCRUED_AMT(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_RENTAL_BILL_DAYS(OL1.ACTUAL_SHIPMENT_DATE,GPS.START_DATE,GPS.END_DATE),OL1.ATTRIBUTE4,OL1.UNIT_LIST_PRICE)) UNBILLED_RENTAL_AMOUNT
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org,
    hz_parties party,
    hz_cust_accounts cust_acct,
    oe_order_headers oh,
    OE_ORDER_LINES OL,
    OE_TRANSACTION_TYPES_VL OTL,
    oe_order_lines ol1,
    hr_operating_units hou,
    mtl_system_items_kfv msi,
    gl_code_combinations_kfv gcc,
    GL_PERIOD_STATUSES GPS,
    --    OE_ORDER_TYPES_V OT,
    -- QP_LIST_HEADERS PH,
    Ra_Salesreps JSR
    --   QP_LIST_LINES PL
  WHERE 1                  = 1
  AND ol.sold_to_org_id    = cust_acct.cust_account_id
  AND cust_acct.party_id   = party.party_id
  AND ol.ship_from_org_id  = ship_from_org.organization_id
  AND OL.HEADER_ID         = OH.HEADER_ID
  AND OL.LINE_TYPE_ID      = OTL.TRANSACTION_TYPE_ID
  AND OL.ORG_ID            = OTL.ORG_ID
  AND OL.LINK_TO_LINE_ID   = OL1.LINE_ID
  AND ol.flow_status_code  ='AWAITING_RETURN'
  AND OTL.NAME             ='RETURN WITH RECEIPT'
  AND OH.ATTRIBUTE1        ='Short Term'
  AND oh.flow_status_code <>'CLOSED'
    --  and OH.ORDER_TYPE_ID = OT.ORDER_TYPE_ID
    --  and ot.name ='WC SHORT TERM RENTAL'
  AND ol.inventory_item_id    = msi.inventory_item_id(+)
  AND msi.organization_id     = ol.ship_from_org_id
  AND oh.org_id               = hou.organization_id
  AND gcc.code_combination_id = ship_from_org.cost_of_sales_account
  AND gps.application_id      =101
  AND GPS.SET_OF_BOOKS_ID     =HOU.SET_OF_BOOKS_ID
  AND GPS.END_DATE            >TRUNC(OL1.ACTUAL_SHIPMENT_DATE)
  AND JSR.SALESREP_ID (+)     = OH.SALESREP_ID
  AND JSR.ORG_ID (+)          = OH.ORG_ID
/
set scan on define on
prompt Creating View Data for Unbilled Rental Revenue
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_INV_UNINVODR_DTL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_INV_UNINVODR_DTL_V',401,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Om Uninv Order Detail V','EXIUDV','','');
--Delete View Columns for EIS_XXWC_INV_UNINVODR_DTL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_INV_UNINVODR_DTL_V',401,FALSE);
--Inserting View Columns for EIS_XXWC_INV_UNINVODR_DTL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','CUSTOMER_NAME',401,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','LAST_ORDER_NUM',401,'Last Order Num','LAST_ORDER_NUM','','','','XXEIS_RS_ADMIN','NUMBER','','','Last Order Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','LAST_INVOICE_NUM',401,'Last Invoice Num','LAST_INVOICE_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Last Invoice Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','LAST_BILLED_DATE',401,'Last Billed Date','LAST_BILLED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Last Billed Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','ORDERED_QUANTITY',401,'Ordered Quantity','ORDERED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Ordered Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','CUSTOMER_NUMBER',401,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','DESCRIPTION',401,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','NEW_SELLING_PRICE',401,'New Selling Price','NEW_SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','New Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','PART_NUMBER',401,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','SEGMENT_NUMBER',401,'Segment Number','SEGMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','LOCATION',401,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','TOTAL_DAYS',401,'Total Days','TOTAL_DAYS','','','','XXEIS_RS_ADMIN','NUMBER','','','Total Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','UNBILLED_RENTAL_AMOUNT',401,'Unbilled Rental Amount','UNBILLED_RENTAL_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','','','Unbilled Rental Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','UNBILLED_RENTAL_DAYS',401,'Unbilled Rental Days','UNBILLED_RENTAL_DAYS','','','','XXEIS_RS_ADMIN','NUMBER','','','Unbilled Rental Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','SALESREP_ID',401,'Salesrep Id','SALESREP_ID','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','PERIOD_NAME',401,'Period Name','PERIOD_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Period Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_INV_UNINVODR_DTL_V','ORDER_TYPE',401,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','');
--Inserting View Components for EIS_XXWC_INV_UNINVODR_DTL_V
--Inserting View Component Joins for EIS_XXWC_INV_UNINVODR_DTL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Unbilled Rental Revenue
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Unbilled Rental Revenue
xxeis.eis_rs_ins.lov( 401,'select  distinct per.period_name , per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     led.accounted_period_type = per.period_type
order by per.period_year desc,per.period_num asc','null','EIS INV PERIOD NAME','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','INV CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'select  cust_acct.account_number Customer_Number,cust_acct.account_name customer_name,party.party_name
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT
 where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID','','INV CUSTOMER NUMBER','This gives the Customer Number','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 401,'SELECT organization_code code, organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','EIS XXWC INV ORGS','XXWC list of inventort orgs','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Unbilled Rental Revenue
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Unbilled Rental Revenue
xxeis.eis_rs_utility.delete_report_rows( 'Unbilled Rental Revenue' );
--Inserting Report - Unbilled Rental Revenue
xxeis.eis_rs_ins.r( 401,'Unbilled Rental Revenue','','Provide a total listing of all unbilled rental revenue by branch (Customer Number, Part Number, Description, Last Invoice Number, Last Order Number, Sales Rep ID, Quantity, New Selling Price, Last Billing Date, Unbilled Days, Unbilled Revenue)','','','','XXEIS_RS_ADMIN','EIS_XXWC_INV_UNINVODR_DTL_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Unbilled Rental Revenue
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'LAST_BILLED_DATE','Last Billing Date','Last Billed Date','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'LAST_INVOICE_NUM','Last Invoice Number','Last Invoice Num','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'LAST_ORDER_NUM','Order Number','Last Order Num','','~~~','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'ORDERED_QUANTITY','Quantity','Ordered Quantity','','~~~','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'DESCRIPTION','Description','Description','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'NEW_SELLING_PRICE','New Selling Price','New Selling Price','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'PART_NUMBER','Part Number','Part Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'SEGMENT_NUMBER','Segment Number','Segment Number','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'LOCATION','Location','Location','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'UNBILLED_RENTAL_AMOUNT','Unbilled Rental Amount','Unbilled Rental Amount','','~T~D~2','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'UNBILLED_RENTAL_DAYS','Unbilled Rental Days','Unbilled Rental Days','','~~~','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'TOTAL_DAYS','Total Days','Total Days','','~~~','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'SALESREP_ID','Salesrep Name','Salesrep Id','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'PERIOD_NAME','Period Name','Period Name','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Unbilled Rental Revenue',401,'ORDER_TYPE','Order Type','Order Type','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_INV_UNINVODR_DTL_V','','');
--Inserting Report Parameters - Unbilled Rental Revenue
xxeis.eis_rs_ins.rp( 'Unbilled Rental Revenue',401,'Customer Name','Customer Name','CUSTOMER_NAME','IN','INV CUSTOMER NAME','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Unbilled Rental Revenue',401,'Customer Number','Customer Number','CUSTOMER_NUMBER','IN','INV CUSTOMER NUMBER','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Unbilled Rental Revenue',401,'Fiscal Month','Fiscal Month','PERIOD_NAME','IN','EIS INV PERIOD NAME','','VARCHAR2','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Unbilled Rental Revenue',401,'Location','Location','LOCATION','IN','EIS XXWC INV ORGS','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Unbilled Rental Revenue
xxeis.eis_rs_ins.rcn( 'Unbilled Rental Revenue',401,'CUSTOMER_NAME','IN',':Customer Name','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Unbilled Rental Revenue',401,'CUSTOMER_NUMBER','IN',':Customer Number','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Unbilled Rental Revenue',401,'LOCATION','IN',':Location','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Unbilled Rental Revenue',401,'PERIOD_NAME','IN',':Fiscal Month','','','Y','2','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Unbilled Rental Revenue
xxeis.eis_rs_ins.rs( 'Unbilled Rental Revenue',401,'LOCATION','ASC','XXEIS_RS_ADMIN','','');
--Inserting Report Triggers - Unbilled Rental Revenue
xxeis.eis_rs_ins.rt( 'Unbilled Rental Revenue',401,'begin
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.Set_period_name_from(:Fiscal Month);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Unbilled Rental Revenue
--Inserting Report Portals - Unbilled Rental Revenue
--Inserting Report Dashboards - Unbilled Rental Revenue
--Inserting Report Security - Unbilled Rental Revenue
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','','LB048272','',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','','10012196','',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','','MM050208','',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','','SO004816','',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50851',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50619',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','51004',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','20005','','50900',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','20634',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50882',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50924',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','51052',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50879',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50852',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','50821',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','20005','','50880',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','401','','51029',401,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Unbilled Rental Revenue','660','','51065',401,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Unbilled Rental Revenue
xxeis.eis_rs_ins.rpivot( 'Unbilled Rental Revenue',401,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','CUSTOMER_NAME','ROW_FIELD','','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','ORDERED_QUANTITY','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','NEW_SELLING_PRICE','DATA_FIELD','MAX','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','PART_NUMBER','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','UNBILLED_RENTAL_AMOUNT','DATA_FIELD','SUM','','4','1','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Unbilled Rental Revenue',401,'Pivot','UNBILLED_RENTAL_DAYS','DATA_FIELD','SUM','','3','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
