--Report Name            : Open Purchase Orders Listing - WC Test
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Open Purchase Orders Listing - WC Test
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_OPEN_ORDERS_TEST_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V',201,'','','','','PK059658','XXEIS','Eis Xxwc Po Open Orders V','EXPOOV','','');
--Delete View Columns for EIS_XXWC_PO_OPEN_ORDERS_TEST_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_OPEN_ORDERS_TEST_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_OPEN_ORDERS_TEST_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','OPERATING_UNIT',201,'Operating Unit','OPERATING_UNIT','','','','PK059658','VARCHAR2','','','Operating Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','UNIT_MEAS_LOOKUP_CODE',201,'Unit Meas Lookup Code','UNIT_MEAS_LOOKUP_CODE','','','','PK059658','VARCHAR2','','','Unit Meas Lookup Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REQUIRED_BY',201,'Required By','REQUIRED_BY','','','','PK059658','DATE','','','Required By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REQ_CREATED_ON',201,'Req Created On','REQ_CREATED_ON','','','','PK059658','DATE','','','Req Created On','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REQ_NUM',201,'Req Num','REQ_NUM','','','','PK059658','VARCHAR2','','','Req Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_TYPE',201,'Po Type','PO_TYPE','','','','PK059658','VARCHAR2','','','Po Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_STATUS',201,'Po Status','PO_STATUS','','','','PK059658','VARCHAR2','','','Po Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIP_NEED_BY_DATE',201,'Ship Need By Date','SHIP_NEED_BY_DATE','','','','PK059658','DATE','','','Ship Need By Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','AMOUNT_DUE',201,'Amount Due','AMOUNT_DUE','','','','PK059658','NUMBER','','','Amount Due','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','AMOUNT_RECEIVED',201,'Amount Received','AMOUNT_RECEIVED','','','','PK059658','NUMBER','','','Amount Received','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','AMOUNT_ORDERED',201,'Amount Ordered','AMOUNT_ORDERED','','','','PK059658','NUMBER','','','Amount Ordered','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','QUANTITY_DUE',201,'Quantity Due','QUANTITY_DUE','','','','PK059658','NUMBER','','','Quantity Due','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','QUANTITY_RECEIVED',201,'Quantity Received','QUANTITY_RECEIVED','','','','PK059658','NUMBER','','','Quantity Received','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','QUANTITY_ORDERED',201,'Quantity Ordered','QUANTITY_ORDERED','','','','PK059658','NUMBER','','','Quantity Ordered','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_UNIT_PRICE',201,'Po Unit Price','PO_UNIT_PRICE','','','','PK059658','NUMBER','','','Po Unit Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ITEM_DESCRIPTION',201,'Item Description','ITEM_DESCRIPTION','','','','PK059658','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ITEM',201,'Item','ITEM','','','','PK059658','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_NUM',201,'Line Num','LINE_NUM','','','','PK059658','NUMBER','','','Line Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','NOTE_TO_RECEIVER',201,'Note To Receiver','NOTE_TO_RECEIVER','','','','PK059658','VARCHAR2','','','Note To Receiver','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REVISED_DATE',201,'Revised Date','REVISED_DATE','','','','PK059658','DATE','','','Revised Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REV',201,'Rev','REV','','','','PK059658','NUMBER','','','Rev','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SUPPLIER_SITE',201,'Supplier Site','SUPPLIER_SITE','','','','PK059658','VARCHAR2','','','Supplier Site','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_SUPPLIER_NAME',201,'Po Supplier Name','PO_SUPPLIER_NAME','','','','PK059658','VARCHAR2','','','Po Supplier Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_NUMBER',201,'Po Number','PO_NUMBER','','','','PK059658','VARCHAR2','','','Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','BUYER_NAME',201,'Buyer Name','BUYER_NAME','','','','PK059658','VARCHAR2','','','Buyer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_CREATION_DATE',201,'Po Creation Date','PO_CREATION_DATE','','','','PK059658','DATE','','','Po Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ORG_PO_LINES',201,'Org Po Lines','ORG_PO_LINES','','','','PK059658','NUMBER','','','Org Po Lines','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','HEADER_NEED_BY_DATE',201,'Header Need By Date','HEADER_NEED_BY_DATE','','','','PK059658','DATE','','','Header Need By Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIP_VIA',201,'Ship Via','SHIP_VIA','','','','PK059658','VARCHAR2','','','Ship Via','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SUPPLIER_CONTACT_PHONE',201,'Supplier Contact Phone','SUPPLIER_CONTACT_PHONE','','','','PK059658','VARCHAR2','','','Supplier Contact Phone','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIP_TO',201,'Ship To','SHIP_TO','','','','PK059658','VARCHAR2','','','Ship To','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','HEADER_PROMISE_DATE',201,'Header Promise Date','HEADER_PROMISE_DATE','','','','PK059658','DATE','','','Header Promise Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_PROMISE_DATE',201,'Line Promise Date','LINE_PROMISE_DATE','','','','PK059658','DATE','','','Line Promise Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ORG_CODE',201,'Org Code','ORG_CODE','','','','PK059658','VARCHAR2','','','Org Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ORGANIZATION_CODE',201,'Organization Code','ORGANIZATION_CODE','','','','PK059658','VARCHAR2','','','Organization Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ATTRIBUTE2',201,'Attribute2','ATTRIBUTE2','','','','PK059658','VARCHAR2','','','Attribute2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','TRANSFERRED_TO_OE_FLAG',201,'Transferred To Oe Flag','TRANSFERRED_TO_OE_FLAG','','','','PK059658','VARCHAR2','','','Transferred To Oe Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#AMU1',201,'Msi#Wc#Amu1','MSI#WC#AMU1','','','','PK059658','VARCHAR2','','','Msi#Wc#Amu1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#LAST_LEAD_TIME1',201,'Msi#Wc#Last Lead Time1','MSI#WC#LAST_LEAD_TIME1','','','','PK059658','VARCHAR2','','','Msi#Wc#Last Lead Time1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#GTP_INDICATOR1',201,'Msi#Wc#Gtp Indicator1','MSI#WC#GTP_INDICATOR1','','','','PK059658','VARCHAR2','','','Msi#Wc#Gtp Indicator1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#HAZMAT_CONTAINER1',201,'Msi#Wc#Hazmat Container1','MSI#WC#HAZMAT_CONTAINER1','','','','PK059658','VARCHAR2','','','Msi#Wc#Hazmat Container1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#HAZMAT_DESCRIPTION1',201,'Msi#Wc#Hazmat Description1','MSI#WC#HAZMAT_DESCRIPTION1','','','','PK059658','VARCHAR2','','','Msi#Wc#Hazmat Description1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#PRISM_PART_NUMBER1',201,'Msi#Wc#Prism Part Number1','MSI#WC#PRISM_PART_NUMBER1','','','','PK059658','VARCHAR2','','','Msi#Wc#Prism Part Number1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#YEARLY_DC_VELOCITY1',201,'Msi#Wc#Yearly Dc Velocity1','MSI#WC#YEARLY_DC_VELOCITY1','','','','PK059658','VARCHAR2','','','Msi#Wc#Yearly Dc Velocity1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#YEARLY_STORE_VELOCITY1',201,'Msi#Wc#Yearly Store Velocity1','MSI#WC#YEARLY_STORE_VELOCITY1','','','','PK059658','VARCHAR2','','','Msi#Wc#Yearly Store Velocity1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#DC_VELOCITY1',201,'Msi#Wc#Dc Velocity1','MSI#WC#DC_VELOCITY1','','','','PK059658','VARCHAR2','','','Msi#Wc#Dc Velocity1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#STORE_VELOCITY1',201,'Msi#Wc#Store Velocity1','MSI#WC#STORE_VELOCITY1','','','','PK059658','VARCHAR2','','','Msi#Wc#Store Velocity1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#ORM_D_FLAG1',201,'Msi#Wc#Orm D Flag1','MSI#WC#ORM_D_FLAG1','','','','PK059658','VARCHAR2','','','Msi#Wc#Orm D Flag1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#COUNTRY_OF_ORIGIN1',201,'Msi#Wc#Country Of Origin1','MSI#WC#COUNTRY_OF_ORIGIN1','','','','PK059658','VARCHAR2','','','Msi#Wc#Country Of Origin1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#CA_PROP_651',201,'Msi#Wc#Ca Prop 651','MSI#WC#CA_PROP_651','','','','PK059658','VARCHAR2','','','Msi#Wc#Ca Prop 651','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#SKU_DESCRIPTION1',201,'Msi#Hds#Sku Description1','MSI#HDS#SKU_DESCRIPTION1','','','','PK059658','VARCHAR2','','','Msi#Hds#Sku Description1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#UPC_PRIMARY1',201,'Msi#Hds#Upc Primary1','MSI#HDS#UPC_PRIMARY1','','','','PK059658','VARCHAR2','','','Msi#Hds#Upc Primary1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#UNSPSC_CODE1',201,'Msi#Hds#Unspsc Code1','MSI#HDS#UNSPSC_CODE1','','','','PK059658','VARCHAR2','','','Msi#Hds#Unspsc Code1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#VENDOR_PART_NUMBER1',201,'Msi#Hds#Vendor Part Number1','MSI#HDS#VENDOR_PART_NUMBER1','','','','PK059658','VARCHAR2','','','Msi#Hds#Vendor Part Number1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#PRODUCT_ID1',201,'Msi#Hds#Product Id1','MSI#HDS#PRODUCT_ID1','','','','PK059658','VARCHAR2','','','Msi#Hds#Product Id1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#INVOICE_UOM1',201,'Msi#Hds#Invoice Uom1','MSI#HDS#INVOICE_UOM1','','','','PK059658','VARCHAR2','','','Msi#Hds#Invoice Uom1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#DROP_SHIPMENT_ELIGAB1',201,'Msi#Hds#Drop Shipment Eligab1','MSI#HDS#DROP_SHIPMENT_ELIGAB1','','','','PK059658','VARCHAR2','','','Msi#Hds#Drop Shipment Eligab1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#DROP_SHIPMENT_ELIGAB',201,'Msi#Hds#Drop Shipment Eligab','MSI#HDS#DROP_SHIPMENT_ELIGAB','','','','PK059658','VARCHAR2','','','Msi#Hds#Drop Shipment Eligab','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#LOB1',201,'Msi#Hds#Lob1','MSI#HDS#LOB1','','','','PK059658','VARCHAR2','','','Msi#Hds#Lob1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MCA#SALES_ACCOUNT',201,'Mca#Sales Account','MCA#SALES_ACCOUNT','','','','PK059658','VARCHAR2','','','Mca#Sales Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MCA#COGS_ACCOUNT',201,'Mca#Cogs Account','MCA#COGS_ACCOUNT','','','','PK059658','VARCHAR2','','','Mca#Cogs Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PVS#R11VENDOR#R11_VENDOR_ID',201,'Pvs#R11vendor#R11 Vendor Id','PVS#R11VENDOR#R11_VENDOR_ID','','','','PK059658','VARCHAR2','','','Pvs#R11vendor#R11 Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PVS#R11VENDOR#R11_VENDOR_SIT',201,'Pvs#R11vendor#R11 Vendor Sit','PVS#R11VENDOR#R11_VENDOR_SIT','','','','PK059658','VARCHAR2','','','Pvs#R11vendor#R11 Vendor Sit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PVS#IWOFIPS#FIPS_CODE',201,'Pvs#Iwofips#Fips Code','PVS#IWOFIPS#FIPS_CODE','','','','PK059658','VARCHAR2','','','Pvs#Iwofips#Fips Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PV#TAXEXEMPT#EXEMPTIONS',201,'Pv#Taxexempt#Exemptions','PV#TAXEXEMPT#EXEMPTIONS','','','','PK059658','VARCHAR2','','','Pv#Taxexempt#Exemptions','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PV#R11VENDOR#R11_VENDOR_ID',201,'Pv#R11vendor#R11 Vendor Id','PV#R11VENDOR#R11_VENDOR_ID','','','','PK059658','VARCHAR2','','','Pv#R11vendor#R11 Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','POH#STANDARDP#FOB_TERMS_TAB',201,'Poh#Standardp#Fob Terms Tab','POH#STANDARDP#FOB_TERMS_TAB','','','','PK059658','VARCHAR2','','','Poh#Standardp#Fob Terms Tab','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','POH#STANDARDP#CARRIER_TERMS_',201,'Poh#Standardp#Carrier Terms ','POH#STANDARDP#CARRIER_TERMS_','','','','PK059658','VARCHAR2','','','Poh#Standardp#Carrier Terms ','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','POH#STANDARDP#FREIGHT_TERMS_',201,'Poh#Standardp#Freight Terms ','POH#STANDARDP#FREIGHT_TERMS_','','','','PK059658','VARCHAR2','','','Poh#Standardp#Freight Terms ','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','POH#STANDARDP#NEED_BY_DATE',201,'Poh#Standardp#Need By Date','POH#STANDARDP#NEED_BY_DATE','','','','PK059658','DATE','','','Poh#Standardp#Need By Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#HAZMAT_PACKAGING_GROU',201,'Msi#Wc#Hazmat Packaging Grou','MSI#WC#HAZMAT_PACKAGING_GROU','','','','PK059658','VARCHAR2','','','Msi#Wc#Hazmat Packaging Grou','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#MSDS_#',201,'Msi#Wc#Msds #','MSI#WC#MSDS_#','','','','PK059658','VARCHAR2','','','Msi#Wc#Msds #','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#VOC_SUB_CATEGORY',201,'Msi#Wc#Voc Sub Category','MSI#WC#VOC_SUB_CATEGORY','','','','PK059658','VARCHAR2','','','Msi#Wc#Voc Sub Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#VOC_CATEGORY',201,'Msi#Wc#Voc Category','MSI#WC#VOC_CATEGORY','','','','PK059658','VARCHAR2','','','Msi#Wc#Voc Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#PESTICIDE_FLAG_STATE',201,'Msi#Wc#Pesticide Flag State','MSI#WC#PESTICIDE_FLAG_STATE','','','','PK059658','VARCHAR2','','','Msi#Wc#Pesticide Flag State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#VOC_GL',201,'Msi#Wc#Voc Gl','MSI#WC#VOC_GL','','','','PK059658','VARCHAR2','','','Msi#Wc#Voc Gl','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#CALC_LEAD_TIME',201,'Msi#Wc#Calc Lead Time','MSI#WC#CALC_LEAD_TIME','','','','PK059658','VARCHAR2','','','Msi#Wc#Calc Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#PESTICIDE_FLAG',201,'Msi#Wc#Pesticide Flag','MSI#WC#PESTICIDE_FLAG','','','','PK059658','VARCHAR2','','','Msi#Wc#Pesticide Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#KEEP_ITEM_ACTIVE',201,'Msi#Wc#Keep Item Active','MSI#WC#KEEP_ITEM_ACTIVE','','','','PK059658','VARCHAR2','','','Msi#Wc#Keep Item Active','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#IMPORT_DUTY_',201,'Msi#Wc#Import Duty ','MSI#WC#IMPORT_DUTY_','','','','PK059658','VARCHAR2','','','Msi#Wc#Import Duty ','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#PRODUCT_CODE',201,'Msi#Wc#Product Code','MSI#WC#PRODUCT_CODE','','','','PK059658','VARCHAR2','','','Msi#Wc#Product Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#AVERAGE_UNITS',201,'Msi#Wc#Average Units','MSI#WC#AVERAGE_UNITS','','','','PK059658','VARCHAR2','','','Msi#Wc#Average Units','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#TAXWARE_CODE',201,'Msi#Wc#Taxware Code','MSI#WC#TAXWARE_CODE','','','','PK059658','VARCHAR2','','','Msi#Wc#Taxware Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#RESERVE_STOCK',201,'Msi#Wc#Reserve Stock','MSI#WC#RESERVE_STOCK','','','','PK059658','VARCHAR2','','','Msi#Wc#Reserve Stock','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#AMU',201,'Msi#Wc#Amu','MSI#WC#AMU','','','','PK059658','VARCHAR2','','','Msi#Wc#Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#LAST_LEAD_TIME',201,'Msi#Wc#Last Lead Time','MSI#WC#LAST_LEAD_TIME','','','','PK059658','VARCHAR2','','','Msi#Wc#Last Lead Time','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#GTP_INDICATOR',201,'Msi#Wc#Gtp Indicator','MSI#WC#GTP_INDICATOR','','','','PK059658','VARCHAR2','','','Msi#Wc#Gtp Indicator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#HAZMAT_CONTAINER',201,'Msi#Wc#Hazmat Container','MSI#WC#HAZMAT_CONTAINER','','','','PK059658','VARCHAR2','','','Msi#Wc#Hazmat Container','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#HAZMAT_DESCRIPTION',201,'Msi#Wc#Hazmat Description','MSI#WC#HAZMAT_DESCRIPTION','','','','PK059658','VARCHAR2','','','Msi#Wc#Hazmat Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#PRISM_PART_NUMBER',201,'Msi#Wc#Prism Part Number','MSI#WC#PRISM_PART_NUMBER','','','','PK059658','VARCHAR2','','','Msi#Wc#Prism Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#YEARLY_DC_VELOCITY',201,'Msi#Wc#Yearly Dc Velocity','MSI#WC#YEARLY_DC_VELOCITY','','','','PK059658','VARCHAR2','','','Msi#Wc#Yearly Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#YEARLY_STORE_VELOCITY',201,'Msi#Wc#Yearly Store Velocity','MSI#WC#YEARLY_STORE_VELOCITY','','','','PK059658','VARCHAR2','','','Msi#Wc#Yearly Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#DC_VELOCITY',201,'Msi#Wc#Dc Velocity','MSI#WC#DC_VELOCITY','','','','PK059658','VARCHAR2','','','Msi#Wc#Dc Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#STORE_VELOCITY',201,'Msi#Wc#Store Velocity','MSI#WC#STORE_VELOCITY','','','','PK059658','VARCHAR2','','','Msi#Wc#Store Velocity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#ORM_D_FLAG',201,'Msi#Wc#Orm D Flag','MSI#WC#ORM_D_FLAG','','','','PK059658','VARCHAR2','','','Msi#Wc#Orm D Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#COUNTRY_OF_ORIGIN',201,'Msi#Wc#Country Of Origin','MSI#WC#COUNTRY_OF_ORIGIN','','','','PK059658','VARCHAR2','','','Msi#Wc#Country Of Origin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#CA_PROP_65',201,'Msi#Wc#Ca Prop 65','MSI#WC#CA_PROP_65','','','','PK059658','VARCHAR2','','','Msi#Wc#Ca Prop 65','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#SKU_DESCRIPTION',201,'Msi#Hds#Sku Description','MSI#HDS#SKU_DESCRIPTION','','','','PK059658','VARCHAR2','','','Msi#Hds#Sku Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#UPC_PRIMARY',201,'Msi#Hds#Upc Primary','MSI#HDS#UPC_PRIMARY','','','','PK059658','VARCHAR2','','','Msi#Hds#Upc Primary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#UNSPSC_CODE',201,'Msi#Hds#Unspsc Code','MSI#HDS#UNSPSC_CODE','','','','PK059658','VARCHAR2','','','Msi#Hds#Unspsc Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#VENDOR_PART_NUMBER',201,'Msi#Hds#Vendor Part Number','MSI#HDS#VENDOR_PART_NUMBER','','','','PK059658','VARCHAR2','','','Msi#Hds#Vendor Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#PRODUCT_ID',201,'Msi#Hds#Product Id','MSI#HDS#PRODUCT_ID','','','','PK059658','VARCHAR2','','','Msi#Hds#Product Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#INVOICE_UOM',201,'Msi#Hds#Invoice Uom','MSI#HDS#INVOICE_UOM','','','','PK059658','VARCHAR2','','','Msi#Hds#Invoice Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#DROP_SHIPMENT',201,'Msi#Hds#Drop Shipment','MSI#HDS#DROP_SHIPMENT','','','','PK059658','VARCHAR2','','','Msi#Hds#Drop Shipment','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#HDS#LOB',201,'Msi#Hds#Lob','MSI#HDS#LOB','','','','PK059658','VARCHAR2','','','Msi#Hds#Lob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#BRANCH',201,'Gcc#Branch','GCC#BRANCH','','','','PK059658','VARCHAR2','','','Gcc#Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#BRANCH',201,'Gcc1#Branch','GCC1#BRANCH','','','','PK059658','VARCHAR2','','','Gcc1#Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#BRANCH',201,'Gcc2#Branch','GCC2#BRANCH','','','','PK059658','VARCHAR2','','','Gcc2#Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI_ORGANIZATION_ID',201,'Msi Organization Id','MSI_ORGANIZATION_ID','','','','PK059658','NUMBER','','','Msi Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PDT_ORG_ID',201,'Pdt Org Id','PDT_ORG_ID','','','','PK059658','NUMBER','','','Pdt Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PDT_DOCUMENT_SUBTYPE',201,'Pdt Document Subtype','PDT_DOCUMENT_SUBTYPE','','','','PK059658','VARCHAR2','','','Pdt Document Subtype','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PDT_DOCUMENT_TYPE_CODE',201,'Pdt Document Type Code','PDT_DOCUMENT_TYPE_CODE','','','','PK059658','VARCHAR2','','','Pdt Document Type Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PSP_ORG_ID',201,'Psp Org Id','PSP_ORG_ID','','','','PK059658','NUMBER','','','Psp Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PVC_VENDOR_SITE_ID',201,'Pvc Vendor Site Id','PVC_VENDOR_SITE_ID','','','','PK059658','NUMBER','','','Pvc Vendor Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PVS_VENDOR_ID',201,'Pvs Vendor Id','PVS_VENDOR_ID','','','','PK059658','NUMBER','','','Pvs Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','CATEGORY_ID',201,'Category Id','CATEGORY_ID','','','','PK059658','NUMBER','','','Category Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','PK059658','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','TERM_ID',201,'Term Id','TERM_ID','','','','PK059658','NUMBER','','','Term Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','AGENT_ID',201,'Agent Id','AGENT_ID','','','','PK059658','NUMBER','','','Agent Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','EMPLOYEE_ID',201,'Employee Id','EMPLOYEE_ID','','','','PK059658','NUMBER','','','Employee Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_TYPE_ID',201,'Line Type Id','LINE_TYPE_ID','','','','PK059658','NUMBER','','','Line Type Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','VENDOR_CONTACT_ID',201,'Vendor Contact Id','VENDOR_CONTACT_ID','','','','PK059658','NUMBER','','','Vendor Contact Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_RELEASE_ID',201,'Po Release Id','PO_RELEASE_ID','','','','PK059658','NUMBER','','','Po Release Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LOCATION_ID',201,'Location Id','LOCATION_ID','','','','PK059658','NUMBER','','','Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SET_OF_BOOKS_ID',201,'Set Of Books Id','SET_OF_BOOKS_ID','','','','PK059658','NUMBER','','','Set Of Books Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIP_TO_LOCATION_ID',201,'Ship To Location Id','SHIP_TO_LOCATION_ID','','','','PK059658','NUMBER','','','Ship To Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','VENDOR_SITE_ID',201,'Vendor Site Id','VENDOR_SITE_ID','','','','PK059658','NUMBER','','','Vendor Site Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','VENDOR_ID',201,'Vendor Id','VENDOR_ID','','','','PK059658','NUMBER','','','Vendor Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REQUISITION_LINE_ID',201,'Requisition Line Id','REQUISITION_LINE_ID','','','','PK059658','NUMBER','','','Requisition Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REQUISITION_HEADER_ID',201,'Requisition Header Id','REQUISITION_HEADER_ID','','','','PK059658','NUMBER','','','Requisition Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PREPARER_ID',201,'Preparer Id','PREPARER_ID','','','','PK059658','NUMBER','','','Preparer Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ITEM_ID',201,'Item Id','ITEM_ID','','','','PK059658','NUMBER','','','Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','DESTINATION_ORGANIZATION_ID',201,'Destination Organization Id','DESTINATION_ORGANIZATION_ID','','','','PK059658','NUMBER','','','Destination Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SOURCE_ORGANIZATION_ID',201,'Source Organization Id','SOURCE_ORGANIZATION_ID','','','','PK059658','NUMBER','','','Source Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SOURCE_TYPE_CODE',201,'Source Type Code','SOURCE_TYPE_CODE','','','','PK059658','VARCHAR2','','','Source Type Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','CANCEL_FLAG',201,'Cancel Flag','CANCEL_FLAG','','','','PK059658','VARCHAR2','','','Cancel Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REQ_QUANTITY_DELIVERED',201,'Req Quantity Delivered','REQ_QUANTITY_DELIVERED','','','','PK059658','NUMBER','','','Req Quantity Delivered','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REQ_QUANTITY_CANCELLED',201,'Req Quantity Cancelled','REQ_QUANTITY_CANCELLED','','','','PK059658','NUMBER','','','Req Quantity Cancelled','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','QUANTITY',201,'Quantity','QUANTITY','','','','PK059658','NUMBER','','','Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','UNIT_PRICE',201,'Unit Price','UNIT_PRICE','','','','PK059658','NUMBER','','','Unit Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','REQ_LINE_CREATION_DATE',201,'Req Line Creation Date','REQ_LINE_CREATION_DATE','','','','PK059658','DATE','','','Req Line Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_DATA_TYPE',201,'Po Data Type','PO_DATA_TYPE','','','','PK059658','VARCHAR2','','','Po Data Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','VARIANCE_ACCOUNT',201,'Variance Account','VARIANCE_ACCOUNT','','','','PK059658','VARCHAR2','','','Variance Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','VARIANCE_CCID',201,'Variance Ccid','VARIANCE_CCID','','','','PK059658','NUMBER','','','Variance Ccid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACCRUAL_ACCOUNT',201,'Accrual Account','ACCRUAL_ACCOUNT','','','','PK059658','VARCHAR2','','','Accrual Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACCRUAL_CCID',201,'Accrual Ccid','ACCRUAL_CCID','','','','PK059658','NUMBER','','','Accrual Ccid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_CHARGE_ACCOUNT',201,'Po Charge Account','PO_CHARGE_ACCOUNT','','','','PK059658','VARCHAR2','','','Po Charge Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','CHARGE_CCID',201,'Charge Ccid','CHARGE_CCID','','','','PK059658','NUMBER','','','Charge Ccid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','COMPANY',201,'Company','COMPANY','','','','PK059658','VARCHAR2','','','Company','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','RATE',201,'Rate','RATE','','','','PK059658','NUMBER','','','Rate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIP_DIST',201,'Ship Dist','SHIP_DIST','','','','PK059658','VARCHAR2','','','Ship Dist','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','DISTRIBUTION_NUM',201,'Distribution Num','DISTRIBUTION_NUM','','','','PK059658','NUMBER','','','Distribution Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','DISTRIBUTION_ID',201,'Distribution Id','DISTRIBUTION_ID','','','','PK059658','NUMBER','','','Distribution Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACCRUE_ON_RECEIPT',201,'Accrue On Receipt','ACCRUE_ON_RECEIPT','','','','PK059658','VARCHAR2','','','Accrue On Receipt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PRICE_OVERRIDE',201,'Price Override','PRICE_OVERRIDE','','','','PK059658','NUMBER','','','Price Override','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','RECEIVE_CLOSE_TOLERANCE',201,'Receive Close Tolerance','RECEIVE_CLOSE_TOLERANCE','','','','PK059658','NUMBER','','','Receive Close Tolerance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','INVOICE_CLOSE_TOLERANCE',201,'Invoice Close Tolerance','INVOICE_CLOSE_TOLERANCE','','','','PK059658','NUMBER','','','Invoice Close Tolerance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','DAYS_LATE_RECEIPT_ALLOWED',201,'Days Late Receipt Allowed','DAYS_LATE_RECEIPT_ALLOWED','','','','PK059658','NUMBER','','','Days Late Receipt Allowed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','DAYS_EARLY_RECEIPT_ALLOWED',201,'Days Early Receipt Allowed','DAYS_EARLY_RECEIPT_ALLOWED','','','','PK059658','NUMBER','','','Days Early Receipt Allowed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','QTY_RCV_TOLERANCE',201,'Qty Rcv Tolerance','QTY_RCV_TOLERANCE','','','','PK059658','NUMBER','','','Qty Rcv Tolerance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_3_WAY_MATCH',201,'Po 3 Way Match','PO_3_WAY_MATCH','','','','PK059658','VARCHAR2','','','Po 3 Way Match','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_4_WAY_MATCH',201,'Po 4 Way Match','PO_4_WAY_MATCH','','','','PK059658','VARCHAR2','','','Po 4 Way Match','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIP_LAST_ACCEPT_DATE',201,'Ship Last Accept Date','SHIP_LAST_ACCEPT_DATE','','','','PK059658','DATE','','','Ship Last Accept Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIP_PROMISED_DATE',201,'Ship Promised Date','SHIP_PROMISED_DATE','','','','PK059658','DATE','','','Ship Promised Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIPMENT_TYPE',201,'Shipment Type','SHIPMENT_TYPE','','','','PK059658','VARCHAR2','','','Shipment Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIPMENT_NUM',201,'Shipment Num','SHIPMENT_NUM','','','','PK059658','NUMBER','','','Shipment Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_LOCATION_ID',201,'Line Location Id','LINE_LOCATION_ID','','','','PK059658','NUMBER','','','Line Location Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_CLOSED_REASON',201,'Line Closed Reason','LINE_CLOSED_REASON','','','','PK059658','VARCHAR2','','','Line Closed Reason','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_CLOSED_DATE',201,'Line Closed Date','LINE_CLOSED_DATE','','','','PK059658','DATE','','','Line Closed Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_CLOSED_CODE',201,'Line Closed Code','LINE_CLOSED_CODE','','','','PK059658','VARCHAR2','','','Line Closed Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','TYPE_1099',201,'Type 1099','TYPE_1099','','','','PK059658','VARCHAR2','','','Type 1099','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','TAX_NAME',201,'Tax Name','TAX_NAME','','','','PK059658','VARCHAR2','','','Tax Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','TAXABLE_FLAG',201,'Taxable Flag','TAXABLE_FLAG','','','','PK059658','VARCHAR2','','','Taxable Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_CANCEL_REASON',201,'Line Cancel Reason','LINE_CANCEL_REASON','','','','PK059658','VARCHAR2','','','Line Cancel Reason','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_CANCEL_DATE',201,'Line Cancel Date','LINE_CANCEL_DATE','','','','PK059658','DATE','','','Line Cancel Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_CANCEL',201,'Line Cancel','LINE_CANCEL','','','','PK059658','VARCHAR2','','','Line Cancel','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MARKET_PRICE',201,'Market Price','MARKET_PRICE','','','','PK059658','NUMBER','','','Market Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_HEADER_ID',201,'Po Header Id','PO_HEADER_ID','','','','PK059658','NUMBER','','','Po Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_LIST_PRICE',201,'Line List Price','LINE_LIST_PRICE','','','','PK059658','NUMBER','','','Line List Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_UNIT',201,'Line Unit','LINE_UNIT','','','','PK059658','VARCHAR2','','','Line Unit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ITEM_REVISION',201,'Item Revision','ITEM_REVISION','','','','PK059658','VARCHAR2','','','Item Revision','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','OPEN_FOR',201,'Open For','OPEN_FOR','','','','PK059658','VARCHAR2','','','Open For','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','OPEN_INVOICE_AMOUNT',201,'Open Invoice Amount','OPEN_INVOICE_AMOUNT','','','','PK059658','NUMBER','','','Open Invoice Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','AMOUNT_BILLED',201,'Amount Billed','AMOUNT_BILLED','','','','PK059658','NUMBER','','','Amount Billed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','AMOUNT_CANCELLED',201,'Amount Cancelled','AMOUNT_CANCELLED','','','','PK059658','NUMBER','','','Amount Cancelled','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','QUANTITY_BILLED',201,'Quantity Billed','QUANTITY_BILLED','','','','PK059658','NUMBER','','','Quantity Billed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','QUANTITY_CANCELLED',201,'Quantity Cancelled','QUANTITY_CANCELLED','','','','PK059658','NUMBER','','','Quantity Cancelled','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','CATEGORY',201,'Category','CATEGORY','','','','PK059658','VARCHAR2','','','Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_TYPE',201,'Line Type','LINE_TYPE','','','','PK059658','VARCHAR2','','','Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_CREATED_BY',201,'Line Created By','LINE_CREATED_BY','','','','PK059658','NUMBER','','','Line Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_CREATION_DATE',201,'Line Creation Date','LINE_CREATION_DATE','','','','PK059658','DATE','','','Line Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_LAST_UPDATED_BY',201,'Line Last Updated By','LINE_LAST_UPDATED_BY','','','','PK059658','NUMBER','','','Line Last Updated By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_LAST_UPDATE_DATE',201,'Line Last Update Date','LINE_LAST_UPDATE_DATE','','','','PK059658','DATE','','','Line Last Update Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LINE_ID',201,'Line Id','LINE_ID','','','','PK059658','NUMBER','','','Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','CLOSED_DATE',201,'Closed Date','CLOSED_DATE','','','','PK059658','DATE','','','Closed Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','CLOSED_CODE',201,'Closed Code','CLOSED_CODE','','','','PK059658','VARCHAR2','','','Closed Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','COMMENTS',201,'Comments','COMMENTS','','','','PK059658','VARCHAR2','','','Comments','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','NOTE_TO_SUPPLIER',201,'Note To Supplier','NOTE_TO_SUPPLIER','','','','PK059658','VARCHAR2','','','Note To Supplier','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','NOTE_TO_AUTHORIZER',201,'Note To Authorizer','NOTE_TO_AUTHORIZER','','','','PK059658','VARCHAR2','','','Note To Authorizer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACCEPTANCE_REQUIRED',201,'Acceptance Required','ACCEPTANCE_REQUIRED','','','','PK059658','VARCHAR2','','','Acceptance Required','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','HOLD',201,'Hold','HOLD','','','','PK059658','VARCHAR2','','','Hold','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACCEPTANCE_DUE_DATE',201,'Acceptance Due Date','ACCEPTANCE_DUE_DATE','','','','PK059658','DATE','','','Acceptance Due Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','CURRENCY_CODE',201,'Currency Code','CURRENCY_CODE','','','','PK059658','VARCHAR2','','','Currency Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','APPROVED_DATE',201,'Approved Date','APPROVED_DATE','','','','PK059658','DATE','','','Approved Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PRINTED_DATE',201,'Printed Date','PRINTED_DATE','','','','PK059658','DATE','','','Printed Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','FREIGHT_TERMS',201,'Freight Terms','FREIGHT_TERMS','','','','PK059658','VARCHAR2','','','Freight Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','FOB',201,'Fob','FOB','','','','PK059658','VARCHAR2','','','Fob','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_TERMS',201,'Po Terms','PO_TERMS','','','','PK059658','VARCHAR2','','','Po Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIP_TO_LOCATION',201,'Ship To Location','SHIP_TO_LOCATION','','','','PK059658','VARCHAR2','','','Ship To Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','END_DATE',201,'End Date','END_DATE','','','','PK059658','DATE','','','End Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','START_DATE',201,'Start Date','START_DATE','','','','PK059658','DATE','','','Start Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_ENABLED',201,'Po Enabled','PO_ENABLED','','','','PK059658','VARCHAR2','','','Po Enabled','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_SUMMARY',201,'Po Summary','PO_SUMMARY','','','','PK059658','VARCHAR2','','','Po Summary','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SUPPLIER_CONTACT_AREA_CODE',201,'Supplier Contact Area Code','SUPPLIER_CONTACT_AREA_CODE','','','','PK059658','VARCHAR2','','','Supplier Contact Area Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SUPPLIER_CONTACT_LAST_NAME',201,'Supplier Contact Last Name','SUPPLIER_CONTACT_LAST_NAME','','','','PK059658','VARCHAR2','','','Supplier Contact Last Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SUPPLIER_CONTACT_FIRST_NAME',201,'Supplier Contact First Name','SUPPLIER_CONTACT_FIRST_NAME','','','','PK059658','VARCHAR2','','','Supplier Contact First Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','RELEASE_NUM',201,'Release Num','RELEASE_NUM','','','','PK059658','NUMBER','','','Release Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PO_NUMBER_RELEASE',201,'Po Number Release','PO_NUMBER_RELEASE','','','','PK059658','VARCHAR2','','','Po Number Release','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','CREATED_BY',201,'Created By','CREATED_BY','','','','PK059658','NUMBER','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LAST_UPDATED_BY',201,'Last Updated By','LAST_UPDATED_BY','','','','PK059658','NUMBER','','','Last Updated By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','LAST_UPDATE_DATE',201,'Last Update Date','LAST_UPDATE_DATE','','','','PK059658','DATE','','','Last Update Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#SUBACCOUNT#DESCR',201,'Gcc2#50368#Subaccount#Descr','GCC2#50368#SUBACCOUNT#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50368#Subaccount#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#RESERVE_STOCK1',201,'Msi#Wc#Reserve Stock1','MSI#WC#RESERVE_STOCK1','','','','PK059658','VARCHAR2','','','Msi#Wc#Reserve Stock1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#SUBACCOUNT',201,'Gcc2#50368#Subaccount','GCC2#50368#SUBACCOUNT','','','','PK059658','VARCHAR2','','','Gcc2#50368#Subaccount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#PRODUCT#DESCR',201,'Gcc2#50368#Product#Descr','GCC2#50368#PRODUCT#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50368#Product#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#PRODUCT',201,'Gcc2#50368#Product','GCC2#50368#PRODUCT','','','','PK059658','VARCHAR2','','','Gcc2#50368#Product','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#FUTURE_USE#DESCR',201,'Gcc2#50368#Future Use#Descr','GCC2#50368#FUTURE_USE#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50368#Future Use#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#FUTURE_USE',201,'Gcc2#50368#Future Use','GCC2#50368#FUTURE_USE','','','','PK059658','VARCHAR2','','','Gcc2#50368#Future Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#DIVISION#DESCR',201,'Gcc2#50368#Division#Descr','GCC2#50368#DIVISION#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50368#Division#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#DIVISION',201,'Gcc2#50368#Division','GCC2#50368#DIVISION','','','','PK059658','VARCHAR2','','','Gcc2#50368#Division','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#DEPARTMENT#DESCR',201,'Gcc2#50368#Department#Descr','GCC2#50368#DEPARTMENT#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50368#Department#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#DEPARTMENT',201,'Gcc2#50368#Department','GCC2#50368#DEPARTMENT','','','','PK059658','VARCHAR2','','','Gcc2#50368#Department','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#ACCOUNT#DESCR',201,'Gcc2#50368#Account#Descr','GCC2#50368#ACCOUNT#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50368#Account#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50368#ACCOUNT',201,'Gcc2#50368#Account','GCC2#50368#ACCOUNT','','','','PK059658','VARCHAR2','','','Gcc2#50368#Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#SUBACCOUNT#DESCR',201,'Gcc1#50368#Subaccount#Descr','GCC1#50368#SUBACCOUNT#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50368#Subaccount#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#SUBACCOUNT',201,'Gcc1#50368#Subaccount','GCC1#50368#SUBACCOUNT','','','','PK059658','VARCHAR2','','','Gcc1#50368#Subaccount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#PRODUCT#DESCR',201,'Gcc1#50368#Product#Descr','GCC1#50368#PRODUCT#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50368#Product#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#PRODUCT',201,'Gcc1#50368#Product','GCC1#50368#PRODUCT','','','','PK059658','VARCHAR2','','','Gcc1#50368#Product','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#FUTURE_USE#DESCR',201,'Gcc1#50368#Future Use#Descr','GCC1#50368#FUTURE_USE#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50368#Future Use#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#FUTURE_USE',201,'Gcc1#50368#Future Use','GCC1#50368#FUTURE_USE','','','','PK059658','VARCHAR2','','','Gcc1#50368#Future Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#DIVISION#DESCR',201,'Gcc1#50368#Division#Descr','GCC1#50368#DIVISION#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50368#Division#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#DIVISION',201,'Gcc1#50368#Division','GCC1#50368#DIVISION','','','','PK059658','VARCHAR2','','','Gcc1#50368#Division','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#DEPARTMENT#DESCR',201,'Gcc1#50368#Department#Descr','GCC1#50368#DEPARTMENT#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50368#Department#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#DEPARTMENT',201,'Gcc1#50368#Department','GCC1#50368#DEPARTMENT','','','','PK059658','VARCHAR2','','','Gcc1#50368#Department','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#ACCOUNT#DESCR',201,'Gcc1#50368#Account#Descr','GCC1#50368#ACCOUNT#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50368#Account#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50368#ACCOUNT',201,'Gcc1#50368#Account','GCC1#50368#ACCOUNT','','','','PK059658','VARCHAR2','','','Gcc1#50368#Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#SUBACCOUNT#DESCR',201,'Gcc#50368#Subaccount#Descr','GCC#50368#SUBACCOUNT#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50368#Subaccount#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#SUBACCOUNT',201,'Gcc#50368#Subaccount','GCC#50368#SUBACCOUNT','','','','PK059658','VARCHAR2','','','Gcc#50368#Subaccount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#PRODUCT#DESCR',201,'Gcc#50368#Product#Descr','GCC#50368#PRODUCT#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50368#Product#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#PRODUCT',201,'Gcc#50368#Product','GCC#50368#PRODUCT','','','','PK059658','VARCHAR2','','','Gcc#50368#Product','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#FUTURE_USE#DESCR',201,'Gcc#50368#Future Use#Descr','GCC#50368#FUTURE_USE#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50368#Future Use#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#FUTURE_USE',201,'Gcc#50368#Future Use','GCC#50368#FUTURE_USE','','','','PK059658','VARCHAR2','','','Gcc#50368#Future Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#DIVISION#DESCR',201,'Gcc#50368#Division#Descr','GCC#50368#DIVISION#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50368#Division#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#DIVISION',201,'Gcc#50368#Division','GCC#50368#DIVISION','','','','PK059658','VARCHAR2','','','Gcc#50368#Division','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#DEPARTMENT#DESCR',201,'Gcc#50368#Department#Descr','GCC#50368#DEPARTMENT#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50368#Department#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#DEPARTMENT',201,'Gcc#50368#Department','GCC#50368#DEPARTMENT','','','','PK059658','VARCHAR2','','','Gcc#50368#Department','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#ACCOUNT#DESCR',201,'Gcc#50368#Account#Descr','GCC#50368#ACCOUNT#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50368#Account#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50368#ACCOUNT',201,'Gcc#50368#Account','GCC#50368#ACCOUNT','','','','PK059658','VARCHAR2','','','Gcc#50368#Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#PROJECT_CODE#DESCR',201,'Gcc2#50328#Project Code#Descr','GCC2#50328#PROJECT_CODE#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50328#Project Code#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#PROJECT_CODE',201,'Gcc2#50328#Project Code','GCC2#50328#PROJECT_CODE','','','','PK059658','VARCHAR2','','','Gcc2#50328#Project Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#PRODUCT#DESCR',201,'Gcc2#50328#Product#Descr','GCC2#50328#PRODUCT#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50328#Product#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#PRODUCT',201,'Gcc2#50328#Product','GCC2#50328#PRODUCT','','','','PK059658','VARCHAR2','','','Gcc2#50328#Product','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#LOCATION#DESCR',201,'Gcc2#50328#Location#Descr','GCC2#50328#LOCATION#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50328#Location#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#LOCATION',201,'Gcc2#50328#Location','GCC2#50328#LOCATION','','','','PK059658','VARCHAR2','','','Gcc2#50328#Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#FUTURE_USE_2#DESCR',201,'Gcc2#50328#Future Use 2#Descr','GCC2#50328#FUTURE_USE_2#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50328#Future Use 2#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#FUTURE_USE_2',201,'Gcc2#50328#Future Use 2','GCC2#50328#FUTURE_USE_2','','','','PK059658','VARCHAR2','','','Gcc2#50328#Future Use 2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#FURTURE_USE#DESCR',201,'Gcc2#50328#Furture Use#Descr','GCC2#50328#FURTURE_USE#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50328#Furture Use#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#FURTURE_USE',201,'Gcc2#50328#Furture Use','GCC2#50328#FURTURE_USE','','','','PK059658','VARCHAR2','','','Gcc2#50328#Furture Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#COST_CENTER#DESCR',201,'Gcc2#50328#Cost Center#Descr','GCC2#50328#COST_CENTER#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50328#Cost Center#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#COST_CENTER',201,'Gcc2#50328#Cost Center','GCC2#50328#COST_CENTER','','','','PK059658','VARCHAR2','','','Gcc2#50328#Cost Center','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#ACCOUNT#DESCR',201,'Gcc2#50328#Account#Descr','GCC2#50328#ACCOUNT#DESCR','','','','PK059658','VARCHAR2','','','Gcc2#50328#Account#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC2#50328#ACCOUNT',201,'Gcc2#50328#Account','GCC2#50328#ACCOUNT','','','','PK059658','VARCHAR2','','','Gcc2#50328#Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#PROJECT_CODE#DESCR',201,'Gcc1#50328#Project Code#Descr','GCC1#50328#PROJECT_CODE#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50328#Project Code#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#PROJECT_CODE',201,'Gcc1#50328#Project Code','GCC1#50328#PROJECT_CODE','','','','PK059658','VARCHAR2','','','Gcc1#50328#Project Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#PRODUCT#DESCR',201,'Gcc1#50328#Product#Descr','GCC1#50328#PRODUCT#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50328#Product#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#PRODUCT',201,'Gcc1#50328#Product','GCC1#50328#PRODUCT','','','','PK059658','VARCHAR2','','','Gcc1#50328#Product','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#LOCATION#DESCR',201,'Gcc1#50328#Location#Descr','GCC1#50328#LOCATION#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50328#Location#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#LOCATION',201,'Gcc1#50328#Location','GCC1#50328#LOCATION','','','','PK059658','VARCHAR2','','','Gcc1#50328#Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#FUTURE_USE_2#DESCR',201,'Gcc1#50328#Future Use 2#Descr','GCC1#50328#FUTURE_USE_2#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50328#Future Use 2#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#FUTURE_USE_2',201,'Gcc1#50328#Future Use 2','GCC1#50328#FUTURE_USE_2','','','','PK059658','VARCHAR2','','','Gcc1#50328#Future Use 2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#FURTURE_USE#DESCR',201,'Gcc1#50328#Furture Use#Descr','GCC1#50328#FURTURE_USE#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50328#Furture Use#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#FURTURE_USE',201,'Gcc1#50328#Furture Use','GCC1#50328#FURTURE_USE','','','','PK059658','VARCHAR2','','','Gcc1#50328#Furture Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#COST_CENTER#DESCR',201,'Gcc1#50328#Cost Center#Descr','GCC1#50328#COST_CENTER#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50328#Cost Center#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#COST_CENTER',201,'Gcc1#50328#Cost Center','GCC1#50328#COST_CENTER','','','','PK059658','VARCHAR2','','','Gcc1#50328#Cost Center','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#ACCOUNT#DESCR',201,'Gcc1#50328#Account#Descr','GCC1#50328#ACCOUNT#DESCR','','','','PK059658','VARCHAR2','','','Gcc1#50328#Account#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC1#50328#ACCOUNT',201,'Gcc1#50328#Account','GCC1#50328#ACCOUNT','','','','PK059658','VARCHAR2','','','Gcc1#50328#Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#PROJECT_CODE#DESCR',201,'Gcc#50328#Project Code#Descr','GCC#50328#PROJECT_CODE#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50328#Project Code#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#PROJECT_CODE',201,'Gcc#50328#Project Code','GCC#50328#PROJECT_CODE','','','','PK059658','VARCHAR2','','','Gcc#50328#Project Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#PRODUCT#DESCR',201,'Gcc#50328#Product#Descr','GCC#50328#PRODUCT#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50328#Product#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#PRODUCT',201,'Gcc#50328#Product','GCC#50328#PRODUCT','','','','PK059658','VARCHAR2','','','Gcc#50328#Product','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#LOCATION#DESCR',201,'Gcc#50328#Location#Descr','GCC#50328#LOCATION#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50328#Location#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#LOCATION',201,'Gcc#50328#Location','GCC#50328#LOCATION','','','','PK059658','VARCHAR2','','','Gcc#50328#Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#FUTURE_USE_2#DESCR',201,'Gcc#50328#Future Use 2#Descr','GCC#50328#FUTURE_USE_2#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50328#Future Use 2#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#FUTURE_USE_2',201,'Gcc#50328#Future Use 2','GCC#50328#FUTURE_USE_2','','','','PK059658','VARCHAR2','','','Gcc#50328#Future Use 2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#FURTURE_USE#DESCR',201,'Gcc#50328#Furture Use#Descr','GCC#50328#FURTURE_USE#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50328#Furture Use#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#FURTURE_USE',201,'Gcc#50328#Furture Use','GCC#50328#FURTURE_USE','','','','PK059658','VARCHAR2','','','Gcc#50328#Furture Use','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#COST_CENTER#DESCR',201,'Gcc#50328#Cost Center#Descr','GCC#50328#COST_CENTER#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50328#Cost Center#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#COST_CENTER',201,'Gcc#50328#Cost Center','GCC#50328#COST_CENTER','','','','PK059658','VARCHAR2','','','Gcc#50328#Cost Center','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#ACCOUNT#DESCR',201,'Gcc#50328#Account#Descr','GCC#50328#ACCOUNT#DESCR','','','','PK059658','VARCHAR2','','','Gcc#50328#Account#Descr','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','GCC#50328#ACCOUNT',201,'Gcc#50328#Account','GCC#50328#ACCOUNT','','','','PK059658','VARCHAR2','','','Gcc#50328#Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PV#PRISM#PRISM_SUPPLIER_NUM',201,'Pv#Prism#Prism Supplier Num','PV#PRISM#PRISM_SUPPLIER_NUM','','','','PK059658','VARCHAR2','','','Pv#Prism#Prism Supplier Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','PV#LEGACY#LEGACY_SUPPLIER_NU',201,'Pv#Legacy#Legacy Supplier Nu','PV#LEGACY#LEGACY_SUPPLIER_NU','','','','PK059658','VARCHAR2','','','Pv#Legacy#Legacy Supplier Nu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#HAZMAT_PACKAGING_GROU1',201,'Msi#Wc#Hazmat Packaging Grou1','MSI#WC#HAZMAT_PACKAGING_GROU1','','','','PK059658','VARCHAR2','','','Msi#Wc#Hazmat Packaging Grou1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#MSDS_#1',201,'Msi#Wc#Msds #1','MSI#WC#MSDS_#1','','','','PK059658','VARCHAR2','','','Msi#Wc#Msds #1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#VOC_SUB_CATEGORY1',201,'Msi#Wc#Voc Sub Category1','MSI#WC#VOC_SUB_CATEGORY1','','','','PK059658','VARCHAR2','','','Msi#Wc#Voc Sub Category1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#VOC_CATEGORY1',201,'Msi#Wc#Voc Category1','MSI#WC#VOC_CATEGORY1','','','','PK059658','VARCHAR2','','','Msi#Wc#Voc Category1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#PESTICIDE_FLAG_STATE1',201,'Msi#Wc#Pesticide Flag State1','MSI#WC#PESTICIDE_FLAG_STATE1','','','','PK059658','VARCHAR2','','','Msi#Wc#Pesticide Flag State1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#VOC_GL1',201,'Msi#Wc#Voc Gl1','MSI#WC#VOC_GL1','','','','PK059658','VARCHAR2','','','Msi#Wc#Voc Gl1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#CALC_LEAD_TIME1',201,'Msi#Wc#Calc Lead Time1','MSI#WC#CALC_LEAD_TIME1','','','','PK059658','VARCHAR2','','','Msi#Wc#Calc Lead Time1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#PESTICIDE_FLAG1',201,'Msi#Wc#Pesticide Flag1','MSI#WC#PESTICIDE_FLAG1','','','','PK059658','VARCHAR2','','','Msi#Wc#Pesticide Flag1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#KEEP_ITEM_ACTIVE1',201,'Msi#Wc#Keep Item Active1','MSI#WC#KEEP_ITEM_ACTIVE1','','','','PK059658','VARCHAR2','','','Msi#Wc#Keep Item Active1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#IMPORT_DUTY_1',201,'Msi#Wc#Import Duty 1','MSI#WC#IMPORT_DUTY_1','','','','PK059658','VARCHAR2','','','Msi#Wc#Import Duty 1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#PRODUCT_CODE1',201,'Msi#Wc#Product Code1','MSI#WC#PRODUCT_CODE1','','','','PK059658','VARCHAR2','','','Msi#Wc#Product Code1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#AVERAGE_UNITS1',201,'Msi#Wc#Average Units1','MSI#WC#AVERAGE_UNITS1','','','','PK059658','VARCHAR2','','','Msi#Wc#Average Units1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','MSI#WC#TAXWARE_CODE1',201,'Msi#Wc#Taxware Code1','MSI#WC#TAXWARE_CODE1','','','','PK059658','VARCHAR2','','','Msi#Wc#Taxware Code1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SHIP_CLOSED_CODE',201,'Ship Closed Code','SHIP_CLOSED_CODE','','','','PK059658','VARCHAR2','','','Ship Closed Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','SEQ#',201,'Seq#','SEQ#','','','','PK059658','NUMBER','','','Seq#','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACCEPTANCE_TYPE',201,'Acceptance Type','ACCEPTANCE_TYPE','','','','PK059658','VARCHAR2','','','Acceptance Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACCEPTED_BY',201,'Accepted By','ACCEPTED_BY','','','','PK059658','VARCHAR2','','','Accepted By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACCEPTED_FLAG',201,'Accepted Flag','ACCEPTED_FLAG','','','','PK059658','VARCHAR2','','','Accepted Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACC_DUE_DATE',201,'Acc Due Date','ACC_DUE_DATE','','','','PK059658','DATE','','','Acc Due Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_OPEN_ORDERS_TEST_V','ACTION_DATE',201,'Action Date','ACTION_DATE','','','','PK059658','DATE','','','Action Date','','','');
--Inserting View Components for EIS_XXWC_PO_OPEN_ORDERS_TEST_V
--Inserting View Component Joins for EIS_XXWC_PO_OPEN_ORDERS_TEST_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Open Purchase Orders Listing - WC Test
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_ins.lov( 201,'select agent_name buyer_name from po_agents_v','','EIS_PO_BUYER_LOV','List of Values for Buyer','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'select poh.segment1 PO_NUMBER,HOU.name OPERATING_UNIT FROM PO_HEADERS_ALL POH,HR_OPERATING_UNITS HOU
where POH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=POH.ORG_ID)','','EIS_PO_PURCHASE_ORDER_NUM_LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select poh.segment1 PO_NUMBER,HOU.name OPERATING_UNIT FROM PO_HEADERS_ALL POH,HR_OPERATING_UNITS HOU
where POH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=POH.ORG_ID)','','EIS_PO_PURCHASE_ORDER_NUM_LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','');
xxeis.eis_rs_ins.lov( 201,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Open Purchase Orders Listing - WC Test
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_utility.delete_report_rows( 'Open Purchase Orders Listing - WC Test' );
--Inserting Report - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_ins.r( 201,'Open Purchase Orders Listing - WC Test','','White Cap Open Purchase Orders Listing - WC Test Report','','','','PK059658','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','Y','','','PK059658','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'AMOUNT_DUE','Amount Due','Amount Due','','~~~','default','','31','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'AMOUNT_ORDERED','Amount Ordered','Amount Ordered','','~~~','default','','29','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'AMOUNT_RECEIVED','Amount Received','Amount Received','','~~~','default','','30','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'BUYER_NAME','Buyer Name','Buyer Name','','','default','','6','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'ITEM','Item','Item','','','default','','20','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','21','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'LINE_NUM','Line','Line Num','','~~~','default','','36','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'NOTE_TO_RECEIVER','Note To Receiver','Note To Receiver','','','default','','10','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'PO_CREATION_DATE','Po Creation Date','Po Creation Date','','','default','','16','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'PO_NUMBER','Po Number','Po Number','','','default','','9','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'PO_STATUS','Po Status','Po Status','','','default','','23','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'PO_SUPPLIER_NAME','Po Supplier Name','Po Supplier Name','','','default','','7','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'PO_TYPE','Po Type','Po Type','','','default','','32','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'PO_UNIT_PRICE','Po Price','Po Unit Price','','$~T~D~2','default','','28','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'QUANTITY_DUE','Qty Due','Quantity Due','','~T~D~0','default','','26','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'QUANTITY_ORDERED','Qty Ordered','Quantity Ordered','','~T~D~0','default','','24','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'QUANTITY_RECEIVED','Qty Received','Quantity Received','','~T~D~0','default','','25','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'REQUIRED_BY','Required By','Required By','','','default','','37','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'REQ_CREATED_ON','Req Created On','Req Created On','','','default','','39','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'REQ_NUM','Req Num','Req Num','','','default','','38','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'REV','Rev','Rev','','~~~','default','','35','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'REVISED_DATE','Po Revision Date','Revised Date','','','default','','17','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'SUPPLIER_SITE','Supplier Site','Supplier Site','','','default','','34','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'UNIT_MEAS_LOOKUP_CODE','UOM','Unit Meas Lookup Code','','','default','','33','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'SHIP_NEED_BY_DATE','Line Need By Date','Ship Need By Date','','','default','','18','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'ORG_PO_LINES','Org PO Lines','Org Po Lines','','~T~D~0','default','','27','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'HEADER_NEED_BY_DATE','Header Need By Date','Header Need By Date','','','default','','19','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'LATE','Late','Header Need By Date','VARCHAR2','','default','','40','Y','','','','','','','CASE WHEN trunc(EXPOOV.LINE_PROMISE_DATE) < TRUNC(SYSDATE) THEN ''Y'' ELSE ''N'' END','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'SHIP_VIA','Ship Via','Ship Via','','','default','','22','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'SUPPLIER_CONTACT_PHONE','Supplier Contact Phone','Supplier Contact Phone','','','default','','8','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'SHIP_TO','Ship To','Ship To','','','default','','2','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'HEADER_PROMISE_DATE','Header Promise Date','Header Promise Date','','','default','','4','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'LINE_PROMISE_DATE','Line Promise Date','Line Promise Date','','','default','','5','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'DIRECT/DROP - Y/N','DIRECT/DROP - Y/N','Line Promise Date','VARCHAR2','','default','','3','Y','','','','','','','case when (EXPOOV.ATTRIBUTE2= ''DIRECT'' AND EXPOOV.ORG_CODE <> EXPOOV.SHIP_TO) OR EXPOOV.SHIP_TO IS NULL Then ''Y'' ELSE ''N'' END','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'ORG_CODE','Org Code','Org Code','','','default','','1','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'ACCEPTANCE_DUE_DATE','Acceptance Due Date','Acceptance Due Date','','','default','','12','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'ACCEPTANCE_TYPE','Latest Acceptance Type','Acceptance Type','','','default','','15','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'ACCEPTED_BY','Accepted By (buyer name)','Accepted By','','','default','','14','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'ACCEPTED_FLAG','Acceptance Applied Y/N','Accepted Flag','','','default','','11','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
xxeis.eis_rs_ins.rc( 'Open Purchase Orders Listing - WC Test',201,'ACTION_DATE','Date Acceptance Applied','Action Date','','','default','','13','N','','','','','','','','PK059658','N','N','','EIS_XXWC_PO_OPEN_ORDERS_TEST_V','','');
--Inserting Report Parameters - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_ins.rp( 'Open Purchase Orders Listing - WC Test',201,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','HDS White Cap - Org','VARCHAR2','N','Y','2','','Y','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Purchase Orders Listing - WC Test',201,'Organization','Organization','ORG_CODE','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Purchase Orders Listing - WC Test',201,'PO Date From','PO Date From','PO_CREATION_DATE','>=','','','DATE','N','Y','3','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Purchase Orders Listing - WC Test',201,'PO Date To','PO Date To','PO_CREATION_DATE','<=','','','DATE','N','Y','4','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Purchase Orders Listing - WC Test',201,'Supplier','Supplier','PO_SUPPLIER_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','5','','Y','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Purchase Orders Listing - WC Test',201,'PO Number From','PO Number From','PO_NUMBER','>=','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','6','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Purchase Orders Listing - WC Test',201,'PO Number To','PO Number To','PO_NUMBER','<=','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','7','','N','CONSTANT','PK059658','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Open Purchase Orders Listing - WC Test',201,'Buyer','Buyer','BUYER_NAME','IN','EIS_PO_BUYER_LOV','','VARCHAR2','N','Y','8','','Y','CONSTANT','PK059658','Y','N','','','');
--Inserting Report Conditions - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_ins.rcn( 'Open Purchase Orders Listing - WC Test',201,'ORG_CODE','IN',':Organization','','','N','2','N','PK059658');
xxeis.eis_rs_ins.rcn( 'Open Purchase Orders Listing - WC Test',201,'OPERATING_UNIT','IN',':Operating Unit','','','Y','3','Y','PK059658');
xxeis.eis_rs_ins.rcn( 'Open Purchase Orders Listing - WC Test',201,'PO_CREATION_DATE','>=',':PO Date From','','','Y','4','Y','PK059658');
xxeis.eis_rs_ins.rcn( 'Open Purchase Orders Listing - WC Test',201,'PO_CREATION_DATE','<=',':PO Date To','','','Y','5','Y','PK059658');
xxeis.eis_rs_ins.rcn( 'Open Purchase Orders Listing - WC Test',201,'PO_SUPPLIER_NAME','IN',':Supplier','','','Y','6','Y','PK059658');
xxeis.eis_rs_ins.rcn( 'Open Purchase Orders Listing - WC Test',201,'PO_NUMBER','>=',':PO Number From','','','Y','7','Y','PK059658');
xxeis.eis_rs_ins.rcn( 'Open Purchase Orders Listing - WC Test',201,'PO_NUMBER','<=',':PO Number To','','','Y','8','Y','PK059658');
xxeis.eis_rs_ins.rcn( 'Open Purchase Orders Listing - WC Test',201,'BUYER_NAME','IN',':Buyer','','','Y','9','Y','PK059658');
xxeis.eis_rs_ins.rcn( 'Open Purchase Orders Listing - WC Test',201,'','','','','AND EXPOOV.QUANTITY_DUE > 0
AND (EXPOOV.PO_STATUS NOT IN( ''Closed For Invoice'' ,''Closed For Receiving''))
AND EXPOOV.SHIP_CLOSED_CODE != ''CLOSED FOR RECEIVING''
AND ( ''All'' IN (:Organization) OR (ORG_CODE IN (:Organization)))','Y','0','','PK059658');
--Inserting Report Sorts - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_ins.rs( 'Open Purchase Orders Listing - WC Test',201,'PO_NUMBER','','PK059658','','');
--Inserting Report Triggers - Open Purchase Orders Listing - WC Test
--Inserting Report Templates - Open Purchase Orders Listing - WC Test
--Inserting Report Portals - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_ins.r_port( 'Open Purchase Orders Listing - WC Test','XXWC_PUR_TOP_RPTS','201','Open Purchase Orders Listing - WC Test','Open Purchase Order report','OA.jsp?page=/eis/oracle/apps/xxeis/reporting/webui/EISLaunchPG&EisProduct=Reporting&Portal=Yes&mod=Purchasing','','Pivot Excel,EXCEL,','CONC','N','PK059658');
--Inserting Report Dashboards - Open Purchase Orders Listing - WC Test
--Inserting Report Security - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','201','','50983',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','201','','50621',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','201','','50893',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','201','','50910',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','201','','50892',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','201','','50921',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','201','','50850',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','','FC004766','',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','20005','','50900',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','','10011678','',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','200','','50905',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','200','','50904',201,'PK059658','','');
xxeis.eis_rs_ins.rsec( 'Open Purchase Orders Listing - WC Test','201','','51369',201,'PK059658','','');
--Inserting Report Pivots - Open Purchase Orders Listing - WC Test
xxeis.eis_rs_ins.rpivot( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','1','2,0|1,1,0','1,1,1,0|None|2');
--Inserting Report Pivot Details For Pivot - Open PO'S By Line Count
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','BUYER_NAME','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','PO_SUPPLIER_NAME','ROW_FIELD','','','3','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','PO_NUMBER','ROW_FIELD','','','2','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','NOTE_TO_RECEIVER','ROW_FIELD','','','6','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','LATE','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','ORG_PO_LINES','DATA_FIELD','COUNT','Count of PO Lines','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','PO_STATUS','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','SUPPLIER_CONTACT_PHONE','ROW_FIELD','','','4','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','LINE_PROMISE_DATE','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''S By Line Count','ACCEPTED_FLAG','ROW_FIELD','','','5','','');
--Inserting Report Summary Calculation Columns For Pivot- Open PO'S By Line Count
xxeis.eis_rs_ins.rpivot( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','2','2,0|1,1,0','1,1,1,0|None|2');
--Inserting Report Pivot Details For Pivot - Open PO's By Qty Received
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','BUYER_NAME','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','LATE','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','QUANTITY_ORDERED','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','QUANTITY_RECEIVED','DATA_FIELD','SUM','','2','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','PO_NUMBER','ROW_FIELD','','','2','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','PO_SUPPLIER_NAME','ROW_FIELD','','','3','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','NOTE_TO_RECEIVER','ROW_FIELD','','','6','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','PO_STATUS','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','SUPPLIER_CONTACT_PHONE','ROW_FIELD','','','4','0','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','SHIP_VIA','PAGE_FIELD','','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','LINE_PROMISE_DATE','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Open Purchase Orders Listing - WC Test',201,'Open PO''s By Qty Received','ACCEPTED_FLAG','ROW_FIELD','','','5','','');
--Inserting Report Summary Calculation Columns For Pivot- Open PO's By Qty Received
END;
/
set scan on define on
