--Report Name            : WC AR Customer Site Inactivation Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for WC AR Customer Site Inactivation Report
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_EIS_AR_INACT_SITES_DTLS_C
xxeis.eis_rs_ins.v( 'XXWC_EIS_AR_INACT_SITES_DTLS_C',222,'','','','','SA059956','XXEIS','Xxwc Eis Ar Inact Sites Dtls C','XEAISDC','','');
--Delete View Columns for XXWC_EIS_AR_INACT_SITES_DTLS_C
xxeis.eis_rs_utility.delete_view_rows('XXWC_EIS_AR_INACT_SITES_DTLS_C',222,FALSE);
--Inserting View Columns for XXWC_EIS_AR_INACT_SITES_DTLS_C
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','AMOUNT_DUE_REMAINING',222,'Amount Due Remaining','AMOUNT_DUE_REMAINING','','','','SA059956','NUMBER','','','Amount Due Remaining','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','ORDER_DATE',222,'Order Date','ORDER_DATE','','','','SA059956','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','LAST_PAYMENT_DATE',222,'Last Payment Date','LAST_PAYMENT_DATE','','','','SA059956','DATE','','','Last Payment Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','LAST_SALE_DATE',222,'Last Sale Date','LAST_SALE_DATE','','','','SA059956','DATE','','','Last Sale Date','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','SITE_CLASSIFICATION',222,'Site Classification','SITE_CLASSIFICATION','','','','SA059956','VARCHAR2','','','Site Classification','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','PROFILE_CLASS',222,'Profile Class','PROFILE_CLASS','','','','SA059956','VARCHAR2','','','Profile Class','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','COLLECTOR',222,'Collector','COLLECTOR','','','','SA059956','VARCHAR2','','','Collector','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','LOCATION',222,'Location','LOCATION','','','','SA059956','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','LOCATION_NUMBER',222,'Location Number','LOCATION_NUMBER','','','','SA059956','VARCHAR2','','','Location Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','ACCOUNT_NAME',222,'Account Name','ACCOUNT_NAME','','','','SA059956','VARCHAR2','','','Account Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_EIS_AR_INACT_SITES_DTLS_C','ACCOUNT_NUMBER',222,'Account Number','ACCOUNT_NUMBER','','','','SA059956','VARCHAR2','','','Account Number','','','');
--Inserting View Components for XXWC_EIS_AR_INACT_SITES_DTLS_C
--Inserting View Component Joins for XXWC_EIS_AR_INACT_SITES_DTLS_C
END;
/
set scan on define on
prompt Creating Report LOV Data for WC AR Customer Site Inactivation Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - WC AR Customer Site Inactivation Report
xxeis.eis_rs_ins.lov( 222,'select distinct (attribute1) from ar.hz_cust_site_uses_all
where org_id = 162','','XXWC Site Classification','Job Class/Site Classification','DM027741',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for WC AR Customer Site Inactivation Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC AR Customer Site Inactivation Report
xxeis.eis_rs_utility.delete_report_rows( 'WC AR Customer Site Inactivation Report' );
--Inserting Report - WC AR Customer Site Inactivation Report
xxeis.eis_rs_ins.r( 222,'WC AR Customer Site Inactivation Report','','Customers that have no activity that will be deactivated when the program runs in Oracle.','','','','SA059956','XXWC_EIS_AR_INACT_SITES_DTLS_C','Y','','','SA059956','','N','White Cap Reports','','CSV,HTML,EXCEL,','N');
--Inserting Report Columns - WC AR Customer Site Inactivation Report
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'ACCOUNT_NAME','Account Name','Account Name','','','default','','2','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'ACCOUNT_NUMBER','Account Number','Account Number','','','default','','1','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'AMOUNT_DUE_REMAINING','Amount Due Remaining','Amount Due Remaining','','~~~','default','','11','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'COLLECTOR','Collector','Collector','','','default','','5','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'LAST_PAYMENT_DATE','Last Payment Date','Last Payment Date','','','default','','9','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'LAST_SALE_DATE','Last Sale Date','Last Sale Date','','','default','','8','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'LOCATION','Location','Location','','','default','','4','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'LOCATION_NUMBER','Location Number','Location Number','','','default','','3','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'ORDER_DATE','Order Date','Order Date','','','default','','10','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'PROFILE_CLASS','Profile Class','Profile Class','','','default','','6','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
xxeis.eis_rs_ins.rc( 'WC AR Customer Site Inactivation Report',222,'SITE_CLASSIFICATION','Site Classification','Site Classification','','','default','','7','N','','','','','','','','SA059956','N','N','','XXWC_EIS_AR_INACT_SITES_DTLS_C','','');
--Inserting Report Parameters - WC AR Customer Site Inactivation Report
xxeis.eis_rs_ins.rp( 'WC AR Customer Site Inactivation Report',222,'Class','Site Classification','SITE_CLASSIFICATION','IN','XXWC Site Classification','JOB','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC AR Customer Site Inactivation Report',222,'Month','Month','','IN','','11','NUMERIC','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','','');
xxeis.eis_rs_ins.rp( 'WC AR Customer Site Inactivation Report',222,'AsofDate','As of Date','','IN','','','DATE','Y','Y','3','','N','CURRENT_DATE','SA059956','Y','N','','','');
--Inserting Report Conditions - WC AR Customer Site Inactivation Report
--Inserting Report Sorts - WC AR Customer Site Inactivation Report
xxeis.eis_rs_ins.rs( 'WC AR Customer Site Inactivation Report',222,'ACCOUNT_NUMBER','ASC','SA059956','','');
xxeis.eis_rs_ins.rs( 'WC AR Customer Site Inactivation Report',222,'LOCATION_NUMBER','ASC','SA059956','','');
--Inserting Report Triggers - WC AR Customer Site Inactivation Report
xxeis.eis_rs_ins.rt( 'WC AR Customer Site Inactivation Report',222,'begin
XXEIS.EIS_RS_XXWC_AR_UTIL_PKG.inactive_site_pre_trig(:Class, :Month, :AsofDate);
end;','B','Y','SA059956');
--Inserting Report Templates - WC AR Customer Site Inactivation Report
--Inserting Report Portals - WC AR Customer Site Inactivation Report
--Inserting Report Dashboards - WC AR Customer Site Inactivation Report
--Inserting Report Security - WC AR Customer Site Inactivation Report
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','51208',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','50894',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','50992',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','50993',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','50854',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','50880',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','50853',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','50879',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','50877',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','222','','50878',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'WC AR Customer Site Inactivation Report','20005','','50897',222,'SA059956','','');
--Inserting Report Pivots - WC AR Customer Site Inactivation Report
END;
/
set scan on define on
