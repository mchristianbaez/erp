--Report Name            : Payment Remittance
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_885_AOPSUB_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_885_AOPSUB_V
xxeis.eis_rsc_ins.v( 'XXEIS_885_AOPSUB_V',200,'Paste SQL View for ACH File','1.0','','','10012196','APPS','ACH File View','X8AV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_885_AOPSUB_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_885_AOPSUB_V',200,FALSE);
--Inserting Object Columns for XXEIS_885_AOPSUB_V
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','SEGMENT1',200,'','','','','','10012196','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','VENDOR_NAME',200,'','','','','','10012196','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','CHECK_NUMBER',200,'','','','','','10012196','NUMBER','','','Check Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','CHECK_DATE',200,'','','','','','10012196','DATE','','','Check Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','INVOICE_NUM',200,'','','','','','10012196','VARCHAR2','','','Invoice Num','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','INVOICE_DATE',200,'','','','','','10012196','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','INVOICE_AMOUNT',200,'','','','~T~D~2','','10012196','NUMBER','','','Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','DISCOUNT_TAKEN',200,'','','','~T~D~2','','10012196','NUMBER','','','Discount Taken','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','AMOUNT_PAID',200,'','','','~T~D~2','','10012196','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_885_AOPSUB_V','DESCRIPTION',200,'','','','','','10012196','VARCHAR2','','','Description','','','','US');
--Inserting Object Components for XXEIS_885_AOPSUB_V
--Inserting Object Component Joins for XXEIS_885_AOPSUB_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report Payment Remittance
prompt Creating Report Data for Payment Remittance
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Payment Remittance
xxeis.eis_rsc_utility.delete_report_rows( 'Payment Remittance' );
--Inserting Report - Payment Remittance
xxeis.eis_rsc_ins.r( 200,'Payment Remittance','','This report will give the detailed remittance information using the supplied criteria.','','','','10012196','XXEIS_885_AOPSUB_V','Y','','SELECT ap.ap_suppliers.segment1, ap.ap_suppliers.vendor_name, ap.ap_checks_all.check_number
, ap.ap_checks_all.check_date, ap.ap_invoices_all.invoice_num, ap.ap_invoices_all.invoice_date
, ap.ap_invoices_all.invoice_amount, ap.ap_invoice_payments_all.discount_taken
, ap.ap_invoices_all.amount_paid, ap.ap_invoices_all.description

FROM ap.ap_invoices_all, ap.ap_invoice_payments_all, ap.ap_checks_all, ap.ap_suppliers

WHERE ap.ap_invoices_all.invoice_id = ap.ap_invoice_payments_all.invoice_id
 AND ap.ap_invoice_payments_all.check_id = ap.ap_checks_all.check_id
 AND ap.ap_invoices_all.vendor_id = ap.ap_suppliers.vendor_id
 AND ap.ap_invoices_all.org_id = 162
','10012196','','N','Payments','','CSV,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - Payment Remittance
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'SEGMENT1','Supplier Num','','','','','','1','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'VENDOR_NAME','Supplier Name','','','','','','2','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'CHECK_NUMBER','Check Number','','','','','','3','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'CHECK_DATE','Check Date','','','','','','4','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'INVOICE_NUM','Invoice Num','','','','','','5','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'INVOICE_DATE','Invoice Date','','','','','','6','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'INVOICE_AMOUNT','Invoice Amount','','','','','','7','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'DISCOUNT_TAKEN','Discount Taken','','','','','','8','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'AMOUNT_PAID','Amount Paid','','','','','','9','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Payment Remittance',200,'DESCRIPTION','Description','','','','','','10','N','','','','','','','','10012196','N','N','','XXEIS_885_AOPSUB_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Payment Remittance
xxeis.eis_rsc_ins.rp( 'Payment Remittance',200,'Check Date From','Check Date From','CHECK_DATE','>=','','','DATE','N','Y','1','','Y','CONSTANT','10012196','Y','N','','Start Date','','XXEIS_885_AOPSUB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Payment Remittance',200,'Check Date To','Check Date To','CHECK_DATE','<=','','','DATE','N','Y','2','','Y','CONSTANT','10012196','Y','N','','End Date','','XXEIS_885_AOPSUB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Payment Remittance',200,'Supplier Num','Supplier Num','SEGMENT1','IN','','','NUMERIC','N','Y','5','Y','Y','CONSTANT','10012196','Y','N','','','','XXEIS_885_AOPSUB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Payment Remittance',200,'Check Number From','Check Number From','CHECK_NUMBER','>=','','','NUMERIC','N','Y','3','','Y','CONSTANT','10012196','Y','N','','','','XXEIS_885_AOPSUB_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Payment Remittance',200,'Check Number To','Check Number To','CHECK_NUMBER','<=','','','NUMERIC','N','Y','4','','Y','CONSTANT','10012196','Y','N','','','','XXEIS_885_AOPSUB_V','','','US','');
--Inserting Dependent Parameters - Payment Remittance
--Inserting Report Conditions - Payment Remittance
xxeis.eis_rsc_ins.rcnh( 'Payment Remittance',200,'CHECK_DATE >= :Check Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date From','','','','','XXEIS_885_AOPSUB_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'Payment Remittance','CHECK_DATE >= :Check Date From ');
xxeis.eis_rsc_ins.rcnh( 'Payment Remittance',200,'CHECK_DATE <= :Check Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_DATE','','Check Date To','','','','','XXEIS_885_AOPSUB_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'Payment Remittance','CHECK_DATE <= :Check Date To ');
xxeis.eis_rsc_ins.rcnh( 'Payment Remittance',200,'CHECK_NUMBER >= :Check Number From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_NUMBER','','Check Number From','','','','','XXEIS_885_AOPSUB_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',200,'Payment Remittance','CHECK_NUMBER >= :Check Number From ');
xxeis.eis_rsc_ins.rcnh( 'Payment Remittance',200,'CHECK_NUMBER <= :Check Number To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CHECK_NUMBER','','Check Number To','','','','','XXEIS_885_AOPSUB_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',200,'Payment Remittance','CHECK_NUMBER <= :Check Number To ');
xxeis.eis_rsc_ins.rcnh( 'Payment Remittance',200,'SEGMENT1 IN :Supplier Num ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT1','','Supplier Num','','','','','XXEIS_885_AOPSUB_V','','','','','','IN','Y','Y','','','','','1',200,'Payment Remittance','SEGMENT1 IN :Supplier Num ');
--Inserting Report Sorts - Payment Remittance
xxeis.eis_rsc_ins.rs( 'Payment Remittance',200,'CHECK_DATE','ASC','10012196','1','');
xxeis.eis_rsc_ins.rs( 'Payment Remittance',200,'CHECK_NUMBER','ASC','10012196','2','');
xxeis.eis_rsc_ins.rs( 'Payment Remittance',200,'INVOICE_NUM','ASC','10012196','3','');
--Inserting Report Triggers - Payment Remittance
--inserting report templates - Payment Remittance
--Inserting Report Portals - Payment Remittance
--inserting report dashboards - Payment Remittance
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Payment Remittance','200','XXEIS_885_AOPSUB_V','XXEIS_885_AOPSUB_V','N','');
--inserting report security - Payment Remittance
xxeis.eis_rsc_ins.rsec( 'Payment Remittance','200','','XXWC_PAY_NO_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'Payment Remittance','200','','XXWC_PAY_W_CALENDAR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'Payment Remittance','200','','XXWC_PAY_DISBURSE',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'Payment Remittance','200','','XXWC_PAYABLES_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'Payment Remittance','200','','XXWC_PAY_MANAGER',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'Payment Remittance','200','','XXWC_PAY_VENDOR_BANK_DETAILS',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'Payment Remittance','200','','XXWC_PAY_VENDOR_MSTR',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'Payment Remittance','200','','XXWC_PAY_VENDOR_MSTR_INQUIRY',200,'10012196','','','');
xxeis.eis_rsc_ins.rsec( 'Payment Remittance','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'10012196','','','');
--Inserting Report Pivots - Payment Remittance
xxeis.eis_rsc_ins.rpivot( 'Payment Remittance',200,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'Payment Remittance',200,'Pivot','SEGMENT1','ROW_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Payment Remittance',200,'Pivot','INVOICE_AMOUNT','DATA_FIELD','SUM','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Payment Remittance',200,'Pivot','DISCOUNT_TAKEN','DATA_FIELD','SUM','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- Payment Remittance
xxeis.eis_rsc_ins.rv( 'Payment Remittance','','Payment Remittance','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
