--Report Name            : Inventory Sales and Reorder(ISR) Datamart QA
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Inventory Sales and Reorder(ISR) Datamart QA
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_835_LWVGQS_V
xxeis.eis_rs_ins.v( 'XXEIS_835_LWVGQS_V',201,'Paste SQL View for Inventory Sales and Reorder Datamart','1.0','','','10011289','APPS','Inventory Sales and Reorder Datamart View','X8LV','','');
--Delete View Columns for XXEIS_835_LWVGQS_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_835_LWVGQS_V',201,FALSE);
--Inserting View Columns for XXEIS_835_LWVGQS_V
xxeis.eis_rs_ins.vc( 'XXEIS_835_LWVGQS_V','CNT',201,'','','','','','10011289','NUMBER','','','Cnt','','','');
--Inserting View Components for XXEIS_835_LWVGQS_V
--Inserting View Component Joins for XXEIS_835_LWVGQS_V
END;
/
set scan on define on
prompt Creating Report Data for Inventory Sales and Reorder(ISR) Datamart QA
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory Sales and Reorder(ISR) Datamart QA
xxeis.eis_rs_utility.delete_report_rows( 'Inventory Sales and Reorder(ISR) Datamart QA' );
--Inserting Report - Inventory Sales and Reorder(ISR) Datamart QA
xxeis.eis_rs_ins.r( 201,'Inventory Sales and Reorder(ISR) Datamart QA','','Datamart for Inventory Sales and Reorder Report','','','','10011289','XXEIS_835_LWVGQS_V','Y','','select count(*) cnt from xxeis.eis_xxwc_po_isr_tab_total
','10011289','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Inventory Sales and Reorder(ISR) Datamart QA
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder(ISR) Datamart QA',201,'CNT','Cnt','','','','','','','','Y','','','','','','','10011289','','','','XXEIS_835_LWVGQS_V','','');
--Inserting Report Parameters - Inventory Sales and Reorder(ISR) Datamart QA
--Inserting Report Conditions - Inventory Sales and Reorder(ISR) Datamart QA
--Inserting Report Sorts - Inventory Sales and Reorder(ISR) Datamart QA
--Inserting Report Triggers - Inventory Sales and Reorder(ISR) Datamart QA
xxeis.eis_rs_ins.rt( 'Inventory Sales and Reorder(ISR) Datamart QA',201,'Begin
/*xxeis.eis_po_xxwc_isr_qa_pkg.main(); */
XXEIS.eis_po_xxwc_isr_pkg_v2.main;
end;','B','Y','10011289');
--Inserting Report Templates - Inventory Sales and Reorder(ISR) Datamart QA
--Inserting Report Portals - Inventory Sales and Reorder(ISR) Datamart QA
--Inserting Report Dashboards - Inventory Sales and Reorder(ISR) Datamart QA
--Inserting Report Security - Inventory Sales and Reorder(ISR) Datamart QA
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder(ISR) Datamart QA','20005','','50861',201,'10011289','','');
--Inserting Report Pivots - Inventory Sales and Reorder(ISR) Datamart QA
END;
/
set scan on define on
