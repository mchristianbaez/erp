--Report Name            : WC Role Responsibilities
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_HDS_WC_ROLE_RESP_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_HDS_WC_ROLE_RESP_V
xxeis.eis_rsc_ins.v( 'XXEIS_HDS_WC_ROLE_RESP_V',85000,'','','','','AB063501','XXEIS','Xxeis Hds Wc Role Resp V','XHWRRV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_HDS_WC_ROLE_RESP_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_HDS_WC_ROLE_RESP_V',85000,FALSE);
--Inserting Object Columns for XXEIS_HDS_WC_ROLE_RESP_V
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','UPDATED_BY',85000,'Updated By','UPDATED_BY','','','','AB063501','VARCHAR2','','','Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','CREATED_BY',85000,'Created By','CREATED_BY','','','','AB063501','VARCHAR2','','','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','END_DATE',85000,'End Date','END_DATE','','','','AB063501','DATE','','','End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','START_DATE',85000,'Start Date','START_DATE','','','','AB063501','DATE','','','Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','SOURCE',85000,'Source','SOURCE','','','','AB063501','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','RESPONSIBILITY_KEY',85000,'Responsibility Key','RESPONSIBILITY_KEY','','','','AB063501','VARCHAR2','','','Responsibility Key','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','EMAIL_ADDRESS',85000,'Email Address','EMAIL_ADDRESS','','','','AB063501','VARCHAR2','','','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','RESPONSIBILITY_NAME',85000,'Responsibility Name','RESPONSIBILITY_NAME','','','','AB063501','VARCHAR2','','','Responsibility Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','ROLE_CODE',85000,'Role Code','ROLE_CODE','','','','AB063501','VARCHAR2','','','Role Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','DESCRIPTION',85000,'Description','DESCRIPTION','','','','AB063501','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_HDS_WC_ROLE_RESP_V','USER_NAME',85000,'User Name','USER_NAME','','','','AB063501','VARCHAR2','','','User Name','','','','US');
--Inserting Object Components for XXEIS_HDS_WC_ROLE_RESP_V
--Inserting Object Component Joins for XXEIS_HDS_WC_ROLE_RESP_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report WC Role Responsibilities
prompt Creating Report Data for WC Role Responsibilities
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(85000);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC Role Responsibilities
xxeis.eis_rsc_utility.delete_report_rows( 'WC Role Responsibilities' );
--Inserting Report - WC Role Responsibilities
xxeis.eis_rsc_ins.r( 85000,'WC Role Responsibilities','','List all WC Role reference users and the assigned responsibilities','','','','AB063501','XXEIS_HDS_WC_ROLE_RESP_V','Y','','','AB063501','','N','Audit Reports','','EXCEL,Pivot Excel,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - WC Role Responsibilities
xxeis.eis_rsc_ins.rc( 'WC Role Responsibilities',85000,'CREATED_BY','Created By','Created By','','','default','','6','','Y','','','','','','','AB063501','N','N','','XXEIS_HDS_WC_ROLE_RESP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Role Responsibilities',85000,'DESCRIPTION','Role Name','Description','','','default','','2','','Y','','','','','','','AB063501','N','N','','XXEIS_HDS_WC_ROLE_RESP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Role Responsibilities',85000,'RESPONSIBILITY_KEY','Responsibility Key','Responsibility Key','','','default','','4','','Y','','','','','','','AB063501','N','N','','XXEIS_HDS_WC_ROLE_RESP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Role Responsibilities',85000,'RESPONSIBILITY_NAME','Responsibility Name','Responsibility Name','','','default','','3','','Y','','','','','','','AB063501','N','N','','XXEIS_HDS_WC_ROLE_RESP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Role Responsibilities',85000,'START_DATE','Start Date','Start Date','','','default','','5','','Y','','','','','','','AB063501','N','N','','XXEIS_HDS_WC_ROLE_RESP_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'WC Role Responsibilities',85000,'USER_NAME','User Name','User Name','','','default','','1','','Y','','','','','','','AB063501','N','N','','XXEIS_HDS_WC_ROLE_RESP_V','','','','US','');
--Inserting Report Parameters - WC Role Responsibilities
xxeis.eis_rsc_ins.rp( 'WC Role Responsibilities',85000,'Role Name','Description','DESCRIPTION','LIKE','','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','AB063501','Y','N','','','','XXEIS_HDS_WC_ROLE_RESP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Role Responsibilities',85000,'Responsibility Key','Responsibility Key','RESPONSIBILITY_KEY','LIKE','','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','AB063501','Y','N','','','','XXEIS_HDS_WC_ROLE_RESP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Role Responsibilities',85000,'Responsibility Name','Responsibility Name','RESPONSIBILITY_NAME','LIKE','','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','AB063501','Y','N','','','','XXEIS_HDS_WC_ROLE_RESP_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Role Responsibilities',85000,'Reference User Name','Login ID','USER_NAME','LIKE','','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','AB063501','Y','N','','','','XXEIS_HDS_WC_ROLE_RESP_V','','','US','');
--Inserting Dependent Parameters - WC Role Responsibilities
--Inserting Report Conditions - WC Role Responsibilities
xxeis.eis_rsc_ins.rcnh( 'WC Role Responsibilities',85000,'XHWRRV.DESCRIPTION LIKE :Role Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DESCRIPTION','','Role Name','','','','','XXEIS_HDS_WC_ROLE_RESP_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC Role Responsibilities','XHWRRV.DESCRIPTION LIKE :Role Name ');
xxeis.eis_rsc_ins.rcnh( 'WC Role Responsibilities',85000,'XHWRRV.RESPONSIBILITY_KEY LIKE :Responsibility Key ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RESPONSIBILITY_KEY','','Responsibility Key','','','','','XXEIS_HDS_WC_ROLE_RESP_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC Role Responsibilities','XHWRRV.RESPONSIBILITY_KEY LIKE :Responsibility Key ');
xxeis.eis_rsc_ins.rcnh( 'WC Role Responsibilities',85000,'XHWRRV.RESPONSIBILITY_NAME LIKE :Responsibility Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','RESPONSIBILITY_NAME','','Responsibility Name','','','','','XXEIS_HDS_WC_ROLE_RESP_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC Role Responsibilities','XHWRRV.RESPONSIBILITY_NAME LIKE :Responsibility Name ');
xxeis.eis_rsc_ins.rcnh( 'WC Role Responsibilities',85000,'XHWRRV.USER_NAME LIKE :Reference User Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','USER_NAME','','Reference User Name','','','','','XXEIS_HDS_WC_ROLE_RESP_V','','','','','','LIKE','Y','Y','','','','','1',85000,'WC Role Responsibilities','XHWRRV.USER_NAME LIKE :Reference User Name ');
--Inserting Report Sorts - WC Role Responsibilities
xxeis.eis_rsc_ins.rs( 'WC Role Responsibilities',85000,'DESCRIPTION','ASC','AB063501','1','');
xxeis.eis_rsc_ins.rs( 'WC Role Responsibilities',85000,'RESPONSIBILITY_NAME','ASC','AB063501','2','');
--Inserting Report Triggers - WC Role Responsibilities
--inserting report templates - WC Role Responsibilities
--Inserting Report Portals - WC Role Responsibilities
--inserting report dashboards - WC Role Responsibilities
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC Role Responsibilities','85000','XXEIS_HDS_WC_ROLE_RESP_V','XXEIS_HDS_WC_ROLE_RESP_V','N','');
--inserting report security - WC Role Responsibilities
xxeis.eis_rsc_ins.rsec( 'WC Role Responsibilities','20005','','XXWC_IT_SECURITY_ADMINISTRATOR',85000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'WC Role Responsibilities','20005','','XXWC_IT_FUNC_CONFIGURATOR',85000,'AB063501','','','');
xxeis.eis_rsc_ins.rsec( 'WC Role Responsibilities','20005','','XXWC_VIEW_ALL_EIS_REPORTS',85000,'AB063501','','','');
--Inserting Report Pivots - WC Role Responsibilities
xxeis.eis_rsc_ins.rpivot( 'WC Role Responsibilities',85000,'Pivot','1','0,0|1,1,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Role Responsibilities',85000,'Pivot','DESCRIPTION','ROW_FIELD','','','1','0','');
xxeis.eis_rsc_ins.rpivot_dtls( 'WC Role Responsibilities',85000,'Pivot','RESPONSIBILITY_NAME','ROW_FIELD','','','2','0','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- WC Role Responsibilities
xxeis.eis_rsc_ins.rv( 'WC Role Responsibilities','','WC Role Responsibilities','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 85000');
END IF;
END;
/
