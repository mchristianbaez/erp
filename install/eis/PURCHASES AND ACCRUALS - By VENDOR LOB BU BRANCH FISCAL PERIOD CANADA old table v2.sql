--Report Name            : PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_1721492_PUMRXD_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1721492_PUMRXD_V
xxeis.eis_rsc_ins.v( 'XXEIS_1721492_PUMRXD_V',682,'Paste SQL View for PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','1.0','','','DV003828','APPS','PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2 View','X1PV123','','Y','VIEW','US','','');
--Delete Object Columns for XXEIS_1721492_PUMRXD_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1721492_PUMRXD_V',682,FALSE);
--Inserting Object Columns for XXEIS_1721492_PUMRXD_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','PERIOD_ID',682,'','','','','','DV003828','NUMBER','','','Period Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','FISCAL_PERIOD',682,'','','','','','DV003828','VARCHAR2','','','Fiscal Period','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','MVID',682,'','','','','','DV003828','VARCHAR2','','','Mvid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','CUST_ID',682,'','','','','','DV003828','NUMBER','','','Cust Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','VENDOR',682,'','','','','','DV003828','VARCHAR2','','','Vendor','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','LOB',682,'','','','','','DV003828','VARCHAR2','','','Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','BU',682,'','','','','','DV003828','VARCHAR2','','','Bu','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','LOB_BRANCH',682,'','','','','','DV003828','VARCHAR2','','','Lob Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','FISCAL_PURCHASES',682,'','','','','','DV003828','NUMBER','','','Fiscal Purchases','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','ACCRUAL_PURCHASES',682,'','','','','','DV003828','NUMBER','','','Accrual Purchases','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','REBATE',682,'','','','','','DV003828','NUMBER','','','Rebate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','COOP',682,'','','','','','DV003828','NUMBER','','','Coop','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','TOTAL',682,'','','','~T~D~2','','DV003828','NUMBER','','','Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','AGREEMENT_YEAR',682,'','','','','','DV003828','NUMBER','','','Agreement Year','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1721492_PUMRXD_V','POSTED_FIN_LOCATION',682,'','','','','','DV003828','VARCHAR2','','','Posted Fin Location','','','','US');
--Inserting Object Components for XXEIS_1721492_PUMRXD_V
--Inserting Object Component Joins for XXEIS_1721492_PUMRXD_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
xxeis.eis_rsc_ins.lov( 682,'select distinct nvl(attribute7, ''NULL'') agreement_year
from apps.QP_LIST_HEADERS_VL','','LOV AGREEMENT_YEAR','LOV attribute7 from apps.QP_LIST_HEADERS_VL  table is the agreement year','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_MVID''','','LOV VENDOR_NAME','VENDOR NAME FROM ar.HZ_PARTIES
','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct period_name
from xla_ae_headers','','LOV PERIOD_NAME','LOV PERIOD NAME FROM xla_ae_headers table','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_LOB''
and party_name not in (''PLUMBING null'', ''INDUSTRIAL PVF'')','','HDS LOB_NAME','LOV party_name From ar.HZ_PARTIES is the LOB','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'SELECT distinct hp.party_name LOB
 FROM apps.hz_parties hp
WHERE  hp.attribute1=''HDS_BU''
and ( hp.party_name in (''ELECTRICAL'',''UTILISERV'')
      or hp.status =''A'')','','HDS BU','BU FROM apps.hz_parties it''s party_name = LOB BUSINESS UNIT','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
xxeis.eis_rsc_utility.delete_report_rows( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2' );
--Inserting Report - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
xxeis.eis_rsc_ins.r( 682,'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','','Spend (including Fiscal Purchases) and Accruals by  Agreement Year, Fiscal Period, MVID, Vendor, LOB, BU, LOB branch, Posted Fin Location
Table used : XXCUS.XXCUS_YTD_INCOME_B, OZF_RESALE_LINES_ALL','','','','KP059700','XXEIS_1721492_PUMRXD_V','Y','','  SELECT AGREEMENT_YEAR ,
  PERIOD_ID ,
  FISCAL_PERIOD ,
  MVID ,
  cust_id,
  VENDOR ,
  LOB ,
  BU ,
  LOB_BRANCH ,
  POSTED_FIN_LOCATION, 
(SELECT SUM(SELLING_PRICE * QUANTITY)
                            FROM APPS.OZF_RESALE_LINES_ALL, ozf.ozf_time_day td
                            WHERE 1=1
                            and SOLD_FROM_CUST_ACCOUNT_ID =M.CUST_ID
                            AND DATE_ORDERED = TD.REPORT_DATE
                            and substr(td.month_id,0,4) =m.agreement_year
                            AND M.LOB_BRANCH =LINE_ATTRIBUTE6
                            AND M.LOB =BILL_TO_PARTY_NAME
                            AND M.BU =LINE_ATTRIBUTE2
                            and td.ent_period_id =m.period_id
                            ) fiscal_Purchases,
   ACCRUAL_PURCHASES ,
   REBATE ,
  COOP ,
  TOTAL
  from 
(
SELECT AGREEMENT_YEAR ,
  PERIOD_ID ,
  FISCAL_PERIOD ,
  MVID ,
  cust_id,
  VENDOR ,
  LOB ,
  BU ,
  LOB_BRANCH ,
  POSTED_FIN_LOCATION,
  0 FISCAL_PURCHASES ,
   MAX(PURCHASES) ACCRUAL_PURCHASES ,
  SUM(REBATE) REBATE ,
  SUM(COOP) COOP ,
  SUM(TOTAL_ACCRUALS) TOTAL
FROM
  (
  SELECT 
    AGREEMENT_YEAR AGREEMENT_YEAR ,
    PERIOD_ID ,
    FISCAL_PERIOD ,
    MVID ,
    ofu_cust_account_id cust_id,
    c.party_name VENDOR ,
    LOB ,
    BU ,
    BRANCH LOB_BRANCH ,
    POSTED_FIN_LOCATION,
    OFFER_NAME ,
    SUM(
    CASE
      WHEN UTILIZATION_TYPE=''ACCRUAL''
      THEN Purchases
      ELSE 0
    END) PURCHASES ,
    SUM(
    CASE
      WHEN REBATE_TYPE IN (''REBATE'')
      THEN accrual_AMOUNT
      ELSE 0
    END) REBATE ,
    SUM(
    CASE
      WHEN REBATE_TYPE IN (''COOP'')
      THEN accrual_AMOUNT
      ELSE 0
    END) COOP ,
    SUM(accrual_AMOUNT) TOTAL_ACCRUALS
    
    FROM 
      XXCUS.XXCUS_YTD_INCOME_B ,
      XXCUS.XXCUS_REBATE_CUSTOMERS C
  WHERE 1=1
  AND OFU_CUST_ACCOUNT_ID=C.CUSTOMER_ID
  AND C.PARTY_ATTRIBUTE1   =''HDS_MVID''
 
  GROUP BY 
   AGREEMENT_YEAR  ,
    PERIOD_ID ,
    FISCAL_PERIOD , 
    MVID ,
    ofu_cust_account_id ,
    c.party_name  ,
    LOB ,
    BU ,
    BRANCH  ,
    posted_fin_location,
    OFFER_NAME 
  ) 
GROUP BY PERIOD_ID ,
  AGREEMENT_YEAR ,
  FISCAL_PERIOD ,
   MVID ,
  cust_id,
  VENDOR ,
  LOB ,
  BU ,
  posted_fin_location,
  LOB_BRANCH 
 )
 M
','KP059700','','N','ADHOC - SUMMARY ACCRUAL DETAILS','','CSV,EXCEL,','N','','','','','','N','APPS','US','','','','');
--Inserting Report Columns - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'LOB_BRANCH','LOB Branch','','','','default','','8','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'ACCRUAL_PURCHASES','Accrual Purchases','','','~,~.~2','default','','11','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'MVID','MVID','','','','default','','4','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'VENDOR','Vendor','','','','default','','5','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'FISCAL_PURCHASES','Fiscal Purchases','','','~,~.~2','default','','10','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'LOB','LOB','','','','default','','6','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'PERIOD_ID','Period ID','','','~~~','default','','2','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'FISCAL_PERIOD','Fiscal Period','','','','default','','3','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'TOTAL','Total','','','~,~.~2','default','','14','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'COOP','COOP','','','~,~.~2','default','','13','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'BU','BU','','','','default','','7','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'REBATE','Rebate','','','~,~.~2','default','','12','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'POSTED_FIN_LOCATION','Posted Fin Location','','','','default','','9','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'AGREEMENT_YEAR','Agreement Year','','','~~~','default','','1','N','Y','','','','','','','KP059700','N','N','','XXEIS_1721492_PUMRXD_V','','','','US','');
--Inserting Report Parameters - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
xxeis.eis_rsc_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'Fiscal Period','','FISCAL_PERIOD','IN','LOV PERIOD_NAME','','VARCHAR2','Y','Y','2','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1721492_PUMRXD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'Lob Name','','LOB','IN','HDS LOB_NAME','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1721492_PUMRXD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'Vendor','','VENDOR','IN','LOV VENDOR_NAME','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1721492_PUMRXD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'Agreement Year','','AGREEMENT_YEAR','IN','LOV AGREEMENT_YEAR','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1721492_PUMRXD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'Lob Branch','','LOB_BRANCH','IN','','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1721492_PUMRXD_V','','','US','');
xxeis.eis_rsc_ins.rp( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'Bu','','BU','IN','HDS BU','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','KP059700','Y','N','','','','XXEIS_1721492_PUMRXD_V','','','US','');
--Inserting Dependent Parameters - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
--Inserting Report Conditions - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
xxeis.eis_rsc_ins.rcnh( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'AGREEMENT_YEAR IN :Agreement Year ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','AGREEMENT_YEAR','','Agreement Year','','','','','XXEIS_1721492_PUMRXD_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','AGREEMENT_YEAR IN :Agreement Year ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'BU IN :Bu ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BU','','Bu','','','','','XXEIS_1721492_PUMRXD_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','BU IN :Bu ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'FISCAL_PERIOD IN :Fiscal Period ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','FISCAL_PERIOD','','Fiscal Period','','','','','XXEIS_1721492_PUMRXD_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','FISCAL_PERIOD IN :Fiscal Period ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'X1PV123.LOB IN :Lob Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOB','','Lob Name','','','','','XXEIS_1721492_PUMRXD_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','X1PV123.LOB IN :Lob Name ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'LOB_BRANCH IN :Lob Branch ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOB_BRANCH','','Lob Branch','','','','','XXEIS_1721492_PUMRXD_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','LOB_BRANCH IN :Lob Branch ');
xxeis.eis_rsc_ins.rcnh( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'VENDOR IN :Vendor ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR','','Vendor','','','','','XXEIS_1721492_PUMRXD_V','','','','','','IN','Y','Y','','','','','1',682,'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','VENDOR IN :Vendor ');
--Inserting Report Sorts - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
xxeis.eis_rsc_ins.rs( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'MVID','ASC','KP059700','1','');
xxeis.eis_rsc_ins.rs( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'LOB','ASC','KP059700','2','');
xxeis.eis_rsc_ins.rs( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'BU','ASC','KP059700','3','');
xxeis.eis_rsc_ins.rs( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2',682,'LOB_BRANCH','ASC','KP059700','4','');
--Inserting Report Triggers - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
--inserting report templates - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
--Inserting Report Portals - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
--inserting report dashboards - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','682','XXEIS_1721492_PUMRXD_V','XXEIS_1721492_PUMRXD_V','N','');
--inserting report security - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
xxeis.eis_rsc_ins.rsec( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','682','','XXCUS_TM_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','682','','XXCUS_TM_ADMIN_USER',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','682','','XXCUS_TM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','682','','XXCUS_TM_ADM_FORMS_RESP',682,'KP059700','','','');
xxeis.eis_rsc_ins.rsec( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','','AB063501','',682,'KP059700','','N','');
--Inserting Report Pivots - PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
--Inserting Report   Version details- PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2
xxeis.eis_rsc_ins.rv( 'PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','','PURCHASES AND ACCRUALS - By VENDOR, LOB, BU, BRANCH, FISCAL PERIOD (CANADA)  - old table v2','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
