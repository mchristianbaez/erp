--Report Name            : HDS GSC iPro Open Purchase Orders Listing
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rsc_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating Object Data EIS_PO_OPEN_ORDERS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_PO_OPEN_ORDERS_V
xxeis.eis_rsc_ins.v( 'EIS_PO_OPEN_ORDERS_V',201,'This view provides details on purchase order lines and invoice information.','','','','XXEIS_RS_ADMIN','XXEIS','EIS PO Open Orders','EPOO','','','VIEW','US','Y','');
--Delete Object Columns for EIS_PO_OPEN_ORDERS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_PO_OPEN_ORDERS_V',201,FALSE);
--Inserting Object Columns for EIS_PO_OPEN_ORDERS_V
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_CREATION_DATE',201,'Standard Who column - date when this record was created','PO_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_HEADERS_ALL','CREATION_DATE','PO Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_NUMBER',201,'Document number - Combined with type_lookup_code and org_id to form unique key','PO_NUMBER','','','PO Purchase Order Number LOV','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','SEGMENT1','PO Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_TYPE',201,'Type of the document. (Combined with segment1and org_id to form unique key; References PO_DOCUMENT_TYPES_ALL_B.document_subtype)','PO_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','TYPE_LOOKUP_CODE','PO Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_SUPPLIER_NAME',201,'Supplier name','PO_SUPPLIER_NAME','','','PO Supplier Name LOV','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDORS','VENDOR_NAME','PO Supplier Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_4_WAY_MATCH',201,'Indicates whether shipment must be inspected before the invoice is paid','PO_4_WAY_MATCH','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINE_LOCATIONS_ALL','INSPECTION_REQUIRED_FLAG','PO 4 Way Match','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_3_WAY_MATCH',201,'Indicates whether shipment must be received before the invoice is paid','PO_3_WAY_MATCH','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINE_LOCATIONS_ALL','RECEIPT_REQUIRED_FLAG','PO 3 Way Match','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_UNIT_PRICE',201,'PO Unit Price','PO_UNIT_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','PO Unit Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_ENABLED',201,'Key Flexfield enabled flag','PO_ENABLED','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ENABLED_FLAG','PO Enabled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_STATUS',201,'Authorization status of the purchase order (References PO_LOOKUP_CODES.lookup_code with LOOKUP_TYPE=AUTHORIZATION STATUS)','PO_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','AUTHORIZATION_STATUS','PO Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_CHARGE_ACCOUNT',201,'PO Charge Account','PO_CHARGE_ACCOUNT','','','PO Accounts LOV','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','PO Charge Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_TERMS',201,'Name of payment term','PO_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','AP_TERMS_TL','NAME','PO Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ACCRUAL_ACCOUNT',201,'Accrual Account','ACCRUAL_ACCOUNT~ACCRUAL_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Accrual Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','AMOUNT_BILLED',201,'Amount Billed','AMOUNT_BILLED~AMOUNT_BILLED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Amount Billed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','AMOUNT_CANCELLED',201,'Amount Cancelled','AMOUNT_CANCELLED~AMOUNT_CANCELLED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Amount Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','AMOUNT_ORDERED',201,'Amount Ordered','AMOUNT_ORDERED~AMOUNT_ORDERED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Amount Ordered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','AMOUNT_RECEIVED',201,'Amount Received','AMOUNT_RECEIVED~AMOUNT_RECEIVED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Amount Received','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','BUYER_NAME',201,'Concatenation of last name, title, first name, middle names and the name the person is known by.','BUYER_NAME~BUYER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PER_ALL_PEOPLE_F','FULL_NAME','Buyer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','CLOSED_CODE',201,'Describes the closure status of the document (References PO_LOOKUP_CODES.lookup_code with LOOKUP_TYPE=DOCUMENT STATE)','CLOSED_CODE~CLOSED_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','CLOSED_CODE','Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','CLOSED_DATE',201,'Date the document was closed','CLOSED_DATE~CLOSED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_HEADERS_ALL','CLOSED_DATE','Closed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','COMPANY',201,'Accounting books name','COMPANY~COMPANY','','','','XXEIS_RS_ADMIN','VARCHAR2','GL_SETS_OF_BOOKS','NAME','Company','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','CURRENCY_CODE',201,'Unique identifier for the currency (References FND_CURRENCIES.currency_code)','CURRENCY_CODE~CURRENCY_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','CURRENCY_CODE','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ITEM',201,'Key flexfield segment','ITEM~ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_SYSTEM_ITEMS_B','SEGMENT1','Item','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ITEM_DESCRIPTION',201,'Item description. (Defaulted from MTL_SYSTEM_ITEMS_TL.description.)','ITEM_DESCRIPTION~ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','ITEM_DESCRIPTION','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_TYPE',201,'Document line type','LINE_TYPE~LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINE_TYPES_TL','LINE_TYPE','Line Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_UNIT',201,'Unit of measure for the quantity ordered. (References MTL_UNITS_OF_MEASURE.unit_of_measure.)','LINE_UNIT~LINE_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','UNIT_MEAS_LOOKUP_CODE','Line Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','OPEN_INVOICE_AMOUNT',201,'Open Invoice Amount','OPEN_INVOICE_AMOUNT~OPEN_INVOICE_AMOUNT','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Open Invoice Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','QUANTITY_BILLED',201,'Quantity invoiced by Oracle Payables against the distribution','QUANTITY_BILLED~QUANTITY_BILLED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_DISTRIBUTIONS_ALL','QUANTITY_BILLED','Quantity Billed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','QUANTITY_CANCELLED',201,'Quantity cancelled','QUANTITY_CANCELLED~QUANTITY_CANCELLED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','QUANTITY_CANCELLED','Quantity Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','QUANTITY_DUE',201,'Quantity Due','QUANTITY_DUE~QUANTITY_DUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_DISTRIBUTIONS_ALL','(ORDERED-CANCELLED-DELIVERED)','Quantity Due','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','QUANTITY_ORDERED',201,'Quantity ordered on the distribution','QUANTITY_ORDERED~QUANTITY_ORDERED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_DISTRIBUTIONS_ALL','QUANTITY_ORDERED','Quantity Ordered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','QUANTITY_RECEIVED',201,'Quantity delivered against the distribution','QUANTITY_RECEIVED~QUANTITY_RECEIVED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_DISTRIBUTIONS_ALL','QUANTITY_DELIVERED','Quantity Received','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SUPPLIER_SITE',201,'Site code name','SUPPLIER_SITE~SUPPLIER_SITE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_SITES_ALL','VENDOR_SITE_CODE','Supplier Site','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','VARIANCE_ACCOUNT',201,'Variance Account','VARIANCE_ACCOUNT~VARIANCE_ACCOUNT','','','','XXEIS_RS_ADMIN','VARCHAR2','CALCULATION COLUMN','CALCULATION COLUMN','Variance Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REQ_NUM',201,'Requisition number','REQ_NUM','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','SEGMENT1','Req Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REQ_CREATED_ON',201,'Standard Who column','REQ_CREATED_ON','','','','XXEIS_RS_ADMIN','DATE','PO_REQUISITION_HEADERS_ALL','CREATION_DATE','Req Created On','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REQUIRED_BY',201,'Date the requisition is needed internally','REQUIRED_BY','','','','XXEIS_RS_ADMIN','DATE','PO_REQUISITION_LINES_ALL','NEED_BY_DATE','Required By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','UNIT_MEAS_LOOKUP_CODE',201,'Unit of measure for the quantity ordered. (References MTL_UNITS_OF_MEASURE.unit_of_measure.)','UNIT_MEAS_LOOKUP_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','UNIT_MEAS_LOOKUP_CODE','Unit Meas Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','UNIT_PRICE',201,'Unit price for the line. (Defaulted from MTL_SYSTEM_ITEMS_B.list_price_per_unit, converted to the documents currency.)','UNIT_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','UNIT_PRICE','Unit Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','QUANTITY',201,'Quantity ordered','QUANTITY','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','QUANTITY','Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REQ_QUANTITY_CANCELLED',201,'Quantity cancelled','REQ_QUANTITY_CANCELLED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','QUANTITY_CANCELLED','Req Quantity Cancelled','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REQ_QUANTITY_DELIVERED',201,'Quantity delivered to date','REQ_QUANTITY_DELIVERED','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','QUANTITY_DELIVERED','Req Quantity Delivered','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SOURCE_TYPE_CODE',201,'Requisition source type of item','SOURCE_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','SOURCE_TYPE_CODE','Source Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','OPERATING_UNIT',201,'Translated name of the organization','OPERATING_UNIT','','','PO Operating Unit List LOV','XXEIS_RS_ADMIN','VARCHAR2','HR_ALL_ORGANIZATION_UNITS_TL','NAME','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_NUM',201,'Line number','LINE_NUM~LINE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','LINE_NUM','Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_HEADER_ID',201,'Document header unique identifier','PO_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_HEADERS_ALL','PO_HEADER_ID','PO Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SUPPLIER_CONTACT_PHONE',201,'Contact phone number','SUPPLIER_CONTACT_PHONE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','PHONE','Supplier Contact Phone','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_SUMMARY',201,'Key Flexfield summary flag','PO_SUMMARY','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','SUMMARY_FLAG','PO Summary','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SET_OF_BOOKS_ID',201,'Accounting books defining column','SET_OF_BOOKS_ID~SET_OF_BOOKS_ID','','','','XXEIS_RS_ADMIN','NUMBER','GL_SETS_OF_BOOKS','SET_OF_BOOKS_ID','Set Of Books Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SHIP_TO_LOCATION_ID',201,'Ship-to location foreign key.','SHIP_TO_LOCATION_ID~SHIP_TO_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_LOCATIONS_ALL','SHIP_TO_LOCATION_ID','Ship To Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','VENDOR_ID',201,'Supplier unique identifier','VENDOR_ID~VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDORS','VENDOR_ID','Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','VENDOR_SITE_ID',201,'Supplier site unique identifier','VENDOR_SITE_ID~VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDOR_SITES_ALL','VENDOR_SITE_ID','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_LOCATION_ID',201,'Document shipment schedule unique identifier','LINE_LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','LINE_LOCATION_ID','Line Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SUPPLIER_CONTACT_FIRST_NAME',201,'Contact first name','SUPPLIER_CONTACT_FIRST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','FIRST_NAME','Supplier Contact First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SUPPLIER_CONTACT_LAST_NAME',201,'Contact last name','SUPPLIER_CONTACT_LAST_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','LAST_NAME','Supplier Contact Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SUPPLIER_CONTACT_AREA_CODE',201,'Contact phone number area code','SUPPLIER_CONTACT_AREA_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_VENDOR_CONTACTS','AREA_CODE','Supplier Contact Area Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ACCEPTANCE_DUE_DATE',201,'Date by which the supplier should accept the purchase order','ACCEPTANCE_DUE_DATE~ACCEPTANCE_DUE_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_HEADERS_ALL','ACCEPTANCE_DUE_DATE','Acceptance Due Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ACCEPTANCE_REQUIRED',201,'Indicates whether acceptance from the supplier is required or not','ACCEPTANCE_REQUIRED~ACCEPTANCE_REQUIRED','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','ACCEPTANCE_REQUIRED_FLAG','Acceptance Required','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ACCRUAL_CCID',201,'Key flexfield combination defining column','ACCRUAL_CCID~ACCRUAL_CCID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Accrual Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ACCRUE_ON_RECEIPT',201,'Indicates whether items are accrued upon receipt','ACCRUE_ON_RECEIPT~ACCRUE_ON_RECEIPT','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINE_LOCATIONS_ALL','ACCRUE_ON_RECEIPT_FLAG','Accrue On Receipt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','AMOUNT_DUE',201,'Amount Due','AMOUNT_DUE~AMOUNT_DUE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','CALCULATION COLUMN','CALCULATION COLUMN','Amount Due','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','APPROVED_DATE',201,'Date the purchase order was last approved','APPROVED_DATE~APPROVED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_HEADERS_ALL','APPROVED_DATE','Approved Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','CATEGORY',201,'Category','CATEGORY~CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','MTL_CATEGORIES_B','CONCATENATED_SEGMENTS','Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','CHARGE_CCID',201,'Key flexfield combination defining column','CHARGE_CCID~CHARGE_CCID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Charge Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','COMMENTS',201,'Descriptive comments for the document','COMMENTS~COMMENTS','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','COMMENTS','Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','CREATED_BY',201,'Standard Who column: the user who created this record. (References FND_USERS.user_id.)','CREATED_BY~CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','CREATED_BY','Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','DAYS_EARLY_RECEIPT_ALLOWED',201,'Days before planned receipt that item may be received','DAYS_EARLY_RECEIPT_ALLOWED~DAYS_EARLY_RECEIPT_ALLOWED','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','DAYS_EARLY_RECEIPT_ALLOWED','Days Early Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','DAYS_LATE_RECEIPT_ALLOWED',201,'Days after the planned receipt that item may be received','DAYS_LATE_RECEIPT_ALLOWED~DAYS_LATE_RECEIPT_ALLOWED','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','DAYS_LATE_RECEIPT_ALLOWED','Days Late Receipt Allowed','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','DISTRIBUTION_ID',201,'Document distribution unique identifier. Primary key for this table.','DISTRIBUTION_ID~DISTRIBUTION_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_DISTRIBUTIONS_ALL','PO_DISTRIBUTION_ID','Distribution Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','DISTRIBUTION_NUM',201,'Distribution number','DISTRIBUTION_NUM~DISTRIBUTION_NUM','','','','XXEIS_RS_ADMIN','NUMBER','PO_DISTRIBUTIONS_ALL','DISTRIBUTION_NUM','Distribution Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','END_DATE',201,'Key Flexfield end date','END_DATE~END_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_HEADERS_ALL','END_DATE_ACTIVE','End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','FOB',201,'Type of free-on-board terms for the document (References PO_LOOKUP_CODES.lookup_code with LOOKUP_TYPE=FOB)','FOB~FOB','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','FOB_LOOKUP_CODE','Fob','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','FREIGHT_TERMS',201,'Type of freight terms for the document (References PO_LOOKUP_CODES.lookup_code with LOOKUP_TYPE=FREIGHT TERMS)','FREIGHT_TERMS~FREIGHT_TERMS','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','FREIGHT_TERMS_LOOKUP_CODE','Freight Terms','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','HOLD',201,'Indicates whether the purchase order is on hold or not','HOLD~HOLD','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','USER_HOLD_FLAG','Hold','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','INVOICE_CLOSE_TOLERANCE',201,'Percentage tolerance within which a shipment is automatically closed for invoicing when billed','INVOICE_CLOSE_TOLERANCE~INVOICE_CLOSE_TOLERANCE','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','INVOICE_CLOSE_TOLERANCE','Invoice Close Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ITEM_REVISION',201,'Item revision. (References MTL_ITEM_REVISIONS_B.revision.)','ITEM_REVISION~ITEM_REVISION','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','ITEM_REVISION','Item Revision','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LAST_UPDATED_BY',201,'Standard Who column - the user who last updated this record (References FND_USERS.user_id)','LAST_UPDATED_BY~LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','PO_HEADERS_ALL','LAST_UPDATED_BY','Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LAST_UPDATE_DATE',201,'Standard Who column - date when this record was last updated','LAST_UPDATE_DATE~LAST_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_HEADERS_ALL','LAST_UPDATE_DATE','Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_CANCEL',201,'Indicates whether the line is cancelled or not','LINE_CANCEL~LINE_CANCEL','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','CANCEL_FLAG','Line Cancel','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_CANCEL_DATE',201,'Cancellation date','LINE_CANCEL_DATE~LINE_CANCEL_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINES_ALL','CANCEL_DATE','Line Cancel Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_CANCEL_REASON',201,'Cancellation reason provided by employee','LINE_CANCEL_REASON~LINE_CANCEL_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','CANCEL_REASON','Line Cancel Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_CLOSED_CODE',201,'Describes closure status of the line','LINE_CLOSED_CODE~LINE_CLOSED_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','CLOSED_CODE','Line Closed Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_CLOSED_DATE',201,'Date the line is closed','LINE_CLOSED_DATE~LINE_CLOSED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINES_ALL','CLOSED_DATE','Line Closed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_CLOSED_REASON',201,'Describes why the line is closed','LINE_CLOSED_REASON~LINE_CLOSED_REASON','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','CLOSED_REASON','Line Closed Reason','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_CREATED_BY',201,'Standard Who column: the user who created this record. (References FND_USERS.user_id.)','LINE_CREATED_BY~LINE_CREATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','CREATED_BY','Line Created By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_CREATION_DATE',201,'Standard Who column: date when this record was created','LINE_CREATION_DATE~LINE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINES_ALL','CREATION_DATE','Line Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_ID',201,'Document line unique identifier','LINE_ID~LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','PO_LINE_ID','Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_LAST_UPDATED_BY',201,'Standard Who column: the user who last updated this record. (References FND_USERS.user_id.)','LINE_LAST_UPDATED_BY~LINE_LAST_UPDATED_BY','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','LAST_UPDATED_BY','Line Last Updated By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_LAST_UPDATE_DATE',201,'Standard Who column: date when this record was last updated','LINE_LAST_UPDATE_DATE~LINE_LAST_UPDATE_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINES_ALL','LAST_UPDATE_DATE','Line Last Update Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_LIST_PRICE',201,'List price for the item on the line. (Defaulted from MTL_SYSTEM_ITEMS_B.list_price_per_unit.)','LINE_LIST_PRICE~LINE_LIST_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','LIST_PRICE_PER_UNIT','Line List Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','MARKET_PRICE',201,'Market price for the item on the line. (Defaulted from MTL_SYSTEM_ITEMS_B.market_price.)','MARKET_PRICE~MARKET_PRICE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','MARKET_PRICE','Market Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','NOTE_TO_AUTHORIZER',201,'Not currently used','NOTE_TO_AUTHORIZER~NOTE_TO_AUTHORIZER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','NOTE_TO_AUTHORIZER','Note To Authorizer','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','NOTE_TO_RECEIVER',201,'Note to the receiver of the purchase order','NOTE_TO_RECEIVER~NOTE_TO_RECEIVER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','NOTE_TO_RECEIVER','Note To Receiver','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','NOTE_TO_SUPPLIER',201,'Note to the supplier','NOTE_TO_SUPPLIER~NOTE_TO_SUPPLIER','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','NOTE_TO_VENDOR','Note To Supplier','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','OPEN_FOR',201,'QuickCode meaning','OPEN_FOR~OPEN_FOR','','','','XXEIS_RS_ADMIN','VARCHAR2','FND_LOOKUP_VALUES','MEANING','Open For','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_DATA_TYPE',201,'Purchase order number type','PO_DATA_TYPE~PO_DATA_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_SYSTEM_PARAMETERS_ALL','MANUAL_PO_NUM_TYPE','Po Data Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_NUMBER_RELEASE',201,'Release number','PO_NUMBER_RELEASE~PO_NUMBER_RELEASE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_RELEASES_ALL','RELEASE_NUM','Po Number Release','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PRICE_OVERRIDE',201,'Order shipment price or break price for blanket purchase orders, RFQs, and quotations','PRICE_OVERRIDE~PRICE_OVERRIDE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','PRICE_OVERRIDE','Price Override','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PRINTED_DATE',201,'Date the document was last printed','PRINTED_DATE~PRINTED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_HEADERS_ALL','PRINTED_DATE','Printed Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','QTY_RCV_TOLERANCE',201,'Maximum over-receipt tolerance percentage','QTY_RCV_TOLERANCE~QTY_RCV_TOLERANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','QTY_RCV_TOLERANCE','Qty Rcv Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','RATE',201,'Currency conversion rate. References GL_DAILY_CONVERSION_RATES_R10.CONVERSION_RATE.','RATE~RATE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_DISTRIBUTIONS_ALL','RATE','Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','RECEIVE_CLOSE_TOLERANCE',201,'Percentage tolerance within which a shipment is automatically closed for receiving','RECEIVE_CLOSE_TOLERANCE~RECEIVE_CLOSE_TOLERANCE','','~T~D~2','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','RECEIVE_CLOSE_TOLERANCE','Receive Close Tolerance','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','RELEASE_NUM',201,'Release number','RELEASE_NUM~RELEASE_NUM','','','','XXEIS_RS_ADMIN','NUMBER','PO_RELEASES_ALL','RELEASE_NUM','Release Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REV',201,'Document revision number','REV~REV','','','','XXEIS_RS_ADMIN','NUMBER','PO_HEADERS_ALL','REVISION_NUM','Rev','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REVISED_DATE',201,'Date the document was last revised','REVISED_DATE~REVISED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_HEADERS_ALL','REVISED_DATE','Revised Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SHIPMENT_NUM',201,'Shipment line number','SHIPMENT_NUM~SHIPMENT_NUM','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINE_LOCATIONS_ALL','SHIPMENT_NUM','Shipment Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SHIPMENT_TYPE',201,'Type of the shipment','SHIPMENT_TYPE~SHIPMENT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINE_LOCATIONS_ALL','SHIPMENT_TYPE','Shipment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SHIP_DIST',201,'Shipment line number','SHIP_DIST~SHIP_DIST','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINE_LOCATIONS_ALL','SHIPMENT_NUM','Ship Dist','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SHIP_LAST_ACCEPT_DATE',201,'Latest acceptable receipt date for the shipment','SHIP_LAST_ACCEPT_DATE~SHIP_LAST_ACCEPT_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINE_LOCATIONS_ALL','LAST_ACCEPT_DATE','Ship Last Accept Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SHIP_NEED_BY_DATE',201,'Need-by date for the shipment schedule','SHIP_NEED_BY_DATE~SHIP_NEED_BY_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINE_LOCATIONS_ALL','NEED_BY_DATE','Ship Need By Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SHIP_PROMISED_DATE',201,'Supplier promised delivery date','SHIP_PROMISED_DATE~SHIP_PROMISED_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_LINE_LOCATIONS_ALL','PROMISED_DATE','Ship Promised Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SHIP_TO_LOCATION',201,'Translated location name','SHIP_TO_LOCATION~SHIP_TO_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','HR_LOCATIONS_ALL_TL','LOCATION_CODE','Ship To Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SHIP_VIA',201,'Type of carrier to be used (References ORG_FREIGHT_TL.freight_code)','SHIP_VIA~SHIP_VIA','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_HEADERS_ALL','SHIP_VIA_LOOKUP_CODE','Ship Via','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','START_DATE',201,'Key Flexfield start date','START_DATE~START_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_HEADERS_ALL','START_DATE_ACTIVE','Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','TAXABLE_FLAG',201,'Indicates whether the document line is taxable or not','TAXABLE_FLAG~TAXABLE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','TAXABLE_FLAG','Taxable Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','TAX_NAME',201,'Tax code applying to the line. (References AP_TAX_CODES_ALL.name.)','TAX_NAME~TAX_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','TAX_NAME','Tax Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','TYPE_1099',201,'1099 type for the purchase order line','TYPE_1099~TYPE_1099','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_LINES_ALL','TYPE_1099','Type 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','VARIANCE_CCID',201,'Key flexfield combination defining column','VARIANCE_CCID~VARIANCE_CCID','','','','XXEIS_RS_ADMIN','NUMBER','GL_CODE_COMBINATIONS','CODE_COMBINATION_ID','Variance Ccid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','AGENT_ID',201,'Buyer unique identifier','AGENT_ID~AGENT_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_AGENTS','AGENT_ID','Agent Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','CATEGORY_ID',201,'Category identifier','CATEGORY_ID~CATEGORY_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_CATEGORIES_B','CATEGORY_ID','Category Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','EMPLOYEE_ID',201,'System generated person primary key from PER_PEOPLE_S.','EMPLOYEE_ID~EMPLOYEE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PER_ALL_PEOPLE_F','PERSON_ID','Employee Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','INVENTORY_ITEM_ID',201,'Inventory item identifier','INVENTORY_ITEM_ID~INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','INVENTORY_ITEM_ID','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LINE_TYPE_ID',201,'Line type unique identifier (References PO_LINE_TYPES_B.line_type_id.)','LINE_TYPE_ID~LINE_TYPE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_LINES_ALL','LINE_TYPE_ID','Line Type Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','LOCATION_ID',201,'System-generated primary key column.','LOCATION_ID~LOCATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','HR_LOCATIONS_ALL','LOCATION_ID','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','MSI_ORGANIZATION_ID',201,'Organization identifier','MSI_ORGANIZATION_ID~MSI_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','MTL_SYSTEM_ITEMS_B','ORGANIZATION_ID','Msi Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PO_RELEASE_ID',201,'Release unique identifier','PO_RELEASE_ID~PO_RELEASE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_RELEASES_ALL','PO_RELEASE_ID','Po Release Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PSP_ORG_ID',201,'Operating unit unique identifier','PSP_ORG_ID~PSP_ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_SYSTEM_PARAMETERS_ALL','ORG_ID','Psp Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PVC_VENDOR_SITE_ID',201,'Supplier contact unique identifier','PVC_VENDOR_SITE_ID~PVC_VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDOR_CONTACTS','VENDOR_CONTACT_ID','Pvc Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PVS_VENDOR_ID',201,'Supplier site unique identifier','PVS_VENDOR_ID~PVS_VENDOR_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDOR_SITES_ALL','VENDOR_SITE_ID','Pvs Vendor Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','TERM_ID',201,'Term identifier','TERM_ID~TERM_ID','','','','XXEIS_RS_ADMIN','NUMBER','AP_TERMS_TL','TERM_ID','Term Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','VENDOR_CONTACT_ID',201,'Supplier contact unique identifier','VENDOR_CONTACT_ID~VENDOR_CONTACT_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_VENDOR_CONTACTS','VENDOR_CONTACT_ID','Vendor Contact Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REQ_LINE_CREATION_DATE',201,'Standard Who column','REQ_LINE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','PO_REQUISITION_LINES_ALL','CREATION_DATE','Req Line Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SEQ',201,'Seq','SEQ','','','','XXEIS_RS_ADMIN','NUMBER','','','Seq','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','CANCEL_FLAG',201,'Indicates whether the requisition is cancelled or not','CANCEL_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_LINES_ALL','CANCEL_FLAG','Cancel Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','TRANSFERRED_TO_OE_FLAG',201,'Indicates whether an internal requisition has been transferred to Order Management','TRANSFERRED_TO_OE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','PO_REQUISITION_HEADERS_ALL','TRANSFERRED_TO_OE_FLAG','Transferred To Oe Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SOURCE_ORGANIZATION_ID',201,'Inventory source organization unique identifier','SOURCE_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','SOURCE_ORGANIZATION_ID','Source Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','DESTINATION_ORGANIZATION_ID',201,'Destination organization unique identifier','DESTINATION_ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','DESTINATION_ORGANIZATION_ID','Destination Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ITEM_ID',201,'Item unique identifier','ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','ITEM_ID','Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','PREPARER_ID',201,'Unique identifier of the employee who prepared the requisition','PREPARER_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_HEADERS_ALL','PREPARER_ID','Preparer Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REQUISITION_HEADER_ID',201,'Requisition header unique identifier','REQUISITION_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_HEADERS_ALL','REQUISITION_HEADER_ID','Requisition Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','REQUISITION_LINE_ID',201,'Requisition line unique identifier','REQUISITION_LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','PO_REQUISITION_LINES_ALL','REQUISITION_LINE_ID','Requisition Line Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','ORGANIZATION_ID',201,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SEQ#',201,'Seq#','SEQ#','','','','XXEIS_RS_ADMIN','NUMBER','','','Seq#','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','SET_OF_BOOKS',201,'Set Of Books','SET_OF_BOOKS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Set Of Books','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','VENDOR_NUMBER',201,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_PO_OPEN_ORDERS_V','COPYRIGHT',201,'Copyright','COPYRIGHT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Copyright','','','','US');
--Inserting Object Components for EIS_PO_OPEN_ORDERS_V
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','AP_TERMS',201,'AP_TERMS_TL','APT','APT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Payment Terms','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','GL_CODE_COMBINATIONS_KFV',201,'GL_CODE_COMBINATIONS','GCC','GCC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Charge Account','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','GL_CODE_COMBINATIONS_KFV',201,'GL_CODE_COMBINATIONS','GCC1','GCC1','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Accrual Account','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','GL_CODE_COMBINATIONS_KFV',201,'GL_CODE_COMBINATIONS','GCC2','GCC2','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Variance Account','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','GL_SETS_OF_BOOKS',201,'GL_SETS_OF_BOOKS','SOB','SOB','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Set Of Books','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','HR_LOCATIONS',201,'HR_LOCATIONS_ALL','HRL','HRL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Locations','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','MTL_CATEGORIES',201,'MTL_CATEGORIES_B','MCA','MCA','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Item Categories','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','MTL_SYSTEM_ITEMS',201,'MTL_SYSTEM_ITEMS_B','MSI','MSI','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Inventory Items','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_DISTRIBUTIONS',201,'PO_DISTRIBUTIONS_ALL','POD','POD','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchasing Distributions','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_DOCUMENT_TYPES',201,'PO_DOCUMENT_TYPES_ALL_B','PDT','PDT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Document Types','','','','','PDT','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_HEADERS',201,'PO_HEADERS_ALL','POH','POH','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchasing Headers','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_LINE_LOCATIONS',201,'PO_LINE_LOCATIONS_ALL','POLL','POLL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Open Purchasing Shipment Schedules','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_LINE_TYPES',201,'PO_LINE_TYPES_B','PLT','PLT','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Line Types','','','','','PLT','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_LINES',201,'PO_LINES_ALL','POL','POL','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchase Document Lines','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_RELEASES',201,'PO_RELEASES_ALL','POR1','POR1','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Purchasing Releases','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_SYSTEM_PARAMETERS',201,'PO_SYSTEM_PARAMETERS_ALL','PSP','PSP','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','System Parameters','','','','','PSP','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_VENDOR_CONTACTS',201,'PO_VENDOR_CONTACTS','PVC','PVC','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Vendor Contacts','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_VENDOR_SITES',201,'PO_VENDOR_SITES_ALL','PVS','PVS','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Supplier Sites','','','','','','Y',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_PO_OPEN_ORDERS_V','PO_VENDORS',201,'PO_VENDORS','PV','PV','XXEIS_RS_ADMIN','XXEIS_RS_ADMIN','-1','Vendors','','','','','','Y',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_PO_OPEN_ORDERS_V
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','AP_TERMS','APT',201,'EPOO.TERM_ID','=','APT.TERM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','GL_CODE_COMBINATIONS_KFV','GCC',201,'EPOO.CHARGE_CCID','=','GCC.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','GL_CODE_COMBINATIONS_KFV','GCC1',201,'EPOO.ACCRUAL_CCID','=','GCC1.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','GL_CODE_COMBINATIONS_KFV','GCC2',201,'EPOO.VARIANCE_CCID','=','GCC2.CODE_COMBINATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','GL_SETS_OF_BOOKS','SOB',201,'EPOO.SET_OF_BOOKS_ID','=','SOB.SET_OF_BOOKS_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','HR_LOCATIONS','HRL',201,'EPOO.LOCATION_ID','=','HRL.LOCATION_ID(+)','','','','Y','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','MTL_CATEGORIES','MCA',201,'EPOO.CATEGORY_ID','=','MCA.CATEGORY_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','MTL_SYSTEM_ITEMS','MSI',201,'EPOO.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','MTL_SYSTEM_ITEMS','MSI',201,'EPOO.MSI_ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_DISTRIBUTIONS','POD',201,'EPOO.DISTRIBUTION_ID','=','POD.PO_DISTRIBUTION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_DOCUMENT_TYPES','PDT',201,'EPOO.PDT_DOCUMENT_SUBTYPE','=','PDT.DOCUMENT_SUBTYPE(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_DOCUMENT_TYPES','PDT',201,'EPOO.PDT_DOCUMENT_TYPE_CODE','=','PDT.DOCUMENT_TYPE_CODE(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_DOCUMENT_TYPES','PDT',201,'EPOO.PDT_ORG_ID','=','PDT.ORG_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_HEADERS','POH',201,'EPOO.PO_HEADER_ID','=','POH.PO_HEADER_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_LINE_LOCATIONS','POLL',201,'EPOO.LINE_LOCATION_ID','=','POLL.LINE_LOCATION_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_LINE_TYPES','PLT',201,'EPOO.LINE_TYPE_ID','=','PLT.LINE_TYPE_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_LINES','POL',201,'EPOO.LINE_ID','=','POL.PO_LINE_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_RELEASES','POR1',201,'EPOO.PO_RELEASE_ID','=','POR1.PO_RELEASE_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_SYSTEM_PARAMETERS','PSP',201,'EPOO.PSP_ORG_ID','=','PSP.ORG_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_VENDOR_CONTACTS','PVC',201,'EPOO.VENDOR_CONTACT_ID','=','PVC.VENDOR_CONTACT_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_VENDOR_SITES','PVS',201,'EPOO.VENDOR_SITE_ID','=','PVS.VENDOR_SITE_ID(+)','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rsc_ins.vcj( 'EIS_PO_OPEN_ORDERS_V','PO_VENDORS','PV',201,'EPOO.VENDOR_ID','=','PV.VENDOR_ID(+)','','','','','XXEIS_RS_ADMIN');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report LOV Data for HDS GSC iPro Open Purchase Orders Listing
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.lov( 201,'select poh.segment1 PO_NUMBER,HOU.name OPERATING_UNIT FROM PO_HEADERS_ALL POH,HR_OPERATING_UNITS HOU
where POH.ORG_ID=HOU.ORGANIZATION_ID
and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=POH.ORG_ID)','','EIS_PO_PURCHASE_ORDER_NUM_LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'select vendor_name from po_vendors','','EIS_PO_SUPPLIER_LOV','','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT CONCATENATED_SEGMENTS
  FROM gl_code_combinations_kfv
 where CHART_OF_ACCOUNTS_ID in (
                select sob.CHART_OF_ACCOUNTS_ID
                  from GL_SETS_OF_BOOKS SOB
                 where  set_of_books_id in ( SELECT column_value from table(xxeis.EIS_RS_MO_MSOB_PKG.get_sob_id_lov)))
   and NVL(SUMMARY_FLAG,''N'') = UPPER(''N'')
order by CONCATENATED_SEGMENTS;','','EIS_GLACCOUNT_CONCAT_LOV','EIS_GLACCOUNT_CONCAT_LOV','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 201,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
prompt Creating Report Data for HDS GSC iPro Open Purchase Orders Listing
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(201);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_utility.delete_report_rows( 'HDS GSC iPro Open Purchase Orders Listing' );
--Inserting Report - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.r( 201,'HDS GSC iPro Open Purchase Orders Listing','','This report can be used to get the details of open purchase orders within a data range, supplier range or PO number range.

Created as part of project #20266.','','','','MM027735','EIS_PO_OPEN_ORDERS_V','Y','','','MM027735','','N','iProcurement','','CSV,Pivot Excel,EXCEL,','','','','','','','','','US','','','','');
--Inserting Report Columns - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'LINE_NUM','Line Num','Line number','','','','','12','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'AMOUNT_CANCELLED','Amount Cancelled','Amount Cancelled','','','','','14','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'AMOUNT_ORDERED','Amount Ordered','Amount Ordered','','','','','15','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'AMOUNT_RECEIVED','Amount Received','Amount Received','','','','','16','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'BUYER_NAME','Buyer Name','Concatenation of last name, title, first name, middle names and the name the person is known by.','','','','','17','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'CLOSED_CODE','Closed Code','Describes the closure status of the document (References PO_LOOKUP_CODES.lookup_code with LOOKUP_TYPE=DOCUMENT STATE)','','','','','18','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'CLOSED_DATE','Closed Date','Date the document was closed','','','','','19','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'COMPANY','Company','Accounting books name','','','','','1','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'CURRENCY_CODE','Currency Code','Unique identifier for the currency (References FND_CURRENCIES.currency_code)','','','','','20','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'ITEM','Item','Key flexfield segment','','','','','21','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'ITEM_DESCRIPTION','Item Description','Item description. (Defaulted from MTL_SYSTEM_ITEMS_TL.description.)','','','','','22','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'LINE_TYPE','Line Type','Document line type','','','','','23','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'LINE_UNIT','Line Unit','Unit of measure for the quantity ordered. (References MTL_UNITS_OF_MEASURE.unit_of_measure.)','','','','','24','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'OPEN_INVOICE_AMOUNT','Open Invoice Amount','Open Invoice Amount','','','','','25','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_3_WAY_MATCH','PO 3 Way Match','Indicates whether shipment must be received before the invoice is paid','','','','','26','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_4_WAY_MATCH','PO 4 Way Match','Indicates whether shipment must be inspected before the invoice is paid','','','','','27','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'QUANTITY_BILLED','Quantity Billed','Quantity invoiced by Oracle Payables against the distribution','','','','','28','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'QUANTITY_CANCELLED','Quantity Cancelled','Quantity cancelled','','','','','29','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'QUANTITY_DUE','Quantity Due','Quantity Due','','~T~D~2','','','30','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'QUANTITY_ORDERED','Quantity Ordered','Quantity ordered on the distribution','','~T~D~2','','','31','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'QUANTITY_RECEIVED','Quantity Received','Quantity delivered against the distribution','','~T~D~2','','','32','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'SUPPLIER_SITE','Supplier Site','Site code name','','','','','33','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'VARIANCE_ACCOUNT','Variance Account','Variance Account','','','','','34','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'ACCRUAL_ACCOUNT','Accrual Account','Accrual Account','','','','','35','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'UNIT_MEAS_LOOKUP_CODE','Unit Meas Lookup Code','Unit of measure for the quantity ordered. (References MTL_UNITS_OF_MEASURE.unit_of_measure.)','','','','','43','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'UNIT_PRICE','Unit Price','Unit price for the line. (Defaulted from MTL_SYSTEM_ITEMS_B.list_price_per_unit, converted to the documents currency.)','','','','','44','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_NUMBER','PO Number','Document number - Combined with type_lookup_code and org_id to form unique key','','','','','3','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_TYPE','PO Type','Type of the document. (Combined with segment1and org_id to form unique key; References PO_DOCUMENT_TYPES_ALL_B.document_subtype)','','','','','4','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_STATUS','PO Status','Authorization status of the purchase order (References PO_LOOKUP_CODES.lookup_code with LOOKUP_TYPE=AUTHORIZATION STATUS)','','','','','5','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_SUPPLIER_NAME','PO Supplier Name','Supplier name','','','','','7','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_TERMS','PO Terms','Name of payment term','','','','','8','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_UNIT_PRICE','PO Unit Price','PO Unit Price','','','','','9','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_CREATION_DATE','PO Creation Date','Standard Who column - date when this record was created','','','','','10','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_ENABLED','PO Enabled','Key Flexfield enabled flag','','','','','11','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_CHARGE_ACCOUNT','PO Charge Account','PO Charge Account','','','','','6','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'AMOUNT_BILLED','Amount Billed','Amount Billed','','','','','13','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'QUANTITY','Quantity','Quantity ordered','','~T~D~2','','','36','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'REQUIRED_BY','Required By','Date the requisition is needed internally','','','','','37','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'REQ_CREATED_ON','Req Created On','Standard Who column','','','','','38','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'REQ_NUM','Req Num','Requisition number','','','','','39','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'REQ_QUANTITY_CANCELLED','Req Quantity Cancelled','Quantity cancelled','','','','','40','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'REQ_QUANTITY_DELIVERED','Req Quantity Delivered','Quantity delivered to date','','','','','41','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'SOURCE_TYPE_CODE','Source Type Code','Requisition source type of item','','','','','42','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS GSC iPro Open Purchase Orders Listing',201,'OPERATING_UNIT','Operating Unit','Translated name of the organization','','','','','2','N','','','','','','','','MM027735','N','N','','EIS_PO_OPEN_ORDERS_V','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Open Purchase Orders Listing',201,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','''HD Supply Corp USD - Org''','VARCHAR2','N','Y','1','Y','Y','CONSTANT','MM027735','Y','N','','','','EIS_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO Date From','PO Date From','PO_CREATION_DATE','>=','','','DATE','N','Y','2','','N','CONSTANT','MM027735','Y','N','','','','EIS_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO Date To','PO Date To','PO_CREATION_DATE','<=','','','DATE','N','Y','3','','N','CONSTANT','MM027735','Y','N','','','','EIS_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO Number To','PO Number To','PO_NUMBER','<=','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','6','','N','CONSTANT','MM027735','Y','N','','','','EIS_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Open Purchase Orders Listing',201,'Supplier','Supplier','PO_SUPPLIER_NAME','IN','EIS_PO_SUPPLIER_LOV','','VARCHAR2','N','Y','4','','N','CONSTANT','MM027735','Y','N','','','','EIS_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO Number From','PO Number From','PO_NUMBER','>=','EIS_PO_PURCHASE_ORDER_NUM_LOV','','VARCHAR2','N','Y','5','','N','CONSTANT','MM027735','Y','N','','','','EIS_PO_OPEN_ORDERS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO Charge Account','PO Charge Account','PO_CHARGE_ACCOUNT','IN','EIS_GLACCOUNT_CONCAT_LOV','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MM027735','Y','Y','GCC','','','EIS_PO_OPEN_ORDERS_V','','','US','');
--Inserting Dependent Parameters - HDS GSC iPro Open Purchase Orders Listing
--Inserting Report Conditions - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Open Purchase Orders Listing',201,'OPERATING_UNIT IN :Operating Unit ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Operating Unit','','','','','EIS_PO_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Open Purchase Orders Listing','OPERATING_UNIT IN :Operating Unit ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_CHARGE_ACCOUNT IN :PO Charge Account ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_CHARGE_ACCOUNT','','PO Charge Account','','','','','EIS_PO_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Open Purchase Orders Listing','PO_CHARGE_ACCOUNT IN :PO Charge Account ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_CREATION_DATE >= :PO Date From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_CREATION_DATE','','PO Date From','','','','','EIS_PO_OPEN_ORDERS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro Open Purchase Orders Listing','PO_CREATION_DATE >= :PO Date From ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_CREATION_DATE <= :PO Date To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_CREATION_DATE','','PO Date To','','','','','EIS_PO_OPEN_ORDERS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro Open Purchase Orders Listing','PO_CREATION_DATE <= :PO Date To ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_NUMBER >= :PO Number From ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_NUMBER','','PO Number From','','','','','EIS_PO_OPEN_ORDERS_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro Open Purchase Orders Listing','PO_NUMBER >= :PO Number From ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_NUMBER <= :PO Number To ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_NUMBER','','PO Number To','','','','','EIS_PO_OPEN_ORDERS_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',201,'HDS GSC iPro Open Purchase Orders Listing','PO_NUMBER <= :PO Number To ');
xxeis.eis_rsc_ins.rcnh( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_SUPPLIER_NAME IN :Supplier ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PO_SUPPLIER_NAME','','Supplier','','','','','EIS_PO_OPEN_ORDERS_V','','','','','','IN','Y','Y','','','','','1',201,'HDS GSC iPro Open Purchase Orders Listing','PO_SUPPLIER_NAME IN :Supplier ');
--Inserting Report Sorts - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.rs( 'HDS GSC iPro Open Purchase Orders Listing',201,'PO_NUMBER','ASC','MM027735','1','');
--Inserting Report Triggers - HDS GSC iPro Open Purchase Orders Listing
--inserting report templates - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.r_tem( 'HDS GSC iPro Open Purchase Orders Listing','HDS GSC Open Purchase Orders Listing','Seeded template for HDS GSC Open Purchase Orders Listing','','','','','','','','','','','HDS GSC Open Purchase Orders Listing.rtf','MM027735','X','','','Y','Y','','');
xxeis.eis_rsc_ins.r_tem( 'HDS GSC iPro Open Purchase Orders Listing','HDS GSC Open Purchase Orders Listing','Seeded template for HDS GSC Open Purchase Orders Listing','','','','','','','','','','','HDS GSC Open Purchase Orders Listing.rtf','MM027735','X','','','Y','Y','','');
--Inserting Report Portals - HDS GSC iPro Open Purchase Orders Listing
--inserting report dashboards - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.R_dash( 'HDS GSC iPro Open Purchase Orders Listing','Dynamic 782','Dynamic 782','vertical stacked bar','large','Po Supplier Name','Po Supplier Name','Po Number','Po Number','Count','MM027735');
xxeis.eis_rsc_ins.R_dash( 'HDS GSC iPro Open Purchase Orders Listing','Open Purchase Orders Report','Open Purchase Orders Report','pie','large','PO Number','PO Number','PO Quantity Ordered','PO Quantity Ordered','Count','MM027735');
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS GSC iPro Open Purchase Orders Listing','201','EIS_PO_OPEN_ORDERS_V','EIS_PO_OPEN_ORDERS_V','N','');
--inserting report security - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','178','','SELF_SERVICES_CATALOG_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','178','','SLF_SRVC_PRCHSNG_5',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_TRNS_ENTRY_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_TRNS_ENTRY_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_SUPPLIER_MAINT_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_MGR_NOSUP_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_MGR_NOSUP_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_PYBLS_MNGR',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_PYABLS_MNGR_CAN',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_MANAGER',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_MGR_NOSUP_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_INQUIRY_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_INQUIRY_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_INQ_CANADA',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_DISBUREMTS_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_DISBURSEMTS_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_DISBURSEMTS_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_PAYABLES_CLOSE_GLBL',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_ADMIN_US_IWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_ADMIN_US_GSCIWO',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_ADMIN_US_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','200','','HDS_AP_ADMIN_CAD_GSC',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','101','','GNRL_LDGR_FSS',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','101','','XXCUS_GL_MANAGER_GLOBAL',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','101','','XXCUS_GL_MANAGER',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','101','','XXCUS_GL_INQUIRY',201,'MM027735','','','');
xxeis.eis_rsc_ins.rsec( 'HDS GSC iPro Open Purchase Orders Listing','101','','XXCUS_GL_ACCOUNTANT_USD',201,'MM027735','','','');
--Inserting Report Pivots - HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.rpivot( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','BUYER_NAME','ROW_FIELD','','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','COMPANY','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','QUANTITY_BILLED','DATA_FIELD','SUM','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','QUANTITY_CANCELLED','DATA_FIELD','SUM','','4','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','QUANTITY_ORDERED','DATA_FIELD','SUM','','1','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','QUANTITY_RECEIVED','DATA_FIELD','SUM','','2','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','PO_NUMBER','ROW_FIELD','','','3','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','PO_TYPE','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','PO_STATUS','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','PO_SUPPLIER_NAME','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','PO_CHARGE_ACCOUNT','PAGE_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'HDS GSC iPro Open Purchase Orders Listing',201,'Pivot','OPERATING_UNIT','ROW_FIELD','','','1','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
--Inserting Report   Version details- HDS GSC iPro Open Purchase Orders Listing
xxeis.eis_rsc_ins.rv( 'HDS GSC iPro Open Purchase Orders Listing','','HDS GSC iPro Open Purchase Orders Listing','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 201');
END IF;
END;
/
