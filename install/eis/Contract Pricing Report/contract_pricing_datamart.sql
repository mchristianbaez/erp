--Report Name            : Contract Pricing Datamart
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_CONT_MODI_DATAMART_V1
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_CONT_MODI_DATAMART_V1
Create or replace View XXEIS.EIS_XXWC_CONT_MODI_DATAMART_V1
(MODIFIER_NAME,CUSTOMER_NAME,QUALIFIER_ATTRIBUTE,PRODUCT,DESCRIPTION,CAT_CLASS,GM,LIST_PRICE,APP,VALUE1,CONTRACT_PRICE,UNITS_SOLD,ACTUAL_SALES,ACTUAL_GROSS_MARGIN,CURRENT_LOCATION_COST,NEW_PRICE_OR_NEW_GM_DISC,INVENTORY_ITEM_ID,LIST_HEADER_ID,PRODUCT_ATTRIBUTE_TYPE,CATEGORY_ID,CUST_ACCOUNT_ID,TYPE,PROCESS_ID,ARITHMETIC_OPERATOR,OPERAND,UNIT_COST,CAT,CAT_DESC,PRICE_BY_CAT,FORMULA_NAME,CONTRACT_TYPE,SALESREP,CAT_CLASS_DESC,TRANSACTION_CODE,CREATION_DATE,PERSON_ID,ACCOUNT_NUMBER,COMMON_OUTPUT_ID,LOCATION,PARTY_SITE_NUMBER,VERSION_NO,PRICE_TYPE) AS 
SELECT "MODIFIER_NAME",
    "CUSTOMER_NAME",
    "QUALIFIER_ATTRIBUTE",
    "PRODUCT",
    "DESCRIPTION",
    "CAT_CLASS",
    "GM",
    "LIST_PRICE",
    "APP",
    "VALUE1",
    "CONTRACT_PRICE",
    "UNITS_SOLD",
    "ACTUAL_SALES",
    "ACTUAL_GROSS_MARGIN",
    "CURRENT_LOCATION_COST",
    "NEW_PRICE_OR_NEW_GM_DISC",
    "INVENTORY_ITEM_ID",
    "LIST_HEADER_ID",
    "PRODUCT_ATTRIBUTE_TYPE",
    "CATEGORY_ID",
    "CUST_ACCOUNT_ID",
    "TYPE",
    "PROCESS_ID",
    "ARITHMETIC_OPERATOR",
    "OPERAND",
    "UNIT_COST",
    "CAT",
    "CAT_DESC",
    "PRICE_BY_CAT",
    "FORMULA_NAME",
    "CONTRACT_TYPE",
    "SALESREP",
    "CAT_CLASS_DESC",
    "TRANSACTION_CODE",
    "CREATION_DATE",
    "PERSON_ID",
    "ACCOUNT_NUMBER",
    "COMMON_OUTPUT_ID",
    "LOCATION",
    "PARTY_SITE_NUMBER",
    "VERSION_NO",
    "PRICE_TYPE"
  FROM XXEIS.eis_xxwc_cont_modi_pricing/
set scan on define on
prompt Creating View Data for Contract Pricing Datamart
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_CONT_MODI_DATAMART_V1
xxeis.eis_rs_ins.v( 'EIS_XXWC_CONT_MODI_DATAMART_V1',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Cont Modi Pricing V','EXCMPV','','');
--Delete View Columns for EIS_XXWC_CONT_MODI_DATAMART_V1
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_CONT_MODI_DATAMART_V1',660,FALSE);
--Inserting View Columns for EIS_XXWC_CONT_MODI_DATAMART_V1
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','NEW_PRICE_OR_NEW_GM_DISC',660,'New Price Or New Gm Disc','NEW_PRICE_OR_NEW_GM_DISC','','','','XXEIS_RS_ADMIN','NUMBER','','','New Price Or New Gm Disc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CURRENT_LOCATION_COST',660,'Current Location Cost','CURRENT_LOCATION_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Current Location Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','ACTUAL_SALES',660,'Actual Sales','ACTUAL_SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Actual Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','UNITS_SOLD',660,'Units Sold','UNITS_SOLD','','','','XXEIS_RS_ADMIN','NUMBER','','','Units Sold','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CONTRACT_PRICE',660,'Contract Price','CONTRACT_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Contract Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','APP',660,'App','APP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','App','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','LIST_PRICE',660,'List Price','LIST_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','List Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','GM',660,'Gm','GM','','','','XXEIS_RS_ADMIN','NUMBER','','','Gm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CAT_CLASS',660,'Cat Class','CAT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','DESCRIPTION',660,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','PRODUCT',660,'Product','PRODUCT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Product','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','MODIFIER_NAME',660,'Modifier Name','MODIFIER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Modifier Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','VALUE1',660,'Value1','VALUE1','','','','XXEIS_RS_ADMIN','NUMBER','','','Value1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','ACTUAL_GROSS_MARGIN',660,'Actual Gross Margin','ACTUAL_GROSS_MARGIN','','','','XXEIS_RS_ADMIN','NUMBER','','','Actual Gross Margin','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','QUALIFIER_ATTRIBUTE',660,'Qualifier Attribute','QUALIFIER_ATTRIBUTE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Qualifier Attribute','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','TYPE',660,'Type','TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','UNIT_COST',660,'Unit Cost','UNIT_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CAT',660,'Cat','CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','ARITHMETIC_OPERATOR',660,'Arithmetic Operator','ARITHMETIC_OPERATOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Arithmetic Operator','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CATEGORY_ID',660,'Category Id','CATEGORY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Category Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CAT_CLASS_DESC',660,'Cat Class Desc','CAT_CLASS_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CAT_DESC',660,'Cat Desc','CAT_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CONTRACT_TYPE',660,'Contract Type','CONTRACT_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Contract Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','FORMULA_NAME',660,'Formula Name','FORMULA_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Formula Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','INVENTORY_ITEM_ID',660,'Inventory Item Id','INVENTORY_ITEM_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','LIST_HEADER_ID',660,'List Header Id','LIST_HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','List Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','OPERAND',660,'Operand','OPERAND','','','','XXEIS_RS_ADMIN','NUMBER','','','Operand','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','PRICE_BY_CAT',660,'Price By Cat','PRICE_BY_CAT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Price By Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','SALESREP',660,'Salesrep','SALESREP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','TRANSACTION_CODE',660,'Transaction Code','TRANSACTION_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Transaction Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','ACCOUNT_NUMBER',660,'Account Number','ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','PERSON_ID',660,'Person Id','PERSON_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Person Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','COMMON_OUTPUT_ID',660,'Common Output Id','COMMON_OUTPUT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Common Output Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CONT_MODI_DATAMART_V1','PRODUCT_ATTRIBUTE_TYPE',660,'Product Attribute Type','PRODUCT_ATTRIBUTE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Product Attribute Type','','','');
--Inserting View Components for EIS_XXWC_CONT_MODI_DATAMART_V1
--Inserting View Component Joins for EIS_XXWC_CONT_MODI_DATAMART_V1
END;
/
set scan on define on
prompt Creating Report Data for Contract Pricing Datamart
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Contract Pricing Datamart
xxeis.eis_rs_utility.delete_report_rows( 'Contract Pricing Datamart' );
--Inserting Report - Contract Pricing Datamart
xxeis.eis_rs_ins.r( 660,'Contract Pricing Datamart','','','','','','XXEIS_RS_ADMIN','EIS_XXWC_CONT_MODI_DATAMART_V1','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Contract Pricing Datamart
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'ACTUAL_SALES','Actual Sales','Actual Sales','','~T~D~2','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'NEW_PRICE_OR_NEW_GM_DISC','New Price Or New GM Disc','New Price Or New Gm Disc','','~~~','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'PRODUCT','Product','Product','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'UNITS_SOLD','Units Sold','Units Sold','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'VALUE1','Value','Value1','','~T~D~2','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'APP','App','App','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'CAT_CLASS','Cat Class','Cat Class','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'CONTRACT_PRICE','Contract Price','Contract Price','','~T~D~2','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'CURRENT_LOCATION_COST','Current Location Cost','Current Location Cost','','~T~D~2','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'DESCRIPTION','Description','Description','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'GM','Min GM','Gm','','~T~D~2','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'LIST_PRICE','List Price','List Price','','~T~D~2','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
xxeis.eis_rs_ins.rc( 'Contract Pricing Datamart',660,'MODIFIER_NAME','Modifier Name','Modifier Name','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CONT_MODI_DATAMART_V1','','');
--Inserting Report Parameters - Contract Pricing Datamart
--Inserting Report Conditions - Contract Pricing Datamart
--Inserting Report Sorts - Contract Pricing Datamart
xxeis.eis_rs_ins.rs( 'Contract Pricing Datamart',660,'MODIFIER_NAME','ASC','XXEIS_RS_ADMIN','2','');
xxeis.eis_rs_ins.rs( 'Contract Pricing Datamart',660,'CUSTOMER_NAME','ASC','XXEIS_RS_ADMIN','1','');
--Inserting Report Triggers - Contract Pricing Datamart
xxeis.eis_rs_ins.rt( 'Contract Pricing Datamart',660,'begin

xxeis.EIS_OM_XXWC_CONT_PRIC_PKG.CONTRACT_PRICING_MODI_DATAMART ;                                    
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Contract Pricing Datamart
--Inserting Report Portals - Contract Pricing Datamart
--Inserting Report Dashboards - Contract Pricing Datamart
--Inserting Report Security - Contract Pricing Datamart
xxeis.eis_rs_ins.rsec( 'Contract Pricing Datamart','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Datamart','660','','51045',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Datamart','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Datamart','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Datamart','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Contract Pricing Datamart','','PP018915','',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Contract Pricing Datamart
END;
/
set scan on define on
