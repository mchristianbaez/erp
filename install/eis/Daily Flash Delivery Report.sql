--Report Name            : Daily Flash Delivery Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V',660,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Ar Flsh Sale By Del V','EXAFSBDV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_FLSH_SALE_DL_NEW_V',660,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','ANONYMOUS','VARCHAR2','','','Branch Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','CURRENT_FISCAL_MONTH',660,'Current Fiscal Month','CURRENT_FISCAL_MONTH','','','','ANONYMOUS','VARCHAR2','','','Current Fiscal Month','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','CURRENT_FISCAL_YEAR',660,'Current Fiscal Year','CURRENT_FISCAL_YEAR','','','','ANONYMOUS','NUMBER','','','Current Fiscal Year','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','DELIVERY_TYPE',660,'Delivery Type','DELIVERY_TYPE','','','','ANONYMOUS','VARCHAR2','','','Delivery Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','SALES',660,'Sales','SALES','','','','ANONYMOUS','NUMBER','','','Sales','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','~T~D~0','','ANONYMOUS','NUMBER','','','Invoice Count','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
--Inserting Object Component Joins for EIS_XXWC_AR_FLSH_SALE_DL_NEW_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for Daily Flash Delivery Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Daily Flash Delivery Report
xxeis.eis_rsc_ins.lov( 660,'select  distinct per.period_name , per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     led.accounted_period_type = per.period_type
order by per.period_num asc,
per.period_year desc','','OM PERIOD NAMES','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for Daily Flash Delivery Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Daily Flash Delivery Report
xxeis.eis_rsc_utility.delete_report_rows( 'Daily Flash Delivery Report' );
--Inserting Report - Daily Flash Delivery Report
xxeis.eis_rsc_ins.r( 660,'Daily Flash Delivery Report','','The purpose of this extract is to provide Finance with a daily report of all sales by branch and aggregated by delivery type.  The selected date parameter represents the desired date of sales (e.g. For sales reported on Aug 1st, set the Date parameter = Aug. 1st.   This report is to be processed daily and intended to accompany the daily extracts, flash_charge and flash_cash.
','','','','SA059956','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Daily Flash Delivery Report
xxeis.eis_rsc_ins.rc( 'Daily Flash Delivery Report',660,'BRANCH_LOCATION','Location','Branch Location','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Daily Flash Delivery Report',660,'DELIVERY_TYPE','Delivery Type','Delivery Type','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Daily Flash Delivery Report',660,'SALES','Net Sales Dollars','Sales','','~T~D~2','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Daily Flash Delivery Report',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','~~~','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Daily Flash Delivery Report',660,'CURRENT_FISCAL_MONTH','Fiscal Month','Current Fiscal Month','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Daily Flash Delivery Report',660,'CURRENT_FISCAL_YEAR','Current Fiscal Year','Current Fiscal Year','','~~~','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','','','US','');
--Inserting Report Parameters - Daily Flash Delivery Report
xxeis.eis_rsc_ins.rp( 'Daily Flash Delivery Report',660,'Period Name','Period Name','','IN','OM PERIOD NAMES','','VARCHAR2','Y','Y','1','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Daily Flash Delivery Report',660,'Location','Location','BRANCH_LOCATION','IN','OM Warehouse All','','VARCHAR2','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','','','US','');
--Inserting Dependent Parameters - Daily Flash Delivery Report
--Inserting Report Conditions - Daily Flash Delivery Report
xxeis.eis_rsc_ins.rcnh( 'Daily Flash Delivery Report',660,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and PROCESS_ID=:SYSTEM.PROCESS_ID','1',660,'Daily Flash Delivery Report','Free Text ');
--Inserting Report Sorts - Daily Flash Delivery Report
--Inserting Report Triggers - Daily Flash Delivery Report
xxeis.eis_rsc_ins.rt( 'Daily Flash Delivery Report',660,'begin
xxeis.EIS_XXWC_DAILY_FLASH_DTLS_PKG.get_daily_flash_dtls(:SYSTEM.PROCESS_ID,:Period Name,:Location);
end;','B','Y','SA059956','AQ');
--inserting report templates - Daily Flash Delivery Report
--Inserting Report Portals - Daily Flash Delivery Report
--inserting report dashboards - Daily Flash Delivery Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Daily Flash Delivery Report','660','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','EIS_XXWC_AR_FLSH_SALE_DL_NEW_V','N','');
--inserting report security - Daily Flash Delivery Report
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','TB003018','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','JS020126','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','MT009628','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','20005','','XXWC_IT_OPERATIONS_ANALYST',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','PP018915','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','AS000277','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','SS084202','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','SG019472','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','60002557','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','ZR023146','',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Daily Flash Delivery Report','','SP003819','',660,'SA059956','','','');
--Inserting Report Pivots - Daily Flash Delivery Report
--Inserting Report   Version details- Daily Flash Delivery Report
xxeis.eis_rsc_ins.rv( 'Daily Flash Delivery Report','','Daily Flash Delivery Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
