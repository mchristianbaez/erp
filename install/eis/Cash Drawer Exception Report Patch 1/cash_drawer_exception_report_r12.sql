--Report Name            : Cash Drawer Exception Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
--Creating View EIS_XXWC_CASH_DRAWN_EXCE_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_V
Create or replace View XXEIS.EIS_XXWC_CASH_DRAWN_EXCE_V
 AS 
SELECT ORDER_NUMBER,
    ORG_ID,
    PAYMENT_TYPE_CODE,
    PAYMENT_TYPE_CODE_NEW,
    TAKEN_BY,
    CASH_TYPE,
    CHK_CARDNO,
    CARD_NUMBER,
    CARD_ISSUER_CODE,
    CARD_ISSUER_NAME,
    NVL(CASH_AMOUNT,0) CASH_AMOUNT,
    CUSTOMER_NUMBER,
    CUSTOMER_NAME,
    BRANCH_NUMBER,
    CHECK_NUMBER,
    ORDER_DATE,
    CASH_DATE,
    PAYMENT_CHANNEL_NAME,
    name,
    CASE
      WHEN Payment_Number=1
       THEN NVL(ORDER_AMOUNT,0)+charge_amt
      WHEN Payment_Number IS NULL
      THEN NVL(ORDER_AMOUNT,0)+charge_amt
      ELSE 0
    END ORDER_AMOUNT,
    CASE
      WHEN PAYMENT_TYPE_CODE ='Prism Return'
      THEN 0
      WHEN Payment_Number IS NULL
      or PAYMENT_NUMBER    =1
   --  THEN NVL(PAYMENT_AMOUNT,0)-(NVL(ORDER_AMOUNT,0)+charge_amt)
  -- THEN (NVL(ORDER_AMOUNT,0)+charge_amt)-NVL(PAYMENT_AMOUNT,0)
   THEN NVL(PAYMENT_AMOUNT,0)
      ELSE 0
    END diff,
    --DECODE(PAYMENT_TYPE_CODE,'Prism Return',0,NVL(PAYMENT_AMOUNT,0)-NVL(ORDER_AMOUNT,0)) diff,
    PARTY_ID,
    CUST_ACCOUNT_ID,
    INVOICE_NUMBER,
    INVOICE_DATE,
    SEGMENT_NUMBER,
    HEADER_ID,
    ON_ACCOUNT,
    Dist_date
  from
    (select  XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CHARGER_AMT(OH.HEADER_ID) charge_amt,
    OH.ORDER_NUMBER,
      oh.org_id,
      CASE
        WHEN ((OE.PAYMENT_TYPE_CODE IN('CHECK','CREDIT_CARD')
        AND upper(ARC.NAME) LIKE UPPER('%Card%'))
        OR OH.ATTRIBUTE8 ='CREDIT_CARD')
        THEN 'Credit Card'
        WHEN ((OE.PAYMENT_TYPE_CODE IN ('CHECK')
        AND UPPER(ARC.NAME) NOT LIKE UPPER('%Card%'))
        OR OH.ATTRIBUTE8 = 'CHECK')
        THEN 'Check'
        WHEN (OE.PAYMENT_TYPE_CODE IN ('CASH')
        OR OH.ATTRIBUTE8            = 'CASH')
        THEN 'Cash'
        ELSE NULL
      END PAYMENT_TYPE_CODE,
      CASE
        WHEN OE.PAYMENT_TYPE_CODE IN( 'CHECK','CREDIT_CARD')
        AND upper(ARC.NAME) LIKE UPPER('%Card%')
        THEN 'Credit Card'
        WHEN OE.PAYMENT_TYPE_CODE IN ('CHECK')
        AND UPPER(ARC.NAME) NOT LIKE UPPER('%Card%')
        THEN 'Check'
        ELSE olkp.meaning
      END PAYMENT_TYPE_CODE_NEW,
      PPF.LAST_NAME TAKEN_BY,
      CASE
        WHEN OTL.name = 'RETURN ORDER'
        THEN 'PRISM RETURN'
        WHEN OE.PAYMENT_TYPE_CODE IN ('CASH','CHECK','CREDIT_CARD')
        AND NOT EXISTS
          (SELECT ORDERED_ITEM
          FROM OE_ORDER_LINES OL
          WHERE OL.HEADER_ID=OH.HEADER_ID
          AND ORDERED_ITEM LIKE '%DEPOSIT%'
          )
          /*AND NOT EXISTS
          (SELECT header_id
          FROM xxwc_om_cash_refund_tbl re
          WHERE re.RETURN_HEADER_ID=oh.header_id
          )*/
        THEN 'CASH SALES'
        WHEN OE.PAYMENT_TYPE_CODE IN ('CASH','CHECK','CREDIT_CARD')
        AND EXISTS
          (SELECT ORDERED_ITEM
          FROM OE_ORDER_LINES OL
          WHERE OL.HEADER_ID=OH.HEADER_ID
          AND ORDERED_ITEM LIKE '%DEPOSIT%'
          )
          /*AND NOT EXISTS
          (SELECT header_id
          FROM xxwc_om_cash_refund_tbl re
          WHERE re.RETURN_HEADER_ID=oh.header_id
          )*/
        THEN 'DEPOSIT'
      END CASH_TYPE,
      DECODE(OE.PAYMENT_TYPE_CODE,'CHECK',OE.CHECK_NUMBER,'CREDIT_CARD',SUBSTR(ITE.CARD_NUMBER,-4)) CHK_CARDNO,
      SUBSTR(ITE.CARD_NUMBER,                                                                  -4) CARD_NUMBER,
      ITE.CARD_ISSUER_CODE,
      ITE.CARD_ISSUER_NAME,
      --  NVL(OE.PAYMENT_AMOUNT,0) CASH_AMOUNT,
      CASE
        WHEN Payment_Number IS NOT NULL
        THEN NVL(OE.PAYMENT_AMOUNT,0)
        ELSE
          (SELECT SUM(ARA.AMOUNT_APPLIED)
          FROM AR_RECEIVABLE_APPLICATIONS ARA,
            RA_CUSTOMER_TRX RCT
          WHERE rct.INTERFACE_header_CONTEXT ='ORDER ENTRY'
          AND RCT.CUSTOMER_TRX_ID            = ARA.APPLIED_CUSTOMER_TRX_ID
          AND ara.display                    ='Y'
          AND TO_CHAR(OH.ORDER_NUMBER)       = RCT.INTERFACE_HEADER_ATTRIBUTE1
          )
      END CASH_AMOUNT,
      /* ( select SUM(ARA.AMOUNT_APPLIED) from
      AR_RECEIVABLE_APPLICATIONS  ARA,
      RA_CUSTOMER_TRX             RCT
      where RCT.CUSTOMER_TRX_ID    =  ARA.APPLIED_CUSTOMER_TRX_ID
      and TO_CHAR(OH.ORDER_NUMBER) =  RCT.INTERFACE_HEADER_ATTRIBUTE1
      ) CASH_AMOUNT,*/
      hca.account_number customer_number,
      HZP.PARTY_NAME CUSTOMER_NAME,
      OOD.ORGANIZATION_CODE BRANCH_NUMBER,
      OE.CHECK_NUMBER,
      TRUNC(OH.ORDERED_DATE) ORDER_DATE,
      --  TRUNC(OH.ORDERED_DATE) CASH_DATE,
      -- TRUNC(oe.creation_date) CASH_DATE,
      DECODE(OTL.name ,'RETURN ORDER',TRUNC(OH.ORDERED_DATE),
      (SELECT MAX(TRUNC(ARA.apply_date))
      FROM AR_RECEIVABLE_APPLICATIONS ARA,
        RA_CUSTOMER_TRX RCT
      WHERE RCT.CUSTOMER_TRX_ID        = ARA.APPLIED_CUSTOMER_TRX_ID(+)
      AND ara.display(+)               ='Y'
      AND rct.INTERFACE_header_CONTEXT ='ORDER ENTRY'
      AND TO_CHAR(OH.ORDER_NUMBER)     = RCT.INTERFACE_HEADER_ATTRIBUTE1
      )) CASH_DATE,
      ITE.PAYMENT_CHANNEL_NAME,
      ARC.name,
      PAYMENT_NUMBER,
      /*(
      CASE
      WHEN Payment_Number IS NULL
      THEN*
      (SELECT SUM(NVL(aps.amount_due_original,0)-NVL(aps.amount_due_remaining,0))
      FROM ar_payment_schedules aps,
        RA_CUSTOMER_TRX RCT
      WHERE rct.INTERFACE_header_CONTEXT ='ORDER ENTRY'
      AND RCT.CUSTOMER_TRX_ID            = aps.CUSTOMER_TRX_ID
      and TO_CHAR(OH.ORDER_NUMBER)       = RCT.INTERFACE_HEADER_ATTRIBUTE1
      ) )PAYMENT_AMOUNT,*/
     -- XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_PAYMENT_AMOUNT(OH.ORDER_NUMBER,OE.PAYMENT_SET_ID,OH.HEADER_ID) PAYMENT_AMOUNT,
     XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_BALANCE_AMOUNT(OH.ORDER_NUMBER,OE.PAYMENT_SET_ID,OH.HEADER_ID)*-1 PAYMENT_AMOUNT,
      CASE
        WHEN (OTL.NAME <> 'STANDARD ORDER'
        OR EXISTS
          (SELECT 1
          FROM OE_ORDER_LINES OL1
          WHERE OL1.header_id = OH.HEADER_ID
          AND OL1.ORDERED_ITEM LIKE '%DEPOSIT%'
          ))
        THEN
          (SELECT NVL(ROUND(SUM(ROUND(OL.ORDERED_QUANTITY*OL.UNIT_SELLING_PRICE,2)+ nvl(OL.TAX_VALUE,0)),2),0) ORDER_AMOUNT
          FROM OE_ORDER_LINES OL
          WHERE OL.HEADER_ID=OH.HEADER_ID
          )
        WHEN OTL.NAME ='STANDARD ORDER'
        THEN
         (SELECT NVL (ROUND(SUM(ROUND(NVL(decode(OTL.name,'BILL ONLY',OL.fulfilled_quantity,OL.SHIPPED_QUANTITY),0)*OL.UNIT_SELLING_PRICE,2)+ nvl(OL.TAX_VALUE,0)),2),0) ORDER_AMOUNT
          FROM OE_ORDER_LINES OL,
          OE_TRANSACTION_TYPES_VL OTL
          WHERE OL.HEADER_ID                                                                       =OH.HEADER_ID
          AND OL.LINE_TYPE_ID    = OTL.TRANSACTION_TYPE_ID
          and NVL(decode(OTL.name,'BILL ONLY',OL.fulfilled_quantity,OL.SHIPPED_QUANTITY),0)   <> 0
          and ((NVL(TRUNC(OL.ACTUAL_SHIPMENT_DATE),XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE) <=XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)
              OR ((OTL.name='BILL ONLY') and (NVL(TRUNC(OL.FULFILLMENT_DATE),XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE) <=XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)
              )
              )
          AND OL.ORDERED_ITEM NOT LIKE '%DEPOSIT%'
          )
        ELSE 0
      END ORDER_AMOUNT,
      HZP.PARTY_ID,
      HCA.CUST_ACCOUNT_ID,
      (SELECT MAX(RCT.TRX_NUMBER)
      FROM RA_CUSTOMER_TRX RCT
      WHERE rct.INTERFACE_header_CONTEXT ='ORDER ENTRY'
      AND TO_CHAR(OH.ORDER_NUMBER)       =RCT.INTERFACE_HEADER_ATTRIBUTE1
      )Invoice_Number,
      --   NULL CREDIT_NUMBER,
      (
      SELECT MAX(RCT.TRX_DATE)
      FROM RA_CUSTOMER_TRX RCT
      WHERE rct.INTERFACE_header_CONTEXT ='ORDER ENTRY'
      AND TO_CHAR(OH.ORDER_NUMBER)       =RCT.INTERFACE_HEADER_ATTRIBUTE1
      )INVOICE_DATE,
      (SELECT NVL(MAX(RCT.CREATION_DATE),NVL(MAX(ol.actual_shipment_date), MAX(ol.fulfillment_date)))
      FROM RA_CUSTOMER_TRX RCT,
        oe_order_lines ol
      WHERE rct.INTERFACE_header_CONTEXT(+) ='ORDER ENTRY'
      AND TO_CHAR(OH.ORDER_NUMBER)          = RCT.INTERFACE_HEADER_ATTRIBUTE1(+)
      AND ol.header_id                      = oh.header_id
      )Dist_date,
      (SELECT MAX(GCC.SEGMENT2)
      FROM OE_ORDER_LINES L,
        RA_CUSTOMER_TRX_LINES RCTL,
        RA_CUSTOMER_TRX RCT,
        RA_CUST_TRX_LINE_GL_DIST RCTG,
        GL_CODE_COMBINATIONS_KFV GCC
      WHERE OH.HEADER_ID               =L.HEADER_ID
      AND rct.INTERFACE_header_CONTEXT ='ORDER ENTRY'
      AND TO_CHAR(OH.ORDER_NUMBER)     =RCTL.INTERFACE_LINE_ATTRIBUTE1
      AND TO_CHAR(L.LINE_ID)           =RCTL.INTERFACE_LINE_ATTRIBUTE6
      AND RCT.CUSTOMER_TRX_ID          =RCTL.CUSTOMER_TRX_ID
      AND RCTL.CUSTOMER_TRX_ID         =RCTG.CUSTOMER_TRX_ID
      AND RCTL.CUSTOMER_TRX_LINE_ID    =RCTG.CUSTOMER_TRX_LINE_ID
      AND rctg.code_combination_id     =gcc.code_combination_id
      )Segment_Number,
      Oh.Header_Id Header_Id,
      0 cash_receipt_id,
      CASE
        WHEN otl.name = 'COUNTER ORDER'
        THEN
          (SELECT SUM(NVL(amount_applied,0))
          FROM ar_receivable_applications rap
          WHERE rap.applied_payment_schedule_id = -1
          AND rap.display                       = 'Y'
          AND NOT EXISTS
            (SELECT 1
            FROM XXWC_OM_CASH_REFUND_TBL XOC
            WHERE XOC.cash_receipt_id=rap.cash_receipt_id
            )
          AND rap.cash_receipt_id IN
            (SELECT cash_receipt_id
            FROM ar_receivable_applications rap1
            WHERE rap1.payment_set_id = oe.payment_set_id
            )
          )
        ELSE 0
      END ON_ACCOUNT
    FROM OE_ORDER_HEADERS OH,
      oe_payments oe,
      oe_lookups olkp,
      hz_parties hzp,
      HZ_CUST_ACCOUNTS HCA,
      ORG_ORGANIZATION_DEFINITIONS OOD,
      IBY_TRXN_EXTENSIONS_V ITE,
      PER_ALL_PEOPLE_F PPF,
      FND_USER FU,
      -- ra_customer_trx trx,
      AR_RECEIPT_METHODS arc,
      oe_transaction_types_VL otl,
      ra_terms ra
    WHERE OH.SOLD_TO_ORG_ID =HCA.CUST_ACCOUNT_ID
    AND OE.HEADER_ID(+)     =OH.HEADER_ID
    AND OH.PAYMENT_TERM_ID  = RA.TERM_ID
  --and oh.header_id=1233249
    AND HZP.PARTY_ID        =HCA.PARTY_ID
    AND OH.SHIP_FROM_ORG_ID =OOD.ORGANIZATION_ID
    AND oh.order_type_id    = otl.transaction_type_id
    AND FU.USER_ID          =OH.CREATED_BY
    and FU.EMPLOYEE_ID      =PPF.PERSON_ID(+)
    --and oe.prepaid_amount <>0 --Commneted By Srinivas
      /*   AND (XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE IS NULL
      OR TRUNC(ORDERED_DATE)                                 >= XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE
      OR TRUNC(oe.creation_date)                             >= XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_FROM_DATE)*/
    AND (XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE                      IS NULL
    OR TRUNC(ORDERED_DATE)                                                    <= TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)
    OR TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_DATE(OE.PAYMENT_SET_ID)) <= TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE))
    AND TRUNC(OH.ORDERED_DATE) BETWEEN NVL(TRUNC(PPF.EFFECTIVE_START_DATE),TRUNC(OH.ORDERED_DATE)) AND NVL(TRUNC(PPF.EFFECTIVE_END_DATE),TRUNC(OH.ORDERED_DATE))
    AND OE.TRXN_EXTENSION_ID           =ITE.TRXN_EXTENSION_ID(+)
    AND oe.receipt_method_id           =arc.receipt_method_id(+)
    AND oe.payment_type_code           = olkp.lookup_code(+)
    AND oe.payment_collection_event(+) ='PREPAY'
    AND (ARC.NAME                     IS NULL
    OR upper(ARC.NAME) NOT LIKE '%POA%')
    AND OTL.name            <> 'RETURN ORDER'
    AND oh.flow_status_code IN('BOOKED','CLOSED')
    AND Olkp.Lookup_Type(+)  = 'PAYMENT TYPE'
    AND otl.name            <> 'INTERNAL ORDER'
    AND (Payment_Number     IS NOT NULL
    OR RA.name              IN ('COD','PRFRDCASH'))
    AND RA.ATTRIBUTE1        = 'Y'
    AND NOT EXISTS
      (SELECT 1
      FROM hz_customer_profiles hcp,
        hz_cust_profile_classes hcpc
      WHERE hca.party_id       = hcp.party_id
      AND hca.cust_account_id  = hcp.cust_account_id
      AND hcp.site_use_id     IS NULL
      AND hcp.profile_class_id = hcpc.profile_class_id(+)
      AND hcpc.name LIKE 'WC%Branches%'
      )
    AND EXISTS
      (SELECT 1
      FROM ra_customer_trx trx
      WHERE trx.interface_header_context  ='ORDER ENTRY'
      AND trx.interface_header_attribute1 = TO_CHAR(oh.order_number)
      )
    AND(otl.name = 'COUNTER ORDER'
    OR NOT EXISTS
      (SELECT 1
      FROM XXWC_OM_CASH_REFUND_TBL XOC
      WHERE XOC.RETURN_HEADER_ID=OH.HEADER_ID
      ) )
    AND ((otl.name ='STANDARD ORDER'
    AND EXISTS
      (SELECT 1
      FROM OE_ORDER_LINES L
      WHERE OH.HEADER_ID                                                          =L.HEADER_ID
      AND ((TRUNC(L.ACTUAL_SHIPMENT_DATE)                                        <= TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE) )
      OR (TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_DATE(OE.PAYMENT_SET_ID)) <= TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)) )
      OR (OH.HEADER_ID                                                            =L.HEADER_ID
      AND L.ORDERED_ITEM LIKE '%DEPOSIT%'
      AND (TRUNC(OH.ORDERED_DATE) <= TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)))
      ) )
    OR(OTL.NAME                                                               <> 'STANDARD ORDER'
    AND((TRUNC(OH.ORDERED_DATE)                                               <= TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE))
    or(TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_DATE(OE.PAYMENT_SET_ID)) <= TRUNC(XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_CASH_TO_DATE)) ) ) )
    ORDER BY OH.HEADER_ID)
  WHERE (NVL(PAYMENT_AMOUNT,0)<>0
  or ON_ACCOUNT                                     <> 0)/
set scan on define on
prompt Creating View Data for Cash Drawer Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_CASH_DRAWN_EXCE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_CASH_DRAWN_EXCE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Cash Drwr Recon V','EXACDRV','','');
--Delete View Columns for EIS_XXWC_CASH_DRAWN_EXCE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_CASH_DRAWN_EXCE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_CASH_DRAWN_EXCE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CASH_DATE',660,'Cash Date','CASH_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Cash Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','BRANCH_NUMBER',660,'Branch Number','BRANCH_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CASH_AMOUNT',660,'Cash Amount','CASH_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Cash Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CARD_ISSUER_NAME',660,'Card Issuer Name','CARD_ISSUER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CASH_TYPE',660,'Cash Type','CASH_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cash Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PAYMENT_TYPE_CODE',660,'Payment Type Code','PAYMENT_TYPE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','TAKEN_BY',660,'Taken By','TAKEN_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Taken By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','SEGMENT_NUMBER',660,'Segment Number','SEGMENT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CHK_CARDNO',660,'Chk Cardno','CHK_CARDNO','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Chk Cardno','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORDER_AMOUNT',660,'Order Amount','ORDER_AMOUNT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Order Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','NAME',660,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORDER_DATE',660,'Order Date','ORDER_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Order Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','DIFF',660,'Diff','DIFF','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Diff','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ON_ACCOUNT',660,'On Account','ON_ACCOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','On Account','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','DIST_DATE',660,'Dist Date','DIST_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Dist Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CUST_ACCOUNT_ID',660,'Cust Account Id','CUST_ACCOUNT_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Cust Account Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PARTY_ID',660,'Party Id','PARTY_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Party Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PAYMENT_CHANNEL_NAME',660,'Payment Channel Name','PAYMENT_CHANNEL_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Channel Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CHECK_NUMBER',660,'Check Number','CHECK_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Check Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CARD_ISSUER_CODE',660,'Card Issuer Code','CARD_ISSUER_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Issuer Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','CARD_NUMBER',660,'Card Number','CARD_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Card Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','ORG_ID',660,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','HEADER_ID',660,'Header Id','HEADER_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Header Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_CASH_DRAWN_EXCE_V','PAYMENT_TYPE_CODE_NEW',660,'Payment Type Code New','PAYMENT_TYPE_CODE_NEW','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Type Code New','','','');
--Inserting View Components for EIS_XXWC_CASH_DRAWN_EXCE_V
--Inserting View Component Joins for EIS_XXWC_CASH_DRAWN_EXCE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Cash Drawer Exception Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Cash Drawer Exception Report
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Cash Drawer Exception Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Cash Drawer Exception Report
xxeis.eis_rs_utility.delete_report_rows( 'Cash Drawer Exception Report' );
--Inserting Report - Cash Drawer Exception Report
xxeis.eis_rs_ins.r( 660,'Cash Drawer Exception Report','','This report provides a total listing of all Cash, Check and Credit Card Sales by Branch (Customer, Taken By, Order Number, Cash Date, Cash Amount, Cash Type, Invoice Number, Invoice Date, Payment Type, Check/Credit Card No.)','','','','XXEIS_RS_ADMIN','EIS_XXWC_CASH_DRAWN_EXCE_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Cash Drawer Exception Report
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'BRANCH_NUMBER','Branch Number','Branch Number','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CASH_AMOUNT','Cash Amount','Cash Amount','','~T~D~2','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CASH_DATE','Cash Date','Cash Date','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CASH_TYPE','Cash Type','Cash Type','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CHK_CARDNO','Check Card No','Chk Cardno','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'NAME','Receipt Method','Name','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'ORDER_AMOUNT','Order Amount','Order Amount','','~T~D~2','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'PAYMENT_TYPE_CODE','Payment Type','Payment Type Code','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'SEGMENT_NUMBER','Segment Number','Segment Number','','','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'TAKEN_BY','Created By','Taken By','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'CARD_ISSUER_NAME','Card Type','Card Issuer Name','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'DIFF','Diff','Diff','','~T~D~2','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'ORDER_DATE','Order Date','Order Date','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'ON_ACCOUNT','On Account','On Account','','~T~D~2','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
xxeis.eis_rs_ins.rc( 'Cash Drawer Exception Report',660,'DIST_DATE','Discrepancy Date','Dist Date','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CASH_DRAWN_EXCE_V','','');
--Inserting Report Parameters - Cash Drawer Exception Report
xxeis.eis_rs_ins.rp( 'Cash Drawer Exception Report',660,'Location','Branch Number','BRANCH_NUMBER','IN','AR Organizaion Code LOV','','VARCHAR2','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Cash Drawer Exception Report',660,'As of Date','As of Date','CASH_DATE','>=','','','DATE','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Cash Drawer Exception Report
xxeis.eis_rs_ins.rcn( 'Cash Drawer Exception Report',660,'BRANCH_NUMBER','IN',':Location','','','Y','1','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Cash Drawer Exception Report
--Inserting Report Triggers - Cash Drawer Exception Report
xxeis.eis_rs_ins.rt( 'Cash Drawer Exception Report',660,'declare
begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.G_CASH_TO_DATE:=:As of Date;
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Cash Drawer Exception Report
xxeis.eis_rs_ins.R_Tem( 'Cash Drawer Exception Report','Cash Drawer Reconciliation','Cash Drawer Reconciliation','','','','','','','','','','','','XXEIS_RS_ADMIN');
--Inserting Report Portals - Cash Drawer Exception Report
--Inserting Report Dashboards - Cash Drawer Exception Report
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 757','Dynamic 757','scatter','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 751','Dynamic 751','pie','large','Cash Amount','Cash Amount','Customer Name','Customer Name','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 755','Dynamic 755','point','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 756','Dynamic 756','stacked line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 758','Dynamic 758','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 754','Dynamic 754','absolute line','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 752','Dynamic 752','pie','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 759','Dynamic 759','horizontal stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.r_dash( 'Cash Drawer Exception Report','Dynamic 753','Dynamic 753','vertical stacked bar','large','Customer Name','Customer Name','Cash Amount','Cash Amount','Count','XXEIS_RS_ADMIN');
--Inserting Report Security - Cash Drawer Exception Report
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','','LB048272','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','20434',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','20475',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50878',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50877',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50879',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50853',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50854',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50721',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50662',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50720',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50723',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50801',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50847',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','260','','50758',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','260','','50770',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','401','','50835',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50838',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','401','','50840',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50665',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50722',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50780',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50667',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','101','','50668',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','','TW008334','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','51045',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','51025',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Cash Drawer Exception Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Cash Drawer Exception Report
xxeis.eis_rs_ins.rpivot( 'Cash Drawer Exception Report',660,'By Payment Type','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - By Payment Type
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CASH_TYPE','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CUSTOMER_NUMBER','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','NAME','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CUSTOMER_NAME','PAGE_FIELD','','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CASH_AMOUNT','DATA_FIELD','SUM','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','BRANCH_NUMBER','ROW_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','PAYMENT_TYPE_CODE','ROW_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Cash Drawer Exception Report',660,'By Payment Type','CARD_ISSUER_NAME','ROW_FIELD','','','3','','');
--Inserting Report Summary Calculation Columns For Pivot- By Payment Type
END;
/
set scan on define on
