--Report Name            : HDS Active Vendor Details
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data XXEIS_ACTIVE_VENDOR_DETAILS
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_ACTIVE_VENDOR_DETAILS
xxeis.eis_rsc_ins.v( 'XXEIS_ACTIVE_VENDOR_DETAILS',200,'','','','','XXEIS_RS_ADMIN','XXEIS','Xxeis Active Vendor Details1','XAVD1','','','VIEW','US','','');
--Delete Object Columns for XXEIS_ACTIVE_VENDOR_DETAILS
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_ACTIVE_VENDOR_DETAILS',200,FALSE);
--Inserting Object Columns for XXEIS_ACTIVE_VENDOR_DETAILS
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','OPERATING_UNIT',200,'Operating Unit','OPERATING_UNIT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','ATTRIBUTE1',200,'Attribute1','ATTRIBUTE1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attribute1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','NUM_1099',200,'Num 1099','NUM_1099','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Num 1099','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','DESCRIPTION',200,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','NAME',200,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','ORG_ID',200,'Org Id','ORG_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','SITE_PAYGROUP',200,'Site Paygroup','SITE_PAYGROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Site Paygroup','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','COUNTRY',200,'Country','COUNTRY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Country','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','ZIP',200,'Zip','ZIP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Zip','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','STATE',200,'State','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','CITY',200,'City','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','City','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','ADDRESS_LINE2',200,'Address Line2','ADDRESS_LINE2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','ADDRESS_LINE1',200,'Address Line1','ADDRESS_LINE1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Address Line1','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','SITE_CREATION',200,'Site Creation','SITE_CREATION','','','','XXEIS_RS_ADMIN','DATE','','','Site Creation','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','VENDOR_SITE_ID',200,'Vendor Site Id','VENDOR_SITE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Vendor Site Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','VENDOR_SITE_CODE',200,'Vendor Site Code','VENDOR_SITE_CODE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Site Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','VENDOR_PAYGROUP',200,'Vendor Paygroup','VENDOR_PAYGROUP','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Paygroup','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','VENDOR_INACTIVE_DATE',200,'Vendor Inactive Date','VENDOR_INACTIVE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Vendor Inactive Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','VENDOR_CREATION',200,'Vendor Creation','VENDOR_CREATION','','','','XXEIS_RS_ADMIN','DATE','','','Vendor Creation','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','VENDOR_NUMBER',200,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','VENDOR_NAME',200,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','CREATION_DATE',200,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','PAY_SITE_FLAG',200,'Pay Site Flag','PAY_SITE_FLAG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pay Site Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','ATTRIBUTE2',200,'Attribute2','ATTRIBUTE2','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','PAYMENT_METHOD',200,'Payment Method','PAYMENT_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Payment Method','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_ACTIVE_VENDOR_DETAILS','REMIT_EMAIL',200,'Remit Email','REMIT_EMAIL','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Remit Email','','','','US');
--Inserting Object Components for XXEIS_ACTIVE_VENDOR_DETAILS
--Inserting Object Component Joins for XXEIS_ACTIVE_VENDOR_DETAILS
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report LOV Data for HDS Active Vendor Details
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS Active Vendor Details
xxeis.eis_rsc_ins.lov( 200,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
prompt Creating Report Data for HDS Active Vendor Details
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(200);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS Active Vendor Details
xxeis.eis_rsc_utility.delete_report_rows( 'HDS Active Vendor Details' );
--Inserting Report - HDS Active Vendor Details
xxeis.eis_rsc_ins.r( 200,'HDS Active Vendor Details','','','','','','XXEIS_RS_ADMIN','XXEIS_ACTIVE_VENDOR_DETAILS','Y','','','XXEIS_RS_ADMIN','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','Y','','','','','','','','US','','','','');
--Inserting Report Columns - HDS Active Vendor Details
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'ADDRESS_LINE1','Address Line1','Address Line1','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'ADDRESS_LINE2','Address Line2','Address Line2','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'ATTRIBUTE1','Prism Vendor Number','Attribute1','','','default','','21','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'CITY','City','City','','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'COUNTRY','Country','Country','','','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'DESCRIPTION','Description','Description','','','default','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'NAME','Name','Name','','','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'NUM_1099','Num 1099','Num 1099','','','default','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'SITE_CREATION','Site Creation','Site Creation','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'SITE_PAYGROUP','Site Paygroup','Site Paygroup','','','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'STATE','State','State','','','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'VENDOR_CREATION','Vendor Creation','Vendor Creation','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'VENDOR_INACTIVE_DATE','Vendor Inactive Date','Vendor Inactive Date','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'VENDOR_PAYGROUP','Vendor Paygroup','Vendor Paygroup','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'VENDOR_SITE_CODE','Vendor Site Code','Vendor Site Code','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'VENDOR_SITE_ID','Vendor Site Id','Vendor Site Id','','~T~D~0','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'ZIP','Zip','Zip','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'CREATION_DATE','Creation_Date  (Xxeis_Active_Vendor_Details)','Creation Date','DATE','','default','','23','Y','Y','','','','','','MAX(XAVD1.CREATION_DATE)','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'ORG_ID','Org Id','Org Id','','~T~D~0','default','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'ATTRIBUTE2','Attribute2','Attribute2','','','default','','20','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'PAYMENT_METHOD','Payment Method','Payment Method','','','default','','24','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS Active Vendor Details',200,'REMIT_EMAIL','Remit Email','Remit Email','','','default','','25','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_ACTIVE_VENDOR_DETAILS','','','GROUP_BY','US','');
--Inserting Report Parameters - HDS Active Vendor Details
xxeis.eis_rsc_ins.rp( 'HDS Active Vendor Details',200,'Organization Name','Organization Name','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_ACTIVE_VENDOR_DETAILS','','','US','');
--Inserting Dependent Parameters - HDS Active Vendor Details
--Inserting Report Conditions - HDS Active Vendor Details
xxeis.eis_rsc_ins.rcnh( 'HDS Active Vendor Details',200,'OPERATING_UNIT IN :Organization Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','OPERATING_UNIT','','Organization Name','','','','','XXEIS_ACTIVE_VENDOR_DETAILS','','','','','','IN','Y','Y','','','','','1',200,'HDS Active Vendor Details','OPERATING_UNIT IN :Organization Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS Active Vendor Details',200,'PAY_SITE_FLAG = ''Y'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PAY_SITE_FLAG','','','','','','','XXEIS_ACTIVE_VENDOR_DETAILS','','','','','','EQUALS','Y','N','','''Y''','','','1',200,'HDS Active Vendor Details','PAY_SITE_FLAG = ''Y'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS Active Vendor Details',200,'SITE_PAYGROUP <> ''EMPLOYEE'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SITE_PAYGROUP','','','','','','','XXEIS_ACTIVE_VENDOR_DETAILS','','','','','','NOTEQUALS','Y','N','','''EMPLOYEE''','','','1',200,'HDS Active Vendor Details','SITE_PAYGROUP <> ''EMPLOYEE'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS Active Vendor Details',200,'Free Text ','FREE_TEXT','','','N','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','GROUP BY
       XAVD1.vendor_name,
       XAVD1.Vendor_Number,
       XAVD1.Vendor_Creation,
       XAVD1.Vendor_Inactive_Date,
       XAVD1.Vendor_PayGroup,
       XAVD1.vendor_site_code,
       XAVD1.vendor_site_id,
       XAVD1.site_creation,
       XAVD1.address_line1,
       XAVD1.address_line2,
       XAVD1.city,
       XAVD1.state,
       XAVD1.zip,
       XAVD1.country,
       XAVD1.Site_PayGroup,
       XAVD1.org_id,
       XAVD1.name,
       XAVD1.description,
       XAVD1.num_1099,
       XAVD1.attribute1,
       XAVD1.attribute2,
       XAVD1.remit_email,
       XAVD1.payment_method

','1',200,'HDS Active Vendor Details','Free Text ');
--Inserting Report Sorts - HDS Active Vendor Details
--Inserting Report Triggers - HDS Active Vendor Details
--inserting report templates - HDS Active Vendor Details
--Inserting Report Portals - HDS Active Vendor Details
--inserting report dashboards - HDS Active Vendor Details
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS Active Vendor Details','200','XXEIS_ACTIVE_VENDOR_DETAILS','XXEIS_ACTIVE_VENDOR_DETAILS','N','');
--inserting report security - HDS Active Vendor Details
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_ADMIN_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_DISBURSEMTS_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_INQUIRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_MGR_NOSUP_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_SUPPLIER_MAINT_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_TRNS_ENTRY_US_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','PAYABLES_INQUIRY',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','PAYABLES_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','20005','','XXWC_VIEW_ALL_EIS_REPORTS',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_MANAGER',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_TRNS_ENTRY_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_SUPPLIER_MAINT_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_MGR_NOSUP_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_DISBURSEMTS_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_AP_ADMIN_CAD_GSC',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_PAYABLES_CLOSE_GSC_GLBL',200,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'HDS Active Vendor Details','200','','HDS_PAYABLES_CLOSE_GLBL',200,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - HDS Active Vendor Details
--Inserting Report   Version details- HDS Active Vendor Details
xxeis.eis_rsc_ins.rv( 'HDS Active Vendor Details','','HDS Active Vendor Details','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 200');
END IF;
END;
/
