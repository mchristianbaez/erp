--Report Name            : Inventory Sales and Reorder Report QA
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Inventory Sales and Reorder Report QA
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_ISR_RPT_QA_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_ISR_RPT_QA_V',201,'','','','','10011289','XXEIS','Eis Xxwc Po Isr V','EXPIV','','');
--Delete View Columns for EIS_XXWC_PO_ISR_RPT_QA_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_ISR_RPT_QA_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_ISR_RPT_QA_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','BPA',201,'Bpa','BPA','','','','10011289','VARCHAR2','','','Bpa','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','ITEM_COST',201,'Item Cost','ITEM_COST','','~T~D~2','','10011289','NUMBER','','','Item Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','AVER_COST',201,'Aver Cost','AVER_COST','','~T~D~2','','10011289','NUMBER','','','Aver Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','AMU',201,'Amu','AMU','','~T~D~0','','10011289','NUMBER','','','Amu','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','PM',201,'Pm','PM','','','','10011289','VARCHAR2','','','Pm','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','STK_FLAG',201,'Stk Flag','STK_FLAG','','','','10011289','VARCHAR2','','','Stk Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','CL',201,'Cl','CL','','','','10011289','VARCHAR2','','','Cl','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','UOM',201,'Uom','UOM','','','','10011289','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','CAT',201,'Cat','CAT','','','','10011289','VARCHAR2','','','Cat','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','DESCRIPTION',201,'Description','DESCRIPTION','','','','10011289','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','ST',201,'St','ST','','','','10011289','VARCHAR2','','','St','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','SOURCE',201,'Source','SOURCE','','','','10011289','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','VENDOR_NAME',201,'Vendor Name','VENDOR_NAME','','','','10011289','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','VENDOR_NUM',201,'Vendor Num','VENDOR_NUM','','','','10011289','VARCHAR2','','','Vendor Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','ITEM_NUMBER',201,'Item Number','ITEM_NUMBER','','','','10011289','VARCHAR2','','','Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','PRE',201,'Pre','PRE','','','','10011289','VARCHAR2','','','Pre','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','ORG',201,'Org','ORG','','','','10011289','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','BUYER',201,'Buyer','BUYER','','','','10011289','VARCHAR2','','','Buyer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','TURNS',201,'Turns','TURNS','','','','10011289','NUMBER','','','Turns','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','THIRTEEN_WK_AN_COGS',201,'Thirteen Wk An Cogs','THIRTEEN_WK_AN_COGS','','','','10011289','NUMBER','','','Thirteen Wk An Cogs','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','THIRTEEN_WK_AVG_INV',201,'Thirteen Wk Avg Inv','THIRTEEN_WK_AVG_INV','','','','10011289','NUMBER','','','Thirteen Wk Avg Inv','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','RES',201,'Res','RES','','~T~D~0','','10011289','NUMBER','','','Res','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','FREEZE_DATE',201,'Freeze Date','FREEZE_DATE','','','','10011289','DATE','','','Freeze Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','FI_FLAG',201,'Fi Flag','FI_FLAG','','','','10011289','VARCHAR2','','','Fi Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','MC',201,'Mc','MC','','','','10011289','VARCHAR2','','','Mc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','BIN_LOC',201,'Bin Loc','BIN_LOC','','','','10011289','VARCHAR2','','','Bin Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','AVAILABLEDOLLAR',201,'Availabledollar','AVAILABLEDOLLAR','','','','10011289','NUMBER','','','Availabledollar','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','AVAILABLE',201,'Available','AVAILABLE','','','','10011289','NUMBER','','','Available','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','QOH',201,'Qoh','QOH','','','','10011289','NUMBER','','','Qoh','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','APR_SALES',201,'Apr Sales','APR_SALES','','','','10011289','NUMBER','','','Apr Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','AUG_SALES',201,'Aug Sales','AUG_SALES','','','','10011289','NUMBER','','','Aug Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','DEC_SALES',201,'Dec Sales','DEC_SALES','','','','10011289','NUMBER','','','Dec Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','FEB_SALES',201,'Feb Sales','FEB_SALES','','','','10011289','NUMBER','','','Feb Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','HIT4_SALES',201,'Hit4 Sales','HIT4_SALES','','','','10011289','NUMBER','','','Hit4 Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','HIT6_SALES',201,'Hit6 Sales','HIT6_SALES','','','','10011289','NUMBER','','','Hit6 Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','JAN_SALES',201,'Jan Sales','JAN_SALES','','','','10011289','NUMBER','','','Jan Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','JUL_SALES',201,'Jul Sales','JUL_SALES','','','','10011289','NUMBER','','','Jul Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','JUNE_SALES',201,'June Sales','JUNE_SALES','','','','10011289','NUMBER','','','June Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','MAR_SALES',201,'Mar Sales','MAR_SALES','','','','10011289','NUMBER','','','Mar Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','MAY_SALES',201,'May Sales','MAY_SALES','','','','10011289','NUMBER','','','May Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','NOV_SALES',201,'Nov Sales','NOV_SALES','','','','10011289','NUMBER','','','Nov Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','OCT_SALES',201,'Oct Sales','OCT_SALES','','','','10011289','NUMBER','','','Oct Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','SEP_SALES',201,'Sep Sales','SEP_SALES','','','','10011289','NUMBER','','','Sep Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','ONE_SALES',201,'One Sales','ONE_SALES','','','','10011289','NUMBER','','','One Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','SIX_SALES',201,'Six Sales','SIX_SALES','','','','10011289','NUMBER','','','Six Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','TWELVE_SALES',201,'Twelve Sales','TWELVE_SALES','','','','10011289','NUMBER','','','Twelve Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','DISTRICT',201,'District','DISTRICT','','','','10011289','VARCHAR2','','','District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','REGION',201,'Region','REGION','','','','10011289','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','MAXN',201,'Maxn','MAXN','','~T~D~0','','10011289','NUMBER','','','Maxn','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','MINN',201,'Minn','MINN','','~T~D~0','','10011289','NUMBER','','','Minn','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','PLT',201,'Plt','PLT','','~T~D~0','','10011289','NUMBER','','','Plt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','PPLT',201,'Pplt','PPLT','','~T~D~0','','10011289','NUMBER','','','Pplt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','TS',201,'Ts','TS','','~T~D~0','','10011289','NUMBER','','','Ts','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','BPA_COST',201,'Bpa Cost','BPA_COST','','~T~D~2','','10011289','NUMBER','','','Bpa Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','ON_ORD',201,'On Ord','ON_ORD','','','','10011289','NUMBER','','','On Ord','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','FML',201,'Fml','FML','','','','10011289','NUMBER','','','Fml','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','OPEN_REQ',201,'Open Req','OPEN_REQ','','','','10011289','NUMBER','','','Open Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','WT',201,'Wt','WT','','','','10011289','NUMBER','','','Wt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','SO',201,'So','SO','','','','10011289','NUMBER','','','So','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','SOURCING_RULE',201,'Sourcing Rule','SOURCING_RULE','','','','10011289','VARCHAR2','','','Sourcing Rule','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','SS',201,'Ss','SS','','','','10011289','NUMBER','','','Ss','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','CLT',201,'Clt','CLT','','','','10011289','NUMBER','','','Clt','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','AVAIL2',201,'Avail2','AVAIL2','','','','10011289','NUMBER','','','Avail2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','INT_REQ',201,'Int Req','INT_REQ','','','','10011289','NUMBER','','','Int Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','DIR_REQ',201,'Dir Req','DIR_REQ','','','','10011289','NUMBER','','','Dir Req','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','SITE_VENDOR_NUM',201,'Site Vendor Num','SITE_VENDOR_NUM','','','','10011289','VARCHAR2','','','Site Vendor Num','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','VENDOR_SITE',201,'Vendor Site','VENDOR_SITE','','','','10011289','VARCHAR2','','','Vendor Site','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','ORG_NAME',201,'Org Name','ORG_NAME','','','','10011289','VARCHAR2','','','Org Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','MF_FLAG',201,'Mf Flag','MF_FLAG','','','','10011289','VARCHAR2','','','Mf Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','SET_OF_BOOKS_ID',201,'Set Of Books Id','SET_OF_BOOKS_ID','','','','10011289','NUMBER','','','Set Of Books Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','ORGANIZATION_ID',201,'Organization Id','ORGANIZATION_ID','','','','10011289','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','INVENTORY_ITEM_ID',201,'Inventory Item Id','INVENTORY_ITEM_ID','','','','10011289','NUMBER','','','Inventory Item Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','COMMON_OUTPUT_ID',201,'Common Output Id','COMMON_OUTPUT_ID','','','','10011289','NUMBER','','','Common Output Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_ISR_RPT_QA_V','PROCESS_ID',201,'Process Id','PROCESS_ID','','','','10011289','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_PO_ISR_RPT_QA_V
--Inserting View Component Joins for EIS_XXWC_PO_ISR_RPT_QA_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Inventory Sales and Reorder Report QA
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Inventory Sales and Reorder Report QA
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct segment1 bin_loc from Mtl_Item_Locations','','Bin Location Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'    SELECT distinct Mcv.SEGMENT2 cat_class
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            MTL_ITEM_CATEGORIES MIC,
            mtl_system_items_kfv msi
            Where   Mcs.Category_Set_Name      = ''Inventory Category''
          AND MCS.STRUCTURE_ID                 = MCV.STRUCTURE_ID
          AND MIC.INVENTORY_ITEM_ID            = MSI.INVENTORY_ITEM_ID
          And Mic.Organization_Id              = msi.Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id
order by Mcv.SEGMENT2','','Catclass Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Exclude,Include,Tool Repair Only','Tool Repair','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','  ,Vendor Number(%),3 Digit Prefix,Item number,Source,2 Digit Cat,4 Digit Cat Class,Default Buyer(%)','Report Criteria','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Active small,All items,All items on hand, Stock items only,Non stock only,Active large, Non-stock on hand,Stock items with 0/0 min/max','EIS PO XXWC ISR REPORT COND','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Include,Time Sensitive Only','EIS PO XXWC Time Sensitive','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','No,Yes','EIS PO XXWC DC Mode','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Source'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Source List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'SELECT DISTINCT segment1 Item
FROM mtl_system_items_b msi
WHERE NOT EXISTS
  (SELECT 1
  FROM mtl_parameters mp
  WHERE organization_code IN (''CAN'',''HDS'',''US1'',''CN1'')
  AND msi.organization_id  = mp.organization_id
  )','','XXWC Item','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct pov.segment1 vendor_number, pov.vendor_name from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC Vendors','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,
  ood.organization_name organization_name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE organization_code NOT IN(''CAN'',''HDS'',''US1'',''CN1'')
ORDER BY organization_code','','XXWC Org Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'','Exclude,Include','XXWC Include Exclude','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Inventory Sales and Reorder Report QA
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Inventory Sales and Reorder Report QA
xxeis.eis_rs_utility.delete_report_rows( 'Inventory Sales and Reorder Report QA' );
--Inserting Report - Inventory Sales and Reorder Report QA
xxeis.eis_rs_ins.r( 201,'Inventory Sales and Reorder Report QA','','This report displays info needed to make inventory replenishment decisions ( by vendor, part number) at selected locations.','','','','10011289','EIS_XXWC_PO_ISR_RPT_QA_V','Y','','','10011289','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Inventory Sales and Reorder Report QA
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'AUG_SALES','Aug','Aug Sales','','~~~','default','','57','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'AVAILABLE','Avail','Available','','~~~','default','','34','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'AVAILABLEDOLLAR','Ext$','Availabledollar','','~~~','default','','36','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'AVER_COST','Aver Cost','Aver Cost','','~T~D~2','default','','27','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'BIN_LOC','Bin Loc','Bin Loc','','','default','','40','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'BPA','Bpa#','Bpa','','','default','','30','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'BUYER','Buyer','Buyer','','','default','','48','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'CAT','Cat/Cl','Cat','','','default','','10','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'CL','Cl','Cl','','','default','','19','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'DEC_SALES','Dec','Dec Sales','','~~~','default','','61','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'DESCRIPTION','Description','Description','','','default','','9','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'FEB_SALES','Feb','Feb Sales','','~~~','default','','51','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'FI_FLAG','FI','Fi Flag','','','default','','42','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'FREEZE_DATE','Freeze Date','Freeze Date','','','default','','43','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'HIT4_SALES','Hit4','Hit4 Sales','','~~~','default','','26','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'HIT6_SALES','Hit6','Hit6 Sales','','~~~','default','','25','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'ITEM_COST','Item Cost','Item Cost','','~T~D~2','default','','28','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'ITEM_NUMBER','Item Number','Item Number','','','default','','3','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'JAN_SALES','Jan','Jan Sales','','~~~','default','','50','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'JUL_SALES','Jul','Jul Sales','','~~~','default','','56','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'JUNE_SALES','June','June Sales','','~~~','default','','55','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'MAR_SALES','Mar','Mar Sales','','~~~','default','','52','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'MAXN','Max','Maxn','NUMBER','~T~D~0','default','','23','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'MAY_SALES','May','May Sales','','~~~','default','','54','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'MC','MC','Mc','','','default','','41','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'MINN','Min','Minn','NUMBER','~T~D~0','default','','22','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'NOV_SALES','Nov','Nov Sales','','~~~','default','','60','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'OCT_SALES','Oct','Oct Sales','','~~~','default','','59','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'ONE_SALES','1','One Sales','','~~~','default','','37','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'ORG','Org','Org','','','default','','1','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'PLT','PLT','Plt','','~T~D~0','default','','12','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'PM','PM','Pm','','','default','','21','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'PPLT','PPLT','Pplt','','~T~D~0','default','','11','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'PRE','Pre','Pre','','','default','','2','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'QOH','QOH','Qoh','','~~~','default','','31','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'RES','Res','Res','NUMBER','~T~D~0','default','','44','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'SEP_SALES','Sep','Sep Sales','','~~~','default','','58','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'SIX_SALES','6','Six Sales','','~~~','default','','38','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'SOURCE','Source','Source','','','default','','7','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'ST','ST','St','','','default','','8','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'STK_FLAG','Stk','Stk Flag','','','default','','20','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'THIRTEEN_WK_AN_COGS','13 Wk An COGS $','Thirteen Wk An Cogs','','~~~','default','','46','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'THIRTEEN_WK_AVG_INV','13 Wk Av Inv $','Thirteen Wk Avg Inv','','~~~','default','','45','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'TS','TS','Ts','NUMBER','~T~D~0','default','','49','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'TURNS','Turns','Turns','','~~~','default','','47','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'TWELVE_SALES','12','Twelve Sales','','~~~','default','','39','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'UOM','UOM','Uom','','','default','','15','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'VENDOR_NAME','Vendor','Vendor Name','','','default','','5','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'VENDOR_NUM','Vend #','Vendor Num','','','default','','4','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'AMU','AMU','Amu','NUMBER','~T~D~0','default','','24','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'FML','FLM','Fml','','~~~','default','','17','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'OPEN_REQ','Req','Open Req','','~~~','default','','32','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'WT','Wt','Wt','','~~~','default','','16','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'SO','So','So','','~~~','default','','18','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'SOURCING_RULE','Sourcing Rule','Sourcing Rule','','','default','','6','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'SS','Ss','Ss','','~~~','default','','14','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'CLT','CLT','Clt','','~~~','default','','13','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'AVAIL2','Avail2','Avail2','','~~~','default','','35','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'BPA_COST','Bpa Cost','Bpa Cost','','~T~D~2','default','','29','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'ON_ORD','On Ord','On Ord','','~~~','default','','33','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'APR_SALES','Apr','Apr Sales','','~~~','default','','53','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'INT_REQ','Int Req','Int Req','','~~~','default','','62','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'DIR_REQ','Dir Req','Dir Req','','~~~','default','','63','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'SITE_VENDOR_NUM','Supplier Number','Site Vendor Num','','','default','','64','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
xxeis.eis_rs_ins.rc( 'Inventory Sales and Reorder Report QA',201,'VENDOR_SITE','Supplier Site','Vendor Site','','','default','','65','N','','','','','','','','10011289','N','N','','EIS_XXWC_PO_ISR_RPT_QA_V','','');
--Inserting Report Parameters - Inventory Sales and Reorder Report QA
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'DC Mode','DC Mode','','IN','EIS PO XXWC DC Mode','''No''','VARCHAR2','N','Y','5','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Starting Bin','Starting Bin','BIN_LOC','>=','Bin Location Lov','','VARCHAR2','N','Y','13','','Y','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','','Y','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Ending Bin','Ending Bin','BIN_LOC','<=','Bin Location Lov','','VARCHAR2','N','Y','14','','Y','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','','Y','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Tool Repair','Tool Repair','','IN','Tool Repair','''Include''','VARCHAR2','N','Y','6','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Time Sensitive','Time Sensitive','','IN','EIS PO XXWC Time Sensitive','','VARCHAR2','N','Y','8','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Vendor','Vendor','VENDOR_NUM','IN','XXWC Vendors','','VARCHAR2','N','Y','15','','Y','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Item','Item','ITEM_NUMBER','IN','XXWC Item','','VARCHAR2','N','Y','16','','Y','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Cat Class','Cat Class','CAT','IN','Catclass Lov','','VARCHAR2','N','Y','17','','Y','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Items w/ 4 mon sales hits','Stock items w/ 4 mon sales hits > x','','IN','','','NUMERIC','N','Y','10','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Report Criteria','Report Criteria','','IN','Report Criteria','','VARCHAR2','N','Y','11','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Report Criteria Value','Report Criteria Value','','IN','','','VARCHAR2','N','Y','12','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Report Filter','Report Condition','','IN','EIS PO XXWC ISR REPORT COND','','VARCHAR2','N','Y','9','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Item List','Item List','','IN','XXWC Item List','','VARCHAR2','N','Y','18','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','19','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Cat Class List','Cat Class List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','20','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Source List','Source List','','IN','XXWC Source List','','VARCHAR2','N','Y','21','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Organization List','Organization List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Organization','Organization','ORG','IN','XXWC Org Lov','','VARCHAR2','N','Y','3','','Y','CONSTANT','10011289','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Inventory Sales and Reorder Report QA',201,'Intangible Items','Intangible Items','','IN','XXWC Include Exclude','''Exclude''','VARCHAR2','N','Y','7','','N','CONSTANT','10011289','Y','N','','','');
--Inserting Report Conditions - Inventory Sales and Reorder Report QA
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report QA',201,'PROCESS_ID','IN',':SYSTEM.PROCESS_ID','','','Y','1','N','10011289');
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report QA',201,'REGION','IN',':Region','','','Y','1','Y','10011289');
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report QA',201,'DISTRICT','IN',':District','','','Y','2','Y','10011289');
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report QA',201,'BIN_LOC','>=',':Starting Bin','','','Y','13','Y','10011289');
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report QA',201,'BIN_LOC','<=',':Ending Bin','','','Y','14','Y','10011289');
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report QA',201,'VENDOR_NUM','IN',':Vendor','','','Y','15','Y','10011289');
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report QA',201,'ITEM_NUMBER','IN',':Item','','','Y','16','Y','10011289');
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report QA',201,'CAT','IN',':Cat Class','','','Y','17','Y','10011289');
xxeis.eis_rs_ins.rcn( 'Inventory Sales and Reorder Report QA',201,'ORG','IN',':Organization','','','Y','3','Y','10011289');
--Inserting Report Sorts - Inventory Sales and Reorder Report QA
--Inserting Report Triggers - Inventory Sales and Reorder Report QA
xxeis.eis_rs_ins.rt( 'Inventory Sales and Reorder Report QA',201,'begin
xxeis.eis_po_xxwc_isr_util_pkg.G_isr_rpt_dc_mod_sub:=:DC Mode;
xxeis.eis_po_xxwc_isr_pkg_v2.Isr_Rpt_Proc(
P_process_id => :SYSTEM.PROCESS_ID,
P_Region =>:Region,
P_District =>:District,
P_Location =>:Organization,
P_Dc_Mode =>:DC Mode,
P_Tool_Repair =>:Tool Repair,
P_Time_Sensitive =>:Time Sensitive,                            
P_Stk_Items_With_Hit4 =>:Items w/ 4 mon sales hits,
p_Report_Condition =>:Report Filter,
P_Report_Criteria            =>:Report Criteria,
P_Report_Criteria_val            =>:Report Criteria Value,
p_start_bin_loc =>:Starting Bin,
p_end_bin_loc =>:Ending Bin,
p_vendor =>:Vendor,
p_item =>:Item,
p_cat_class =>:Cat Class,
p_org_list =>:Organization List,
P_ITEM_LIST =>:Item List,
P_SUPPLIER_LIST =>:Vendor List,
P_CAT_CLASS_LIST =>:Cat Class List,
P_SOURCE_LIST =>:Source List,
P_INTANGIBLES => :Intangible Items
);
end;','B','Y','10011289');
xxeis.eis_rs_ins.rt( 'Inventory Sales and Reorder Report QA',201,'begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(:SYSTEM.PROCESS_ID);
end;','A','Y','10011289');
--Inserting Report Templates - Inventory Sales and Reorder Report QA
--Inserting Report Portals - Inventory Sales and Reorder Report QA
--Inserting Report Dashboards - Inventory Sales and Reorder Report QA
--Inserting Report Security - Inventory Sales and Reorder Report QA
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','20005','','50861',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','201','','50621',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','20005','','50900',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50846',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50862',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50848',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50868',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50849',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50867',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','660','','50870',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','201','','50850',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','201','','50872',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50990',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','175','','50922',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','175','','50941',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50882',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50883',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50981',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50855',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','401','','50884',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','704','','50885',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','661','','50891',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','201','','50921',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','201','','50892',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','201','','50910',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','201','','50893',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','201','','50983',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','201','','50989',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','','10010494','',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','20005','','50843',201,'10011289','','');
xxeis.eis_rs_ins.rsec( 'Inventory Sales and Reorder Report QA','20005','','51207',201,'10011289','','');
--Inserting Report Pivots - Inventory Sales and Reorder Report QA
END;
/
set scan on define on
