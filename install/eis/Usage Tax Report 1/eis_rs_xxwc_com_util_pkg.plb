create or replace
PACKAGE BODY                                                   EIS_RS_XXWC_COM_UTIL_PKG AS

FUNCTION get_period_name (P_LEDGER_ID NUMBER, P_GL_DATE DATE)
RETURN VARCHAR2
IS
 l_period_name varchar2(20);
BEGIN

 select period_name
   into l_period_name
  from  gl_periods gp, gl_ledgers gl
  WHERE gp.period_set_name = gl.period_set_name
   and  gl.ledger_id = P_LEDGER_ID
   AND  P_GL_DATE between gp.start_date and gp.end_date;

  return l_period_name;
 EXCEPTION
 WHEN OTHERS THEN
   RETURN NULL;
END;

function get_vendor_name (p_inventory_item_id number, p_organization_id number)
RETURN VARCHAR2
is
 l_vendor_name varchar2(240);
BEGIN

    select max(pov.vendor_name )
    into l_vendor_name
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov
    where 1=1
    and ass.inventory_item_id(+)=p_inventory_item_id
    and ass.organization_id(+)=p_organization_id
    and rco.sourcing_rule_id(+)=ass.sourcing_rule_id
    and sso.sr_receipt_id(+)=rco.sr_receipt_id
    and pov.vendor_id(+)=sso.vendor_id;

  return l_vendor_name;
 exception
 WHEN OTHERS THEN
   return null;
END;


function get_vendor_number (p_inventory_item_id number, p_organization_id number)
RETURN VARCHAR2
is
 l_vendor_number varchar2(240);
BEGIN

    select max(pov.segment1 )
    into l_vendor_number
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov
    where 1=1
    and ass.inventory_item_id(+)=p_inventory_item_id
    and ass.organization_id(+)=p_organization_id
    and rco.sourcing_rule_id(+)=ass.sourcing_rule_id
    and sso.sr_receipt_id(+)=rco.sr_receipt_id
    and pov.vendor_id(+)=sso.vendor_id;

  return l_vendor_number;
 exception
 WHEN OTHERS THEN
   return null;
END;



Function Get_Beg_Inv (P_Inventory_Item_Id Number, P_Organization_Id Number,p_Date Date)
Return Number
Is
 l_Beg_Inv Number;
BEGIN

   select sum(moq.transaction_quantity)
    into  l_beg_inv
    from mtl_onhand_quantities_detail moq
    where trunc(moq.date_received)<=p_Date
    and inventory_item_id          =P_Inventory_Item_Id
    and organization_id            =p_organization_id
    and moq.owning_tp_type         = 1;

  return l_Beg_Inv;
 exception
 WHEN OTHERS THEN
   Return Null;
END;


Function get_End_Inv (P_Inventory_Item_Id Number, P_Organization_Id Number,p_Date Date)
Return Number
Is
 l_End_Inv Number;
BEGIN

    Select Sum(Moq.Transaction_Quantity)
    Into L_End_Inv
    From  Mtl_Onhand_Quantities_Detail Moq
    Where 1                      =1
    And Trunc(Moq.Date_Received)<= p_Date
    And Inventory_Item_Id        = P_Inventory_Item_Id
    and organization_id          = p_organization_id
    And Moq.Owning_Tp_Type       = 1;
  return l_End_Inv;
 exception
 WHEN OTHERS THEN
   return null;
END;


FUNCTION get_turns(p_inventory_item_id NUMBER, p_organization_id NUMBER,P_Start_Date Date,P_End_Date Date)
RETURN Number
Is
 l_cogs_Amt Number;
 l_min_date date;
 l_end_date date;
 l_turns number;
 l_avg_inv number;
 
BEGIN

  begin
  select min(vdate) 
    into l_min_date
   from xxeis.eis_xxwc_inv_val_dm_tab;
  exception 
   when others then
      l_min_date := null;
  end; 
  IF l_min_date > p_end_date THEN
    l_end_date := l_min_date;
  END IF;
  
  begin
    select nvl(sum(((ol.ordered_quantity * DECODE(ol.line_category_code,'RETURN',-1,1))-nvl(ol.cancelled_quantity,0)) *nvl(ol.unit_cost,0)),0) * 4
    into l_cogs_amt
    from oe_order_headers oh,
         oe_order_lines ol
    where oh.header_id=ol.header_id
    and trunc(oh.booked_date) between P_Start_Date and nvl(l_end_date,P_End_Date)
    and ol.inventory_item_id =p_inventory_item_id
    And Ol.Ship_From_Org_Id  =P_Organization_Id
    And Oh.Flow_Status_Code In ('BOOKED','CLOSED','CANCELLED');
  exception
   WHEN OTHERS THEN 
     l_cogs_amt := 0;
  end;
  
  begin
  
   select avg(nvl(OHQTY,0)* nvl(unit_cost,0))
    into l_avg_inv
     from xxeis.eis_xxwc_inv_val_dm_tab
    where vdate between p_start_date and nvl(l_end_date,P_End_Date);   
     
     l_turns := l_cogs_amt/l_avg_inv;
        
  exception 
     when others then
        l_turns :=0;
  end;
  
  
  return nvl(l_turns,0);
 exception
 WHEN OTHERS THEN
   Return 0;
END;


FUNCTION get_cogs (p_inventory_item_id NUMBER, p_organization_id NUMBER,P_Start_Date Date,P_End_Date Date)
RETURN Number
Is
 l_cogs_Amt Number;
BEGIN

  select nvl(sum(((ol.ordered_quantity * DECODE(ol.line_category_code,'RETURN',-1,1))-nvl(ol.cancelled_quantity,0)) *nvl(ol.unit_cost,0)),0)
  into l_cogs_amt
  from oe_order_headers oh,
       oe_order_lines ol
  where oh.header_id=ol.header_id
  and trunc(oh.booked_date) between P_Start_Date and P_End_Date
  and ol.inventory_item_id =p_inventory_item_id
  And Ol.Ship_From_Org_Id  =P_Organization_Id
  And Oh.Flow_Status_Code Not In ('ENTERED');
  
  return l_cogs_Amt;
 exception
 WHEN OTHERS THEN
   Return 0;
END;


FUNCTION get_Cogs_Amt (p_inventory_item_id NUMBER, p_organization_id NUMBER,P_Start_Date Date,P_End_Date Date)
RETURN Number
Is
 l_cogs_Amt Number;
BEGIN

  select nvl(sum(ol.ordered_quantity*nvl(ol.unit_selling_price,0)),0)
  into l_cogs_amt
  from oe_order_headers oh,
       oe_order_lines ol
  where oh.header_id=ol.header_id
  and trunc(oh.ordered_date) between P_Start_Date and P_End_Date
  and ol.inventory_item_id =p_inventory_item_id
  And Ol.Ship_From_Org_Id  =P_Organization_Id
  And Oh.Flow_Status_Code Not In ('CANCELLED')
  and ol.flow_status_code not in ('CANCELLED');


  return l_cogs_Amt;
 exception
 WHEN OTHERS THEN
   Return 0;
END;

FUNCTION get_item_selling_price (p_inventory_item_id NUMBER, p_org_id NUMBER)
RETURN Number
is
 l_item_price Number;
BEGIN

   Select Nvl(Unit_Selling_Price,0)
   Into L_Item_Price
    From Ra_Customer_Trx_Lines_all
    Where Customer_Trx_Line_Id
           In
              (
                 Select Max(Customer_Trx_Line_Id)
                  From Ra_Customer_Trx_Lines_All Rctl
                  Where Rctl.Inventory_Item_Id = P_Inventory_Item_Id
                  And Org_Id=P_Org_Id
              );

  return l_item_price;
 exception
 WHEN OTHERS THEN
   return null;
END;



FUNCTION get_last_order_num (p_inventory_item_id NUMBER, p_customer_id NUMBER)
Return Number
Is
 l_order_num Number;
BEGIN

  select interface_header_attribute1
    into l_order_num
    from ra_customer_trx
    where customer_trx_id in
          (
            select max(rct.customer_trx_id)
            from ra_customer_trx rct,
                 ra_customer_trx_lines rctl,
                 oe_order_lines ol,
                 oe_order_lines oll,
                 mtl_system_items_b msi
            where bill_to_customer_id            = p_customer_id
            and rct.interface_header_attribute1 is not null
            and rct.interface_header_context     ='ORDER ENTRY'
            AND TO_CHAR(oll.line_id)             = rctl.interface_line_attribute6
            and oll.link_to_line_id               = ol.line_id
            and ol.inventory_item_id             = p_inventory_item_id
            AND oll.inventory_item_id            = msi.inventory_item_id
            AND oll.SHIP_FROM_ORG_ID             = msi.organization_id
            AND msi.segment1                     = 'Rental Charge'
            and rctl.customer_trx_id             = rct.customer_trx_id
            --and oll.inventory_item_id             = p_inventory_item_id
          );


  return l_order_num;
 exception
 WHEN OTHERS THEN
   return null;
END;


FUNCTION get_dist_invoice_num (p_customer_trx_id NUMBER, p_invoice_number VARCHAR2, p_order_type VARCHAR2)
Return VARCHAR2
is
 l_invoice_num VARCHAR2(240);
 l_amount number;
BEGIN

    IF p_order_type IN ('WC LONG TERM RENTAL','WC SHORT TERM RENTAL') THEN
        select sum(extended_amount) 
          into l_amount
         from ra_customer_trx_lines_all rctl
          where rctl.customer_trx_id = p_customer_trx_id;
          
         if l_amount <> 0 then
            l_invoice_num := p_invoice_number;
         ELSE
           l_invoice_num := NULL;
         end if; 
          
    ELSE
      l_invoice_num := p_invoice_number;
    END IF;
    
  return l_invoice_num;
 exception
 WHEN OTHERS THEN
   Return Null;
END;

FUNCTION get_account_type_code (p_transaction_id NUMBER, p_account_id number) Return VARCHAR2

is
 l_account_line_code VARCHAR2(240);
 
BEGIN
 
 select max(LU1.MEANING)
  into  l_account_line_code
  from MTL_TRANSACTION_ACCOUNTS MTA, 
       MFG_LOOKUPS LU1 
 where LU1.LOOKUP_TYPE      = 'CST_ACCOUNTING_LINE_TYPE' 
   AND LU1.LOOKUP_CODE      = MTA.ACCOUNTING_LINE_TYPE   
   AND mta.transaction_id   = p_transaction_id
   AND mta.ACCOUNTING_LINE_TYPE in ('35','36');
   --AND mta.reference_account = p_account_id;
   
    
  return l_account_line_code;
 exception
 WHEN OTHERS THEN
   Return Null;
END;


FUNCTION get_last_invoice_num (p_inventory_item_id NUMBER, p_customer_id NUMBER, p_header_id number)
Return VARCHAR2
is
 l_invoice_num VARCHAR2(240);
BEGIN

    select trx_number
    into l_invoice_num
    from ra_customer_trx
    where customer_trx_id in
          (
            select max(rct.customer_trx_id)
            from ra_customer_trx rct,
                 ra_customer_trx_lines rctl,
                 oe_order_lines ol,
                 oe_order_lines oll,
                 mtl_system_items_b msi
            where bill_to_customer_id            = p_customer_id
            and rct.interface_header_attribute1 is not null
            and rct.interface_header_context     ='ORDER ENTRY'
            AND TO_CHAR(oll.line_id)             = rctl.interface_line_attribute6
            and oll.link_to_line_id              = ol.line_id            
            AND ol.header_id                     = p_header_id
            and ol.inventory_item_id             = p_inventory_item_id
            AND oll.inventory_item_id            = msi.inventory_item_id
            AND oll.SHIP_FROM_ORG_ID             = msi.organization_id
            AND msi.segment1                     = 'Rental Charge'
            and rctl.customer_trx_id             = rct.customer_trx_id
            --and oll.inventory_item_id             = p_inventory_item_id
          );

  return l_invoice_num;
 exception
 WHEN OTHERS THEN
   Return Null;
END;

FUNCTION get_rental_item (p_line_id NUMBER)
Return VARCHAR2
is
 l_item VARCHAR2(240);
BEGIN
 
   select ordered_item
     into l_item
     from oe_order_lines 
    where line_id = p_line_id;
  

  return l_item;
 exception
 WHEN OTHERS THEN
   Return Null;
END;


FUNCTION get_rental_item_desc (p_line_id NUMBER)
Return VARCHAR2
is
 l_item_desc VARCHAR2(240);
BEGIN
 
   select msi.description
     into l_item_desc
     from oe_order_lines ol,
          mtl_system_items_b msi
    where line_id = p_line_id
      and msi.inventory_item_id = ol.inventory_item_id 
      and msi.organization_id   = ol.ship_from_org_id;
  
  return l_item_desc;
 exception
 WHEN OTHERS THEN
   Return Null;
END;




FUNCTION get_last_billed_date (p_inventory_item_id NUMBER, p_customer_id NUMBER,p_header_id number)
Return Date
is
 l_invoice_date Date;
BEGIN

    select trunc(trx_date)
    into   l_invoice_date
    From Ra_Customer_Trx
    where customer_trx_id in
          (
            select max(rct.customer_trx_id)
            from ra_customer_trx rct,
                 ra_customer_trx_lines rctl,
                 oe_order_lines ol,
                 oe_order_lines oll,
                 mtl_system_items_b msi
            where bill_to_customer_id            = p_customer_id
            and rct.interface_header_attribute1 is not null
            and rct.interface_header_context     ='ORDER ENTRY'
            AND TO_CHAR(oll.line_id)             = rctl.interface_line_attribute6
            and oll.link_to_line_id               = ol.line_id
            and ol.inventory_item_id             = p_inventory_item_id
            AND oll.inventory_item_id            = msi.inventory_item_id
            AND oll.SHIP_FROM_ORG_ID             = msi.organization_id
            AND msi.segment1                     = 'Rental Charge'
            and rctl.customer_trx_id             = rct.customer_trx_id
            --and oll.inventory_item_id             = p_inventory_item_id
          );



  return l_invoice_date;
 exception
 WHEN OTHERS THEN
   return null;
END;


FUNCTION Get_New_Selling_Price (p_inventory_item_id NUMBER,p_organization_id NUMBER, p_customer_id NUMBER)
Return Number
Is
 l_selling_price Number;
BEGIN

       select nvl(unit_selling_price,0)
      into l_selling_price
        FROM ra_customer_trx_lines
       where customer_trx_line_id in
            (
             select max(header_id)
            from oe_order_lines oel
            where oel.inventory_item_id= p_inventory_item_id
            and oel.ship_from_org_id   = p_organization_id
            AND oel.sold_to_org_id     = p_customer_id            
            );
                  

  return l_selling_price;
 exception
 WHEN OTHERS THEN
   Return Null;
END;

FUNCTION Get_list_Price (p_inventory_item_id NUMBER,p_organization_id NUMBER)
Return Number
is
 l_list_price Number;
BEGIN

     select nvl(unit_list_price,0)
     into l_list_price
      from oe_order_lines
      where line_id in
            (
             select max(oel.line_id)
            from oe_order_lines oel
            where oel.inventory_item_id = p_inventory_item_id
            and oel.ship_from_org_id    = p_organization_id
            );


  return l_list_price;
 exception
 WHEN OTHERS THEN
   return null;
END;

FUNCTION Get_last_list_Price (p_inventory_item_id NUMBER,p_organization_id NUMBER)
Return Number
is
 l_list_price Number;
BEGIN

     select nvl(unit_list_price,0)
     into l_list_price
      from oe_order_lines
      where line_id in
            (
             select min(oel.line_id)
            from oe_order_lines oel
            where oel.inventory_item_id = p_inventory_item_id
            and oel.ship_from_org_id    = p_organization_id
            );


  return l_list_price;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;

FUNCTION Get_Curr_lP_start_date (p_list_header_id NUMBER,p_list_line_id NUMBER)
Return Date
IS
 l_Curr_lp_start_date Date;
BEGIN

     select START_DATE_ACTIVE
     into l_Curr_lp_start_date
      from qp_list_lines
      where list_line_id in
            (
             select max(qll.list_line_id)
            from qp_list_lines qll
            where qll.list_header_id = p_list_header_id
            and qll.list_line_id    = p_list_line_id
            );


  return l_Curr_lp_start_date;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;


FUNCTION Get_Curr_lP_end_date (p_list_header_id NUMBER,p_list_line_id NUMBER)
Return Date
IS
 l_Curr_lp_end_date Date;
BEGIN

     select END_DATE_ACTIVE
     into l_Curr_lp_end_date
      from qp_list_lines
      where list_line_id in
            (
             select max(qll.list_line_id)
            from qp_list_lines qll
            where qll.list_header_id = p_list_header_id
            and qll.list_line_id    = p_list_line_id
            );


  return l_Curr_lp_end_date;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;

FUNCTION Get_Last_lP_start_date (p_list_header_id NUMBER,p_list_line_id NUMBER)
Return Date
is
 l_last_lp_start_date Date;
BEGIN

     select START_DATE_ACTIVE
     into l_last_lp_start_date
      from qp_list_lines
      where list_line_id in
            (
             select min(qll.list_line_id)
            from qp_list_lines qll
            where qll.list_header_id = p_list_header_id
            and qll.list_line_id    = p_list_line_id
            );


  return l_last_lp_start_date;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;



Function Get_Mtd_Sales (p_inventory_item_id NUMBER,p_organization_id NUMBER, P_Start_Date Date default null,P_End_Date Date default null)
RETURN Number
is
 l_Mtd_sales Number;
BEGIN

select nvl(sum(oel.ordered_quantity),0)
Into l_Mtd_sales
    from  oe_order_headers oeh,
          oe_order_lines oel
    WHERE  TRUNC(ordered_date) BETWEEN nvl(P_Start_Date,G_DATE_FROM) AND nvl(P_End_Date,G_DATE_TO)
    And Oeh.Header_Id        =Oel.Header_Id
    and oel.inventory_item_id=p_inventory_item_id
    And Oel.Ship_From_Org_Id =P_Organization_Id
    And Oeh.Flow_Status_Code Not In ('CANCELLED')
    and oel.flow_status_code not in ('CANCELLED');

  return l_Mtd_sales;
 exception
 WHEN OTHERS THEN
   RETURN 0;
END;

Function GET_TOT_SALES (p_inventory_item_id NUMBER,p_organization_id NUMBER, P_Start_Date Date default null,P_End_Date Date default null)
RETURN Number
is
 l_Mtd_sales Number;
BEGIN

select  nvl(sum(oel.ordered_quantity),0)
Into l_Mtd_sales
    from  oe_order_headers oeh,
          oe_order_lines oel
    WHERE  TRUNC(ORDERED_DATE) BETWEEN NVL(P_START_DATE,G_DATE_FROM) AND NVL(P_END_DATE,G_DATE_TO)
    AND OEH.HEADER_ID        =OEL.HEADER_ID
     AND OEL.FLOW_STATUS_CODE <>'CANCELLED'
    and oel.inventory_item_id=p_inventory_item_id
    And Oel.Ship_From_Org_Id =P_Organization_Id    ;

  return l_Mtd_sales;
 exception
 WHEN OTHERS THEN
   RETURN 0;
END;

Function GET_TOT_SALES_DOLLORS(p_inventory_item_id NUMBER,p_organization_id NUMBER, P_Start_Date Date default null,P_End_Date Date default null)
RETURN Number
is
 l_Mtd_sales Number;
BEGIN

select  nvl(sum(decode(otl.order_category_code,'RETURN',(OEL.ordered_quantity*-1),OEL.ordered_quantity)),0) * Nvl(SUM(Unit_Selling_Price),0)
Into l_Mtd_sales
    from  oe_order_headers oeh,
          OE_ORDER_LINES OEL,
          OE_TRANSACTION_TYPES_VL otl,
          wsh_delivery_details wdd
    WHERE  1  =  1
    and TRUNC(ordered_date) BETWEEN nvl(P_Start_Date,G_DATE_FROM) AND nvl(P_End_Date,G_DATE_TO)
    AND OEH.HEADER_ID        =OEL.HEADER_ID
    AND OTL.ORG_ID           = OEL.ORG_ID
    AND OTL.TRANSACTION_TYPE_ID = OEL.LINE_TYPE_ID
    AND OEL.LINE_ID     = WDD.SOURCE_LINE_ID
    AND (WDD.RELEASED_STATUS IN ('C') OR OEL.FLOW_STATUS_CODE='CLOSED')
    and OEL.FLOW_STATUS_CODE <>'CANCELLED'
    and oel.inventory_item_id=p_inventory_item_id
    And Oel.Ship_From_Org_Id =P_Organization_Id    ;

  return l_Mtd_sales;
 exception
 WHEN OTHERS THEN
   RETURN 0;
END;


FUNCTION GET_TOT_SALES_DLR (P_LINE_ID NUMBER, P_START_DATE DATE DEFAULT NULL,P_END_DATE DATE DEFAULT NULL)
RETURN NUMBER
IS
 L_MTD_SALES NUMBER;
 l_cnt number;

BEGIN

/*SELECT COUNT(*)
into l_cnt
FROM WSH_DELIVERY_DETAILS WDD
WHERE  WDD.SOURCE_LINE_ID=P_LINE_ID
and released_status='C';

if l_cnt > 0 then*/

select nvl(OEL.ordered_quantity,0) * Nvl(Unit_Selling_Price,0)
Into l_Mtd_sales
    FROM  OE_ORDER_HEADERS OEH,
          OE_ORDER_LINES OEL,
          OE_TRANSACTION_TYPES_VL OTL
    WHERE  1  =  1
    and TRUNC(ordered_date) BETWEEN nvl(P_Start_Date,G_DATE_FROM) AND nvl(P_End_Date,G_DATE_TO)
    AND OEH.HEADER_ID        =OEL.HEADER_ID
    AND OTL.ORG_ID           = OEL.ORG_ID
    AND OTL.TRANSACTION_TYPE_ID = OEL.LINE_TYPE_ID
    AND OEL.FLOW_STATUS_CODE IN ('PRE-BILLING_ACCEPTANCE','CLOSED' )
    AND OEL.LINE_ID=P_LINE_ID   ;

--end if;
  return l_Mtd_sales;
 exception
 WHEN OTHERS THEN
   RETURN 0;
End;


FUNCTION GET_TOT_PRIZINGZONE_SALES_DLR (P_INVENTORY_ITEM_ID NUMBER,P_ORGANIZATION_ID NUMBER,P_ZONE varchar2,P_START_DATE DATE DEFAULT NULL,P_END_DATE DATE DEFAULT NULL)
RETURN NUMBER
IS
 L_PRICE_SALES NUMBER;

BEGIN

select NVL(SUM(nvl(OEL.ordered_quantity,0) * Nvl(Unit_Selling_Price,0)),0)
Into L_PRICE_SALES
    FROM  OE_ORDER_HEADERS OEH,
          OE_ORDER_LINES OEL,
          OE_TRANSACTION_TYPES_VL OTL,
          mtl_parameters mtp
    WHERE  1  =  1
    and TRUNC(ordered_date) BETWEEN nvl(P_Start_Date,G_DATE_FROM) AND nvl(P_End_Date,G_DATE_TO)
    AND OEH.HEADER_ID           = OEL.HEADER_ID
    AND OTL.ORG_ID              = OEL.ORG_ID
    AND OTL.TRANSACTION_TYPE_ID = OEL.LINE_TYPE_ID
    AND OEL.FLOW_STATUS_CODE    IN ('PRE-BILLING_ACCEPTANCE','CLOSED' )
    AND OEL.SHIP_FROM_ORG_ID    = MTP.ORGANIZATION_ID
    AND OEL.inventory_item_id   = P_INVENTORY_ITEM_ID
    --AND OEL.SHIP_FROM_ORG_ID    = P_ORGANIZATION_ID
    and MTP.ATTRIBUTE6          = P_ZONE;
    

--end if;
  return L_PRICE_SALES;
 exception
 WHEN OTHERS THEN
   RETURN 0;
End;


FUNCTION Get_Ytd_Sales (p_inventory_item_id NUMBER, p_organization_id NUMBER,P_Year_Start_Date Date)
RETURN Number
is
 l_Ytd_sales Number;
BEGIN

select  nvl(sum(oel.ordered_quantity),0)
Into l_Ytd_sales
    from  oe_order_headers oeh,
          oe_order_lines oel
    WHERE  TRUNC(ordered_date) BETWEEN P_Year_Start_Date AND TRUNC(sysdate)
    AND oeh.header_id        =oel.header_id
    and oel.inventory_item_id=p_inventory_item_id
    AND oel.ship_from_org_id =p_organization_id;

  return l_Ytd_sales;
 exception
 WHEN OTHERS THEN
   return null;
END;



Function Get_header_hold (p_order_header_id NUMBER,p_line_id NUMBER )
RETURN VARCHAR2
Is
 l_hold_name varchar2(400);
  l_cnt number;
BEGIN


select count(*)
Into l_cnt
    from    Oe_Order_Holds Hold,
    Oe_Hold_Sources hold_Source,
    Oe_Hold_Definitions Hold_Definition
    Where Hold.Header_Id=P_Order_Header_Id
    and hold.line_id  is null
     And Hold.Hold_Source_Id            = Hold_Source.Hold_Source_Id
  and hold_source.hold_id                   = hold_definition.hold_id
   and Hold.hold_release_id is null;

     if l_cnt >1 then
     return 'Multiple Holds Exist';
     end if;

select Hold_Definition.name
Into l_hold_name
    from    Oe_Order_Holds Hold,
    Oe_Hold_Sources hold_Source,
    Oe_Hold_Definitions Hold_Definition
    Where Hold.Header_Id=P_Order_Header_Id
    and Hold.line_id  is null
     And Hold.Hold_Source_Id            = Hold_Source.Hold_Source_Id
  And Hold_Source.Hold_Id                   = Hold_Definition.Hold_Id
   and Hold.hold_release_id is null;



  return l_hold_name;
 exception
 WHEN OTHERS THEN
   Return Null;
END;


Function Get_line_hold (p_order_header_id NUMBER,p_line_id NUMBER )
RETURN VARCHAR2
Is
 l_hold_name varchar2(400);
 l_cnt number;
BEGIN


select count(*)
Into l_cnt
    from    Oe_Order_Holds Hold,
    Oe_Hold_Sources hold_Source,
    Oe_Hold_Definitions Hold_Definition
    Where Hold.Header_Id=P_Order_Header_Id
     and hold.line_id  =p_line_id
     and hold.hold_source_id            = hold_source.hold_source_id
     and hold_source.hold_id                   = hold_definition.hold_id
       and Hold.hold_release_id is null;


     if l_cnt >1 then
     return 'Multiple Holds Exist';
     end if;


select Hold_Definition.name
Into l_hold_name
    from    Oe_Order_Holds Hold,
    Oe_Hold_Sources hold_Source,
    Oe_Hold_Definitions Hold_Definition
    Where Hold.Header_Id=P_Order_Header_Id
     and Hold.line_id  =p_line_id
     And Hold.Hold_Source_Id            = Hold_Source.Hold_Source_Id
     And Hold_Source.Hold_Id                   = Hold_Definition.Hold_Id
     and Hold.hold_release_id is null;


  return l_hold_name;
 exception
 WHEN OTHERS THEN
   Return Null;
END;

Function Get_header_hold_date (p_order_header_id NUMBER,p_line_id NUMBER )
RETURN date
Is
 l_hold_date date;
  l_cnt number;
BEGIN


/*select count(*)
Into l_cnt
    from    Oe_Order_Holds Hold,
    Oe_Hold_Sources hold_Source,
    Oe_Hold_Definitions Hold_Definition
    Where Hold.Header_Id=P_Order_Header_Id
    and hold.line_id  is null
     And Hold.Hold_Source_Id            = Hold_Source.Hold_Source_Id
  and hold_source.hold_id                   = hold_definition.hold_id
   and Hold.hold_release_id is null;

     if l_cnt >1 then
     return 'Multiple Holds Exist';
     end if;*/

select max(hold.creation_date)
--Hold_Definition.creation_date commented by srini
Into l_hold_date
    from    Oe_Order_Holds Hold,
    Oe_Hold_Sources hold_Source,
    Oe_Hold_Definitions Hold_Definition
    Where Hold.Header_Id=P_Order_Header_Id
    and Hold.line_id  is null
     And Hold.Hold_Source_Id            = Hold_Source.Hold_Source_Id
  And Hold_Source.Hold_Id                   = Hold_Definition.Hold_Id
   and Hold.hold_release_id is null;



  return l_hold_date;
 exception
 WHEN OTHERS THEN
   Return Null;
END;


function get_line_hold_date (p_order_header_id number,p_line_id number )
RETURN date
Is
 l_hold_date date;
 l_cnt number;
BEGIN


/*select count(*)
Into l_cnt
    from    Oe_Order_Holds Hold,
    Oe_Hold_Sources hold_Source,
    Oe_Hold_Definitions Hold_Definition
    Where Hold.Header_Id=P_Order_Header_Id
     and hold.line_id  =p_line_id
     and hold.hold_source_id            = hold_source.hold_source_id
     and hold_source.hold_id                   = hold_definition.hold_id
       and Hold.hold_release_id is null;


     if l_cnt >1 then
     return 'Multiple Holds Exist';
     end if;*/


select MAX(Hold.creation_date)
--Hold_Definition.creation_date commented by srini
Into l_hold_date
    from    Oe_Order_Holds Hold,
    Oe_Hold_Sources hold_Source,
    Oe_Hold_Definitions Hold_Definition
    Where Hold.Header_Id=P_Order_Header_Id
     and Hold.line_id  =p_line_id
     And Hold.Hold_Source_Id            = Hold_Source.Hold_Source_Id
     And Hold_Source.Hold_Id                   = Hold_Definition.Hold_Id
     and Hold.hold_release_id is null;


  return l_hold_date;
 exception
 WHEN OTHERS THEN
   return null;
END;

Function Get_Discount_Amt (p_order_header_id NUMBER,p_line_id NUMBER default null )
RETURN Number
IS
 l_adj_amt Number := 0;
BEGIN

select sum(nvl(adjusted_amount,0))
Into l_adj_amt
 FROM OE_PRICE_ADJUSTMENTS_V
     WHERE LIST_LINE_TYPE_CODE='DIS'
     And Header_Id=P_Order_Header_Id
     and line_id=nvl(p_line_id,line_id);


  return l_adj_amt;
 exception
 WHEN OTHERS THEN
   Return 0;
END;

Function Get_Special_cost (p_header_id NUMBER, p_line_id NUMBER, p_inventory_item_id NUMBER)
RETURN Number
is
 l_special_cost number := 0;
BEGIN

/*select discount_value
  INTO l_disc_amt
  from ozf_sd_batch_lines_all
 where order_header_id              = p_header_id
   and order_line_id                = p_line_id
   AND discount_type = 'NEWPRICE';*/
 select min(opa.operand)
  INTO l_special_cost
  from oe_price_adjustments_v opa,
       ozf_sd_request_headers_all_b  osh,
       ozf_sd_request_lines_all osl
 where opa.adjustment_name   = osh.request_number
   AND osh.request_header_id = osl.request_header_id
   AND osl.inventory_item_id = p_inventory_item_id
   AND opa.header_id         = p_header_id
   and opa.line_id           = p_line_id;

  return l_special_cost;
 exception
 WHEN OTHERS THEN
   Return 0;
END;

Function get_ssd_req_number (p_header_id NUMBER, p_line_id NUMBER, p_inventory_item_id NUMBER)
RETURN VARCHAR2
is
 l_ssd_number VARCHAR2(100);
BEGIN

/*select discount_value
  INTO l_disc_amt
  from ozf_sd_batch_lines_all
 where order_header_id              = p_header_id
   and order_line_id                = p_line_id
   AND discount_type = 'NEWPRICE';*/
 select min(osh.request_number)
  INTO l_ssd_number
  from oe_price_adjustments_v opa,
       ozf_sd_request_headers_all_b  osh,
       ozf_sd_request_lines_all osl
 where opa.adjustment_name   = osh.request_number
   AND osh.request_header_id = osl.request_header_id
   AND osl.inventory_item_id = p_inventory_item_id
   AND opa.header_id         = p_header_id
   and opa.line_id           = p_line_id;

  return l_ssd_number;
 exception
 WHEN OTHERS THEN
   Return 0;
END;

Function Get_Freight_Amt (p_order_header_id NUMBER,p_line_id NUMBER default null)
RETURN Number
IS
 l_adj_amt Number;
BEGIN

select sum(nvl(adjusted_amount,0))
Into l_adj_amt
 FROM OE_PRICE_ADJUSTMENTS_V
     WHERE LIST_LINE_TYPE_CODE='FREIGHT_CHARGE'
     And Header_Id=P_Order_Header_Id
     and line_id=nvl(p_line_id,line_id);


  return l_adj_amt;
 exception
 WHEN OTHERS THEN
   Return 0;
END;


PROCEDURE SET_DATE_FROM (P_DATE_FROM DATE )
IS
BEGIN
G_DATE_FROM :=P_DATE_FROM;
END;

PROCEDURE set_date_to (p_date_to DATE )
IS
BEGIN
G_DATE_TO :=p_date_to;
END;


FUNCTION GET_PERIOD  RETURN VARCHAR2
IS
BEGIN
return G_PERIOD_NAME;

FND_FILE.PUT_LINE(FND_FILE.LOG,'GET PERIOD NAME'||G_PERIOD_NAME);
EXCEPTION
WHEN OTHERS THEN
FND_FILE.PUT_LINE(FND_FILE.LOG,'Error Message'||SQLCODE||SQLERRM);
return null;
END;

FUNCTION GET_DATE_FROM  RETURN DATE
IS
BEGIN
return G_DATE_FROM;

FND_FILE.PUT_LINE(FND_FILE.LOG,'Get date'||G_DATE_FROM);
EXCEPTION
WHEN OTHERS THEN
FND_FILE.PUT_LINE(FND_FILE.LOG,'Error Message'||SQLCODE||SQLERRM);
return null;
END;



FUNCTION get_date_to  RETURN DATE
IS
BEGIN
RETURN G_DATE_TO;
End;


FUNCTION GET_CANCELLED_QTY (p_inventory_item_id NUMBER, p_organization_id NUMBER)
RETURN Number
IS
 l_CANCELLED_QTY Number;
BEGIN

select sum(oel.ordered_quantity)
Into l_CANCELLED_QTY
    from  oe_order_headers oeh,
          OE_ORDER_LINES OEL
    WHERE  TRUNC(ordered_date) BETWEEN G_DATE_FROM AND G_DATE_TO
    AND oeh.header_id        =oel.header_id
    and oel.inventory_item_id=p_inventory_item_id
    and oel.ship_from_org_id =p_organization_id
    and oel.flow_status_code    in ('CANCELLED') ;
    return l_CANCELLED_QTY;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;

FUNCTION get_line_disp_qty (p_inventory_item_id NUMBER, p_organization_id NUMBER)
RETURN Number
is
 l_DISP_QTY Number;
BEGIN

select sum(oel.ordered_quantity)
Into l_DISP_QTY
    from  oe_order_headers oeh,
          OE_ORDER_LINES OEL
    WHERE  TRUNC(ordered_date) BETWEEN G_DATE_FROM AND G_DATE_TO
    AND oeh.header_id        =oel.header_id
    and oel.inventory_item_id=p_inventory_item_id
    and oel.ship_from_org_id =p_organization_id
    and OEL.SHIP_FROM_ORG_ID <>oeh.SHIP_FROM_ORG_ID;
  return l_DISP_QTY;
 exception
 WHEN OTHERS THEN
   return null;
END;



FUNCTION GET_CANCELLED_COUNT (p_inventory_item_id NUMBER, p_organization_id NUMBER)
RETURN Number
IS
 l_CANCELLED_COUNT Number;
BEGIN

select COUNT(oel.LINE_ID)
Into l_CANCELLED_COUNT
    from  oe_order_headers oeh,
          OE_ORDER_LINES OEL
    WHERE  TRUNC(ordered_date) BETWEEN G_DATE_FROM AND G_DATE_TO
    AND oeh.header_id        =oel.header_id
    and oel.inventory_item_id=p_inventory_item_id
    and oel.ship_from_org_id =p_organization_id
    and oel.flow_status_code    in ('CANCELLED') ;
    RETURN l_CANCELLED_COUNT;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;

FUNCTION get_line_disp_COUNT (p_inventory_item_id NUMBER, p_organization_id NUMBER)
RETURN Number
is
 l_DISP_COUNT Number;
BEGIN

select  COUNT(oel.LINE_ID)
Into l_DISP_COUNT
    from  oe_order_headers oeh,
          OE_ORDER_LINES OEL
    WHERE  TRUNC(ordered_date) BETWEEN G_DATE_FROM AND G_DATE_TO
    AND oeh.header_id        =oel.header_id
    and oel.inventory_item_id=p_inventory_item_id
    and oel.ship_from_org_id =p_organization_id
    and OEL.SHIP_FROM_ORG_ID <>oeh.SHIP_FROM_ORG_ID;
  return l_DISP_COUNT;
 exception
 WHEN OTHERS THEN
   return null;
END;



Procedure Set_period_name_from (P_period_name_from varchar)
Is
Begin
G_Period_Name_From:=P_Period_Name_From;
end;

Procedure Set_period_name_to (P_period_name_to varchar)
Is
Begin
G_Period_Name_to:=P_period_name_to;
end;


FUNCTION get_period_name_from  RETURN varchar
IS
BEGIN
return G_PERIOD_NAME_from;
End;

FUNCTION get_period_name_to  RETURN varchar
IS
Begin
Return G_Period_Name_to;
End;

Function get_last_year_period_name(p_ledger_id number)  RETURN varchar
IS
l_period_year number;
l_period_num number;
l_period_name varchar2(50);
l_set_of_books_id number;
BEGIN
  select  gp.period_year,gp.period_num
  into l_period_year,l_period_num
  from  gl_periods gp, gl_ledgers gl
  WHERE gp.period_set_name = gl.period_set_name
  and period_name=G_PERIOD_NAME
  and gl.ledger_id=p_ledger_id;

  select  gps.period_name
  into l_period_name
  from  gl_period_statuses gps
  WHERE  gps.APPLICATION_ID          =101
  and gps.SET_OF_BOOKS_ID=p_ledger_id
  and period_year=(l_period_year-1)
  and period_num=l_period_num;

return l_period_name;
End;



Function get_period_end_date(p_period_name varchar2,p_ledger_id number)   RETURN Date
IS
l_period_date date;
BEGIN
    select  end_date
    into l_period_date
    from  gl_periods gp, gl_ledgers gl
    WHERE gp.period_set_name = gl.period_set_name
     and  gl.ledger_id = P_LEDGER_ID
     AND  period_name=P_period_name;
     return l_period_date;
     exception
 WHEN OTHERS THEN
   return null;
END;


Function get_period_Start_date(p_period_name varchar2,p_ledger_id number)   RETURN Date
IS
l_period_date date;
BEGIN
    select  start_date
    into l_period_date
    from  gl_periods gp, gl_ledgers gl
    WHERE gp.period_set_name = gl.period_set_name
     and  gl.ledger_id = P_LEDGER_ID
     AND  period_name=P_period_name;
     return l_period_date;
     Exception
 WHEN OTHERS THEN
   Return Null;
END;

Function get_year_start_date (p_period_name varchar2,p_ledger_id number)  RETURN Date
IS
l_period_date date;
BEGIN
     select  gps.year_start_date
  into l_period_date
  from  gl_period_statuses gps
  WHERE  gps.APPLICATION_ID          =101
   and set_of_books_id= p_ledger_id
  and period_name=p_period_name;
  return l_period_date;
exception
 WHEN OTHERS THEN
   return null;
End;

Function Get_tax_jurdisjiction_code (p_tax_line_id NUMBER )
RETURN VARCHAR2
Is
l_jurisdiction_code varchar2(600);
begin
   SELECT tax_jurisdiction_code
    Into l_jurisdiction_code
    FROM zx_lines_v
    WHERE tax_line_id=p_tax_line_id;
    Return l_jurisdiction_code;
End;

Function Get_shipping_mthd (p_FREIGHT_CODE VARCHAR2) RETURN VARCHAR2
Is
l_description varchar2(600);
begin
    SELECT DISTINCT OF_SHIP_VIA.DESCRIPTION
    Into l_description
    FROM ORG_FREIGHT OF_SHIP_VIA
    WHERE  OF_SHIP_VIA.FREIGHT_CODE=p_FREIGHT_CODE;
     Return l_description;
End;

PROCEDURE set_getpaid_data (p_custno VARCHAR2, p_trx_number varchar2) 
IS
l_shipvia varchar2(600);
begin
    
        SELECT shipvia,warehse,ordnum, invno
        Into G_CNV_INV_TAB.SHIPVIA,G_CNV_INV_TAB.warehse,G_CNV_INV_TAB.ordnum, G_CNV_INV_TAB.INVNO
        FROM xxwc.XXWC_AREXTI_GETPAID_DUMP_TBL
        WHERE  custno = p_custno
          AND  invno  = p_trx_number;
End;

Function Get_shipvia (p_custno VARCHAR2, p_trx_number varchar2) RETURN VARCHAR2
Is
l_shipvia varchar2(600);
begin
   
   IF G_CNV_INV_TAB.custno = p_custno and G_CNV_INV_TAB.INVNO = p_trx_number THEN 
     RETURN G_CNV_INV_TAB.SHIPVIA;
   ELSE  
       set_getpaid_data(p_custno,p_trx_number);          
  END IF; 
  RETURN G_CNV_INV_TAB.SHIPVIA;    
End;

Function Get_WareHse (p_custno VARCHAR2, p_trx_number varchar2) RETURN VARCHAR2
Is
l_shipvia varchar2(600);
begin
   
   IF G_CNV_INV_TAB.custno = p_custno and G_CNV_INV_TAB.INVNO = p_trx_number THEN 
     RETURN G_CNV_INV_TAB.WAREHSE;
   ELSE  
       set_getpaid_data(p_custno,p_trx_number);          
  END IF; 
  RETURN G_CNV_INV_TAB.WAREHSE;    
End;


Function Get_ORDNUM (p_custno VARCHAR2, p_trx_number varchar2) RETURN VARCHAR2
Is
l_shipvia varchar2(600);
begin
   
   IF G_CNV_INV_TAB.custno = p_custno and G_CNV_INV_TAB.INVNO = p_trx_number THEN 
     RETURN G_CNV_INV_TAB.ORDNUM;
   ELSE  
       set_getpaid_data(p_custno,p_trx_number);          
  END IF; 
  RETURN G_CNV_INV_TAB.ORDNUM;    
End;



Function Get_po_cost (P_requisition_header_id NUMBER,P_po_header_id NUMBER) RETURN NUMBER
Is
l_po_cost Number;
Begin
    SELECT ROUND(SUM(quantity*unit_price),2)
    Into l_po_cost
    FROM po_lines pl2,
         oe_drop_ship_sources od1
    WHERE pl2.po_line_id           = od1.po_line_id
    AND  od1.requisition_header_id = P_requisition_header_id
    AND  od1.po_header_id          = P_po_header_id;
    Return l_po_cost;
End;

Function Get_shipping_mthd2 (p_party_id NUMBER) RETURN VARCHAR2
Is
l_description varchar2(600);
begin
    SELECT DISTINCT OF_SHIP_VIA.DESCRIPTION
    Into l_description
    FROM ORG_FREIGHT OF_SHIP_VIA
    WHERE p_party_id = OF_SHIP_VIA.party_id;
     Return l_description;
End;


Function Get_shipping_mthd3 (p_FREIGHT_CODE VARCHAR2) RETURN VARCHAR2
Is
l_description varchar2(600);
begin

      SELECT DISTINCT  MEANING
      INTO L_DESCRIPTION
      FROM FND_LOOKUP_VALUES_VL
      WHERE  LOOKUP_TYPE          = 'SHIP_METHOD'
      AND END_DATE_ACTIVE IS NULL
      and lookup_code=p_FREIGHT_CODE;


      IF UPPER(L_DESCRIPTION) LIKE UPPER('%Will%Call%') OR UPPER(L_DESCRIPTION) LIKE UPPER('%Walk%In%')  THEN

         RETURN 'Will Call';

      ELSIF UPPER(L_DESCRIPTION) LIKE upper('%Our%Truck%') OR upper(L_DESCRIPTION) LIKE  upper('%Salesperson%Delivery%')   THEN

          RETURN 'Our Truck';

      ELSIF UPPER(L_DESCRIPTION) LIKE UPPER('%UPS') OR UPPER(L_DESCRIPTION) LIKE UPPER('%FEDEX EAST-Parcel-Ground') OR UPPER(L_DESCRIPTION) LIKE UPPER('%FEDEX NATIONAL-LTL-Ground')   THEN

          RETURN '3rd Party Deliveries';

      ELSE
          RETURN '3rd Party Deliveries';
      END IF;

exception
 WHEN OTHERS THEN
   RETURN NULL;

End;


Function Get_shipping_mthd4 (p_FREIGHT_CODE VARCHAR2) RETURN VARCHAR2
Is
l_description varchar2(600);
begin

      SELECT DISTINCT  MEANING
      INTO L_DESCRIPTION
      FROM FND_LOOKUP_VALUES_VL
      WHERE  LOOKUP_TYPE          = 'SHIP_METHOD'
      And End_Date_Active Is Null
      And Lookup_Code=P_Freight_Code;

      return l_description;

exception
 WHEN OTHERS THEN
   RETURN NULL;

End;

Function Get_rental_start_date (P_line_id NUMBER) RETURN Date
Is
l_date Date;
Begin

   SELECT TRUNC(oll.actual_shipment_date)
   Into l_date
    FROM oe_order_lines oll ,
         oe_transaction_types_vl ott1
    WHERE oll.line_type_id        =ott1.transaction_type_id
    AND ott1.name='RENTAL LINE'
    And Oll.Line_Id                =P_Line_Id
  ;

    Return l_date;

end;

Function Get_rental_days (P_header_id NUMBER,p_link_to_line_id number,p_line_id NUMBER) RETURN Number
Is
L_Days           number;
l_shipment_date  date;
l_flow_status_code varchar2(50);
l_creation_date  date;
Begin
     /* Select trunc(Date_Scheduled)
      Into  L_Shipment_Date
      From  Wsh_Delivery_Details
      Where Source_Header_Id=P_Header_Id
      and   source_line_id=P_line_id;*/

   SELECT oll.actual_shipment_date
   Into l_shipment_date
    FROM oe_order_lines oll ,
         oe_transaction_types_vl ott1
    Where Oll.Line_Type_Id        =Ott1.Transaction_Type_Id
    and ott1.name                 ='RENTAL LINE'
    and oll.line_id               =p_link_to_line_id  ;

   select case when shipment_line_status_code='FULLY RECEIVED' then creation_date-l_shipment_date
  else  sysdate-l_shipment_date
  end
  into L_Days
    From Rcv_Shipment_Lines Rsl
    Where Rsl.Oe_Order_Line_Id        =p_line_id   ;

-- (trunc(creation_date - add_months(l_shipment_date,trunc(months_between(creation_date,l_shipment_date) ))))
-- Else  round((Sysdate)-L_Shipment_Date)
         --  end

    return l_days;
  exception  when others then
  --  return null;
 return round((Sysdate)-L_Shipment_Date);
end;

function GET_ONHAND_INV (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number)
Return Number
is
 l_Onhand_Inv Number;
BEGIN

   select nvl(sum(moq.transaction_quantity),0)
    Into  L_Onhand_Inv
    from mtl_onhand_quantities_detail moq
    Where Inventory_Item_Id          =P_Inventory_Item_Id
    and organization_id              =p_organization_id;

  return l_Onhand_Inv;
 exception
 WHEN OTHERS THEN
   return 0;
END;

function GET_oldest_born_date (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number)
Return date
is
 l_born_date date;
BEGIN

   select trunc(min(orig_date_received))
    Into  l_born_date
    from mtl_onhand_quantities_detail moq
    Where Inventory_Item_Id          =P_Inventory_Item_Id
    and organization_id              =p_organization_id;

  return l_born_date;
 exception
 WHEN OTHERS THEN
   return null;
END;


function GET_ONHAND_date (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number)
Return date
is
 l_onhand_date date;
BEGIN

   select max(date_received)
    Into  l_onhand_date
    from mtl_onhand_quantities_detail moq
    Where Inventory_Item_Id          =P_Inventory_Item_Id
    and organization_id              =p_organization_id;

  return l_onhand_date;
 exception
 WHEN OTHERS THEN
   return null;
END;

function GET_ONHAND_qty (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number,p_date date default null)
Return Number
is
 l_Onhand_qty Number;
BEGIN

   select nvl(sum(moq.transaction_quantity),0)
    into  l_Onhand_qty
    from mtl_material_transactions moq
    where INVENTORY_ITEM_ID          =P_INVENTORY_ITEM_ID
    And Organization_Id             =P_Organization_Id
    and trunc(transaction_date)<=nvl(p_date,g_date_to);

  return l_Onhand_qty;
 exception
 WHEN OTHERS THEN
   return 0;
END;


function Get_Sub_Onhand_qty (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number,p_date date default null)
Return Number
IS
 l_Sub_Onhand_qty Number;
BEGIN

   select nvl(sum(moq.transaction_quantity),0)
    into  l_Sub_Onhand_qty
      From Mtl_Item_Sub_Inventories  Mis,
           mtl_material_transactions moq,
           Mtl_Secondary_Inventories Msci
      where  moq.inventory_item_id = mis.inventory_item_id
      And    moq.Organization_Id = Mis.Organization_Id
      And    Mis.Secondary_Inventory = Msci.Secondary_Inventory_Name
      And	   Mis.Organization_Id = Msci.Organization_Id
      And upper(Msci.secondary_inventory_name) in('DBRENTAL','DRRENTAL','LOSTRENTAL','RENTAL','RENTAL N/R','RENTALHLD')
      And Moq.Inventory_Item_Id          =P_Inventory_Item_Id
      AND MOQ.ORGANIZATION_ID             =P_ORGANIZATION_ID
      and trunc(transaction_date)<=nvl(p_date,g_date_to);
  return l_Sub_Onhand_qty;
 exception
 WHEN OTHERS THEN
   RETURN 0;
END;

function GET_item_cost (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number)
return number
is

l_item_cost number;
begin
  select Actual_Cost
  into l_item_cost
  from  mtl_material_transactions
  where TRANSACTION_ID =
                        ( select max(transaction_id)
                          from mtl_material_transactions moq
                          Where Inventory_Item_Id          =P_Inventory_Item_Id
                          And Organization_Id              =P_Organization_Id
                          And Trunc(Transaction_Date)      <=G_Date_To
                          );
     return L_ITEM_COST;
 exception
 WHEN OTHERS THEN
   return null;
END;

function Get_item_demand_qty (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number)
Return Number
IS
 l_demand_qty Number;
BEGIN

   Select Sum(Ol.Ordered_Quantity)
    INTO  L_DEMAND_QTY
    From Oe_Order_Lines  Ol
    WHERE OL.INVENTORY_ITEM_ID         =P_INVENTORY_ITEM_ID
    AND OL.SHIP_FROM_ORG_ID            =P_ORGANIZATION_ID
    and flow_status_code not in ('CLOSED','CANCELLED');

  return l_demand_qty;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;


function Get_Item_Crossref (P_INVENTORY_ITEM_ID Number,p_organization_id Number)
Return VARCHAR2
IS
 l_cross_ref VARCHAR2(255);
BEGIN

   select cross_reference
    Into  L_Cross_Ref
    From Mtl_Cross_References_V  Mcr
    Where Mcr.Inventory_Item_Id   = P_Inventory_Item_Id
    And Mcr.Cross_Reference_Type  = 'VENDOR'
    and nvl(mcr.organization_id,p_organization_id)=p_organization_id;

  return l_cross_ref;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;

/*FUNCTION Get_primary_bin_loc (P_INVENTORY_ITEM_ID NUMBER, P_ORGANIZATION_ID NUMBER)
RETURN VARCHAR2
IS
 l_seg VARCHAR2(5000);
BEGIN

    Select Segment1
    Into  L_Seg
    From Mtl_Item_Locations_Kfv
    Where Inventory_Item_Id  = P_Inventory_Item_Id
    AND ORGANIZATION_ID      =P_ORGANIZATION_ID
    and segment1  like '1%';


  return l_seg;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;*/

FUNCTION Get_primary_bin_loc (P_INVENTORY_ITEM_ID NUMBER, P_ORGANIZATION_ID NUMBER)
RETURN VARCHAR2
IS
 l_seg VARCHAR2(5000);
BEGIN

    Select mil.Segment1
    INTO  L_SEG
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
         MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID  = P_INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID      =P_ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR = MIL.INVENTORY_LOCATION_ID
    and mil.segment1  like '1%';


  return l_seg;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;

/*FUNCTION GET_ALTERNATE_BIN_LOC (P_INVENTORY_ITEM_ID NUMBER, P_ORGANIZATION_ID NUMBER)
RETURN VARCHAR2
IS
 l_str VARCHAR2(5000);
BEGIN

   FOR GET_BIN_LOC IN( SELECT CONCATENATED_SEGMENTS
  --  INTO  l_str
    From Mtl_Item_Locations_Kfv
    Where Inventory_Item_Id  = P_Inventory_Item_Id
    AND ORGANIZATION_ID      = P_ORGANIZATION_ID
    and segment1 not like '1%')
    LOOP
    l_str:=l_str||','||GET_BIN_LOC.CONCATENATED_SEGMENTS;
    END LOOP;

  return substr(l_str,2);
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;*/

FUNCTION GET_ALTERNATE_BIN_LOC (P_INVENTORY_ITEM_ID NUMBER, P_ORGANIZATION_ID NUMBER)
RETURN VARCHAR2
IS
 l_str VARCHAR2(5000);
BEGIN

   FOR GET_BIN_LOC IN( SELECT mil.CONCATENATED_SEGMENTS
  --  INTO  l_str
    FROM MTL_ITEM_LOCATIONS_KFV  MIL,
         MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID  = P_INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID      = P_ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR = MIL.INVENTORY_LOCATION_ID
    and mil.segment1 not like '1%')
    LOOP
    l_str:=l_str||','||GET_BIN_LOC.CONCATENATED_SEGMENTS;
    END LOOP;

  return substr(l_str,2);
 exception
 WHEN OTHERS THEN
   RETURN NULL;
END;

function get_bin_loc_flag (p_inventory_item_id number,p_organization_id number) return varchar2
is
l_return_flag varchar2(1);
BEGIN

    IF g_start_bin IS NULL  AND g_end_bin IS NULL THEN
        RETURN 'Y';
   elsif g_start_bin IS not NULL  AND g_end_bin IS not NULL THEN

        Select 'Y'
        INTO  l_return_flag
        FROM MTL_ITEM_LOCATIONS_KFV MIL,
             MTL_SECONDARY_LOCATORS MSL
        where msl.inventory_item_id  = p_inventory_item_id
        AND MSL.ORGANIZATION_ID      =P_ORGANIZATION_ID
        AND msl.secondary_locator = mil.inventory_location_id
        AND substr(mil.segment1,3) BETWEEN g_start_bin AND g_end_bin;

        RETURN l_return_flag;
    elsif g_start_bin IS NOT NULL  AND g_end_bin IS NULL  THEN

        Select 'Y'
        INTO  l_return_flag
        FROM MTL_ITEM_LOCATIONS_KFV MIL,
             MTL_SECONDARY_LOCATORS MSL
        where msl.inventory_item_id  = p_inventory_item_id
        AND MSL.ORGANIZATION_ID      =P_ORGANIZATION_ID
        AND msl.secondary_locator = mil.inventory_location_id
        AND substr(mil.segment1,3 )>= g_start_bin ;

        RETURN l_return_flag;

    elsif g_start_bin IS  NULL  AND g_end_bin IS NOT NULL  THEN

        Select 'Y'
        INTO  l_return_flag
        FROM MTL_ITEM_LOCATIONS_KFV MIL,
             MTL_SECONDARY_LOCATORS MSL
        where msl.inventory_item_id  = p_inventory_item_id
        AND MSL.ORGANIZATION_ID      =P_ORGANIZATION_ID
        AND msl.secondary_locator = mil.inventory_location_id
        AND substr(mil.segment1,3 )<= g_end_bin ;
        RETURN l_return_flag;
 end if;



     exception
 when others then
   RETURN 'N';
End;

FUNCTION GET_FINAL_END_QTY (P_INVENTORY_ITEM_ID NUMBER, P_ORGANIZATION_ID NUMBER,P_DATE DATE)
Return Number
IS
 l_Final_End_Qty Number;
BEGIN

   Select Sum(mmt.Transaction_Quantity)
    Into l_Final_End_Qty
    From  Mtl_Material_Transactions Mmt
    Where 1                          =1
    And Trunc(Mmt.Transaction_Date)  <= P_Date
    And Inventory_Item_Id            = P_Inventory_Item_Id
    AND ORGANIZATION_ID              = P_ORGANIZATION_ID;

  RETURN L_FINAL_END_QTY;

 EXCEPTION
 WHEN OTHERS THEN
   RETURN NULL;
END;


Function Get_Schedule_Part_Flag (P_Header_Id Number, P_Line_Id Number ) Return Varchar2
Is
l_return varchar2(10);
Begin

    Select 'Yes'
    Into L_Return
    From Oe_Price_Adjustments_V
    Where upper(Adjustment_Name) LIKE 'GSA%SCHEDULE%'
    And Header_Id=P_Header_Id
    And Line_Id=P_Line_Id;

    Return L_Return;

Exception When Others Then
Return 'No';

end;


Function Get_Sale_Cost (P_Customer_Trx_Id Number)  Return Number
Is
l_amount number;
Begin

    Select Sum (Ol.Ordered_Quantity*nvl(Ol.Unit_Cost,0))
    Into L_Amount
    from  ra_customer_trx rct,
          Ra_Customer_Trx_Lines Rctl,
          Oe_Order_Headers Oh,
          Oe_Order_Lines   Ol,
          Oe_Payments      Oep
    Where Rctl.Customer_Trx_Id     = Rct.Customer_Trx_Id
    And Rctl.Sales_Order_Line      = Ol.Line_Number
    And Rctl.Inventory_Item_Id     = Ol.Inventory_Item_Id
    And Ol.Header_Id               = Oh.Header_Id
    And To_Char(Oh.Order_Number)   = To_Char(Rct.Interface_Header_Attribute1)
    And Rctl.Customer_Trx_Id       = P_Customer_Trx_Id
    And Oep.Header_Id              = Ol.Header_Id
    And Oep.Line_Id                = Ol.Line_Id;

    return l_amount;

    Exception When Others Then
    Return 0;
End;

FUNCTION Get_open_sales_orders(p_inventory_item_id NUMBER,p_organization_id   NUMBER)
  RETURN number
Is
  L_Order_Numbers Number;
Begin

  SELECT COUNT(oh.order_number)
  INTO l_order_numbers
  FROM OE_ORDER_HEADERS OH,OE_ORDER_LINES OL
  WHERE oh.header_id=ol.header_id
  AND ol.inventory_item_id     = p_inventory_item_id
  AND ol.ship_from_org_id      = p_organization_id
  AND ol.flow_status_code NOT IN ('CLOSED','CANCELLED');

  RETURN l_order_numbers;

EXCEPTION
WHEN OTHERS THEN
  RETURN NULL;
END;


Function Get_Effective_Period_Num(P_Period Varchar2,P_Ledger_Id In Number ) Return Number
Is
l_effective_period_num gl_period_statuses.effective_period_num%type;

begin

        select gp.effective_period_num
        into   l_effective_period_num
        from   gl_period_statuses gp
        where gp.ledger_id	=	p_ledger_id
        and gp.period_name	=	p_period
        and gp.application_id=	101;

Return L_Effective_Period_Num;

Exception
WHEN OTHERS THEN
  Return Null;
END;

FUNCTION GET_INV_CAT_CLASS (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
begin
        SELECT Mcv.concatenated_segments
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Inventory Category'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  RETURN NULL;
END;

FUNCTION get_inv_gm (P_Inventory_Item_Id Number ,P_Organization_Id Number, p_item_category_id NUMBER) RETURN NUMBER
is
l_min_margin number;
begin
        SELECT max(xopg.min_margin)
        INTO l_min_margin
        from  mtl_parameters mtp,
              xxwc.xxwc_om_pricing_guardrail xopg            
         Where nvl(xopg.item_category_id,p_item_category_id)   = p_item_category_id
           AND (P_Inventory_Item_Id is null or nvl(xopg.inventory_item_id,P_Inventory_Item_Id) = P_Inventory_Item_Id)
           and xopg.price_zone              = mtp.attribute6
           And mtp.Organization_Id          = P_Organization_Id
           AND xopg.status ='CURRENT';
           
  Return l_min_margin;

Exception
WHEN OTHERS THEN
  return null;
END;



FUNCTION Get_inv_cat_seg1 (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
Begin
        SELECT Mcv.SEGMENT1
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Inventory Category'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  RETURN NULL;
End;


FUNCTION Get_inv_prod_cat_class (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
BEGIN
        SELECT Mcv.SEGMENT2
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Product'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  return null;
END;


FUNCTION Get_inv_vel_cat_class (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
BEGIN
        SELECT Mcv.SEGMENT1
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Sales Velocity'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  Return Null;
END;


FUNCTION Get_inv_cat_class_desc (P_Inventory_Item_Id NUMBER ,p_organization_id NUMBER) RETURN VARCHAR2
Is
l_cat_class_desc varchar2(400);
Begin
      select Flex_Val.Description
      INTO l_cat_class_desc
      from  mtl_categories_kfv mcv,
            mtl_category_sets mcs,
            mtl_item_categories mic,
            fnd_flex_values_vl flex_val,
            fnd_flex_value_sets flex_name
          Where   Mcs.Category_Set_Name      ='Inventory Category'
        And Mcs.Structure_Id                 = Mcv.Structure_Id
        And Mic.Category_Set_Id              = Mcs.Category_Set_Id
        And Mic.Category_Id                  = Mcv.Category_Id
        And Flex_Value                       = Mcv.Segment2
        And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
        And Mic.Organization_Id              = P_Organization_Id
        And Flex_Name.Flex_Value_Set_Id      = Flex_Val.Flex_Value_Set_Id
        And Flex_Name.Flex_Value_Set_Name    = 'XXWC_CATEGORY_CLASS';

  Return L_Cat_Class_Desc;

Exception
WHEN OTHERS THEN
  Return Null;
END;

function get_reason_code(p_header_id number,p_line_id number) return varchar2
Is
l_reason_code varchar2(500);
Begin

    Select Reason_Code
    into l_reason_code
    From Oe_Reasons
    Where Header_Id=P_Header_Id
    And Entity_Code='LINE'
    And Entity_Id=P_Line_Id;

    return l_reason_code;

Exception
WHEN OTHERS THEN
  RETURN NULL;
END;

Function Get_Sales_Profit(P_Header_Id Number ) Return number
Is
L_Gross_Profit Number;
Begin

              Select round(Sum(Ordered_Quantity*nvl(Unit_Selling_Price,0))-Sum(Ordered_Quantity*nvl(Unit_Cost,0)),2)
              into l_gross_profit
              From Oe_Order_Lines
              Where Header_Id=P_Header_Id
              And Flow_Status_Code<>'CANCELLED';

              return l_gross_profit;
Exception
WHEN OTHERS THEN
  RETURN NULL;
END;

function get_sales_profit_percentage(p_header_id number) return number
Is
L_Gross_Profit_Per Number;
L_Order_Sales Number;
l_sales_cost number;
Begin

      Select Sum(Ordered_Quantity*Nvl(Unit_Selling_Price,0)),
             Sum(Ordered_Quantity*Nvl(Unit_Cost,0))
      Into   L_Order_Sales,
             L_Sales_Cost
      From   Oe_Order_Lines
      Where  Header_Id=P_Header_Id
      and    flow_status_code <> 'CANCELLED';

      If L_Order_Sales > 0 Then
          L_Gross_Profit_Per:=((L_Order_Sales-L_Sales_Cost)/L_Order_Sales)*100;
      Else L_Gross_Profit_Per:=0;
      End If;

      Return L_Gross_Profit_Per;

Exception
WHEN OTHERS THEN
  Return Null;
END;

procedure set_payment_type (p_payment_type varchar2 )
is
begin
G_Payment_type:=p_payment_type;
End;

function get_payment_type  return varchar2
is
begin
Return G_Payment_type;
End;

FUNCTION Get_gsa_modifier_amt  (p_product_val VARCHAR2,p_list_price_amt number) RETURN number
IS
l_arithmetic_operator VARCHAR2(20);
l_value NUMBER;
l_gsa_modifier_amt number;
Begin

    SELECT  ql.arithmetic_operator,ql.operand
    INTO   l_arithmetic_operator,l_value
    FROM   qp_list_headers qh,
           qp_modifier_summary_v ql
    Where  Qh.List_Header_Id                       = Ql.List_Header_Id
     and   qh.name                                 = 'GSA PRICING'
    and    ql.product_attr_value                   = p_product_val;
   -- AND     nvl(trunc(ql.end_date_active),trunc(SYSDATE))>=trunc(SYSDATE);

    if    L_Arithmetic_Operator ='%' then
          l_gsa_modifier_amt:=((p_list_price_amt)*(l_value/100));

          if nvl(p_list_price_amt ,0) =0 then
             return l_gsa_modifier_amt;
          else
              return (p_list_price_amt-l_gsa_modifier_amt);
          end  if;

    elsif l_arithmetic_operator='NEWPRICE' then
          return l_value;
    ELSE
        RETURN 0;
    end if;

    exception WHEN others THEN
    return  0;

End;



FUNCTION Get_gsa_disc_amt  (p_product_val VARCHAR2,p_list_price_amt number) RETURN number
IS
l_arithmetic_operator VARCHAR2(20);
l_value NUMBER;
l_gsa_modifier_amt number;
Begin

    SELECT  ql.arithmetic_operator,ql.operand
    INTO   l_arithmetic_operator,l_value
    FROM   qp_list_headers qh,
           qp_modifier_summary_v Ql
    Where  Qh.List_Header_Id                       = Ql.List_Header_Id
     And   Qh.Name                                 = 'GSA PRICING'
    And    Ql.Product_Attr_Value                   = P_Product_Val
    AND     nvl(trunc(ql.end_date_active),trunc(SYSDATE))>=trunc(SYSDATE);

    if    L_Arithmetic_Operator ='%' then
          l_gsa_modifier_amt:=((p_list_price_amt)*(l_value/100));
    Elsif L_Arithmetic_Operator='NEWPRICE' Then
          l_gsa_modifier_amt:=l_value;
    ELSE
        RETURN 0;
    END IF;

     RETURN l_gsa_modifier_amt;

    exception WHEN others THEN
    return  null;

End;

Function Get_Adj_Auto_Modifier_Amt  (P_Header_Id Number,P_Line_Id Number) Return Number
Is
L_Adj_Amt Number;

Begin

  Select nvl((Sum(Adj_Auto.Adjusted_Amount)*(-1)),0)
  Into L_Adj_Amt
  from Oe_Price_Adjustments_V Adj_Auto
  Where  Adj_Auto.Header_Id    = P_Header_Id
  And    Adj_Auto.Line_Id      = P_Line_Id
   AND adj_auto.automatic_flag = 'Y';

 Return L_Adj_Amt;
     Exception When Others Then
    return  0;
end;

Function Get_line_modifier_name  (P_Header_Id Number,P_Line_Id Number) Return VARCHAR2
Is
L_MOD_NAME VARCHAR2(240);

Begin

 Select max(adjustment_name)
  Into L_MOD_NAME
  from Oe_Price_Adjustments_V Adj
  Where  Adj.Header_Id    = P_Header_Id
  And    Adj.Line_Id      = P_Line_Id
  and   price_adjustment_id in 
      (select max(price_adjustment_id) 
         from oe_price_adjustments_v adj1
         Where  Adj1.Header_Id    = P_Header_Id
           And  Adj1.Line_Id      = P_Line_Id
           AND  adj1.list_line_type_code ='DIS'
           AND adj1.applied_flag ='Y'
       );    

 Return L_MOD_NAME;
     Exception When Others Then
    return  0;
end;


Function Get_Adj_Auto_Modifier_Name  (P_Header_Id Number,P_Line_Id Number) Return varchar2
Is
L_Adj_NAME varchar2(240);

Begin

  Select max(adjustment_name)
  Into L_Adj_NAME
  from Oe_Price_Adjustments_V Adj_Auto
  Where  Adj_Auto.Header_Id    = P_Header_Id
  And    Adj_Auto.Line_Id      = P_Line_Id
   AND adj_auto.automatic_flag = 'Y';

 Return L_Adj_NAME;
     Exception When Others Then
    return  0;
end;



Function Get_Item_Avg_Cost  (P_Inventory_Item_Id Number,P_Organization_Id Number) Return Number
Is
l_avg_cost number;
Begin
  Select  Avg(Item_Cost)
  Into    L_Avg_Cost
  From    Cst_Item_Costs
  Where   Cost_Type_Id    = 2
  and     inventory_item_id in
                             (
                              select component_item_id
                              from  bom_bill_of_materials    bbm,
                                    Bom_Inventory_Components Bic
                              where bbm.bill_sequence_id    = bic.bill_sequence_id
                              And   Bbm.Assembly_Item_Id    = P_Inventory_Item_Id
                             -- and   nvl(bbm.organization_id     = P_Organization_Id
                              );

  Return L_Avg_Cost;

      Exception When Others Then
    return  0;

End;

function get_fly_item_selling_price  (p_header_id number,p_inventory_item_id number) return number
is
l_selling_price number;
begin
 select sum(nvl(ol.unit_selling_price,0))
 into l_selling_price
 from oe_order_lines ol
 where ol.header_id         = p_header_id
 and  ol.inventory_item_id in
                             (
                              select component_item_id
                              from  bom_bill_of_materials    bbm,
                                    bom_inventory_components bic
                              where bbm.bill_sequence_id    = bic.bill_sequence_id
                              and   bbm.assembly_item_id    = p_inventory_item_id
                             -- and   bbm.organization_id     = ol.ship_from_org_id
                              );
      return l_selling_price;
      exception when others then
    return  0;
End;

function get_line_ship_confirm_flag  (p_header_id number,p_line_id number) return varchar2
is
l_flag varchar2(2);
BEGIN
        select max('Y')
        into l_flag
        from wsh_delivery_details
        where source_header_id=p_header_id
        AND SOURCE_LINE_ID=P_LINE_ID
        and released_status in ('C');
return l_flag;
      exception when others then
    return  'N';

End;

Function Get_Rental_Unit_Selling_Price  (P_Line_Id Number) Return Number
Is
L_Price Number;
Begin
Select Max(Unit_Selling_Price)
into l_price
    FROM oe_order_lines
    Where Ordered_Item ='Rental Charge'
    And Link_To_Line_Id=P_Line_Id;

    Return L_Price;
          Exception When Others Then
    return  0;
end;


Function Get_Line_Shipment_Date  (P_Line_Id Number) Return Date
is

l_ship_date date;

begin

  select wnd.initial_pickup_date
  into  l_ship_date
  From  Wsh_Delivery_Assignments  Wda,
        Wsh_Delivery_Details      Wdd,
        Wsh_New_Deliveries        Wnd
  Where Wda.Delivery_Detail_Id  = Wdd.Delivery_Detail_Id
  and   wnd.delivery_id         = wda.delivery_id
  and   source_line_id          = p_line_id
  and   source_code            = 'OE';


    return l_ship_date;

    Exception When Others Then
    Return  null;
end;

function Get_trader (P_HEADER_ID number, P_LINE_ID number)
return number
is
 L_TRADER number := 0;
BEGIN

select count(*)
  into L_TRADER
  from OE_PRICE_ADJUSTMENTS_V
 where HEADER_ID              = P_HEADER_ID
   and LINE_ID                = P_LINE_ID
   and ADJUSTMENT_NAME like '%TRADER%';


      return L_TRADER;


EXCEPTION when OTHERS then
    return  0;
END;

function Get_MfgAdjust (P_HEADER_ID number, P_LINE_ID number)
return number
is
 L_MFGADJ number := 0;
BEGIN

select -1*adjusted_amount --operand
  into L_MFGADJ
  from OE_PRICE_ADJUSTMENTS_V oe,
       QP_LIST_HEADERS qph
 where oe.HEADER_ID              = P_HEADER_ID
   and oe.LINE_ID                = P_LINE_ID
   AND oe.list_header_id         = qph.list_header_id   
   and (ADJUSTMENT_NAME like '%MFG DISC%' or (qph.attribute10 ='Manufacturer''s Promotion'));


    return L_MFGADJ;


EXCEPTION when OTHERS then
    return  0;
END;

function Get_Subinventory (p_organization_id number)
return Varchar2
IS
 l_sub_inv Varchar2(50);
BEGIN

select SECONDARY_INVENTORY_NAME
  into l_sub_inv
  FROM MTL_SECONDARY_INVENTORIES
 where ORGANIZATION_ID=p_organization_id;


      return l_sub_inv;


EXCEPTION when OTHERS then
    RETURN  NULL;
END;


function Get_Last_Updt_by  (p_line_id number) return varchar2
IS
l_name varchar2(100);
begin
         SELECT ppf.full_name
            into l_name
          from oe_order_lines ol,
               per_all_people_f ppf,
               fnd_user fu
          where OL.LAST_UPDATED_BY  = FU.USER_ID
        --   and ol.flow_status_code  = 'CANCELLED'
           and fu.employee_id       = ppf.person_id(+)
           and ol.creation_date between ppf.effective_start_date and nvl(ppf.effective_end_date,ol.creation_date)
           AND ol.line_id           = p_line_id;

RETURN L_NAME;

      exception when others then
    return  NULL;

End;

Function Get_Cash_amount  (p_header_id Number) Return Number
is
l_cash_amount number;
BEGIN
      SELECT  rctl.extended_amount
       INTO  l_cash_amount
        FROM oe_order_headers oh,
             oe_order_lines ol,
             oe_payments oe,
             ra_customer_trx_lines rctl
      WHERE 1                      =1
      AND oh.header_id             =ol.header_id
      AND oe.header_id             =oh.header_id
      AND to_char(oh.order_number) =rctl.interface_line_attribute1
      AND to_char(ol.line_id)      =rctl.interface_line_attribute6
      AND oe.header_id     =p_header_id;




    Return l_cash_amount;
          Exception When Others Then
    return  0;
end;
function Get_isr_rpt_dc_mod_sub  return varchar2
is
begin
return G_isr_rpt_dc_mod_sub;
end;


Function Get_item_purchased  (P_Inventory_Item_Id Number,P_Organization_Id Number) Return Number
is
l_purchased_item number;
begin


       SELECT count(DISTINCT poh.po_header_id)
         into l_purchased_item
        FROM po_headers poh,
        po_lines pol,
        po_line_locations poll,
        mtl_system_items_kfv msi
        WHERE poh.po_header_id=pol.po_header_id
        AND pol.po_line_id=poll.po_line_id
        AND msi.inventory_item_id=pol.item_id
        AND msi.organization_id=poll.ship_to_organization_id
        AND msi.inventory_item_id=P_Inventory_Item_Id
        and MSI.ORGANIZATION_ID=P_ORGANIZATION_ID
        AND trunc(poh.creation_date) >= sysdate -365;
        --and trunc(poh.creation_date) <=nvl(g_date_from,trunc(poh.creation_date))
       -- and trunc(poh.creation_date) >=nvl(g_date_to,trunc(poh.creation_date)) ;


  Return l_purchased_item;

      Exception When Others Then
    return  0;

end;

function get_isr_item_cost (p_inventory_item_id number,p_organization_id number) return number
is
l_item_cost number;
begin

  select unit_price
  into l_item_cost
  from po_lines_all
  where po_line_id in
            ( select max(pol.po_line_id)
             from po_headers poh,
                  po_line_locations poll,
                  po_lines pol,
                  po_vendors   pov
             where poh.type_lookup_code      = 'BLANKET'
             and   poh.po_header_id          = pol.po_header_id
             and   pol.po_line_id            = poll.po_line_id(+)
             and   nvl(poh.cancel_flag,'N')='N'
            --and  poll.quantity_received > 0
            and  pol.item_id                 = p_inventory_item_id
            and (poh.global_agreement_flag ='Y' or poll.ship_to_organization_id = p_organization_id)
            and poh.vendor_id                = pov.vendor_id
            and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)
            );

  Return l_item_cost;

      exception when others then
    return  null;

end;

function get_isr_bpa_doc (p_inventory_item_id number,p_organization_id number) return number
is
l_bpa_doc number;
begin

  select segment1
  into l_bpa_doc
  from po_headers_all
  where po_header_id in
            ( select max(poh.po_header_id)
             from po_headers poh,
                  po_line_locations poll,
                  po_lines_all pol,
                  po_vendors   pov
             where poh.type_lookup_code      = 'BLANKET'
             and   poh.po_header_id          = pol.po_header_id
             and   nvl(poh.cancel_flag,'N')='N'
             and   pol.po_line_id            = poll.po_line_id(+)
            --and  poll.quantity_received > 0
              and  pol.item_id                 = p_inventory_item_id
              and (poh.global_agreement_flag ='Y' or poll.ship_to_organization_id = p_organization_id)
              and poh.vendor_id                = pov.vendor_id
              and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)
            );

  Return l_bpa_doc;

      exception when others then
    return  null;

end;

   FUNCTION get_isr_open_po_qty (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN NUMBER
   is
      L_OPEN_QTY                  number;
      L_INTERNAL_RECEIPT_QTY      number;
      L_ONORDER_QTY               number;
      L_COMPONENT_QTY             number;
      l_assembly_qy               number;
   BEGIN
      SELECT SUM (qty)
        INTO l_open_qty
        FROM (SELECT SUM (
                          poll.quantity
                        - NVL (poll.quantity_received, 0)
                        - NVL (poll.quantity_cancelled, 0))
                        qty
                FROM po_headers poh,
                     po_line_locations poll,
                     po_lines pol,
                     po_vendors pov
               WHERE     1 = 1
                     AND NVL (poh.cancel_flag, 'N') = 'N'
                     AND NVL (pol.cancel_flag, 'N') = 'N'
                     AND NVL (poll.cancel_flag, 'N') = 'N'
                     AND NVL (poh.closed_code, 'OPEN') NOT IN
                            ('FINALLY CLOSED', 'CLOSED')
                     AND NVL (poll.closed_code, 'OPEN') NOT IN
                            ('CLOSED', 'FINALLY CLOSED')
                     AND NVL (pol.closed_code, 'OPEN') NOT IN
                            ('CLOSED', 'FINALLY CLOSED')
                     AND poh.type_lookup_code IN
                            ('STANDARD', 'BLANKET', 'PLANNED')
                     AND poh.po_header_id = pol.po_header_id
                     AND pol.po_line_id = poll.po_line_id
                     AND poll.quantity - NVL (poll.quantity_received, 0) > 0
                     AND pol.item_id = p_inventory_item_id
                     AND poll.ship_to_organization_id = p_organization_id
                     and POH.VENDOR_ID = POV.VENDOR_ID);
                     
    --query to get internal receipt qty---

    select  SUM(OEL.ORDERED_QUANTITY-NVL(OEL.FULFILLED_QUANTITY,0)) 
    into l_internal_receipt_qty
    from
        APPS.OE_ORDER_LINES_ALL             OEL,
        apps.OE_ORDER_HEADERS_ALL           OEH,
        APPS.PO_REQUISITION_HEADERS_ALL     PRH
    where OEH.HEADER_ID        = OEL.HEADER_ID
    and OEH.SOURCE_DOCUMENT_ID = PRh.REQUISITION_HEADER_ID
    and OEL.SOURCE_TYPE_CODE   = 'INTERNAL'
    and OEL.FLOW_STATUS_CODE   ='CLOSED'
    and OEL.INVENTORY_ITEM_ID     =  P_INVENTORY_ITEM_ID
    AND OeL.SHIP_FROM_ORG_ID      =  P_ORGANIZATION_ID
    and not exists(select 1 
                    from APPS.RCV_TRANSACTIONS RT,
                          APPS.PO_REQUISITION_LINES_ALL PRL
                    where RT.SOURCE_DOCUMENT_CODE     ='REQ'
                    and   RT.REQUISITION_LINE_ID      = PRL.REQUISITION_LINE_ID
                    and   PRL.REQUISITION_HEADER_ID   = PRH.REQUISITION_HEADER_ID
                    );
--how ROP and Min-Max get the WIP Demand (If the item is a component on a work order).  

      select SUM(O.REQUIRED_QUANTITY - O.QUANTITY_ISSUED)
      into L_COMPONENT_QTY
         FROM wip_discrete_jobs d,
              wip_requirement_operations o
         WHERE o.wip_entity_id     = d.wip_entity_id
         and O.ORGANIZATION_ID   = D.ORGANIZATION_ID
         AND d.organization_id   = P_ORGANIZATION_ID
         and O.INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID
         AND o.date_required    <= sysdate
         AND o.required_quantity >= o.quantity_issued
         AND o.operation_seq_num > 0
         AND d.status_type in (1,3,4,6)
         and O.WIP_SUPPLY_TYPE not in (5,6)
       --  AND nvl(o.supply_subinventory,1) = decode(:subinv,NULL,nvl(o.supply_subinventory,1),:subinv)
         AND  NOT EXISTS(SELECT/*+ index(mtl MTL_DEMAND_N12)*/  wip.wip_entity_id
                      FROM wip_so_allocations wip, mtl_demand mtl
                      WHERE wip_entity_id = o.wip_entity_id
                      AND wip.organization_id = P_ORGANIZATION_ID
                      AND wip.organization_id = mtl.organization_id
                      AND wip.demand_source_header_id = mtl.demand_source_header_id
                      AND wip.demand_source_line = mtl.demand_source_line
                      and WIP.DEMAND_SOURCE_DELIVERY = MTL.DEMAND_SOURCE_DELIVERY
                      AND mtl.inventory_item_id = P_INVENTORY_ITEM_ID);
--how ROP and Min-Max get WIP Supply (If it's an assembly item on a work order).

        SELECT SUM(NVL(start_quantity,0)
               - NVL(quantity_completed,0)
               - NVL(QUANTITY_SCRAPPED,0))
         into l_assembly_qy
         FROM wip_discrete_jobs
         WHERE organization_id = P_ORGANIZATION_ID
         AND primary_item_id =P_INVENTORY_ITEM_ID
         AND status_type in (1,3,4,6)
         and JOB_TYPE in (1,3)
         and SCHEDULED_COMPLETION_DATE <= TO_DATE(TO_CHAR(sysdate),'DD-MON-RR');
   
      
      L_ONORDER_QTY:= NVL(L_OPEN_QTY,0)+NVL(L_INTERNAL_RECEIPT_QTY,0)+nvl(L_COMPONENT_QTY,0)+nvl(l_assembly_qy,0);
      
      return l_onorder_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


function get_isr_open_req_qty (p_inventory_item_id number,p_organization_id number) return number
is
l_open_qty number;
begin


        SELECT SUM(prdl.req_line_quantity) QTY
        into l_open_qty
        from  po_requisition_headers prh,
              po_req_distributions prdl,
              po_requisition_lines prl              
        where prh.requisition_header_id = prl.requisition_header_id  
        AND prdl.requisition_line_id=prl.requisition_line_id
        and item_id=p_inventory_item_id
        and nvl(prh.cancel_flag,'N')='N'
        and nvl(prl.cancel_flag,'N')='N'
        and NVL(PRL.CLOSED_CODE,'OPEN')='OPEN'
        and PRDL.gl_encumbered_date is not null--added by srinivas 
        and destination_organization_id=p_organization_id
        AND not exists (select 1
                        from po_action_history pah 
                        WHERE  pah.object_id = prh.requisition_header_id
                          AND pah.object_type_code  = 'REQUISITION'
                          AND pah.action_code       = 'CANCEL'
                       )
        and not exists (select 1
                        from po_distributions pod
                        WHERE POD.REQ_DISTRIBUTION_ID=PRDL.DISTRIBUTION_ID
                       )
       and not exists(select 1 from oe_order_headers oh
                       where order_type_id = 1011                       
                       and oh.source_document_id = prl.REQUISITION_HEADER_ID);

   --   and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)


  Return l_open_qty;

      exception when others then
    return  null;

end;

function get_isr_avail_qty (p_inventory_item_id number,p_organization_id number) return number
is
l_demand_qty number:=0;
l_avail_qty  number;
BEGIN
--This is code is commneted by srinivas--
     /* select sum(ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))
      into  l_demand_qty
      from  oe_order_lines_all ol
      where OL.INVENTORY_ITEM_ID     = P_INVENTORY_ITEM_ID
      and   OL.SHIP_FROM_ORG_ID      = P_ORGANIZATION_ID
      and   (ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))>0
      and   ol.flow_status_code not  in ('CANCELLED','CLOSED');*/
      
      --The following code added by srinivas---
      
  SELECT NVL(SUM(MR.RESERVATION_QUANTITY),0)
        into  l_demand_qty
      FROM    OE_ORDER_LINES OL,
              MTL_SALES_ORDERS MSO,
              MTL_RESERVATIONS MR
      WHERE MR.INVENTORY_ITEM_ID      = P_INVENTORY_ITEM_ID
        AND   MR.ORGANIZATION_ID        = P_ORGANIZATION_ID
        AND   MR.DEMAND_SOURCE_LINE_ID  = OL.LINE_ID
        --AND  ol.flow_status_CODE not in ('CLOSED')
        AND   MR.DEMAND_SOURCE_HEADER_ID=MSO.SALES_ORDER_ID;

     l_avail_qty:=(get_onhand_inv(p_inventory_item_id,p_organization_id)-nvl(l_demand_qty,0));
     return l_avail_qty;
    exception when no_data_found then
     return  l_avail_qty;

end;

function get_isr_ss_cnt (p_inventory_item_id number,p_organization_id number) return number
is
l_ss_item_cnt number;
begin

      select count(*)
      into  l_ss_item_cnt
      from  mtl_system_items_kfv msi
      where msi.inventory_item_id        = p_inventory_item_id
      and   msi.source_organization_id   = p_organization_id   ;

  Return l_ss_item_cnt;

      exception when others then
    return  null;

end;

function get_order_line_status(p_line_id in number)  return varchar2
is
L_LINE_STATUS varchar2(50);
l_status varchar2(50);
begin

    select ol.FLOW_STATUS_CODE
      into L_LINE_STATUS
      from OE_ORDER_LINES OL
    where OL.LINE_ID = P_LINE_ID;
    
    l_status:= APPS.OE_LINE_STATUS_PUB.GET_LINE_STATUS(P_LINE_ID,L_LINE_STATUS);
    
    return L_STATUS;
    
    EXCEPTION
    when OTHERS then
    return null;
      
end;

function get_order_line_status(p_line_id in number, p_flow_status_code in varchar2)  return varchar2
is
L_LINE_STATUS varchar2(50);
l_status varchar2(50);
begin
   
  IF p_flow_status_code = 'AWAITING_SHIPPING' THEN
     BEGIN
      select max(released_status) 
        into L_LINE_STATUS
        from WSH_DELIVERY_DETAILS 
        where source_line_id = p_line_id;
        IF l_line_status ='B' THEN
           RETURN 'Backordered';
        END IF;
      EXCEPTION
        WHEN OTHERS THEN
          NULL;
     END;       
  END IF;
  
    select ol.FLOW_STATUS_CODE
      into L_LINE_STATUS
      from OE_ORDER_LINES OL
    where OL.LINE_ID = P_LINE_ID;
    
    l_status:= APPS.OE_LINE_STATUS_PUB.GET_LINE_STATUS(P_LINE_ID,p_flow_status_code);
    
    return L_STATUS;
    
    EXCEPTION
    when OTHERS then
    return null;
      
end;


function get_grossmgr_rpt_sale_rep_flag  (p_salerep_number varchar2) return varchar2
IS
l_return_flag varchar2(2);

Begin
 if G_Manger_Type is null then
     begin
         Select 'M'
          into G_Manger_Type
          From Xxwc.Dw_Salesreps Ds
          Where 1=1
          And Ntid=Fnd_Global.User_Name;
     Exception When No_Data_Found Then
     Null;
     End;

    Begin
        Select distinct 'DM',Districtmanager_Hremployeeid
        Into G_Manger_Type,G_Salerep_Num
        From Xxwc.Dw_Salesreps Ds
        Where 1=1
        And districtmanager_Ntid=Fnd_Global.User_Name;
   Exception When No_Data_Found Then
   Null;
   End;

   Begin
       Select distinct 'RM',Regionalmanager_Hremployeeid
        Into G_Manger_Type,G_Salerep_Num
        From Xxwc.Dw_Salesreps Ds
        Where 1=1
        And regionalmanager_Ntid=Fnd_Global.User_Name;
   Exception When No_Data_Found Then
   Null;
   End;

   Begin
      Select distinct 'NM',Nationalmanager_Hremployeeid
      Into G_Manger_Type,G_Salerep_Num
      From Xxwc.Dw_Salesreps Ds
      Where 1=1
     And Nationalmanager_Ntid=Fnd_Global.User_Name;
   Exception When No_Data_Found Then
     Null;
    End;
Else
    If G_Manger_Type ='M' Then
       return 'Y';
    Elsif G_Manger_Type ='DM' Then

         Begin
              Select max('Y')
              Into l_return_flag
              From Xxwc.Dw_Salesreps Ds
              where Districtmanager_Hremployeeid=G_Salerep_Num
              And Hremployeeid =P_Salerep_Number;
         Exception When No_Data_Found Then
         null;
         End;

    Elsif G_Manger_Type ='RM' Then
        Begin
              Select max('Y')
              Into l_return_flag
              From Xxwc.Dw_Salesreps Ds
              where Regionalmanager_Hremployeeid=G_Salerep_Num
              And (Hremployeeid =P_Salerep_Number or Districtmanager_Hremployeeid=P_Salerep_Number);
       Exception When No_Data_Found Then
       Null;
       End;

    Elsif G_Manger_Type ='NM' Then
         Begin
              Select max('Y')
              Into l_return_flag
              From Xxwc.Dw_Salesreps Ds
              where Nationalmanager_Hremployeeid=G_Salerep_Num
              And (Hremployeeid =P_Salerep_Number or Districtmanager_Hremployeeid=P_Salerep_Number or Regionalmanager_Hremployeeid=P_Salerep_Number);
         Exception When No_Data_Found Then
       Null;
       End;
   End If;

End If;
  return nvl(l_return_flag,'N');
End;

function get_invoice_amt  (p_header_id in number ) return number
is
l_inv_amt number;
begin
select  sum(ps.amount_due_original)
INTO L_INV_AMT
 FROM oe_order_headers oh,
      RA_CUSTOMER_TRX RCT,
      AR_PAYMENT_SCHEDULES PS
WHERE  1=1
  AND PS.CUSTOMER_TRX_ID          = RCT.CUSTOMER_TRX_ID
  AND TO_CHAR(OH.ORDER_NUMBER)    = RCT.INTERFACE_HEADER_ATTRIBUTE1
  AND RCT.INTERFACE_HEADER_CONTEXT = 'ORDER ENTRY'
  and oh.header_id                = p_header_id;
  return l_inv_amt;
  exception when others then
  return 0;

End;

function get_cust_invoice_amt  (p_trx_id in number ) return number
is
l_inv_amt number;
begin
select  sum(ps.amount_due_original)
INTO L_INV_AMT
 FROM 
  
  AR_PAYMENT_SCHEDULES PS
WHERE  1=1
  AND PS.CUSTOMER_TRX_ID           = p_trx_id
  ;
  return l_inv_amt;
  exception when others then
  return 0;

End;

function GET_UNIT_COST (p_inventory_item_id number, P_ORGANIZATION_ID number,P_Segment1 varchar2)
Return Number
Is
L_Unit_Cost Number;
l_line_id number;
L_Inventory_Item_Id Number;

BEGIN

if instr(upper(P_Segment1),'RR') <> 0 then
Select Max(Inventory_Item_Id)
Into  L_Inventory_Item_Id
From Mtl_System_Items_Kfv Where Segment1 Like substr(P_Segment1,3)
And Organization_Id=P_Organization_Id;
Else
Select Max(Inventory_Item_Id)
Into  L_Inventory_Item_Id
From Mtl_System_Items_Kfv Where Segment1 Like substr(P_Segment1,2)
And Organization_Id=P_Organization_Id;
end if;


/* Select Max(Line_Id)
 into l_line_id
    From Oe_Order_Lines  Ol
    Where Ol.Inventory_Item_Id         =L_Inventory_Item_Id
    And Ol.Ship_From_Org_Id            =P_Organization_Id ;

    If L_Line_Id Is Null Then
     Select Oll.unit_cost
    INTO  l_unit_cost
    From Oe_Order_Lines  Oll
    Where Line_Id In
    (Select Max(Line_Id) From
    Oe_Order_Lines  Ol
     Where Ol.Inventory_Item_Id         =P_Inventory_Item_Id
     And Ol.Ship_From_Org_Id            =P_Organization_Id
     );
 Else
    Select Ol1.Unit_Cost
    Into  L_Unit_Cost
    From Oe_Order_Lines Ol1 Where
    Line_Id =L_Line_Id;
end if;
*/

l_unit_cost:=APPS.CST_COST_API.GET_ITEM_COST(1,L_Inventory_Item_Id,P_Organization_Id);
  return l_unit_cost;
exception
 When Others Then
 l_unit_cost:=2;
   return l_unit_cost;
END;

function Get_Subinv_Onhand_qty (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number,p_date date default null)
Return Number
is
l_Subinv_Onhand_qty Number;
BEGIN

   select nvl(sum(moq.transaction_quantity),0)
    into  l_Subinv_Onhand_qty
    from MTL_MATERIAL_TRANSACTIONS MOQ,
         MTL_SECONDARY_INVENTORIES MSINV
    where MOQ.INVENTORY_ITEM_ID          =P_INVENTORY_ITEM_ID
    and MOQ.ORGANIZATION_ID             =P_ORGANIZATION_ID
    and MOQ.ORGANIZATION_ID = MSINV.ORGANIZATION_ID(+)
    and MOQ.SUBINVENTORY_CODE = MSINV.SECONDARY_INVENTORY_NAME
    AND upper(MSINV.SECONDARY_INVENTORY_NAME) in ('DBRENTAL','DRRENTAL','LOSTRENTAL','RENTAL','RENTAL N/R','RENTALHLD')
    and trunc(MOQ.transaction_date)<=nvl(p_date,g_date_to);

  return l_Subinv_Onhand_qty;
exception
 WHEN OTHERS THEN
   return 0;
End;
Function Get_Fixed_Assets_Cnt  (P_Organization_Id In Number ) Return Number
is
l_cnt Number;
BEGIN

   Select Count(*)
   Into L_Cnt
   From Mtl_System_Items_Kfv
    Where Segment1 Like 'R%'
    and organization_id=p_organization_id;
  return l_cnt;
Exception
 WHEN OTHERS THEN
   Return 0;
END;

Function Get_bo_flag  (P_line_id In Number ) Return varchar2
IS
l_flag Number;
BEGIN

Select max('Y')
Into L_Flag
  From Wsh_Delivery_Details
  Where Released_Status='B'
  And Source_Line_Id=P_Line_Id;

  return l_flag;
Exception
 When Others Then
   RETURN 'N';
END;

Function get_backorder_qty  (P_header_id In Number,P_line_id In Number ) Return varchar2
IS
l_qty Number;
BEGIN

Select nvl(ol.ordered_quantity,0) ordered_quantity
INTO L_QTY
  FROM OE_ORDER_LINES OL,
       WSH_DELIVERY_DETAILS WDD
  WHERE WDD.RELEASED_STATUS='B'
  AND WDD.SOURCE_HEADER_ID=OL.HEADER_ID
  And wdd.Source_Line_Id=ol.Line_Id
  AND WDD.SOURCE_HEADER_ID=P_HEADER_ID
  And wdd.Source_Line_Id=P_Line_Id;

  return l_qty;
Exception
 When Others Then
   RETURN 0;
END;


function get_line_rma_flag  (p_line_id number) return varchar2
is
l_return_flag varchar2(5);
begin
select 'Y'
Into l_return_flag
    FROM Rcv_Shipment_Lines
    WHERE Source_Document_Code   ='RMA'
    and oe_order_line_id         =p_line_id
    and shipment_line_status_code='FULLY RECEIVED';
    return l_return_flag;
Exception
 When Others Then
   return 'N';
end;

function get_po_vendor_name(p_inventory_item_id number,p_organization_id number) return varchar2
is
l_vendor varchar2(500);
begin
select max(PV.VENDOR_NAME)
into l_vendor
from PO_VENDORS PV,
PO_LINES_ALL POL,
PO_LINE_LOCATIONS_ALL POLL,
po_headers_all poh
where POL.ITEM_ID = P_INVENTORY_ITEM_ID
and POLL.SHIP_TO_ORGANIZATION_ID = P_ORGANIZATION_ID
and POL.PO_LINE_ID = POLL.PO_LINE_ID
and POH.PO_HEADER_ID = POL.PO_HEADER_ID
and poh.vendor_id = pv.vendor_id;
    return L_VENDOR;
Exception
 when OTHERS then
   return NULL;
END;

function get_po_vendor_number (p_inventory_item_id number,p_organization_id number) return varchar2
is
l_vendor varchar2(500);
begin
select max(PV.segment1)
into l_vendor
from PO_VENDORS PV,
PO_LINES_ALL POL,
PO_LINE_LOCATIONS_ALL POLL,
po_headers_all poh
where POL.ITEM_ID = P_INVENTORY_ITEM_ID
and POLL.SHIP_TO_ORGANIZATION_ID = P_ORGANIZATION_ID
and POL.PO_LINE_ID = POLL.PO_LINE_ID
and POH.PO_HEADER_ID = POL.PO_HEADER_ID
and poh.vendor_id = pv.vendor_id;
    return L_VENDOR;
EXCEPTION
 when OTHERS then
   return null;
END;

function GET_LOT_STATUS (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number,P_LINE_ID number) return varchar2
is
l_reason varchar2(50);
begin
 select 'Lot Control'
  into  l_reason
  from MTL_SYSTEM_ITEMS_B
  where INVENTORY_ITEM_ID =P_INVENTORY_ITEM_ID
  and organization_id     =p_organization_id
  and LOT_CONTROL_CODE =2
  and not exists ( select 1  from MTL_RESERVATIONS
                    where DEMAND_SOURCE_LINE_ID=P_LINE_ID
                      AND organization_id = p_organization_id
                      AND LOT_NUMBER is not null
                    );
  return l_reason;
EXCEPTION
 when OTHERS then
   return null;
END;

function get_isr_sourcing_rule (p_inventory_item_id number,p_organization_id number) return varchar2
is
l_sr_name varchar2(500);
begin
      select max(msr.sourcing_rule_name )
      into l_sr_name
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         mrp_sourcing_rules msr,
         po_vendors pov
    where 1=1
    and ass.inventory_item_id(+)=p_inventory_item_id
    and ass.organization_id(+)=p_organization_id
    and rco.sourcing_rule_id(+)=ass.sourcing_rule_id
    and sso.sr_receipt_id(+)=rco.sr_receipt_id
    and msr.sourcing_rule_id =ass.sourcing_rule_id
    and pov.vendor_id(+)=sso.vendor_id;

    return  l_sr_name;
    EXCEPTION
 when others then
   return null;
end;

function get_isr_ss (p_inventory_item_id number,p_organization_id number) return number
is
l_ss  number;
begin
     select mst.safety_stock_quantity
     into l_ss
    from mtl_safety_stocks_view mst,org_acct_periods oap,
    org_organization_definitions ood
    where ood.organization_id=oap.organization_id
    and inventory_item_id= p_inventory_item_id
    and mst.organization_id=p_organization_id
    and mst.organization_id=oap.organization_id
    and  trunc(mst.effectivity_date) between period_start_date
    and nvl(period_close_date,sysdate)
        and  TRUNC(sysdate) between PERIOD_START_DATE
    and NVL(PERIOD_CLOSE_DATE,sysdate);

    return l_ss;

EXCEPTION
 when others then
   return null;
END;

FUNCTION GET_CHARGER_AMT (P_HEADER_ID NUMBER) RETURN NUMBER IS
L_CHARGE_AMT NUMBER;
BEGIN
If G_Pre_Header_Id= P_Header_Id Then
Return 0;
Else
G_PRE_HEADER_ID := P_HEADER_ID;
begin
  select NVL(OCLV.CHARGE_AMOUNT,0)
    INTO L_CHARGE_AMT
    FROM OE_CHARGE_LINES_V OCLV
    Where OCLV.Header_Id =P_Header_Id;
    RETURN L_CHARGE_AMT;
    EXCEPTION
    WHEN NO_DATA_FOUND THEN
    L_CHARGE_AMT:=0;
    RETURN L_CHARGE_AMT;
  end;
End If;
RETURN L_CHARGE_AMT;
end;

function get_cash_drawer_amt (p_header_id number) return number
Is
L_Order_Amount        Number;
G_Insert_Recd_Cnt    Number:=0;
L_Count               Number;
begin


If G_Prev_Header_Id= P_Header_Id Then
Return 0;
Else
G_Prev_Header_Id := P_Header_Id;

  select ROUND(SUM(((OL.ORDERED_QUANTITY*OL.UNIT_SELLING_PRICE)+ OL.TAX_VALUE) * DECODE(ol.line_category_code,'RETURN',-1,1)),2) ORDER_AMOUNT
    INTO L_ORDER_AMOUNT
    From Oe_Order_Lines Ol
    Where Ol.Header_Id=P_Header_Id;
    return L_ORDER_AMOUNT;
End If;

/*select COUNT(*)
INTO L_COUNT from
Xxwc_Om_Cash_Refund_Tbl
WHERE HEADER_ID=P_HEADER_ID;

  select SUM((OL.ORDERED_QUANTITY*OL.UNIT_SELLING_PRICE)+ OL.TAX_VALUE) ORDER_AMOUNT
    INTO L_ORDER_AMOUNT
    from oe_order_lines ol
    where OL.HEADER_ID=P_HEADER_ID;


If G_Insert_Recd_Cnt=0  Then
G_Insert_Recd_Cnt     :=G_Insert_Recd_Cnt+1;
G_TB_DATA_TAB(G_INSERT_RECD_CNT):=P_HEADER_ID;
 Return L_Order_Amount;
 End If;

if L_COUNT =1 then
    return L_ORDER_AMOUNT;
else
 for I in G_TB_DATA_TAB.first .. G_TB_DATA_TAB.last LOOP
 if G_TB_DATA_TAB(I)= P_HEADER_ID then
  return 0;
  end if;
end LOOP;
end if;
G_INSERT_RECD_CNT     :=G_INSERT_RECD_CNT+1;
G_TB_DATA_TAB(G_INSERT_RECD_CNT):=P_HEADER_ID;
return L_ORDER_AMOUNT;
*/
end;



Function Get_Customer_Total(P_Cust_Account_Id Number ) Return Number Is
L_Cust_Total  Number;
Begin
/*If g_prev_cust_id= P_Cust_Account_Id Then
Return L_Cust_Total;
Else
g_prev_cust_id := P_Cust_Account_Id;*/

Select
--Sum(Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Amount_Remaining ( Ps.Payment_Schedule_Id, Ps.Due_Date, Null, Null, (Ps.Amount_Due_Original)  *Nvl(Ps.Exchange_Rate,1), Ps.Amount_Applied, Ps.Amount_Credited, Ps.Amount_Adjusted, Ps.Class ))
sum(Ps.Amount_Due_Remaining)
Into L_Cust_Total
     From     Ra_Cust_Trx_Types Ctt,
          Ra_Customer_Trx Trx,
          Hz_Cust_Accounts Cust,
          Hz_Parties Party,
          Ar_Payment_Schedules Ps,
          Ra_Cust_Trx_Line_Gl_Dist Gld
  WHERE TRUNC (Ps.Gl_Date)           <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
    AND Trx.Cust_Trx_Type_Id            = Ctt.Cust_Trx_Type_Id
    AND Trx.Bill_To_Customer_Id         = Cust.Cust_Account_Id
    AND Cust.Party_Id                   = Party.Party_Id
    AND Ps.Customer_Trx_Id              = Trx.Customer_Trx_Id
    And Trx.Customer_Trx_Id             = Gld.Customer_Trx_Id
    And Gld.Account_Class               = 'REC'
    And Gld.Latest_Rec_Flag             = 'Y'
    And Ctt.Org_Id                      = trx.org_id
    and cust.cust_account_id           = P_Cust_Account_Id;
    

    

    Return NVL(L_Cust_Total,0);

 -- end if;

  End;


Function Get_Customer_AMT_DUE(P_Cust_Account_Id Number ) Return Number Is
L_Cust_Total  Number;
Begin
/*If g_prev_cust_id= P_Cust_Account_Id Then
Return L_Cust_Total;
Else
g_prev_cust_id := P_Cust_Account_Id;*/

Select
 sum(xxeis.eis_rs_ar_fin_com_util_pkg.amount_remaining ( ps.payment_schedule_id, ps.due_date, null, null, (ps.amount_due_original)  *nvl(ps.exchange_rate,1), ps.amount_applied, ps.amount_credited, ps.amount_adjusted, ps.class )) 
Into L_Cust_Total
     From     Ra_Cust_Trx_Types Ctt,
          Ra_Customer_Trx Trx,
          Hz_Cust_Accounts Cust,
          Hz_Parties Party,
          Ar_Payment_Schedules Ps,
          Ra_Cust_Trx_Line_Gl_Dist Gld
  WHERE TRUNC (Ps.Gl_Date)           <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
    AND Trx.Bill_To_Customer_Id         = Cust.Cust_Account_Id
    AND Cust.Party_Id                   = Party.Party_Id
    AND Ps.Customer_Trx_Id              = Trx.Customer_Trx_Id
    And Trx.Customer_Trx_Id             = Gld.Customer_Trx_Id
    And Gld.Account_Class               = 'REC'
    And Gld.Latest_Rec_Flag             = 'Y'
    and cust.cust_account_id           = P_Cust_Account_Id;
    

    

    Return L_Cust_Total;

 -- end if;

  end;


Function Get_Customer_Receipt_Total(P_Cust_Account_Id Number ) Return Number Is
L_Cust_Total Number;
Begin
If P_Cust_Account_Id is not null then

 Select
 Sum(Ps.Amount_Due_Remaining)
 into L_Cust_Total
  from
        Ar_Payment_Schedules Ps,
        AR_CASH_RECEIPTS CR,
        Hz_Cust_Accounts Cust,
        Hz_Parties Party
 Where Trunc (Ps.Gl_Date)                   <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
    And Ps.Cash_Receipt_Id                  = Cr.Cash_Receipt_Id
    And Ps.Customer_Id                      = Cust.Cust_Account_Id(+)
    And Cust.Party_Id                       = Party.Party_Id(+)
    and cust.cust_account_id                = P_Cust_Account_Id
    AND NOT EXISTS
      (SELECT 1
      FROM Ar_Cash_Receipt_History Crh1
      WHERE Crh1.Cash_Receipt_Id = Cr.Cash_Receipt_Id
      And Trunc (Crh1.Gl_Date)  <= Xxeis.Eis_Rs_Ar_Fin_Com_Util_Pkg.Get_Asof_Date
      And Status                 = 'REVERSED'
      );

      Return NVL(L_Cust_Total,0);
      Else
      Return 0;
      End If;
      end;

      FUNCTION GET_RENTAL_BILL_DAYS(P_SHP_DATE DATE,P_START_DATE DATE,P_END_DATE DATE ) RETURN NUMBER
      IS
      begin
      if           ((TRUNC(P_END_DATE)-TRUNC(P_SHP_DATE))+1) > ((TRUNC(P_END_DATE)-TRUNC(P_START_DATE))+1) then
                 return ROUND((trunc(P_END_DATE)-trunc(P_START_DATE))+1);
      else
      RETURN ROUND((trunc(P_END_DATE)-trunc(P_SHP_DATE))+1,0);
      end if;
      end;

     FUNCTION GET_ACCRUED_AMT(P_days NUMBER,P_dff_price NUMBER,P_list_price NUMBER ) RETURN NUMBER
      is
      l_amt number:=0;
      begin
          L_AMT:=    FLOOR(P_DAYS/7)*4*NVL(P_DFF_PRICE,P_LIST_PRICE);----get weeks
        if  MOD(P_DAYS,7) >3 then-----remaining days  if grt>4 then
          L_AMT:=L_AMT+ 4* NVL(P_DFF_PRICE,P_LIST_PRICE);
       ELSIF    MOD(P_DAYS,7) <4  then ----remaining days  if less<3 then
           L_AMT:=L_AMT+ MOD(P_DAYS,7)* NVL(P_DFF_PRICE,P_LIST_PRICE);
      end if;


      RETURN l_amt;

      end;
       function get_rental_long_bill_days(p_shp_date date,p_end_date date ) return number
       is
       L_DAYS number;
       l_minus_days number;
       begin
       
       
       select case when MOD((TRUNC(p_end_date)-TRUNC(p_shp_date))+1,28) <> 0 then  28*( FLOOR(((TRUNC(p_end_date)-TRUNC(p_shp_date))+1)/28))
       else   28*( FLOOR(((TRUNC(p_end_date)-TRUNC(p_shp_date))+1)/28)-1) end 
       into l_minus_days
       from DUAL;
      
      
      return round(((TRUNC(p_end_date)-TRUNC(p_shp_date))+1)-l_minus_days);
       
    /*   select
     case
      WHEN p_end_date -p_shp_date BETWEEN 29 AND 56
      THEN ((p_end_date-p_shp_date)-28)
      when (p_end_date -p_shp_date) between 57 and 84
      THEN ((p_end_date-p_shp_date)-56)
      when (p_end_date -p_shp_date) between 85 and 112
      then ((p_end_date-p_shp_date)-84)
      when (p_end_date -p_shp_date) between 113 and 140
      then ((p_end_date-p_shp_date)-112)
      when (p_end_date -p_shp_date) between 141 and 168
      then ((p_end_date-p_shp_date)-140)
      when (p_end_date -p_shp_date) between 169 and 196
      then ((p_end_date-p_shp_date)-168)
      when (p_end_date -p_shp_date) between 197 and 224
      then ((p_end_date-p_shp_date)-196)
      when (p_end_date -p_shp_date) between 225 and 252
      then ((p_end_date-p_shp_date)-224)
      when (p_end_date -p_shp_date) between 253 and 280
      then ((p_end_date-p_shp_date)-252)
      when (p_end_date -p_shp_date) between 281 and 308
      THEN ((p_end_date-p_shp_date)-280) END
      into l_days
      FROM    dual;
      return l_days;*/
       end;
       
/*FUNCTION Get_Curr_list_Price (P_SEGMENT VARCHAR2)
Return Number
is
 l_list_price Number;
BEGIN

        SELECT operand
           into l_list_price
          FROM qp_list_lines_v 
            WHERE list_line_id IN
                  (SELECT MAX(qllv.list_line_id)
                    FROM qp_list_lines_v qllv,
                         qp_pricing_attributes qpa
                    WHERE 1                           =1
                     and qllv.list_line_id             = qpa.list_line_id
                     and qpa.product_attribute_context = 'ITEM'
                     and qpa.product_attribute         = 'PRICING_ATTRIBUTE1'
                     and qllv.product_attr_val_disp =p_segment
                     and trunc(qllv.start_date_active) >=nvl(g_date_from,trunc(qllv.start_date_active))
                     and trunc(qllv.end_date_active) <=nvl(g_date_to,trunc(qllv.end_date_active))
                   );

  return l_list_price;
 exception
 WHEN OTHERS THEN
   return null;
END;       */


--FUNCTION GET_CURR_LIST_PRICE  (P_SEGMENT1 VARCHAR2) RETURN NUMBER
FUNCTION Get_Curr_list_Price  (p_item_id number) RETURN NUMBER
is
 l_list_price number;
 l_cnt  number;
BEGIN



    /*  select count(*)
      into l_cnt
      from qp_list_lines_v qllv,
            qp_pricing_attributes qpa,
            qp_list_headers       qslv
      where 1                           =1
      and qslv.list_header_id            =qllv.list_header_id
      and upper(qslv.name)             like 'CATEGORY%'
      and qllv.list_line_id             = qpa.list_line_id
      and qpa.product_attribute_context = 'ITEM'
      and qpa.product_attribute         = 'PRICING_ATTRIBUTE1'
      and qllv.product_attr_val_disp =p_segment1;*/
                     
      -- if l_cnt =1 then              
      /*  SELECT operand
           into l_list_price
          FROM qp_list_lines_v 
            WHERE list_line_id IN
                  (SELECT MAX(qllv.list_line_id)
                    from qp_list_lines_v qllv,
                       --  qp_pricing_attributes qpa,
                         qp_list_headers       qslv
                    where 1                           =1
                    and qslv.list_header_id            =qllv.list_header_id
                     and upper(qslv.name)             like 'CATEGORY%'
                     --and qllv.list_line_id             = qpa.list_line_id
                     and qllv.product_attribute_context = 'ITEM'
                    -- and qpa.product_attribute         = 'PRICING_ATTRIBUTE1'
                     and qllv.product_attr_val_disp =p_segment1
                   --  and  trunc(sysdate)>= nvl(trunc(qllv.START_DATE_ACTIVE),trunc(sysdate)) 
                  --    and  trunc(sysdate)<= nvl(trunc(qllv.END_DATE_ACTIVE),trunc(sysdate))
                   );*/
                   ---Added by Srinivas --
                   
           SELECT OPERAND       
             into l_list_price
            FROM qp_list_lines_v 
            WHERE list_line_id IN
                  (SELECT MAX(qllv.list_line_id)
                    FROM  QP_LIST_LINES_V QLLV,
                          qp_list_headers       qslv
                    where 1                           =1
                    and qslv.list_header_id           =qllv.list_header_id
                    And qslv.list_type_code           ='PRL'
                     --and upper(qslv.name)             like 'CATEGORY%'
                    AND QLLV.PRODUCT_ATTRIBUTE_CONTEXT = 'ITEM'
                    AND QLLV.PRODUCT_ID =p_item_id);
                   
           return l_list_price;          
        
                   
     /* else
      
              SELECT operand
           into l_list_price
          FROM qp_list_lines_v 
            WHERE list_line_id IN
                  (SELECT MAX(qllv.list_line_id)
                    from qp_list_lines_v qllv,
                         qp_pricing_attributes qpa,
                         qp_list_headers       qslv
                    where 1                           =1
                    and qslv.list_header_id            =qllv.list_header_id
                     and upper(qslv.name)             like 'CATEGORY%'
                     and qllv.list_line_id             = qpa.list_line_id
                     and qpa.product_attribute_context = 'ITEM'
                     and qpa.product_attribute         = 'PRICING_ATTRIBUTE1'
                     and qllv.product_attr_val_disp =p_segment1
                  --   and  nvl(trunc(qllv.start_date_active),trunc(g_date_from))>= nvl(trunc(qllv.start_date_active),trunc(sysdate)) 
                   --   and  nvl(trunc(qllv.end_date_active)trunc(G_DATE_TO)<= nvl(trunc(qllv.end_date_active),trunc(sysdate))
                   );
      
      end if;
      
*/

 exception
 WHEN OTHERS THEN
   return null;
END;       

/*Function get_application_method (P_Inventory_Item_Id Number, P_Organization_Id Number)
Return varchar2
is
 l_application_method varchar2(100);
BEGIN

   /* Select apps.qp_qp_form_pricing_attr.get_meaning(qll.arithmetic_operator, 'ARITHMETIC_OPERATOR')
    into l_application_method
    from  qp_secu_list_headers_vl qsl,
          qp_modifier_summary_v qll,
          mtl_system_items_kfv msi
    where 1                      =1
     and qsl.list_header_id            =qll.list_header_id
     and qll.product_attribute_context ='ITEM'
     and msi.segment1                  =to_char(qll.product_attr_value)
     and msi.inventory_item_id        = p_inventory_item_id
     and msi.organization_id          = p_organization_id
     and upper(qsl.name) not like '%BRANCH%';*/
     
  /*   select apps.qp_qp_form_pricing_attr.get_meaning(oea.arithmetic_operator, 'ARITHMETIC_OPERATOR')
       into l_application_method
      from oe_price_adjustments_v oea,
           oe_order_lines ol
        where  ol.header_id                      = oea.header_id(+)
          and  ol.line_id                        = oea.line_id(+) 
          and upper(oea.adjustment_name) not like '%BRANCH%'
          and oea.list_line_type_code='DIS'
          --and oea.header_id=10764;
          and ol.inventory_item_id=p_inventory_item_id
          and ol.ship_from_org_id=p_organization_id;
     

  return l_application_method;
 exception
 WHEN OTHERS THEN
   return null;
END;*/

Function get_application_method (p_header_id number,p_line_id number)
Return varchar2
is
 l_application_method varchar2(100);
BEGIN

   /* Select apps.qp_qp_form_pricing_attr.get_meaning(qll.arithmetic_operator, 'ARITHMETIC_OPERATOR')
    into l_application_method
    from  qp_secu_list_headers_vl qsl,
          qp_modifier_summary_v qll,
          mtl_system_items_kfv msi
    where 1                      =1
     and qsl.list_header_id            =qll.list_header_id
     and qll.product_attribute_context ='ITEM'
     and msi.segment1                  =to_char(qll.product_attr_value)
     and msi.inventory_item_id        = p_inventory_item_id
     and msi.organization_id          = p_organization_id
     and upper(qsl.name) not like '%BRANCH%';*/
     
     select apps.qp_qp_form_pricing_attr.get_meaning(oea.arithmetic_operator, 'ARITHMETIC_OPERATOR')     
       into l_application_method
      from oe_price_adjustments_v oea,   
           qp_list_headers_vl qlh,
           qp_qualifiers qual
         --  hz_cust_site_uses hcsu           
        where  1=1          
      --    and oea.list_line_type_code            = 'DIS'
          and oea.list_header_id                 = qlh.list_header_id
          and qual.list_header_id                = qlh.list_header_id
          and qual.qualifier_context             = 'CUSTOMER'
          and qual.QUALIFIER_ATTRIBUTE in ('QUALIFIER_ATTRIBUTE11' ,'QUALIFIER_ATTRIBUTE14')
        --  and qual.orig_sys_qualifier_ref        = hcsu.site_use_id
       --   and hcsu.site_use_code = 'BILL_TO' or hcsu.site_use_code = 'SHIP_TO'
          and upper(oea.adjustment_name) not like '%BRANCH%'
          and oea.header_id                      = p_header_id
          and oea.line_id                        = p_line_id;
          
     

  return l_application_method;
 exception
 WHEN OTHERS THEN
   return null;
end;


Function get_application_value (p_header_id number,p_line_id number)
Return number
is
 l_application_val number;
BEGIN

   /* Select apps.qp_qp_form_pricing_attr.get_meaning(qll.arithmetic_operator, 'ARITHMETIC_OPERATOR')
    into l_application_method
    from  qp_secu_list_headers_vl qsl,
          qp_modifier_summary_v qll,
          mtl_system_items_kfv msi
    where 1                      =1
     and qsl.list_header_id            =qll.list_header_id
     and qll.product_attribute_context ='ITEM'
     and msi.segment1                  =to_char(qll.product_attr_value)
     and msi.inventory_item_id        = p_inventory_item_id
     and msi.organization_id          = p_organization_id
     and upper(qsl.name) not like '%BRANCH%';*/
     
     select operand
       into l_application_val
      from oe_price_adjustments_v oea,   
           qp_list_headers_vl qlh,
           qp_qualifiers qual        
        where  1=1          
          and oea.list_header_id                 = qlh.list_header_id
          and qual.list_header_id                = qlh.list_header_id
          and qual.qualifier_context             = 'CUSTOMER'
          and qual.QUALIFIER_ATTRIBUTE in ('QUALIFIER_ATTRIBUTE11' ,'QUALIFIER_ATTRIBUTE14')
          and upper(oea.adjustment_name) not like '%BRANCH%'
          and oea.header_id                      = p_header_id
          and oea.line_id                        = p_line_id;
     

  return l_application_val;
 exception
 WHEN OTHERS THEN
   return null;
end;


Function get_shipped_qty (p_header_id number,p_line_id number)
Return number
is
 l_shipped_qty number;
 l_returned_qty  number;
BEGIN

   /*  select sum(NVL(ol.shipped_quantity,OL.FULFILLED_QUANTITY))
       into l_shipped_qty
      from oe_order_lines ol
       where  ol.flow_status_code='CLOSED'
        and  ol.header_id=p_header_id
        and  ol.line_id  =p_line_id 
       /* and exists (select 1
                    from  oe_price_adjustments_v oea,   
                         qp_list_headers_vl qlh,
                         qp_qualifiers qual        
                      where  1=1          
                        and oea.list_header_id                 = qlh.list_header_id
                        and qual.list_header_id                = qlh.list_header_id
                        and qual.qualifier_context             = 'CUSTOMER'
                        and qual.QUALIFIER_ATTRIBUTE in ('QUALIFIER_ATTRIBUTE11' ,'QUALIFIER_ATTRIBUTE14')
                        and upper(oea.adjustment_name) not like '%BRANCH%'
                        and oea.header_id                      = p_header_id
                        and oea.line_id                        = p_line_id
                      );*/
                      
                      
    /*  select sum(NVL(orlr.shipped_quantity,orlr.FULFILLED_QUANTITY))
      into l_returned_qty
      from oe_order_lines orlr
       where orlr.line_category_code= 'RETURN'
        and  orlr.reference_header_id=p_header_id
        and  orlr.reference_line_id=p_line_id
        and  orlr.flow_status_code='CLOSED';   */

  /*  begin       
          select NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))
          into l_shipped_qty
          from oe_order_lines ol
          where  ol.flow_status_code='CLOSED'
          and  OL.HEADER_ID=P_HEADER_ID
          and  ol.line_id  =p_line_id ;
     exception 
        when others then
          l_shipped_qty := 0;
     end;  
     begin        
          select sum(NVL(orlr.shipped_quantity,orlr.FULFILLED_QUANTITY))
            into l_returned_qty
            from oe_order_lines orlr
           where orlr.line_category_code= 'RETURN'
            and  orlr.reference_header_id=p_header_id
            and  orlr.reference_line_id=p_line_id
            and  orlr.flow_status_code='CLOSED'; 
      exception
        when others then
           l_returned_qty := 0;
     end;      */
          
       select DECODE (LINE_CATEGORY_CODE, 'RETURN',
              NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY))*-1,
              NVL(OL.SHIPPED_QUANTITY,NVL(OL.FULFILLED_QUANTITY,OL.ORDERED_QUANTITY)))
          into l_shipped_qty
          from oe_order_lines ol
          where  ol.flow_status_code='CLOSED'
          and  OL.HEADER_ID=P_HEADER_ID
          and  ol.line_id  =p_line_id ;
     

  return (nvl(l_shipped_qty,0));
 exception
 WHEN OTHERS THEN
   return null;
end;


Function get_returned_qty (p_header_id number,p_line_id number)
Return number
is
 l_returned_qty number;
BEGIN

     select sum(orlr.shipped_quantity)
       into l_returned_qty
      from oe_order_lines orlr
       where orlr.line_category_code= 'RETURN'
        and  orlr.header_id=p_header_id
        and  orlr.line_id=p_line_id
        and  orlr.flow_status_code='CLOSED';      

  return l_returned_qty;
 exception
 WHEN OTHERS THEN
   return null;
end;


Function get_sales_dollars (p_header_id number,p_line_id number)
Return number
is
 l_sales_dollars number;
BEGIN

     select sum(ol.unit_selling_price*ol.ordered_quantity)
       into l_sales_dollars
      from oe_order_lines ol,oe_price_adjustments_v oel
       where ol.header_id=oel.header_id(+)
        and  ol.line_id=oel.line_id(+)
        --and  oel.adjustment_name ='CSP'
        and  ol.flow_status_code='CLOSED'
        and  ol.header_id=p_header_id
        and  ol.line_id  =p_line_id        
        --and  ol.actual_shipment_date between EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM AND EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO     
      ;
     
  return l_sales_dollars;
 exception
 WHEN OTHERS THEN
   return null;
end;


Function get_return_dollars (p_header_id number,p_line_id number)
Return number
is
 l_return_dollars number;
BEGIN

     select (sum(orlr.unit_selling_price*orlr.ordered_quantity)*-1)
       into l_return_dollars
      from oe_order_lines orlr,
           oe_price_adjustments_v oel
       where orlr.line_category_code= 'RETURN'
        and  orlr.header_id=oel.header_id(+)
        and  orlr.line_id=oel.line_id(+)
        --and  oel.adjustment_name ='CSP'
        and  orlr.flow_status_code='CLOSED'
        and  orlr.header_id=p_header_id
        and  orlr.line_id  =p_line_id   
        and  orlr.reference_line_id is not null  
        --and  orlr.actual_shipment_date between EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_FROM AND EIS_RS_XXWC_COM_UTIL_PKG.GET_DATE_TO
      group by orlr.ordered_item, oel.adjustment_name;
     

  return l_return_dollars;
 exception
 WHEN OTHERS THEN
   return null;
end;

/*function GET_AVERAGE_COST (p_header_id number, p_line_id number)
RETURN NUMBER
is
 l_avg_cost number;
BEGIN

            select QRY.ACTUAL_COST
                  into l_avg_cost
              FROM
                (SELECT d.trx_number,
                  c.line_number,
                  c.sales_order,
                  b.ship_from_org_id,
                  a.inventory_item_id,
                  c.sales_order_line,
                  b.ordered_item,
                  b.ordered_quantity,
                  b.unit_selling_price,
                  B.UNIT_COST,
                  SUM(a.actual_cost) actual_cost,
                  a.transaction_costed_date
                FROM MTL_CST_ACTUAL_COST_DETAILS a,
                  OE_ORDER_LINES b,
                  RA_CUSTOMER_TRX_LINES c,
                  RA_CUSTOMER_TRX D
                WHERE A.INVENTORY_ITEM_ID    =B.INVENTORY_ITEM_ID
                AND A.ORGANIZATION_ID        =B.SHIP_FROM_ORG_ID
                AND B.HEADER_ID=P_HEADER_ID
                and b.line_id  =p_line_id
                --AND c.sales_order            = '10004691'
                AND b.line_id                =c.interface_line_attribute6
                AND c.interface_line_context ='ORDER ENTRY'
                AND a.transaction_costed_date=
                  (SELECT MIN(transaction_costed_date)
                  FROM INV.MTL_CST_ACTUAL_COST_DETAILS d
                  WHERE a.inventory_item_id    =d.inventory_item_id
                  AND a.organization_id        =d.organization_id
                  AND d.transaction_costed_date>b.fulfillment_date
                  )
                AND d.customer_trx_id=c.customer_trx_id
                AND d.creation_date  > sysdate-2
                group by d.trx_number, c.line_number, c.sales_order, b.ship_from_org_id, a.inventory_item_id, c.sales_order_line, b.ordered_item, b.ordered_quantity, b.unit_selling_price, B.UNIT_COST, a.transaction_costed_date
                ) qry;

  return l_avg_cost;
 exception
 WHEN OTHERS THEN
   return null;
END;*/

function GET_AVERAGE_COST (p_header_id number, p_line_id number)
RETURN NUMBER
is
 l_avg_cost number;
BEGIN

            SELECT 
                  SUM(a.actual_cost) actual_cost                  
                  into l_avg_cost
                FROM MTL_CST_ACTUAL_COST_DETAILS a,
                  OE_ORDER_LINES b,
                  RA_CUSTOMER_TRX_LINES c,
                  RA_CUSTOMER_TRX D
                WHERE A.INVENTORY_ITEM_ID    =B.INVENTORY_ITEM_ID
                AND A.ORGANIZATION_ID        =B.SHIP_FROM_ORG_ID
                AND B.HEADER_ID=P_HEADER_ID
                and b.line_id  =p_line_id
                --AND c.sales_order            = '10004691'
                AND b.line_id                =c.interface_line_attribute6
                AND c.interface_line_context ='ORDER ENTRY'
                AND a.transaction_costed_date=
                  (SELECT MIN(transaction_costed_date)
                  FROM INV.MTL_CST_ACTUAL_COST_DETAILS d
                  WHERE a.inventory_item_id    =d.inventory_item_id
                  AND a.organization_id        =d.organization_id
                  AND d.transaction_costed_date>b.fulfillment_date
                  )
                AND d.customer_trx_id=c.customer_trx_id
                and d.creation_date  > sysdate-2
                group by d.trx_number, c.line_number, c.sales_order, b.ship_from_org_id, a.inventory_item_id, c.sales_order_line, b.ordered_item, b.ordered_quantity, b.unit_selling_price, B.UNIT_COST, a.transaction_costed_date; 

  return l_avg_cost;
 exception
 WHEN OTHERS THEN
   return null;
END;

function get_average_cost (p_inventory_item_id NUMBER, p_organization_id NUMBER, p_shipment_date date, p_line_id number)
RETURN NUMBER
is
 l_avg_cost number;
BEGIN

          
           select min(actual_cost) 
             into l_avg_cost
           from (
           SELECT SUM(a.actual_cost) actual_cost           
             FROM MTL_CST_ACTUAL_COST_DETAILS a                 
            WHERE A.INVENTORY_ITEM_ID    = p_inventory_item_id
              AND A.ORGANIZATION_ID      = p_organization_id
              --and e.trx_source_line_id   = p_line_id
            --  and e.transaction_id=a.transaction_id
              AND a.transaction_id= (SELECT MIN(transaction_id)
                                       FROM MTL_CST_ACTUAL_COST_DETAILS d
                                      WHERE a.inventory_item_id    =d.inventory_item_id
                                        AND a.organization_id      =d.organization_id
                                        AND d.transaction_costed_date > p_shipment_date                                       
                                         ) 
              AND a.transaction_costed_date=
                  (SELECT MIN(transaction_costed_date)
                     FROM MTL_CST_ACTUAL_COST_DETAILS d
                    WHERE a.inventory_item_id    =d.inventory_item_id
                      AND a.organization_id      =d.organization_id
                      AND d.transaction_costed_date > p_shipment_date
                  )
              group by a.INVENTORY_ITEM_ID, a.ORGANIZATION_ID, a.transaction_id,a.transaction_costed_date) qry;               
           

  return nvl(l_avg_cost,0);
 exception
 WHEN OTHERS THEN
   return null;
END;

----Credit Meemo---

FUNCTION XXWC_CAL_RESTOCK_PER(L_HEADER_ID IN NUMBER)  RETURN NUMBER IS
L_RESTOCK_AMOUNT              NUMBER;
L_NON_RESTOCK_AMOUNT          NUMBER;
L_RESTOCK_PER                 NUMBER;
BEGIN
L_RESTOCK_AMOUNT      :=0;
L_NON_RESTOCK_AMOUNT  :=0;
L_RESTOCK_PER         :=0;

 SELECT SUM(ORDERED_QUANTITY*UNIT_SELLING_PRICE) 
  INTO L_RESTOCK_AMOUNT
  FROM  OE_ORDER_LINES
  WHERE HEADER_ID =L_HEADER_ID
  AND ORDERED_ITEM='RESTOCKING';

IF L_RESTOCK_AMOUNT <> 0 THEN 

  SELECT nvl(SUM(ORDERED_QUANTITY*UNIT_SELLING_PRICE),1)
  INTO L_NON_RESTOCK_AMOUNT
  FROM  OE_ORDER_LINES
  WHERE HEADER_ID =L_HEADER_ID;
 -- AND ORDERED_ITEM !='RESTOCKING';
  
   -- L_RESTOCK_PER :=ROUND((L_RESTOCK_AMOUNT/NVL(L_NON_RESTOCK_AMOUNT,1))*100,2)/L_RESTOCK_AMOUNT;
  L_RESTOCK_PER := ROUND((L_RESTOCK_AMOUNT/NVL(L_NON_RESTOCK_AMOUNT,1))*100,2);
    
      RETURN L_RESTOCK_PER;
ELSE
    RETURN L_RESTOCK_PER;
END IF;
 
EXCEPTION
WHEN OTHERS THEN
DBMS_OUTPUT.PUT_LINE('THE ERROR IS '||SQLCODE||SQLERRM);
L_RESTOCK_PER:=0;
RETURN L_RESTOCK_PER;
END;

FUNCTION XXWC_GET_FISCAL_YEAR (P_ORG_ID IN NUMBER,
                                                  P_DATE IN DATE) RETURN NUMBER 
IS
L_PERIOD_YEAR NUMBER  ;
BEGIN
          SELECT GPS.PERIOD_YEAR
          INTO L_PERIOD_YEAR
          from  HR_OPERATING_UNITS HOU,
                GL_LEDGERS GL,
                GL_PERIOD_STATUSES GPS
         where  HOU.ORGANIZATION_ID             = P_ORG_ID
          AND GL.LEDGER_ID                    = HOU.SET_OF_BOOKS_ID
          and gl.ledger_id                    = gps.set_of_books_id
          AND GL.ACCOUNTED_PERIOD_TYPE        = GPS.PERIOD_TYPE
          AND TRUNC(P_DATE) BETWEEN GPS.START_DATE AND GPS.END_DATE
          AND GPS.APPLICATION_ID = 101;
          
  RETURN L_PERIOD_YEAR;
  
  EXCEPTION
  WHEN OTHERS THEN 
  RETURN NULL;
  END;

  FUNCTION XXWC_GET_FISCAL_MONTH (P_ORG_ID IN NUMBER,
                                                  P_DATE IN DATE) RETURN VARCHAR2 
IS
L_PERIOD_MONTH VARCHAR2(20)  ;
BEGIN
          SELECT GPS.PERIOD_NAME
          INTO L_PERIOD_MONTH
          from  HR_OPERATING_UNITS HOU,
                GL_LEDGERS GL,
                GL_PERIOD_STATUSES GPS
         where  HOU.ORGANIZATION_ID             = P_ORG_ID
          AND GL.LEDGER_ID                    = HOU.SET_OF_BOOKS_ID
          and gl.ledger_id                    = gps.set_of_books_id
          AND GL.ACCOUNTED_PERIOD_TYPE        = GPS.PERIOD_TYPE
          AND TRUNC(P_DATE) BETWEEN GPS.START_DATE AND GPS.END_DATE
          AND GPS.APPLICATION_ID = 101;
          
  RETURN L_PERIOD_MONTH;
  
  EXCEPTION
  WHEN OTHERS THEN 
  RETURN NULL;
  END;

FUNCTION       CALC_ORDERED_AMOUNT  (P_HEADER_ID IN NUMBER) RETURN NUMBER IS
L_EXTENDED_AMT NUMBER;
L_TAX_AMT NUMBER;
L_CHARGE_AMT NUMBER;
L_TOTAL_AMT NUMBER;
BEGIN
      SELECT nvl(SUM(ORDERED_QUANTITY*UNIT_SELLING_PRICE),0)
      INTO L_EXTENDED_AMT
      FROM OE_ORDER_LINES 
      WHERE HEADER_ID=P_HEADER_ID ;
      
      SELECT nvl(SUM(TAX_VALUE),0)
      INTO L_TAX_AMT
      FROM   OE_ORDER_LINES 
      WHERE HEADER_ID=P_HEADER_ID;
      
      SELECT nvl(SUM(CHARGE_AMOUNT),0) 
      INTO L_CHARGE_AMT
      FROM OE_CHARGE_LINES_V
      WHERE HEADER_ID=P_HEADER_ID;
      
      L_TOTAL_AMT:=(L_EXTENDED_AMT+L_TAX_AMT+L_CHARGE_AMT)*-1;
      
      RETURN L_TOTAL_AMT;
      EXCEPTION WHEN OTHERS THEN
      L_TOTAL_AMT:=0;
       RETURN L_TOTAL_AMT;
       END;

--Credit Memo---

--ap balance---
Function get_ap_balance (p_vendor_id number,p_org_id number)
Return number
IS
 l_balance number;
BEGIN

     select sum(APS.AMOUNT_REMAINING)
       into l_balance
      FROM AP_INVOICES           API,
           AP_PAYMENT_SCHEDULES  APS
       where 1  =  1
         and API.VENDOR_ID               = P_VENDOR_ID
         and API.ORG_ID                  = p_org_id
         AND API.INVOICE_ID              = APS.INVOICE_ID
         and API.ORG_ID                  = APS.ORG_ID
         and API.CANCELLED_DATE    is null ;
     

  return l_balance;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
end;

---ap balance----

--ar balance---
Function get_ar_balance (p_bill_to_customer_id number)
Return number
IS
 l_balance number;
BEGIN

     select sum(PS.AMOUNT_DUE_REMAINING)
       into l_balance
      FROM RA_CUSTOMER_TRX       TRX,
           AR_PAYMENT_SCHEDULES  PS
       WHERE TRX.CUSTOMER_TRX_ID         = PS.CUSTOMER_TRX_ID
         and TRX.BILL_TO_CUSTOMER_ID         = p_bill_to_customer_id
      ;
     

  return l_balance;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
end;

---ar balance----

--Receipt Date Period---
Function GET_RECEIPT_DATE_PERIOD (P_TRX_ID number)
return date
is
 l_period_name  varchar2(20);
 l_receipt_date date ;
BEGIN

     SELECT MAX(ACR.RECEIPT_DATE)
 INTO L_RECEIPT_DATE
 FROM 
    AR_RECEIVABLE_APPLICATIONS ARA,
    AR_CASH_RECEIPTS ACR
 WHERE ARA.APPLIED_CUSTOMER_TRX_ID=P_TRX_ID
    AND ARA.DISPLAY              ='Y'
    AND ARA.STATUS               ='APP'
    AND ARA.APPLICATION_TYPE     ='CASH'
    AND ARA.CASH_RECEIPT_ID      =ACR.CASH_RECEIPT_ID;
     

  return L_RECEIPT_DATE;
 EXCEPTION WHEN OTHERS THEN
  L_RECEIPT_DATE:= NULL;
  RETURN L_RECEIPT_DATE;
end;

--Receipt Date Period---

--Receipt Date Period---
function get_trx_period_name (p_trx_id number)
return varchar2
is
 l_period_name varchar2(20);
 l_ledgre_id   number;
 L_GL_DATE     DATE;
BEGIN

     SELECT  MAX(SET_OF_BOOKS_ID),
            MAX(ARA.GL_DATE)
    INTO    L_LEDGRE_ID,
            L_GL_DATE
    FROM  AR_RECEIVABLE_APPLICATIONS ARA
   WHERE  ARA.APPLIED_CUSTOMER_TRX_ID=P_TRX_ID
    AND   ARA.DISPLAY              ='Y'
    AND   ARA.STATUS               ='APP'
    AND   ARA.APPLICATION_TYPE     ='CASH';

    select period_name
    into l_period_name
    from  gl_periods gp, gl_ledgers gl
    WHERE gp.period_set_name = gl.period_set_name
    AND  GL.LEDGER_ID = L_LEDGRE_ID
    AND  L_GL_DATE between gp.start_date and gp.end_date;
     

  return L_PERIOD_NAME;
 EXCEPTION
 WHEN OTHERS THEN
   RETURN NULL;
end;

--Receipt Date Period---

function get_usetax_amt(p_amount number,p_post_code number) return number is
 l_state_tax_rate       number;
 l_county_tax_rate      number;
 l_city_tax_rate        number;
 l_tax_amount           number;
 l_stcode               number;
 l_cntycode             number;
 l_total_tax_rate       number;
 begin
 
  
     BEGIN
      select stcode,cntycode,nvl(curuserate,0)
      into  l_stcode,l_cntycode, l_city_tax_rate
      from taxware.taxlocltax
      where zipcode=p_post_code;
     exception 
       when others then
          l_city_tax_rate := 0;
     end;
     
     begin 
      select nvl(curuserate,0)
      into l_county_tax_rate
      from taxware.taxcntytax
      where cntycode=l_cntycode;
     exception 
      when others then
        l_county_tax_rate := 0;
     end; 
     begin 
      select nvl(curuserate,0)
      into  l_state_tax_rate
      from  taxware.taxsttax
      where stcode= l_stcode;
     exception 
        when others then
           l_state_tax_rate := 0;
     end; 
       l_total_tax_rate :=(l_state_tax_rate+l_county_tax_rate+l_city_tax_rate);
      l_tax_amount:=(p_amount*l_total_tax_rate);
      return l_tax_amount;
      exception 
      when others then
          l_tax_amount :=0;
            return l_tax_amount;
end;

function get_usetax_amt(p_amount number,p_post_code VARCHAR2) return number is
 l_state_tax_rate       number;
 l_county_tax_rate      number;
 l_city_tax_rate        number;
 l_tax_amount           number;
 l_stcode               number;
 l_cntycode             number;
 l_total_tax_rate       number;
 l_zip_code              number;
 begin
 
  
     BEGIN
      l_zip_code := to_number(case WHEN instr(p_post_code,'-') > 0  THEN substr(p_post_code,1,instr(p_post_code,'-')-1) else p_post_code end);       
      select stcode,cntycode,nvl(curuserate,0)
      into  l_stcode,l_cntycode, l_city_tax_rate
      from taxware.taxlocltax
      where zipcode=l_zip_code;
     exception 
       when others then
          l_city_tax_rate := 0;
     end;
     
     begin 
      select nvl(curuserate,0)
      into l_county_tax_rate
      from taxware.taxcntytax
      where cntycode=l_cntycode;
     exception 
      when others then
        l_county_tax_rate := 0;
     end; 
     begin 
      select nvl(curuserate,0)
      into  l_state_tax_rate
      from  taxware.taxsttax
      where stcode= l_stcode;
     exception 
        when others then
           l_state_tax_rate := 0;
     end; 
       l_total_tax_rate :=(l_state_tax_rate+l_county_tax_rate+l_city_tax_rate);
      l_tax_amount:=(p_amount*l_total_tax_rate);
      return l_tax_amount;
      exception 
      when others then
          l_tax_amount :=0;
            return l_tax_amount;
end;


function get_usetax_rate(p_post_code VARCHAR2) return number is
 l_state_tax_rate       number;
 l_county_tax_rate      number;
 l_city_tax_rate        number;
 l_tax_amount           number;
 l_stcode               number;
 l_cntycode             number;
 l_total_tax_rate       number := 0;
 l_zip_code             NUMBER :=0;
 begin
 
  
     BEGIN
       l_zip_code := to_number(case WHEN instr(p_post_code,'-') > 0  THEN substr(p_post_code,1,instr(p_post_code,'-')-1) else p_post_code end);       
      select stcode,cntycode,nvl(curuserate,0)
      into  l_stcode,l_cntycode, l_city_tax_rate
      from taxware.taxlocltax
      where zipcode=l_zip_code;
     exception 
       when others then
          l_city_tax_rate := 0;
     end;
     
     begin 
      select nvl(curuserate,0)
      into l_county_tax_rate
      from taxware.taxcntytax
      where cntycode=l_cntycode;
     exception 
      when others then
        l_county_tax_rate := 0;
     end; 
     begin 
      select nvl(curuserate,0)
      into  l_state_tax_rate
      from  taxware.taxsttax
      where stcode= l_stcode;
     exception 
        when others then
           l_state_tax_rate := 0;
     end; 
   l_total_tax_rate :=(l_state_tax_rate+l_county_tax_rate+l_city_tax_rate);

      
      return l_total_tax_rate;
      exception 
      when others then
          l_total_tax_rate :=0;
            return l_total_tax_rate;
end;

function get_usetax_rate(p_post_code number) return number is
 l_state_tax_rate       number;
 l_county_tax_rate      number;
 l_city_tax_rate        number;
 l_tax_amount           number;
 l_stcode               number;
 l_cntycode             number;
 l_total_tax_rate       number := 0;
 begin
 
  
     BEGIN
      select stcode,cntycode,nvl(curuserate,0)
      into  l_stcode,l_cntycode, l_city_tax_rate
      from taxware.taxlocltax
      where zipcode=p_post_code;
     exception 
       when others then
          l_city_tax_rate := 0;
     end;
     
     begin 
      select nvl(curuserate,0)
      into l_county_tax_rate
      from taxware.taxcntytax
      where cntycode=l_cntycode;
     exception 
      when others then
        l_county_tax_rate := 0;
     end; 
     begin 
      select nvl(curuserate,0)
      into  l_state_tax_rate
      from  taxware.taxsttax
      where stcode= l_stcode;
     exception 
        when others then
           l_state_tax_rate := 0;
     end; 
   l_total_tax_rate :=(l_state_tax_rate+l_county_tax_rate+l_city_tax_rate);

      
      return l_total_tax_rate;
      exception 
      when others then
          l_total_tax_rate :=0;
            return l_total_tax_rate;
end;



--Vendor Quote Batch
FUNCTION get_venquote_glstring( p_order_number IN VARCHAR2, p_type IN VARCHAR2,p_line_id IN NUMBER) RETURN VARCHAR2
IS
  l_location gl_code_combinations_kfv.segment2%type;
  l_string gl_code_combinations_kfv.concatenated_segments%type;
  l_trx_number ra_customer_trx.trx_number%type;
  l_trx_date VARCHAR2(20);
BEGIN
	  SELECT gcc.segment2,
	    gcc.concatenated_segments,
	    rct.trx_number,
	    TO_CHAR(rct.trx_date)
	  INTO l_location,
	    l_string,
	    l_trx_number,
	    l_trx_date
	  FROM gl_code_combinations_kfv gcc,
	    ra_customer_trx rct,
	    ra_customer_trx_lines rctl,
	    ra_cust_trx_line_gl_dist rctlgl,
	    oe_order_headers oeh,
	    oe_order_lines oel
	  WHERE oeh.header_id             = oel.header_id
	  AND oeh.order_number            = p_order_number
	  AND oel.line_id                 = p_line_id
	  AND TO_CHAR(oeh.order_number)   = rctl.interface_line_attribute1
	  AND TO_CHAR(oel.line_id )       = rctL.interface_LINE_attribute6
	  AND rctl.line_type              ='LINE'
	  AND rct.customer_trx_id         = rctl.customer_trx_id
	  AND rctlgl.customer_trx_id      = rctl.customer_trx_id
	  AND rctlgl.customer_trx_line_id = rctl.customer_trx_line_id
	  AND gcc.code_combination_id     = rctlgl.code_combination_id;
	  IF UPPER(p_type)                ='LOCATION' THEN
	    RETURN l_location;
	  elsIF UPPER(P_TYPE) ='STRING' THEN
	    RETURN l_string;
	  elsif upper(p_type)='TRXNUM'THEN
	    RETURN l_trx_number;
	  elsif upper(p_type)='TRXDATE' THEN
	    RETURN l_trx_date;
	  END IF;
	EXCEPTION
	WHEN OTHERS THEN
	  RETURN NULL;
	  DBMS_OUTPUT.PUT_LINE('the error is '||SQLCODE||SQLERRM);
	END;

--Vendor Quote Batch

function GET_INT_REQ_SO_QTY  (P_INVENTORY_ITEM_ID number,P_ORGANIZATION_ID number)
 RETURN NUMBER IS 

L_INTER_SALES_ORDER_QTY NUMBER;
L_INTERNAL_REQ_QTY      NUMBER;
L_BOOKED_QTY            NUMBER;
L_AVIALBLE_QTY          NUMBER;
l_avialble_qty2         number;

BEGIN

      
 --query to get internal sales order qty--  
 
   /*   SELECT   SUM(OEL.ORDERED_QUANTITY-NVL(OL.FULFILLED_QUANTITY,0))
      into L_INTER_SALES_ORDER_QTY
FROM  OE_ORDER_LINES         OEL,
      OE_ORDER_HEADERS        OEH
     -- PO_REQUISITION_HEADERS  PORH,
     -- PO_REQUISITION_LINES    PORL
  where OEH.HEADER_ID               = OEL.HEADER_ID
 -- AND OEL.SOURCE_DOCUMENT_ID        = PORH.REQUISITION_HEADER_ID
--  AND OEL.SOURCE_DOCUMENT_LINE_ID   = PORL.REQUISITION_LINE_ID
 -- AND PORH.REQUISITION_HEADER_ID    = PORL.REQUISITION_HEADER_ID
  and OEL.SOURCE_TYPE_CODE          ='INTERNAL'
  and oeh. order_type_id = 1011   
 -- and PORL.SOURCE_TYPE_CODE         ='INVENTORY'
--  AND OEL.ORDER_SOURCE_ID           = 10             --order_source_id for 'Internal'
--AND oel.orig_sys_document_ref = Int_Req_num'
 -- and OEL.ORG_ID                    = PORH.ORG_ID
  and OEL.INVENTORY_ITEM_ID          = P_INVENTORY_ITEM_ID
  and OEL.SHIP_FROM_ORG_ID           = P_ORGANIZATION_ID
--  and PORH.TRANSFERRED_TO_OE_FLAG        = 'Y'
  and OL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED')
 and  exists(select 1 
                      from PO_REQUISITION_LINES prl
                       where  oh.source_document_id = prl.REQUISITION_HEADER_ID);*/
         select SUM(OEL.ORDERED_QUANTITY - NVL(OEL.FULFILLED_QUANTITY,0)- NVL(mr.reservation_quantity , 0))
          into  L_INTER_SALES_ORDER_QTY
          from  OE_ORDER_LINES_ALL         OEL,
                OE_ORDER_HEADERS_ALL        OEH,
                MTL_SALES_ORDERS MSO,
                MTL_RESERVATIONS MR
         where OEH.HEADER_ID                = OEL.HEADER_ID
          and OEL.SOURCE_TYPE_CODE          = 'INTERNAL'
          and OEH. ORDER_TYPE_ID            = 1011   
          and MR.INVENTORY_ITEM_ID(+)       = OEL.INVENTORY_ITEM_ID
          and MR.ORGANIZATION_ID (+)        = OEL.SHIP_FROM_ORG_ID
          AND MR.DEMAND_SOURCE_LINE_ID(+)   = OEL.LINE_ID
          and MSO.SALES_ORDER_ID(+)         = MR.DEMAND_SOURCE_HEADER_ID
         -- and OEL.ORDER_SOURCE_ID           = 10 
          and OEL.INVENTORY_ITEM_ID         = P_INVENTORY_ITEM_ID
          and OEL.SHIP_FROM_ORG_ID          = P_ORGANIZATION_ID
          and OeL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED')
          and  exists(select 1 
                              from PO_REQUISITION_LINES_ALL PRL
                               where  oeh.source_document_id = prl.REQUISITION_HEADER_ID);
  
  --query to get internal req which are not interfaced for sales orders ---
  
  
  SELECT SUM(PRL.QUANTITY)
  into L_INTERNAL_REQ_QTY
  from  PO_REQUISITION_LINES      PRL,
        PO_REQUISITION_HEADERS    PRH
  where PRH.REQUISITION_HEADER_ID   = PRL.REQUISITION_HEADER_ID
  AND PRH.TYPE_LOOKUP_CODE          = 'INTERNAL'
  AND PRL.SOURCE_TYPE_CODE          = 'INVENTORY'
  AND PRL.ITEM_ID                   = P_INVENTORY_ITEM_ID
  and PRL.SOURCE_ORGANIZATION_ID    = P_ORGANIZATION_ID
  AND PRH.TRANSFERRED_TO_OE_FLAG    = 'N';
  
  --Query to get all booked sales order qty's exculding the return orders and open quotes and internale sales orders
  
  SELECT  SUM(OL.ORDERED_QUANTITY - NVL(OL.FULFILLED_QUANTITY,0)- NVL(mr.reservation_quantity , 0))
      INTO  L_BOOKED_QTY
      FROM  OE_ORDER_LINES OL,
            OE_ORDER_HEADERS OH,
            MTL_SALES_ORDERS MSO,
            MTL_RESERVATIONS MR
      where OH.HEADER_ID                  =  OL.HEADER_ID
      and OL.INVENTORY_ITEM_ID          =  P_INVENTORY_ITEM_ID
      and OL.SHIP_FROM_ORG_ID           =  P_ORGANIZATION_ID
      and MR.INVENTORY_ITEM_ID(+)       = OL.INVENTORY_ITEM_ID
      and MR.ORGANIZATION_ID (+)        = OL.SHIP_FROM_ORG_ID
      AND MR.DEMAND_SOURCE_LINE_ID(+)   = OL.LINE_ID
      and MSO.SALES_ORDER_ID(+)         = MR.DEMAND_SOURCE_HEADER_ID
      AND (OL.ORDERED_QUANTITY-NVL(OL.FULFILLED_QUANTITY,0))>0
      AND OH.FLOW_STATUS_CODE IN ('BOOKED')
      and OH.TRANSACTION_PHASE_CODE     <> 'N'--NOT ALLOWED TO OPEN QUOTES
      and OL.LINE_CATEGORY_CODE         <>'RETURN' --NOT ALLOWED TO RETURN oRDERS
      and OL.SOURCE_TYPE_CODE           <> 'INTERNAL'--Not allowed to Internale Sales Orders
      --and OH. ORDER_TYPE_ID            <> 1012   --Not allowed to Internale Sales Orders
      --and OL.ORDER_SOURCE_ID           <> 10 --Not allowed to Internale Sales Orders
      and OL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED');
  
  L_AVIALBLE_QTY := GET_ISR_AVAIL_QTY (P_INVENTORY_ITEM_ID ,P_ORGANIZATION_ID );

 L_AVIALBLE_QTY2 := L_AVIALBLE_QTY-(nvl(L_BOOKED_QTY,0)+nvl(L_INTER_SALES_ORDER_QTY,0)+nvl(L_INTERNAL_REQ_QTY,0));
 
 RETURN L_AVIALBLE_QTY2;
 
 EXCEPTION WHEN OTHERS THEN
 L_AVIALBLE_QTY2 :=0;
 end;

function get_cash_from_date return date is
begin
return g_cash_from_date;
end;

function get_cash_to_date return date is
begin
return G_CASH_TO_DATE;
end;

function GET_ORG_ITEM_RESERVE_QTY (P_INVENTORY_ITEM_ID number,P_ORGANIZATION_ID number) return number IS
l_reserve_qty number;
begin

 SELECT NVL(SUM(MR.RESERVATION_QUANTITY),0)
        into  l_reserve_qty
      FROM    OE_ORDER_LINES OL,
              MTL_SALES_ORDERS MSO,
              MTL_RESERVATIONS MR
      WHERE MR.INVENTORY_ITEM_ID      = P_INVENTORY_ITEM_ID
        AND   MR.ORGANIZATION_ID        = P_ORGANIZATION_ID
        AND   MR.DEMAND_SOURCE_LINE_ID  = OL.LINE_ID
        --AND  ol.flow_status_CODE not in ('CLOSED')
        AND   MR.DEMAND_SOURCE_HEADER_ID=MSO.SALES_ORDER_ID;

return l_reserve_qty;

end;


function       GET_CREDIT_MEMO_TAX_VAL (P_TYPE IN VARCHAR2,P_CUSTOMER_TRX_ID in number,P_CUSTOMER_TRX_LINE_ID in number)
return number is 
L_TAX_VALUE number;
L_TAX_RATE NUMBER;
begin
select NVL(SUM(ZL.TAX_AMT),0) TAX_VAL,
       NVL(SUM(ZL.TAX_RATE),0) TAX_RATE
      into L_TAX_VALUE,
          L_TAX_RATE
from  zx_lines zl
 where  zl.trx_id                     = P_CUSTOMER_TRX_ID
  and ZL.TRX_LINE_ID                  = P_CUSTOMER_TRX_LINE_ID
  and ZL.APPLICATION_ID               = 222 ;
  if P_TYPE='TAXAMT' then
   return L_TAX_VALUE;
  ELSIF P_TYPE='TAXRATE' then
  return L_TAX_RATE;
  END IF;
  EXCEPTION
  when OTHERS then
  L_TAX_VALUE :=0;
  return L_TAX_VALUE;
  end;
  
  
  
  function GET_BEST_BUY (P_ITEM_ID number,P_ORGANIZATION_ID number,P_VENDOR_NUMBER varchar2) return number
  is
  l_best_price number;
  begin
  select MAX(NVL(POL.UNIT_PRICE,0))
  into L_BEST_PRICE
 from   PO_HEADERS          POH,
        PO_LINES            POL,
        PO_LINE_LOCATIONS   POLL,
        PO_VENDORS          PV
 where  POH.PO_HEADER_ID              = POL.PO_HEADER_ID
  and   POH.TYPE_LOOKUP_CODE          = 'BLANKET'
  and   POH.enabled_flag              = 'Y'
  AND   NVL (poh.cancel_flag, 'N')    = 'N'
  and   POLL.PO_HEADER_ID(+)          = POL.PO_HEADER_ID
  and   POLL.PO_LINE_ID  (+)          = POL.PO_LINE_ID
  and   POL.ITEM_ID                   = P_ITEM_ID
  and   nvl(POLL.SHIP_TO_ORGANIZATION_ID,P_ORGANIZATION_ID) = P_ORGANIZATION_ID
  and   POH.VENDOR_ID                 = PV.VENDOR_ID
  and   PV.SEGMENT1                   = P_VENDOR_NUMBER;
  
  return L_BEST_PRICE;
  
  EXCEPTION
  when NO_DATA_FOUND then
   L_BEST_PRICE :=0;
   return L_BEST_PRICE;
  when OTHERS then
  L_BEST_PRICE :=0;
   return L_BEST_PRICE;
   end;
  
  FUNCTION GET_CONTRACT_CUSTOMER_NAME(
      P_SITE_USE_CODE  IN VARCHAR2,
      P_QUAL_ATTRIBUTE IN VARCHAR2)
    RETURN VARCHAR2
  IS
    L_CUSTOMER_NAME VARCHAR2(2000);
  begin

  
    IF P_SITE_USE_CODE IN ('Bill To','Bill To Customer') THEN
      SELECT DISTINCT HP.PARTY_NAME
      INTO L_CUSTOMER_NAME
      FROM HZ_PARTIES HP,
        HZ_CUST_ACCOUNTS HCA
      WHERE HP.PARTY_ID      =HCA.PARTY_ID
      and HCA.CUST_ACCOUNT_ID=P_QUAL_ATTRIBUTE;
  
 RETURN L_CUSTOMER_NAME;
   -- elsif P_SITE_USE_CODE   IN ('Ship To','Ship To Customer') THEN
   else
      SELECT DISTINCT HP.PARTY_NAME
      INTO L_CUSTOMER_NAME
      FROM HZ_PARTIES HP,
        HZ_CUST_ACCOUNTS HCA,
        HZ_CUST_ACCT_SITES HCAS,
        HZ_CUST_SITE_USES_ALL HCSU
      WHERE HCSU.CUST_ACCT_SITE_ID=HCAS.CUST_ACCT_SITE_ID
      AND HCAS.CUST_ACCOUNT_ID    =HCA.CUST_ACCOUNT_ID
      and HP.PARTY_ID             =HCA.PARTY_ID
      and HCSU.SITE_USE_ID        =P_QUAL_ATTRIBUTE;

       RETURN L_CUSTOMER_NAME;
    end if;
   -- RETURN L_CUSTOMER_NAME;
  EXCEPTION
  WHEN OTHERS THEN
    L_CUSTOMER_NAME:=null;

    return L_CUSTOMER_NAME;
  END;

  procedure set_cost_location (P_ORGANIZATION_CODE VARCHAR2)
  is
  
  begin
  select organization_id
  into g_organization_id
  from mtl_parameters 
  where organization_code = P_ORGANIZATION_CODE;
   
  
  EXCEPTION
  when OTHERS then
   g_organization_id := NULL;
   end;
  
 
 function get_cost_location return number
 is 
 begin
  return g_organization_id;
 end;
 
Function get_req_source(p_organization_id number) 
RETURN VARCHAR2
IS
 l_org_code varchar2(20);
BEGIN

 select organization_code
   into l_org_code
  from  mtl_parameters mp
  WHERE mp.organization_id = p_organization_id;

  return l_org_code;
 EXCEPTION
 WHEN OTHERS THEN
   RETURN NULL;
END;


FUNCTION get_order_line_po_cost(p_inventory_item_id NUMBER, p_organization_id NUMBER,P_Order_Date Date) RETURN Number
IS
 l_po_cost number;
BEGIN

 select max(actual_material)
   into l_po_cost
   from CST_CG_COST_HISTORY_V  
  WHERE organization_id = p_organization_id
    AND inventory_item_id = p_inventory_item_id
    and  transaction_id =(
    select max(transaction_id) FROM CST_CG_COST_HISTORY_V
    WHERE organization_id = p_organization_id
    AND inventory_item_id = p_inventory_item_id
   -- AND transaction_type IN ('PO Receipt','Account receipt','Account alias receipt')
    AND transaction_type = 'PO Receipt'    
    AND transaction_costed_date <= P_Order_Date);


  return l_po_cost;
 EXCEPTION
 WHEN OTHERS THEN
   RETURN NULL;
END;

       
END EIS_RS_XXWC_COM_UTIL_PKG;
/