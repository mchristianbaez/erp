--Report Name            : Usage Tax Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_USAGE_TAX_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_USAGE_TAX_V
Create or replace View XXEIS.EIS_XXWC_AR_USAGE_TAX_V
(FISCAL_MONTH,SEGMENT1,TYPE,LOCATION,DESCRIPTION,BRANCH,STATE,CITY,CITY_TAX_RATE,TAX_AMT,VALUE,AMT) AS 
SELECT oap.period_name fiscal_month,
    mgp.segment1,
    mgp.segment1
    ||' Adjustment' Type,
    haou.name location,
    mgp.description,
    gcc.segment2 branch,
    --  msi.concatenated_segments item,
    haou.region_2 state,
    HAOU.TOWN_OR_CITY CITY ,
    xxeis.eis_rs_xxwc_com_util_pkg.get_usetax_rate(haou.postal_code)  city_tax_rate,
    --NVL(tax.curuserate,0)                                              +NVL(ctax.curuserate,0)+NVL(stax.curuserate,0) city_tax_rate,
    (NVL(mmt.primary_quantity,0)* NVL(mmt.actual_cost,0))* (  xxeis.eis_rs_xxwc_com_util_pkg.get_usetax_rate(haou.postal_code)) tax_amt,
    (NVL(mmt.primary_quantity,0)* NVL(mmt.actual_cost,0)) value,
    xxeis.eis_rs_xxwc_com_util_pkg.get_usetax_amt((mmt.primary_quantity*mmt.actual_cost),haou.postal_code) amt
  FROM mtl_transaction_types mtt,
    org_acct_periods oap,
    mtl_system_items_kfv msi,
    mtl_material_transactions mmt,
    mtl_generic_dispositions mgp,
    mtl_parameters mp,
    hr_organization_units_v haou,
    gl_code_combinations gcc--,
   -- taxlocltax tax,
   -- taxware.taxcntytax ctax,
    --taxware.taxsttax stax
  WHERE msi.organization_id          = mp.organization_id
  AND mmt.organization_id            = mp.organization_id
  AND mmt.inventory_item_id          = msi.inventory_item_id
  AND mmt.transaction_source_type_id = 6
  AND mgp.disposition_id             = mmt.transaction_source_id
  AND mgp.organization_id            = mp.organization_id
  AND oap.organization_id (+)        = mmt.organization_id
  AND oap.acct_period_id (+)         = mmt.acct_period_id
  --AND haou.postal_code               = tax.zipcode(+)
  --AND tax.cntycode                   = ctax.cntycode(+)
  --AND tax.stcode                     = stax.stcode
  AND mmt.transaction_type_id = mtt.transaction_type_id(+)
  AND haou.organization_id = msi.organization_id
  AND mgp.distribution_account = gcc.code_combination_id(+)
  AND mgp.segment1            IN('TYPE 6','TYPE 7','TYPE 8','TYPE 9','TYPE 11','TYPE 12','TYPE 25')
  AND EXISTS
    (SELECT 1
    FROM xxeis.eis_org_access_v
    WHERE organization_id = msi.organization_id
    )
/
set scan on define on
prompt Creating View Data for Usage Tax Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_USAGE_TAX_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_USAGE_TAX_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Usage Tax V','EXAUTV');
--Delete View Columns for EIS_XXWC_AR_USAGE_TAX_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_USAGE_TAX_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_USAGE_TAX_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','CITY',222,'City','CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','City');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','STATE',222,'State','STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','State');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','LOCATION',222,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','FISCAL_MONTH',222,'Fiscal Month','FISCAL_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fiscal Month');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','BRANCH',222,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','AMT',222,'Amt','AMT','','','','XXEIS_RS_ADMIN','NUMBER','','','Amt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','CITY_TAX_RATE',222,'City Tax Rate','CITY_TAX_RATE','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','City Tax Rate');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','DESCRIPTION',222,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','SEGMENT1',222,'Segment1','SEGMENT1','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Segment1');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','TAX_AMT',222,'Tax Amt','TAX_AMT','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Tax Amt');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','VALUE',222,'Value','VALUE','','~,~2','','XXEIS_RS_ADMIN','NUMBER','','','Value');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_USAGE_TAX_V','TYPE',222,'Type','TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Type');
--Inserting View Components for EIS_XXWC_AR_USAGE_TAX_V
--Inserting View Component Joins for EIS_XXWC_AR_USAGE_TAX_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Usage Tax Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Usage Tax Report
xxeis.eis_rs_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_NAME
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE              
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_NAME','','Fiscal Month','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Usage Tax Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Usage Tax Report
xxeis.eis_rs_utility.delete_report_rows( 'Usage Tax Report' );
--Inserting Report - Usage Tax Report
xxeis.eis_rs_ins.r( 222,'Usage Tax Report','','Provide the detail of taxes applied to usage of product.','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_USAGE_TAX_V','Y','','','XXEIS_RS_ADMIN','N','N','White Cap Reports','RTF,PDF,','CSV,HTML,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Usage Tax Report
xxeis.eis_rs_ins.rc( 'Usage Tax Report',222,'CITY','City','City','','','','','5','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_USAGE_TAX_V','','');
xxeis.eis_rs_ins.rc( 'Usage Tax Report',222,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','','','1','N','','ROW_FIELD','','','','1','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_USAGE_TAX_V','','');
xxeis.eis_rs_ins.rc( 'Usage Tax Report',222,'LOCATION','Location','Location','','','','','2','N','','ROW_FIELD','','','','2','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_USAGE_TAX_V','','');
xxeis.eis_rs_ins.rc( 'Usage Tax Report',222,'STATE','State','State','','','','','4','N','','PAGE_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_USAGE_TAX_V','','');
xxeis.eis_rs_ins.rc( 'Usage Tax Report',222,'BRANCH','Segment Location','Branch','','','','','3','N','','ROW_FIELD','','','','3','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_USAGE_TAX_V','','');
xxeis.eis_rs_ins.rc( 'Usage Tax Report',222,'TAX_AMT','Tax Amt','Tax Amt','','','','','9','N','','DATA_FIELD','','SUM','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_USAGE_TAX_V','','');
xxeis.eis_rs_ins.rc( 'Usage Tax Report',222,'TYPE','Type','Type','','','','','6','N','','COL_FIELD','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_USAGE_TAX_V','','');
xxeis.eis_rs_ins.rc( 'Usage Tax Report',222,'VALUE','Amount','Value','','','','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_USAGE_TAX_V','','');
xxeis.eis_rs_ins.rc( 'Usage Tax Report',222,'CITY_TAX_RATE','City Tax Rate','City Tax Rate','','','','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_USAGE_TAX_V','','');
--Inserting Report Parameters - Usage Tax Report
xxeis.eis_rs_ins.rp( 'Usage Tax Report',222,'Fiscal Month','Fiscal Month','FISCAL_MONTH','IN','Fiscal Month','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Usage Tax Report
xxeis.eis_rs_ins.rcn( 'Usage Tax Report',222,'FISCAL_MONTH','IN',':Fiscal Month','','','Y','','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Usage Tax Report
xxeis.eis_rs_ins.rs( 'Usage Tax Report',222,'FISCAL_MONTH','ASC','XXEIS_RS_ADMIN');
--Inserting Report Triggers - Usage Tax Report
--Inserting Report Templates - Usage Tax Report
--Inserting Report Portals - Usage Tax Report
--Inserting Report Dashboards - Usage Tax Report
--Inserting Report Security - Usage Tax Report
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','401','','50619',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','401','','50879',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','401','','50851',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','401','','51029',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','401','','50852',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','401','','50821',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','222','','51004',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','222','','50871',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','1','','51028',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','401','','50872',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','222','','21404',222,'XXEIS_RS_ADMIN','');
xxeis.eis_rs_ins.rsec( 'Usage Tax Report','222','','20678',222,'XXEIS_RS_ADMIN','');
END;
/
set scan on define on
