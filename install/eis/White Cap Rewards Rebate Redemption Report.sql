--Report Name            : White Cap Rewards Rebate Redemption Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_OM_REWARDS_RED_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_OM_REWARDS_RED_V
Create or replace View XXEIS.EIS_XXWC_OM_REWARDS_RED_V
(CUSTOMER_NUMBER,CUSTOMER_NAME,SALES_AMOUNT,TAX_VALUE,BRANCH,REBATE_VOUCHER_NUMBER,REBATE_VOUCHER_AMOUNT,DATE_REDEEMED,ORDER_NUMBER,INVOICE_NUMBER,SALESREP_NAME,SALESREP_NUMBER,BILL_TO_STATE) AS 
SELECT CUST_ACCT.ACCOUNT_NUMBER CUSTOMER_NUMBER,
    NVL(CUST_ACCT.ACCOUNT_NAME,PARTY.PARTY_NAME) CUSTOMER_NAME,
    SUM(NVL(ol.unit_selling_price,0)                           * DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity)) Sales_Amount,
    SUM(NVL(DECODE(ol.line_category_code,'RETURN',(ol.tax_value*-1),ol.tax_value),0)) tax_value,
    SHIP_FROM_ORG.ORGANIZATION_CODE Branch,
    MAX(oe.check_number) Rebate_Voucher_Number,
    SUM(oe.PAYMENT_AMOUNT) rebate_voucher_Amount,
    MAX(TRUNC(oe.creation_date)) Date_Redeemed,
    OH.ORDER_NUMBER ,
    RCT.TRX_NUMBER INVOICE_NUMBER, 
    REP.NAME SALESREP_NAME,
    REP.SALESREP_NUMBER,
    upper(hl.state) bill_to_state
    ---Primary Keys
    --descr#flexfield#start
    --descr#flexfield#end
    --gl#accountff#start
    --gl#accountff#end
  FROM mtl_parameters ship_from_org,
    hz_parties party,
    hz_parties partyb,
    hz_cust_accounts cust_acct,
    oe_order_headers oh,
    oe_order_lines ol,
    oe_payments oe,
    ra_customer_trx rct,
    hz_cust_site_uses hcsu,
    hz_cust_acct_sites hcas,
    hz_party_sites hps,
    hz_locations hl,
    Ra_Salesreps Rep,
    AR_RECEIPT_METHODS arc
  WHERE Ol.Sold_To_Org_Id           = Cust_Acct.Cust_Account_Id
  AND cust_acct.party_id            = party.party_id
  AND OE.HEADER_ID                  = OH.HEADER_ID
  AND rct.bill_to_site_use_id       = hcsu.site_use_id
  AND hcsu.CUST_ACCT_SITE_ID        = hcas.CUST_ACCT_SITE_ID
  AND hcas.party_site_id            = hps.party_site_id
  AND hps.location_id               = hl.location_id   
  AND partyb.party_id               = hps.party_id
  AND oe.receipt_method_id          = arc.receipt_method_id
  AND rct.interface_header_context  = 'ORDER ENTRY'
  AND TO_CHAR (oh.order_number)     = TO_CHAR (rct.interface_header_attribute1)
  AND OH.SALESREP_ID                = REP.SALESREP_ID(+)
  AND oh.org_id                     = rep.org_id(+)
  AND oh.Ship_From_Org_Id           = Ship_From_Org.Organization_Id(+)
  AND Ol.Header_Id                  = Oh.Header_Id
  AND upper(arc.name)               LIKE Upper('%Rewards%Rebate%')
    /*AND (upper(arc.name) LIKE '%GIFT%'
    OR upper(arc.name) LIKE '%REBATE%')*/
  GROUP BY CUST_ACCT.ACCOUNT_NUMBER,
    NVL(CUST_ACCT.ACCOUNT_NAME,PARTY.PARTY_NAME),
    SHIP_FROM_ORG.ORGANIZATION_CODE,
    oe.check_number,
    OH.ORDER_number,
    rct.trx_number,
    REP.NAME,
    REP.SALESREP_NUMBER,
    hl.state
/
set scan on define on
prompt Creating View Data for White Cap Rewards Rebate Redemption Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_REWARDS_RED_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_REWARDS_RED_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Rewards Red V','EXORRV','','');
--Delete View Columns for EIS_XXWC_OM_REWARDS_RED_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_REWARDS_RED_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_REWARDS_RED_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','DATE_REDEEMED',660,'Date Redeemed','DATE_REDEEMED','','','','XXEIS_RS_ADMIN','DATE','','','Date Redeemed','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','REBATE_VOUCHER_AMOUNT',660,'Rebate Voucher Amount','REBATE_VOUCHER_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Rebate Voucher Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','REBATE_VOUCHER_NUMBER',660,'Rebate Voucher Number','REBATE_VOUCHER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Rebate Voucher Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','BRANCH',660,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','TAX_VALUE',660,'Tax Value','TAX_VALUE','','','','XXEIS_RS_ADMIN','NUMBER','','','Tax Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','SALES_AMOUNT',660,'Sales Amount','SALES_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','BILL_TO_STATE',660,'Bill To State','BILL_TO_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Bill To State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_REWARDS_RED_V','SALESREP_NUMBER',660,'Salesrep Number','SALESREP_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesrep Number','','','');
--Inserting View Components for EIS_XXWC_OM_REWARDS_RED_V
--Inserting View Component Joins for EIS_XXWC_OM_REWARDS_RED_V
END;
/
set scan on define on
prompt Creating Report LOV Data for White Cap Rewards Rebate Redemption Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - White Cap Rewards Rebate Redemption Report
xxeis.eis_rs_ins.lov( 660,'select  RS.Name,SALESREP_ID  from  RA_SALESREPS RS, HR_OPERATING_UNITS OU
WHERE RS.org_id = OU.organization_id
AND RS.NAME is not null','','OM SALES REP','This gives the sales representative name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct state from hz_locations','','HDS Bill To State','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for White Cap Rewards Rebate Redemption Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - White Cap Rewards Rebate Redemption Report
xxeis.eis_rs_utility.delete_report_rows( 'White Cap Rewards Rebate Redemption Report' );
--Inserting Report - White Cap Rewards Rebate Redemption Report
xxeis.eis_rs_ins.r( 660,'White Cap Rewards Rebate Redemption Report','','The purpose of this report is to provide Marketing with rebate redemption details (i.e. Preferred Cash Customer rebate usage) in support of the White Cap Rewards Program.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_REWARDS_RED_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - White Cap Rewards Rebate Redemption Report
xxeis.eis_rs_ins.rc( 'White Cap Rewards Rebate Redemption Report',660,'BRANCH','Redeemed at Branch Number','Branch','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_REWARDS_RED_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Rewards Rebate Redemption Report',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_REWARDS_RED_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Rewards Rebate Redemption Report',660,'CUSTOMER_NUMBER','Customer Account Number','Customer Number','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_REWARDS_RED_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Rewards Rebate Redemption Report',660,'DATE_REDEEMED','Date Redeemed','Date Redeemed','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_REWARDS_RED_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Rewards Rebate Redemption Report',660,'REBATE_VOUCHER_AMOUNT','Rebate Voucher Amount','Rebate Voucher Amount','','~T~D~2','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_REWARDS_RED_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Rewards Rebate Redemption Report',660,'REBATE_VOUCHER_NUMBER','Redeemed Rebate Voucher Number','Rebate Voucher Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_REWARDS_RED_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Rewards Rebate Redemption Report',660,'SALES_AMOUNT','Total Sales Amount','Sales Amount','','~T~D~2','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_REWARDS_RED_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Rewards Rebate Redemption Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_REWARDS_RED_V','','');
xxeis.eis_rs_ins.rc( 'White Cap Rewards Rebate Redemption Report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_REWARDS_RED_V','','');
--Inserting Report Parameters - White Cap Rewards Rebate Redemption Report
xxeis.eis_rs_ins.rp( 'White Cap Rewards Rebate Redemption Report',660,'White Cap Branch','White Cap Branch','BRANCH','IN','AR Organizaion Code LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Rewards Rebate Redemption Report',660,'Redeemed From Date','Redeemed From Date','DATE_REDEEMED','>=','','','DATE','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Rewards Rebate Redemption Report',660,'Redeemed To Date','Redeemed To Date','DATE_REDEEMED','<=','','','DATE','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Rewards Rebate Redemption Report',660,'Customer Bill To State','Bill To State','BILL_TO_STATE','IN','HDS Bill To State','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'White Cap Rewards Rebate Redemption Report',660,'Salesrep Name','Salesrep Name','SALESREP_NAME','IN','OM SALES REP','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - White Cap Rewards Rebate Redemption Report
xxeis.eis_rs_ins.rcn( 'White Cap Rewards Rebate Redemption Report',660,'DATE_REDEEMED','>=',':Redeemed From Date','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap Rewards Rebate Redemption Report',660,'DATE_REDEEMED','<=',':Redeemed To Date','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap Rewards Rebate Redemption Report',660,'BRANCH','IN',':White Cap Branch','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap Rewards Rebate Redemption Report',660,'BILL_TO_STATE','IN',':Customer Bill To State','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'White Cap Rewards Rebate Redemption Report',660,'SALESREP_NAME','IN',':Salesrep Name','','','Y','5','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - White Cap Rewards Rebate Redemption Report
--Inserting Report Triggers - White Cap Rewards Rebate Redemption Report
--Inserting Report Templates - White Cap Rewards Rebate Redemption Report
--Inserting Report Portals - White Cap Rewards Rebate Redemption Report
--Inserting Report Dashboards - White Cap Rewards Rebate Redemption Report
--Inserting Report Security - White Cap Rewards Rebate Redemption Report
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','51065',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','51025',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'White Cap Rewards Rebate Redemption Report','660','','21623',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - White Cap Rewards Rebate Redemption Report
END;
/
set scan on define on
