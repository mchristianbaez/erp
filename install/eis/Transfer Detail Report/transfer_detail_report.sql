--Report Name            : Transfer Detail Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_PO_TRANSFER_DTL_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_PO_TRANSFER_DTL_V
Create or replace View XXEIS.EIS_XXWC_PO_TRANSFER_DTL_V
 AS 
SELECT DECODE (ol.option_number,'',(ol.line_number
    ||'.'
    ||ol.shipment_number),(ol.line_number
    ||'.'
    ||ol.shipment_number
    ||'.'
    ||ol.option_number)) order_line,
    mtp1.organization_code ship_to_org,
    ood.organization_code ship_from_org ,
    ood.organization_name ship_from_name,
    oh.attribute7 buyer,
    --ppf.full_name buyer,
    tab.item_number item,
    tab.description item_description,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_item_crossref(tab.inventory_item_id,tab.organization_id) SUPPLIER_ITEM_NUMBER,
    cr.cross_reference SUPPLIER_ITEM_NUMBER,
    --NULL VENDOR_NUMBER,
    tab.vendor_num VENDOR_NUMBER,
    tab.vendor_name vendor,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_vendor_name(msi.inventory_item_id,msi.organization_id) vendor,
    tab.source SOURCE,
    ol.ordered_quantity ord_qty,
    ol.shipping_quantity qty_delivered,
    ol.order_quantity_uom uom,
    TRUNC(oh.ordered_date) ordered_date,
    TRUNC(ol.actual_shipment_date) ship_date,
    oh.order_number iso,
    hl.TOWN_OR_CITY ship_from_city,
    hl.REGION_2 ship_from_state,
    ship_to_hl.city ship_to_city,
    ship_to_hl.state ship_to_state ,
    -- NVL(flv.meaning, initcap(REPLACE(oh.flow_status_code,'_',' '))) order_status,
    XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.GET_ORDER_LINE_STATUS(OL.LINE_ID) order_status,
    flv_ship_mtd.meaning ship_method,
    --xxeis.eis_rs_utility.get_lookup_meaning('SHIPPING_METHOD',ol.shipping_method_code,660) ship_method,
    tab.inv_cat_seg1
    ||'.'
    ||tab.cat cat_class,
    fvc.description
    ||'.'
    ||fvcc.description cat_class_desc,
    --xxeis.eis_rs_xxwc_com_util_pkg.get_inv_cat_class_desc(tab.inventory_item_id,tab.organization_id) cat_class_desc,
    --null cat_class_desc,
    tab.region Ship_from_region,
    tab.district Ship_from_district,
    mtp1.attribute9 Ship_to_region,
    mtp1.attribute8 Ship_to_district,
    NVL(ol.unit_cost,0) item_cost,
    DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity) * NVL(ol.unit_cost,0) ext$
  FROM org_organization_definitions ood,
    oe_order_lines ol,
    oe_order_headers oh,
    hz_cust_site_uses ship_to_hcsu,
    hz_cust_acct_sites ship_to_hcas,
    hz_party_sites ship_to_hps,
    PO_LOCATION_ASSOCIATIONS pla,
    hz_locations ship_to_hl,
    hz_parties party,
    hz_cust_accounts cust_acct,
    oe_transaction_types_vl oth,
    fnd_lookup_values_vl flv,
    mtl_parameters mtp1, -- Ship to Org
    hr_organization_units hou,
    hr_locations hl, -- ship_to_org location
    xxeis.eis_xxwc_po_isr_tab tab,
    fnd_flex_values_vl fvc,
    fnd_flex_values_vl fvcc,
    (SELECT MAX(b.cross_reference) cross_reference,
      b.inventory_item_id
    FROM MTL_CROSS_REFERENCES_B b
    WHERE b.cross_reference_type(+) = 'VENDOR'
    GROUP BY inventory_item_id
    )cr,
    FND_LOOKUP_VALUES_VL FLV_SHIP_MTD
  WHERE 1                            = 1
  AND oh.header_id                   = ol.header_id
  AND ol.ship_from_org_id            = ood.organization_id
  AND ol.ship_to_org_id              = ship_to_hcsu.site_use_id
  AND ship_to_hcsu.site_use_id       = pla.site_use_id
  AND ship_to_hcsu.cust_acct_site_id = ship_to_hcas.cust_acct_site_id
  AND ship_to_hcas.party_site_id     = ship_to_hps.party_site_id
  AND ship_to_hps.location_id        = ship_to_hl.location_id
  AND party.party_id                 = ship_to_hps.party_id
  AND ol.sold_to_org_id              = cust_acct.cust_account_id
  AND party.party_id                 = cust_acct.party_id
  AND oh.order_type_id               = oth.transaction_type_id
  AND oth.name                       = 'INTERNAL ORDER'
  AND ol.inventory_item_id           = tab.inventory_item_id(+)
  AND ol.ship_from_org_id            = tab.organization_id(+)
  AND tab.inventory_item_id          = cr.inventory_item_id(+)
  AND fvc.flex_value_set_id          = 1015058
  AND fvcc.flex_value_set_id         = 1015059
  AND fvc.flex_value                 = tab.inv_cat_seg1
  AND fvcc.flex_value                = tab.cat
  AND flv.lookup_type(+)             = 'FLOW_STATUS'
  AND FLV_SHIP_MTD.LOOKUP_TYPE(+)    ='SHIP_METHOD'
  AND FLV_SHIP_MTD.LOOKUP_CODE(+)    = OL.SHIPPING_METHOD_CODE
  AND flv.lookup_code(+)             = oh.flow_status_code
  AND ol.shipping_quantity           IS NOT NULL
  AND mtp1.organization_id  (+)      = pla.organization_id
  AND ol.ship_from_org_id            = hou.organization_id
  AND hou.location_id                = hl.location_id
  AND (OL.FLOW_STATUS_CODE           IN ('CLOSED' )
      OR ol.user_item_description    ='DELIVERED')
/
set scan on define on
prompt Creating View Data for Transfer Detail Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_PO_TRANSFER_DTL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_PO_TRANSFER_DTL_V',201,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Po Transfer Dtl V','EXPTDV','','');
--Delete View Columns for EIS_XXWC_PO_TRANSFER_DTL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_PO_TRANSFER_DTL_V',201,FALSE);
--Inserting View Columns for EIS_XXWC_PO_TRANSFER_DTL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','EXT$',201,'Ext$','EXT$','','','','XXEIS_RS_ADMIN','NUMBER','','','Ext$','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ITEM_COST',201,'Item Cost','ITEM_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Item Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','CAT_CLASS_DESC',201,'Cat Class Desc','CAT_CLASS_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','CAT_CLASS',201,'Cat Class','CAT_CLASS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Cat Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_METHOD',201,'Ship Method','SHIP_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ORDER_STATUS',201,'Order Status','ORDER_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_STATE',201,'Ship To State','SHIP_TO_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_CITY',201,'Ship To City','SHIP_TO_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_STATE',201,'Ship From State','SHIP_FROM_STATE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship From State','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_CITY',201,'Ship From City','SHIP_FROM_CITY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship From City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_DATE',201,'Ship Date','SHIP_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ship Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ORDERED_DATE',201,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','UOM',201,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','QTY_DELIVERED',201,'Qty Delivered','QTY_DELIVERED','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty Delivered','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ORD_QTY',201,'Ord Qty','ORD_QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Ord Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SOURCE',201,'Source','SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','VENDOR',201,'Vendor','VENDOR','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','VENDOR_NUMBER',201,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SUPPLIER_ITEM_NUMBER',201,'Supplier Item Number','SUPPLIER_ITEM_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Supplier Item Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ITEM_DESCRIPTION',201,'Item Description','ITEM_DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ITEM',201,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','BUYER',201,'Buyer','BUYER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Buyer','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_NAME',201,'Ship From Name','SHIP_FROM_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship From Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_ORG',201,'Ship From Org','SHIP_FROM_ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship From Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_ORG',201,'Ship To Org','SHIP_TO_ORG','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Org','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ORDER_LINE',201,'Order Line','ORDER_LINE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Line','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','ISO',201,'Iso','ISO','','','','XXEIS_RS_ADMIN','NUMBER','','','Iso','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_DISTRICT',201,'Ship From District','SHIP_FROM_DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship From District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_FROM_REGION',201,'Ship From Region','SHIP_FROM_REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship From Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_DISTRICT',201,'Ship To District','SHIP_TO_DISTRICT','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To District','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_PO_TRANSFER_DTL_V','SHIP_TO_REGION',201,'Ship To Region','SHIP_TO_REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Ship To Region','','','');
--Inserting View Components for EIS_XXWC_PO_TRANSFER_DTL_V
--Inserting View Component Joins for EIS_XXWC_PO_TRANSFER_DTL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Transfer Detail Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Transfer Detail Report
xxeis.eis_rs_ins.lov( 201,'select distinct PAPF.FULL_NAME,HOU.name OPERATING_UNIT  from PO_VENDORS POV,
       po_vendor_sites_all pvs,
       PER_ALL_PEOPLE_F PAPF,
       po_headers_all poh,HR_OPERATING_UNITS HOU
 WHERE poh.vendor_id = pov.vendor_id
   AND poh.vendor_site_id = pvs.vendor_site_id
   and POH.TYPE_LOOKUP_CODE in (''STANDARD'', ''BLANKET'', ''PLANNED'')
   and POH.AGENT_ID = PAPF.PERSON_ID
   and PVS.ORG_ID=HOU.ORGANIZATION_ID
   and exists(select 1 from XXEIS.EIS_MO_ORG_TMP_V
            WHERE ORG_ID=pvs.ORG_ID)','','BUYER_NAME','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Item'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Item List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Supplier'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Supplier List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select list_name from XXWC_PARAM_LIST where list_type = ''Cat Class''  and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Cat Class List','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct pov.segment1 vendor_number, pov.vendor_name from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC Vendors','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,
  ood.organization_name organization_name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE organization_code NOT IN(''CAN'',''HDS'',''US1'',''CN1'')
ORDER BY organization_code','','XXWC Org Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'SELECT ood.organization_code organization_code,
  ood.organization_name organization_name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE organization_code NOT IN(''CAN'',''HDS'',''US1'',''CN1'')
ORDER BY organization_code','','XXWC Org Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 201,'select distinct pov.vendor_name , pov.segment1 vendor_number from po_vendors pov, mtl_system_items_b msi, mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr
WHERE msi.inventory_item_id         = msa.inventory_item_id
  AND msi.organization_id           = msa.organization_id
  AND msa.sourcing_rule_id          = msro.sourcing_rule_id
  AND msa.sourcing_rule_id          = msr.sourcing_rule_id
  AND msro.sr_receipt_id            = msso.sr_receipt_id
  AND msso.vendor_id                = pov.vendor_id
  AND msi.source_type               = 2','','XXWC Vendor Name','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Transfer Detail Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Transfer Detail Report
xxeis.eis_rs_utility.delete_report_rows( 'Transfer Detail Report' );
--Inserting Report - Transfer Detail Report
xxeis.eis_rs_ins.r( 201,'Transfer Detail Report','','Report to pull detailed transfer history between orgs and DC/HUBs','','','','XXEIS_RS_ADMIN','EIS_XXWC_PO_TRANSFER_DTL_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','EXCEL,','N');
--Inserting Report Columns - Transfer Detail Report
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'BUYER','Buyer','Buyer','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'CAT_CLASS','Cat Class','Cat Class','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'CAT_CLASS_DESC','Cat Class Description','Cat Class Desc','','','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'EXT$','Ext$','Ext$','','~T~D~2','default','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'ITEM','Item','Item','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'ITEM_COST','Item Cost','Item Cost','','~T~D~2','default','','28','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'ORDERED_DATE','Order Date','Ordered Date','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'ORDER_STATUS','Order Line Status','Order Status','','','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'ORD_QTY','Ord Qty','Ord Qty','','~~~','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'QTY_DELIVERED','Qty Delivered','Qty Delivered','','~~~','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_DATE','Ship Date','Ship Date','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_FROM_CITY','Ship From City','Ship From City','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_FROM_NAME','Ship From Name','Ship From Name','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_FROM_ORG','Ship From Org','Ship From Org','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_FROM_STATE','Ship From State','Ship From State','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_METHOD','Ship Method','Ship Method','','','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_CITY','Ship to City','Ship To City','','','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_ORG','Ship To Org','Ship To Org','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_STATE','Ship to State','Ship To State','','','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SOURCE','Source','Source','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SUPPLIER_ITEM_NUMBER','Supplier Item Number','Supplier Item Number','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'UOM','UOM','Uom','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'VENDOR','Vendor','Vendor','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_DISTRICT','District','Ship To District','','','default','','27','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'SHIP_TO_REGION','Region','Ship To Region','','','default','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'ISO','ISO','Iso','','~~~','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
xxeis.eis_rs_ins.rc( 'Transfer Detail Report',201,'ORDER_LINE','Order Line#','Order Line','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_PO_TRANSFER_DTL_V','','');
--Inserting Report Parameters - Transfer Detail Report
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Ship to Region','Ship to Region','SHIP_TO_REGION','IN','Region Lov','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Ship to District','Ship to District','SHIP_TO_DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Ship to Location','Ship to Location','SHIP_TO_ORG','IN','XXWC Org Lov','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Ship to Location List','Ship to Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Ship From Region','Ship From Region','SHIP_FROM_REGION','IN','Region Lov','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Ship From District','Ship From District','SHIP_FROM_DISTRICT','IN','District Lov','','VARCHAR2','N','Y','6','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Ship From Location','Ship From Location','SHIP_FROM_ORG','IN','XXWC Org Lov','','VARCHAR2','N','Y','7','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Ship From Location List','Ship From Location List','','IN','XXWC Org List','','VARCHAR2','N','Y','8','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Vendor Name','Vendor Name','VENDOR','IN','XXWC Vendor Name','','VARCHAR2','N','Y','9','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Vendor Number','Vendor Number','VENDOR_NUMBER','IN','XXWC Vendors','','VARCHAR2','N','Y','10','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Vendor List','Vendor List','','IN','XXWC Supplier List','','VARCHAR2','N','Y','11','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Parts List','Parts List','','IN','XXWC Item List','','VARCHAR2','N','Y','12','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'CatClass List','CatClass List','','IN','XXWC Cat Class List','','VARCHAR2','N','Y','13','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Order Date From','Order Date From','ORDERED_DATE','>=','','','DATE','N','Y','14','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Order Date To','Order Date To','ORDERED_DATE','<=','','','DATE','N','Y','15','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Actual Ship Date From','Actual Ship Date From','SHIP_DATE','>=','','','DATE','N','Y','16','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Actual Ship Date To','Actual Ship Date To','SHIP_DATE','<=','','','DATE','N','Y','17','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Transfer Detail Report',201,'Buyer Name','Buyer Name','BUYER','IN','BUYER_NAME','','VARCHAR2','N','N','18','','N','CONSTANT','XXEIS_RS_ADMIN','N','N','','','');
--Inserting Report Conditions - Transfer Detail Report
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'ORDERED_DATE','>=',':Order Date From','','','Y','14','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'ORDERED_DATE','<=',':Order Date To','','','Y','15','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'SHIP_DATE','>=',':Actual Ship Date From','','','Y','16','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'SHIP_DATE','<=',':Actual Ship Date To','','','Y','17','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'BUYER','IN',':Buyer Name','','','Y','14','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'VENDOR','IN',':Vendor Name','','','Y','9','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'SHIP_TO_REGION','IN',':Ship to Region','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'SHIP_TO_DISTRICT','IN',':Ship to District','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'SHIP_TO_ORG','IN',':Ship to Location','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'SHIP_FROM_REGION','IN',':Ship From Region','','','Y','5','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'SHIP_FROM_DISTRICT','IN',':Ship From District','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'SHIP_FROM_ORG','IN',':Ship From Location','','','Y','7','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'VENDOR_NUMBER','IN',':Vendor Number','','','Y','10','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Transfer Detail Report',201,'','','','','AND (:Parts List is null or EXISTS (SELECT 1  FROM 
                         xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :Parts List
                            AND LIST_TYPE =''Item''
                           AND process_id = :SYSTEM.PROCESS_ID
                           AND list_value= EXPTDV.ITEM) 
     )
AND (:Vendor List is null or EXISTS (SELECT 1  FROM 
                         xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :Vendor List
                            AND LIST_TYPE =''Supplier''
                            AND process_id = :SYSTEM.PROCESS_ID
                            AND EXPTDV.VENDOR_NUMBER= list_value) 
     )
AND (:CatClass List is null or EXISTS (SELECT 1  FROM 
                        xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :CatClass List
                            AND LIST_TYPE =''Cat Class''
                            AND process_id = :SYSTEM.PROCESS_ID
                            AND EXPTDV.CAT_CLASS = list_value) 
     )
AND  (:Ship to Location List IS NULL OR EXISTS (SELECT 1  FROM 
                        xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :Ship to Location List                            
                           AND LIST_TYPE =''Org''
                           AND process_id = :SYSTEM.PROCESS_ID     
                           AND LPAD(EXPTDV.ship_to_org,3,''0'')= list_value)
     )
AND  (:Ship From Location List IS NULL OR EXISTS (SELECT 1  FROM 
                         xxeis.EIS_XXWC_PARAM_PARSE_LIST
                         WHERE LIST_NAME= :Ship From Location List                            
                           AND LIST_TYPE =''Org''
                           AND process_id = :SYSTEM.PROCESS_ID     
                           AND LPAD(EXPTDV.ship_from_org,3,''0'')= list_value)
     )

        
        ','Y','0','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Transfer Detail Report
--Inserting Report Triggers - Transfer Detail Report
xxeis.eis_rs_ins.rt( 'Transfer Detail Report',201,'BEGIN

 IF :Vendor List is NOT NULL THEN
     XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:Vendor List,''Supplier'');
 END IF;

 IF :Parts List is NOT NULL THEN
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:Parts List,''Item'');  
 END IF;

 IF :Ship to Location List is NOT NULL THEN
     XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:Ship to Location List,''Org'');
 END IF;

 IF :Ship From Location List is NOT NULL THEN
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:Ship From Location List,''Org'');
 END IF;

 IF :CatClass List is NOT NULL THEN
   XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(:SYSTEM.PROCESS_ID,:CatClass List,''Cat Class'');  
 END IF;

END;','B','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rt( 'Transfer Detail Report',201,'begin
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(:SYSTEM.PROCESS_ID);
end;','A','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Transfer Detail Report
--Inserting Report Portals - Transfer Detail Report
--Inserting Report Dashboards - Transfer Detail Report
--Inserting Report Security - Transfer Detail Report
xxeis.eis_rs_ins.rsec( 'Transfer Detail Report','201','','50983',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Transfer Detail Report','201','','50621',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Transfer Detail Report','201','','50893',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Transfer Detail Report','201','','51369',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Transfer Detail Report','201','','50910',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Transfer Detail Report','201','','50892',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Transfer Detail Report','201','','50921',201,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Transfer Detail Report','201','','51489',201,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Transfer Detail Report
END;
/
set scan on define on
