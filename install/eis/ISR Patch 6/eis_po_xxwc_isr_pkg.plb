create or replace
PACKAGE BODY eis_po_xxwc_isr_pkg AS

PROCEDURE write_log (  p_text IN VARCHAR2
                     )
Is
BEGIN
     IF nvl(G_log_mode ,'FND')='FND' THEN
        fnd_file.put_line(fnd_file.LOG,p_text);
     ELSE
         dbms_output.put_line(p_text);
     END IF;    
END;

procedure get_vendor_info (p_inventory_item_id in number, p_organization_id in number, p_vendor_name out varchar2, p_vendor_number out varchar2, p_vendor_site out varchar2)
is
 l_vendor_name   varchar2(240) := NULL;
 l_vendor_number varchar2(240) := NULL;
 l_vendor_site   varchar2(240) := NULL;
 l_vendor_id     number;
BEGIN

   
   Begin
    select pov.vendor_name, pov.segment1, pvs.vendor_site_code
     into l_vendor_name, l_vendor_number, l_vendor_site
     from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov,
         po_vendor_sites_all pvs,
         mtl_system_items_b msi
    where 1=1
    AND msi.inventory_item_id = ass.inventory_item_id
    AND msi.organization_id   = ass.organization_id
    and ass.inventory_item_id = p_inventory_item_id
    and ass.organization_id   = p_organization_id
    and msi.source_type       = 2
    and rco.sourcing_rule_id  = ass.sourcing_rule_id
    and sso.sr_receipt_id     = rco.sr_receipt_id
    AND SSO.SOURCE_TYPE       = 3 
    and pov.vendor_id         = sso.vendor_id
    and pvs.vendor_site_id    = sso.vendor_site_id;
    
    p_vendor_name := l_vendor_name;
    p_vendor_number := l_vendor_number;
    p_vendor_site  := l_vendor_site;
    
   EXCEPTION 
     WHEN OTHERS THEN
       l_vendor_name   := NULL;
       p_vendor_name   := NULL;
       p_vendor_number := NULL;
       p_vendor_site   := NULL;
   END;
   
   IF l_vendor_name IS NULL THEN
     select vendor_id 
       into l_vendor_id
     from (
           select count(sso.vendor_id) cnt, sso.vendor_id     
            from mrp_sr_assignments  ass,
                 mrp_sr_receipt_org rco,
                 mrp_sr_source_org sso,
                 po_vendors pov,                 
                 mtl_system_items_b msi
            where 1=1
            AND msi.inventory_item_id = ass.inventory_item_id
            AND msi.organization_id   = ass.organization_id
            and ass.inventory_item_id = p_inventory_item_id
            --and ass.organization_id   = p_organization_id
            and msi.source_type       = 2
            and rco.sourcing_rule_id  = ass.sourcing_rule_id
            and sso.sr_receipt_id     = rco.sr_receipt_id
            AND SSO.SOURCE_TYPE       = 3 
            and pov.vendor_id         = sso.vendor_id        
            group by sso.vendor_id
            order by 1 desc
          )a where rownum <=1  ;
      
     select segment1, vendor_name
       into l_vendor_number, l_vendor_name
      from po_vendors
     where vendor_id = l_vendor_id;
      
      
   END IF;
  
    p_vendor_name := l_vendor_name;
    p_vendor_number := l_vendor_number;
    p_vendor_site  := l_vendor_site;

 exception
 WHEN OTHERS THEN
       p_vendor_name   := NULL;
       p_vendor_number := NULL;
       p_vendor_site   := NULL;
END;

PROCEDURE MAIN IS

CURSOR c_item_cur 
                is (
                    SELECT * 
                    from xxeis.eis_xxwc_po_isr_tab_v
                    );

CURSOR c_item_cur1 
                is (
                    SELECT * 
                    from xxeis.eis_xxwc_po_isr_mst_tab_v
                    );                    
type period_tab is table of varchar2(200)  index by binary_integer; 
period_name_tab                period_tab;
l_yes_no                       VARCHAR2(10);
l_current_period_year          VARCHAR2(20);
l_current_effective_period_num VARCHAR2(240);
l_period_name                  varchar2(100);
l_store_period_column_name     varchar2(240);
l_other_period_column_name     varchar2(240);
l_update_str                   VARCHAR2(10000);    
lv_program_location            varchar2(4000);   
l_period_count                 NUMBER :=0;
l_item_cost                    NUMBER;
l_avail_d                      NUMBER;
l_on_ord                       NUMBER;
l_avail2                       NUMBER;
L_REF_CURSOR2                  CURSOR_TYPE2;
l_vendor_name                  VARCHAR2(240);
l_vendor_number                VARCHAR2(50); 
l_vendor_site                  VARCHAR2(100); 
l_organization_id              NUMBER;
l_count                        NUMBER;
l_main_sql                     VARCHAR2(32000);



BEGIN
  lv_program_location := '1, Program Start ';
  write_log('Truncate Start '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
  EXECUTE IMMEDIATE 'truncate table xxeis.eis_xxwc_po_isr_tab';
  l_main_sql := ' SELECT * 
                    from xxeis.eis_xxwc_po_isr_tab_v ';
  write_log('Bulk collect Main query start '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );   
  OPEN l_ref_cursor2 FOR l_main_sql;
  loop
     FETCH L_REF_CURSOR2 BULK COLLECT INTO G_VIEW_RECORD2 LIMIT 10000;
     l_count := 0;
     lv_program_location := '3, After Main query Fetch';
     FOR j in 1..G_VIEW_RECORD2.count 
     Loop
       lv_program_location := '4, Populate Main query into table record ';
       l_item_cost     := nvl(G_VIEW_RECORD2(j).bpa_cost,G_VIEW_RECORD2(j).item_cost);
       l_avail_d       := G_VIEW_RECORD2(j).avail *  G_VIEW_RECORD2(j).aver_cost;
       l_on_ord        := G_VIEW_RECORD2(j).supply - nvl(G_VIEW_RECORD2(j).open_req,0);
       l_avail2        := G_VIEW_RECORD2(j).qoh - G_VIEW_RECORD2(j).demand;
       l_vendor_name   := G_VIEW_RECORD2(j).vendor_name;
       l_vendor_number := G_VIEW_RECORD2(j).vendor_number;
       l_vendor_site   := G_VIEW_RECORD2(j).vendor_site;
       
       IF G_VIEW_RECORD2(j).st ='I' or G_VIEW_RECORD2(j).vendor_number is null THEN 
          l_organization_id := CASE WHEN G_VIEW_RECORD2(j).st = 'I' THEN
                                   G_VIEW_RECORD2(j).source_organization_id
                               ELSE G_VIEW_RECORD2(j).organization_id    
                               END; 
          get_vendor_info(G_VIEW_RECORD2(j).inventory_item_id, l_organization_id, l_vendor_name,l_vendor_number,l_vendor_site); 
       END IF;
       
       G_VIEW_TAB2(j).org                    :=  G_VIEW_RECORD2(j).org; 
       G_VIEW_TAB2(j).pre                    :=  G_VIEW_RECORD2(j).pre; 
       G_VIEW_TAB2(j).item_number            :=  G_VIEW_RECORD2(j).item_number;
       G_VIEW_TAB2(j).vendor_num             :=  l_vendor_number;
       G_VIEW_TAB2(j).vendor_name            :=  l_vendor_Name;
       G_VIEW_TAB2(j).source                 :=  G_VIEW_RECORD2(j).source;
       G_VIEW_TAB2(j).st                     :=  G_VIEW_RECORD2(j).st;
       G_VIEW_TAB2(j).description            :=  G_VIEW_RECORD2(j).description;
       G_VIEW_TAB2(j).cat                    :=  G_VIEW_RECORD2(j).cat_Class;
       G_VIEW_TAB2(j).pplt                   :=  G_VIEW_RECORD2(j).pplt;
       G_VIEW_TAB2(j).plt                    :=  G_VIEW_RECORD2(j).plt;
       G_VIEW_TAB2(j).uom                    :=  G_VIEW_RECORD2(j).uom;
       G_VIEW_TAB2(j).cl                     :=  G_VIEW_RECORD2(j).cl;
       G_VIEW_TAB2(j).stk_flag               :=  G_VIEW_RECORD2(j).stk;
       G_VIEW_TAB2(j).pm                     :=  G_VIEW_RECORD2(j).pm;
       G_VIEW_TAB2(j).minn                   :=  G_VIEW_RECORD2(j).min;
       G_VIEW_TAB2(j).maxn                   :=  G_VIEW_RECORD2(j).max;
       G_VIEW_TAB2(j).amu                    :=  G_VIEW_RECORD2(j).amu;
       G_VIEW_TAB2(j).mf_flag                :=  G_VIEW_RECORD2(j).mf_flag;
       G_VIEW_TAB2(j).hit6_store_Sales       :=  NULL;
       G_VIEW_TAB2(j).hit6_other_inv_sales   :=  NULL;
       G_VIEW_TAB2(j).aver_cost              :=  G_VIEW_RECORD2(j).aver_cost;
       G_VIEW_TAB2(j).item_cost              :=  l_item_cost;
       G_VIEW_TAB2(j).bpa_cost               :=  G_VIEW_RECORD2(j).bpa_cost;
       G_VIEW_TAB2(j).bpa                    :=  G_VIEW_RECORD2(j).bpa;
       G_VIEW_TAB2(j).QOH                    :=  G_VIEW_RECORD2(j).QOH;
       G_VIEW_TAB2(j).on_ord                 :=  l_on_ord;
       G_VIEW_TAB2(j).available              :=  G_VIEW_RECORD2(j).avail;
       G_VIEW_TAB2(j).availabledollar        :=  l_avail_d;
       G_VIEW_TAB2(j).one_store_sale         :=  NULL;
       G_VIEW_TAB2(j).six_store_sale         :=  NULL;
       G_VIEW_TAB2(j).twelve_store_Sale      :=  NULL;
       G_VIEW_TAB2(j).one_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).six_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).twelve_other_inv_Sale  :=  NULL;
       G_VIEW_TAB2(j).bin_loc                :=  G_VIEW_RECORD2(j).bin_loc;
       G_VIEW_TAB2(j).mc                     :=  G_VIEW_RECORD2(j).mc;
       G_VIEW_TAB2(j).fi_flag                :=  G_VIEW_RECORD2(j).fi;
       G_VIEW_TAB2(j).freeze_date            :=  G_VIEW_RECORD2(j).freeze_date;
       G_VIEW_TAB2(j).res                    :=  G_VIEW_RECORD2(j).res;
       G_VIEW_TAB2(j).thirteen_wk_avg_inv    :=  G_VIEW_RECORD2(j).thirteen_wk_avg_inv;
       G_VIEW_TAB2(j).thirteen_wk_an_cogs    :=  G_VIEW_RECORD2(j).thirteen_wk_an_cogs;
       G_VIEW_TAB2(j).turns                  :=  G_VIEW_RECORD2(j).turns;
       G_VIEW_TAB2(j).buyer                  :=  G_VIEW_RECORD2(j).buyer;
       G_VIEW_TAB2(j).ts                     :=  G_VIEW_RECORD2(j).ts;
       G_VIEW_TAB2(j).jan_store_sale         :=  NULL;
       G_VIEW_TAB2(j).feb_store_sale         :=  NULL;
       G_VIEW_TAB2(j).mar_store_sale         :=  NULL;
       G_VIEW_TAB2(j).apr_store_sale         :=  NULL;
       G_VIEW_TAB2(j).may_store_sale         :=  NULL;
       G_VIEW_TAB2(j).jun_store_sale         :=  NULL;
       G_VIEW_TAB2(j).jul_store_sale         :=  NULL;
       G_VIEW_TAB2(j).aug_store_sale         :=  NULL;
       G_VIEW_TAB2(j).sep_store_sale         :=  NULL;
       G_VIEW_TAB2(j).oct_store_sale         :=  NULL;
       G_VIEW_TAB2(j).nov_store_sale         :=  NULL;
       G_VIEW_TAB2(j).dec_store_sale         :=  NULL;
       G_VIEW_TAB2(j).jan_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).feb_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).mar_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).apr_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).may_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).jun_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).jul_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).aug_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).sep_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).oct_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).nov_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).dec_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).hit4_store_Sales       :=  NULL;
       G_VIEW_TAB2(j).hit4_other_inv_Sales   :=  NULL;
       G_VIEW_TAB2(j).so                     :=  G_VIEW_RECORD2(j).so;
       G_VIEW_TAB2(j).inventory_item_id      :=  G_VIEW_RECORD2(j).inventory_item_id;
       G_VIEW_TAB2(j).organization_id        :=  G_VIEW_RECORD2(j).organization_id;
       G_VIEW_TAB2(j).set_of_books_id        :=  G_VIEW_RECORD2(j).set_of_books_id;
       G_VIEW_TAB2(j).org_name               :=  G_VIEW_RECORD2(j).organization_name;
       G_VIEW_TAB2(j).district               :=  G_VIEW_RECORD2(j).district;
       G_VIEW_TAB2(j).region                 :=  G_VIEW_RECORD2(j).region;
       G_VIEW_TAB2(j).inv_Cat_Seg1           :=  G_VIEW_RECORD2(j).inv_cat_seg1;
       G_VIEW_TAB2(j).wt                     :=  G_VIEW_RECORD2(j).wt;
       G_VIEW_TAB2(j).ss                     :=  G_VIEW_RECORD2(j).ss;
       G_VIEW_TAB2(j).fml                    :=  G_VIEW_RECORD2(j).fml;
       G_VIEW_TAB2(j).open_req               :=  G_VIEW_RECORD2(j).open_req;
       G_VIEW_TAB2(j).sourcing_rule          :=  G_VIEW_RECORD2(j).sourcing_rule;
       G_VIEW_TAB2(j).clt                    :=  G_VIEW_RECORD2(j).clt;
       G_VIEW_TAB2(j).avail2                 :=  l_avail2;
       G_VIEW_TAB2(j).int_req                :=  G_VIEW_RECORD2(j).int_req;
       G_VIEW_TAB2(j).dir_req                :=  G_VIEW_RECORD2(j).dir_req;
       G_VIEW_TAB2(j).demand                 :=  G_VIEW_RECORD2(j).demand;
       G_VIEW_TAB2(j).ITEM_STATUS_CODE       :=  G_VIEW_RECORD2(j).ITEM_STATUS_CODE;
       G_VIEW_TAB2(j).SITE_VENDOR_NUM        :=  G_VIEW_RECORD2(j).vendor_number;
       G_VIEW_TAB2(j).vendor_site            :=  G_VIEW_RECORD2(j).vendor_site;       
    
     l_count := l_count + 1;
     END loop;
     IF l_count >= 1  THEN
        lv_program_location := '5, Insert into eis_xxwc_po_isr_tab ';
        forall j in 1 .. G_VIEW_TAB2.count 
            INSERT INTO xxeis.eis_xxwc_po_isr_tab
                 values g_view_tab2(j);	
        COMMIT;  
     end if;  
     
     lv_program_location := '6, Delete tab ';	 
     G_VIEW_TAB2.delete;
     G_VIEW_RECORD2.delete;
     IF l_ref_cursor2%NOTFOUND Then
        CLOSE l_ref_cursor2;
        EXIT;
     End if;
      
      
  END LOOP;
  write_log(' Bulk Collect Main query End '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );

fnd_file.put_line(fnd_file.log,' Before insert commit'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
      COMMIT;
  fnd_file.put_line(fnd_file.log,'After Insert Commit'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
  FOR c_distinct_sob_id IN(select 2061 set_of_books_id from dual)
  loop
             
         period_name_tab.DELETE;
         --get current period and year
         lv_program_location :='Getting current period and year';         
         SELECT period_name,
                 period_year,
                 effective_period_num 
         INTO   l_period_name,
                 l_current_period_year,
                 l_current_effective_period_num
         FROM gl_period_statuses 
         where application_id=101
         and trunc(sysdate) between start_date and end_date
         and adjustment_period_flag  <>'Y'
         and set_of_books_id         = c_distinct_sob_id.set_of_books_id;
         
       /*       
          -- Added to get the previous period
         BEGIN
                               SELECT period_name 
                                INTO period_name_tab(l_period_count)
                                FROM gl_period_statuses
                                WHERE application_id=101
                                AND period_num    = l_current_effective_period_num
                                AND period_year             = l_current_period_year
                                AND adjustment_period_flag  <>'Y'
                                AND set_of_books_id         = c_distinct_sob_id.set_of_books_id;
                     
                                 dbms_output.put_line('period_name_tab(l_period_count)='||period_name_tab(l_period_count));
                                 fnd_file.put_line(fnd_file.log,'period_name_tab(l_period_count)='||period_name_tab(l_period_count));
                                 fnd_file.put_line(fnd_file.log,'l_current_effective_period_num is:'||l_current_effective_period_num);
                                 fnd_file.put_line(fnd_file.log,'l_current_period_year is:'||l_current_period_year);
                                 lv_program_location :=' Next period and year is='||period_name_tab(l_period_count)||','||l_current_period_year;
                                 write_log(lv_program_location);
                        exception WHEN others THEN
                        l_period_count:=l_period_count-1;
                        fnd_file.put_line(fnd_file.log,'Inside Exception ='||l_period_count);
                        exit;
          END;
         */
         --period_name_tab(l_period_count):=l_period_name;
         lv_program_location :=' current period and year, effective_period_num is='||l_period_name||','||l_current_period_year ||','||l_current_effective_period_num;
         write_log(lv_program_location);

              --store next period names for existing year in array list
                  loop
                      l_period_count:=l_period_count+1;
                      l_current_effective_period_num:=l_current_effective_period_num-1;
                       BEGIN
                             
                       SELECT period_name 
                                INTO period_name_tab(l_period_count)
                                FROM gl_period_statuses
                                WHERE application_id=101
                                AND effective_period_num    = l_current_effective_period_num
                                AND period_year             = l_current_period_year
                                AND adjustment_period_flag  <>'Y'
                                AND set_of_books_id         = c_distinct_sob_id.set_of_books_id;
                                 dbms_output.put_line('period_name_tab(l_period_count)='||period_name_tab(l_period_count));
                                 fnd_file.put_line(fnd_file.log,'period_name_tab(l_period_count)='||period_name_tab(l_period_count));
                                 fnd_file.put_line(fnd_file.log,'l_current_effective_period_num is:'||l_current_effective_period_num);
                                 fnd_file.put_line(fnd_file.log,'l_current_period_year is:'||l_current_period_year);
                                 lv_program_location :=' Next period and year is='||period_name_tab(l_period_count)||','||l_current_period_year;
                                 write_log(lv_program_location);
                        exception WHEN others THEN
                        l_period_count:=l_period_count-1;
                        fnd_file.put_line(fnd_file.log,'Inside Exception ='||l_period_count);
                        exit;
                        END;
                    exit WHEN l_period_count=12;
                    END loop;
                    
                    
              dbms_output.put_line('l_period_count before change in year ='||l_period_count);  
              --when there is change in year then store next period names for prevous year in array list
              FOR c_period_name IN (SELECT period_name 
                                    FROM (
                                           SELECT period_name
                                           FROM   gl_period_statuses
                                            WHERE application_id          = 101
                                            AND   period_year             = (l_current_period_year-1)
                                            AND   set_of_books_id         = c_distinct_sob_id.set_of_books_id
                                            AND   adjustment_period_flag  <>'Y'
                                            ORDER BY effective_period_num DESC
                                           )
                                      WHERE ROWNUM<=(12-l_period_count)
                                      )
              loop
                  dbms_output.put_line('l_period_count='||l_period_count);  
                  l_period_count:=l_period_count+1;
                  period_name_tab(l_period_count):=c_period_name.period_name;
                  lv_program_location :=' Next period and year is='||period_name_tab(l_period_count)||','||(l_current_period_year-1);
                  write_log(lv_program_location);
             END loop;
      
  END loop;
  
 fnd_file.put_line(fnd_file.log,'After Period tab'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') ); 
 
  FOR period_name_cnt IN 1..12
  loop
     lv_program_location :=' getting update period name for ='||period_name_tab(period_name_cnt);
     write_log(lv_program_location);
    IF upper(period_name_tab(period_name_cnt)) LIKE  '%JAN%' THEN
       l_store_period_column_name:='jan_store_sale';
       l_other_period_column_name :='jan_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%FEB%' THEN
       l_store_period_column_name:='feb_store_sale';
       l_other_period_column_name :='feb_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%MAR%' THEN
       l_store_period_column_name:='mar_store_sale';
       l_other_period_column_name :='mar_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%APR%' THEN
       l_store_period_column_name:='apr_store_sale';
       l_other_period_column_name :='apr_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%MAY%' THEN
       l_store_period_column_name:='may_store_sale';
       l_other_period_column_name :='may_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%JUN%' THEN
       l_store_period_column_name:='jun_store_sale';
       l_other_period_column_name :='jun_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%JUL%' THEN
       l_store_period_column_name:='jul_store_sale';
       l_other_period_column_name :='jul_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%AUG%' THEN
       l_store_period_column_name:='aug_store_sale';
       l_other_period_column_name :='aug_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%SEP%' THEN
       l_store_period_column_name:='sep_store_sale';
       l_other_period_column_name :='sep_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%OCT%' THEN
       l_store_period_column_name:='oct_store_sale';
       l_other_period_column_name :='oct_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%NOV%' THEN
       l_store_period_column_name:='nov_store_sale';
       l_other_period_column_name :='nov_other_inv_sale';
    elsif upper(period_name_tab(period_name_cnt)) LIKE  '%DEC%' THEN
       l_store_period_column_name:='dec_store_sale';
       l_other_period_column_name :='dec_other_inv_sale';
   END IF;
     
   l_update_str:=
                'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set '||l_store_period_column_name||' =(select sum(mdh.sales_order_demand + nvl(std_wip_usage,0))
                from   mtl_demand_histories mdh,
                       gl_period_statuses    gps,
                       org_organization_definitions   ood 
                where 1=1
                and mdh.inventory_item_id      = tab.inventory_item_id
                and mdh.organization_id        = tab.organization_id
                and gps.application_id         = 101
                and trunc(mdh.period_start_date) between gps.start_date and gps.end_date
                and ood.set_of_books_id=gps.set_of_books_id
                and ood.organization_id=mdh.organization_id 
                and gps.period_name = '''||period_name_tab(period_name_cnt)||''' )
                 ';

     lv_program_location :=' update sql query ='||l_update_str;
     write_log(lv_program_location);  
     BEGIN
        EXECUTE IMMEDIATE l_update_str; COMMIT;
     exception WHEN others THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
          
      ---DC mode calcualations     
     l_update_str:=
      'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set '||l_other_period_column_name||' =(select sum(mdh.sales_order_demand + NVL(std_wip_usage,0))
      from   mtl_demand_histories mdh,
             gl_period_statuses    gps,
             org_organization_definitions   ood,
             mtl_system_items_kfv msi
      where 1=1
      and msi.inventory_item_id       =tab.inventory_item_id
      and msi.source_organization_id     = tab.organization_id
      AND msi.source_organization_id <> msi.organization_id
      and gps.application_id         = 101
      and trunc(period_start_date) between gps.start_date and gps.end_date
      and ood.set_of_books_id=gps.set_of_books_id
      and ood.organization_id=mdh.organization_id 
      and msi.organization_id= mdh.organization_id
      and   msi.inventory_item_id=mdh.inventory_item_id
      and gps.period_name = '''||period_name_tab(period_name_cnt)||''' ) 
       ';
      lv_program_location :=' update sql query ='||l_update_str;
      write_log(lv_program_location);
       
     BEGIN
     fnd_file.put_line(fnd_file.log,'Before update of period '||period_name_tab(period_name_cnt)||' ' ||to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
        EXECUTE IMMEDIATE l_update_str;COMMIT;
    fnd_file.put_line(fnd_file.log,'After update of period '||period_name_tab(period_name_cnt)||' '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );    
     exception WHEN others THEN
      lv_program_location :=' Exception in updating the other sales column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;

  END loop;
  fnd_file.put_line(fnd_file.log,'After 12 buckts population'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') ); 
  
  BEGIN
       l_update_str:= 'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set HIT4_STORE_SALES  =(select count(distinct oeh.header_id)
      from   oe_order_headers_all      oeh,
             oe_order_lines_all        oel,
             oe_transaction_types_tl ott
      where 1=1
      and oel.inventory_item_id       = tab.inventory_item_id
      and oel.ship_from_org_id        = tab.organization_id
      and oel.header_id               = oeh.header_id
      AND oeh.order_number             is not null
      AND oel.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = oel.Ship_to_Org_ID )
      AND ((ott.name != ''COUNTER LINE'' and Trunc(oel.Actual_Shipment_Date) >  (Trunc(sysdate-(4*4*7))))            
            OR 
          (ott.name = ''COUNTER LINE'' and Trunc(oel.fulfillment_Date) >  (Trunc(sysdate-(4*4*7)))))                                     
      and  oel.Flow_Status_Code       = ''CLOSED''
      and oel.line_category_code      <> ''RETURN''
      AND  oel.invoice_interface_status_code = ''YES''            
      and not exists (select 1 from dual where (DECODE(oel.line_category_code,''RETURN'',(oel.ordered_quantity*-1),oel.ordered_quantity)                                               
                                                   
                                               ) = 
                                               (select -1* sum(DECODE(olr.line_category_code,''RETURN'',(olr.ordered_quantity*-1),olr.ordered_quantity))
                                                  from oe_order_lines_all olr                                                      
                                                 where olr.reference_header_id = oeh.header_id 
                                                   AND olr.reference_line_id   = oel.line_id                                                   
                                                   AND olr.line_category_code  = ''RETURN''
                                                   and olr.return_context      = ''ORDER''                                                
                                               )
                      )
       ) ';
      
     dbms_output.put_line('L_UPDATE_STR='||l_update_str);
     fnd_file.put_line(fnd_file.LOG,'L_UPDATE_STR='||l_update_str);
     lv_program_location :=' update sql query ='||l_update_str;
     
      EXECUTE IMMEDIATE l_update_str;    COMMIT;
      fnd_file.put_line(fnd_file.log,'HIT 4 update of period '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );    
   exception WHEN others THEN
    lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
    write_log(lv_program_location);
     END;
     
   ---DC mode calcualations
     BEGIN
       l_update_str:= 'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set HIT4_OTHER_INV_SALES  =(select count(distinct oeh.header_id)
      from   oe_order_headers_all      oeh,
             oe_order_lines_all        oel,
             oe_transaction_types_tl ott,
             mtl_system_items_kfv msi
      where 1=1
      and  oel.header_id             = oeh.header_id
      and oel.Flow_Status_Code      = ''CLOSED''
      and oel.line_category_code      <> ''RETURN''
      AND oel.invoice_interface_status_code = ''YES''
      and tab.inventory_item_id      = msi.inventory_item_id
      and tab.organization_id        = msi.source_organization_id      
      AND oeh.order_number             is not null
      AND oel.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = oel.Ship_to_Org_ID )
      AND ((ott.name != ''COUNTER LINE'' and Trunc(oel.Actual_Shipment_Date) >  (Trunc(sysdate-(4*4*7))))            
            OR 
          (ott.name = ''COUNTER LINE'' and Trunc(oel.fulfillment_Date) >  (Trunc(sysdate-(4*4*7)))))                                     
      and not exists (select 1 from dual where (DECODE(oel.line_category_code,''RETURN'',(oel.ordered_quantity*-1),oel.ordered_quantity)                                               
                                                   
                                               ) = 
                                               (select -1* sum(DECODE(olr.line_category_code,''RETURN'',(olr.ordered_quantity*-1),olr.ordered_quantity))
                                                  from oe_order_lines_all olr                                                      
                                                 where olr.reference_header_id = oeh.header_id 
                                                   AND olr.reference_line_id   = oel.line_id                                                   
                                                   AND olr.line_category_code  = ''RETURN''
                                                   and olr.return_context      = ''ORDER''                                                
                                               )
                      )
      and msi.organization_id        = oel.ship_from_org_id
      and msi.inventory_item_id      = oel.inventory_item_id      
      ) ';     
     lv_program_location :=' update sql query ='||l_update_str;
     write_log(lv_program_location);
        EXECUTE IMMEDIATE l_update_str;    COMMIT;
        fnd_file.put_line(fnd_file.log,'HIT others 4 update  '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );    
   exception WHEN others THEN
    lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
    write_log(lv_program_location);   
     END;
     

       
          begin
       l_update_str:= 'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set HIT6_STORE_SALES  =(select count(distinct oeh.header_id)
      from   oe_order_headers_all      oeh,
             oe_order_lines_all        oel,
             oe_transaction_types_tl ott
      where 1=1
      and oel.header_id              = oeh.header_id
      and  oel.Flow_Status_Code      = ''CLOSED''
      and  oel.line_category_code      <> ''RETURN''
      AND  oel.invoice_interface_status_code = ''YES''
      and oel.inventory_item_id      = tab.inventory_item_id
      and oel.ship_from_org_id       = tab.organization_id
      AND oeh.order_number             is not null
      AND oel.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = oel.Ship_to_Org_ID )                            
      AND ((ott.name != ''COUNTER LINE'' and Trunc(oel.Actual_Shipment_Date) >  (Trunc(sysdate-(6*4*7))))            
            OR 
          (ott.name = ''COUNTER LINE'' and Trunc(oel.fulfillment_Date) >  (Trunc(sysdate-(6*4*7)))))                                     
      and not exists (select 1 from dual where (DECODE(oel.line_category_code,''RETURN'',(oel.ordered_quantity*-1),oel.ordered_quantity)                                               
                                                   
                                               ) = 
                                               (select -1* sum(DECODE(olr.line_category_code,''RETURN'',(olr.ordered_quantity*-1),olr.ordered_quantity))
                                                  from oe_order_lines_all olr                                                      
                                                 where olr.reference_header_id = oeh.header_id 
                                                   AND olr.reference_line_id   = oel.line_id                                                   
                                                   AND olr.line_category_code  = ''RETURN''
                                                   and olr.return_context      = ''ORDER''                                                
                                               )
                      )
        )  ';     
     lv_program_location :=' update sql query ='||l_update_str;
     write_log(lv_program_location);
        EXECUTE IMMEDIATE l_update_str;    COMMIT;
     fnd_file.put_line(fnd_file.log,'HIT 6 update  '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );       
   exception WHEN others THEN
   lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
   write_log(lv_program_location);
     END;
     
         ---DC mode calcualations  
         
       BEGIN
       l_update_str:= 'update xxeis.Eis_Xxwc_Po_Isr_Tab tab set HIT6_OTHER_INV_SALES  =(select count(distinct oeh.header_id)
      from   oe_order_headers_all      oeh,
             oe_order_lines_all        oel,
             oe_transaction_types_tl ott,
             mtl_system_items_kfv msi
      where 1=1
      and  oel.header_id              = oeh.header_id
      and  oel.Flow_Status_Code       = ''CLOSED''
      and oel.line_category_code      <> ''RETURN''
      AND  oel.invoice_interface_status_code = ''YES''
      and tab.inventory_item_id       = msi.inventory_item_id
      and tab.organization_id         = msi.source_organization_id
      AND oeh.order_number             is not null
      AND oel.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = oel.Ship_to_Org_ID )
      AND ((ott.name != ''COUNTER LINE'' and Trunc(oel.Actual_Shipment_Date) >  (Trunc(sysdate-(6*4*7))))            
            OR 
          (ott.name = ''COUNTER LINE'' and Trunc(oel.fulfillment_Date) >  (Trunc(sysdate-(6*4*7)))))                                     
      and not exists (select 1 from dual where (DECODE(oel.line_category_code,''RETURN'',(oel.ordered_quantity*-1),oel.ordered_quantity)                                               
                                                   
                                               ) = 
                                               (select -1* sum(DECODE(olr.line_category_code,''RETURN'',(olr.ordered_quantity*-1),olr.ordered_quantity))
                                                  from oe_order_lines_all olr                                                      
                                                 where olr.reference_header_id = oeh.header_id 
                                                   AND olr.reference_line_id   = oel.line_id                                                   
                                                   AND olr.line_category_code  = ''RETURN''
                                                   and olr.return_context      = ''ORDER''                                                
                                               )
                      )
      and msi.organization_id        = oel.ship_from_org_id
      and msi.inventory_item_id      = oel.inventory_item_id
     )  ';
 
     lv_program_location :=' update sql query ='||l_update_str;
     write_log(lv_program_location);
        EXECUTE IMMEDIATE l_update_str;    COMMIT;
     fnd_file.put_line(fnd_file.log,'HIT 6 others update  '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );          
   exception WHEN others THEN
   lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
   write_log(lv_program_location);
     END;
fnd_file.put_line(fnd_file.log,'After HITS buckts population'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );      
  BEGIN
      UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET one_store_sale =(SELECT sum(DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))
      FROM   oe_order_lines_all ol,
             OE_ORDER_HEADERS_ALL OH,
              oe_transaction_types_tl ott
      where oh.header_id          = ol.header_id
      and inventory_item_id       = tab.inventory_item_id
      and ol.ship_from_org_id     = tab.organization_id
      and  ol.flow_status_code    = 'CLOSED'
      and ol.line_category_code   <> 'RETURN'
      and ol.invoice_interface_status_code = 'YES' 
      AND oh.order_number             is not null
      AND ol.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ol.Ship_to_Org_ID )
      AND ((ott.name != 'COUNTER LINE' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(1*4*7))))            
            OR 
          (ott.name = 'COUNTER LINE' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(1*4*7)))))
      )  ;                                  
   
fnd_file.put_line(fnd_file.log,' one store others update  '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );  
  commit;
      exception WHEN others THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
     
       ---DC mode calcualations  
         BEGIN
      UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET one_other_inv_sale =(SELECT sum(DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))
      FROM   oe_order_lines_all ol,
             oe_order_headers_all oh,
             oe_transaction_types_tl ott,
             mtl_system_items_kfv msi
      where oh.header_id              = ol.header_id
      and msi.inventory_item_id       = tab.inventory_item_id
      and msi.source_organization_id  = tab.organization_id
      and msi.organization_id         = ol.ship_from_org_id
      and msi.inventory_item_id       = ol.inventory_item_id
      and ol.flow_status_code       = 'CLOSED'
      and ol.invoice_interface_status_code = 'YES' 
      AND oh.order_number             is not null
      AND ol.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ol.Ship_to_Org_ID )
      AND ((ott.name != 'COUNTER LINE' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(30))))            
            OR 
          (ott.name = 'COUNTER LINE' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(30)))))
      )  ;                                  

      fnd_file.put_line(fnd_file.log,' one store others update  '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );   
      commit;
      exception WHEN others THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
     
     
       BEGIN
      UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET six_store_sale =(SELECT sum(DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))
      from   oe_order_lines_all ol,
             oe_order_headers_all oh,
             oe_transaction_types_tl ott
      where oh.header_id          = ol.header_id
      and inventory_item_id       = tab.inventory_item_id
      and ol.ship_from_org_id     = tab.organization_id
      and  ol.flow_status_code   = 'CLOSED'
      and ol.invoice_interface_status_code = 'YES' 
      AND oh.order_number             is not null
      AND ol.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ol.Ship_to_Org_ID )
      AND ((ott.name != 'COUNTER LINE' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(182))))            
            OR 
          (ott.name = 'COUNTER LINE' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(182)))))
      ) ;
      fnd_file.put_line(fnd_file.log,' six store  update  '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );   
      commit;
      exception WHEN others THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
  
  
     ---DC mode calcualations 
     
      BEGIN
          UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET six_other_inv_sale =(SELECT sum(DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))
      FROM   oe_order_lines_all ol,
             oe_order_headers_all oh,
             oe_transaction_types_tl ott,
             mtl_system_items_kfv msi
      where oh.header_id              = ol.header_id
      and msi.inventory_item_id       = tab.inventory_item_id
      and msi.source_organization_id  = tab.organization_id
      and msi.organization_id         = ol.ship_from_org_id
      and msi.inventory_item_id       = ol.inventory_item_id
      and ol.flow_status_code       = 'CLOSED'
      and ol.invoice_interface_status_code = 'YES' 
      AND oh.order_number             is not null
      AND ol.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ol.Ship_to_Org_ID )
      AND ((ott.name != 'COUNTER LINE' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(182))))            
            OR 
          (ott.name = 'COUNTER LINE' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(182)))))
      ) ;
      fnd_file.put_line(fnd_file.log,' six store others  update  '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );   
      commit;      

      EXCEPTION WHEN OTHERS THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;
  
      BEGIN
          UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET twelve_store_sale =(SELECT sum(DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))
      from   oe_order_lines_all ol,
             oe_order_headers_all oh,
             oe_transaction_types_tl ott
      where oh.header_id          = ol.header_id
      and inventory_item_id       = tab.inventory_item_id
      and ol.ship_from_org_id     = tab.organization_id
      and  ol.flow_status_code   = 'CLOSED'
      and ol.invoice_interface_status_code = 'YES' 
      AND oh.order_number             is not null
      AND ol.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ol.Ship_to_Org_ID )
      AND ((ott.name != 'COUNTER LINE' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(365))))            
            OR 
          (ott.name = 'COUNTER LINE' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(365)))))
      );
            fnd_file.put_line(fnd_file.log,' 12 store   update  '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );   
      commit;      

      
      EXCEPTION when OTHERS then
       lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
       write_log(lv_program_location);
     END;
     ---DC mode calcualations 
   
   
      BEGIN
          UPDATE xxeis.Eis_Xxwc_Po_Isr_Tab tab SET twelve_other_inv_sale =(SELECT sum(DECODE(ol.line_category_code,'RETURN',(ol.ordered_quantity*-1),ol.ordered_quantity))
      FROM   oe_order_lines_all ol,
             oe_order_headers_all oh,
             oe_transaction_types_tl ott,
             mtl_system_items_kfv msi
      where oh.header_id              = ol.header_id
      and msi.inventory_item_id       = tab.inventory_item_id
      and msi.source_organization_id  = tab.organization_id
      and msi.organization_id         = ol.ship_from_org_id
      and msi.inventory_item_id       = ol.inventory_item_id
      and ol.flow_status_code       = 'CLOSED'
      and ol.invoice_interface_status_code = 'YES' 
      AND oh.order_number             is not null
      AND ol.line_type_id             = ott.transaction_type_id
      And Not Exists ( Select 1
                            From Mtl_Parameters Mp
                            Where  mp.Organization_ID = ol.Ship_to_Org_ID )
      AND ((ott.name != 'COUNTER LINE' and Trunc(ol.Actual_Shipment_Date) >  (Trunc(sysdate-(365))))            
            OR 
          (ott.name = 'COUNTER LINE' and Trunc(ol.fulfillment_Date) >  (Trunc(sysdate-(365)))))
      ) ;
      fnd_file.put_line(fnd_file.log,' 12 store others  update  '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );   
      commit;      

      
      EXCEPTION WHEN OTHERS THEN
      lv_program_location :=' Exception in updating the column and exception is '||substr(sqlerrm,1,240);
      write_log(lv_program_location);
     END;

 write_log('Bulk collect Main query MST start '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
  
  lv_program_location := '25, Main query MST Start ';
  
    l_main_sql := ' SELECT * 
                    from xxeis.EIS_XXWC_PO_ISR_MST_TAB_V ';
  
  
  OPEN l_ref_cursor2 FOR l_main_sql;
  loop
     FETCH L_REF_CURSOR2 BULK COLLECT INTO G_VIEW_RECORD2 LIMIT 10000;
     l_count := 0;
     lv_program_location := '26, After Main query MST Fetch';
     FOR j in 1..G_VIEW_RECORD2.count 
     Loop
       lv_program_location := '27, Populate Main query into table record ';
       l_item_cost     := nvl(G_VIEW_RECORD2(j).bpa_cost,G_VIEW_RECORD2(j).item_cost);
       l_avail_d       := G_VIEW_RECORD2(j).avail *  G_VIEW_RECORD2(j).aver_cost;
       l_on_ord        := G_VIEW_RECORD2(j).supply - nvl(G_VIEW_RECORD2(j).open_req,0);
       l_avail2        := G_VIEW_RECORD2(j).qoh - G_VIEW_RECORD2(j).demand;
       l_vendor_name   := G_VIEW_RECORD2(j).vendor_name;
       l_vendor_number := G_VIEW_RECORD2(j).vendor_number;
       l_vendor_site   := G_VIEW_RECORD2(j).vendor_site;
       
       IF G_VIEW_RECORD2(j).st ='I' or G_VIEW_RECORD2(j).vendor_number is null THEN 
          l_organization_id := CASE WHEN G_VIEW_RECORD2(j).st = 'I' THEN
                                   G_VIEW_RECORD2(j).source_organization_id
                               ELSE G_VIEW_RECORD2(j).organization_id    
                               END; 
          get_vendor_info(G_VIEW_RECORD2(j).inventory_item_id, l_organization_id, l_vendor_name,l_vendor_number,l_vendor_site); 
       END IF;
       
       G_VIEW_TAB2(j).org                    :=  G_VIEW_RECORD2(j).org; 
       G_VIEW_TAB2(j).pre                    :=  G_VIEW_RECORD2(j).pre; 
       G_VIEW_TAB2(j).item_number            :=  G_VIEW_RECORD2(j).item_number;
       G_VIEW_TAB2(j).vendor_num             :=  l_vendor_number;
       G_VIEW_TAB2(j).vendor_name            :=  l_vendor_Name;
       G_VIEW_TAB2(j).source                 :=  G_VIEW_RECORD2(j).source;
       G_VIEW_TAB2(j).st                     :=  G_VIEW_RECORD2(j).st;
       G_VIEW_TAB2(j).description            :=  G_VIEW_RECORD2(j).description;
       G_VIEW_TAB2(j).cat                    :=  G_VIEW_RECORD2(j).cat_Class;
       G_VIEW_TAB2(j).pplt                   :=  G_VIEW_RECORD2(j).pplt;
       G_VIEW_TAB2(j).plt                    :=  G_VIEW_RECORD2(j).plt;
       G_VIEW_TAB2(j).uom                    :=  G_VIEW_RECORD2(j).uom;
       G_VIEW_TAB2(j).cl                     :=  G_VIEW_RECORD2(j).cl;
       G_VIEW_TAB2(j).stk_flag               :=  G_VIEW_RECORD2(j).stk;
       G_VIEW_TAB2(j).pm                     :=  G_VIEW_RECORD2(j).pm;
       G_VIEW_TAB2(j).minn                   :=  G_VIEW_RECORD2(j).min;
       G_VIEW_TAB2(j).maxn                   :=  G_VIEW_RECORD2(j).max;
       G_VIEW_TAB2(j).amu                    :=  G_VIEW_RECORD2(j).amu;
       G_VIEW_TAB2(j).mf_flag                :=  G_VIEW_RECORD2(j).mf_flag;
       G_VIEW_TAB2(j).hit6_store_Sales       :=  NULL;
       G_VIEW_TAB2(j).hit6_other_inv_sales   :=  NULL;
       G_VIEW_TAB2(j).aver_cost              :=  G_VIEW_RECORD2(j).aver_cost;
       G_VIEW_TAB2(j).item_cost              :=  l_item_cost;
       G_VIEW_TAB2(j).bpa_cost               :=  G_VIEW_RECORD2(j).bpa_cost;
       G_VIEW_TAB2(j).bpa                    :=  G_VIEW_RECORD2(j).bpa;
       G_VIEW_TAB2(j).QOH                    :=  G_VIEW_RECORD2(j).QOH;
       G_VIEW_TAB2(j).on_ord                 :=  l_on_ord;
       G_VIEW_TAB2(j).available              :=  G_VIEW_RECORD2(j).avail;
       G_VIEW_TAB2(j).availabledollar        :=  l_avail_d;
       G_VIEW_TAB2(j).one_store_sale         :=  NULL;
       G_VIEW_TAB2(j).six_store_sale         :=  NULL;
       G_VIEW_TAB2(j).twelve_store_Sale      :=  NULL;
       G_VIEW_TAB2(j).one_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).six_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).twelve_other_inv_Sale  :=  NULL;
       G_VIEW_TAB2(j).bin_loc                :=  G_VIEW_RECORD2(j).bin_loc;
       G_VIEW_TAB2(j).mc                     :=  G_VIEW_RECORD2(j).mc;
       G_VIEW_TAB2(j).fi_flag                :=  G_VIEW_RECORD2(j).fi;
       G_VIEW_TAB2(j).freeze_date            :=  G_VIEW_RECORD2(j).freeze_date;
       G_VIEW_TAB2(j).res                    :=  G_VIEW_RECORD2(j).res;
       G_VIEW_TAB2(j).thirteen_wk_avg_inv    :=  G_VIEW_RECORD2(j).thirteen_wk_avg_inv;
       G_VIEW_TAB2(j).thirteen_wk_an_cogs    :=  G_VIEW_RECORD2(j).thirteen_wk_an_cogs;
       G_VIEW_TAB2(j).turns                  :=  G_VIEW_RECORD2(j).turns;
       G_VIEW_TAB2(j).buyer                  :=  G_VIEW_RECORD2(j).buyer;
       G_VIEW_TAB2(j).ts                     :=  G_VIEW_RECORD2(j).ts;
       G_VIEW_TAB2(j).jan_store_sale         :=  NULL;
       G_VIEW_TAB2(j).feb_store_sale         :=  NULL;
       G_VIEW_TAB2(j).mar_store_sale         :=  NULL;
       G_VIEW_TAB2(j).apr_store_sale         :=  NULL;
       G_VIEW_TAB2(j).may_store_sale         :=  NULL;
       G_VIEW_TAB2(j).jun_store_sale         :=  NULL;
       G_VIEW_TAB2(j).jul_store_sale         :=  NULL;
       G_VIEW_TAB2(j).aug_store_sale         :=  NULL;
       G_VIEW_TAB2(j).sep_store_sale         :=  NULL;
       G_VIEW_TAB2(j).oct_store_sale         :=  NULL;
       G_VIEW_TAB2(j).nov_store_sale         :=  NULL;
       G_VIEW_TAB2(j).dec_store_sale         :=  NULL;
       G_VIEW_TAB2(j).jan_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).feb_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).mar_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).apr_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).may_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).jun_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).jul_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).aug_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).sep_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).oct_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).nov_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).dec_other_inv_sale     :=  NULL;
       G_VIEW_TAB2(j).hit4_store_Sales       :=  NULL;
       G_VIEW_TAB2(j).hit4_other_inv_Sales   :=  NULL;
       G_VIEW_TAB2(j).so                     :=  G_VIEW_RECORD2(j).so;
       G_VIEW_TAB2(j).inventory_item_id      :=  G_VIEW_RECORD2(j).inventory_item_id;
       G_VIEW_TAB2(j).organization_id        :=  G_VIEW_RECORD2(j).organization_id;
       G_VIEW_TAB2(j).set_of_books_id        :=  G_VIEW_RECORD2(j).set_of_books_id;
       G_VIEW_TAB2(j).org_name               :=  G_VIEW_RECORD2(j).organization_name;
       G_VIEW_TAB2(j).district               :=  G_VIEW_RECORD2(j).district;
       G_VIEW_TAB2(j).region                 :=  G_VIEW_RECORD2(j).region;
       G_VIEW_TAB2(j).inv_Cat_Seg1           :=  G_VIEW_RECORD2(j).inv_cat_seg1;
       G_VIEW_TAB2(j).wt                     :=  G_VIEW_RECORD2(j).wt;
       G_VIEW_TAB2(j).ss                     :=  G_VIEW_RECORD2(j).ss;
       G_VIEW_TAB2(j).fml                    :=  G_VIEW_RECORD2(j).fml;
       G_VIEW_TAB2(j).open_req               :=  G_VIEW_RECORD2(j).open_req;
       G_VIEW_TAB2(j).sourcing_rule          :=  G_VIEW_RECORD2(j).sourcing_rule;
       G_VIEW_TAB2(j).clt                    :=  G_VIEW_RECORD2(j).clt;
       G_VIEW_TAB2(j).avail2                 :=  l_avail2;
       G_VIEW_TAB2(j).int_req                :=  G_VIEW_RECORD2(j).int_req;
       G_VIEW_TAB2(j).dir_req                :=  G_VIEW_RECORD2(j).dir_req;
       G_VIEW_TAB2(j).demand                 :=  G_VIEW_RECORD2(j).demand;
       G_VIEW_TAB2(j).ITEM_STATUS_CODE       :=  G_VIEW_RECORD2(j).ITEM_STATUS_CODE;
       G_VIEW_TAB2(j).SITE_VENDOR_NUM        :=  G_VIEW_RECORD2(j).vendor_number;
       G_VIEW_TAB2(j).vendor_site            :=  G_VIEW_RECORD2(j).vendor_site;       
    
     l_count := l_count + 1;
     END loop;
     IF l_count >= 1  THEN
        lv_program_location := '5, Insert into eis_xxwc_po_isr_tab ';
        forall j in 1 .. G_VIEW_TAB2.count 
            INSERT INTO xxeis.eis_xxwc_po_isr_tab
                 values g_view_tab2(j);	
        COMMIT;  
     end if;  
     
     lv_program_location := '6, Delete tab ';	 
     G_VIEW_TAB2.delete;
     G_VIEW_RECORD2.delete;
     IF l_ref_cursor2%NOTFOUND Then
        CLOSE l_ref_cursor2;
        EXIT;
     End if;
      
      
  END LOOP;
  write_log(' Bulk Collect Main MST query End '|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );
      COMMIT;     
fnd_file.put_line(fnd_file.log,'After MST insert commit'|| to_char(sysdate,'DD-MON-YYYY HH:MI:SS') );           
  END  ;--MAIN
  
  PROCEDURE ISR_RPT_PROC
                     (p_process_id           IN NUMBER,
                      p_region               IN VARCHAR2,
                      p_district             IN VARCHAR2,
                      p_location             IN VARCHAR2,
                      p_dc_mode              IN VARCHAR2,
                      p_tool_repair          IN VARCHAR2,
                      p_time_sensitive       IN VARCHAR2,   
                      p_stk_items_with_hit4  IN VARCHAR2,
                      p_Report_Condition     in varchar2,
                      p_report_criteria      in varchar2,
                      p_report_criteria_val  in varchar2,
                      p_start_bin_loc        in varchar2,
                      p_end_bin_loc          in varchar2,
                      p_vendor               in varchar2,
                      p_item                 in varchar2,
                      p_cat_class            in varchar2 ,
                      P_ORG_LIST             IN VARCHAR2,
                      P_ITEM_LIST            IN VARCHAR2,
                      p_supplier_list        in varchar2,
                      P_CAT_CLASS_LIST       in varchar2,
                      P_SOURCE_LIST          in varchar2,
                      P_INTANGIBLES          in varchar2 
                      )
 IS
 l_query                varchar2(32000);
 L_CONDITION_STR        VARCHAR2(32000);
 LV_PROGRAM_LOCATION    VARCHAR2(2000);
 L_REF_CURSOR           CURSOR_TYPE;
 L_INSERT_RECD_CNT      NUMBER;
 L_ORG_LIST_VLAUES      VARCHAR2(32000);
 L_SUPPLIER_EXISTS      VARCHAR2(32767);
 L_ORG_EXISTS           VARCHAR2(32000);
 l_item_exists          varchar2(32000);
 L_CATCLASS_EXISTS      VARCHAR2(32000);  
 L_source_exists          VARCHAR2(32000); 
 L_LLEGTH NUMBER;
 

 BEGIN
       if p_region is not null then
           l_condition_str:= l_condition_str||' and REGION in ('||xxeis.eis_rs_utility.get_param_values(p_region)||' )';
       END IF;
       if p_district is not null then
          l_condition_str:= l_condition_str||' and DISTRICT in ('||xxeis.eis_rs_utility.get_param_values(p_district)||' )';
       END IF;
       if p_location is not null then
          L_CONDITION_STR:= L_CONDITION_STR||' and ORG in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_LOCATION)||' )';
        /*  l_location_exists:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''ORG''
                                     and list_values like '''||'%'||p_location||'%'||'''
                                     )';
      ELSE
       l_location_exists:='and 1=1';*/
       end if;
       if p_vendor is not null then
          L_CONDITION_STR:= L_CONDITION_STR||' and VENDOR_NUM in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_VENDOR)||' )';
      /* l_vendor_exists :='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''VENDOR''
                                     and list_values like '''||'%'||p_vendor||'%'||'''
                                     )';
        FND_FILE.PUT_LINE(FND_FILE.LOG,'l_vendor_exists is '||L_VENDOR_EXISTS);
        ELSE
        l_vendor_exists:='and 1=1';*/
       END IF;
       if p_item is not null then
          L_CONDITION_STR:= L_CONDITION_STR||' and msi.concatenated_segments in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ITEM)||' )';
      /* L_ITEM_EXISTS:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''ITEM''
                                     and list_values like '''||'%'||p_item||'%'||'''
                                     )';
       ELSE
        L_ITEM_EXISTS:='and 1=1';*/
      
       end if;
       if p_cat_class is not null then
          l_condition_str:= l_condition_str||' and CAT in ('||xxeis.eis_rs_utility.get_param_values(p_cat_class)||' )';
       END IF;       
     /*  If P_Dc_Mode Is Not Null
       l_condition_str:= l_condition_str:=||'and ORG_NAME ='P_Location;
       Then
       End If;*/
       if (P_INTANGIBLES ='Exclude') then             
         l_condition_str:= l_condition_str||' and nvl(ITEM_STATUS_CODE,''Active'') <> ''Intangible'''; 
       end if;
       
       if (P_TOOL_REPAIR ='Tool Repair Only' or P_TOOL_REPAIR like 'Tool%') then             
         l_condition_str:= l_condition_str||' and CAT = ''TH01''';
       end if;
       
       if p_tool_repair ='Exclude' then             
         l_condition_str:= l_condition_str||' and CAT <> ''TH01''';
       END IF;
       
       if p_report_condition='All items' then
           l_condition_str:= l_condition_str || ' and 1=1 ';
       end if;    
       
       if p_report_condition='All items on hand' then
            l_condition_str:= l_condition_str||' and QOH > 0 ';
       end if;    
       
        if (p_report_condition='Non-stock on hand' or p_report_condition like 'Non-%')  then
          l_condition_str:= l_condition_str||' and STK_FLAG  =''N'''||' and QOH > 0 ';
       end if;    
       
       if (p_report_condition='Non stock only' or (p_report_condition like 'Non%' and p_report_condition not like 'Non-%')) then
            l_condition_str:= l_condition_str||' and STK_FLAG =''N'' ';
       end if;        
       
       if p_report_condition='Active large' then
            L_CONDITION_STR:= L_CONDITION_STR||' and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or twelve_sales > 0 or on_ord > 0 or Available > 0 or Available < 0)';
       end if;       
       
       if p_report_condition='Active small' then
          l_condition_str:= l_condition_str||' AND (( STK_FLAG =''Y'' OR (STK_FLAG =''N'' and (QOH > 0 or on_ord > 0 or open_req > 0 or  int_req > 0 or demand > 0 or dir_req > 0 or Available > 0 )))'
                                            ||   ' AND ((NOT EXISTS (select 1 from dual where REGEXP_LIKE(substr(item_number,1,1),''[a-zA-Z]''))) or item_number like ''SP%''))';
          ---l_condition_str:= l_condition_str||' and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or on_ord > 0 or Available > 0 or Available < 0)';
       end if;  
       
       if p_report_condition='Stock items only' then
          l_condition_str:= l_condition_str||' and STK_FLAG =''Y''';
       end if;       
       
       if p_report_condition='Stock items with 0/0 min/max' then
          l_condition_str:= l_condition_str||' and STK_FLAG =''Y'''||' and MINN=0 and MAXN=0';
       end if;          
       
      if (p_time_sensitive ='Time Sensitive Only' or p_time_sensitive like 'Time%') then
          l_condition_str:= l_condition_str||' and TS > 0 and TS < 4000';
       END IF;
       
        IF p_stk_items_with_hit4 IS NOT NULL  THEN
          l_condition_str:= l_condition_str||' and STK_FLAG =''Y'''|| ' and hit4_sales >'||p_stk_items_with_hit4;
       END IF;      
             
       IF P_ORG_LIST IS NOT NULL THEN 
         XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_ORG_LIST,'Org');
         L_ORG_EXISTS:= ' AND EXISTS (SELECT 1  
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME     ='''||P_ORG_LIST||'''
                                         AND x.LIST_TYPE     =''Org''
                                         AND x.process_id    = '||P_PROCESS_ID||'
                                         AND TAB.ORG = REGEXP_REPLACE(X.list_value,''[^[a-z,A-Z,0-9]]*'')) ';                            
  
       ELSE
         L_ORG_EXISTS :=' and 1=1 ';
       END IF;
    
       IF P_SUPPLIER_LIST IS NOT NULL THEN 
          XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_SUPPLIER_LIST,'Supplier');        
          L_SUPPLIER_EXISTS :=' AND EXISTS (SELECT 1  
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''||P_SUPPLIER_LIST||'''
                                         AND x.LIST_TYPE    =''Supplier''
                                         AND x.process_id   = '||P_PROCESS_ID||'
                                         AND TAB.vendor_num = REGEXP_REPLACE(X.list_value,''[^[a-z,A-Z,0-9]]*'')) ';  
       ELSE
          L_SUPPLIER_EXISTS :=' and 1=1 ';
       END IF;
       
       IF P_ITEM_LIST IS NOT NULL THEN 
          XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_ITEM_LIST,'Item');      
          L_ITEM_EXISTS:=' AND EXISTS (SELECT 1  
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''||P_ITEM_LIST||'''
                                         AND x.LIST_TYPE    =''Item''
                                         AND x.process_id   = '||P_PROCESS_ID||'
                                         AND TAB.item_number = REGEXP_REPLACE(X.list_value,''[^[a-z,A-Z,0-9]]*'')) ';  
        ELSE
          L_ITEM_EXISTS :=' and 1=1 ';
        end if;
        
       IF P_CAT_CLASS_LIST IS NOT NULL THEN 
          XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_CAT_CLASS_LIST,'Cat Class');      
          L_ITEM_EXISTS:=' AND EXISTS (SELECT 1  
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''||P_CAT_CLASS_LIST||'''
                                         AND x.LIST_TYPE    =''Cat Class''
                                         AND x.process_id   = '||P_PROCESS_ID||'
                                         AND TAB.cat        = REGEXP_REPLACE(X.list_value,''[^[a-z,A-Z,0-9]]*'')) ';  
       ELSE       
         L_CATCLASS_EXISTS :=' and 1=1 ';       
       end if;        
       
      IF P_SOURCE_LIST IS NOT NULL THEN 
         XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID,P_SOURCE_LIST,'Source');      
         L_ITEM_EXISTS:=' AND EXISTS (SELECT 1  
                                        FROM xxeis.EIS_XXWC_PARAM_PARSE_LIST x
                                       WHERE x.LIST_NAME    ='''||P_SOURCE_LIST||'''
                                         AND x.LIST_TYPE    =''Source''
                                         AND x.process_id   = '||P_PROCESS_ID||'
                                         AND TAB.source     = REGEXP_REPLACE(X.list_value,''[^[a-z,A-Z,0-9]]*'')) ';        

       ELSE       
         L_source_exists :=' and 1=1 ';       
       END IF;               
      
    
    
    
    if p_report_criteria is not null and p_report_criteria_val is not null  then
          if (p_report_criteria='Vendor Number(%)' or p_report_criteria like 'Vendor%')  then
             l_condition_str:= l_condition_str||' and upper(VENDOR_NUM) like '''||upper(p_report_criteria_val)||''''; 
             Fnd_File.Put_Line(Fnd_File.Log,l_condition_str);
          end if;
          if (p_report_criteria='3 Digit Prefix' or p_report_criteria like '3%')  then
             l_condition_str:= l_condition_str||' and upper(PRE) = '''||upper(p_report_criteria_val)||''''; 
          end if;
          if (p_report_criteria='Item number' or p_report_criteria like 'Item%')   then
            l_condition_str:= l_condition_str||' and upper(ITEM_NUMBER) = '''||upper(p_report_criteria_val)||''''; 
          end if;   
          if p_report_criteria='Source' and p_report_criteria_val is not null  then
             L_Condition_Str:= L_Condition_Str||' and upper(SOURCE) = '''||upper(p_report_criteria_val)||''''; 
            NULL;
          END IF; 
          IF p_report_criteria='External source vendor'   then
             -- L_Condition_Str:= L_Condition_Str||' and ITEM_NUMBER = '''||P_Report_Criteria_Val||''; 
          null;
          end if;   
          if (p_report_criteria='2 Digit Cat' or p_report_criteria like '2%')  then
             l_condition_str:= l_condition_str||' and upper(inv_cat_seg1) = '''||upper(p_report_criteria_val)||''''; 
            null;
          END IF;  
          if (p_report_criteria='4 Digit Cat Class' or p_report_criteria like '4%')   then
             l_condition_str:= l_condition_str||' and upper(CAT) = '''||upper(p_report_criteria_val)||''''; 
          END IF;      
          if (P_REPORT_CRITERIA='Default Buyer(%)' or P_REPORT_CRITERIA like 'Default%')  then
             l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(p_report_criteria_val)||''''; --commented by santosh
            -- l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(replace(p_report_criteria_val,''''))||''''; --commented by santosh
--             L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(P_REPORT_CRITERIA_VAL)||''')'; -- added by santosh
      --       L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(replace(replace(P_REPORT_CRITERIA_VAL,'''',''),',',', '))||''')'; -- added by santosh

             --l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_report_criteria_val))||''''; 
          end if;              
       END IF;  
       if p_start_bin_loc is not null  then
         l_condition_str:= l_condition_str||' and upper(Bin_Loc) >= '''||upper(p_start_bin_loc)||''''; 
       END IF;
       if p_end_bin_loc is not null   then
         l_condition_str:= l_condition_str||' and upper(Bin_Loc) <= '''||upper(p_end_bin_loc)||''''; 
       END IF;         
      
      
    /*   IF p_all_items ='Yes'   THEN
       l_condition_str:= ' and 1=1';
       END IF;
*/


  l_query:= 'select tab.* 
             from 
             xxeis.EIS_XXWC_PO_ISR_V tab,
             mtl_system_items_kfv msi
             where 1=1 
             and msi.inventory_item_id = tab.inventory_item_id
             and msi.organization_id   = tab.organization_id  '||
             L_CONDITION_STR||L_ORG_EXISTS||L_SUPPLIER_EXISTS||L_ITEM_EXISTS||L_CATCLASS_EXISTS||L_SOURCE_EXISTS;
            lv_program_location:='The main query is ' ||l_query;
            write_log(lv_program_location);
            l_insert_recd_cnt:=1;
   OPEN l_ref_cursor FOR l_query;
   LOOP
        FETCH l_ref_cursor  INTO g_view_record;
        Exit When L_Ref_Cursor%Notfound; 
        G_View_Tab(L_Insert_Recd_Cnt).Org:=                   G_View_Record.Org ;                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt).Pre:=                   G_View_Record.Pre ;                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt).ITEM_NUMBER:=           g_view_record.ITEM_NUMBER ;                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt).Vendor_Num:=            G_View_Record.Vendor_Num  ;                                                                                                                                                                    
        G_View_Tab(L_Insert_Recd_Cnt ).VENDOR_NAME:=          g_view_record.VENDOR_NAME ;                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).Source:=               G_View_Record.Source ;                                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).St:=                   G_View_Record.St      ;                                                                                                                                                                  
        G_View_Tab(L_Insert_Recd_Cnt ).Description:=          G_View_Record.Description  ;                                                                                                                                                                                
        G_View_Tab(L_Insert_Recd_Cnt ).Cat:=                  G_View_Record.Cat     ;                                                                                                                                                                                       
        G_View_Tab(L_Insert_Recd_Cnt ).Pplt:=                 G_View_Record.Pplt  ;                                                                                                                                                                                         
        G_View_Tab(L_Insert_Recd_Cnt ).Plt:=                  G_View_Record.Plt   ;                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Uom:=                  G_View_Record.Uom     ;                                                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).Cl:=                   G_View_Record.Cl     ;                                                                                                                                                                                                
        G_View_Tab(L_Insert_Recd_Cnt ).Stk_Flag:=             G_View_Record.Stk_Flag  ;                                                                                                                                                                                                     
        G_View_Tab(L_Insert_Recd_Cnt ).Pm:=                   G_View_Record.Pm      ;                                                                                                                                                                                                
        G_View_Tab(L_Insert_Recd_Cnt ).Minn:=                 G_View_Record.Minn    ;                                                                                                                                                                                                          
        G_View_Tab(L_Insert_Recd_Cnt ).Maxn:=                 G_View_Record.Maxn  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Amu:=                  G_View_Record.Amu     ;                                                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).Mf_Flag:=              G_View_Record.Mf_Flag    ;                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Hit6_Sales:=           G_View_Record.Hit6_Sales     ;                                                                                                                                                                                                         
        G_View_Tab(L_Insert_Recd_Cnt ).Aver_Cost:=            G_View_Record.Aver_Cost   ;                                                                                                                                                                                                           
        G_View_Tab(L_Insert_Recd_Cnt ).Item_Cost:=            G_View_Record.Item_Cost;                                                                                                                                                                                                              
        G_VIEW_TAB(L_INSERT_RECD_CNT ).BPA:=                  G_VIEW_RECORD.BPA;   
        G_View_Tab(L_Insert_Recd_Cnt ).Bpa_cost:=             G_View_Record.Bpa_cost; 
        G_View_Tab(L_Insert_Recd_Cnt ).Qoh:=                  G_View_Record.Qoh;                                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Available:=            G_View_Record.Available;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Availabledollar:=      G_View_Record.Availabledollar;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Jan_Sales:=            G_View_Record.Jan_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Feb_Sales:=            G_View_Record.Feb_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Mar_Sales:=            G_View_Record.Mar_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Apr_Sales:=            G_View_Record.Apr_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).May_Sales:=            G_View_Record.May_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).June_Sales:=           G_View_Record.June_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Jul_Sales:=            G_View_Record.Jul_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Aug_Sales:=            G_View_Record.Aug_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Sep_Sales:=            G_View_Record.Sep_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Oct_Sales:=            G_View_Record.Oct_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Nov_Sales:=            G_View_Record.Nov_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Dec_Sales:=            G_View_Record.Dec_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Hit4_Sales:=           G_View_Record.Hit4_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).One_Sales:=            G_View_Record.One_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Six_Sales:=            G_View_Record.Six_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Twelve_Sales:=         G_View_Record.Twelve_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Bin_Loc:=              G_View_Record.Bin_Loc;                                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt ).Mc:=                   G_View_Record.Mc;                                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt ).Fi_Flag:=              G_View_Record.Fi_Flag;                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Freeze_Date:=          G_View_Record.Freeze_Date;                                                                                                                                                                                         
        G_View_Tab(L_Insert_Recd_Cnt ).Res:=                  G_View_Record.Res;                                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt ).Thirteen_Wk_Avg_Inv:=  G_View_Record.Thirteen_Wk_Avg_Inv  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).THIRTEEN_WK_AN_COGS:=  g_view_record.THIRTEEN_WK_AN_COGS ;                                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Turns:=                G_View_Record.Turns  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Buyer:=                G_View_Record.Buyer  ;                                                                                                                                                                                                    
        G_VIEW_TAB(L_INSERT_RECD_CNT ).TS:=                   G_VIEW_RECORD.TS   ;                                                                                                                                                                                                           
        G_VIEW_TAB(L_INSERT_RECD_CNT ).SO:=                   G_VIEW_RECORD.SO  ;  
        g_view_tab(l_insert_recd_cnt ).sourcing_rule:=        g_view_record.sourcing_rule  ;  
        g_View_Tab(L_Insert_Recd_Cnt ).clt:=                  g_View_Record.clt  ; 
        G_View_Tab(L_Insert_Recd_Cnt ).INVENTORY_ITEM_ID:=    g_view_record.INVENTORY_ITEM_ID  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Organization_Id:=      G_View_Record.Organization_Id   ;                                                                                                                                                                                                           
        g_view_tab(l_insert_recd_cnt ).set_of_books_id:=      g_view_record.set_of_books_id  ;
        G_View_Tab(L_Insert_Recd_Cnt ).on_ord:=               g_view_record.on_ord ;
        G_View_Tab(L_Insert_Recd_Cnt ).ORG_NAME:=             g_view_record.ORG_NAME   ;                                                                                                                                                                                          
        G_View_Tab(L_Insert_Recd_Cnt ).District:=             G_View_Record.District ;                                                                                                                                                                                          
        g_view_tab(l_insert_recd_cnt ).region:=               g_view_record.region ;          
        G_View_Tab(L_Insert_Recd_Cnt ).wt:=             g_view_record.wt   ;                                                                                                                                                                                          
        g_view_tab(l_insert_recd_cnt ).ss:=             g_view_record.ss ;                                                                                                                                                                                          
        g_view_tab(l_insert_recd_cnt ).fml:=               g_view_record.fml ;  
        g_view_tab(l_insert_recd_cnt ).open_req:=               g_view_record.open_req ;          
        
        g_view_tab(l_insert_recd_cnt ).common_output_id:=xxeis.eis_rs_common_outputs_s.nextval;
        g_view_tab(l_insert_recd_cnt ).process_id:=p_process_id;
        g_view_tab(l_insert_recd_cnt ).avail2:=G_View_Record.avail2;
        g_view_tab(l_insert_recd_cnt ).int_req:=G_View_Record.int_req;
        g_view_tab(l_insert_recd_cnt ).dir_req:=G_View_Record.dir_req;
        g_view_tab(l_insert_recd_cnt ).demand:=G_View_Record.demand;
        
        g_view_tab(l_insert_recd_cnt ).site_vendor_num:=G_View_Record.site_vendor_num;
        g_view_tab(l_insert_recd_cnt ).vendor_site:=G_View_Record.vendor_site;
        l_insert_recd_cnt:=l_insert_recd_cnt+1;       
   END loop;
   CLOSE l_ref_cursor; 
   begin
  
         fnd_file.put_line(fnd_file.log,'l_insert_recd_cnt' ||l_insert_recd_cnt);
         fnd_file.put_line(fnd_file.log,'first common_output_id' ||g_view_tab(1).common_output_id);
     --    Fnd_File.Put_Line(Fnd_File.Log,'last common_output_id' ||G_View_Tab(L_Insert_Recd_Cnt).Common_Output_Id);
         IF l_insert_recd_cnt >= 1  then
            forall i IN g_view_tab.FIRST .. g_view_tab.LAST 
              INSERT INTO xxeis.eis_xxwc_po_isr_rpt_v
                    values g_view_tab (i);
         END IF;      
         commit;
         
         g_view_tab.DELETE;
    exception
         when others
         then
           fnd_file.put_line(fnd_file.log,'Inside Exception==>'||substr(sqlerrm,1,240));
      END;

  END;

 PROCEDURE ISR_LIVE_RPT_PROC
                     (p_process_id           IN NUMBER,
                      p_region               IN VARCHAR2,
                      p_district             IN VARCHAR2,
                      p_location             IN VARCHAR2,
                      p_dc_mode              IN VARCHAR2,
                      p_tool_repair          IN VARCHAR2,
                      p_time_sensitive       IN VARCHAR2,   
                      p_stk_items_with_hit4  IN VARCHAR2,
                      p_Report_Condition     in varchar2,
                      p_report_criteria      in varchar2,
                      p_report_criteria_val  in varchar2,
                      p_start_bin_loc        in varchar2,
                      p_end_bin_loc          in varchar2,
                      p_vendor               in varchar2,
                      p_item                 in varchar2,
                      p_cat_class            in varchar2 ,
                      P_ORG_LIST             IN VARCHAR2,
                      P_ITEM_LIST            IN VARCHAR2,
                      p_supplier_list        in varchar2,
                      P_CAT_CLASS_LIST       in varchar2,
                      P_SOURCE_LIST          in varchar2
                      )
 IS
 l_query                varchar2(32000);
 L_CONDITION_STR        VARCHAR2(32000);
 LV_PROGRAM_LOCATION    VARCHAR2(2000);
 L_REF_CURSOR           CURSOR_TYPE;
 L_INSERT_RECD_CNT      NUMBER;
 L_ORG_LIST_VLAUES      VARCHAR2(32000);
 L_SUPPLIER_EXISTS      VARCHAR2(32767);
 L_ORG_EXISTS           VARCHAR2(32000);
 l_item_exists          varchar2(32000);
 L_CATCLASS_EXISTS      VARCHAR2(32000);  
 L_source_exists          VARCHAR2(32000); 
 L_LLEGTH NUMBER;
 

 BEGIN
       if p_region is not null then
           l_condition_str:= l_condition_str||' and REGION in ('||xxeis.eis_rs_utility.get_param_values(p_region)||' )';
       END IF;
       if p_district is not null then
          l_condition_str:= l_condition_str||' and DISTRICT in ('||xxeis.eis_rs_utility.get_param_values(p_district)||' )';
       END IF;
       if p_location is not null then
          L_CONDITION_STR:= L_CONDITION_STR||' and ORG in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_LOCATION)||' )';
        /*  l_location_exists:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''ORG''
                                     and list_values like '''||'%'||p_location||'%'||'''
                                     )';
      ELSE
       l_location_exists:='and 1=1';*/
       end if;
       if p_vendor is not null then
          L_CONDITION_STR:= L_CONDITION_STR||' and VENDOR_NAME in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_VENDOR)||' )';
      /* l_vendor_exists :='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''VENDOR''
                                     and list_values like '''||'%'||p_vendor||'%'||'''
                                     )';
        FND_FILE.PUT_LINE(FND_FILE.LOG,'l_vendor_exists is '||L_VENDOR_EXISTS);
        ELSE
        l_vendor_exists:='and 1=1';*/
       END IF;
       if p_item is not null then
          L_CONDITION_STR:= L_CONDITION_STR||' and msi.concatenated_segments in ('||XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(P_ITEM)||' )';
      /* L_ITEM_EXISTS:='And exists (select 1 from apps.XXWC_PARAM_LIST
                                    where list_name=''ITEM''
                                     and list_values like '''||'%'||p_item||'%'||'''
                                     )';
       ELSE
        L_ITEM_EXISTS:='and 1=1';*/
      
       end if;
       if p_cat_class is not null then
          l_condition_str:= l_condition_str||' and CAT in ('||xxeis.eis_rs_utility.get_param_values(p_cat_class)||' )';
       END IF;       
     /*  If P_Dc_Mode Is Not Null
       l_condition_str:= l_condition_str:=||'and ORG_NAME ='P_Location;
       Then
       End If;*/
       if (P_TOOL_REPAIR ='Tool Repair Only' or P_TOOL_REPAIR like 'Tool%') then             
         l_condition_str:= l_condition_str||' and CAT = ''TH01''';
       end if;
       
       if p_tool_repair ='Exclude' then             
         l_condition_str:= l_condition_str||' and CAT <> ''TH01''';
       END IF;
       
       if p_report_condition='All items' then
           l_condition_str:= l_condition_str || ' and 1=1 ';
       end if;    
       
       if p_report_condition='All items on hand' then
            l_condition_str:= l_condition_str||' and QOH > 0 ';
       end if;    
       
        if (p_report_condition='Non-stock on hand' or p_report_condition like 'Non-%')  then
          l_condition_str:= l_condition_str||' and STK_FLAG  =''N'''||' and QOH > 0 ';
       end if;    
       
       if (p_report_condition='Non stock only' or (p_report_condition like 'Non%' and p_report_condition not like 'Non-%')) then
            l_condition_str:= l_condition_str||' and STK_FLAG =''N'' ';
       end if;        
       
       if p_report_condition='Active large' then
            L_CONDITION_STR:= L_CONDITION_STR||' and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or twelve_sales > 0 or on_ord > 0 or Available > 0 or Available < 0)';
       end if;       
       
       if p_report_condition='Active small' then
          l_condition_str:= l_condition_str||' AND (( STK_FLAG =''Y'' OR (STK_FLAG =''N'' and (QOH > 0 or on_ord > 0 or open_req > 0 or  int_req > 0 or demand > 0 or dir_req > 0 or Available > 0 )))'
                                            ||   ' AND ((NOT EXISTS (select 1 from dual where REGEXP_LIKE(substr(item_number,1,1),''[a-zA-Z]''))) or item_number like ''SP%''))';
          ---l_condition_str:= l_condition_str||' and STK_FLAG in (''N'',''Y'') '||' and (QOH > 0 or on_ord > 0 or Available > 0 or Available < 0)';
       end if;  
       
       if p_report_condition='Stock items only' then
          l_condition_str:= l_condition_str||' and STK_FLAG =''Y''';
       end if;       
       
       if p_report_condition='Stock items with 0/0 min/max' then
          l_condition_str:= l_condition_str||' and STK_FLAG =''Y'''||' and MINN=0 and MAXN=0';
       end if;          
       
      if (p_time_sensitive ='Time Sensitive Only' or p_time_sensitive like 'Time%') then
          l_condition_str:= l_condition_str||' and TS > 0 and TS < 4000';
       END IF;
       
        IF p_stk_items_with_hit4 IS NOT NULL  THEN
          l_condition_str:= l_condition_str||' and STK_FLAG =''Y'''|| ' and hit4_sales >'||p_stk_items_with_hit4;
       END IF;      
       
       IF P_ORG_LIST IS NOT NULL THEN 
         L_ORG_EXISTS:= ' AND EXISTS (SELECT 1  FROM 
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME='''||P_ORG_LIST||'''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                         TAB.org, 1, 1)<>0) ';
          ELSE
       
       L_ORG_EXISTS :=' and 1=1 ';
       
       END IF;
    
         IF P_SUPPLIER_LIST IS NOT NULL THEN 
       L_SUPPLIER_EXISTS :=' AND EXISTS (SELECT 1  FROM 
                            APPS.XXWC_PARAM_LIST
                            WHERE LIST_NAME='''||P_SUPPLIER_LIST||'''
                            AND DBMS_LOB.INSTR(LIST_VALUES,
                            TAB.vendor_name, 1, 1)<>0) ';
       ELSE
       L_SUPPLIER_EXISTS :=' and 1=1 ';
       END IF;
       IF P_ITEM_LIST IS NOT NULL THEN 
        L_ITEM_EXISTS:=' AND EXISTS (SELECT 1  FROM 
                        APPS.XXWC_PARAM_LIST
                        WHERE LIST_NAME='''||P_ITEM_LIST||'''
                        AND DBMS_LOB.INSTR(LIST_VALUES,
                        TAB.item_number, 1, 1)<>0) ';
         ELSE
        L_ITEM_EXISTS :=' and 1=1 ';
        end if;
        
       IF P_CAT_CLASS_LIST IS NOT NULL THEN 
         L_CATCLASS_EXISTS:= ' AND EXISTS (SELECT 1  FROM 
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME='''||P_CAT_CLASS_LIST||'''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                         TAB.cat, 1, 1)<>0) ';
          ELSE
       
       L_CATCLASS_EXISTS :=' and 1=1 ';
       
       end if;        
       
      IF P_SOURCE_LIST IS NOT NULL THEN 
         L_source_exists:= ' AND EXISTS (SELECT 1  FROM 
                         APPS.XXWC_PARAM_LIST
                         WHERE LIST_NAME='''||P_SOURCE_LIST||'''
                         AND DBMS_LOB.INSTR(LIST_VALUES,
                         TAB.source, 1, 1)<>0) ';
          ELSE
       
       L_source_exists :=' and 1=1 ';
       
       END IF;               
      
    
    
    
      if p_report_criteria is not null and p_report_criteria_val is not null  then
          if (p_report_criteria='Vendor Number(%)' or p_report_criteria like 'Vendor%')  then
             l_condition_str:= l_condition_str||' and upper(VENDOR_NUM) like '''||upper(p_report_criteria_val)||''''; 
             Fnd_File.Put_Line(Fnd_File.Log,l_condition_str);
          end if;
          if (p_report_criteria='3 Digit Prefix' or p_report_criteria like '3%')  then
             l_condition_str:= l_condition_str||' and upper(PRE) = '''||upper(p_report_criteria_val)||''''; 
          end if;
          if (p_report_criteria='Item number' or p_report_criteria like 'Item%')   then
            l_condition_str:= l_condition_str||' and upper(ITEM_NUMBER) = '''||upper(p_report_criteria_val)||''''; 
          end if;   
          if p_report_criteria='Source' and p_report_criteria_val is not null  then
             L_Condition_Str:= L_Condition_Str||' and upper(SOURCE) = '''||upper(p_report_criteria_val)||''''; 
            NULL;
          END IF; 
          IF p_report_criteria='External source vendor'   then
             -- L_Condition_Str:= L_Condition_Str||' and ITEM_NUMBER = '''||P_Report_Criteria_Val||''; 
          null;
          end if;   
          if (p_report_criteria='2 Digit Cat' or p_report_criteria like '2%')  then
             l_condition_str:= l_condition_str||' and upper(inv_cat_seg1) = '''||upper(p_report_criteria_val)||''''; 
            null;
          END IF;  
          if (p_report_criteria='4 Digit Cat Class' or p_report_criteria like '4%')   then
             l_condition_str:= l_condition_str||' and upper(CAT) = '''||upper(p_report_criteria_val)||''''; 
          END IF;      
          if (P_REPORT_CRITERIA='Default Buyer(%)' or P_REPORT_CRITERIA like 'Default%')  then
             l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(p_report_criteria_val)||''''; --commented by santosh
            -- l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(replace(p_report_criteria_val,''''))||''''; --commented by santosh
--             L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(P_REPORT_CRITERIA_VAL)||''')'; -- added by santosh
      --       L_CONDITION_STR:= L_CONDITION_STR||' and upper(Buyer) in ('''||UPPER(replace(replace(P_REPORT_CRITERIA_VAL,'''',''),',',', '))||''')'; -- added by santosh

             --l_condition_str:= l_condition_str||' and upper(Buyer) like '''||upper(XXEIS.EIS_RS_UTILITY.GET_PARAM_VALUES(p_report_criteria_val))||''''; 
          end if;              
       END IF; 
       if p_start_bin_loc is not null  then
         l_condition_str:= l_condition_str||' and upper(Bin_Loc) >= '''||upper(p_start_bin_loc)||''''; 
       END IF;
       if p_end_bin_loc is not null   then
         l_condition_str:= l_condition_str||' and upper(Bin_Loc) <= '''||upper(p_end_bin_loc)||''''; 
       END IF;         
      
      
    /*   IF p_all_items ='Yes'   THEN
       l_condition_str:= ' and 1=1';
       END IF;
*/


  l_query:= 'select tab.* 
             from 
             xxeis.EIS_XXWC_PO_ISR_LIVE_V tab,
             mtl_system_items_kfv msi
             where 1=1 
             and msi.inventory_item_id = tab.inventory_item_id
             and msi.organization_id   = tab.organization_id  '||
             L_CONDITION_STR||L_ORG_EXISTS||L_SUPPLIER_EXISTS||L_ITEM_EXISTS||L_CATCLASS_EXISTS||L_SOURCE_EXISTS;
            lv_program_location:='The main query is ' ||l_query;
            write_log(lv_program_location);
            l_insert_recd_cnt:=1;
   OPEN l_ref_cursor FOR l_query;
   LOOP
        FETCH l_ref_cursor  INTO g_view_record;
        Exit When L_Ref_Cursor%Notfound; 
        G_View_Tab(L_Insert_Recd_Cnt).Org:=                   G_View_Record.Org ;                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt).Pre:=                   G_View_Record.Pre ;                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt).ITEM_NUMBER:=           g_view_record.ITEM_NUMBER ;                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt).Vendor_Num:=            G_View_Record.Vendor_Num  ;                                                                                                                                                                    
        G_View_Tab(L_Insert_Recd_Cnt ).VENDOR_NAME:=          g_view_record.VENDOR_NAME ;                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).Source:=               G_View_Record.Source ;                                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).St:=                   G_View_Record.St      ;                                                                                                                                                                  
        G_View_Tab(L_Insert_Recd_Cnt ).Description:=          G_View_Record.Description  ;                                                                                                                                                                                
        G_View_Tab(L_Insert_Recd_Cnt ).Cat:=                  G_View_Record.Cat     ;                                                                                                                                                                                       
        G_View_Tab(L_Insert_Recd_Cnt ).Pplt:=                 G_View_Record.Pplt  ;                                                                                                                                                                                         
        G_View_Tab(L_Insert_Recd_Cnt ).Plt:=                  G_View_Record.Plt   ;                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Uom:=                  G_View_Record.Uom     ;                                                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).Cl:=                   G_View_Record.Cl     ;                                                                                                                                                                                                
        G_View_Tab(L_Insert_Recd_Cnt ).Stk_Flag:=             G_View_Record.Stk_Flag  ;                                                                                                                                                                                                     
        G_View_Tab(L_Insert_Recd_Cnt ).Pm:=                   G_View_Record.Pm      ;                                                                                                                                                                                                
        G_View_Tab(L_Insert_Recd_Cnt ).Minn:=                 G_View_Record.Minn    ;                                                                                                                                                                                                          
        G_View_Tab(L_Insert_Recd_Cnt ).Maxn:=                 G_View_Record.Maxn  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Amu:=                  G_View_Record.Amu     ;                                                                                                                                                                                                 
        G_View_Tab(L_Insert_Recd_Cnt ).Mf_Flag:=              G_View_Record.Mf_Flag    ;                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Hit6_Sales:=           G_View_Record.Hit6_Sales     ;                                                                                                                                                                                                         
        G_View_Tab(L_Insert_Recd_Cnt ).Aver_Cost:=            G_View_Record.Aver_Cost   ;                                                                                                                                                                                                           
        G_View_Tab(L_Insert_Recd_Cnt ).Item_Cost:=            G_View_Record.Item_Cost;                                                                                                                                                                                                              
        G_VIEW_TAB(L_INSERT_RECD_CNT ).BPA:=                  G_VIEW_RECORD.BPA;   
        G_View_Tab(L_Insert_Recd_Cnt ).Bpa_cost:=             G_View_Record.Bpa_cost; 
        G_View_Tab(L_Insert_Recd_Cnt ).Qoh:=                  G_View_Record.Qoh;                                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Available:=            G_View_Record.Available;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Availabledollar:=      G_View_Record.Availabledollar;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Jan_Sales:=            G_View_Record.Jan_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Feb_Sales:=            G_View_Record.Feb_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Mar_Sales:=            G_View_Record.Mar_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Apr_Sales:=            G_View_Record.Apr_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).May_Sales:=            G_View_Record.May_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).June_Sales:=           G_View_Record.June_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Jul_Sales:=            G_View_Record.Jul_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Aug_Sales:=            G_View_Record.Aug_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Sep_Sales:=            G_View_Record.Sep_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Oct_Sales:=            G_View_Record.Oct_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Nov_Sales:=            G_View_Record.Nov_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Dec_Sales:=            G_View_Record.Dec_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Hit4_Sales:=           G_View_Record.Hit4_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).One_Sales:=            G_View_Record.One_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Six_Sales:=            G_View_Record.Six_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Twelve_Sales:=         G_View_Record.Twelve_Sales;                                                                                                                                                                                                              
        G_View_Tab(L_Insert_Recd_Cnt ).Bin_Loc:=              G_View_Record.Bin_Loc;                                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt ).Mc:=                   G_View_Record.Mc;                                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt ).Fi_Flag:=              G_View_Record.Fi_Flag;                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Freeze_Date:=          G_View_Record.Freeze_Date;                                                                                                                                                                                         
        G_View_Tab(L_Insert_Recd_Cnt ).Res:=                  G_View_Record.Res;                                                                                                                                                                                                      
        G_View_Tab(L_Insert_Recd_Cnt ).Thirteen_Wk_Avg_Inv:=  G_View_Record.Thirteen_Wk_Avg_Inv  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).THIRTEEN_WK_AN_COGS:=  g_view_record.THIRTEEN_WK_AN_COGS ;                                                                                                                                                                                                             
        G_View_Tab(L_Insert_Recd_Cnt ).Turns:=                G_View_Record.Turns  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Buyer:=                G_View_Record.Buyer  ;                                                                                                                                                                                                    
        G_VIEW_TAB(L_INSERT_RECD_CNT ).TS:=                   G_VIEW_RECORD.TS   ;                                                                                                                                                                                                           
        G_VIEW_TAB(L_INSERT_RECD_CNT ).SO:=                   G_VIEW_RECORD.SO  ;  
        g_view_tab(l_insert_recd_cnt ).sourcing_rule:=        g_view_record.sourcing_rule  ;  
        g_View_Tab(L_Insert_Recd_Cnt ).clt:=                  g_View_Record.clt  ; 
        G_View_Tab(L_Insert_Recd_Cnt ).INVENTORY_ITEM_ID:=    g_view_record.INVENTORY_ITEM_ID  ;                                                                                                                                                                                                            
        G_View_Tab(L_Insert_Recd_Cnt ).Organization_Id:=      G_View_Record.Organization_Id   ;                                                                                                                                                                                                           
        g_view_tab(l_insert_recd_cnt ).set_of_books_id:=      g_view_record.set_of_books_id  ;
        G_View_Tab(L_Insert_Recd_Cnt ).on_ord:=               g_view_record.on_ord ;
        G_View_Tab(L_Insert_Recd_Cnt ).ORG_NAME:=             g_view_record.ORG_NAME   ;                                                                                                                                                                                          
        G_View_Tab(L_Insert_Recd_Cnt ).District:=             G_View_Record.District ;                                                                                                                                                                                          
        g_view_tab(l_insert_recd_cnt ).region:=               g_view_record.region ;          
        G_View_Tab(L_Insert_Recd_Cnt ).wt:=             g_view_record.wt   ;                                                                                                                                                                                          
        g_view_tab(l_insert_recd_cnt ).ss:=             g_view_record.ss ;                                                                                                                                                                                          
        g_view_tab(l_insert_recd_cnt ).fml:=               g_view_record.fml ;  
        g_view_tab(l_insert_recd_cnt ).open_req:=               g_view_record.open_req ;          
        
        g_view_tab(l_insert_recd_cnt ).common_output_id:=xxeis.eis_rs_common_outputs_s.nextval;
        g_view_tab(l_insert_recd_cnt ).process_id:=p_process_id;
        g_view_tab(l_insert_recd_cnt ).avail2:=G_View_Record.avail2;
        g_view_tab(l_insert_recd_cnt ).int_req:=G_View_Record.int_req;
        g_view_tab(l_insert_recd_cnt ).dir_req:=G_View_Record.dir_req;
        g_view_tab(l_insert_recd_cnt ).demand:=G_View_Record.demand;
        l_insert_recd_cnt:=l_insert_recd_cnt+1;       
   END loop;
   CLOSE l_ref_cursor; 
   begin
  
         fnd_file.put_line(fnd_file.log,'l_insert_recd_cnt' ||l_insert_recd_cnt);
         fnd_file.put_line(fnd_file.log,'first common_output_id' ||g_view_tab(1).common_output_id);
     --    Fnd_File.Put_Line(Fnd_File.Log,'last common_output_id' ||G_View_Tab(L_Insert_Recd_Cnt).Common_Output_Id);
         IF l_insert_recd_cnt >= 1  then
            forall i IN g_view_tab.FIRST .. g_view_tab.LAST 
              INSERT INTO xxeis.eis_xxwc_po_isr_rpt_v
                    values g_view_tab (i);
         END IF;      
         commit;
         
         g_view_tab.DELETE;
    exception
         when others
         then
           fnd_file.put_line(fnd_file.log,'Inside Exception==>'||substr(sqlerrm,1,240));
      END;

  END;  
 
End;
/