create or replace
PACKAGE BODY eis_po_xxwc_isr_util_pkg AS

function get_vendor_name (p_inventory_item_id number, p_organization_id number)
RETURN VARCHAR2
is
 l_vendor_name varchar2(240) := NULL;
BEGIN

   /* select max(pov.vendor_name )
    into l_vendor_name
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov
    where 1=1
    and ass.inventory_item_id(+)=p_inventory_item_id
    and ass.organization_id(+)=p_organization_id
    and rco.sourcing_rule_id(+)=ass.sourcing_rule_id
    and sso.sr_receipt_id(+)=rco.sr_receipt_id
    and pov.vendor_id(+)=sso.vendor_id;*/
    
   Begin
    select pov.vendor_name
     into l_vendor_name
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov,
         mtl_system_items_b msi
    where 1=1
    AND msi.inventory_item_id = ass.inventory_item_id
    AND msi.organization_id   = ass.organization_id
    and ass.inventory_item_id = p_inventory_item_id
    and ass.organization_id   = p_organization_id
    and msi.source_type       = 2
    and rco.sourcing_rule_id  = ass.sourcing_rule_id
    and sso.sr_receipt_id     = rco.sr_receipt_id
    AND SSO.SOURCE_TYPE       = 3 
    and pov.vendor_id         = sso.vendor_id;
   EXCEPTION 
     WHEN OTHERS THEN
       l_vendor_name := NULL;
   END;
   
   IF l_vendor_name IS NULL THEN
     select MAX(pov.vendor_name)
      into l_vendor_name
      from mrp_sr_assignments  ass,
           mrp_sr_receipt_org rco,
           mrp_sr_source_org sso,
           po_vendors pov,
           mtl_system_items_b msi
      where 1=1
      AND msi.inventory_item_id = ass.inventory_item_id
      AND msi.organization_id   = ass.organization_id
      and ass.inventory_item_id = p_inventory_item_id
      --and ass.organization_id   = p_organization_id
      and msi.source_type       = 2
      and rco.sourcing_rule_id  = ass.sourcing_rule_id
      and sso.sr_receipt_id     = rco.sr_receipt_id
      AND SSO.SOURCE_TYPE       = 3 
      and pov.vendor_id         = sso.vendor_id;
   END IF;

  return l_vendor_name;
 exception
 WHEN OTHERS THEN
   return null;
END;


function get_vendor_number (p_inventory_item_id number, p_organization_id number)
RETURN VARCHAR2
is
 l_vendor_number varchar2(240);
BEGIN

   Begin
    select pov.segment1
     into l_vendor_number
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         po_vendors pov,
         mtl_system_items_b msi
    where 1=1
    AND msi.inventory_item_id = ass.inventory_item_id
    AND msi.organization_id   = ass.organization_id
    and ass.inventory_item_id = p_inventory_item_id
    and ass.organization_id   = p_organization_id
    and msi.source_type       = 2
    and rco.sourcing_rule_id  = ass.sourcing_rule_id
    and sso.sr_receipt_id     = rco.sr_receipt_id
    AND SSO.SOURCE_TYPE       = 3 
    and pov.vendor_id         = sso.vendor_id;
   EXCEPTION 
     WHEN OTHERS THEN
       l_vendor_number := NULL;
   END;
   
   IF l_vendor_number IS NULL THEN
     select MAX(pov.segment1)
      into l_vendor_number
      from mrp_sr_assignments  ass,
           mrp_sr_receipt_org rco,
           mrp_sr_source_org sso,
           po_vendors pov,
           mtl_system_items_b msi
      where 1=1
      AND msi.inventory_item_id = ass.inventory_item_id
      AND msi.organization_id   = ass.organization_id
      and ass.inventory_item_id = p_inventory_item_id
      --and ass.organization_id   = p_organization_id
      and msi.source_type       = 2
      and rco.sourcing_rule_id  = ass.sourcing_rule_id
      and sso.sr_receipt_id     = rco.sr_receipt_id
      AND SSO.SOURCE_TYPE       = 3 
      and pov.vendor_id         = sso.vendor_id;
   END IF;

  return l_vendor_number;
 exception
 WHEN OTHERS THEN
   return null;
END;


FUNCTION GET_INV_CAT_CLASS (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
BEGIN
        SELECT Mcv.SEGMENT2
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Inventory Category'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  RETURN NULL;
END;

FUNCTION Get_inv_cat_seg1 (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
Begin
        SELECT Mcv.SEGMENT1
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Inventory Category'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  RETURN NULL;
End;

FUNCTION Get_inv_vel_cat_class (P_INVENTORY_ITEM_ID NUMBER ,P_ORGANIZATION_ID NUMBER) RETURN VARCHAR2
IS
l_cat_class varchar2(400);
BEGIN
        SELECT Mcv.SEGMENT1
        INTO L_CAT_CLASS
        from  Mtl_Categories_Kfv Mcv,
            MTL_CATEGORY_SETS MCS,
            Mtl_Item_Categories Mic
            Where   Mcs.Category_Set_Name      = 'Sales Velocity'
          And Mcs.Structure_Id                 = Mcv.Structure_Id
          And Mic.Inventory_Item_Id            = P_Inventory_Item_Id
          And Mic.Organization_Id              = P_Organization_Id
          AND MIC.CATEGORY_SET_ID              = MCS.CATEGORY_SET_ID
          And Mic.Category_Id                  = Mcv.Category_Id;

  Return L_Cat_Class;

Exception
WHEN OTHERS THEN
  Return Null;
END;

function get_isr_item_cost (p_inventory_item_id number,p_organization_id number) return number
is
l_item_cost number;
begin

  select unit_price
  into l_item_cost
  from po_lines_all
  where po_line_id in
            ( select max(pol.po_line_id)
             from po_headers poh,
                  po_line_locations poll,
                  po_lines pol,
                  po_vendors   pov
             where poh.type_lookup_code      = 'BLANKET'
             and   poh.po_header_id          = pol.po_header_id
             and   pol.po_line_id            = poll.po_line_id(+)
             and   nvl(poh.cancel_flag,'N')='N'
            --and  poll.quantity_received > 0
            and  pol.item_id                 = p_inventory_item_id
            and (poh.global_agreement_flag ='Y' or poll.ship_to_organization_id = p_organization_id)
            and poh.vendor_id                = pov.vendor_id
            and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)
            );

  Return l_item_cost;

      exception when others then
    return  null;

end;

function get_isr_bpa_doc (p_inventory_item_id number,p_organization_id number) return number
is
l_bpa_doc number;
begin

  select segment1
  into l_bpa_doc
  from po_headers_all
  where po_header_id in
            ( select max(poh.po_header_id)
             from po_headers poh,
                  po_line_locations poll,
                  po_lines_all pol,
                  po_vendors   pov
             where poh.type_lookup_code      = 'BLANKET'
             and   poh.po_header_id          = pol.po_header_id
             and   nvl(poh.cancel_flag,'N')='N'
             and   pol.po_line_id            = poll.po_line_id(+)
            --and  poll.quantity_received > 0
              and  pol.item_id                 = p_inventory_item_id
              and (poh.global_agreement_flag ='Y' or poll.ship_to_organization_id = p_organization_id)
              and poh.vendor_id                = pov.vendor_id
              and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)
            );

  Return l_bpa_doc;

      exception when others then
    return  null;

end;

   FUNCTION get_isr_open_po_qty (p_inventory_item_id    NUMBER,
                                 p_organization_id      NUMBER)
      RETURN NUMBER
   is
      L_OPEN_QTY                  number;
      L_INTERNAL_RECEIPT_QTY      number;
      L_ONORDER_QTY               number;
      L_COMPONENT_QTY             number;
      l_assembly_qy               number;
   BEGIN
      SELECT SUM (qty)
        INTO l_open_qty
        FROM (SELECT SUM (
                          poll.quantity
                        - NVL (poll.quantity_received, 0)
                        - NVL (poll.quantity_cancelled, 0))
                        qty
                FROM po_headers poh,
                     po_line_locations poll,
                     po_lines pol,
                     po_vendors pov
               WHERE     1 = 1
                     AND NVL (poh.cancel_flag, 'N') = 'N'
                     AND NVL (pol.cancel_flag, 'N') = 'N'
                     AND NVL (poll.cancel_flag, 'N') = 'N'
                     AND NVL (poh.closed_code, 'OPEN') NOT IN
                            ('FINALLY CLOSED', 'CLOSED')
                     AND NVL (poll.closed_code, 'OPEN') NOT IN
                            ('CLOSED', 'FINALLY CLOSED')
                     AND NVL (pol.closed_code, 'OPEN') NOT IN
                            ('CLOSED', 'FINALLY CLOSED')
                     AND poh.type_lookup_code IN
                            ('STANDARD', 'BLANKET', 'PLANNED')
                     AND poh.po_header_id = pol.po_header_id
                     AND pol.po_line_id = poll.po_line_id
                     AND poll.quantity - NVL (poll.quantity_received, 0) > 0
                     AND pol.item_id = p_inventory_item_id
                     AND poll.ship_to_organization_id = p_organization_id
                     and POH.VENDOR_ID = POV.VENDOR_ID);
                     
    --query to get internal receipt qty---

    select  SUM(OEL.ORDERED_QUANTITY-NVL(OEL.FULFILLED_QUANTITY,0)) 
    into l_internal_receipt_qty
    from
        APPS.OE_ORDER_LINES_ALL             OEL,
        apps.OE_ORDER_HEADERS_ALL           OEH,
        APPS.PO_REQUISITION_HEADERS_ALL     PRH
    where OEH.HEADER_ID        = OEL.HEADER_ID
    and OEH.SOURCE_DOCUMENT_ID = PRh.REQUISITION_HEADER_ID
    and OEL.SOURCE_TYPE_CODE   = 'INTERNAL'
    and OEL.FLOW_STATUS_CODE   ='CLOSED'
    and OEL.INVENTORY_ITEM_ID     =  P_INVENTORY_ITEM_ID
    AND OeL.SHIP_FROM_ORG_ID      =  P_ORGANIZATION_ID
    and not exists(select 1 
                    from APPS.RCV_TRANSACTIONS RT,
                          APPS.PO_REQUISITION_LINES_ALL PRL
                    where RT.SOURCE_DOCUMENT_CODE     ='REQ'
                    and   RT.REQUISITION_LINE_ID      = PRL.REQUISITION_LINE_ID
                    and   PRL.REQUISITION_HEADER_ID   = PRH.REQUISITION_HEADER_ID
                    );
--how ROP and Min-Max get the WIP Demand (If the item is a component on a work order).  

      select SUM(O.REQUIRED_QUANTITY - O.QUANTITY_ISSUED)
      into L_COMPONENT_QTY
         FROM wip_discrete_jobs d,
              wip_requirement_operations o
         WHERE o.wip_entity_id     = d.wip_entity_id
         and O.ORGANIZATION_ID   = D.ORGANIZATION_ID
         AND d.organization_id   = P_ORGANIZATION_ID
         and O.INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID
         AND o.date_required    <= sysdate
         AND o.required_quantity >= o.quantity_issued
         AND o.operation_seq_num > 0
         AND d.status_type in (1,3,4,6)
         and O.WIP_SUPPLY_TYPE not in (5,6)
       --  AND nvl(o.supply_subinventory,1) = decode(:subinv,NULL,nvl(o.supply_subinventory,1),:subinv)
         AND  NOT EXISTS(SELECT/*+ index(mtl MTL_DEMAND_N12)*/  wip.wip_entity_id
                      FROM wip_so_allocations wip, mtl_demand mtl
                      WHERE wip_entity_id = o.wip_entity_id
                      AND wip.organization_id = P_ORGANIZATION_ID
                      AND wip.organization_id = mtl.organization_id
                      AND wip.demand_source_header_id = mtl.demand_source_header_id
                      AND wip.demand_source_line = mtl.demand_source_line
                      and WIP.DEMAND_SOURCE_DELIVERY = MTL.DEMAND_SOURCE_DELIVERY
                      AND mtl.inventory_item_id = P_INVENTORY_ITEM_ID);
--how ROP and Min-Max get WIP Supply (If it's an assembly item on a work order).

        SELECT SUM(NVL(start_quantity,0)
               - NVL(quantity_completed,0)
               - NVL(QUANTITY_SCRAPPED,0))
         into l_assembly_qy
         FROM wip_discrete_jobs
         WHERE organization_id = P_ORGANIZATION_ID
         AND primary_item_id =P_INVENTORY_ITEM_ID
         AND status_type in (1,3,4,6)
         and JOB_TYPE in (1,3)
         and SCHEDULED_COMPLETION_DATE <= TO_DATE(TO_CHAR(sysdate),'DD-MON-RR');
   
      
      L_ONORDER_QTY:= NVL(L_OPEN_QTY,0)+NVL(L_INTERNAL_RECEIPT_QTY,0)+nvl(L_COMPONENT_QTY,0)+nvl(l_assembly_qy,0);
      
      return l_onorder_qty;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;


function get_isr_open_req_qty (P_INVENTORY_ITEM_ID number,P_ORGANIZATION_ID number,p_req_type varchar2) return number
is
l_open_qty number;
begin


       /* SELECT SUM(prdl.req_line_quantity) QTY
        into l_open_qty
        from  po_requisition_headers prh,
              po_req_distributions prdl,
              po_requisition_lines prl              
        where prh.requisition_header_id = prl.requisition_header_id  
        AND prdl.requisition_line_id=prl.requisition_line_id
        and item_id=p_inventory_item_id
        and nvl(prh.cancel_flag,'N')='N'
        and nvl(prl.cancel_flag,'N')='N'
        and nvl(prl.closed_code,'OPEN')='OPEN'
        AND prh.authorization_status = 'APPROVED'
        and destination_organization_id=p_organization_id
        AND not exists (select 1
                        from po_action_history pah 
                        WHERE  pah.object_id = prh.requisition_header_id
                          AND pah.object_type_code  = 'REQUISITION'
                          AND pah.action_code       = 'CANCEL'
                       )
        and not exists (select 1
                        from po_distributions pod
                        WHERE POD.REQ_DISTRIBUTION_ID=PRDL.DISTRIBUTION_ID
                       )
       and not exists(select 1 from oe_order_headers oh
                       where order_type_id = 1011                       
                       and oh.source_document_id = prl.REQUISITION_HEADER_ID);*/
                       
        select sum(qty) 
        into l_open_qty
        from
        (SELECT NVL(SUM(to_org_primary_quantity), 0) QTY          
        FROM apps.mtl_supply sup,
             apps.po_requisition_headers_all  prh
        WHERE sup.supply_type_code            IN ('REQ')
        AND sup.destination_type_code          = 'INVENTORY'
        AND sup.to_organization_id             = p_organization_id
        AND sup.item_id                        = p_inventory_item_id
        AND prh.requisition_header_id          = sup.req_header_id
        AND p_req_type                         <> 'INVENTORY'
        --AND (p_req_type ='BOTH' OR  prh.type_lookup_code = p_req_type)
        AND (NVL(sup.from_organization_id,-1) <> p_organization_id
        OR (sup.from_organization_id           = p_organization_id
        AND EXISTS
          (SELECT 'x'
          FROM apps.mtl_secondary_inventories sub1
          WHERE sub1.organization_id        = sup.from_organization_id
          AND sub1.secondary_inventory_name = sup.from_subinventory
          AND sub1.availability_type       <> 1
          ) ) )
        AND (sup.to_subinventory IS NULL
        OR (EXISTS
          (SELECT 'x'
          FROM apps.mtl_secondary_inventories sub2
          WHERE sub2.secondary_inventory_name = sup.to_subinventory
          AND sub2.organization_id            = sup.to_organization_id
          AND sub2.availability_type          = DECODE(2, 1,sub2.availability_type, 1)
          ) ) )
        AND( (p_req_type IN ('INVENTORY','VENDOR','BOTH') AND  NOT EXISTS
          (SELECT 'X'
          FROM apps.OE_DROP_SHIP_SOURCES ODSS
          WHERE DECODE(sup.PO_HEADER_ID, NULL, sup.REQ_LINE_ID, sup.PO_LINE_LOCATION_ID) = DECODE(sup.PO_HEADER_ID,NULL, ODSS.REQUISITION_LINE_ID, ODSS.LINE_LOCATION_ID)
          )) OR (p_req_type ='DIRECT' AND EXISTS
          (SELECT 'X'
          FROM apps.OE_DROP_SHIP_SOURCES ODSS
          WHERE DECODE(sup.PO_HEADER_ID, NULL, sup.REQ_LINE_ID, sup.PO_LINE_LOCATION_ID) = DECODE(sup.PO_HEADER_ID,NULL, ODSS.REQUISITION_LINE_ID, ODSS.LINE_LOCATION_ID)
          )))
        AND (sup.po_line_location_id IS NULL
        OR EXISTS
          (SELECT 'x'
          FROM apps.po_line_locations_all lilo
          WHERE lilo.line_location_id = sup.po_line_location_id
          AND NVL(lilo.vmi_flag,'N')  = 'N'
          ) )
        AND (sup.req_line_id IS NULL
        OR EXISTS
          (SELECT 'x'
          FROM apps.po_requisition_lines_all prl
          WHERE prl.requisition_line_id = sup.req_line_id
           AND  prl.line_location_id is null
           AND (p_req_type ='BOTH' OR p_req_type ='DIRECT' OR  prl.source_type_code = p_req_type)
           AND NVL(prl.vmi_flag,'N')     = 'N'
          ) )
     UNION 
        SELECT nvl(SUM(prl.quantity),0) QTY
          --INTO l_open_qty
        FROM po_requisition_headers_all prh,
          po_requisition_lines_all prl
        WHERE prh.requisition_header_id  = prl.requisition_header_id
        AND item_id                      = p_inventory_item_id
        AND NVL(prh.cancel_flag,'N')     = 'N'
        AND NVL(prl.cancel_flag,'N')     = 'N'
        AND NVL(prl.closed_code,'OPEN' ) = 'OPEN'
        AND prh.authorization_status     = 'INCOMPLETE'
        AND destination_organization_id  = p_organization_id
        AND p_req_type                   = 'INVENTORY'
        AND source_type_code             = 'INVENTORY'
        AND NOT EXISTS
          (SELECT 1
          FROM po_action_history pah
          WHERE pah.object_id      = prh.requisition_header_id
          AND pah.object_type_code = 'REQUISITION'
          AND pah.action_code      = 'CANCEL'
          )
     );     
                       

   --   and pov.segment1                 = Get_Vendor_Number(p_inventory_item_id,p_organization_id)


  Return l_open_qty;

      exception when others then
      return  0;

end;

function get_isr_avail_qty (p_inventory_item_id number,p_organization_id number) return number
is
l_demand_qty number:=0;
l_avail_qty  number;
BEGIN
--This is code is commneted by srinivas--
     /* select sum(ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))
      into  l_demand_qty
      from  oe_order_lines_all ol
      where OL.INVENTORY_ITEM_ID     = P_INVENTORY_ITEM_ID
      and   OL.SHIP_FROM_ORG_ID      = P_ORGANIZATION_ID
      and   (ol.ordered_quantity-nvl(ol.fulfilled_quantity,0))>0
      and   ol.flow_status_code not  in ('CANCELLED','CLOSED');*/
      
      --The following code added by srinivas---
      
  SELECT NVL(SUM(MR.RESERVATION_QUANTITY),0)
        into  l_demand_qty
      FROM    --OE_ORDER_LINES OL,
             -- MTL_SALES_ORDERS MSO,
              MTL_RESERVATIONS MR
      WHERE MR.INVENTORY_ITEM_ID      = P_INVENTORY_ITEM_ID
        AND   MR.ORGANIZATION_ID        = P_ORGANIZATION_ID;
       -- AND   MR.DEMAND_SOURCE_LINE_ID  = OL.LINE_ID
--        AND  ol.flow_status_CODE not in ('CLOSED')
       -- AND   MR.DEMAND_SOURCE_HEADER_ID=MSO.SALES_ORDER_ID;

       l_avail_qty :=(xxeis.eis_po_xxwc_isr_util_qa_pkg.GET_PLANNING_QUANTITY(2,1,P_ORGANIZATION_ID,NULL,P_INVENTORY_ITEM_ID)-nvl(l_demand_qty,0));
     --l_avail_qty:=(get_onhand_inv(p_inventory_item_id,p_organization_id)-nvl(l_demand_qty,0));
     return l_avail_qty;
    exception when no_data_found then
     return  l_avail_qty;

end;

function get_isr_ss_cnt (p_inventory_item_id number,p_organization_id number) return number
is
l_ss_item_cnt number;
begin

      select count(*)
      into  l_ss_item_cnt
      from  mtl_system_items_kfv msi
      where msi.inventory_item_id        = p_inventory_item_id
      and   msi.source_organization_id   = p_organization_id   ;

  Return l_ss_item_cnt;

      exception when others then
    return  null;

end;


function get_isr_sourcing_rule (p_inventory_item_id number,p_organization_id number) return varchar2
is
l_sr_name varchar2(500);
begin
      select max(msr.sourcing_rule_name )
      into l_sr_name
    from mrp_sr_assignments  ass,
         mrp_sr_receipt_org rco,
         mrp_sr_source_org sso,
         mrp_sourcing_rules msr,
         po_vendors pov
    where 1=1
    and ass.inventory_item_id(+)=p_inventory_item_id
    and ass.organization_id(+)=p_organization_id
    and rco.sourcing_rule_id(+)=ass.sourcing_rule_id
    and sso.sr_receipt_id(+)=rco.sr_receipt_id
    and msr.sourcing_rule_id =ass.sourcing_rule_id
    and pov.vendor_id(+)=sso.vendor_id;

    return  l_sr_name;
    EXCEPTION
 when others then
   return null;
end;

function get_isr_ss (p_inventory_item_id number,p_organization_id number) return number
is
l_ss  number;
begin
     select mst.safety_stock_quantity
     into l_ss
    from mtl_safety_stocks_view mst,org_acct_periods oap,
    org_organization_definitions ood
    where ood.organization_id=oap.organization_id
    and inventory_item_id= p_inventory_item_id
    and mst.organization_id=p_organization_id
    and mst.organization_id=oap.organization_id
    and  trunc(mst.effectivity_date) between period_start_date
    and nvl(period_close_date,sysdate)
        and  TRUNC(sysdate) between PERIOD_START_DATE
    and NVL(PERIOD_CLOSE_DATE,sysdate);

    return l_ss;

EXCEPTION
 when others then
   return null;
end;

function GET_INT_REQ_SO_QTY  (P_INVENTORY_ITEM_ID number,P_ORGANIZATION_ID number)
 RETURN NUMBER IS 

L_INTER_SALES_ORDER_QTY NUMBER;
L_INTERNAL_REQ_QTY      NUMBER;
L_BOOKED_QTY            NUMBER;
L_AVIALBLE_QTY          NUMBER;
l_avialble_qty2         number;

BEGIN

      
 --query to get internal sales order qty--  
 
   /*   SELECT   SUM(OEL.ORDERED_QUANTITY-NVL(OL.FULFILLED_QUANTITY,0))
      into L_INTER_SALES_ORDER_QTY
FROM  OE_ORDER_LINES         OEL,
      OE_ORDER_HEADERS        OEH
     -- PO_REQUISITION_HEADERS  PORH,
     -- PO_REQUISITION_LINES    PORL
  where OEH.HEADER_ID               = OEL.HEADER_ID
 -- AND OEL.SOURCE_DOCUMENT_ID        = PORH.REQUISITION_HEADER_ID
--  AND OEL.SOURCE_DOCUMENT_LINE_ID   = PORL.REQUISITION_LINE_ID
 -- AND PORH.REQUISITION_HEADER_ID    = PORL.REQUISITION_HEADER_ID
  and OEL.SOURCE_TYPE_CODE          ='INTERNAL'
  and oeh. order_type_id = 1011   
 -- and PORL.SOURCE_TYPE_CODE         ='INVENTORY'
--  AND OEL.ORDER_SOURCE_ID           = 10             --order_source_id for 'Internal'
--AND oel.orig_sys_document_ref = Int_Req_num'
 -- and OEL.ORG_ID                    = PORH.ORG_ID
  and OEL.INVENTORY_ITEM_ID          = P_INVENTORY_ITEM_ID
  and OEL.SHIP_FROM_ORG_ID           = P_ORGANIZATION_ID
--  and PORH.TRANSFERRED_TO_OE_FLAG        = 'Y'
  and OL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED')
 and  exists(select 1 
                      from PO_REQUISITION_LINES prl
                       where  oh.source_document_id = prl.REQUISITION_HEADER_ID);*/
         select SUM(OEL.ORDERED_QUANTITY - NVL(OEL.FULFILLED_QUANTITY,0)- NVL(mr.reservation_quantity , 0))
          into  L_INTER_SALES_ORDER_QTY
          from  OE_ORDER_LINES_ALL         OEL,
                OE_ORDER_HEADERS_ALL        OEH,
                MTL_SALES_ORDERS MSO,
                MTL_RESERVATIONS MR
         where OEH.HEADER_ID                = OEL.HEADER_ID
          and OEL.SOURCE_TYPE_CODE          = 'INTERNAL'
          and OEH. ORDER_TYPE_ID            = 1011   
          and MR.INVENTORY_ITEM_ID(+)       = OEL.INVENTORY_ITEM_ID
          and MR.ORGANIZATION_ID (+)        = OEL.SHIP_FROM_ORG_ID
          AND MR.DEMAND_SOURCE_LINE_ID(+)   = OEL.LINE_ID
          and MSO.SALES_ORDER_ID(+)         = MR.DEMAND_SOURCE_HEADER_ID
         -- and OEL.ORDER_SOURCE_ID           = 10 
          and OEL.INVENTORY_ITEM_ID         = P_INVENTORY_ITEM_ID
          and OEL.SHIP_FROM_ORG_ID          = P_ORGANIZATION_ID
          and OeL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED')
          and  exists(select 1 
                              from PO_REQUISITION_LINES_ALL PRL
                               where  oeh.source_document_id = prl.REQUISITION_HEADER_ID);
  
  --query to get internal req which are not interfaced for sales orders ---
  
  
  SELECT SUM(PRL.QUANTITY)
  into L_INTERNAL_REQ_QTY
  from  PO_REQUISITION_LINES      PRL,
        PO_REQUISITION_HEADERS    PRH
  where PRH.REQUISITION_HEADER_ID   = PRL.REQUISITION_HEADER_ID
  AND PRH.TYPE_LOOKUP_CODE          = 'INTERNAL'
  AND PRL.SOURCE_TYPE_CODE          = 'INVENTORY'
  AND PRL.ITEM_ID                   = P_INVENTORY_ITEM_ID
  and PRL.SOURCE_ORGANIZATION_ID    = P_ORGANIZATION_ID
  AND PRH.TRANSFERRED_TO_OE_FLAG    = 'N';
  
  --Query to get all booked sales order qty's exculding the return orders and open quotes and internale sales orders
  
  SELECT  SUM(OL.ORDERED_QUANTITY - NVL(OL.FULFILLED_QUANTITY,0)- NVL(mr.reservation_quantity , 0))
      INTO  L_BOOKED_QTY
      FROM  OE_ORDER_LINES OL,
            OE_ORDER_HEADERS OH,
            MTL_SALES_ORDERS MSO,
            MTL_RESERVATIONS MR
      where OH.HEADER_ID                  =  OL.HEADER_ID
      and OL.INVENTORY_ITEM_ID          =  P_INVENTORY_ITEM_ID
      and OL.SHIP_FROM_ORG_ID           =  P_ORGANIZATION_ID
      and MR.INVENTORY_ITEM_ID(+)       = OL.INVENTORY_ITEM_ID
      and MR.ORGANIZATION_ID (+)        = OL.SHIP_FROM_ORG_ID
      AND MR.DEMAND_SOURCE_LINE_ID(+)   = OL.LINE_ID
      and MSO.SALES_ORDER_ID(+)         = MR.DEMAND_SOURCE_HEADER_ID
      AND (OL.ORDERED_QUANTITY-NVL(OL.FULFILLED_QUANTITY,0))>0
      AND OH.FLOW_STATUS_CODE IN ('BOOKED')
      and OH.TRANSACTION_PHASE_CODE     <> 'N'--NOT ALLOWED TO OPEN QUOTES
      and OL.LINE_CATEGORY_CODE         <>'RETURN' --NOT ALLOWED TO RETURN oRDERS
      and OL.SOURCE_TYPE_CODE           <> 'INTERNAL'--Not allowed to Internale Sales Orders
      --and OH. ORDER_TYPE_ID            <> 1012   --Not allowed to Internale Sales Orders
      --and OL.ORDER_SOURCE_ID           <> 10 --Not allowed to Internale Sales Orders
      and OL.FLOW_STATUS_CODE not  in ('CANCELLED','CLOSED');
  
  L_AVIALBLE_QTY := GET_ISR_AVAIL_QTY (P_INVENTORY_ITEM_ID ,P_ORGANIZATION_ID );

 L_AVIALBLE_QTY2 := L_AVIALBLE_QTY-(nvl(L_BOOKED_QTY,0)+nvl(L_INTER_SALES_ORDER_QTY,0)+nvl(L_INTERNAL_REQ_QTY,0));
 
 RETURN L_AVIALBLE_QTY2;
 
 EXCEPTION WHEN OTHERS THEN
 L_AVIALBLE_QTY2 :=0;
 end;

function Get_isr_rpt_dc_mod_sub  return varchar2
is
begin
return G_isr_rpt_dc_mod_sub;
end;

function GET_ONHAND_INV (P_INVENTORY_ITEM_ID number, P_ORGANIZATION_ID number)
Return Number
is
 l_Onhand_Inv Number;
BEGIN

   select nvl(sum(moq.transaction_quantity),0)
    Into  L_Onhand_Inv
    from mtl_onhand_quantities_detail moq
    Where Inventory_Item_Id          =P_Inventory_Item_Id
    and organization_id              =p_organization_id;

  return l_Onhand_Inv;
 exception
 WHEN OTHERS THEN
   return 0;
END;

FUNCTION Get_primary_bin_loc (P_INVENTORY_ITEM_ID NUMBER, P_ORGANIZATION_ID NUMBER)
RETURN VARCHAR2
IS
 l_seg VARCHAR2(5000);
BEGIN

    Select mil.Segment1
    INTO  L_SEG
    FROM MTL_ITEM_LOCATIONS_KFV MIL,
         MTL_SECONDARY_LOCATORS MSL
    WHERE MSL.INVENTORY_ITEM_ID  = P_INVENTORY_ITEM_ID
    AND MSL.ORGANIZATION_ID      =P_ORGANIZATION_ID
    AND MSL.SECONDARY_LOCATOR = MIL.INVENTORY_LOCATION_ID
    and mil.segment1  like '1%';


  return l_seg;
 exception
 WHEN OTHERS THEN
   RETURN NULL;
end;
---Below code for Demand Qty---

    FUNCTION get_staged_qty( p_org_id          NUMBER
                           , p_subinv          VARCHAR2
                           , p_item_id         NUMBER
                           , p_order_line_id   NUMBER
                           , p_include_nonnet  NUMBER) RETURN NUMBER IS

        l_staged_qty  NUMBER := 0;

    BEGIN

        BEGIN
            --
            -- Bugfix 2333526: Need to calculate staged quantity
            -- for sub level planning.  If passed-in (planning)
            -- sub is the also the staging sub, then ignore
            -- p_include_nonnet
            --
            SELECT NVL(SUM(primary_reservation_quantity),0)
              INTO l_staged_qty
              FROM mtl_reservations
             WHERE organization_id        = p_org_id
               AND inventory_item_id      = p_item_id
               AND demand_source_line_id  = p_order_line_id
               AND demand_source_type_id  IN (2,8,12)
               AND NVL(staged_flag, 'X')  = 'Y'
               AND subinventory_code      IS NOT NULL
               AND subinventory_code     <> p_subinv; -- Bug 4313204

        EXCEPTION
            WHEN OTHERS THEN
                 l_staged_qty  := 0;
        END;

        RETURN l_staged_qty;

    END get_staged_qty;


FUNCTION get_pick_released_qty( p_org_id          NUMBER
                                  , p_subinv          VARCHAR2
                                  , p_item_id         NUMBER
                                  , p_order_line_id   NUMBER) RETURN NUMBER IS

        l_pick_released_qty  NUMBER := 0;

    BEGIN

        BEGIN
            --
            -- Move order type 3 is pick wave, source type 2 is sales order
            -- Bug 3181367 added transaction_source_type_id 8 too.
            SELECT NVL(sum(mtrl.quantity - NVL(mtrl.quantity_delivered,0)),0)
              INTO l_pick_released_qty
              FROM mtl_txn_request_headers  mtrh,
                   mtl_txn_request_lines    mtrl
             WHERE mtrh.move_order_type            = 3
               AND mtrh.header_id                  = mtrl.header_id
               AND mtrl.organization_id            = p_org_id
               AND mtrl.inventory_item_id          = p_item_id
               AND mtrl.from_subinventory_code     = p_subinv
               AND mtrl.txn_source_line_id         = p_order_line_id
               AND mtrl.transaction_source_type_id in (2,8)
               AND mtrl.line_status NOT IN (5,6);

        EXCEPTION
            WHEN OTHERS THEN
                 l_pick_released_qty  := 0;
        END;

        RETURN l_pick_released_qty;

    END get_pick_released_qty;
    
       FUNCTION get_loaded_qty(    p_org_id          NUMBER
                              , p_subinv          VARCHAR2
                              , p_level           NUMBER
                              , p_item_id         NUMBER
                              , p_net_rsv         NUMBER
                              , p_net_unrsv       NUMBER ) RETURN NUMBER IS

      CURSOR c_loaded_quantities_v IS
           SELECT SUM(quantity) FROM wms_loaded_quantities_v
           WHERE  inventory_item_id = p_item_id
           AND subinventory_code = nvl(p_subinv , subinventory_code )
           AND organization_id = p_org_id;

      l_loaded_qty NUMBER := 0 ;

   BEGIN
     --The loaded quantity will be calculated only if the report is ran with
     --parameters "reserved demand=>No , unreserved demand=>No".
     --If the parameters are "yes", the MTL_RESERVATIONS or MMTT will be accounted for this qty.

     IF ( p_net_rsv = 2 and  p_net_unrsv = 2 )THEN

      OPEN c_loaded_quantities_v ;
      FETCH c_loaded_quantities_v INTO l_loaded_qty;
      CLOSE c_loaded_quantities_v;
     END IF;



     return ( l_loaded_qty ) ;
   EXCEPTION
     WHEN OTHERS THEN
           
               fnd_file.put_line(fnd_file.log,'The error is '||SQLCODE||SQLERRM);
           
             RAISE;
   END get_loaded_qty;
   
   
   FUNCTION get_shipped_qty( p_organization_id    IN      NUMBER
                            , p_inventory_item_id  IN      NUMBER
                            , p_order_line_id      IN      NUMBER) RETURN NUMBER IS

        l_shipped_qty NUMBER := 0;

    BEGIN

        --
        -- Only look at source types 2 and 8 (sales orders, internal orders)
        --
        SELECT NVL(SUM(primary_quantity),0)
          INTO l_shipped_qty
          FROM mtl_material_transactions
         WHERE transaction_action_id = 1
           AND source_line_id        = p_order_line_id
           AND organization_id       = p_organization_id
           AND inventory_item_id     = p_inventory_item_id
           AND transaction_source_type_id in (2,8);

        IF l_shipped_qty IS NULL THEN
           l_shipped_qty := 0;
        ELSE
           l_shipped_qty := -1 * l_shipped_qty;
        END IF;

        RETURN l_shipped_qty;

    end GET_SHIPPED_QTY;
    

    


    FUNCTION get_supply_qty( p_org_id              NUMBER
                           , p_subinv              VARCHAR2
                           , p_item_id             NUMBER
                           , p_postproc_lead_time  NUMBER
                           , p_cal_code            VARCHAR2
                           , p_except_id           NUMBER
                           , p_level               NUMBER
                           , p_s_cutoff            DATE
			                  , p_include_po          NUMBER
                           , p_include_mo          NUMBER
			                  , p_vmi_enabled         VARCHAR2
                           , p_include_nonnet      NUMBER
                           , p_include_wip         NUMBER
                           , p_include_if          NUMBER
                           /* nsinghi MIN-MAX INVCONV start */
                           , p_process_org         VARCHAR2
                           /* nsinghi MIN-MAX INVCONV end */
                           ) RETURN NUMBER IS

        l_qty          NUMBER := 0;
        l_total        NUMBER := 0;
        L_PUOM         varchar2(3);
     --   G_TRACE_ON     number:=1;


   -- supply records. Modified the query to consider receipt_date for
   -- records with supply type code 'SHIPMENT' and 'RECEIVING'

        l_stmt         VARCHAR2(4000) :=
              ' SELECT NVL(sum(to_org_primary_quantity), 0)
                FROM mtl_supply         sup
               WHERE sup.supply_type_code IN (''PO'',''REQ'',''SHIPMENT'',''RECEIVING'')
                 AND sup.destination_type_code  = ''INVENTORY''
                 AND sup.to_organization_id     = :l_org_id
                 AND sup.item_id                = :l_item_id
                 AND (NVL(sup.from_organization_id,-1) <> :l_org_id
                      OR (sup.from_organization_id      = :l_org_id
                          AND ((:l_include_nonnet       = 2
                                AND
                                EXISTS (SELECT ''x''
                                          FROM mtl_secondary_inventories sub1
                                         WHERE sub1.organization_id          = sup.from_organization_id
                                           AND sub1.secondary_inventory_name = sup.from_subinventory
                                           AND sub1.availability_type       <> 1
                                       )
                               )
                               OR :l_level = 2
                              )
                         )
                     )
                 AND (sup.to_subinventory IS NULL
                      OR
                      (EXISTS (SELECT ''x''
                                 FROM mtl_secondary_inventories sub2
                                WHERE sub2.secondary_inventory_name = sup.to_subinventory
                                  AND sub2.organization_id          = sup.to_organization_id
                                  AND sub2.availability_type        = decode(:l_include_nonnet,
                                                                             1,sub2.availability_type,
                                                                             1)
                              )
                      )
                      OR :l_level = 2
                     )
                 AND (:l_level = 1 OR to_subinventory = :l_subinv)
-- Bug 5041763 Not considering supply from drop ship orders
                    AND NOT EXISTS (SELECT ''X'' FROM apps.OE_DROP_SHIP_SOURCES ODSS
                            WHERE   DECODE(sup.PO_HEADER_ID, NULL, sup.REQ_LINE_ID, sup.PO_LINE_LOCATION_ID) =
                                    DECODE(sup.PO_HEADER_ID,NULL, ODSS.REQUISITION_LINE_ID, ODSS.LINE_LOCATION_ID)) ';
                                    
                                    
            /*' SELECT NVL(sum(to_org_primary_quantity), 0)
                FROM mtl_supply         sup
                  , apps.bom_calendar_dates c
                  , apps.bom_calendar_dates c1
               WHERE sup.supply_type_code IN (''PO'',''REQ'',''SHIPMENT'',''RECEIVING'')
                 AND sup.destination_type_code  = ''INVENTORY''
                 AND sup.to_organization_id     = :l_org_id
                 AND sup.item_id                = :l_item_id
                 AND c.calendar_code            = :l_cal_code
                 AND c.exception_set_id         = :l_except_id
                 AND c.calendar_date            = trunc(decode(sup.supply_type_code, ''SHIPMENT'', sup.receipt_date, ''RECEIVING'', sup.receipt_date,nvl(sup.need_by_date, sup.receipt_date)))
                 AND c1.calendar_code           = c.calendar_code
                 AND c1.exception_set_id        = c.exception_set_id
                 AND c1.seq_num                 = (c.next_seq_num + trunc(:l_postproc_lead_time))
                 AND c1.calendar_date   <= :l_s_cutoff + 0.99999  /* bug no 6009682 */
                /* AND (NVL(sup.from_organization_id,-1) <> :l_org_id
                      OR (sup.from_organization_id      = :l_org_id
                          AND ((:l_include_nonnet       = 2
                                AND
                                EXISTS (SELECT ''x''
                                          FROM mtl_secondary_inventories sub1
                                         WHERE sub1.organization_id          = sup.from_organization_id
                                           AND sub1.secondary_inventory_name = sup.from_subinventory
                                           AND sub1.availability_type       <> 1
                                       )
                               )
                               OR :l_level = 2
                              )
                         )
                     )
                 AND (sup.to_subinventory IS NULL
                      OR
                      (EXISTS (SELECT ''x''
                                 FROM mtl_secondary_inventories sub2
                                WHERE sub2.secondary_inventory_name = sup.to_subinventory
                                  AND sub2.organization_id          = sup.to_organization_id
                                  AND sub2.availability_type        = decode(:l_include_nonnet,
                                                                             1,sub2.availability_type,
                                                                             1)
                              )
                      )
                      OR :l_level = 2
                     )
                 AND (:l_level = 1 OR to_subinventory = :l_subinv)
-- Bug 5041763 Not considering supply from drop ship orders
                    AND NOT EXISTS (SELECT ''X'' FROM apps.OE_DROP_SHIP_SOURCES ODSS
                            WHERE   DECODE(sup.PO_HEADER_ID, NULL, sup.REQ_LINE_ID, sup.PO_LINE_LOCATION_ID) =
                                    DECODE(sup.PO_HEADER_ID,NULL, ODSS.REQUISITION_LINE_ID, ODSS.LINE_LOCATION_ID)) '; */



      l_vmi_stmt     VARCHAR2(2000) :=
            ' AND (sup.po_line_location_id IS NULL
                   OR EXISTS (SELECT ''x''
                                FROM po_line_locations_all lilo
                               WHERE lilo.line_location_id    = sup.po_line_location_id
                                 AND NVL(lilo.vmi_flag,''N'') = ''N''
                             )
                  )
              AND (sup.req_line_id IS NULL
                   OR EXISTS (SELECT ''x''
                                FROM po_requisition_lines_all prl
                               WHERE prl.requisition_line_id = sup.req_line_id
                                 AND NVL(prl.vmi_flag,''N'') = ''N''
                             )
                  )';


        TYPE c_po_sup_curtype IS REF CURSOR;
        c_po_qty c_po_sup_curtype;

    BEGIN
        
            /*dbms_output.put_line('sbitrap_org_id: '               || to_char(p_org_id)         ||
                        ', p_subinv: '             || p_subinv                  ||
                        ', p_item_id: '            || to_char(p_item_id)        ||
                        ', p_postproc_lead_time: ' || to_char(p_postproc_lead_time) ||
                        ', p_cal_code: '           || p_cal_code                ||
                        ', p_except_id: '          || to_char(p_except_id)      ||
                        ', p_level: '              || to_char(p_level)          ||
                        ', p_s_cutoff: '           || to_char(p_s_cutoff, 'DD-MON-YYYY HH24:MI:SS') ||
                        ', p_include_po: '         || to_char(p_include_po)     ||
                        ', p_include_mo: '         || to_char(p_include_mo)     ||
                        ', p_include_nonnet: '     || to_char(p_include_nonnet) ||
                        ', p_include_wip: '        || to_char(p_include_wip)    ||
                        ', p_include_if: '         || to_char(p_include_if)
                        , 'get_supply_qty'
                        , 9);
        */

        l_total := 0;

        --
        -- MTL_SUPPLY
        --
 
        IF p_include_po = 1 THEN
	   IF (p_vmi_enabled = 'Y') and (p_level= 1) then
	      OPEN c_po_qty FOR l_stmt || l_vmi_stmt
		USING
                p_org_id, p_item_id, p_org_id, p_org_id, p_include_nonnet,
                p_level, p_include_nonnet, p_level, p_level, p_subinv;
   
	    else
	      OPEN c_po_qty FOR l_stmt
		USING
                p_org_id, p_item_id, p_org_id, p_org_id, p_include_nonnet,
                p_level, p_include_nonnet, p_level, p_level, p_subinv;
	   END IF;

            FETCH c_po_qty into l_qty;
              dbms_output.put_line('the main query is '||l_stmt ||l_vmi_stmt);
            CLOSE c_po_qty;
         
            dbms_output.put_line('Supply from mtl_supply: ' || to_char(l_qty)
                        );
          

            l_total := l_total + l_qty;

        END IF;
        --
        -- Take into account the quantity for which a move order
        -- has already been created assuming that the move order
        -- can be created only within the same org
        
        IF (p_level = 2 and p_include_mo = 1) THEN

            SELECT NVL(SUM(mtrl.quantity - NVL(mtrl.quantity_delivered,0)),0)
                                                   
            INTO l_qty
            FROM mtl_transaction_types  mtt,
                 mtl_txn_request_lines  mtrl
            WHERE mtt.transaction_action_id IN (2,28)
                AND mtt.transaction_type_id   = mtrl.transaction_type_id
                AND mtrl.organization_id      = p_org_id
                AND mtrl.inventory_item_id    = p_item_id
                AND mtrl.to_subinventory_code = p_subinv
                AND mtrl.line_status IN (3,7) ;
                --AND mtrl.date_required      <= p_s_cutoff + 0.99999;    

         
           dbms_output.put_line('Supply from move orders: ' || to_char(l_qty)
                      );
         
            l_total := l_total + l_qty;
        END IF;

        --
        -- Supply FROM WIP discrete job is to be included at Org Level Planning Only
        --
        IF p_level = 1 AND p_include_wip = 1
        THEN


/* Here check will need to be made if the org is Process org or Discrete org. */

           IF p_process_org = 'Y' THEN
              SELECT
                 SUM ( NVL((NVL(d.wip_plan_qty, d.plan_qty) - d.actual_qty), 0) *
                     (original_primary_qty/original_qty))
              INTO l_qty
              FROM   gme_material_details d
               ,      gme_batch_header     h
               WHERE  h.batch_type IN (0,10)
               AND    h.batch_status IN (1,2)
               AND    h.batch_id = d.batch_id
               AND    d.inventory_item_id = p_item_id
               AND    d.organization_id = p_org_id
               AND    NVL(d.original_qty, 0) <> 0
               --AND    d.material_requirement_date <= p_s_cutoff
               AND    d.line_type > 0;

               dbms_output.put_line('Supply from OPM Batches : ' || to_char(l_qty)
                         );
         
              l_total := l_total + NVL(l_qty,0);
              
              SELECT sum(NVL(start_quantity,0)
                         - NVL(quantity_completed,0)
                         - NVL(quantity_scrapped,0))
                INTO l_qty
                FROM wip_discrete_jobs
               WHERE organization_id = p_org_id
                 AND primary_item_id = p_item_id
                 AND job_type in (1,3)
                 AND status_type IN (1,3,4,6)
                 --Bug 2647862
                -- AND scheduled_completion_date <= p_s_cutoff + 0.99999 /* bug no 6009682 */
                 AND (NVL(start_quantity,0) - NVL(quantity_completed,0)
                                            - NVL(quantity_scrapped,0)) > 0;

                l_total := l_total + NVL(l_qty,0);
                
                   --
              -- WIP REPETITIVE JOBS to be included at Org Level Planning Only
              --
              SELECT SUM(daily_production_rate *
                         GREATEST(0, LEAST(processing_work_days,
                                            p_s_cutoff +90 - first_unit_completion_date
                                          )
                                 )
                         - quantity_completed)
                INTO l_qty
                FROM wip_repetitive_schedules wrs,
                     wip_repetitive_items wri
               WHERE wrs.organization_id = p_org_id
                 AND wrs.status_type IN (1,3,4,6)
                 AND wri.organization_id = p_org_id
                 AND wri.primary_item_id = p_item_id
                 AND wri.wip_entity_id   = wrs.wip_entity_id
                 AND wri.line_id         = wrs.line_id
                 AND (daily_production_rate *
                      GREATEST(0, LEAST(processing_work_days,
                                        p_s_cutoff + 90 - first_unit_completion_date
                                       )
                              )
                      - quantity_completed) > 0 ;

      
              dbms_output.put_line('Supply from WIP repetitive schedules: ' || to_char(l_qty)
                             );
           

              l_total := l_total + NVL(l_qty,0);

           ELSE
/* nsinghi MIN-MAX INVCONV end */

              SELECT sum(NVL(start_quantity,0)
                         - NVL(quantity_completed,0)
                         - NVL(quantity_scrapped,0))
                INTO l_qty
                FROM wip_discrete_jobs
               WHERE organization_id = p_org_id
                 AND primary_item_id = p_item_id
                 AND job_type in (1,3)
                 AND status_type IN (1,3,4,6)
                 --Bug 2647862
                -- AND scheduled_completion_date <= p_s_cutoff + 0.99999 /* bug no 6009682 */
                 AND (NVL(start_quantity,0) - NVL(quantity_completed,0)
                                            - NVL(quantity_scrapped,0)) > 0;

      
           dbms_output.put_line('Supply from WIP discrete jobs: ' || to_char(l_qty)
                          );
           
              l_total := l_total + NVL(l_qty,0);

              --
              -- WIP REPETITIVE JOBS to be included at Org Level Planning Only
              --
              SELECT SUM(daily_production_rate *
                         GREATEST(0, LEAST(processing_work_days,
                                            p_s_cutoff +90 - first_unit_completion_date
                                          )
                                 )
                         - quantity_completed)
                INTO l_qty
                FROM wip_repetitive_schedules wrs,
                     wip_repetitive_items wri
               WHERE wrs.organization_id = p_org_id
                 AND wrs.status_type IN (1,3,4,6)
                 AND wri.organization_id = p_org_id
                 AND wri.primary_item_id = p_item_id
                 AND wri.wip_entity_id   = wrs.wip_entity_id
                 AND wri.line_id         = wrs.line_id
                 AND (daily_production_rate *
                      GREATEST(0, LEAST(processing_work_days,
                                        p_s_cutoff + 90 - first_unit_completion_date
                                       )
                              )
                      - quantity_completed) > 0 ;

      
              dbms_output.put_line('Supply from WIP repetitive schedules: ' || to_char(l_qty)
                             );
           

              l_total := l_total + NVL(l_qty,0);

           END IF; /* p_process_org = 'Y' */

        END IF;

        IF (p_include_if = 2)
        THEN
            RETURN(l_total);
        END IF;

        --
        -- po_requisitions_interface_all
        --
        -- Bug: 2320752
        -- Do not include records in error status
        -- kkoothan Bug Fix 2891818
        -- Used NVL function for the condition involving process_flag
        -- so that the interface records with NULL value for process_flag
        -- are considered as Interface Supply.
        --
        /* Bug 3894347 -- Added the following section of code to consider conversion
           of quantities in po_requisitions_interface_all if uom_code is different than
           primary uom code */

        SELECT uom_code
          INTO l_puom
        FROM mtl_system_items_vl msiv , mtl_units_of_measure_vl muom
        WHERE msiv.inventory_item_id = p_item_id
          AND msiv.organization_id = p_org_id
          AND muom.unit_of_measure = msiv.primary_unit_of_measure;

        --Bug9122329, calling the function get_item_uom_code in case when uom_code is null
        --in the po_requisitions_interface_all table.

      /*  SELECT NVL(SUM(DECODE(NVL(uom_code,get_item_uom_code(unit_of_measure)),
                                L_PUOM,QUANTITY,
                                APPS.INV_CONVERT.INV_UM_CONVERT(P_ITEM_ID,null,QUANTITY,NVL(UOM_CODE,GET_ITEM_UOM_CODE(UNIT_OF_MEASURE)),L_PUOM,null,null)
                              )),0)
  
          INTO l_qty
          FROM po_requisitions_interface_all
         WHERE destination_organization_id = p_org_id
           AND item_id                     = p_item_id
           AND p_include_po                = 1
           AND (p_level = 1 or destination_subinventory = p_subinv)
           AND need_by_date               <= (trunc(p_s_cutoff) + 1 - (1/(24*60*60)))
           AND NVL(process_flag,'@@@') <> 'ERROR'
           AND (NVL(source_organization_id,-1) <> p_org_id OR
                (source_organization_id         = p_org_id AND
                 (( p_include_nonnet            = 2 AND
                   EXISTS (SELECT 'x'
                             FROM mtl_secondary_inventories sub1
                            WHERE sub1.organization_id          = source_organization_id
                              AND sub1.secondary_inventory_name = source_subinventory
                              AND sub1.availability_type       <> 1)) OR
                 p_level = 2)
               ))
           AND (destination_subinventory IS NULL OR
                EXISTS (SELECT 1
                          FROM mtl_secondary_inventories sub2
                         WHERE secondary_inventory_name = destination_subinventory
                           AND destination_subinventory = NVL(p_subinv,
                                                          destination_subinventory)
                           AND sub2.organization_id     = p_org_id
                           AND sub2.availability_type   = decode(p_include_nonnet,
                                                                 1,sub2.availability_type,1)) OR
                p_level = 2);

    
        dbms_output.put_line('Supply from po_requisitions_interface_all: ' || to_char(l_qty)
                    );*/
      
        l_total := l_total + NVL(l_qty,0);

        --
        -- WIP_JOB_SCHEDULE_INTERFACE, processed immediately, hence not included
        --
        -- Supply FROM Flow to be included in org level only
        --
        IF p_level = 1
        THEN
            SELECT SUM(NVL(planned_quantity,0)
                       - NVL(quantity_completed,0))
              INTO l_qty
              FROM wip_flow_schedules
             WHERE organization_id = p_org_id
               AND primary_item_id = p_item_id
               AND status          = 1
               AND scheduled_flag  = 1   -- Bug 3151797
                     --Bug 2647862
               --AND scheduled_completion_date <= p_s_cutoff + 0.99999 /* bug no 6009682 */
               AND (NVL(planned_quantity,0)
                    - NVL(quantity_completed,0)) > 0;

       
        dbms_output.put_line('Supply from WIP flow schedules: ' || to_char(l_qty)
                        );
       
            l_total := l_total + NVL(l_qty,0);
        END IF;

        RETURN(l_total);

    EXCEPTION
        WHEN others THEN
            IF c_po_qty%ISOPEN  THEN
               CLOSE c_po_qty;
            END IF;
          RETURN(l_total);
            dbms_output.put_line('The error is '||SQLCODE||SQLERRM);
      
            
    END get_supply_qty;
  
  
      FUNCTION get_item_uom_code (p_uom_name   VARCHAR2) RETURN VARCHAR2 IS

    l_uom_code MTL_UNITS_OF_MEASURE.UOM_CODE%type := NULL;

BEGIN

    SELECT uom_code
    INTO l_uom_code
    FROM mtl_units_of_measure_vl
    WHERE unit_of_measure = p_uom_name;

    RETURN (l_uom_code);

    EXCEPTION
        WHEN OTHERS THEN
         
           DBMS_OUTPUT.PUT_LINE('Error in  get_item_uom_code function');
          
    RAISE;

END get_item_uom_code;



FUNCTION get_demand_qty( p_org_id          NUMBER
                           , p_subinv          VARCHAR2
                           , p_level           NUMBER
                           , p_item_id         NUMBER
                           , p_d_cutoff        DATE
                           , p_include_nonnet  NUMBER
                           , p_net_rsv         NUMBER
                           , p_net_unrsv       NUMBER
                           , p_net_wip         NUMBER
                           /* nsinghi MIN-MAX INVCONV start */
                           , p_process_org     VARCHAR2
                           /* nsinghi MIN-MAX INVCONV end */
                           ) RETURN NUMBER IS

        qty                  NUMBER := 0;
        total                NUMBER := 0;
        l_total_demand_qty   NUMBER := 0;
        l_demand_qty         NUMBER := 0;
        l_total_reserve_qty  NUMBER := 0;
        l_pick_released_qty  NUMBER := 0;
        l_staged_qty         NUMBER := 0;
        l_sub_reserve_qty    NUMBER := 0;
        l_allocated_qty      NUMBER := 0;
	l_loaded_qty	     NUMBER := 0; /*Bug 6240025 */

    begin
    
    DBMS_OUTPUT.PUT_LINE('The Process start here');
           --
        -- select unreserved qty from mtl_demand for non oe rows.
        --
        if P_NET_UNRSV = 1 then
         DBMS_OUTPUT.PUT_LINE(' unreserved qty from mtl_demand for non oe rows.');
           /*4518296*/
            select sum(PRIMARY_UOM_QUANTITY - GREATEST(NVL(RESERVATION_QUANTITY,0),
                                              NVL(COMPLETED_QUANTITY,0)))
              into qty
              from mtl_demand
             WHERE RESERVATION_TYPE     = 1
               AND parent_demand_id    IS NULL
               AND ORGANIZATION_ID      = p_org_id
               and PRIMARY_UOM_QUANTITY > GREATEST(NVL(RESERVATION_QUANTITY,0),
                                                   NVL(COMPLETED_QUANTITY,0))
               and INVENTORY_ITEM_ID    = P_ITEM_ID
               --and REQUIREMENT_DATE      <= sysdate + 0.99999 /* bug no 6009682 */
               and demand_source_type not in (2,8,12)
               and (p_level      = 1 or
                    SUBINVENTORY = p_subinv)   -- Included later for ORG Level
               and (SUBINVENTORY is null or
                    p_level = 2 or
                    EXISTS (SELECT 1
                              FROM MTL_SECONDARY_INVENTORIES S
                             WHERE S.ORGANIZATION_ID          = p_org_id
                               AND S.SECONDARY_INVENTORY_NAME = SUBINVENTORY
                               AND S.availability_type        = DECODE(p_include_nonnet,
                                                                       1,
                                                                       S.availability_type,
                                                                       1)))
/* nsinghi MIN-MAX INVCONV start */
               AND (locator_id IS NULL OR
                    p_level = 2 OR
                     EXISTS (SELECT 1 FROM mtl_item_locations mil
                            WHERE mil.organization_id = p_org_id
                            AND   mil.inventory_location_id = locator_id
                            AND   mil.subinventory_code = NVL(subinventory, mil.subinventory_code)
                            AND   mil.availability_type = decode(p_include_nonnet,1,mil.availability_type,1)))
               AND (lot_number IS NULL OR
                    p_level = 2 OR
                     EXISTS (SELECT 1 FROM mtl_lot_numbers mln
                            WHERE mln.organization_id = p_org_id
                            AND   mln.lot_number = lot_number
                            AND   mln.inventory_item_id = p_item_id
                            and   MLN.AVAILABILITY_TYPE = DECODE(P_INCLUDE_NONNET,1,MLN.AVAILABILITY_TYPE,1)));
                            
             DBMS_OUTPUT.PUT_LINE(' unreserved qty from mtl_demand for non oe rows.'||qty);
             
             total := total + NVL(qty,0);
/* nsinghi MIN-MAX INVCONV end */

         /*   IF G_TRACE_ON = 1 THEN
            print_debug('Demand from mtl_demand: ' || to_char(qty), 'get_demand_qty', 9);
            END IF;
            
        END IF;*/

        --
        -- select the reserved quantity from mtl_reservations for non OE rows
        --
        if P_NET_RSV = 1 then
        dbms_output.put_line('select the reserved quantity from mtl_reservations for non OE rows ');
            select sum(PRIMARY_RESERVATION_QUANTITY)
              into qty
              from mtl_reservations
             where ORGANIZATION_ID = p_org_id
               and INVENTORY_ITEM_ID = P_ITEM_ID
               --and REQUIREMENT_DATE <= sysdate + 0.99999 /* bug no 6009682 */
               and demand_source_type_id not in (2,8,12)
               and (p_level = 1  or
                    SUBINVENTORY_CODE = p_subinv) -- Included later for ORG Level
               and (SUBINVENTORY_CODE is null or
                    p_level = 2 or
                    EXISTS (SELECT 1
                              FROM MTL_SECONDARY_INVENTORIES S
                             WHERE S.ORGANIZATION_ID = p_org_id
                               AND S.SECONDARY_INVENTORY_NAME = SUBINVENTORY_CODE
                               AND S.availability_type = DECODE(p_include_nonnet,
                                                                1,
                                                                S.availability_type,
                                                                1)))
/* nsinghi MIN-MAX INVCONV start */
               AND (locator_id IS NULL OR
                    p_level = 2 OR
                     EXISTS (SELECT 1 FROM mtl_item_locations mil
                            WHERE mil.organization_id = p_org_id
                            AND   mil.inventory_location_id = locator_id
                            AND   mil.subinventory_code = NVL(subinventory_code, mil.subinventory_code)
                            AND   mil.availability_type = decode(p_include_nonnet,1,mil.availability_type,1)))
               AND (lot_number IS NULL OR
                    p_level = 2 OR
                     EXISTS (SELECT 1 FROM mtl_lot_numbers mln
                            WHERE mln.organization_id = p_org_id
                            AND   mln.lot_number = lot_number
                            AND   mln.inventory_item_id = p_item_id
                            and   MLN.AVAILABILITY_TYPE = DECODE(P_INCLUDE_NONNET,1,MLN.AVAILABILITY_TYPE,1)));
                dbms_output.put_line('select the reserved quantity from mtl_reservations for non OE rows '||qty);
/* nsinghi MIN-MAX INVCONV end */

          /*  IF G_TRACE_ON = 1 THEN
            print_debug('Demand (reserved qty) for non OE rows in mtl_reservations: ' || to_char(qty)
                        , 'get_demand_qty'
                        , 9);
            END IF;*/
            TOTAL := TOTAL + NVL(QTY,0);
              dbms_output.put_line('select the reserved quantity from mtl_reservations for non OE rows '||TOTAL);
        END IF;

        --
        -- get the total demand which is the difference between the
        -- ordered qty. and the shipped qty.
        -- This gives the total demand including the reserved
        -- and the unreserved material.
        --
        -- Bug 2333526: For sub level planning we need to compute
        -- the staged qty.  The existing WHERE clause makes sure
        -- we only do this when the order is sourced from the
        -- planning sub: level = 1... or SUBINVENTORY = p_subinv
        --
        -- Bug 2350243: For sub level, calculate pick released
        -- (move order) qty
        --

        -- Bug 3480523, from patch I onwards schedule_ship_date is being populated
        -- with time component, hence truncating it to get the same day demand. These
        -- changes are also in mtl_reservation queries.
        if P_NET_UNRSV = 1 then
        dbms_output.put_line('get the total demand');
            select SUM( NVL(ORDERED_QUANTITY,0) -
                       nvl(XXEIS.eis_po_xxwc_isr_util_qa_pkg.get_shipped_qty(p_org_id, p_item_id, ool.line_id),0)),
                   SUM(DECODE(2,
                              2, XXEIS.EIS_PO_XXWC_ISR_UTIL_QA_PKG.GET_STAGED_QTY( P_ORG_ID
                                               --, p_subinv
                                                , ool.subinventory
                                               , p_item_id
                                               , ool.line_id
                                               , p_include_nonnet),
                              0)
                      ),
                   SUM(DECODE(2,
                              2, XXEIS.EIS_PO_XXWC_ISR_UTIL_QA_PKG.GET_PICK_RELEASED_QTY( P_ORG_ID
                                                     -- , p_subinv
                                                       , ool.subinventory
                                                      , p_item_id
                                                      , ool.line_id),
                              0)
                      )
              into l_total_demand_qty, l_staged_qty, l_pick_released_qty
              from oe_order_lines_all ool
             where ship_from_org_id = p_org_id
               and open_flag = 'Y'
               AND visible_demand_flag = 'Y'
               AND shipped_quantity is null
               and INVENTORY_ITEM_ID = p_item_id
               --and schedule_ship_date <= sysdate + 0.99999 /* bug no 6009682 */
               AND DECODE( OOL.SOURCE_DOCUMENT_TYPE_ID
                         , 10, 8
                         , DECODE(OOL.LINE_CATEGORY_CODE, 'ORDER',2,12)) IN (2,8,12)
               and ((p_level = 1
                      AND DECODE( OOL.SOURCE_DOCUMENT_TYPE_ID
                               , 10, 8
                               , DECODE(OOL.LINE_CATEGORY_CODE, 'ORDER',2,12)) <> 8)
                    OR SUBINVENTORY = p_subinv)  -- Included later for ORG Level
               and (SUBINVENTORY is null or
                        2 = 2 or
                    EXISTS (SELECT 1
                              FROM MTL_SECONDARY_INVENTORIES S
                             WHERE S.ORGANIZATION_ID = p_org_id
                               AND S.SECONDARY_INVENTORY_NAME = SUBINVENTORY
                               AND S.availability_type = DECODE(p_include_nonnet,
                                                                1,
                                                                S.AVAILABILITY_TYPE,
                                                                1)));
             dbms_output.put_line('get the total demand'|| to_char(l_total_demand_qty));
            /*IF G_TRACE_ON = 1 THEN
            print_debug('Demand from sales orders: ' ||
                        ' Ordered: '        || to_char(l_total_demand_qty)  ||
                        ', Pick released: ' || to_char(l_pick_released_qty) ||
                        ', Staged: '        || to_char(l_staged_qty)
                        , 'get_demand_qty'
                        , 9);
            END IF;*/
        end if;

        --
        -- Find out the reserved qty for the material from mtl_reservations
        --
        -- Since total demand = reserved + unreserved, and we know total
        -- demand from oe_order_lines_all (above) we only need to query
        -- mtl_reservations if the user wants one of the following:
        --
        --  1) Only reserved: (p_net_rsv = 1 and p_net_unrsv = 2)
        --
        --  OR
        --
        --  2) Only unreserved: (p_net_rsv = 2 and p_net_unrsv = 1)
        --

        IF ((p_net_rsv = 1 and p_net_unrsv = 2)
            OR
            (p_net_rsv = 2 and p_net_unrsv = 1))
        THEN
        
         dbms_output.put_line('Only reserved');
            select sum(PRIMARY_RESERVATION_QUANTITY)
              into l_total_reserve_qty
              from mtl_reservations
             WHERE ORGANIZATION_ID = p_org_id
               and INVENTORY_ITEM_ID = p_item_id
               --and REQUIREMENT_DATE <= p_d_cutoff + 0.99999 /* bug no 6009682 */
               and demand_source_type_id in (2,8,12)
               and ((p_level = 1 AND demand_source_type_id <> 8) OR
                     SUBINVENTORY_CODE = p_subinv)  -- Included later for ORG Level
               and (SUBINVENTORY_CODE is null or
                    p_level = 2 or
                    EXISTS (SELECT 1
                              FROM MTL_SECONDARY_INVENTORIES S
                             WHERE S.ORGANIZATION_ID = p_org_id
                               AND S.SECONDARY_INVENTORY_NAME = SUBINVENTORY_CODE
                               AND S.availability_type = DECODE(p_include_nonnet,
                                                                1,
                                                                S.availability_type,
                                                                1)))
/* nsinghi MIN-MAX INVCONV start */
               AND (locator_id IS NULL OR
                    p_level = 2 OR
                     EXISTS (SELECT 1 FROM mtl_item_locations mil
                            WHERE mil.organization_id = p_org_id
                            AND   mil.inventory_location_id = locator_id
                            AND   mil.subinventory_code = NVL(subinventory_code, mil.subinventory_code)
                            AND   mil.availability_type = decode(p_include_nonnet,1,mil.availability_type,1)))
               AND (lot_number IS NULL OR
                    p_level = 2 OR
                     EXISTS (SELECT 1 FROM mtl_lot_numbers mln
                            WHERE mln.organization_id = p_org_id
                            AND   mln.lot_number = lot_number
                            AND   mln.inventory_item_id = p_item_id
                            AND   mln.availability_type = decode(p_include_nonnet,1,mln.availability_type,1)))
              -- Bug 5041763 excluding drop ship demand
                  and  NOT EXISTS (SELECT 1
                                    FROM OE_DROP_SHIP_SOURCES ODSS
                                  where  ODSS.LINE_ID = DEMAND_SOURCE_LINE_ID);
                  dbms_output.put_line('Only reserved'||l_total_reserve_qty);

/* nsinghi MIN-MAX INVCONV end */

          /* IF G_TRACE_ON = 1 THEN
           print_debug('Reserved demand (sales orders): ' || to_char(l_total_reserve_qty)
                       , 'get_demand_qty'
                       , 9);
           END IF;*/
        END IF;


        --
        -- Bug 3238390, we need to take care of reservations with sub but sales order
        -- with no sub for sub level planning. Adding the below query
        --
       
       /*  print_debug('Allocated demand for subinventory: ' || to_char(l_allocated_qty)
                       , 'get_demand_qty'
                       , 9);*/

        --
        -- total demand is calculated as follows:
        -- if we have to consider both unreserved matl and reserved matl. then the
        --    demand is simply the total demand = ordered qty - shipped qty.
        --    Bug 2333526: Deduct staged qty for sub level.  (l_staged_qty
        --    is always set to 0 for org level planning).
        --    Bug 3238390, add reserved qty for sales orders with no sub
        --    and reservation with sub for sub level planning.
        -- elsif we have to take into account only reserved matl. then the
        --    demand is simply the reservations from mtl_reservations for the matl.
        -- elsif we have to take into account just the unreserved matl. then the
        --    demand is total demand - the reservations for the material.
        --    Bug 3238390, add reserved qty for sales orders with no sub
        --    and reservation with sub for sub level planning, so that demand doesn't go -ve.
        if P_NET_UNRSV = 1 and P_NET_RSV = 2 then
          dbms_output.put_line(' P_NET_UNRSV = 1 and P_NET_RSV = 1');
           l_demand_qty := NVL(l_total_demand_qty,0)
                           - NVL(l_staged_qty,0)
                           - NVL(l_pick_released_qty,0)
                           + NVL(l_sub_reserve_qty,0)
                           + NVL(L_ALLOCATED_QTY,0);
         DBMS_OUTPUT.PUT_LINE(' P_NET_UNRSV = 1 and P_NET_RSV = 1 is '||L_DEMAND_QTY);
         DBMS_OUTPUT.PUT_LINE(' L_TOTAL_DEMAND_QTY'||L_TOTAL_DEMAND_QTY);
         DBMS_OUTPUT.PUT_LINE(' l_pick_released_qty'||L_PICK_RELEASED_QTY);
         DBMS_OUTPUT.PUT_LINE(' l_sub_reserve_qty'||L_SUB_RESERVE_QTY);
         DBMS_OUTPUT.PUT_LINE(' L_ALLOCATED_QTY'||L_ALLOCATED_QTY);
         dbms_output.put_line(' l_staged_qty'||l_staged_qty);

        ELSIF p_net_rsv = 2 THEN
           L_DEMAND_QTY := NVL(L_TOTAL_RESERVE_QTY,0) + NVL(L_ALLOCATED_QTY,0);
                  dbms_output.put_line(' p_net_rsv'||l_demand_qty);

        ELSIF p_net_unrsv = 1 THEN
           l_demand_qty := NVL(l_total_demand_qty,0) - NVL(l_total_reserve_qty,0) + NVL(l_sub_reserve_qty,0);
           
                    dbms_output.put_line(' p_net_unrsv'||l_demand_qty);

        end if;
      /*  IF G_TRACE_ON = 1 THEN
        print_debug('Demand from shippable orders: ' || to_char(l_demand_qty)
                    , 'get_demand_qty'
                    , 9);
        END IF;*/
        total := total + NVL(l_demand_qty,0);
        
           dbms_output.put_line(' total := total + NVL(l_demand_qty,0);'||total);

        --
        -- Take care of internal orders for org level planning
        --
        if P_LEVEL = 1 then
         dbms_output.put_line('Take care of internal orders for org level planning'); 
            l_total_demand_qty := 0;
            l_demand_qty := 0;
            l_total_reserve_qty := 0;

            --
            -- get the total demand which is the difference between the
            -- ordered qty. and the shipped qty.
            -- This gives the total demand including the reserved
            -- and the unreserved material.
            --
  -- Bug 2820011. Modified the where clause to make use of source_document_id
  -- and source_document_line_id of oe_order_lines_all instead of
  -- orig_sys_document_ref and orig_sys_line_ref to identify requisitions
  -- and requisition lines uniquely.

            if p_net_unrsv = 1 then
                select SUM( NVL(ORDERED_QUANTITY,0) -
                           XXEIS.eis_po_xxwc_isr_util_qa_pkg.get_shipped_qty(p_org_id, p_item_id, so.line_id))
                  into l_total_demand_qty
                  from oe_order_lines_all so,
--                     po_requisition_headers_all poh,
                       po_requisition_lines_all pol
                 where so.SOURCE_DOCUMENT_ID  = pol.requisition_header_id
--                 and poh.requisition_header_id = pol.requisition_header_id
                   and so.source_document_line_id = pol.requisition_line_id
                   and (pol.DESTINATION_ORGANIZATION_ID <> p_org_id or
                        (pol.DESTINATION_ORGANIZATION_ID = p_org_id and  -- Added code Bug#1012179
                          ( pol.DESTINATION_TYPE_CODE = 'EXPENSE' OR  --Bug#3619239 started
-- Bug 3619239 The functionality is added so that demand from Internal Sales Requisitions are taken
-- into consideration if Destination Type is Inventory and Destination Subinventory is Non Quantity Tracked
			   (  pol.DESTINATION_TYPE_CODE = 'INVENTORY'
			      AND pol.DESTINATION_SUBINVENTORY IS NOT NULL
			      AND EXISTS (select 1 from
			                  MTL_SECONDARY_INVENTORIES
                                          where SECONDARY_INVENTORY_NAME = pol.DESTINATION_SUBINVENTORY
                                          and ORGANIZATION_ID = pol.DESTINATION_ORGANIZATION_ID
                                          and QUANTITY_TRACKED = 2)
			    )
			   )-- Bug#3619239 ended
			  )
                        )
                   and so.ship_from_org_ID = p_org_id
                   and so.open_flag = 'Y'
                   AND so.visible_demand_flag = 'Y'
                   AND shipped_quantity is null
                   and so.INVENTORY_ITEM_ID = p_item_id
                 --  and schedule_ship_date <= sysdate + 0.99999 /* bug no 6009682 */
                   and DECODE(so.SOURCE_DOCUMENT_TYPE_ID, 10, 8,DECODE(so.LINE_CATEGORY_CODE, 'ORDER',2,12)) = 8
                   and (SUBINVENTORY is null or
                        EXISTS (SELECT 1
                                  FROM MTL_SECONDARY_INVENTORIES S
                                 WHERE S.ORGANIZATION_ID = p_org_id
                                   AND S.SECONDARY_INVENTORY_NAME = SUBINVENTORY
                                   AND S.availability_type = DECODE(p_include_nonnet,
                                                                    1,
                                                                    S.availability_type,
                                                                    1)));

             /*   IF G_TRACE_ON = 1 THEN
                print_debug('Total demand (internal orders): ' || to_char(l_total_demand_qty)
                            , 'get_demand_qty'
                            , 9);
                END IF;*/
                
                   dbms_output.put_line('Take care of internal orders for org level planning'||l_total_demand_qty); 
            end if;

            --
            -- Find out the reserved qty for the material from mtl_reservations
            --
            IF ((p_net_rsv = 1 and p_net_unrsv = 2)
                OR
                (p_net_rsv = 2 and p_net_unrsv = 1))
            THEN
               --
               -- Include the reserved demand from mtl_reservations
               --
               select sum(PRIMARY_RESERVATION_QUANTITY)
                 into l_total_reserve_qty
                 from mtl_reservations md, oe_order_lines_all so,
--                      po_req_distributions_all pod,    Bug 5934651
                      po_requisition_lines_all pol
                where md.DEMAND_SOURCE_LINE_ID = so.LINE_ID
--                  and to_number(so.ORIG_SYS_LINE_REF)     = pod.DISTRIBUTION_ID --Bug#2883172
                    and so.SOURCE_DOCUMENT_ID  = pol.requisition_header_id         -- Bug 5934651
                    and so.source_document_line_id = pol.requisition_line_id
--                  and pod.REQUISITION_LINE_ID  = pol.REQUISITION_LINE_ID
                  and (pol.DESTINATION_ORGANIZATION_ID <> p_org_id or
                       (pol.DESTINATION_ORGANIZATION_ID = p_org_id
                        and  -- Added code Bug#1012179
                        ( pol.DESTINATION_TYPE_CODE = 'EXPENSE' OR  -- Bug#3619239 started
-- Bug 3619239 The functionality is added so that demand from Internal Sales Requisitions are taken
-- into consideration if Destination Type is Inventory and Destination Subinventory is Non Quantity Tracked
			  (  pol.DESTINATION_TYPE_CODE = 'INVENTORY'
			     AND pol.DESTINATION_SUBINVENTORY IS NOT NULL
			     AND EXISTS (select 1 from
			                 MTL_SECONDARY_INVENTORIES
                                         where SECONDARY_INVENTORY_NAME = pol.DESTINATION_SUBINVENTORY
                                         and ORGANIZATION_ID = pol.DESTINATION_ORGANIZATION_ID
                                         and QUANTITY_TRACKED = 2)
			   )
			 )-- Bug#3619239 ended
			)
                      )
                  and ORGANIZATION_ID = p_org_id
                  and md.INVENTORY_ITEM_ID = p_item_id
                  --and REQUIREMENT_DATE <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                  and demand_source_type_id = 8
                  and (SUBINVENTORY_CODE is null or
                       EXISTS (SELECT 1
                                 FROM MTL_SECONDARY_INVENTORIES S
                                WHERE S.ORGANIZATION_ID = p_org_id
                                  AND S.SECONDARY_INVENTORY_NAME = SUBINVENTORY_CODE
                                  AND S.availability_type = DECODE(p_include_nonnet,
                                                                   1,
                                                                   S.availability_type,
                                                                   1)))
/* nsinghi MIN-MAX INVCONV start */
                  AND (md.locator_id IS NULL OR
                       p_level = 2 OR
                        EXISTS (SELECT 1 FROM mtl_item_locations mil
                               WHERE mil.organization_id = p_org_id
                               AND   mil.inventory_location_id = md.locator_id
                               AND   mil.subinventory_code = NVL(md.subinventory_code, mil.subinventory_code)
                               AND   mil.availability_type = decode(p_include_nonnet,1,mil.availability_type,1)))
                  AND (md.lot_number IS NULL OR
                       p_level = 2 OR
                        EXISTS (SELECT 1 FROM mtl_lot_numbers mln
                               WHERE mln.organization_id = p_org_id
                               AND   mln.lot_number = md.lot_number
                               AND   mln.inventory_item_id = p_item_id
                               AND   mln.availability_type = decode(p_include_nonnet,1,mln.availability_type,1)));
/* nsinghi MIN-MAX INVCONV end */

             /*  IF G_TRACE_ON = 1 THEN
               print_debug('Reserved demand (internal orders): ' || to_char(l_total_reserve_qty)
                           , 'get_demand_qty'
                           , 9);
               END IF;*/
               
                 dbms_output.put_line('Include the reserved demand from mtl_reservations'||l_total_reserve_qty);
            END IF;

            --
            -- total demand is calculated as follows:
            -- if we have to consider both unreserved matl and reserved matl. then the
            --    demand is simply the total demand = ordered qty - shipped qty.
            -- elsif we have to take into account only reserved matl. then the
            --    demand is simply the reservations from mtl_reservations for the matl.
            -- elsif we have to take into account just the unreserved matl. then the
            --    demand is total demand - the reservations for the material.
            --
            if p_net_unrsv = 1 and p_net_rsv = 1 then
               l_demand_qty := NVL(l_total_demand_qty,0);

            elsif p_net_rsv = 1 then
               l_demand_qty := NVL(l_total_reserve_qty,0);

            elsif p_net_unrsv = 1 then
               l_demand_qty := NVL(l_total_demand_qty,0) - NVL(l_total_reserve_qty,0);
            end if;
          /*  IF G_TRACE_ON = 1 THEN
            print_debug('Demand from internal orders: ' || to_char(l_demand_qty)
                        , 'get_demand_qty'
                        , 9);
            END IF;*/
            total := total + NVL(l_demand_qty,0);
            
                      dbms_output.put_line('  total := total + NVL(l_demand_qty,0)  is '||total);

        end if; -- end if level=1

        --
     /* Bug 3364512. Demand is double for back-to-back sales orders after
	auto-create requisition and for sales orders with ATO items after
	auto-create WIP job. Commenting the below code which fetches duplicate
	demand */

        -- WIP Reservations from mtl_demand
        --
     /*   IF p_level = 1 THEN
            --
            -- SUBINVENTORY IS Always expected to be Null when Reservation_type is 3.
            --
            select sum(PRIMARY_UOM_QUANTITY - GREATEST(NVL(RESERVATION_QUANTITY,0),
                   NVL(COMPLETED_QUANTITY,0)))
              into qty
              from mtl_demand
             where RESERVATION_TYPE = 3
               and ORGANIZATION_ID = p_org_id
               and PRIMARY_UOM_QUANTITY >
                    GREATEST(NVL(RESERVATION_QUANTITY,0), NVL(COMPLETED_QUANTITY,0))
               and INVENTORY_ITEM_ID = p_item_id
               and REQUIREMENT_DATE <= p_d_cutoff
               and p_net_rsv = 1;

            IF G_TRACE_ON = 1 THEN
            print_debug('WIP Reservations from mtl_demand: ' || to_char(qty)
                        , 'get_demand_qty'
                        , 9);
            END IF;
            total := total + NVL(qty,0);
        END IF; */

        --
        -- Wip Components are to be included at the Org Level Planning only.
        -- Qty Issued Substracted from the Qty Required
        --
        if (p_net_wip = 1 and p_level = 1)
        then

/* nsinghi MIN-MAX INVCONV start */

           IF p_process_org = 'Y' THEN

/*      Here we need include the query to include OPM as source of demand.
Since GME will always give the complete demand (including reserved demand)
so subtracting the reserved demand as reserved demand will be considered
above from mtl_reservations query. */


              SELECT
                 SUM ( NVL(NVL(d.wip_plan_qty, d.plan_qty) - d.actual_qty, 0)-
                       NVL(mtr.primary_reservation_quantity,0))
              INTO qty
              FROM   gme_material_details d
              ,      gme_batch_header     h
              ,      mtl_reservations     mtr
              WHERE  h.batch_type IN (0,10)
              AND    h.batch_status IN (1,2)
              AND    h.batch_id = d.batch_id
              AND    d.line_type = -1
--              AND    NVL(d.original_qty, 0) <> 0       --commented as part of bug 8434499
              AND    d.organization_id = p_org_id
              AND    d.inventory_item_id = p_item_id
              AND    d.batch_id = mtr.demand_source_header_id (+)
              AND    d.material_detail_id = mtr.demand_source_line_id (+)
              AND    d.inventory_item_id = mtr.inventory_item_id (+)
              AND    d.organization_id = mtr.organization_id (+)
              AND    NVL(NVL(d.wip_plan_qty, d.plan_qty) - d.actual_qty, 0)-
                       NVL(mtr.primary_reservation_quantity,0) > 0
              AND    NVL(mtr.demand_source_type_id, 5) = 5
              --AND    d.material_requirement_date <= p_d_cutoff
              AND    (mtr.subinventory_code IS NULL OR
                      EXISTS (SELECT 1
                                FROM mtl_secondary_inventories s
                               WHERE s.organization_id = p_org_id
                                 AND s.secondary_inventory_name = mtr.subinventory_code
                                 AND s.availability_type = DECODE(p_include_nonnet,1,s.availability_type,1)))
              AND    (mtr.locator_id IS NULL OR
                       EXISTS (SELECT 1 FROM mtl_item_locations mil
                              WHERE mil.organization_id = p_org_id
                              AND   mil.inventory_location_id = mtr.locator_id
                              AND   mil.subinventory_code = NVL(mtr.subinventory_code, mil.subinventory_code)
                              AND   mil.availability_type = DECODE(p_include_nonnet,1,mil.availability_type,1)))
              AND    (mtr.lot_number IS NULL OR
                       EXISTS (SELECT 1 FROM mtl_lot_numbers mln
                              WHERE mln.organization_id = p_org_id
                              AND   mln.lot_number = mtr.lot_number
                              AND   mln.inventory_item_id = p_item_id
                              AND   mln.availability_type = DECODE(p_include_nonnet,1,mln.availability_type,1)));

             /* IF G_TRACE_ON = 1 THEN
              print_debug('Batch Material requirements for OPM Batches : ' || to_char(qty)
                          , 'get_demand_qty'
                          , 9);
              END IF;*/
              total := total + NVL(qty,0);
              
              dbms_output.put_line(' p_process_org'||total);
              
              select sum(o.required_quantity - o.quantity_issued)
                into qty
                from wip_discrete_jobs d, wip_requirement_operations o
               where o.wip_entity_id     = d.wip_entity_id
                 and o.organization_id   = d.organization_id
                 and d.organization_id   = p_org_id
                 and o.inventory_item_id = p_item_id
                 --and o.date_required    <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                 and o.required_quantity > 0
                 and o.required_quantity > o.quantity_issued
                 and o.operation_seq_num > 0
                 and d.status_type in (1,3,4,6) -- Excluded 5 from selection Bug#1016495
                 and o.wip_supply_type not in (5,6); -- Included 5 from the selection Bug#4488415

              /*IF G_TRACE_ON = 1 THEN
              print_debug('WIP component requirements for discrete jobs: ' || to_char(qty)
                          , 'get_demand_qty'
                          , 9);
              END IF;*/
              TOTAL := TOTAL + NVL(QTY,0);
              dbms_output.put_line(' p_process_org'||total);
              --
              -- Demand Qty to be added for a released repetitive schedule
              -- Bug#691471
              --
              /*4518296*/
              select sum(o.required_quantity - o.quantity_issued)
                into qty
                from wip_repetitive_schedules r, wip_requirement_operations o
               where o.wip_entity_id          = r.wip_entity_id
                 and o.repetitive_schedule_id = r.repetitive_schedule_id
                 and o.organization_id        = r.organization_id
                 and r.organization_id        = p_org_id
                 and o.inventory_item_id      = p_item_id
                 --and o.date_required          <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                 and o.required_quantity      > 0
                 and o.required_quantity      > o.quantity_issued
                 and o.operation_seq_num      > 0
                 and r.status_type in (1,3,4,6) -- Excluded 5 from selection Bug#1016495
                 and O.WIP_SUPPLY_TYPE not in (5,6); -- Included 5 from the selection Bug#4488415
           /*   IF G_TRACE_ON = 1 THEN
              print_debug('WIP component requirements for repetitive schedules: ' || to_char(qty)
                          , 'get_demand_qty'
                          , 9);
              END IF;*/
              total := total + NVL(qty,0);
              
                     dbms_output.put_line(' p_process_org 3 '||total);


           ELSE
/* nsinghi MIN-MAX INVCONV end */
              /*4518296*/

              select sum(o.required_quantity - o.quantity_issued)
                into qty
                from wip_discrete_jobs d, wip_requirement_operations o
               where o.wip_entity_id     = d.wip_entity_id
                 and o.organization_id   = d.organization_id
                 and d.organization_id   = p_org_id
                 and o.inventory_item_id = p_item_id
                 --and o.date_required    <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                 and o.required_quantity > 0
                 and o.required_quantity > o.quantity_issued
                 and o.operation_seq_num > 0
                 and d.status_type in (1,3,4,6) -- Excluded 5 from selection Bug#1016495
                 and o.wip_supply_type not in (5,6); -- Included 5 from the selection Bug#4488415

              /*IF G_TRACE_ON = 1 THEN
              print_debug('WIP component requirements for discrete jobs: ' || to_char(qty)
                          , 'get_demand_qty'
                          , 9);
              END IF;*/
              TOTAL := TOTAL + NVL(QTY,0);
       dbms_output.put_line(' p_process_org'||total);
              --
              -- Demand Qty to be added for a released repetitive schedule
              -- Bug#691471
              --
              /*4518296*/
              select sum(o.required_quantity - o.quantity_issued)
                into qty
                from wip_repetitive_schedules r, wip_requirement_operations o
               where o.wip_entity_id          = r.wip_entity_id
                 and o.repetitive_schedule_id = r.repetitive_schedule_id
                 and o.organization_id        = r.organization_id
                 and r.organization_id        = p_org_id
                 and o.inventory_item_id      = p_item_id
                 --and o.date_required          <= p_d_cutoff + 0.99999 /* bug no 6009682 */
                 and o.required_quantity      > 0
                 and o.required_quantity      > o.quantity_issued
                 and o.operation_seq_num      > 0
                 and r.status_type in (1,3,4,6) -- Excluded 5 from selection Bug#1016495
                 and O.WIP_SUPPLY_TYPE not in (5,6); -- Included 5 from the selection Bug#4488415
           /*   IF G_TRACE_ON = 1 THEN
              print_debug('WIP component requirements for repetitive schedules: ' || to_char(qty)
                          , 'get_demand_qty'
                          , 9);
              END IF;*/
              total := total + NVL(qty,0);
              
                     dbms_output.put_line(' p_process_org 3 '||total);

           END IF; /* p_process_org = 'Y' */

        end if;

        --
        -- Include move orders:
        -- Leave out the closed or cancelled lines
        -- Select only Issue from Stores for org level planning
        -- Also select those lines for sub level planning.
        --
        -- Exclude move orders created for WIP Issue transaction
        -- (txn type = 35, INV_Globals.G_TYPE_XFER_ORDER_WIP_ISSUE)
        -- since these are already taken into account (above) by
        -- directly querying the WIP tables for open component requirements
        --

        -- kkoothan Part of Bug Fix: 2875583
        -- Converting the quantities to the primary uom as the quantity
        -- and quantity delivered in mtl_txn_request_lines
        -- are in transaction uom.

--Bug 3057273, Move order demand should be excluded if net unreserved demand is No.
  if p_net_unrsv = 1 then

        /*SELECT SUM(MTRL.QUANTITY - NVL(MTRL.QUANTITY_DELIVERED,0))
          INTO qty
          FROM MTL_TXN_REQUEST_LINES MTRL,
               MTL_TRANSACTION_TYPES MTT
         WHERE MTT.TRANSACTION_TYPE_ID = MTRL.TRANSACTION_TYPE_ID
           AND MTRL.TRANSACTION_TYPE_ID <> INV_Globals.G_TYPE_XFER_ORDER_WIP_ISSUE
           AND MTRL.ORGANIZATION_ID = p_org_id
           AND MTRL.INVENTORY_ITEM_ID = p_item_id
           AND MTRL.LINE_STATUS NOT IN (5,6)
           AND MTT.TRANSACTION_ACTION_ID = 1
           AND (p_level = 1  OR
                MTRL.FROM_SUBINVENTORY_CODE = p_subinv)
           AND (MTRL.FROM_SUBINVENTORY_CODE IS NULL OR
                p_level = 2  OR
                EXISTS (SELECT 1
                          FROM MTL_SECONDARY_INVENTORIES S
                         WHERE S.ORGANIZATION_ID = p_org_id
                           AND S.SECONDARY_INVENTORY_NAME = MTRL.FROM_SUBINVENTORY_CODE
                           AND S.AVAILABILITY_TYPE = DECODE(p_include_nonnet,
                                                            1,S.AVAILABILITY_TYPE,1)))
           AND MTRL.DATE_REQUIRED <= p_d_cutoff;*/

           SELECT NVL(SUM(apps.inv_decimals_pub.get_primary_quantity( p_org_id
                                                             ,p_item_id
                                                             , mtrl.uom_code
                                                             , mtrl.quantity - NVL(mtrl.quantity_delivered,0))
                                                             ),0)
           INTO  qty
           FROM  MTL_TXN_REQUEST_LINES MTRL,
                 MTL_TRANSACTION_TYPES MTT
           where  MTT.TRANSACTION_TYPE_ID = MTRL.TRANSACTION_TYPE_ID
           AND    MTRL.TRANSACTION_TYPE_ID <> apps.INV_Globals.G_TYPE_XFER_ORDER_WIP_ISSUE
           AND    MTRL.ORGANIZATION_ID = p_org_id
           AND    MTRL.INVENTORY_ITEM_ID = p_item_id
           AND    MTRL.LINE_STATUS  IN (3,7)--Changed for Bug 5330189: 3 = Approved 7 = Pre-Approved
           AND    MTT.TRANSACTION_ACTION_ID = 1
           AND    (p_level = 1  OR
                   MTRL.FROM_SUBINVENTORY_CODE = p_subinv)
           AND    (MTRL.FROM_SUBINVENTORY_CODE IS NULL OR
                  p_level = 2  OR
                  EXISTS (SELECT 1
                          FROM MTL_SECONDARY_INVENTORIES S
                          WHERE   S.ORGANIZATION_ID = p_org_id
                          AND     S.SECONDARY_INVENTORY_NAME = MTRL.FROM_SUBINVENTORY_CODE
                          AND     S.AVAILABILITY_TYPE = DECODE(p_include_nonnet,
                                                        1,S.AVAILABILITY_TYPE,1)))
           --AND mtrl.date_required <= p_d_cutoff + 0.99999 /* bug no 6009682 */
/* nsinghi MIN-MAX INVCONV start */
           AND (mtrl.from_locator_id IS NULL OR
                p_level = 2 OR
                 EXISTS (SELECT 1 FROM mtl_item_locations mil
                        WHERE mil.organization_id = p_org_id
                        AND   mil.inventory_location_id = mtrl.from_locator_id
                        AND   mil.subinventory_code = NVL(mtrl.from_subinventory_code, mil.subinventory_code)
                        AND   mil.availability_type = decode(p_include_nonnet,1,mil.availability_type,1)))
           AND (mtrl.lot_number IS NULL OR
                p_level = 2 OR
                 EXISTS (SELECT 1 FROM mtl_lot_numbers mln
                        WHERE mln.organization_id = p_org_id
                        AND   mln.lot_number = mtrl.lot_number
                        AND   mln.inventory_item_id = p_item_id
                        AND   mln.availability_type = decode(p_include_nonnet,1,mln.availability_type,1)));
/* nsinghi MIN-MAX INVCONV end */

      /*  IF G_TRACE_ON = 1 THEN
        PRINT_DEBUG('Demand from open move orders: ' || TO_CHAR(QTY), 'get_demand_qty', 9);
        END IF;*/

        total := total + NVL(qty,0);
        
               dbms_output.put_line(' p_process_org4'||total);

  end if;

        --
        -- Include the sub transfer and the staging transfer move orders
        -- for sub level planning
        -- Bug 3057273, Move order demand should be excluded if net unreserved demand is No.

        IF (p_level = 2 and p_net_unrsv = 1) THEN
            -- kkoothan Part of Bug Fix: 2875583
            -- Converting the quantities to the primary uom as the quantity
            -- and quantity delivered in mtl_txn_request_lines
            -- are in transaction uom.

            /*SELECT NVL(sum(mtrl.quantity - NVL(mtrl.quantity_delivered,0)),0)
              INTO qty
              FROM mtl_transaction_types  mtt,
                   mtl_txn_request_lines  mtrl
             WHERE mtt.transaction_action_id IN (2,28)
               AND mtt.transaction_type_id     = mtrl.transaction_type_id
               AND mtrl.organization_id        = p_org_id
               AND mtrl.inventory_item_id      = p_item_id
               AND mtrl.from_subinventory_code = p_subinv
               AND mtrl.line_status NOT IN (5,6)
               AND mtrl.date_required         <= p_d_cutoff;*/

            select NVL(SUM( MTRL.QUANTITY - NVL(MTRL.QUANTITY_DELIVERED,0))
                                                             ,0)
            INTO qty
            FROM mtl_transaction_types  mtt,
                 mtl_txn_request_lines  mtrl
            WHERE mtt.transaction_action_id IN (2,28)
              AND mtt.transaction_type_id     = mtrl.transaction_type_id
              AND mtrl.organization_id        = p_org_id
              AND mtrl.inventory_item_id      = p_item_id
              AND mtrl.from_subinventory_code = p_subinv
              AND MTRL.LINE_STATUS  IN (3,7); --Changed for Bug 5330189: 3 = Approved 7 = Pre-Approved
              --AND mtrl.date_required <= p_d_cutoff + 0.99999; /* bug no 6009682 */

           /* IF G_TRACE_ON = 1 THEN
            print_debug('Qty pending out due to sub transfers and the staging transfer move orders: '
                         || to_char(qty)
                        , 'get_demand_qty'
                        , 9);
            END IF;*/
            total := total + NVL(qty,0);
            
                   dbms_output.put_line(' p_process_org 6'||total);
        END IF;

         -- Bug 5041763 need to exclude drop ship reservation from on-hand qty to get correct availability
           select sum(PRIMARY_RESERVATION_QUANTITY)
           into   qty
           from   mtl_reservations
           WHERE  ORGANIZATION_ID = p_org_id
           and    INVENTORY_ITEM_ID = p_item_id
           and    demand_source_type_id  = 2
           and    supply_source_type_id = 13
           --and    REQUIREMENT_DATE <= p_d_cutoff + 0.99999 /* bug no 6009682 */
           and    ((p_level = 1 ) OR
                  SUBINVENTORY_CODE = p_subinv)
           and ( SUBINVENTORY_CODE is null or
                 p_level = 2 or
                 EXISTS (SELECT 1
                        FROM MTL_SECONDARY_INVENTORIES S
                        WHERE S.ORGANIZATION_ID = p_org_id
                        AND S.SECONDARY_INVENTORY_NAME = SUBINVENTORY_CODE
                        AND S.availability_type = DECODE(p_include_nonnet,
                                                         1,
                                                         S.availability_type,
                                                         1)))
           and    EXISTS (SELECT 1
                            FROM OE_DROP_SHIP_SOURCES ODSS
                            WHERE  ODSS.LINE_ID = DEMAND_SOURCE_LINE_ID);
           TOTAL := TOTAL + NVL(QTY,0);
              dbms_output.put_line(' p_process_org 7'||total);
	--Bug 6240025 BEGIN
	l_loaded_qty := XXEIS.eis_po_xxwc_isr_util_qa_pkg.get_loaded_qty(p_org_id
				      , p_subinv
				      , p_level
				      , p_item_id
				      , p_net_rsv
				      , p_net_unrsv);
	total := total+NVL(l_loaded_qty,0);
	--Bug 6240025 END
        return(TOTAL);
         dbms_output.put_line(' return toatl is '||total);

 
    end if ;
       exception
        when OTHERS then
        return(NVL(TOTAL,0));
        DBMS_OUTPUT.PUT_LINE('The error is'||SQLCODE||SQLERRM);
       
end;				 

PROCEDURE GET_PLANNING_QUANTITY(
     P_INCLUDE_NONNET  IN NUMBER
   , P_LEVEL           IN NUMBER
   , P_ORG_ID          IN NUMBER
   , P_SUBINV          IN VARCHAR2
   , P_ITEM_ID         IN NUMBER
   , P_GRADE_CODE      IN VARCHAR2                       -- invConv change
   , X_QOH             OUT NOCOPY NUMBER                         -- invConv change
   , X_SQOH            OUT NOCOPY NUMBER                         -- invConv change
) IS

     x_return_status   VARCHAR2(30);
     l_qoh              NUMBER := 0;
     l_moq_qty          NUMBER := 0;
     l_mmtt_qty_src     NUMBER := 0;
     l_mmtt_qty_dest    NUMBER := 0;
     l_sqoh             NUMBER := 0;         -- invConv change
     l_moq_sqty         NUMBER := 0;         -- invConv change
     l_mmtt_sqty_src    NUMBER := 0;         -- invConv change
     l_mmtt_sqty_dest   NUMBER := 0;         -- invConv change
     l_lot_control     NUMBER := 1;
     l_debug           NUMBER := NVL(FND_PROFILE.VALUE('INV_DEBUG_TRACE'),0);
     l_lpn_qty         NUMBER := 0;    -- Bug 4209192
     l_default_status_id  number:= -1; -- Added for 6633612

-- invConv changes begin
l_uom_ind        VARCHAR2(4);

CURSOR get_item_info( l_org_id IN NUMBER
                    , l_item_id  IN NUMBER) IS
SELECT tracking_quantity_ind
, lot_control_code
FROM mtl_system_items_b
WHERE inventory_item_id = l_item_id
AND organization_id = l_org_id;
-- invConv changes end

BEGIN  

   -- invConv changes begin
   -- Only run this function when DUOM item.
   OPEN get_item_info( p_org_id, p_item_id);
   FETCH get_item_info
    INTO l_uom_ind
       , l_lot_control;
   CLOSE get_item_info;

    -- invConv change : this is included in the above cursor.
    -- SELECT lot_control_code
    -- into l_lot_control
    -- from  mtl_system_items_b
    -- where inventory_item_id = p_item_id
    -- and   organization_id = p_org_id;

	   l_default_status_id := -1;


    IF (p_level = 1) THEN
    -- Organization Level

/* nsinghi MIN-MAX INVCONV start */

        IF p_include_nonnet = 1 THEN

        -- invConv change : replaced primary by secondary qty field.

            SELECT SUM(moq.primary_transaction_quantity)
                 , SUM( NVL(moq.secondary_transaction_quantity, 0))
            INTO   l_moq_qty
                 , l_moq_sqty
            FROM   mtl_onhand_quantities_detail moq, mtl_lot_numbers mln, mtl_secondary_inventories msi
            WHERE  moq.organization_id = p_org_id
            AND    moq.inventory_item_id = p_item_id
            AND    msi.organization_id = moq.organization_id
            AND    msi.secondary_inventory_name = moq.subinventory_code
            AND    moq.organization_id = nvl(moq.planning_organization_id, moq.organization_id)
            AND    moq.lot_number = mln.lot_number(+)
            AND    moq.organization_id = mln.organization_id(+)
            AND    moq.inventory_item_id = mln.inventory_item_id(+)
            AND    trunc(nvl(mln.expiration_date, sysdate+1)) > trunc(sysdate)
            AND    nvl(moq.planning_tp_type,2) = 2;


        ELSE /* include nettable */

           SELECT SUM(mon.primary_transaction_quantity)
                , SUM( NVL(mon.secondary_transaction_quantity, 0))
           INTO   l_moq_qty
                , l_moq_sqty
           FROM   apps.mtl_onhand_net mon, mtl_lot_numbers mln
           WHERE  mon.organization_id = p_org_id
           AND    mon.inventory_item_id = p_item_id
           AND    mon.organization_id = nvl(mon.planning_organization_id, mon.organization_id)
           AND    mon.lot_number = mln.lot_number(+)
           AND    mon.organization_id = mln.organization_id(+)
           AND    mon.inventory_item_id = mln.inventory_item_id(+)
           AND    trunc(nvl(mln.expiration_date, sysdate+1)) > trunc(sysdate)
           AND    nvl(mon.planning_tp_type,2) = 2;

        END IF;

       

/* nsinghi MIN-MAX INVCONV end */

        IF (l_lot_control = 2) THEN /* Lot - Full Control*/

	   -- Added the below if for 6633612
	 IF l_default_status_id = -1 THEN

        

           SELECT SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                  Sign(mmtt.primary_quantity)) * Abs( mmtt.primary_quantity ))
                , SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                  Sign(mmtt.secondary_transaction_quantity)) * Abs( NVL(mmtt.secondary_transaction_quantity, 0) ))
           INTO   l_mmtt_qty_src
                , l_mmtt_sqty_src
           FROM   apps.mtl_material_transactions_temp mmtt
           WHERE  mmtt.organization_id = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.posting_flag = 'Y'
           AND    mmtt.subinventory_code IS NOT NULL
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id NOT IN (24,30)
           AND    EXISTS (SELECT 'x' FROM mtl_secondary_inventories msi
                  WHERE msi.organization_id = mmtt.organization_id
                  AND   msi.secondary_inventory_name = mmtt.subinventory_code
                  AND    msi.availability_type = decode(p_include_nonnet,1,msi.availability_type,1))
           AND    mmtt.planning_organization_id IS NULL
           AND    EXISTS (SELECT 'x' FROM apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
                          WHERE  mtlt.transaction_temp_id = mmtt.transaction_temp_id
                          AND    mtlt.lot_number = mln.lot_number(+)
                          AND    p_org_id = mln.organization_id(+)
                          AND    p_item_id = mln.inventory_item_id(+)
/* nsinghi MIN-MAX INVCONV start */
                          AND    nvl(mln.availability_type,2) = decode(p_include_nonnet,1,nvl(mln.availability_type,2),1)
                          AND    trunc(nvl(nvl(mtlt.lot_expiration_date,mln.expiration_Date),SYSDATE+1))> trunc(sysdate))
           AND (mmtt.locator_id IS NULL OR
                    (mmtt.locator_id IS NOT NULL AND
                     EXISTS (SELECT 'x' FROM mtl_item_locations mil
                            WHERE mmtt.organization_id = mil.organization_id
                            AND   mmtt.locator_id = mil.inventory_location_id
                            AND   mmtt.subinventory_code = mil.subinventory_code
                            AND   mil.availability_type = decode(p_include_nonnet,1,mil.availability_type,1))))
/* nsinghi MIN-MAX INVCONV end */

	        AND  nvl(mmtt.planning_tp_type,2) = 2;

         

           SELECT SUM(Abs(mmtt.primary_quantity))
                , SUM(Abs( NVL(mmtt.secondary_transaction_quantity, 0) ))
           INTO   l_mmtt_qty_dest
                , l_mmtt_sqty_dest
           FROM   apps.mtl_material_transactions_temp mmtt
           WHERE  decode(mmtt.transaction_action_id,3,
                  mmtt.transfer_organization,mmtt.organization_id) = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.posting_flag = 'Y'
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id  in (2,28,3)
           AND
           (
              (mmtt.transfer_subinventory IS NULL)
              OR
              (
                 mmtt.transfer_subinventory IS NOT NULL
                 AND    EXISTS
                 (
                    SELECT 'x' FROM mtl_secondary_inventories msi
                    WHERE msi.organization_id = decode(mmtt.transaction_action_id,
                          3, mmtt.transfer_organization,mmtt.organization_id)
                       AND   msi.secondary_inventory_name = mmtt.transfer_subinventory
                       AND   msi.availability_type = decode(p_include_nonnet,1,msi.availability_type,1)
                 )
              )
           )
           AND    mmtt.planning_organization_id IS NULL
           AND    EXISTS
           (
              SELECT 'x' FROM apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
              WHERE  mtlt.transaction_temp_id = mmtt.transaction_temp_id
                 AND    mtlt.lot_number = mln.lot_number (+)
                 AND    decode(mmtt.transaction_action_id,
                                    3, mmtt.transfer_organization,mmtt.organization_id) = mln.organization_id(+)
                 AND    p_item_id = mln.inventory_item_id(+)
/* nsinghi MIN-MAX INVCONV start */
                  AND    nvl(mln.availability_type,2) = decode(p_include_nonnet,1,nvl(mln.availability_type,2),1)
                  AND    trunc(nvl(nvl(mtlt.lot_expiration_Date,mln.expiration_date),sysdate+1))> trunc(sysdate)
           )
           AND
           (
              mmtt.transfer_to_location IS NULL OR
              (
                 mmtt.transfer_to_location IS NOT NULL AND
                 EXISTS
                 (
                    SELECT 'x' FROM mtl_item_locations mil
                    WHERE decode(mmtt.transaction_action_id,
                                     3, mmtt.transfer_organization,mmtt.organization_id) = mil.organization_id
                       AND   mmtt.transfer_to_location = mil.inventory_location_id
                       AND   mmtt.transfer_subinventory = mil.subinventory_code
                       AND   mil.availability_type = decode(p_include_nonnet,1,mil.availability_type,1)
                 )
              )
           )
/* nsinghi MIN-MAX INVCONV end */
	        AND  nvl(mmtt.planning_tp_type,2) = 2;

 
         ELSE /* default material status enabled of the org */


	   SELECT SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                  Sign(mtlt.primary_quantity)) * Abs( mtlt.primary_quantity ))
                , SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                  Sign(mtlt.secondary_quantity)) * Abs( NVL(mtlt.secondary_quantity, 0) ))
           INTO   l_mmtt_qty_src
                , l_mmtt_sqty_src
           FROM   apps.mtl_material_transactions_temp mmtt,apps.mtl_transaction_lots_temp mtlt
           WHERE  mmtt.organization_id = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.posting_flag = 'Y'
	   AND    mtlt.transaction_temp_id = mmtt.transaction_temp_id
           AND    mmtt.subinventory_code IS NOT NULL
	   AND    mmtt.subinventory_code IS NOT NULL
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id NOT IN (24,30)
           AND    EXISTS (SELECT 'x' FROM mtl_secondary_inventories msi
                  WHERE msi.organization_id = mmtt.organization_id
                  AND   msi.secondary_inventory_name = mmtt.subinventory_code
                  )
           AND    mmtt.planning_organization_id IS NULL
	   AND    EXISTS (SELECT 'x' FROM apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
                          WHERE  mtlt.transaction_temp_id = mmtt.transaction_temp_id
                          AND    mtlt.lot_number = mln.lot_number(+)
                          AND    p_org_id = mln.organization_id(+)
                          AND    p_item_id = mln.inventory_item_id(+)
                          AND    trunc(nvl(nvl(mtlt.lot_expiration_date,mln.expiration_Date),SYSDATE+1))> trunc(sysdate))
           AND (mmtt.locator_id IS NULL OR
                    (mmtt.locator_id IS NOT NULL AND
                     EXISTS (SELECT 'x' FROM mtl_item_locations mil
                            WHERE mmtt.organization_id = mil.organization_id
                            AND   mmtt.locator_id = mil.inventory_location_id
                            AND   mmtt.subinventory_code = mil.subinventory_code)))
	   AND EXISTS (SELECT 'x' FROM apps.mtl_material_statuses mms
		       WHERE mms.status_id= nvl(apps.INV_MATERIAL_STATUS_GRP.get_default_status(mmtt.organization_id,
	                                                        mmtt.inventory_item_id,
		                                            mmtt.subinventory_code,
				       		            mmtt.locator_id,
						            mtlt.lot_number,
          					            mmtt.lpn_id,  mmtt.transaction_action_id), mms.status_id)
                       AND mms.availability_type =1)
	   AND  nvl(mmtt.planning_tp_type,2) = 2;

 

           SELECT SUM(Abs(mtlt.primary_quantity))
                , SUM(Abs( NVL(mtlt.secondary_quantity, 0) ))
           INTO   l_mmtt_qty_dest
                , l_mmtt_sqty_dest
           FROM   apps.mtl_material_transactions_temp mmtt, apps.mtl_transaction_lots_temp mtlt
           WHERE  decode(mmtt.transaction_action_id,3,
                  mmtt.transfer_organization,mmtt.organization_id) = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.posting_flag = 'Y'
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id  in (2,28,3)
   	   AND    mtlt.transaction_temp_id = mmtt.transaction_temp_id
           AND
           (
              (mmtt.transfer_subinventory IS NULL)
              OR
              (
                 mmtt.transfer_subinventory IS NOT NULL
                 AND    EXISTS
                 (
                    SELECT 'x' FROM mtl_secondary_inventories msi
                    WHERE msi.organization_id = decode(mmtt.transaction_action_id,
                          3, mmtt.transfer_organization,mmtt.organization_id)
                       AND   msi.secondary_inventory_name = mmtt.transfer_subinventory
                 )
              )
           )
           AND    mmtt.planning_organization_id IS NULL
           AND    EXISTS
           (
              SELECT 'x' FROM apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
              WHERE  mtlt.transaction_temp_id = mmtt.transaction_temp_id
                 AND    mtlt.lot_number = mln.lot_number (+)
                 AND    decode(mmtt.transaction_action_id,
                                    3, mmtt.transfer_organization,mmtt.organization_id) = mln.organization_id(+)
                 AND    p_item_id = mln.inventory_item_id(+)
/* nsinghi MIN-MAX INVCONV start */
                  AND    trunc(nvl(nvl(mtlt.lot_expiration_Date,mln.expiration_date),sysdate+1))> trunc(sysdate)
           )
           AND
           (
              mmtt.transfer_to_location IS NULL OR
              (
                 mmtt.transfer_to_location IS NOT NULL AND
                 EXISTS
                 (
                    SELECT 'x' FROM mtl_item_locations mil
                    WHERE decode(mmtt.transaction_action_id,
                                     3, mmtt.transfer_organization,mmtt.organization_id) = mil.organization_id
                       AND   mmtt.transfer_to_location = mil.inventory_location_id
                       AND   mmtt.transfer_subinventory = mil.subinventory_code
                 )
              )
           )
           AND EXISTS (SELECT 'x' FROM apps.mtl_material_statuses mms
		       WHERE mms.status_id= nvl(apps.INV_MATERIAL_STATUS_GRP.get_default_status(decode(mmtt.transaction_action_id,3, mmtt.transfer_organization,mmtt.organization_id),
							                                 mmtt.inventory_item_id,
											 mmtt.transfer_subinventory,
										         mmtt.transfer_to_location,
										         mtlt.lot_number,
          										 mmtt.lpn_id,  mmtt.transaction_action_id,
											 apps.INV_MATERIAL_STATUS_GRP.get_default_status(mmtt.organization_id,
					                                                 mmtt.inventory_item_id,
						                                         mmtt.subinventory_code,
								       		         mmtt.locator_id,
										         mtlt.lot_number,
				          					         mmtt.lpn_id,  mmtt.transaction_action_id)), mms.status_id)
                       AND mms.availability_type =1)
/* nsinghi MIN-MAX INVCONV end */
	        AND  nvl(mmtt.planning_tp_type,2) = 2;

-- Rkatoori, For sub inventory transfer type, transfer_organization is null, so we have to do testing in that aspect..
-- If there are any issues, need to add decode for that..
	  END IF; /* End of IF l_default_status_id = -1 */

        ELSE /* non lot controlled */
	 -- Added the below if for 6633612
	 IF l_default_status_id = -1 THEN

  

	   SELECT SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                  Sign(mmtt.primary_quantity)) * Abs( mmtt.primary_quantity ))
                , SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                  Sign(mmtt.secondary_transaction_quantity)) * Abs( NVL(mmtt.secondary_transaction_quantity, 0) ))
           INTO   l_mmtt_qty_src
                , l_mmtt_sqty_src
           FROM  apps.mtl_material_transactions_temp mmtt
           WHERE  mmtt.organization_id = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.posting_flag = 'Y'
           AND    mmtt.subinventory_code IS NOT NULL
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id NOT IN (24,30)
           AND    EXISTS (select 'x' from mtl_secondary_inventories msi
                  WHERE msi.organization_id = mmtt.organization_id
                  AND   msi.secondary_inventory_name = mmtt.subinventory_code
                  AND    msi.availability_type = decode(p_include_nonnet,1,msi.availability_type,1))
           AND    mmtt.planning_organization_id IS NULL

/* nsinghi MIN-MAX INVCONV start */
           AND (mmtt.locator_id IS NULL OR
                    (mmtt.locator_id IS NOT NULL AND
                     EXISTS (select 'x' from mtl_item_locations mil
                            WHERE mmtt.organization_id = mil.organization_id
                            AND   mmtt.locator_id = mil.inventory_location_id
                            AND   mmtt.subinventory_code = mil.subinventory_code
                            AND   mil.availability_type = decode(p_include_nonnet,1,mil.availability_type,1))))
/* nsinghi MIN-MAX INVCONV end */

	        AND  nvl(mmtt.planning_tp_type,2) = 2;


           SELECT SUM(Abs(mmtt.primary_quantity))
                , SUM(Abs( NVL(mmtt.secondary_transaction_quantity, 0) ))
           INTO   l_mmtt_qty_dest
                , l_mmtt_sqty_dest
           FROM   apps.mtl_material_transactions_temp mmtt
           WHERE  decode(mmtt.transaction_action_id,3,
                  mmtt.transfer_organization,mmtt.organization_id) = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.posting_flag = 'Y'
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id  in (2,28,3)
           AND    ((mmtt.transfer_subinventory IS NULL) OR
                   (mmtt.transfer_subinventory IS NOT NULL
           AND    EXISTS (select 'x' from mtl_secondary_inventories msi
                     WHERE msi.organization_id = decode(mmtt.transaction_action_id,
                                                  3, mmtt.transfer_organization,mmtt.organization_id)
                     AND   msi.secondary_inventory_name = mmtt.transfer_subinventory
                     AND   msi.availability_type = decode(p_include_nonnet,1,msi.availability_type,1))))
           AND    mmtt.planning_organization_id IS NULL

/* nsinghi MIN-MAX INVCONV start */
           AND (mmtt.transfer_to_location IS NULL OR
                    (mmtt.transfer_to_location IS NOT NULL AND
                     EXISTS (select 'x' from mtl_item_locations mil
                            WHERE decode(mmtt.transaction_action_id,
                                     3, mmtt.transfer_organization,mmtt.organization_id) = mil.organization_id
                            AND   mmtt.transfer_to_location = mil.inventory_location_id
                            AND   mmtt.transfer_subinventory = mil.subinventory_code
                            AND   mil.availability_type = decode(p_include_nonnet,1,mil.availability_type,1))))
/* nsinghi MIN-MAX INVCONV end */

	        AND  nvl(mmtt.planning_tp_type,2) = 2;

	  ELSE	 /* default material status enabled of the org */


	   SELECT SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                  Sign(mmtt.primary_quantity)) * Abs( mmtt.primary_quantity ))
                , SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                  Sign(mmtt.secondary_transaction_quantity)) * Abs( NVL(mmtt.secondary_transaction_quantity, 0) ))
           INTO   l_mmtt_qty_src
                , l_mmtt_sqty_src
           FROM   apps.mtl_material_transactions_temp mmtt
           WHERE  mmtt.organization_id = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.posting_flag = 'Y'
           AND    mmtt.subinventory_code IS NOT NULL
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id NOT IN (24,30)
           AND    EXISTS (select 'x' from mtl_secondary_inventories msi
                  WHERE msi.organization_id = mmtt.organization_id
                  AND   msi.secondary_inventory_name = mmtt.subinventory_code)
           AND    mmtt.planning_organization_id IS NULL

/* nsinghi MIN-MAX INVCONV start */
           AND (mmtt.locator_id IS NULL OR
                    (mmtt.locator_id IS NOT NULL AND
                     EXISTS (select 'x' from mtl_item_locations mil
                            WHERE mmtt.organization_id = mil.organization_id
                            AND   mmtt.locator_id = mil.inventory_location_id
                            AND   mmtt.subinventory_code = mil.subinventory_code)))
/* nsinghi MIN-MAX INVCONV end */
	   AND EXISTS (SELECT 'x' FROM apps.mtl_material_statuses mms
		       WHERE mms.status_id= nvl(apps.INV_MATERIAL_STATUS_GRP.get_default_status(mmtt.organization_id,
	                                                        mmtt.inventory_item_id,
		                                            mmtt.subinventory_code,
				       		            mmtt.locator_id,
						            mmtt.lot_number,
          					            mmtt.lpn_id,  mmtt.transaction_action_id), mms.status_id)
                       AND mms.availability_type =1)
	        AND  nvl(mmtt.planning_tp_type,2) = 2;

  

           SELECT SUM(Abs(mmtt.primary_quantity))
                , SUM(Abs( NVL(mmtt.secondary_transaction_quantity, 0) ))
           INTO   l_mmtt_qty_dest
                , l_mmtt_sqty_dest
           FROM   apps.mtl_material_transactions_temp mmtt
           WHERE  decode(mmtt.transaction_action_id,3,
                  mmtt.transfer_organization,mmtt.organization_id) = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.posting_flag = 'Y'
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id  in (2,28,3)
           AND    ((mmtt.transfer_subinventory IS NULL) OR
                   (mmtt.transfer_subinventory IS NOT NULL
           AND    EXISTS (select 'x' from mtl_secondary_inventories msi
                     WHERE msi.organization_id = decode(mmtt.transaction_action_id,
                                                  3, mmtt.transfer_organization,mmtt.organization_id)
                     AND   msi.secondary_inventory_name = mmtt.transfer_subinventory)))
           AND    mmtt.planning_organization_id IS NULL

/* nsinghi MIN-MAX INVCONV start */
           AND (mmtt.transfer_to_location IS NULL OR
                    (mmtt.transfer_to_location IS NOT NULL AND
                     EXISTS (select 'x' from mtl_item_locations mil
                            WHERE decode(mmtt.transaction_action_id,
                                     3, mmtt.transfer_organization,mmtt.organization_id) = mil.organization_id
                            AND   mmtt.transfer_to_location = mil.inventory_location_id
                            AND   mmtt.transfer_subinventory = mil.subinventory_code)))
/* nsinghi MIN-MAX INVCONV end */
           AND EXISTS (SELECT 'x' FROM apps.mtl_material_statuses mms
		       WHERE mms.status_id= nvl(apps.INV_MATERIAL_STATUS_GRP.get_default_status(decode(mmtt.transaction_action_id,3, mmtt.transfer_organization,mmtt.organization_id),
							                                 mmtt.inventory_item_id,
											 mmtt.transfer_subinventory,
										         mmtt.transfer_to_location,
										         mmtt.lot_number,
          										 mmtt.lpn_id,  mmtt.transaction_action_id,
											 apps.INV_MATERIAL_STATUS_GRP.get_default_status(mmtt.organization_id,
					                                                 mmtt.inventory_item_id,
						                                         mmtt.subinventory_code,
								       		         mmtt.locator_id,
										         mmtt.lot_number,
				          					         mmtt.lpn_id,  mmtt.transaction_action_id)), mms.status_id)
                       AND mms.availability_type =1)

	        AND  nvl(mmtt.planning_tp_type,2) = 2;


	   END IF; /* End of IF l_default_status_id = -1 */

        END IF;

    -- Bug 4209192, adding below query to account for undelivered LPNs for WIP assembly completions.
     SELECT SUM(apps.inv_decimals_pub.get_primary_quantity( p_org_id
                                                         ,p_item_id
                                                         ,mtrl.uom_code
                                                         ,mtrl.quantity - NVL(mtrl.quantity_delivered,0))
                                                        )
        INTO  l_lpn_qty
        FROM  mtl_txn_request_lines mtrl, mtl_txn_request_headers mtrh, mtl_transaction_types mtt
        where mtrl.organization_id = p_org_id
        AND   mtrl.inventory_item_id = p_item_id
        AND   mtrl.header_id = mtrh.header_id
        AND   mtrh.move_order_type = 6 -- Putaway Move Order
        AND   mtrl.transaction_source_type_id = 5 -- Wip
        AND   mtt.transaction_action_id = 31 -- WIP Assembly Completion
        AND   mtt.transaction_type_id   = mtrl.transaction_type_id
        AND   mtrl.line_status = 7 -- Pre Approved
        AND   mtrl.lpn_id is not null;

  


    ELSIF (p_level = 2) THEN

/* nsinghi MIN-MAX INVCONV start */

/* If Min-Max Planning is run at sub-inventory level, value for include-nonnettable is always
assumned to be 1. Thus no need to check for nettablity when run at sub-inv level. */

/* nsinghi MIN-MAX INVCONV end */

    -- Subinventory level
       SELECT SUM(moq.primary_transaction_quantity)
            , SUM( NVL(moq.secondary_transaction_quantity, 0))
       INTO   l_moq_qty
            , l_moq_sqty
       FROM   mtl_onhand_quantities_detail moq, mtl_lot_numbers mln
       WHERE  moq.organization_id = p_org_id
       AND    moq.inventory_item_id = p_item_id
       AND    moq.subinventory_code = p_subinv
       AND    moq.lot_number = mln.lot_number(+)
       AND    moq.organization_id = mln.organization_id(+)
       AND    moq.inventory_item_id = mln.inventory_item_id(+)
       AND    trunc(nvl(mln.expiration_date, sysdate+1)) > trunc(sysdate);


       IF (l_lot_control = 2) THEN

           SELECT SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                      Sign(mmtt.primary_quantity)) * Abs( mmtt.primary_quantity ))
                , SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                Sign(mmtt.secondary_transaction_quantity)) * Abs( NVL(mmtt.secondary_transaction_quantity, 0) ))
           INTO   l_mmtt_qty_src
                , l_mmtt_sqty_src
           FROM   apps.mtl_material_transactions_temp mmtt
           WHERE  mmtt.organization_id = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.subinventory_code = p_subinv
           AND    mmtt.posting_flag = 'Y'
           AND    mmtt.subinventory_code IS NOT NULL
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    EXISTS (select 'x' from apps.mtl_transaction_lots_temp mtlt, apps.mtl_lot_numbers mln
                          WHERE  mtlt.transaction_temp_id = mmtt.transaction_temp_id
                          AND    mtlt.lot_number = mln.lot_number (+)
                          AND    p_org_id = mln.organization_id(+)
                          AND    p_item_id = mln.inventory_item_id(+)
                          AND    trunc(nvl(nvl(mtlt.lot_expiration_date,mln.expiration_Date),sysdate+1))> trunc(sysdate))
           AND    mmtt.transaction_action_id NOT IN (24,30);

  
           SELECT SUM(Abs(mmtt.primary_quantity))
                , SUM(Abs( NVL(mmtt.secondary_transaction_quantity, 0)))
           INTO   l_mmtt_qty_dest
                , l_mmtt_sqty_dest
           FROM   apps.mtl_material_transactions_temp mmtt
           WHERE  decode(mmtt.transaction_action_id,3,
                   mmtt.transfer_organization,mmtt.organization_id) = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.transfer_subinventory = p_subinv
           AND    mmtt.posting_flag = 'Y'
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    EXISTS (select 'x' from apps.mtl_transaction_lots_temp mtlt, mtl_lot_numbers mln
                         WHERE  mtlt.transaction_temp_id = mmtt.transaction_temp_id
                         AND    mtlt.lot_number = mln.lot_number (+)
                         AND    decode(mmtt.transaction_action_id,3,
                                      mmtt.transfer_organization,mmtt.organization_id) = mln.organization_id(+)
                         AND    p_item_id = mln.inventory_item_id(+)
                         AND    trunc(nvl(nvl(mtlt.lot_expiration_date,mln.expiration_Date),sysdate+1))> trunc(sysdate))
           AND    mmtt.transaction_action_id  in (2,28,3);

       ELSE
           SELECT SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                      Sign(mmtt.primary_quantity)) * Abs( mmtt.primary_quantity ))
                , SUM(Decode(mmtt.transaction_action_id, 1, -1, 2, -1, 28, -1, 3, -1,
                Sign(mmtt.secondary_transaction_quantity)) * Abs( NVL(mmtt.secondary_transaction_quantity, 0) ))
           INTO   l_mmtt_qty_src
                , l_mmtt_sqty_src
           FROM   apps.mtl_material_transactions_temp mmtt
           WHERE  mmtt.organization_id = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.subinventory_code = p_subinv
           AND    mmtt.posting_flag = 'Y'
           AND    mmtt.subinventory_code IS NOT NULL
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id NOT IN (24,30);



           SELECT SUM(Abs(mmtt.primary_quantity))
                , SUM(Abs( NVL(mmtt.secondary_transaction_quantity, 0)))
           INTO   l_mmtt_qty_dest
                , l_mmtt_sqty_dest
           FROM   apps.mtl_material_transactions_temp mmtt
           WHERE  decode(mmtt.transaction_action_id,3,
                   mmtt.transfer_organization,mmtt.organization_id) = p_org_id
           AND    mmtt.inventory_item_id = p_item_id
           AND    mmtt.transfer_subinventory = p_subinv
           AND    mmtt.posting_flag = 'Y'
           AND    Nvl(mmtt.transaction_status,0) <> 2
           AND    mmtt.transaction_action_id  in (2,28,3);

       END IF;

    END IF;

    -- Bug 4209192, adding undelivered LPN l_lpn_qty for WIP assembly completions in total onhand.
       l_qoh :=  nvl(l_moq_qty,0) + nvl(l_mmtt_qty_src,0) + nvl(l_mmtt_qty_dest,0) + nvl(l_lpn_qty,0);

    -- invConv change
    l_sqoh :=  nvl(l_moq_sqty,0) + nvl(l_mmtt_sqty_src,0) + nvl(l_mmtt_sqty_dest,0);



   x_qoh   := l_qoh;

   -- invConv changes begin
   IF (l_uom_ind = 'P')
   THEN
      -- This is not a DUOM item.
  
      x_sqoh   := NULL;
   ELSE
      x_sqoh   := l_sqoh;
   END IF;
   -- invConv changes end


EXCEPTION
    WHEN OTHERS THEN
        x_qoh   := NULL;
        x_sqoh  := NULL;

END GET_PLANNING_QUANTITY;

FUNCTION GET_PLANNING_QUANTITY(
     P_INCLUDE_NONNET  NUMBER
   , P_LEVEL           NUMBER
   , P_ORG_ID          NUMBER
   , P_SUBINV          VARCHAR2
   , P_ITEM_ID         NUMBER
) RETURN NUMBER IS

     l_qoh             NUMBER := 0;
     l_sqoh            NUMBER := NULL;
     l_debug           NUMBER := NVL(FND_PROFILE.VALUE('INV_DEBUG_TRACE'),0);

BEGIN


-- invConv changes begin :
-- Calling the new GET_PLANNING_QUANTITY procedure
GET_PLANNING_QUANTITY(
     P_INCLUDE_NONNET  => P_INCLUDE_NONNET
   , P_LEVEL           => P_LEVEL
   , P_ORG_ID          => P_ORG_ID
   , P_SUBINV          => P_SUBINV
   , P_ITEM_ID         => P_ITEM_ID
   , P_GRADE_CODE      => NULL                        -- invConv change
   , X_QOH             => l_qoh                       -- invConv change
   , X_SQOH            => l_sqoh);                    -- invConv change
-- invConv changes end.



    RETURN(l_qoh);


EXCEPTION
    WHEN OTHERS THEN
        RETURN(0);

END GET_PLANNING_QUANTITY;				
  
  Function get_org_rec_qty (p_inventory_item_id in number, p_organization_id in number) return number
is
l_open_qty number;
begin
select sum(qty) 
        into l_open_qty
        from
        (SELECT NVL(SUM(to_org_primary_quantity), 0) QTY          
        FROM apps.mtl_supply sup
        WHERE sup.supply_type_code            IN ('SHIPMENT')  -- Need to check the supply_type_codde
        AND sup.destination_type_code          = 'INVENTORY'
        AND sup.from_organization_id           = p_organization_id
        AND sup.intransit_owning_org_id is not null
        AND sup.item_id                        = p_inventory_item_id);
        
        return (l_open_qty);
        
        exception
        when others then 
        return (0);
        end get_org_rec_qty;
  
End;
/