CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_PO_ISR_MST_TAB_V" ("ORG", "ORGANIZATION_NAME", "DISTRICT", "REGION", "PRE", "ITEM_NUMBER", "VENDOR_NAME", "VENDOR_NUMBER", "SOURCE", "ST", "DESCRIPTION", "CAT_CLASS", "INV_CAT_SEG1", "PPLT", "PLT", "UOM", "CL", "STK", "PM", "MIN", "MAX", "HIT_6", "AVER_COST", "ITEM_COST", "BPA_COST", "BPA", "QOH", "ON_ORD", "AVAIL", "AVAIL_D", "BIN_LOC", "MC", "FI", "FREEZE_DATE", "RES", "THIRTEEN_WK_AVG_INV", "THIRTEEN_WK_AN_COGS", "TURNS", "BUYER", "TS", "AMU", "SO", "MF_FLAG", "WT", "SS", "SOURCING_RULE", "FML", "OPEN_REQ", "INVENTORY_ITEM_ID", "ORGANIZATION_ID", "SET_OF_BOOKS_ID", "CLT", "AVAIL2", "SUPPLY", "DEMAND", "INT_REQ", "DIR_REQ", "ITEM_STATUS_CODE", "VENDOR_SITE", "SOURCE_ORGANIZATION_ID")
AS
  SELECT ood.organization_code org,
    ood.organization_name organization_name,
    mtp.attribute8 district,
    mtp.attribute9 region,
    SUBSTR (msi.segment1, 1, 3) pre,
    msi.segment1 item_number,
    --xxeis.EIS_PO_XXWC_ISR_UTIL_PKG.get_vendor_name(msi.inventory_item_id, msi.organization_id) vendor_name,
    --xxeis.EIS_PO_XXWC_ISR_UTIL_PKG.get_vendor_number(msi.inventory_item_id, msi.organization_id) vendor_number,
    pov.vendor_name vendor_name,
    pov.segment1 vendor_number,
    CASE
      WHEN item_source_type.meaning = 'Supplier'
      THEN pov.segment1
      WHEN item_source_type.meaning = 'Inventory'
      THEN
        (SELECT organization_code
        FROM org_organization_definitions source_org
        WHERE source_org.organization_id = msi.source_organization_id
        )
      ELSE NULL
    END source,
    CASE
      WHEN item_source_type.meaning = 'Supplier'
      THEN 'S'
      WHEN item_source_type.meaning = 'Inventory'
      THEN 'I'
      ELSE NULL
    END st,
    msi.description description,
    mcvc.segment2 cat_class,
    mcvc.segment1 inv_cat_seg1,
    preprocessing_lead_time pplt,
    msi.full_lead_time plt,
    msi.primary_uom_code uom,
    mcvs.segment1 cl,
    CASE
      WHEN (mcvs.segment1 IN ('1', '2', '3', '4', '5', '6', '7', '8', '9', 'C','B'))
      THEN 'Y'
      WHEN ( mcvs.segment1    IN ('E')
      AND (min_minmax_quantity = 0
      AND max_minmax_quantity  = 0))
      THEN 'N'
      WHEN (mcvs.segment1     IN ('E')
      AND (min_minmax_quantity > 0
      AND max_minmax_quantity  > 0))
      THEN 'Y'
      WHEN (mcvs.segment1 IN ('N', 'Z'))
        --AND ITEM_TYPE                                                                                           ='NON-STOCK')
      THEN 'N'
      ELSE 'N'
    END stk,
    mrp_planning_code.meaning pm,
    min_minmax_quantity MIN,
    max_minmax_quantity MAX,
    NULL hit_6,
    NVL (apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) aver_cost,
    list_price_per_unit item_cost,
    0 bpa_cost,
    0 bpa,
    0 QOH,
    0 ON_ORD,
    0 avail,
    0 avail_d,
    xxeis.eis_po_xxwc_isr_util_pkg.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) bin_loc,
    NULL mc,
    mcvp.segment1 fi,
    NULL freeze_date,
    msi.attribute21 res,
    NULL thirteen_wk_avg_inv,
    NULL thirteen_wk_an_cogs,
    NULL turns,
    ppf.full_name buyer,
    shelf_life_days ts,
    msi.attribute20 amu,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_ss_cnt ( msi.inventory_item_id, msi.organization_id) so,
    NULL mf_flag,
    msi.unit_weight wt,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_ss (msi.inventory_item_id, msi.organization_id) ss,
    msr.sourcing_rule_name sourcing_rule,
    msi.fixed_lot_multiplier fml,
    0 open_req,
    msi.inventory_item_id,
    msi.organization_id,
    ood.set_of_books_id,
    MSI.ATTRIBUTE30 CLT,
    -- xxeis.eis_po_xxwc_isr_util_pkg.GET_INT_REQ_SO_QTY(msi.inventory_item_id, msi.organization_id)avail2
    0 avail2,
    --(xxeis.eis_po_xxwc_isr_util_pkg.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)                           - XXEIS.eis_po_xxwc_isr_util_pkg.GET_DEMAND_QTY( MSI.ORGANIZATION_ID,NULL,1,MSI.INVENTORY_ITEM_ID,sysdate,2,1,1,1,'Y'))avail2,
    0 supply,
    0 demand,
    0 int_req,
    0 dir_req,
    msi.INVENTORY_ITEM_STATUS_CODE ITEM_STATUS_CODE,
    pvs.vendor_site_code vendor_site,
    msi.source_organization_id
  FROM mtl_system_items_b msi,
    org_organization_definitions ood,
    Mtl_Categories_Kfv mcvs,
    Mtl_Item_Categories mics,
    Mtl_Categories_Kfv mcvp,
    Mtl_Item_Categories micp,
    Mtl_Categories_Kfv mcvc,
    Mtl_Item_Categories micc,
    mrp_sr_assignments msa,
    mrp_sr_receipt_org msro,
    mrp_sr_source_org msso,
    mrp_sourcing_rules msr,
    po_vendors pov,
    po_vendor_sites pvs,
    mfg_lookups mrp_planning_code,
    mfg_lookups item_source_type,
    per_people_x ppf,
    mtl_parameters mtp,
    mfg_lookups sfty_stk
  WHERE msi.organization_id            = ood.organization_id
  AND msi.buyer_id                     = ppf.person_id(+)
  AND msi.inventory_item_id            = mics.inventory_item_id(+)
  AND msi.organization_id              = mics.organization_id(+)
  AND mics.Category_Id                 = mcvs.Category_Id(+)
  AND mcvs.Structure_Id(+)             = 50410
  AND mics.category_set_id(+)          = 1100000044
  AND msi.inventory_item_id            = micp.inventory_item_id(+)
  AND msi.organization_id              = micp.organization_id(+)
  AND micp.Category_Id                 = mcvp.Category_Id(+)
  AND mcvp.Structure_Id(+)             = 50408
  AND micp.category_set_id(+)          = 1100000043
  AND msi.inventory_item_id            = micc.inventory_item_id
  AND msi.organization_id              = micc.organization_id
  AND micc.Category_Id                 = mcvc.Category_Id
  AND mcvc.Structure_Id                = 101
  AND micc.category_set_id             = 1100000062
  AND msi.inventory_item_id            = msa.inventory_item_id(+)
  AND msi.organization_id              = msa.organization_id(+)
  AND msa.sourcing_rule_id             = msro.sourcing_rule_id(+)
  AND msa.sourcing_rule_id             = msr.sourcing_rule_id(+)
  AND msro.sr_receipt_id               = msso.sr_receipt_id(+)
  AND msso.vendor_id                   = pov.vendor_id(+)
  AND msso.vendor_site_id              = pvs.vendor_site_id(+)
  AND mrp_planning_code.lookup_type(+) = 'MTL_MATERIAL_PLANNING'
  AND mrp_planning_code.lookup_code(+) = msi.inventory_planning_code
  AND item_source_type.lookup_type(+)  = 'MTL_SOURCE_TYPES'
  AND item_source_type.lookup_code(+)  = msi.source_type
  AND msi.organization_id              = mtp.organization_id
  AND mtp.organization_id              = 222
  AND sfty_stk.lookup_type(+)          = 'MTL_SAFETY_STOCK_TYPE'  
  AND SFTY_STK.LOOKUP_CODE(+)          = MSI.MRP_SAFETY_STOCK_CODE
/