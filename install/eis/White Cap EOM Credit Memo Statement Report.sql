--Report Name            : White Cap EOM Credit Memo Statement Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C',222,'','','','','ID020048','XXEIS','Eis Xxwc Ar Credit Memo Dtls C','EXACMDC','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_CREDIT_MEMO_DTLS_C',222,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','ORIGIN_DATE',222,'Origin Date','ORIGIN_DATE','','','','ID020048','DATE','','','Origin Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','TAKEN_BY',222,'Taken By','TAKEN_BY','','','','ID020048','VARCHAR2','','','Taken By','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','MASTER_NAME',222,'Master Name','MASTER_NAME','','','','ID020048','VARCHAR2','','','Master Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','ID020048','VARCHAR2','','','Customer Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CUSTOMER_TERRITORY',222,'Customer Territory','CUSTOMER_TERRITORY','','','','ID020048','VARCHAR2','','','Customer Territory','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','SALES_REP_NO',222,'Sales Rep No','SALES_REP_NO','','','','ID020048','VARCHAR2','','','Sales Rep No','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','QTY_SHIPPED',222,'Qty Shipped','QTY_SHIPPED','','~T~D~2','','ID020048','NUMBER','','','Qty Shipped','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_LINE_AMOUNT',222,'Invoice Line Amount','INVOICE_LINE_AMOUNT','','~T~D~2','','ID020048','NUMBER','','','Invoice Line Amount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CREDIT_REASON_SALE',222,'Credit Reason Sale','CREDIT_REASON_SALE','','','','ID020048','VARCHAR2','','','Credit Reason Sale','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','PART_NUMBER',222,'Part Number','PART_NUMBER','','','','ID020048','VARCHAR2','','','Part Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','ORIGINAL_INVOICE_NUMBER',222,'Original Invoice Number','ORIGINAL_INVOICE_NUMBER','','','','ID020048','VARCHAR2','','','Original Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','ID020048','VARCHAR2','','','Invoice Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','TAX_AMT',222,'Tax Amt','TAX_AMT','','~T~D~2','','ID020048','NUMBER','','','Tax Amt','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','AMOUNT_PAID',222,'Amount Paid','AMOUNT_PAID','','~T~D~2','','ID020048','NUMBER','','','Amount Paid','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','LOCATION',222,'Location','LOCATION','','','','ID020048','VARCHAR2','','','Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_TOTAL',222,'Invoice Total','INVOICE_TOTAL','','~T~D~2','','ID020048','NUMBER','','','Invoice Total','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','FREIGHT_ORIGINAL',222,'Freight Original','FREIGHT_ORIGINAL','','~T~D~2','','ID020048','NUMBER','','','Freight Original','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','TAX_RATE',222,'Tax Rate','TAX_RATE','','~T~D~2','','ID020048','NUMBER','','','Tax Rate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','BUSINESS_DATE',222,'Business Date','BUSINESS_DATE','','','','ID020048','DATE','','','Business Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_DATE',222,'Invoice Date','INVOICE_DATE','','','','ID020048','DATE','','','Invoice Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','INVOICE_TYPE',222,'Invoice Type','INVOICE_TYPE','','','','ID020048','VARCHAR2','','','Invoice Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','SHIP_NAME',222,'Ship Name','SHIP_NAME','','','','ID020048','VARCHAR2','','','Ship Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','AR_CUSTOMER_NUMBER',222,'Ar Customer Number','AR_CUSTOMER_NUMBER','','','','ID020048','VARCHAR2','','','Ar Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','ID020048','VARCHAR2','','','Customer Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','FISCAL_MONTH',222,'Fiscal Month','FISCAL_MONTH','','','','ID020048','VARCHAR2','','','Fiscal Month','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','FISCAL_YEAR',222,'Fiscal Year','FISCAL_YEAR','','','','ID020048','NUMBER','','','Fiscal Year','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','ID020048','VARCHAR2','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','ORGINAL_ORDER_NUMBER',222,'Orginal Order Number','ORGINAL_ORDER_NUMBER','','','','ID020048','VARCHAR2','','','Orginal Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','RESTOCK_FEE_PERCENT',222,'Restock Fee Percent','RESTOCK_FEE_PERCENT','','','','ID020048','VARCHAR2','','','Restock Fee Percent','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','CREDIT',222,'Credit','CREDIT','','','','ID020048','VARCHAR2','','','Credit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','GLNO',222,'Glno','GLNO','','','','ID020048','VARCHAR2','','','Glno','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','PROCESS_ID',222,'Process Id','PROCESS_ID','','','','ID020048','NUMBER','','','Process Id','','','','US');
--Inserting Object Components for EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
--Inserting Object Component Joins for EIS_XXWC_AR_CREDIT_MEMO_DTLS_C
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for White Cap EOM Credit Memo Statement Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - White Cap EOM Credit Memo Statement Report
xxeis.eis_rsc_ins.lov( 222,'SELECT organization_code,organization_id,organization_name FROM org_organization_definitions','','AR Organizaion Code LOV','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_NAME
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_NAME','','Fiscal Month','','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT DISTINCT GPS.PERIOD_YEAR
FROM      HR_OPERATING_UNITS          HOU,
          GL_LEDGERS                  GL,
          gl_period_statuses          gps
WHERE
 GL.LEDGER_ID                      = HOU.SET_OF_BOOKS_ID
AND GL.LEDGER_ID                      = GPS.SET_OF_BOOKS_ID
AND GL.ACCOUNTED_PERIOD_TYPE          = GPS.PERIOD_TYPE
order by GPS.PERIOD_YEAR
','','Fiscal Year','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for White Cap EOM Credit Memo Statement Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - White Cap EOM Credit Memo Statement Report
xxeis.eis_rsc_utility.delete_report_rows( 'White Cap EOM Credit Memo Statement Report' );
--Inserting Report - White Cap EOM Credit Memo Statement Report
xxeis.eis_rsc_ins.r( 222,'White Cap EOM Credit Memo Statement Report','','','','','','SA059956','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','Y','','','SA059956','','Y','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - White Cap EOM Credit Memo Statement Report
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_TYPE','Invoice Type','Invoice Type','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'ORIGIN_DATE','Origin Date','Origin Date','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'SHIP_NAME','Ship Name','Ship Name','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'TAX_RATE','Tax Rate','Tax Rate','','~T~D~0','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'AMOUNT_PAID','Amount Paid','Amount Paid','','~T~D~2','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'BUSINESS_DATE','Business Date','Business Date','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'AR_CUSTOMER_NUMBER','Ar Customer Number','Ar Customer Number','','','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'FISCAL_MONTH','Fiscal Month','Fiscal Month','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_TOTAL','Invoice Total','Invoice Total','','~T~D~0','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'LOCATION','Location','Location','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'TAX_AMT','Tax Amt','Tax Amt','','~T~D~2','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'CREDIT_REASON_SALE','Credit Reason Sale','Credit Reason Sale','','','default','','21','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','26','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'FISCAL_YEAR','Fiscal Year','Fiscal Year','','~~~','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'FREIGHT_ORIGINAL','Freight Original','Freight Original','','~T~D~2','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'CUSTOMER_TERRITORY','Customer Territory','Customer Territory','','','default','','25','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'TAKEN_BY','Taken By','Taken By','','','default','','29','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'INVOICE_LINE_AMOUNT','Invoice Line Amount','Invoice Line Amount','','~T~D~2','default','','22','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'MASTER_NAME','Master Name','Master Name','','','default','','27','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'ORIGINAL_INVOICE_NUMBER','Original Invoice Number','Original Invoice Number','','','default','','19','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'PART_NUMBER','Part Number','Part Number','','','default','','20','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'QTY_SHIPPED','Qty Shipped','Qty Shipped','','~~~','default','','23','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'SALES_REP_NO','Sales Rep No','Sales Rep No','','','default','','24','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'ORDER_NUMBER','Order Number','Order Number','','','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'ORGINAL_ORDER_NUMBER','Orginal Order Number','Orginal Order Number','','','default','','28','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap EOM Credit Memo Statement Report',222,'RESTOCK_FEE_PERCENT','Restock Fee Percent','Restock Fee Percent','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','','US','');
--Inserting Report Parameters - White Cap EOM Credit Memo Statement Report
xxeis.eis_rsc_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Fiscal Year','Fiscal Year','FISCAL_YEAR','IN','Fiscal Year','SELECT distinct period_year FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','NUMBER','Y','Y','1','N','N','SQL','SA059956','Y','N','','','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Fiscal Month','Fiscal Month','FISCAL_MONTH','IN','Fiscal Month','SELECT distinct period_name FROM GL_PERIOD_STATUSES WHERE  APPLICATION_ID=101 AND CLOSING_STATUS=''O'' and sysdate between start_date and end_date','VARCHAR2','Y','Y','2','N','N','SQL','SA059956','Y','N','','','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Location','Location','LOCATION','IN','AR Organizaion Code LOV','','VARCHAR2','N','Y','3','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Origin Date From','Origin Date From','ORIGIN_DATE','>=','','','DATE','N','Y','4','Y','N','CONSTANT','SA059956','Y','N','','Start Date','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap EOM Credit Memo Statement Report',222,'Origin Date To','Origin Date To','ORIGIN_DATE','<=','','','DATE','N','Y','5','Y','N','CONSTANT','SA059956','Y','N','','End Date','','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','','','US','');
--Inserting Dependent Parameters - White Cap EOM Credit Memo Statement Report
--Inserting Report Conditions - White Cap EOM Credit Memo Statement Report
xxeis.eis_rsc_ins.rcnh( 'White Cap EOM Credit Memo Statement Report',222,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and process_id= :SYSTEM.PROCESS_ID','1',222,'White Cap EOM Credit Memo Statement Report','Free Text ');
--Inserting Report Sorts - White Cap EOM Credit Memo Statement Report
--Inserting Report Triggers - White Cap EOM Credit Memo Statement Report
xxeis.eis_rsc_ins.rt( 'White Cap EOM Credit Memo Statement Report',222,'BEGIN
XXEIS.eis_xxwc_credit_stmt_pkg.Populate_credit_invoices(
P_Period_Year => :Fiscal Year,
P_period_name => :Fiscal Month,
P_Process_id => :SYSTEM.PROCESS_ID,
p_location => :Location,
p_Origin_Date_From => :Origin Date From,
p_Origin_Date_To => :Origin Date To
);
END;','B','Y','SA059956','AQ');
--inserting report templates - White Cap EOM Credit Memo Statement Report
--Inserting Report Portals - White Cap EOM Credit Memo Statement Report
--inserting report dashboards - White Cap EOM Credit Memo Statement Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'White Cap EOM Credit Memo Statement Report','222','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','EIS_XXWC_AR_CREDIT_MEMO_DTLS_C','N','');
--inserting report security - White Cap EOM Credit Memo Statement Report
xxeis.eis_rsc_ins.rsec( 'White Cap EOM Credit Memo Statement Report','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap EOM Credit Memo Statement Report','','PP018915','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap EOM Credit Memo Statement Report','','LA023190','',222,'SA059956','','','');
--Inserting Report Pivots - White Cap EOM Credit Memo Statement Report
--Inserting Report   Version details- White Cap EOM Credit Memo Statement Report
xxeis.eis_rsc_ins.rv( 'White Cap EOM Credit Memo Statement Report','','White Cap EOM Credit Memo Statement Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
