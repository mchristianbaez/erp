--Report Name            : Cycle Count Item Rejection
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_CYCLE_REJECT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_CYCLE_REJECT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_CYCLE_REJECT_V',401,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Cycle Reject V','EXCRV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_CYCLE_REJECT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_CYCLE_REJECT_V',401,FALSE);
--Inserting Object Columns for EIS_XXWC_CYCLE_REJECT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','BEGINING_COUNT_DATE',401,'Begining Count Date','BEGINING_COUNT_DATE','','','','ANONYMOUS','DATE','','','Begining Count Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','FULL_NAME',401,'Full Name','FULL_NAME','','','','ANONYMOUS','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','AVG_COST',401,'Avg Cost','AVG_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Avg Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','TS',401,'Ts','TS','','','','ANONYMOUS','CHAR','','','Ts','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','ITEM_DESCRIPTION',401,'Item Description','ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Item Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','ITEM_NUMBER',401,'Item Number','ITEM_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Item Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','PRIMARY_UOM_CODE',401,'Primary Uom Code','PRIMARY_UOM_CODE','','','','ANONYMOUS','VARCHAR2','','','Primary Uom Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','SV_CLASS',401,'Sv Class','SV_CLASS','','','','ANONYMOUS','VARCHAR2','','','Sv Class','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','CAT_CALSS',401,'Cat Calss','CAT_CALSS','','','','ANONYMOUS','VARCHAR2','','','Cat Calss','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','COUNT_DATE',401,'Count Date','COUNT_DATE','','','','ANONYMOUS','DATE','','','Count Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','REGION',401,'Region','REGION','','','','ANONYMOUS','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','DISTRICT',401,'District','DISTRICT','','','','ANONYMOUS','VARCHAR2','','','District','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','ORG_CODE',401,'Org Code','ORG_CODE','','','','ANONYMOUS','VARCHAR2','','','Org Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','INVENTORY_ITEM_ID',401,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','PRIOR_COUNT_DATE',401,'Prior Count Date','PRIOR_COUNT_DATE','','','','ANONYMOUS','DATE','','','Prior Count Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','END_COUNT_DATE',401,'End Count Date','END_COUNT_DATE','','','','ANONYMOUS','DATE','','','End Count Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','ORG',401,'Org','ORG','','','','ANONYMOUS','VARCHAR2','','','Org','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','STATUS',401,'Status','STATUS','','','','ANONYMOUS','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','COUNTED_BY_EMPLOYEE_ID_CURRENT',401,'Counted By Employee Id Current','COUNTED_BY_EMPLOYEE_ID_CURRENT','','','','ANONYMOUS','NUMBER','','','Counted By Employee Id Current','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','COUNTED_BY_EMPLOYEE_ID_FIRST',401,'Counted By Employee Id First','COUNTED_BY_EMPLOYEE_ID_FIRST','','','','ANONYMOUS','NUMBER','','','Counted By Employee Id First','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_CYCLE_REJECT_V','REJECTED_DATE',401,'Rejected Date','REJECTED_DATE','','','','ANONYMOUS','DATE','','','Rejected Date','','','','US');
--Inserting Object Components for EIS_XXWC_CYCLE_REJECT_V
--Inserting Object Component Joins for EIS_XXWC_CYCLE_REJECT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report LOV Data for Cycle Count Item Rejection
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Cycle Count Item Rejection
xxeis.eis_rsc_ins.lov( 401,'select list_name from XXWC_PARAM_LIST where list_type = ''Org'' and (PUBLIC_FLAG =''Y'' OR USER_ID = fnd_global.user_id)','','XXWC Org List','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE9 Region from mtl_parameters','','Region Lov','','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'SELECT organization_code code, organization_name name
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE EXISTS
  (SELECT 1
  FROM XXEIS.EIS_ORG_ACCESS_V
  WHERE organization_id = ood.organization_id
  )
ORDER BY organization_code','','EIS XXWC INV ORGS','XXWC list of inventort orgs','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 401,'select distinct ATTRIBUTE8 District from mtl_parameters','','District Lov','District Lov','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
prompt Creating Report Data for Cycle Count Item Rejection
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(401);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Cycle Count Item Rejection
xxeis.eis_rsc_utility.delete_report_rows( 'Cycle Count Item Rejection' );
--Inserting Report - Cycle Count Item Rejection
xxeis.eis_rsc_ins.r( 401,'Cycle Count Item Rejection','','Details which items were rejected from a count at a specific organization','','','','XXEIS_RS_ADMIN','EIS_XXWC_CYCLE_REJECT_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Cycle Count Item Rejection
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'AVG_COST','Avg Cost','Avg Cost','','~T~D~2','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'CAT_CALSS','Cat Calss','Cat Calss','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'COUNT_DATE','Count Date','Count Date','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'DISTRICT','District','District','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'FULL_NAME','Rejected By','Full Name','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'ITEM_DESCRIPTION','Item Description','Item Description','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'ITEM_NUMBER','Item#','Item Number','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'PRIMARY_UOM_CODE','UOM','Primary Uom Code','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'REGION','Region','Region','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'SV_CLASS','SV Class','Sv Class','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'TS','TS','Ts','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'ORG_CODE','Organization','Org Code','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Cycle Count Item Rejection',401,'REJECTED_DATE','Rejected Date','Rejected Date','','','','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_CYCLE_REJECT_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Cycle Count Item Rejection
xxeis.eis_rsc_ins.rp( 'Cycle Count Item Rejection',401,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CYCLE_REJECT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Item Rejection',401,'District','District','DISTRICT','IN','District Lov','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CYCLE_REJECT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Item Rejection',401,'Org','Org','ORG_CODE','IN','EIS XXWC INV ORGS','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CYCLE_REJECT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Item Rejection',401,'OrgList','Org List','','IN','XXWC Org List','','VARCHAR2','N','Y','4','N','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CYCLE_REJECT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Item Rejection',401,'Count Date Beginning','Count Date Beginning','BEGINING_COUNT_DATE','>=','','','DATE','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CYCLE_REJECT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Cycle Count Item Rejection',401,'Count date ending','Count date ending','BEGINING_COUNT_DATE','<=','','','DATE','N','Y','6','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','EIS_XXWC_CYCLE_REJECT_V','','','US','');
--Inserting Dependent Parameters - Cycle Count Item Rejection
--Inserting Report Conditions - Cycle Count Item Rejection
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Item Rejection',401,'BEGINING_COUNT_DATE >= :Count Date Beginning ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BEGINING_COUNT_DATE','','Count Date Beginning','','','','','EIS_XXWC_CYCLE_REJECT_V','','','','','','GREATER_THAN_EQUALS','Y','Y','','','','','1',401,'Cycle Count Item Rejection','BEGINING_COUNT_DATE >= :Count Date Beginning ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Item Rejection',401,'BEGINING_COUNT_DATE <= :Count date ending ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BEGINING_COUNT_DATE','','Count date ending','','','','','EIS_XXWC_CYCLE_REJECT_V','','','','','','LESS_THAN_EQUALS','Y','Y','','','','','1',401,'Cycle Count Item Rejection','BEGINING_COUNT_DATE <= :Count date ending ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Item Rejection',401,'DISTRICT IN :District ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','DISTRICT','','District','','','','','EIS_XXWC_CYCLE_REJECT_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Item Rejection','DISTRICT IN :District ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Item Rejection',401,'ORG_CODE IN :Org ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ORG_CODE','','Org','','','','','EIS_XXWC_CYCLE_REJECT_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Item Rejection','ORG_CODE IN :Org ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Item Rejection',401,'REGION IN :Region ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_XXWC_CYCLE_REJECT_V','','','','','','IN','Y','Y','','','','','1',401,'Cycle Count Item Rejection','REGION IN :Region ');
xxeis.eis_rsc_ins.rcnh( 'Cycle Count Item Rejection',401,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND ( :OrgList is NULL  OR
          EXISTS  (SELECT 1
  from xxeis.eis_xxwc_param_parse_list x
  where x.list_name     =:OrgList
  and x.list_type     =''Org''
 and x.process_id    = :system.process_id
and  EXCRV.ORG_CODE =regexp_replace(x.list_value,''[^[a-z,A-Z,0-9]]*'')))
 ','1',401,'Cycle Count Item Rejection','Free Text ');
--Inserting Report Sorts - Cycle Count Item Rejection
--Inserting Report Triggers - Cycle Count Item Rejection
xxeis.eis_rsc_ins.rt( 'Cycle Count Item Rejection',401,'Declare 
L_TEMP 		varchar2(240);
begin
L_TEMP 		:=:OrgList ;
if l_temp is not null then 
XXEIS.EIS_RS_XXWC_COM_UTIL_PKG.PARSE_PARAM_LIST(P_PROCESS_ID => :SYSTEM.PROCESS_ID,
						P_LIST_NAME => L_TEMP ,
						P_LIST_TYPE => ''Org''
						);

end if;
end;','B','Y','XXEIS_RS_ADMIN','');
xxeis.eis_rsc_ins.rt( 'Cycle Count Item Rejection',401,'BEGIN
xxeis.EIS_RS_XXWC_COM_UTIL_PKG.parse_cleanup_table(P_PROCESS_ID	=> :SYSTEM.PROCESS_ID);
END;','A','Y','XXEIS_RS_ADMIN','');
--inserting report templates - Cycle Count Item Rejection
--Inserting Report Portals - Cycle Count Item Rejection
--inserting report dashboards - Cycle Count Item Rejection
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Cycle Count Item Rejection','401','EIS_XXWC_CYCLE_REJECT_V','EIS_XXWC_CYCLE_REJECT_V','N','');
--inserting report security - Cycle Count Item Rejection
xxeis.eis_rsc_ins.rsec( 'Cycle Count Item Rejection','401','','XXWC_INVENTORY_SUPER_USER',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Item Rejection','401','','XXWC_INVENTORY_SPEC_SCC',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Item Rejection','401','','XXWC_INVENTORY_CONTROL_SR_MGR',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Item Rejection','401','','XXWC_INV_ACCOUNTANT',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Item Rejection','401','','HDS_INVNTRY',401,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'Cycle Count Item Rejection','201','','XXWC_PURCHASING_INQUIRY',401,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - Cycle Count Item Rejection
xxeis.eis_rsc_ins.rpivot( 'Cycle Count Item Rejection',401,'Rejection Count','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Rejection Count
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Item Rejection',401,'Rejection Count','DISTRICT','PAGE_FIELD','','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Item Rejection',401,'Rejection Count','REGION','PAGE_FIELD','','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Item Rejection',401,'Rejection Count','ITEM_NUMBER','DATA_FIELD','COUNT','','1','','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'Cycle Count Item Rejection',401,'Rejection Count','FULL_NAME','ROW_FIELD','','','1','1','');
--Inserting Report Summary Calculation Columns For Pivot- Rejection Count
--Inserting Report   Version details- Cycle Count Item Rejection
xxeis.eis_rsc_ins.rv( 'Cycle Count Item Rejection','','Cycle Count Item Rejection','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 401');
END IF;
END;
/
