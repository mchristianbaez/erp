--Report Name            : Customer Account Contacts Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_AR_CUST_CONT_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_CUST_CONT_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_CUST_CONT_V',222,'','','','','ANONYMOUS','XXEIS','Eis Xxwc Ar Cust Cont V','EXACCV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_AR_CUST_CONT_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_CUST_CONT_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_CUST_CONT_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','FULL_NAME',222,'Full Name','FULL_NAME','','','','ANONYMOUS','VARCHAR2','','','Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','ACCOUNT_NAME',222,'Account Name','ACCOUNT_NAME','','','','ANONYMOUS','VARCHAR2','','','Account Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','ACCOUNT_NUMBER',222,'Account Number','ACCOUNT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Account Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','ADDRESS',222,'Address','ADDRESS','','','','ANONYMOUS','VARCHAR2','','','Address','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','PHONE_NUMBER',222,'Phone Number','PHONE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Phone Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','ROLE',222,'Role','ROLE','','','','ANONYMOUS','VARCHAR2','','','Role','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','CONTACT_NUMBER',222,'Contact Number','CONTACT_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Contact Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','FAX',222,'Fax','FAX','','','','ANONYMOUS','VARCHAR2','','','Fax','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','SALESPERSON',222,'Salesperson','SALESPERSON','','','','ANONYMOUS','VARCHAR2','','','Salesperson','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','STATUS',222,'Status','STATUS','','','','ANONYMOUS','VARCHAR2','','','Status','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','EMAIL_ADDRESS',222,'Email Address','EMAIL_ADDRESS','','','','ANONYMOUS','VARCHAR2','','','Email Address','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','SITE_NAME',222,'Site Name','SITE_NAME','','','','ANONYMOUS','VARCHAR2','','','Site Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','SITE_NUMBER',222,'Site Number','SITE_NUMBER','','','','ANONYMOUS','VARCHAR2','','','Site Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','ANONYMOUS','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','LOCATION_ID',222,'Location Id','LOCATION_ID','','','','ANONYMOUS','NUMBER','','','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','ACCOUNT_STATUS',222,'Account Status','ACCOUNT_STATUS','','','','ANONYMOUS','VARCHAR2','','','Account Status','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','BMGR',222,'Bmgr','BMGR','','','','ANONYMOUS','VARCHAR2','','','Bmgr','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','CREDIT_MANAGER',222,'Credit Manager','CREDIT_MANAGER','','','','ANONYMOUS','VARCHAR2','','','Credit Manager','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','DMGR',222,'Dmgr','DMGR','','','','ANONYMOUS','VARCHAR2','','','Dmgr','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','DSMGR',222,'Dsmgr','DSMGR','','','','ANONYMOUS','VARCHAR2','','','Dsmgr','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','LAST_INVOICE_DATE',222,'Last Invoice Date','LAST_INVOICE_DATE','','','','ANONYMOUS','DATE','','','Last Invoice Date','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','LAST_UPDATE_DATE',222,'Last Update Date','LAST_UPDATE_DATE','','','','ANONYMOUS','DATE','','','Last Update Date','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','RSMGR',222,'Rsmgr','RSMGR','','','','ANONYMOUS','VARCHAR2','','','Rsmgr','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','RVP',222,'Rvp','RVP','','','','ANONYMOUS','VARCHAR2','','','Rvp','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_CUST_CONT_V','SALES_PERSON_NTID',222,'Sales Person Ntid','SALES_PERSON_NTID','','','','ANONYMOUS','VARCHAR2','','','Sales Person Ntid','','','','');
--Inserting Object Components for EIS_XXWC_AR_CUST_CONT_V
--Inserting Object Component Joins for EIS_XXWC_AR_CUST_CONT_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for Customer Account Contacts Report - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Customer Account Contacts Report - WC
xxeis.eis_rsc_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
    where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT FLV.MEANING,HRR.RESPONSIBILITY_TYPE
FROM FND_LOOKUP_VALUES FLV,
     HZ_ROLE_RESPONSIBILITY HRR
WHERE FLV.LOOKUP_CODE=HRR.RESPONSIBILITY_TYPE
AND   FLV.LOOKUP_TYPE=''SITE_USE_CODE''
UNION
SELECT ''Contact Only'',''Contact_Only'' FROM DUAL','','EIS XXWC CUSTOMER ROLE','EIS XXWC CUSTOMER ROLE','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT res.resource_name
FROM hz_cust_site_uses_all SU,
  jtf_rs_salesreps SR,
  JTF_RS_RESOURCE_EXTNS_VL RES
WHERE 1                    = 1
AND SU.PRIMARY_SALESREP_ID = SR.SALESREP_ID (+)
AND SR.RESOURCE_ID         = RES.RESOURCE_ID (+)
AND res.resource_name     IS NOT NULL','','EIS XXWC CUSTOMER SALESREPS','EIS XXWC CUSTOMER SALESREPS','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT CASE
    WHEN current_role_state =''A'' THEN
    ''ACTIVE''
    WHEN current_role_state =''I'' THEN
    ''INACTIVE''
    ELSE ''ALL''
    END STATUS
    FROM HZ_CUST_ACCOUNT_ROLES
    UNION
    SELECT ''ALL'' FROM DUAL','','XXWC CUST ACC STATUS','XXWC CUST ACC STATUS','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ''ACTIVE'' customer_status FROM dual
UNION ALL
SELECT ''INACTIVE'' customer_status FROM dual','','EIS XXWC STATUS LOV','','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for Customer Account Contacts Report - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Customer Account Contacts Report - WC
xxeis.eis_rsc_utility.delete_report_rows( 'Customer Account Contacts Report - WC' );
--Inserting Report - Customer Account Contacts Report - WC
xxeis.eis_rsc_ins.r( 222,'Customer Account Contacts Report - WC','','EiS report to display all customer contacts','','','','SA059956','EIS_XXWC_AR_CUST_CONT_V','Y','','','SA059956','','Y','White Cap Reports','PDF,','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - Customer Account Contacts Report - WC
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'ACCOUNT_NAME','Account Name','Account Name','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'ACCOUNT_NUMBER','Account Number','Account Number','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'FULL_NAME','Full Name','Full Name','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'ADDRESS','Address','Address','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'PHONE_NUMBER','Phone Number','Phone Number','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'ROLE','Role','Role','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'CONTACT_NUMBER','Contact Number','Contact Number','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'FAX','Fax','Fax','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'SALESPERSON','Salesperson','Salesperson','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'EMAIL_ADDRESS','Email Address','Email Address','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'STATUS','Role Status','Status','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'ACCOUNT_STATUS','Account Status','Account Status','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'BMGR','BMgr','Bmgr','','','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'CREDIT_MANAGER','Credit Manager','Credit Manager','','','','','21','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'DMGR','DMgr','Dmgr','','','default','','17','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'DSMGR','DSMgr','Dsmgr','','','default','','16','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'LAST_INVOICE_DATE','Account Last Invoice Date','Last Invoice Date','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'LAST_UPDATE_DATE','Last Update Date','Last Update Date','','','default','','20','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'RSMGR','RSMgr','Rsmgr','','','default','','18','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'RVP','RVP','Rvp','','','default','','19','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'Customer Account Contacts Report - WC',222,'SALES_PERSON_NTID','Sales Person NT ID','Sales Person Ntid','','','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_CUST_CONT_V','','','','US','');
--Inserting Report Parameters - Customer Account Contacts Report - WC
xxeis.eis_rsc_ins.rp( 'Customer Account Contacts Report - WC',222,'Account Name','Account Name','ACCOUNT_NAME','IN','Customer Name','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_CONT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Contacts Report - WC',222,'Account Number','Account Number','ACCOUNT_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_CONT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Contacts Report - WC',222,'Role','Role','ROLE','IN','EIS XXWC CUSTOMER ROLE','Authorized Buyer','VARCHAR2','N','Y','3','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_CONT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Contacts Report - WC',222,'Sales Person','Sales Person','SALESPERSON','IN','EIS XXWC CUSTOMER SALESREPS','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_CONT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Contacts Report - WC',222,'Role Status','Role Status','STATUS','IN','XXWC CUST ACC STATUS','ACTIVE','VARCHAR2','Y','Y','6','Y','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_CONT_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Customer Account Contacts Report - WC',222,'Account Status','Account Status','ACCOUNT_STATUS','IN','EIS XXWC STATUS LOV','ACTIVE','VARCHAR2','N','Y','5','N','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_CUST_CONT_V','','','US','');
--Inserting Dependent Parameters - Customer Account Contacts Report - WC
--Inserting Report Conditions - Customer Account Contacts Report - WC
xxeis.eis_rsc_ins.rcnh( 'Customer Account Contacts Report - WC',222,'ACCOUNT_NAME IN :Account Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT_NAME','','Account Name','','','','','EIS_XXWC_AR_CUST_CONT_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Account Contacts Report - WC','ACCOUNT_NAME IN :Account Name ');
xxeis.eis_rsc_ins.rcnh( 'Customer Account Contacts Report - WC',222,'ACCOUNT_NUMBER IN :Account Number ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT_NUMBER','','Account Number','','','','','EIS_XXWC_AR_CUST_CONT_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Account Contacts Report - WC','ACCOUNT_NUMBER IN :Account Number ');
xxeis.eis_rsc_ins.rcnh( 'Customer Account Contacts Report - WC',222,'ROLE IN :Role ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ROLE','','Role','','','','','EIS_XXWC_AR_CUST_CONT_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Account Contacts Report - WC','ROLE IN :Role ');
xxeis.eis_rsc_ins.rcnh( 'Customer Account Contacts Report - WC',222,'SALESPERSON IN :Sales Person ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SALESPERSON','','Sales Person','','','','','EIS_XXWC_AR_CUST_CONT_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Account Contacts Report - WC','SALESPERSON IN :Sales Person ');
xxeis.eis_rsc_ins.rcnh( 'Customer Account Contacts Report - WC',222,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','AND ( ''ALL'' IN (:Role Status) OR (STATUS IN (:Role Status)))','1',222,'Customer Account Contacts Report - WC','Free Text ');
xxeis.eis_rsc_ins.rcnh( 'Customer Account Contacts Report - WC',222,'EXACCV.ACCOUNT_STATUS IN Account Status','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT_STATUS','','Account Status','','','','','EIS_XXWC_AR_CUST_CONT_V','','','','','','IN','Y','Y','','','','','1',222,'Customer Account Contacts Report - WC','EXACCV.ACCOUNT_STATUS IN Account Status');
--Inserting Report Sorts - Customer Account Contacts Report - WC
--Inserting Report Triggers - Customer Account Contacts Report - WC
--inserting report templates - Customer Account Contacts Report - WC
--Inserting Report Portals - Customer Account Contacts Report - WC
--inserting report dashboards - Customer Account Contacts Report - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Customer Account Contacts Report - WC','222','EIS_XXWC_AR_CUST_CONT_V','EIS_XXWC_AR_CUST_CONT_V','N','');
--inserting report security - Customer Account Contacts Report - WC
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','222','','XXWC_CRE_ASSOC_CUST_MAINT',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','222','','XXWC_CRE_ASSOC_COLLECTIONS',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','222','','XXWC_CRE_CREDIT_COLL_MGR_NOREC',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','222','','XXWC_CRE_CREDIT_COLL_MGR',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10010432','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','KP033687','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','JA007238','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011062','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','AB054078','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','TB026983','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','CB007314','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10012084','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011446','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','SA016918','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','SS084202','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011288','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','JH041872','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10010484','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011002','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','DV053262','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10003919','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','JJ037712','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011287','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011791','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','HS022205','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011000','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','AI033683','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','SH020351','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011948','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011847','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011137','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','MV062055','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011514','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10010264','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10010772','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','JR051139','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','RN049805','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','RM053967','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','CM049574','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011946','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011809','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10010258','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','SJ042446','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011260','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011341','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10011984','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','RH010148','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','MG001485','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10010755','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','JG050887','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10012127','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10010807','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','10010905','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','KC045951','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'Customer Account Contacts Report - WC','','MD023824','',222,'SA059956','','','');
--Inserting Report Pivots - Customer Account Contacts Report - WC
--Inserting Report   Version details- Customer Account Contacts Report - WC
xxeis.eis_rsc_ins.rv( 'Customer Account Contacts Report - WC','','Customer Account Contacts Report - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
