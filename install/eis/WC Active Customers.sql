--Report Name            : WC Active Customers
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
CREATE OR REPLACE VIEW APPS.XXEIS_714_FNYPKQ_V (CUSTOMER_ID, CUSTOMER_NAME, CUSTOMER_NUMBER, PARTY_ID, PARTY_NUMBER, PARTY_TYPE, CUSTOMER_KEY, STATUS, ORIG_SYSTEM_REFERENCE, CUSTOMER_CATEGORY_CODE, CUSTOMER_CATEGORY_MEANING, CUSTOMER_CLASS_CODE, CUSTOMER_CLASS_MEANING, CUSTOMER_TYPE, CUSTOMER_TYPE_MEANING, PRIMARY_SALESREP_ID, PRIMARY_SALESREP_NAME, SIC_CODE, TAX_REFERENCE, TAX_CODE, FOB_POINT, FOB_POINT_MEANING, SHIP_VIA, GSA_INDICATOR, TAXPAYER_ID, PRICE_LIST_ID, PRICE_LIST_NAME, FREIGHT_TERM, FREIGHT_TERM_MEANING, ORDER_TYPE_ID, ORDER_TYPE_NAME, SALES_CHANNEL_CODE, SALES_CHANNEL_MEANING, WAREHOUSE_ID, WAREHOUSE_NAME, MISSION_STATEMENT, NUM_OF_EMPLOYEES, POTENTIAL_REVENUE_CURR_FY, POTENTIAL_REVENUE_NEXT_FY, FISCAL_YEAREND_MONTH, YEAR_ESTABLISHED, ANALYSIS_FY, COMPETITOR_FLAG, REFERENCE_USE_FLAG, THIRD_PARTY_FLAG, ATTRIBUTE_CATEGORY, ATTRIBUTE1, ATTRIBUTE2, ATTRIBUTE3, ATTRIBUTE4, ATTRIBUTE5, ATTRIBUTE6, ATTRIBUTE7, ATTRIBUTE8, ATTRIBUTE9, ATTRIBUTE10, ATTRIBUTE11, ATTRIBUTE12,
  ATTRIBUTE13, ATTRIBUTE14, ATTRIBUTE15, LAST_UPDATED_BY, LAST_UPDATE_DATE, OBJECT_VERSION, LAST_UPDATE_LOGIN, CREATED_BY, CREATION_DATE, CUSTOMER_PROFILE_CLASS_ID, PROFILE_CLASS_NAME, CUSTOMER_NAME_PHONETIC, TAX_HEADER_LEVEL_FLAG, TAX_ROUNDING_RULE, GLOBAL_ATTRIBUTE_CATEGORY, GLOBAL_ATTRIBUTE1, GLOBAL_ATTRIBUTE2, GLOBAL_ATTRIBUTE3, GLOBAL_ATTRIBUTE4, GLOBAL_ATTRIBUTE5, GLOBAL_ATTRIBUTE6, GLOBAL_ATTRIBUTE7, GLOBAL_ATTRIBUTE8, GLOBAL_ATTRIBUTE9, GLOBAL_ATTRIBUTE10, GLOBAL_ATTRIBUTE11, GLOBAL_ATTRIBUTE12, GLOBAL_ATTRIBUTE13, GLOBAL_ATTRIBUTE14, GLOBAL_ATTRIBUTE15, GLOBAL_ATTRIBUTE16, GLOBAL_ATTRIBUTE17, GLOBAL_ATTRIBUTE18, GLOBAL_ATTRIBUTE19, GLOBAL_ATTRIBUTE20, COTERMINATE_DAY_MONTH, PERSON_PRE_NAME_ADJUNCT, PERSON_FIRST_NAME, PERSON_MIDDLE_NAME, PERSON_LAST_NAME, PERSON_SUFFIX, PERSON_NAME_PHONETIC, PERSON_FIRST_NAME_PHONETIC, PERSON_LAST_NAME_PHONETIC, SHIP_SETS_INCLUDE_LINES_FLAG, ARRIVALSETS_INCLUDE_LINES_FLAG, SCHED_DATE_PUSH_FLAG, OVER_SHIPMENT_TOLERANCE,
  UNDER_SHIPMENT_TOLERANCE, OVER_RETURN_TOLERANCE, UNDER_RETURN_TOLERANCE, ITEM_CROSS_REF_PREF, DATE_TYPE_PREFERENCE, DATES_NEGATIVE_TOLERANCE, DATES_POSITIVE_TOLERANCE, INVOICE_QUANTITY_RULE, ATTRIBUTE16, ATTRIBUTE17, ATTRIBUTE18, ATTRIBUTE19, ATTRIBUTE20, DUNS_NUMBER, DUNS_NUMBER_C, OBJECT_VERSION_NUMBER, ORGANIZATION_PROFILE_ID, PERSON_PROFILE_ID, SIC_CODE_TYPE, PARTY_NAME, ADDRESS1, CITY, STATE, POSTAL_CODE, COUNTRY, COUNTRY_CODE, PARTY_SITE_NUMBER, ADDRESS2, ADDRESS3, ADDRESS4, COUNTY, PROVINCE, ADDRESS_KEY, CUST_ACCT_SITE_ID, ACCOUNT_NAME, IDENTIFYING_ADDRESS_FLAG, FU1_USER_ID, FU2_USER_ID, TERRITORY_CODE, LOCATION_ID, PARTY_SITE_ID, CUST_ACCOUNT_ID, CUST_ACCT_ID1, CONTACT_POINT_ID, ORGANIZATION_ID, ORG_ID, SALESREP_ID, PARTY_PARTY_ID, CP_CUST_ACCOUNT_PROFILE_ID, PL_PRICE_LIST_ID, OT_ORDER_TYPE_ID, HOU_ORGANIZATION_ID, PARTY_NAME1, OPERATING_UNIT, CONTACT_RESTRICTION_ID, PROFILE_CLASS_ID, COPYRIGHT, CUST#PARTY_TYPE, CUST_ACCT#PARTY_TYPE, CUST#LEGAL_PLACEMENT,
  CUST_ACCT#LEGAL_PLACEMENT, CUST_ACCT#AUTO_APPLY_CREDIT_, CUST#AUTO_APPLY_CREDIT_MEMO, CUST#VNDR_CODE_AND_FRULOC, CUST_ACCT#VNDR_CODE_AND_FRUL, CUST_ACCT#BRANCH_DESCRIPTION, CUST#BRANCH_DESCRIPTION, CUST_ACCT#CUSTOMER_SOURCE, CUST#CUSTOMER_SOURCE, CUST_ACCT#LEGAL_COLLECTION_I, CUST#LEGAL_COLLECTION_INDICA, CUST#PRISM_NUMBER, CUST_ACCT#PRISM_NUMBER, CUST#AUTHORIZED_BUYER_REQUIR, CUST_ACCT#AUTHORIZED_BUYER_R, CUST#AUTHORIZED_BUYER_NOTES, CUST_ACCT#AUTHORIZED_BUYER_N, CUST#PREDOMINANT_TRADE, CUST_ACCT#PREDOMINANT_TRADE, CUST#YES#NOTE_LINE_#5, CUST_ACCT#YES#NOTE_LINE_#5, CUST#YES#NOTE_LINE_#1, CUST_ACCT#YES#NOTE_LINE_#1, CUST#YES#NOTE_LINE_#2, CUST_ACCT#YES#NOTE_LINE_#2, CUST#YES#NOTE_LINE_#3, CUST_ACCT#YES#NOTE_LINE_#3, CUST_ACCT#YES#NOTE_LINE_#4, CUST#YES#NOTE_LINE_#4, HCS#PRINT_PRICES_ON_ORDER, HCS#JOINT_CHECK_AGREEMENT, HCS#OWNER_FINANCED, HCS#JOB_INFORMATION_ON_FILE, HCS#TAX_EXEMPTION_TYPE, HCS#TAX_EXEMPT1, HCS#PRISM_NUMBER, HCS#MANDATORY_PO_NUMBER, HCS#LIEN_RELEASE_DATE,
  HCS#YES#NOTICE_TO_OWNER_JOB_, HCS#YES#NOTICE_TO_OWNER_PREL, HCS#YES#NOTICE_TO_OWNER_PREL1, HL#PAY_TO_VENDOR_ID, HL#LOB, HL#PAY_TO_VENDOR_CODE, CUST_PARTY#101#PARTY_TYPE, PARTY#101#PARTY_TYPE, PARTY#101#INTERCOMPANY_RECEI, CUST_PARTY#101#INTERCOMPANY_, PARTY#101#COOP_OVERRIDE, CUST_PARTY#101#COOP_OVERRIDE, CUST_PARTY#101#PAYMENT_OVERR, PARTY#101#PAYMENT_OVERRIDE, CUST_PARTY#101#DEFAULT_PAYME, PARTY#101#DEFAULT_PAYMENT_AC, CUST_PARTY#101#DEFAULT_PAYME1, PARTY#101#DEFAULT_PAYMENT_LO, CUST_PARTY#101#COLLECTOR, PARTY#101#COLLECTOR, CUST_PARTY#101#DEFAULT_PRODU, PARTY#101#DEFAULT_PRODUCT_SE, CUST_PARTY#101#DEFAULT_LOCAT, PARTY#101#DEFAULT_LOCATION_S, PARTY#101#DEFAULT_COOP_ACCOU, CUST_PARTY#101#DEFAULT_COOP_, CUST_PARTY#101#DEFAULT_REBAT, PARTY#101#DEFAULT_REBATE_ACC, CUST_PARTY#101#DEFAULT_COST_, PARTY#101#DEFAULT_COST_CENTE, CUST_PARTY#101#INTERCOMPANY_1, PARTY#101#INTERCOMPANY__PAYA, CUST_PARTY#102#PARTY_TYPE, PARTY#102#PARTY_TYPE, CUST_PARTY#102#INTERCOMPANY_,
  PARTY#102#INTERCOMPANY_RECEI, CUST_PARTY#102#COOP_OVERRIDE, PARTY#102#COOP_OVERRIDE, PARTY#102#PAYMENT_OVERRIDE, CUST_PARTY#102#PAYMENT_OVERR, CUST_PARTY#102#DEFAULT_PAYME, PARTY#102#DEFAULT_PAYMENT_AC, CUST_PARTY#102#DEFAULT_PAYME1, PARTY#102#DEFAULT_PAYMENT_LO, CUST_PARTY#102#COLLECTOR, PARTY#102#COLLECTOR, CUST_PARTY#102#DEFAULT_PRODU, PARTY#102#DEFAULT_PRODUCT_SE, CUST_PARTY#102#DEFAULT_LOCAT, PARTY#102#DEFAULT_LOCATION_S, CUST_PARTY#102#DEFAULT_COOP_, PARTY#102#DEFAULT_COOP_ACCOU, PARTY#102#DEFAULT_REBATE_ACC, CUST_PARTY#102#DEFAULT_REBAT, PARTY#102#DEFAULT_COST_CENTE, CUST_PARTY#102#DEFAULT_COST_, CUST_PARTY#102#INTERCOMPANY_1, PARTY#102#INTERCOMPANY_PAYAB, HPS#REBT_PAY_TO_VNDR_ID, HPS#REBT_LOB, HPS#REBT_VNDR_CODE, HPS#REBT_VNDR_FLAG, HPS#REBT_VNDR_TAX_ID_NBR)
AS
  SELECT CUSTOMER_ID,
    CUSTOMER_NAME,
    CUSTOMER_NUMBER,
    PARTY_ID,
    PARTY_NUMBER,
    PARTY_TYPE,
    CUSTOMER_KEY,
    STATUS,
    ORIG_SYSTEM_REFERENCE,
    CUSTOMER_CATEGORY_CODE,
    CUSTOMER_CATEGORY_MEANING,
    CUSTOMER_CLASS_CODE,
    CUSTOMER_CLASS_MEANING,
    CUSTOMER_TYPE,
    CUSTOMER_TYPE_MEANING,
    PRIMARY_SALESREP_ID,
    PRIMARY_SALESREP_NAME,
    SIC_CODE,
    TAX_REFERENCE,
    TAX_CODE,
    FOB_POINT,
    FOB_POINT_MEANING,
    SHIP_VIA,
    GSA_INDICATOR,
    TAXPAYER_ID,
    PRICE_LIST_ID,
    PRICE_LIST_NAME,
    FREIGHT_TERM,
    FREIGHT_TERM_MEANING,
    ORDER_TYPE_ID,
    ORDER_TYPE_NAME,
    SALES_CHANNEL_CODE,
    SALES_CHANNEL_MEANING,
    WAREHOUSE_ID,
    WAREHOUSE_NAME,
    MISSION_STATEMENT,
    NUM_OF_EMPLOYEES,
    POTENTIAL_REVENUE_CURR_FY,
    POTENTIAL_REVENUE_NEXT_FY,
    FISCAL_YEAREND_MONTH,
    YEAR_ESTABLISHED,
    ANALYSIS_FY,
    COMPETITOR_FLAG,
    REFERENCE_USE_FLAG,
    THIRD_PARTY_FLAG,
    ATTRIBUTE_CATEGORY,
    ATTRIBUTE1,
    ATTRIBUTE2,
    ATTRIBUTE3,
    ATTRIBUTE4,
    ATTRIBUTE5,
    ATTRIBUTE6,
    ATTRIBUTE7,
    ATTRIBUTE8,
    ATTRIBUTE9,
    ATTRIBUTE10,
    ATTRIBUTE11,
    ATTRIBUTE12,
    ATTRIBUTE13,
    ATTRIBUTE14,
    ATTRIBUTE15,
    LAST_UPDATED_BY,
    LAST_UPDATE_DATE,
    OBJECT_VERSION,
    LAST_UPDATE_LOGIN,
    CREATED_BY,
    CREATION_DATE,
    CUSTOMER_PROFILE_CLASS_ID,
    PROFILE_CLASS_NAME,
    CUSTOMER_NAME_PHONETIC,
    TAX_HEADER_LEVEL_FLAG,
    TAX_ROUNDING_RULE,
    GLOBAL_ATTRIBUTE_CATEGORY,
    GLOBAL_ATTRIBUTE1,
    GLOBAL_ATTRIBUTE2,
    GLOBAL_ATTRIBUTE3,
    GLOBAL_ATTRIBUTE4,
    GLOBAL_ATTRIBUTE5,
    GLOBAL_ATTRIBUTE6,
    GLOBAL_ATTRIBUTE7,
    GLOBAL_ATTRIBUTE8,
    GLOBAL_ATTRIBUTE9,
    GLOBAL_ATTRIBUTE10,
    GLOBAL_ATTRIBUTE11,
    GLOBAL_ATTRIBUTE12,
    GLOBAL_ATTRIBUTE13,
    GLOBAL_ATTRIBUTE14,
    GLOBAL_ATTRIBUTE15,
    GLOBAL_ATTRIBUTE16,
    GLOBAL_ATTRIBUTE17,
    GLOBAL_ATTRIBUTE18,
    GLOBAL_ATTRIBUTE19,
    GLOBAL_ATTRIBUTE20,
    COTERMINATE_DAY_MONTH,
    PERSON_PRE_NAME_ADJUNCT,
    PERSON_FIRST_NAME,
    PERSON_MIDDLE_NAME,
    PERSON_LAST_NAME,
    PERSON_SUFFIX,
    PERSON_NAME_PHONETIC,
    PERSON_FIRST_NAME_PHONETIC,
    PERSON_LAST_NAME_PHONETIC,
    SHIP_SETS_INCLUDE_LINES_FLAG,
    ARRIVALSETS_INCLUDE_LINES_FLAG,
    SCHED_DATE_PUSH_FLAG,
    OVER_SHIPMENT_TOLERANCE,
    UNDER_SHIPMENT_TOLERANCE,
    OVER_RETURN_TOLERANCE,
    UNDER_RETURN_TOLERANCE,
    ITEM_CROSS_REF_PREF,
    DATE_TYPE_PREFERENCE,
    DATES_NEGATIVE_TOLERANCE,
    DATES_POSITIVE_TOLERANCE,
    INVOICE_QUANTITY_RULE,
    ATTRIBUTE16,
    ATTRIBUTE17,
    ATTRIBUTE18,
    ATTRIBUTE19,
    ATTRIBUTE20,
    DUNS_NUMBER,
    DUNS_NUMBER_C,
    OBJECT_VERSION_NUMBER,
    ORGANIZATION_PROFILE_ID,
    PERSON_PROFILE_ID,
    SIC_CODE_TYPE,
    PARTY_NAME,
    ADDRESS1,
    CITY,
    STATE,
    POSTAL_CODE,
    COUNTRY,
    COUNTRY_CODE,
    PARTY_SITE_NUMBER,
    ADDRESS2,
    ADDRESS3,
    ADDRESS4,
    COUNTY,
    PROVINCE,
    ADDRESS_KEY,
    CUST_ACCT_SITE_ID,
    ACCOUNT_NAME,
    IDENTIFYING_ADDRESS_FLAG,
    FU1_USER_ID,
    FU2_USER_ID,
    TERRITORY_CODE,
    LOCATION_ID,
    PARTY_SITE_ID,
    CUST_ACCOUNT_ID,
    CUST_ACCT_ID1,
    CONTACT_POINT_ID,
    ORGANIZATION_ID,
    ORG_ID,
    SALESREP_ID,
    PARTY_PARTY_ID,
    CP_CUST_ACCOUNT_PROFILE_ID,
    PL_PRICE_LIST_ID,
    OT_ORDER_TYPE_ID,
    HOU_ORGANIZATION_ID,
    PARTY_NAME1,
    OPERATING_UNIT,
    CONTACT_RESTRICTION_ID,
    PROFILE_CLASS_ID,
    COPYRIGHT,
    CUST#PARTY_TYPE,
    CUST_ACCT#PARTY_TYPE,
    CUST#LEGAL_PLACEMENT,
    CUST_ACCT#LEGAL_PLACEMENT,
    CUST_ACCT#AUTO_APPLY_CREDIT_,
    CUST#AUTO_APPLY_CREDIT_MEMO,
    CUST#VNDR_CODE_AND_FRULOC,
    CUST_ACCT#VNDR_CODE_AND_FRUL,
    CUST_ACCT#BRANCH_DESCRIPTION,
    CUST#BRANCH_DESCRIPTION,
    CUST_ACCT#CUSTOMER_SOURCE,
    CUST#CUSTOMER_SOURCE,
    CUST_ACCT#LEGAL_COLLECTION_I,
    CUST#LEGAL_COLLECTION_INDICA,
    CUST#PRISM_NUMBER,
    CUST_ACCT#PRISM_NUMBER,
    CUST#AUTHORIZED_BUYER_REQUIR,
    CUST_ACCT#AUTHORIZED_BUYER_R,
    CUST#AUTHORIZED_BUYER_NOTES,
    CUST_ACCT#AUTHORIZED_BUYER_N,
    CUST#PREDOMINANT_TRADE,
    CUST_ACCT#PREDOMINANT_TRADE,
    CUST#YES#NOTE_LINE_#5,
    CUST_ACCT#YES#NOTE_LINE_#5,
    CUST#YES#NOTE_LINE_#1,
    CUST_ACCT#YES#NOTE_LINE_#1,
    CUST#YES#NOTE_LINE_#2,
    CUST_ACCT#YES#NOTE_LINE_#2,
    CUST#YES#NOTE_LINE_#3,
    CUST_ACCT#YES#NOTE_LINE_#3,
    CUST_ACCT#YES#NOTE_LINE_#4,
    CUST#YES#NOTE_LINE_#4,
    HCS#PRINT_PRICES_ON_ORDER,
    HCS#JOINT_CHECK_AGREEMENT,
    HCS#OWNER_FINANCED,
    HCS#JOB_INFORMATION_ON_FILE,
    HCS#TAX_EXEMPTION_TYPE,
    HCS#TAX_EXEMPT HCS#TAX_EXEMPT1,
    HCS#PRISM_NUMBER,
    HCS#MANDATORY_PO_NUMBER,
    HCS#LIEN_RELEASE_DATE,
    HCS#YES#NOTICE_TO_OWNER_JOB_,
    HCS#YES#NOTICE_TO_OWNER_PREL,
    HCS#YES#NOTICE_TO_OWNER_PREL1,
    HL#PAY_TO_VENDOR_ID,
    HL#LOB,
    HL#PAY_TO_VENDOR_CODE,
    CUST_PARTY#101#PARTY_TYPE,
    PARTY#101#PARTY_TYPE,
    PARTY#101#INTERCOMPANY_RECEI,
    CUST_PARTY#101#INTERCOMPANY_,
    PARTY#101#COOP_OVERRIDE,
    CUST_PARTY#101#COOP_OVERRIDE,
    CUST_PARTY#101#PAYMENT_OVERR,
    PARTY#101#PAYMENT_OVERRIDE,
    CUST_PARTY#101#DEFAULT_PAYME,
    PARTY#101#DEFAULT_PAYMENT_AC,
    CUST_PARTY#101#DEFAULT_PAYME1,
    PARTY#101#DEFAULT_PAYMENT_LO,
    CUST_PARTY#101#COLLECTOR,
    PARTY#101#COLLECTOR,
    CUST_PARTY#101#DEFAULT_PRODU,
    PARTY#101#DEFAULT_PRODUCT_SE,
    CUST_PARTY#101#DEFAULT_LOCAT,
    PARTY#101#DEFAULT_LOCATION_S,
    PARTY#101#DEFAULT_COOP_ACCOU,
    CUST_PARTY#101#DEFAULT_COOP_,
    CUST_PARTY#101#DEFAULT_REBAT,
    PARTY#101#DEFAULT_REBATE_ACC,
    CUST_PARTY#101#DEFAULT_COST_,
    PARTY#101#DEFAULT_COST_CENTE,
    CUST_PARTY#101#INTERCOMPANY_1,
    PARTY#101#INTERCOMPANY__PAYA,
    CUST_PARTY#102#PARTY_TYPE,
    PARTY#102#PARTY_TYPE,
    CUST_PARTY#102#INTERCOMPANY_,
    PARTY#102#INTERCOMPANY_RECEI,
    CUST_PARTY#102#COOP_OVERRIDE,
    PARTY#102#COOP_OVERRIDE,
    PARTY#102#PAYMENT_OVERRIDE,
    CUST_PARTY#102#PAYMENT_OVERR,
    CUST_PARTY#102#DEFAULT_PAYME,
    PARTY#102#DEFAULT_PAYMENT_AC,
    CUST_PARTY#102#DEFAULT_PAYME1,
    PARTY#102#DEFAULT_PAYMENT_LO,
    CUST_PARTY#102#COLLECTOR,
    PARTY#102#COLLECTOR,
    CUST_PARTY#102#DEFAULT_PRODU,
    PARTY#102#DEFAULT_PRODUCT_SE,
    CUST_PARTY#102#DEFAULT_LOCAT,
    PARTY#102#DEFAULT_LOCATION_S,
    CUST_PARTY#102#DEFAULT_COOP_,
    PARTY#102#DEFAULT_COOP_ACCOU,
    PARTY#102#DEFAULT_REBATE_ACC,
    CUST_PARTY#102#DEFAULT_REBAT,
    PARTY#102#DEFAULT_COST_CENTE,
    CUST_PARTY#102#DEFAULT_COST_,
    CUST_PARTY#102#INTERCOMPANY_1,
    PARTY#102#INTERCOMPANY_PAYAB,
    HPS#REBT_PAY_TO_VNDR_ID,
    HPS#REBT_LOB,
    HPS#REBT_VNDR_CODE,
    HPS#REBT_VNDR_FLAG,
    HPS#REBT_VNDR_TAX_ID_NBR
  FROM xxeis.EIS_AR_CUSTOMER_V;
/
prompt Creating Object Data XXEIS_714_FNYPKQ_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_714_FNYPKQ_V
xxeis.eis_rsc_ins.v( 'XXEIS_714_FNYPKQ_V',222,'Paste SQL View for WC Active Customers','1.0','','','MM027735','APPS','WC Active Customers View','X7FV','','Y','VIEW','US','','','');
--Delete Object Columns for XXEIS_714_FNYPKQ_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_714_FNYPKQ_V',222,FALSE);
--Inserting Object Columns for XXEIS_714_FNYPKQ_V
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ADDRESS1',222,'','','','','','MM027735','VARCHAR2','','','Address1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CITY',222,'','','','','','MM027735','VARCHAR2','','','City','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','STATE',222,'','','','','','MM027735','VARCHAR2','','','State','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','POSTAL_CODE',222,'','','','','','MM027735','VARCHAR2','','','Postal Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','COUNTRY_CODE',222,'','','','','','MM027735','VARCHAR2','','','Country Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ADDRESS2',222,'','','','','','MM027735','VARCHAR2','','','Address2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ADDRESS3',222,'','','','','','MM027735','VARCHAR2','','','Address3','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ADDRESS4',222,'','','','','','MM027735','VARCHAR2','','','Address4','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','COUNTY',222,'','','','','','MM027735','VARCHAR2','','','County','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ACCOUNT_NAME',222,'','','','','','MM027735','VARCHAR2','','','Account Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PROFILE_CLASS_NAME',222,'','','','','','MM027735','VARCHAR2','','','Profile Class Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY_NAME',222,'','','','','','MM027735','VARCHAR2','','','Party Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_NUMBER',222,'','','','','','MM027735','VARCHAR2','','','Customer Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY_TYPE',222,'','','','','','MM027735','VARCHAR2','','','Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','STATUS',222,'','','','','','MM027735','VARCHAR2','','','Status','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ORIG_SYSTEM_REFERENCE',222,'','','','','','MM027735','VARCHAR2','','','Orig System Reference','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_CLASS_CODE',222,'','','','','','MM027735','VARCHAR2','','','Customer Class Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_TYPE',222,'','','','','','MM027735','VARCHAR2','','','Customer Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','SIC_CODE',222,'','','','','','MM027735','VARCHAR2','','','Sic Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CONTACT_RESTRICTION_ID',222,'Contact Restriction Id','','','','','MM027735','NUMBER','','','Contact Restriction Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','COPYRIGHT',222,'Copyright','','','','','MM027735','VARCHAR2','','','Copyright','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CP_CUST_ACCOUNT_PROFILE_ID',222,'Cp Cust Account Profile Id','','','','','MM027735','NUMBER','','','Cp Cust Account Profile Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#AUTHORIZED_BUYER_NOTES',222,'Cust#Authorized Buyer Notes','','','','','MM027735','VARCHAR2','','','Cust#Authorized Buyer Notes','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#AUTHORIZED_BUYER_REQUIR',222,'Cust#Authorized Buyer Requir','','','','','MM027735','VARCHAR2','','','Cust#Authorized Buyer Requir','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#AUTO_APPLY_CREDIT_MEMO',222,'Cust#Auto Apply Credit Memo','','','','','MM027735','VARCHAR2','','','Cust#Auto Apply Credit Memo','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#CUSTOMER_SOURCE',222,'Cust#Customer Source','','','','','MM027735','VARCHAR2','','','Cust#Customer Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#LEGAL_COLLECTION_INDICA',222,'Cust#Legal Collection Indica','','','','','MM027735','VARCHAR2','','','Cust#Legal Collection Indica','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#LEGAL_PLACEMENT',222,'Cust#Legal Placement','','','','','MM027735','VARCHAR2','','','Cust#Legal Placement','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#PREDOMINANT_TRADE',222,'Cust#Predominant Trade','','','','','MM027735','VARCHAR2','','','Cust#Predominant Trade','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#PRISM_NUMBER',222,'Cust#Prism Number','','','','','MM027735','VARCHAR2','','','Cust#Prism Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#YES#NOTE_LINE_#1',222,'Cust#Yes#Note Line #1','','','','','MM027735','VARCHAR2','','','Cust#Yes#Note Line #1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#YES#NOTE_LINE_#2',222,'Cust#Yes#Note Line #2','','','','','MM027735','VARCHAR2','','','Cust#Yes#Note Line #2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#YES#NOTE_LINE_#3',222,'Cust#Yes#Note Line #3','','','','','MM027735','VARCHAR2','','','Cust#Yes#Note Line #3','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#YES#NOTE_LINE_#4',222,'Cust#Yes#Note Line #4','','','','','MM027735','VARCHAR2','','','Cust#Yes#Note Line #4','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#YES#NOTE_LINE_#5',222,'Cust#Yes#Note Line #5','','','','','MM027735','VARCHAR2','','','Cust#Yes#Note Line #5','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#AUTHORIZED_BUYER_N',222,'Cust Acct#Authorized Buyer N','','','','','MM027735','VARCHAR2','','','Cust Acct#Authorized Buyer N','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#AUTHORIZED_BUYER_R',222,'Cust Acct#Authorized Buyer R','','','','','MM027735','VARCHAR2','','','Cust Acct#Authorized Buyer R','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#AUTO_APPLY_CREDIT_',222,'Cust Acct#Auto Apply Credit ','','','','','MM027735','VARCHAR2','','','Cust Acct#Auto Apply Credit ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#CUSTOMER_SOURCE',222,'Cust Acct#Customer Source','','','','','MM027735','VARCHAR2','','','Cust Acct#Customer Source','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#LEGAL_COLLECTION_I',222,'Cust Acct#Legal Collection I','','','','','MM027735','VARCHAR2','','','Cust Acct#Legal Collection I','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#LEGAL_PLACEMENT',222,'Cust Acct#Legal Placement','','','','','MM027735','VARCHAR2','','','Cust Acct#Legal Placement','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#PREDOMINANT_TRADE',222,'Cust Acct#Predominant Trade','','','','','MM027735','VARCHAR2','','','Cust Acct#Predominant Trade','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#PRISM_NUMBER',222,'Cust Acct#Prism Number','','','','','MM027735','VARCHAR2','','','Cust Acct#Prism Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#YES#NOTE_LINE_#1',222,'Cust Acct#Yes#Note Line #1','','','','','MM027735','VARCHAR2','','','Cust Acct#Yes#Note Line #1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#YES#NOTE_LINE_#2',222,'Cust Acct#Yes#Note Line #2','','','','','MM027735','VARCHAR2','','','Cust Acct#Yes#Note Line #2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#YES#NOTE_LINE_#3',222,'Cust Acct#Yes#Note Line #3','','','','','MM027735','VARCHAR2','','','Cust Acct#Yes#Note Line #3','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#YES#NOTE_LINE_#4',222,'Cust Acct#Yes#Note Line #4','','','','','MM027735','VARCHAR2','','','Cust Acct#Yes#Note Line #4','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#YES#NOTE_LINE_#5',222,'Cust Acct#Yes#Note Line #5','','','','','MM027735','VARCHAR2','','','Cust Acct#Yes#Note Line #5','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#COLLECTOR',222,'Cust Party#101#Collector','','','','','MM027735','VARCHAR2','','','Cust Party#101#Collector','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#COOP_OVERRIDE',222,'Cust Party#101#Coop Override','','','','','MM027735','VARCHAR2','','','Cust Party#101#Coop Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#DEFAULT_COOP_',222,'Cust Party#101#Default Coop ','','','','','MM027735','VARCHAR2','','','Cust Party#101#Default Coop ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#DEFAULT_COST_',222,'Cust Party#101#Default Cost ','','','','','MM027735','VARCHAR2','','','Cust Party#101#Default Cost ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#DEFAULT_LOCAT',222,'Cust Party#101#Default Locat','','','','','MM027735','VARCHAR2','','','Cust Party#101#Default Locat','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#DEFAULT_PAYME',222,'Cust Party#101#Default Payme','','','','','MM027735','VARCHAR2','','','Cust Party#101#Default Payme','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#DEFAULT_PAYME1',222,'Cust Party#101#Default Payme1','','','','','MM027735','VARCHAR2','','','Cust Party#101#Default Payme1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#DEFAULT_PRODU',222,'Cust Party#101#Default Produ','','','','','MM027735','VARCHAR2','','','Cust Party#101#Default Produ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#DEFAULT_REBAT',222,'Cust Party#101#Default Rebat','','','','','MM027735','VARCHAR2','','','Cust Party#101#Default Rebat','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#INTERCOMPANY_',222,'Cust Party#101#Intercompany ','','','','','MM027735','VARCHAR2','','','Cust Party#101#Intercompany ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#INTERCOMPANY_1',222,'Cust Party#101#Intercompany 1','','','','','MM027735','VARCHAR2','','','Cust Party#101#Intercompany 1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#PARTY_TYPE',222,'Cust Party#101#Party Type','','','','','MM027735','VARCHAR2','','','Cust Party#101#Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#101#PAYMENT_OVERR',222,'Cust Party#101#Payment Overr','','','','','MM027735','VARCHAR2','','','Cust Party#101#Payment Overr','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#COLLECTOR',222,'Cust Party#102#Collector','','','','','MM027735','VARCHAR2','','','Cust Party#102#Collector','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#COOP_OVERRIDE',222,'Cust Party#102#Coop Override','','','','','MM027735','VARCHAR2','','','Cust Party#102#Coop Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#DEFAULT_COOP_',222,'Cust Party#102#Default Coop ','','','','','MM027735','VARCHAR2','','','Cust Party#102#Default Coop ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#DEFAULT_COST_',222,'Cust Party#102#Default Cost ','','','','','MM027735','VARCHAR2','','','Cust Party#102#Default Cost ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#DEFAULT_LOCAT',222,'Cust Party#102#Default Locat','','','','','MM027735','VARCHAR2','','','Cust Party#102#Default Locat','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#DEFAULT_PAYME',222,'Cust Party#102#Default Payme','','','','','MM027735','VARCHAR2','','','Cust Party#102#Default Payme','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#DEFAULT_PAYME1',222,'Cust Party#102#Default Payme1','','','','','MM027735','VARCHAR2','','','Cust Party#102#Default Payme1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#DEFAULT_PRODU',222,'Cust Party#102#Default Produ','','','','','MM027735','VARCHAR2','','','Cust Party#102#Default Produ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#DEFAULT_REBAT',222,'Cust Party#102#Default Rebat','','','','','MM027735','VARCHAR2','','','Cust Party#102#Default Rebat','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#INTERCOMPANY_',222,'Cust Party#102#Intercompany ','','','','','MM027735','VARCHAR2','','','Cust Party#102#Intercompany ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#INTERCOMPANY_1',222,'Cust Party#102#Intercompany 1','','','','','MM027735','VARCHAR2','','','Cust Party#102#Intercompany 1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#PARTY_TYPE',222,'Cust Party#102#Party Type','','','','','MM027735','VARCHAR2','','','Cust Party#102#Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_PARTY#102#PAYMENT_OVERR',222,'Cust Party#102#Payment Overr','','','','','MM027735','VARCHAR2','','','Cust Party#102#Payment Overr','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#JOB_INFORMATION_ON_FILE',222,'Hcs#Job Information On File','','','','','MM027735','VARCHAR2','','','Hcs#Job Information On File','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#JOINT_CHECK_AGREEMENT',222,'Hcs#Joint Check Agreement','','','','','MM027735','VARCHAR2','','','Hcs#Joint Check Agreement','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#LIEN_RELEASE_DATE',222,'Hcs#Lien Release Date','','','','','MM027735','VARCHAR2','','','Hcs#Lien Release Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#MANDATORY_PO_NUMBER',222,'Hcs#Mandatory Po Number','','','','','MM027735','VARCHAR2','','','Hcs#Mandatory Po Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#OWNER_FINANCED',222,'Hcs#Owner Financed','','','','','MM027735','VARCHAR2','','','Hcs#Owner Financed','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#PRINT_PRICES_ON_ORDER',222,'Hcs#Print Prices On Order','','','','','MM027735','VARCHAR2','','','Hcs#Print Prices On Order','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#PRISM_NUMBER',222,'Hcs#Prism Number','','','','','MM027735','VARCHAR2','','','Hcs#Prism Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#TAX_EXEMPT1',222,'Hcs#Tax Exempt1','','','','','MM027735','VARCHAR2','','','Hcs#Tax Exempt1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#TAX_EXEMPTION_TYPE',222,'Hcs#Tax Exemption Type','','','','','MM027735','VARCHAR2','','','Hcs#Tax Exemption Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#YES#NOTICE_TO_OWNER_JOB_',222,'Hcs#Yes#Notice To Owner Job ','','','','','MM027735','VARCHAR2','','','Hcs#Yes#Notice To Owner Job ','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#YES#NOTICE_TO_OWNER_PREL',222,'Hcs#Yes#Notice To Owner Prel','','','','','MM027735','VARCHAR2','','','Hcs#Yes#Notice To Owner Prel','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HCS#YES#NOTICE_TO_OWNER_PREL1',222,'Hcs#Yes#Notice To Owner Prel1','','','','','MM027735','VARCHAR2','','','Hcs#Yes#Notice To Owner Prel1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HOU_ORGANIZATION_ID',222,'Hou Organization Id','','','','','MM027735','NUMBER','','','Hou Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','OPERATING_UNIT',222,'Operating Unit','','','','','MM027735','VARCHAR2','','','Operating Unit','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ORG_ID',222,'Org Id','','','','','MM027735','NUMBER','','','Org Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','OT_ORDER_TYPE_ID',222,'Ot Order Type Id','','','','','MM027735','NUMBER','','','Ot Order Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#COLLECTOR',222,'Party#101#Collector','','','','','MM027735','VARCHAR2','','','Party#101#Collector','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#COOP_OVERRIDE',222,'Party#101#Coop Override','','','','','MM027735','VARCHAR2','','','Party#101#Coop Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#DEFAULT_COOP_ACCOU',222,'Party#101#Default Coop Accou','','','','','MM027735','VARCHAR2','','','Party#101#Default Coop Accou','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#DEFAULT_COST_CENTE',222,'Party#101#Default Cost Cente','','','','','MM027735','VARCHAR2','','','Party#101#Default Cost Cente','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#DEFAULT_LOCATION_S',222,'Party#101#Default Location S','','','','','MM027735','VARCHAR2','','','Party#101#Default Location S','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#DEFAULT_PAYMENT_AC',222,'Party#101#Default Payment Ac','','','','','MM027735','VARCHAR2','','','Party#101#Default Payment Ac','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#DEFAULT_PAYMENT_LO',222,'Party#101#Default Payment Lo','','','','','MM027735','VARCHAR2','','','Party#101#Default Payment Lo','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#DEFAULT_PRODUCT_SE',222,'Party#101#Default Product Se','','','','','MM027735','VARCHAR2','','','Party#101#Default Product Se','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#DEFAULT_REBATE_ACC',222,'Party#101#Default Rebate Acc','','','','','MM027735','VARCHAR2','','','Party#101#Default Rebate Acc','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#INTERCOMPANY_RECEI',222,'Party#101#Intercompany Recei','','','','','MM027735','VARCHAR2','','','Party#101#Intercompany Recei','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#INTERCOMPANY__PAYA',222,'Party#101#Intercompany  Paya','','','','','MM027735','VARCHAR2','','','Party#101#Intercompany  Paya','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#PARTY_TYPE',222,'Party#101#Party Type','','','','','MM027735','VARCHAR2','','','Party#101#Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#101#PAYMENT_OVERRIDE',222,'Party#101#Payment Override','','','','','MM027735','VARCHAR2','','','Party#101#Payment Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#COLLECTOR',222,'Party#102#Collector','','','','','MM027735','VARCHAR2','','','Party#102#Collector','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#COOP_OVERRIDE',222,'Party#102#Coop Override','','','','','MM027735','VARCHAR2','','','Party#102#Coop Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#DEFAULT_COOP_ACCOU',222,'Party#102#Default Coop Accou','','','','','MM027735','VARCHAR2','','','Party#102#Default Coop Accou','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#DEFAULT_COST_CENTE',222,'Party#102#Default Cost Cente','','','','','MM027735','VARCHAR2','','','Party#102#Default Cost Cente','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#DEFAULT_LOCATION_S',222,'Party#102#Default Location S','','','','','MM027735','VARCHAR2','','','Party#102#Default Location S','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#DEFAULT_PAYMENT_AC',222,'Party#102#Default Payment Ac','','','','','MM027735','VARCHAR2','','','Party#102#Default Payment Ac','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#DEFAULT_PAYMENT_LO',222,'Party#102#Default Payment Lo','','','','','MM027735','VARCHAR2','','','Party#102#Default Payment Lo','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#DEFAULT_PRODUCT_SE',222,'Party#102#Default Product Se','','','','','MM027735','VARCHAR2','','','Party#102#Default Product Se','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#DEFAULT_REBATE_ACC',222,'Party#102#Default Rebate Acc','','','','','MM027735','VARCHAR2','','','Party#102#Default Rebate Acc','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#INTERCOMPANY_PAYAB',222,'Party#102#Intercompany Payab','','','','','MM027735','VARCHAR2','','','Party#102#Intercompany Payab','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#INTERCOMPANY_RECEI',222,'Party#102#Intercompany Recei','','','','','MM027735','VARCHAR2','','','Party#102#Intercompany Recei','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#PARTY_TYPE',222,'Party#102#Party Type','','','','','MM027735','VARCHAR2','','','Party#102#Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY#102#PAYMENT_OVERRIDE',222,'Party#102#Payment Override','','','','','MM027735','VARCHAR2','','','Party#102#Payment Override','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY_NAME1',222,'Party Name1','','','','','MM027735','VARCHAR2','','','Party Name1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY_PARTY_ID',222,'Party Party Id','','','','','MM027735','NUMBER','','','Party Party Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PL_PRICE_LIST_ID',222,'Pl Price List Id','','','~T~D~2','','MM027735','NUMBER','','','Pl Price List Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PROFILE_CLASS_ID',222,'Profile Class Id','','','','','MM027735','NUMBER','','','Profile Class Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','SALESREP_ID',222,'Salesrep Id','','','','','MM027735','NUMBER','','','Salesrep Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','COUNTRY',222,'','','','','','MM027735','VARCHAR2','','','Country','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY_SITE_NUMBER',222,'','','','','','MM027735','VARCHAR2','','','Party Site Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PROVINCE',222,'','','','','','MM027735','VARCHAR2','','','Province','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ADDRESS_KEY',222,'','','','','','MM027735','VARCHAR2','','','Address Key','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT_SITE_ID',222,'','','','','','MM027735','NUMBER','','','Cust Acct Site Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','IDENTIFYING_ADDRESS_FLAG',222,'','','','','','MM027735','VARCHAR2','','','Identifying Address Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','FU1_USER_ID',222,'','','','','','MM027735','NUMBER','','','Fu1 User Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','FU2_USER_ID',222,'','','','','','MM027735','NUMBER','','','Fu2 User Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','TERRITORY_CODE',222,'','','','','','MM027735','VARCHAR2','','','Territory Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','LOCATION_ID',222,'','','','','','MM027735','NUMBER','','','Location Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY_SITE_ID',222,'','','','','','MM027735','NUMBER','','','Party Site Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCOUNT_ID',222,'','','','','','MM027735','NUMBER','','','Cust Account Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT_ID1',222,'','','','','','MM027735','NUMBER','','','Cust Acct Id1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CONTACT_POINT_ID',222,'','','','','','MM027735','NUMBER','','','Contact Point Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ORGANIZATION_ID',222,'','','','','','MM027735','NUMBER','','','Organization Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#PARTY_TYPE',222,'','','','','','MM027735','VARCHAR2','','','Cust#Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#PARTY_TYPE',222,'','','','','','MM027735','VARCHAR2','','','Cust Acct#Party Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#VNDR_CODE_AND_FRULOC',222,'','','','','','MM027735','VARCHAR2','','','Cust#Vndr Code And Fruloc','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#VNDR_CODE_AND_FRUL',222,'','','','','','MM027735','VARCHAR2','','','Cust Acct#Vndr Code And Frul','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST#BRANCH_DESCRIPTION',222,'','','','','','MM027735','VARCHAR2','','','Cust#Branch Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUST_ACCT#BRANCH_DESCRIPTION',222,'','','','','','MM027735','VARCHAR2','','','Cust Acct#Branch Description','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HL#PAY_TO_VENDOR_ID',222,'','','','','','MM027735','VARCHAR2','','','Hl#Pay To Vendor Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HL#LOB',222,'','','','','','MM027735','VARCHAR2','','','Hl#Lob','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HL#PAY_TO_VENDOR_CODE',222,'','','','','','MM027735','VARCHAR2','','','Hl#Pay To Vendor Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HPS#REBT_PAY_TO_VNDR_ID',222,'','','','','','MM027735','VARCHAR2','','','Hps#Rebt Pay To Vndr Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HPS#REBT_LOB',222,'','','','','','MM027735','VARCHAR2','','','Hps#Rebt Lob','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HPS#REBT_VNDR_CODE',222,'','','','','','MM027735','VARCHAR2','','','Hps#Rebt Vndr Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HPS#REBT_VNDR_FLAG',222,'','','','','','MM027735','VARCHAR2','','','Hps#Rebt Vndr Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','HPS#REBT_VNDR_TAX_ID_NBR',222,'','','','','','MM027735','VARCHAR2','','','Hps#Rebt Vndr Tax Id Nbr','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','TAX_REFERENCE',222,'','','','','','MM027735','VARCHAR2','','','Tax Reference','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','TAX_CODE',222,'','','','','','MM027735','VARCHAR2','','','Tax Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','FOB_POINT',222,'','','','','','MM027735','VARCHAR2','','','Fob Point','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','FOB_POINT_MEANING',222,'','','','','','MM027735','VARCHAR2','','','Fob Point Meaning','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','SHIP_VIA',222,'','','','','','MM027735','VARCHAR2','','','Ship Via','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GSA_INDICATOR',222,'','','','','','MM027735','VARCHAR2','','','Gsa Indicator','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','TAXPAYER_ID',222,'','','','','','MM027735','VARCHAR2','','','Taxpayer Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PRICE_LIST_ID',222,'','','','~T~D~2','','MM027735','NUMBER','','','Price List Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PRICE_LIST_NAME',222,'','','','','','MM027735','VARCHAR2','','','Price List Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','FREIGHT_TERM',222,'','','','','','MM027735','VARCHAR2','','','Freight Term','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','FREIGHT_TERM_MEANING',222,'','','','','','MM027735','VARCHAR2','','','Freight Term Meaning','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ORDER_TYPE_ID',222,'','','','','','MM027735','NUMBER','','','Order Type Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ORDER_TYPE_NAME',222,'','','','','','MM027735','VARCHAR2','','','Order Type Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','SALES_CHANNEL_CODE',222,'','','','','','MM027735','VARCHAR2','','','Sales Channel Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','SALES_CHANNEL_MEANING',222,'','','','','','MM027735','VARCHAR2','','','Sales Channel Meaning','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','WAREHOUSE_ID',222,'','','','','','MM027735','NUMBER','','','Warehouse Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','WAREHOUSE_NAME',222,'','','','','','MM027735','VARCHAR2','','','Warehouse Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','MISSION_STATEMENT',222,'','','','','','MM027735','VARCHAR2','','','Mission Statement','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','NUM_OF_EMPLOYEES',222,'','','','','','MM027735','NUMBER','','','Num Of Employees','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','POTENTIAL_REVENUE_CURR_FY',222,'','','','','','MM027735','NUMBER','','','Potential Revenue Curr Fy','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','POTENTIAL_REVENUE_NEXT_FY',222,'','','','','','MM027735','NUMBER','','','Potential Revenue Next Fy','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','FISCAL_YEAREND_MONTH',222,'','','','','','MM027735','VARCHAR2','','','Fiscal Yearend Month','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','YEAR_ESTABLISHED',222,'','','','','','MM027735','NUMBER','','','Year Established','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ANALYSIS_FY',222,'','','','','','MM027735','VARCHAR2','','','Analysis Fy','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','COMPETITOR_FLAG',222,'','','','','','MM027735','VARCHAR2','','','Competitor Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','REFERENCE_USE_FLAG',222,'','','','','','MM027735','VARCHAR2','','','Reference Use Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','THIRD_PARTY_FLAG',222,'','','','','','MM027735','VARCHAR2','','','Third Party Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE_CATEGORY',222,'','','','','','MM027735','VARCHAR2','','','Attribute Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE1',222,'','','','','','MM027735','VARCHAR2','','','Attribute1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE2',222,'','','','','','MM027735','VARCHAR2','','','Attribute2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE3',222,'','','','','','MM027735','VARCHAR2','','','Attribute3','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE4',222,'','','','','','MM027735','VARCHAR2','','','Attribute4','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE5',222,'','','','','','MM027735','VARCHAR2','','','Attribute5','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE6',222,'','','','','','MM027735','VARCHAR2','','','Attribute6','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE7',222,'','','','','','MM027735','VARCHAR2','','','Attribute7','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE8',222,'','','','','','MM027735','VARCHAR2','','','Attribute8','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE9',222,'','','','','','MM027735','VARCHAR2','','','Attribute9','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE10',222,'','','','','','MM027735','VARCHAR2','','','Attribute10','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE11',222,'','','','','','MM027735','VARCHAR2','','','Attribute11','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE12',222,'','','','','','MM027735','VARCHAR2','','','Attribute12','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE13',222,'','','','','','MM027735','VARCHAR2','','','Attribute13','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE14',222,'','','','','','MM027735','VARCHAR2','','','Attribute14','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE15',222,'','','','','','MM027735','VARCHAR2','','','Attribute15','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','LAST_UPDATED_BY',222,'','','','','','MM027735','VARCHAR2','','','Last Updated By','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','LAST_UPDATE_DATE',222,'','','','','','MM027735','DATE','','','Last Update Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','OBJECT_VERSION',222,'','','','','','MM027735','NUMBER','','','Object Version','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','LAST_UPDATE_LOGIN',222,'','','','','','MM027735','NUMBER','','','Last Update Login','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CREATED_BY',222,'','','','','','MM027735','VARCHAR2','','','Created By','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CREATION_DATE',222,'','','','','','MM027735','DATE','','','Creation Date','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_PROFILE_CLASS_ID',222,'','','','','','MM027735','NUMBER','','','Customer Profile Class Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_NAME_PHONETIC',222,'','','','','','MM027735','VARCHAR2','','','Customer Name Phonetic','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','TAX_HEADER_LEVEL_FLAG',222,'','','','','','MM027735','VARCHAR2','','','Tax Header Level Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','TAX_ROUNDING_RULE',222,'','','','','','MM027735','VARCHAR2','','','Tax Rounding Rule','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE_CATEGORY',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute Category','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE1',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute1','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE2',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute2','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE3',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute3','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE4',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute4','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE5',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute5','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE6',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute6','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE7',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute7','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE8',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute8','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE9',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute9','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE10',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute10','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE11',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute11','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE12',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute12','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE13',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute13','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE14',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute14','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE15',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute15','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE16',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute16','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE17',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute17','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE18',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute18','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE19',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute19','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','GLOBAL_ATTRIBUTE20',222,'','','','','','MM027735','VARCHAR2','','','Global Attribute20','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','COTERMINATE_DAY_MONTH',222,'','','','','','MM027735','VARCHAR2','','','Coterminate Day Month','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PERSON_PRE_NAME_ADJUNCT',222,'','','','','','MM027735','VARCHAR2','','','Person Pre Name Adjunct','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PERSON_FIRST_NAME',222,'','','','','','MM027735','VARCHAR2','','','Person First Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PERSON_MIDDLE_NAME',222,'','','','','','MM027735','VARCHAR2','','','Person Middle Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PERSON_LAST_NAME',222,'','','','','','MM027735','VARCHAR2','','','Person Last Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PERSON_SUFFIX',222,'','','','','','MM027735','VARCHAR2','','','Person Suffix','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PERSON_NAME_PHONETIC',222,'','','','','','MM027735','VARCHAR2','','','Person Name Phonetic','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PERSON_FIRST_NAME_PHONETIC',222,'','','','','','MM027735','VARCHAR2','','','Person First Name Phonetic','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PERSON_LAST_NAME_PHONETIC',222,'','','','','','MM027735','VARCHAR2','','','Person Last Name Phonetic','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','SHIP_SETS_INCLUDE_LINES_FLAG',222,'','','','','','MM027735','VARCHAR2','','','Ship Sets Include Lines Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ARRIVALSETS_INCLUDE_LINES_FLAG',222,'','','','','','MM027735','VARCHAR2','','','Arrivalsets Include Lines Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','SCHED_DATE_PUSH_FLAG',222,'','','','','','MM027735','VARCHAR2','','','Sched Date Push Flag','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','OVER_SHIPMENT_TOLERANCE',222,'','','','','','MM027735','NUMBER','','','Over Shipment Tolerance','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','UNDER_SHIPMENT_TOLERANCE',222,'','','','','','MM027735','NUMBER','','','Under Shipment Tolerance','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','OVER_RETURN_TOLERANCE',222,'','','','','','MM027735','NUMBER','','','Over Return Tolerance','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','UNDER_RETURN_TOLERANCE',222,'','','','','','MM027735','NUMBER','','','Under Return Tolerance','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ITEM_CROSS_REF_PREF',222,'','','','','','MM027735','VARCHAR2','','','Item Cross Ref Pref','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','DATE_TYPE_PREFERENCE',222,'','','','','','MM027735','VARCHAR2','','','Date Type Preference','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','DATES_NEGATIVE_TOLERANCE',222,'','','','','','MM027735','NUMBER','','','Dates Negative Tolerance','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','DATES_POSITIVE_TOLERANCE',222,'','','','','','MM027735','NUMBER','','','Dates Positive Tolerance','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','INVOICE_QUANTITY_RULE',222,'','','','','','MM027735','VARCHAR2','','','Invoice Quantity Rule','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE16',222,'','','','','','MM027735','VARCHAR2','','','Attribute16','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE17',222,'','','','','','MM027735','VARCHAR2','','','Attribute17','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE18',222,'','','','','','MM027735','VARCHAR2','','','Attribute18','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE19',222,'','','','','','MM027735','VARCHAR2','','','Attribute19','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ATTRIBUTE20',222,'','','','','','MM027735','VARCHAR2','','','Attribute20','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','DUNS_NUMBER',222,'','','','','','MM027735','NUMBER','','','Duns Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','DUNS_NUMBER_C',222,'','','','','','MM027735','VARCHAR2','','','Duns Number C','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','OBJECT_VERSION_NUMBER',222,'','','','','','MM027735','NUMBER','','','Object Version Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','ORGANIZATION_PROFILE_ID',222,'','','','','','MM027735','NUMBER','','','Organization Profile Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PERSON_PROFILE_ID',222,'','','','','','MM027735','NUMBER','','','Person Profile Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','SIC_CODE_TYPE',222,'','','','','','MM027735','VARCHAR2','','','Sic Code Type','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_ID',222,'','','','','','MM027735','NUMBER','','','Customer Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_NAME',222,'','','','','','MM027735','VARCHAR2','','','Customer Name','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY_ID',222,'','','','','','MM027735','NUMBER','','','Party Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PARTY_NUMBER',222,'','','','','','MM027735','VARCHAR2','','','Party Number','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_KEY',222,'','','','','','MM027735','VARCHAR2','','','Customer Key','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_CATEGORY_CODE',222,'','','','','','MM027735','VARCHAR2','','','Customer Category Code','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_CATEGORY_MEANING',222,'','','','','','MM027735','VARCHAR2','','','Customer Category Meaning','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_CLASS_MEANING',222,'','','','','','MM027735','VARCHAR2','','','Customer Class Meaning','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','CUSTOMER_TYPE_MEANING',222,'','','','','','MM027735','VARCHAR2','','','Customer Type Meaning','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PRIMARY_SALESREP_ID',222,'','','','','','MM027735','NUMBER','','','Primary Salesrep Id','','','','US','');
xxeis.eis_rsc_ins.vc( 'XXEIS_714_FNYPKQ_V','PRIMARY_SALESREP_NAME',222,'','','','','','MM027735','VARCHAR2','','','Primary Salesrep Name','','','','US','');
--Inserting Object Components for XXEIS_714_FNYPKQ_V
--Inserting Object Component Joins for XXEIS_714_FNYPKQ_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for WC Active Customers
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - WC Active Customers
xxeis.eis_rsc_ins.lov( 222,'select distinct party_name from hz_parties p, hz_cust_accounts c
    where p.party_id = c.party_id','null','Customer Name','Displays List of Values for Customer Name ','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select account_number from hz_cust_accounts','null','Customer Number','Displays List of Values for Customer Number','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct name from hz_cust_profile_classes','null','PROFILE CLASS','This LOV lists all the profile classes of the customers','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for WC Active Customers
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - WC Active Customers
xxeis.eis_rsc_utility.delete_report_rows( 'WC Active Customers',222 );
--Inserting Report - WC Active Customers
xxeis.eis_rsc_ins.r( 222,'WC Active Customers','','WC Active Customers','','','','XXEIS_RS_ADMIN','XXEIS_714_FNYPKQ_V','Y','','select * from xxeis.EIS_AR_CUSTOMER_V
','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,EXCEL,Pivot Excel,','N','','','','','','N','APPS','US','','','','','','','','','','','','','','','','');
--Inserting Report Columns - WC Active Customers
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'ADDRESS1','Address1','','','','default','','5','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'CITY','City','','','','default','','7','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'STATE','State','','','','default','','8','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'POSTAL_CODE','Postal Code','','','','default','','10','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'COUNTRY_CODE','Country Code','','','','default','','11','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'ADDRESS2','Address2','','','','default','','6','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'ADDRESS3','Address3','','','','default','','12','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'ADDRESS4','Address4','','','','default','','13','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'COUNTY','County','','','','default','','9','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'ACCOUNT_NAME','Account Name','','','','default','','3','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'PROFILE_CLASS_NAME','Profile Class Name','','','','default','','14','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'PARTY_NAME','Party Name','','','','default','','15','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'PARTY_TYPE','Party Type','','','','default','','16','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'ORIG_SYSTEM_REFERENCE','Orig System Reference','','','','default','','2','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'CUSTOMER_CLASS_CODE','Customer Class Code','','','','default','','17','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'CUSTOMER_TYPE','Customer Type','','','','default','','18','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'SIC_CODE','Sic Code','','','','default','','4','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'STATUS','Status','','','','default','','19','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
xxeis.eis_rsc_ins.rc( 'WC Active Customers',222,'CUSTOMER_NUMBER','Customer Number','','','','default','','1','N','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','XXEIS_714_FNYPKQ_V','','','','US','','');
--Inserting Report Parameters - WC Active Customers
xxeis.eis_rsc_ins.rp( 'WC Active Customers',222,'Account Name','','ACCOUNT_NAME','IN','Customer Name','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_714_FNYPKQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Active Customers',222,'Party Name','','PARTY_NAME','IN','Customer Name','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_714_FNYPKQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Active Customers',222,'Profile Class Name','','PROFILE_CLASS_NAME','IN','PROFILE CLASS','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_714_FNYPKQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Active Customers',222,'Customer Number','','CUSTOMER_NUMBER','IN','Customer Number','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','','XXEIS_714_FNYPKQ_V','','','US','');
xxeis.eis_rsc_ins.rp( 'WC Active Customers',222,'Status','','STATUS','IN','','A','VARCHAR2','N','Y','5','Y','Y','CONSTANT','XXEIS_RS_ADMIN','N','N','','','','XXEIS_714_FNYPKQ_V','','','US','');
--Inserting Dependent Parameters - WC Active Customers
--Inserting Report Conditions - WC Active Customers
xxeis.eis_rsc_ins.rcnh( 'WC Active Customers',222,'ACCOUNT_NAME IN :Account Name ','SIMPLE','','','Y','','1');
xxeis.eis_rsc_ins.rcnd( '','','ACCOUNT_NAME','','Account Name','','','','','XXEIS_714_FNYPKQ_V','','','','','','IN','Y','Y','','','','','1',222,'WC Active Customers','ACCOUNT_NAME IN :Account Name ');
xxeis.eis_rsc_ins.rcnh( 'WC Active Customers',222,'CUSTOMER_NUMBER IN :Customer Number ','SIMPLE','','','Y','','2');
xxeis.eis_rsc_ins.rcnd( '','','CUSTOMER_NUMBER','','Customer Number','','','','','XXEIS_714_FNYPKQ_V','','','','','','IN','Y','Y','','','','','1',222,'WC Active Customers','CUSTOMER_NUMBER IN :Customer Number ');
xxeis.eis_rsc_ins.rcnh( 'WC Active Customers',222,'PARTY_NAME IN :Party Name ','SIMPLE','','','Y','','3');
xxeis.eis_rsc_ins.rcnd( '','','PARTY_NAME','','Party Name','','','','','XXEIS_714_FNYPKQ_V','','','','','','IN','Y','Y','','','','','1',222,'WC Active Customers','PARTY_NAME IN :Party Name ');
xxeis.eis_rsc_ins.rcnh( 'WC Active Customers',222,'PROFILE_CLASS_NAME IN :Profile Class Name ','SIMPLE','','','Y','','4');
xxeis.eis_rsc_ins.rcnd( '','','PROFILE_CLASS_NAME','','Profile Class Name','','','','','XXEIS_714_FNYPKQ_V','','','','','','IN','Y','Y','','','','','1',222,'WC Active Customers','PROFILE_CLASS_NAME IN :Profile Class Name ');
xxeis.eis_rsc_ins.rcnh( 'WC Active Customers',222,'STATUS IN :Status ','SIMPLE','','','Y','','5');
xxeis.eis_rsc_ins.rcnd( '','','STATUS','','Status','','','','','XXEIS_714_FNYPKQ_V','','','','','','IN','Y','Y','','','','','1',222,'WC Active Customers','STATUS IN :Status ');
--Inserting Report Sorts - WC Active Customers
xxeis.eis_rsc_ins.rs( 'WC Active Customers',222,'ACCOUNT_NAME','ASC','XXEIS_RS_ADMIN','1','');
--Inserting Report Triggers - WC Active Customers
--inserting report templates - WC Active Customers
--Inserting Report Portals - WC Active Customers
--inserting report dashboards - WC Active Customers
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'WC Active Customers','222','XXEIS_714_FNYPKQ_V','XXEIS_714_FNYPKQ_V','N','');
--inserting report security - WC Active Customers
xxeis.eis_rsc_ins.rsec( 'WC Active Customers','222','','RECEIVABLES_MANAGER',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'WC Active Customers','20005','','XXWC_VIEW_ALL_EIS_REPORTS',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'WC Active Customers','222','','XXWC_RECEIVABLES_INQUIRY_WC',222,'XXEIS_RS_ADMIN','','','');
xxeis.eis_rsc_ins.rsec( 'WC Active Customers','222','','XXWC_AR_IRECEIVABLES',222,'XXEIS_RS_ADMIN','','','');
--Inserting Report Pivots - WC Active Customers
--Inserting Report Distribution Details 
--Inserting Report Distribution Details 
--Inserting Report SubTotals Details 
--Inserting Report aggregations 
--Inserting Report   Version details- WC Active Customers
xxeis.eis_rsc_ins.rv( 'WC Active Customers','','WC Active Customers','SA059956','29-JUN-2017');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
