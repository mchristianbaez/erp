--Report Name            : CSP Agreement Status - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_WC_OM_CSP_AGRMNT_STATUS_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_WC_OM_CSP_AGRMNT_STATUS_V
xxeis.eis_rsc_ins.v( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V',660,'','','','','SA059956','XXEIS','Eis Wc Om Csp Agrmnt Status V','EWOCASV','','','VIEW','US','','');
--Delete Object Columns for EIS_WC_OM_CSP_AGRMNT_STATUS_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_WC_OM_CSP_AGRMNT_STATUS_V',660,FALSE);
--Inserting Object Columns for EIS_WC_OM_CSP_AGRMNT_STATUS_V
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','ORGANIZATION_ID',660,'Organization Id','ORGANIZATION_ID','','','','SA059956','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','REGION',660,'Region','REGION','','','','SA059956','VARCHAR2','','','Region','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','BRANCH',660,'Branch','BRANCH','','','','SA059956','VARCHAR2','','','Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','SA059956','VARCHAR2','','','Vendor Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','SA059956','VARCHAR2','','','Vendor Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','VQN_NUMBER',660,'Vqn Number','VQN_NUMBER','','','','SA059956','VARCHAR2','','','Vqn Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','SPECIAL_COST',660,'Special Cost','SPECIAL_COST','','','','SA059956','NUMBER','','','Special Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','SKU',660,'Sku','SKU','','','','SA059956','VARCHAR2','','','Sku','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','START_DATE',660,'Start Date','START_DATE','','','','SA059956','DATE','','','Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','AGREEMENT_ID',660,'Agreement Id','AGREEMENT_ID','','','','SA059956','NUMBER','','','Agreement Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','AGREEMENT_HEADER_STATUS',660,'Agreement Header Status','AGREEMENT_HEADER_STATUS','','','','SA059956','VARCHAR2','','','Agreement Header Status','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','AGREEMENT_LINE_STATUS',660,'Agreement Line Status','AGREEMENT_LINE_STATUS','','','','SA059956','VARCHAR2','','','Agreement Line Status','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','JOB_NAME',660,'Job Name','JOB_NAME','','','','SA059956','VARCHAR2','','','Job Name','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','JOB_NUMBER',660,'Job Number','JOB_NUMBER','','','','SA059956','VARCHAR2','','','Job Number','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','MASTER_ACCOUNT_NAME',660,'Master Account Name','MASTER_ACCOUNT_NAME','','','','SA059956','VARCHAR2','','','Master Account Name','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','MASTER_ACCOUNT_NUMBER',660,'Master Account Number','MASTER_ACCOUNT_NUMBER','','','','SA059956','VARCHAR2','','','Master Account Number','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','SALESREP_NAME',660,'Salesrep Name','SALESREP_NAME','','','','SA059956','VARCHAR2','','','Salesrep Name','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_WC_OM_CSP_AGRMNT_STATUS_V','SALESREP_NUMBER',660,'Salesrep Number','SALESREP_NUMBER','','','','SA059956','VARCHAR2','','','Salesrep Number','','','','');
--Inserting Object Components for EIS_WC_OM_CSP_AGRMNT_STATUS_V
--Inserting Object Component Joins for EIS_WC_OM_CSP_AGRMNT_STATUS_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report LOV Data for CSP Agreement Status - WC
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - CSP Agreement Status - WC
xxeis.eis_rsc_ins.lov( 660,'SELECT organization_code branch,
  organization_name
FROM org_organization_definitions ood
WHERE SYSDATE < NVL (ood.disable_date, SYSDATE + 1)
AND EXISTS
  (SELECT 1
  FROM xxeis.eis_org_access_v
  WHERE organization_id = ood.organization_id
  )','','XXWC OM Organization Code','','ANONYMOUS',NULL,'Y','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT DISTINCT mp.attribute9 region
FROM mtl_parameters mp
WHERE mp.attribute9 IS NOT NULL
ORDER BY 1','','EIS XXWC Region LOV','This LOV Lists all region details.','SA059956',NULL,'N','','','N','N','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
prompt Creating Report Data for CSP Agreement Status - WC
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(660);
IF mod_exist = 'Y' THEN 
--Deleting Report data - CSP Agreement Status - WC
xxeis.eis_rsc_utility.delete_report_rows( 'CSP Agreement Status - WC' );
--Inserting Report - CSP Agreement Status - WC
xxeis.eis_rsc_ins.r( 660,'CSP Agreement Status - WC','','CSP agreement VQN lines that are in DRAFT status','','','','SA059956','EIS_WC_OM_CSP_AGRMNT_STATUS_V','Y','','','SA059956','','N','Pricing','','EXCEL,','N','','','','','','N','','US','','','','');
--Inserting Report Columns - CSP Agreement Status - WC
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'BRANCH','Branch','Branch','','','default','','12','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'REGION','Region','Region','','','default','','1','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'SKU','SKU','Sku','','','default','','8','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'SPECIAL_COST','Special Cost','Special Cost','','~,~.~2','default','','9','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'START_DATE','Start Date','Start Date','','','default','','13','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','11','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'VQN_NUMBER','VQN Number','Vqn Number','','','default','','10','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'AGREEMENT_HEADER_STATUS','Agreement Header Status','Agreement Header Status','','','default','','14','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'AGREEMENT_LINE_STATUS','Agreement Line Status','Agreement Line Status','','','default','','15','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'JOB_NAME','Job Name','Job Name','','','default','','5','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'JOB_NUMBER','Job Number','Job Number','','','default','','6','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'MASTER_ACCOUNT_NAME','Master Account Name','Master Account Name','','','default','','3','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'MASTER_ACCOUNT_NUMBER','Master Account Number','Master Account Number','','','default','','4','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'SALESREP_NAME','Salesrep Name','Salesrep Name','','','default','','7','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'CSP Agreement Status - WC',660,'AGREEMENT_ID','Agreement ID','Agreement Id','','~~~','default','','2','','Y','','','','','','','SA059956','N','N','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','US','');
--Inserting Report Parameters - CSP Agreement Status - WC
xxeis.eis_rsc_ins.rp( 'CSP Agreement Status - WC',660,'Branch','Branch','BRANCH','IN','XXWC OM Organization Code','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','US','');
xxeis.eis_rsc_ins.rp( 'CSP Agreement Status - WC',660,'Region','Region','REGION','IN','EIS XXWC Region LOV','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','US','');
--Inserting Dependent Parameters - CSP Agreement Status - WC
--Inserting Report Conditions - CSP Agreement Status - WC
xxeis.eis_rsc_ins.rcnh( 'CSP Agreement Status - WC',660,'EWOCASV.BRANCH IN Branch','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','BRANCH','','Branch','','','','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','','','IN','Y','Y','','','','','1',660,'CSP Agreement Status - WC','EWOCASV.BRANCH IN Branch');
xxeis.eis_rsc_ins.rcnh( 'CSP Agreement Status - WC',660,'EWOCASV.REGION IN Region','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','REGION','','Region','','','','','EIS_WC_OM_CSP_AGRMNT_STATUS_V','','','','','','IN','Y','Y','','','','','1',660,'CSP Agreement Status - WC','EWOCASV.REGION IN Region');
--Inserting Report Sorts - CSP Agreement Status - WC
xxeis.eis_rsc_ins.rs( 'CSP Agreement Status - WC',660,'','ASC','SA059956','1','');
--Inserting Report Triggers - CSP Agreement Status - WC
--inserting report templates - CSP Agreement Status - WC
--Inserting Report Portals - CSP Agreement Status - WC
--inserting report dashboards - CSP Agreement Status - WC
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'CSP Agreement Status - WC','660','EIS_WC_OM_CSP_AGRMNT_STATUS_V','EIS_WC_OM_CSP_AGRMNT_STATUS_V','N','');
--inserting report security - CSP Agreement Status - WC
xxeis.eis_rsc_ins.rsec( 'CSP Agreement Status - WC','661','','XXWC_PRICING_MANAGER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'CSP Agreement Status - WC','660','','XXWC_ORDER_MGMT_PRICING_SUPER',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'CSP Agreement Status - WC','660','','XXWC_ORDER_MGMT_PRICING_FULL',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'CSP Agreement Status - WC','660','','XXWC_ORDER_MGMT_PRICING_STD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'CSP Agreement Status - WC','660','','XXWC_ORDER_MGMT_PRICING_LTD',660,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'CSP Agreement Status - WC','','SS084202','',660,'SA059956','','N','');
--Inserting Report Pivots - CSP Agreement Status - WC
--Inserting Report   Version details- CSP Agreement Status - WC
xxeis.eis_rsc_ins.rv( 'CSP Agreement Status - WC','','CSP Agreement Status - WC','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 660');
END IF;
END;
/
