--Report Name            : Daily Flash Delivery Report � for Sales Revenue as % to Plan
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
Create or replace View XXEIS.EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
(CURRENT_FISCAL_MONTH,CURRENT_FISCAL_YEAR,DELIVERY_TYPE,BRANCH_LOCATION,INVOICE_COUNT,SALES) AS 
SELECT gp.period_name current_fiscal_month,
    gp.period_year current_fiscal_year,
    xxeis.eis_rs_xxwc_com_util_pkg.get_shipping_mthd3 ( ol.shipping_method_code) delivery_type,
    mp.organization_code branch_location,
    COUNT ( DISTINCT xxeis.eis_rs_xxwc_com_util_pkg.get_dist_invoice_num ( rct.customer_trx_id, rct.trx_number, otlh.name)) invoice_count,
    SUM ( NVL (rctl.quantity_invoiced, rctl.quantity_credited) * NVL (ol.unit_selling_price, 0)) sales
    -- NVL(SUM(DECODE(otl.order_category_code,'RETURN',(NVL(Ol.ordered_quantity,0)*-1),NVL(Ol.ordered_quantity,0)) * NVL(Ol.Unit_Selling_Price,0)),0) sales
    -- SUM(oep.payment_amount) cash_sales,
    --   xxeis.eis_rs_xxwc_com_util_pkg.get_payment_type payment_type
  FROM ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    oe_order_headers oh,
    oe_order_lines ol,
    org_organization_definitions mp,
    gl_periods gp,
    gl_sets_of_books gsob,
    oe_transaction_types_vl otl,
    oe_transaction_types_vl otlh
  WHERE 1 = 1
    --AND TRUNC(rct.creation_date)       = TRUNC(xxeis.eis_rs_xxwc_com_util_pkg.get_date_from)
    /* AND rct.creation_date >= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 0.25
    AND rct.creation_date <= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 1.25*/
  AND rctl.customer_trx_id           = rct.customer_trx_id
  AND rctl.interface_line_attribute6 = TO_CHAR (ol.line_id)
  AND rctl.inventory_item_id         = ol.inventory_item_id
  AND ol.header_id                   = oh.header_id
  AND otl.transaction_type_id        = ol.line_type_id
  AND otlh.transaction_type_id       = oh.order_type_id
  AND TO_CHAR (oh.order_number)      = rct.interface_header_attribute1
  AND mp.organization_id(+)          = ol.ship_from_org_id
  AND gsob.set_of_books_id           = mp.set_of_books_id
  AND ol.ordered_item NOT           IN('CONTOFFSET','CONTBILL')
  AND gsob.period_set_name           = gp.period_set_name
    /* commented by velumula*/
    /* AND ((xxeis.eis_rs_xxwc_com_util_pkg.get_period_name_from IS NULL
    AND gp.period_name                                        IN
    (SELECT period_name
    FROM gl_periods gp1
    WHERE gp1.period_set_name = gp.period_set_name
    AND TRUNC(sysdate) BETWEEN TRUNC(gp.start_date) AND TRUNC(gp.end_date)
    ) )
    OR (xxeis.eis_rs_xxwc_com_util_pkg.get_period_name_from IS NOT NULL
    AND gp.period_name                                       = xxeis.eis_rs_xxwc_com_util_pkg.get_period_name_from) )*/
  AND gp.period_name = xxeis.eis_rs_xxwc_com_util_pkg.get_period_name_from
  AND rct.creation_date BETWEEN gp.start_date+0.25 AND gp.end_date+1.25
    /*AND EXISTS
    (SELECT 1
    FROM oe_payments oep
    WHERE oep.header_id        = ol.header_id
    AND oep.payment_type_code IN ('CASH','CREDIT_CARD')
    )*/
  AND rctl.interface_line_context(+) = 'ORDER ENTRY'
  AND NOT EXISTS
    (SELECT 1
    FROM hz_customer_profiles hcp,
      hz_cust_profile_classes hcpc,
      hz_cust_accounts hca
    WHERE hca.party_id       = hcp.party_id
    AND oh.sold_to_org_id    = hcp.cust_account_id
    AND hcp.site_use_id     IS NULL
    AND hcp.profile_class_id = hcpc.profile_class_id(+)
    AND hcpc.name LIKE 'WC%Branches%'
    )
    /*AND NOT EXISTS
    (SELECT 1
    FROM OE_PRICE_ADJUSTMENTS_V
    WHERE HEADER_ID     = OL.HEADER_ID
    AND LINE_ID         = OL.LINE_ID
    AND adjustment_name ='BRANCH_COST_MODIFIER'
    )*/
  GROUP BY gp.period_name,
    gp.period_year,
    xxeis.eis_rs_xxwc_com_util_pkg.get_shipping_mthd3 ( ol.shipping_method_code),
    mp.organization_code
/
set scan on define on
prompt Creating View Data for Daily Flash Delivery Report � for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Ar Flsh Sale By Del V','EXAFSBDV','','');
--Delete View Columns for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_FLSH_SALE_BY_DEL_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','BRANCH_LOCATION',660,'Branch Location','BRANCH_LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','CURRENT_FISCAL_MONTH',660,'Current Fiscal Month','CURRENT_FISCAL_MONTH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Current Fiscal Month','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','CURRENT_FISCAL_YEAR',660,'Current Fiscal Year','CURRENT_FISCAL_YEAR','','','','XXEIS_RS_ADMIN','NUMBER','','','Current Fiscal Year','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','DELIVERY_TYPE',660,'Delivery Type','DELIVERY_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Delivery Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','SALES',660,'Sales','SALES','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','INVOICE_COUNT',660,'Invoice Count','INVOICE_COUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoice Count','','','');
--Inserting View Components for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
--Inserting View Component Joins for EIS_XXWC_AR_FLSH_SALE_BY_DEL_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Daily Flash Delivery Report � for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Daily Flash Delivery Report � for Sales Revenue as % to Plan
xxeis.eis_rs_ins.lov( 660,'select  distinct per.period_name , per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     led.accounted_period_type = per.period_type
order by per.period_num asc,
per.period_year desc','','OM PERIOD NAMES','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Daily Flash Delivery Report � for Sales Revenue as % to Plan
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Daily Flash Delivery Report � for Sales Revenue as % to Plan
xxeis.eis_rs_utility.delete_report_rows( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan' );
--Inserting Report - Daily Flash Delivery Report � for Sales Revenue as % to Plan
xxeis.eis_rs_ins.r( 660,'Daily Flash Delivery Report � for Sales Revenue as % to Plan','','The purpose of this extract is to provide Finance with a daily report of all sales by branch and aggregated by delivery type.  The selected date parameter represents the desired date of sales (e.g. For sales reported on Aug 1st, set the Date parameter = Aug. 1st.   This report is to be processed daily and intended to accompany the daily extracts, flash_charge and flash_cash.
','','','','XXEIS_RS_ADMIN','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,HTML,EXCEL,','N');
--Inserting Report Columns - Daily Flash Delivery Report � for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'BRANCH_LOCATION','Location','Branch Location','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'DELIVERY_TYPE','Delivery Type','Delivery Type','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'SALES','Net Sales Dollars','Sales','','~T~D~2','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'INVOICE_COUNT','Invoice Count','Invoice Count','','~~~','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'CURRENT_FISCAL_MONTH','Fiscal Month','Current Fiscal Month','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
xxeis.eis_rs_ins.rc( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'CURRENT_FISCAL_YEAR','Fiscal Year','Current Fiscal Year','','~~~','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_AR_FLSH_SALE_BY_DEL_V','','');
--Inserting Report Parameters - Daily Flash Delivery Report � for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'Location','Location','BRANCH_LOCATION','IN','OM Warehouse All','','VARCHAR2','Y','Y','2','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'Period Name','Period Name','','IN','OM PERIOD NAMES','','VARCHAR2','N','Y','1','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Daily Flash Delivery Report � for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rcn( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'','','','','AND ( ''All'' IN (:Location) OR (BRANCH_LOCATION IN (:Location)))','Y','1','','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Daily Flash Delivery Report � for Sales Revenue as % to Plan
--Inserting Report Triggers - Daily Flash Delivery Report � for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rt( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan',660,'begin

xxeis.eis_rs_xxwc_com_util_pkg.SET_PERIOD_NAME(:Period Name);

end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Daily Flash Delivery Report � for Sales Revenue as % to Plan
--Inserting Report Portals - Daily Flash Delivery Report � for Sales Revenue as % to Plan
--Inserting Report Dashboards - Daily Flash Delivery Report � for Sales Revenue as % to Plan
--Inserting Report Security - Daily Flash Delivery Report � for Sales Revenue as % to Plan
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','','LC053655','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','','10010432','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','','RB054040','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','','RV003897','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','','SS084202','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','','SO004816','',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50870',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50871',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','50869',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','20005','','50900',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Daily Flash Delivery Report � for Sales Revenue as % to Plan','660','','51044',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Daily Flash Delivery Report � for Sales Revenue as % to Plan
END;
/
set scan on define on
