--Report Name            : Purchases and Accruals -  Summary
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_PURCH_ACCR_SUM_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_PURCH_ACCR_SUM_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_PURCH_ACCR_SUM_V
 ("AGREEMENT_YEAR","CALENDAR_YEAR","FISCAL_YEAR","FISCAL_PERIOD","MVID","VENDOR_NAME","LOB","BU","PURCHASES","REBATE","COOP","TOTAL") as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_PURCH_ACCR_SUM_V
 ("AGREEMENT_YEAR","CALENDAR_YEAR","FISCAL_YEAR","FISCAL_PERIOD","MVID","VENDOR_NAME","LOB","BU","PURCHASES","REBATE","COOP","TOTAL") as ');
l_stmt :=  'SELECT
      agreement_year
      ,calendar_year
      ,fiscal_year
      ,fiscal_period
      ,mvid
      ,vendor_name 
      ,LOB
      ,BU 
      ,SUM(PURCHASES)  AS PURCHASES
      ,SUM(REBATE) AS REBATE
      , SUM(COOP) AS COOP
      ,SUM(TOTAL) AS TOTAL
FROM 



(
SELECT 
  agreement_year
  ,calendar_year
  ,fiscal_year
  ,fiscal_period
  ,MVID
  ,c.party_name vendor_name
  ,LOB
  ,BU 
  , 0 Purchases
  ,SUM(CASE WHEN REBATE_TYPE =''REBATE'' THEN ACCRUAL_AMOUNT ELSE 0 END) REBATE
  ,SUM(CASE WHEN REBATE_TYPE =''COOP'' THEN ACCRUAL_AMOUNT ELSE 0 END) COOP
  ,SUM(ACCRUAL_AMOUNT) TOTAL
FROM XXCUS.XXCUS_YTD_INCOME_B
,XXCUS.XXCUS_REBATE_CUSTOMERS C
WHERE 1=1
  AND C.CUSTOMER_ID=OFU_CUST_ACCOUNT_ID
  AND C.PARTY_ATTRIBUTE1 =''HDS_MVID''
GROUP BY
  agreement_year
  ,calendar_year
  ,fiscal_year
  ,fiscal_period
  ,mvid
  ,c.party_name 
  ,LOB
  ,BU

union
 SELECT 
       to_number(M.CALENDAR_YEAR) AGREEMENT_YEAR
       ,to_number(M.CALENDAR_YEAR) calendar_year
       ,YEAR_ID FISCAL_YEAR
       ,t.name fiscal_period
       ,C.CUSTOMER_ATTRIBUTE2 MVID
      ,C.PARTY_NAME VENDOR_name    
      ,Z.PARTY_NAME LOB
      ,z1.party_name BU
      ,M.TOTAL_PURCHASES PURCHASES
      ,0 REBATE
      ,0 COOP
      ,0 TOTAL

 FROM  
        APPS.XXCUSOZF_PURCHASES_MV M
        ,XXCUS.XXCUS_REBATE_CUSTOMERS C
        ,APPS.HZ_PARTIES Z
        ,APPS.HZ_PARTIES Z1
        ,ozf.ozf_time_ent_period t

       
WHERE 1=1
        AND M.LOB_ID=Z.PARTY_ID
        AND M.MVID=C.CUSTOMER_ID
        AND M.BU_ID=Z1.PARTY_ID
        and m.period_id=t.ent_period_id
        AND GRP_MVID       = 0
        AND grp_branch     = 1
        AND GRP_QTR        = 1
        AND grp_year       = 0
        AND GRP_LOB        = 0
        AND  GRP_BU_ID       = 0
        AND grp_period     = 0
        AND grp_cal_year   =0
        AND GRP_CAL_PERIOD = 1
        AND C.PARTY_ATTRIBUTE1=''HDS_MVID'' 
        AND Z.ATTRIBUTE1=''HDS_LOB''
        and z1.attribute1=''HDS_BU''
        )
group by 
      agreement_year
      ,calendar_year
      ,fiscal_year
      ,fiscal_period
      ,mvid
      ,vendor_name 
      ,LOB
      ,BU 





             

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Object Data XXEIS_PURCH_ACCR_SUM_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_PURCH_ACCR_SUM_V
xxeis.eis_rsc_ins.v( 'XXEIS_PURCH_ACCR_SUM_V',682,'Paste SQL View for Purchases and Accruals -  Summary','','','','DV003828','APPS','Purchases and Accruals -  Summary View','X1UV','','Y','VIEW','US','','');
--Delete Object Columns for XXEIS_PURCH_ACCR_SUM_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_PURCH_ACCR_SUM_V',682,FALSE);
--Inserting Object Columns for XXEIS_PURCH_ACCR_SUM_V
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','MVID',682,'','MVID','','','','DV003828','VARCHAR2','','','Mvid','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','LOB',682,'','LOB','','','','DV003828','VARCHAR2','','','Lob','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','REBATE',682,'','REBATE','','','','DV003828','NUMBER','','','Rebate','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','COOP',682,'','COOP','','','','DV003828','NUMBER','','','Coop','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','TOTAL',682,'','TOTAL','','~T~D~2','','DV003828','NUMBER','','','Total','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','AGREEMENT_YEAR',682,'','','','','','DV003828','NUMBER','','','Agreement Year','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','BU',682,'','','','','','DV003828','VARCHAR2','','','Bu','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','CALENDAR_YEAR',682,'','','','','','DV003828','NUMBER','','','Calendar Year','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','FISCAL_PERIOD',682,'','','','','','DV003828','VARCHAR2','','','Fiscal Period','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','FISCAL_YEAR',682,'','','','','','DV003828','NUMBER','','','Fiscal Year','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','PURCHASES',682,'','','','','','DV003828','NUMBER','','','Purchases','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_PURCH_ACCR_SUM_V','VENDOR_NAME',682,'','','','','','DV003828','VARCHAR2','','','Vendor Name','','','','US');
--Inserting Object Components for XXEIS_PURCH_ACCR_SUM_V
--Inserting Object Component Joins for XXEIS_PURCH_ACCR_SUM_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report LOV Data for Purchases and Accruals -  Summary
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - Purchases and Accruals -  Summary
xxeis.eis_rsc_ins.lov( 682,'select distinct nvl(attribute7, ''NULL'') agreement_year
from apps.QP_LIST_HEADERS_VL','','LOV AGREEMENT_YEAR','LOV attribute7 from apps.QP_LIST_HEADERS_VL  table is the agreement year','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_MVID''','','LOV VENDOR_NAME','VENDOR NAME FROM ar.HZ_PARTIES
','ID020048',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct party_name
from ar.HZ_PARTIES
where ATTRIBUTE1 = ''HDS_LOB''
and party_name not in (''PLUMBING null'', ''INDUSTRIAL PVF'')','','HDS LOB_NAME','LOV party_name From ar.HZ_PARTIES is the LOB','ANONYMOUS',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 682,'select distinct name
from ozf.OZF_TIME_ENT_PERIOD','','LOV PERIOD','LOV NAME FROM ozf.OZF_TIME_ENT_PERIOD is the Period','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
prompt Creating Report Data for Purchases and Accruals -  Summary
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(682);
IF mod_exist = 'Y' THEN 
--Deleting Report data - Purchases and Accruals -  Summary
xxeis.eis_rsc_utility.delete_report_rows( 'Purchases and Accruals -  Summary' );
--Inserting Report - Purchases and Accruals -  Summary
xxeis.eis_rsc_ins.r( 682,'Purchases and Accruals -  Summary','','Spend and Accruals by Vendor, Calendar Year, Agreement Year, Fiscal Year, Fiscal Period, LOB, BU
Objects used:  XXCUS_YTD_INCOME_B, XXCUSOZF_PURCHASES_MV
','','','','MC027824','XXEIS_PURCH_ACCR_SUM_V','Y','','-------------------------------------------------------------------------------
----PURCHASES AND ACCRUALS -  Summary-------
-------------------------------------------------------------------------------
SELECT
      agreement_year
      ,calendar_year
      ,fiscal_year
      ,fiscal_period
      ,mvid
      ,vendor_name 
      ,LOB
      ,BU 
      ,SUM(PURCHASES)  AS PURCHASES
      ,SUM(REBATE) AS REBATE
      , SUM(COOP) AS COOP
      ,SUM(TOTAL) AS TOTAL
FROM 



(
SELECT 
  agreement_year
  ,calendar_year
  ,fiscal_year
  ,fiscal_period
  ,MVID
  ,c.party_name vendor_name
  ,LOB
  ,BU 
  , 0 Purchases
  ,SUM(CASE WHEN REBATE_TYPE =''REBATE'' THEN ACCRUAL_AMOUNT ELSE 0 END) REBATE
  ,SUM(CASE WHEN REBATE_TYPE =''COOP'' THEN ACCRUAL_AMOUNT ELSE 0 END) COOP
  ,SUM(ACCRUAL_AMOUNT) TOTAL
FROM XXCUS.XXCUS_YTD_INCOME_B
,XXCUS.XXCUS_REBATE_CUSTOMERS C
WHERE 1=1
  AND C.CUSTOMER_ID=OFU_CUST_ACCOUNT_ID
  AND C.PARTY_ATTRIBUTE1 =''HDS_MVID''
GROUP BY
  agreement_year
  ,calendar_year
  ,fiscal_year
  ,fiscal_period
  ,mvid
  ,c.party_name 
  ,LOB
  ,BU

union
 SELECT 
       to_number(M.CALENDAR_YEAR) AGREEMENT_YEAR
       ,to_number(M.CALENDAR_YEAR) calendar_year
       ,YEAR_ID FISCAL_YEAR
       ,t.name fiscal_period
       ,C.CUSTOMER_ATTRIBUTE2 MVID
      ,C.PARTY_NAME VENDOR_name    
      ,Z.PARTY_NAME LOB
      ,z1.party_name BU
      ,M.TOTAL_PURCHASES PURCHASES
      ,0 REBATE
      ,0 COOP
      ,0 TOTAL

 FROM  
        APPS.XXCUSOZF_PURCHASES_MV M
        ,XXCUS.XXCUS_REBATE_CUSTOMERS C
        ,APPS.HZ_PARTIES Z
        ,APPS.HZ_PARTIES Z1
        ,ozf.ozf_time_ent_period t

       
WHERE 1=1
        AND M.LOB_ID=Z.PARTY_ID
        AND M.MVID=C.CUSTOMER_ID
        AND M.BU_ID=Z1.PARTY_ID
        and m.period_id=t.ent_period_id
        AND GRP_MVID       = 0
        AND grp_branch     = 1
        AND GRP_QTR        = 1
        AND grp_year       = 0
        AND GRP_LOB        = 0
        AND  GRP_BU_ID       = 0
        AND grp_period     = 0
        AND grp_cal_year   =0
        AND GRP_CAL_PERIOD = 1
        AND C.PARTY_ATTRIBUTE1=''HDS_MVID'' 
        AND Z.ATTRIBUTE1=''HDS_LOB''
        and z1.attribute1=''HDS_BU''
        )
group by 
      agreement_year
      ,calendar_year
      ,fiscal_year
      ,fiscal_period
      ,mvid
      ,vendor_name 
      ,LOB
      ,BU 





             
','MC027824','','N','ADHOC - SUMMARY ACCRUAL DETAILS','','CSV,EXCEL,Pivot Excel,','','','','','','','N','APPS','US','','','','');
--Inserting Report Columns - Purchases and Accruals -  Summary
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'TOTAL','Total','','','~,~.~2','default','','12','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'MVID','MVID','','','','default','','7','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'COOP','Coop','','','~,~.~2','default','','11','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'LOB','LOB','','','','default','','5','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'REBATE','Rebate','','','~,~.~2','default','','10','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'AGREEMENT_YEAR','Agreement Year','','','~T~D~0','default','','1','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'BU','BU','','','','default','','6','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'CALENDAR_YEAR','Calendar Year','','','~T~D~0','default','','2','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'FISCAL_PERIOD','Fiscal Period','','','','default','','4','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'FISCAL_YEAR','Fiscal Year','','','~T~D~0','default','','3','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'PURCHASES','Purchases','','','~,~.~2','default','','9','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'Purchases and Accruals -  Summary',682,'VENDOR_NAME','Vendor Name','','','','default','','8','N','','','','','','','','MC027824','N','N','','XXEIS_PURCH_ACCR_SUM_V','','','GROUP_BY','US','');
--Inserting Report Parameters - Purchases and Accruals -  Summary
xxeis.eis_rsc_ins.rp( 'Purchases and Accruals -  Summary',682,'Lob','','LOB','IN','HDS LOB_NAME','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_PURCH_ACCR_SUM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchases and Accruals -  Summary',682,'Vendor','','VENDOR_NAME','IN','LOV VENDOR_NAME','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_PURCH_ACCR_SUM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchases and Accruals -  Summary',682,'Agreement Year','','AGREEMENT_YEAR','IN','LOV AGREEMENT_YEAR','','VARCHAR2','N','Y','1','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_PURCH_ACCR_SUM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchases and Accruals -  Summary',682,'Calendar Year','','CALENDAR_YEAR','IN','LOV AGREEMENT_YEAR','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_PURCH_ACCR_SUM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchases and Accruals -  Summary',682,'Fiscal Period','','FISCAL_PERIOD','IN','LOV PERIOD','','VARCHAR2','N','Y','5','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_PURCH_ACCR_SUM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchases and Accruals -  Summary',682,'Fiscal Year','','FISCAL_YEAR','IN','LOV AGREEMENT_YEAR','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_PURCH_ACCR_SUM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'Purchases and Accruals -  Summary',682,'Vendor Name','','VENDOR_NAME','IN','LOV VENDOR_NAME','','VARCHAR2','N','Y','7','Y','Y','CONSTANT','MC027824','Y','N','','','','XXEIS_PURCH_ACCR_SUM_V','','','US','');
--Inserting Dependent Parameters - Purchases and Accruals -  Summary
--Inserting Report Conditions - Purchases and Accruals -  Summary
xxeis.eis_rsc_ins.rcnh( 'Purchases and Accruals -  Summary',682,'AGREEMENT_YEAR IN :Agreement Year ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','AGREEMENT_YEAR','','Agreement Year','','','','','XXEIS_PURCH_ACCR_SUM_V','','','','','','IN','Y','Y','','','','','1',682,'Purchases and Accruals -  Summary','AGREEMENT_YEAR IN :Agreement Year ');
xxeis.eis_rsc_ins.rcnh( 'Purchases and Accruals -  Summary',682,'CALENDAR_YEAR IN :Calendar Year ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','CALENDAR_YEAR','','Calendar Year','','','','','XXEIS_PURCH_ACCR_SUM_V','','','','','','IN','Y','Y','','','','','1',682,'Purchases and Accruals -  Summary','CALENDAR_YEAR IN :Calendar Year ');
xxeis.eis_rsc_ins.rcnh( 'Purchases and Accruals -  Summary',682,'FISCAL_PERIOD IN :Fiscal Period ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','FISCAL_PERIOD','','Fiscal Period','','','','','XXEIS_PURCH_ACCR_SUM_V','','','','','','IN','Y','Y','','','','','1',682,'Purchases and Accruals -  Summary','FISCAL_PERIOD IN :Fiscal Period ');
xxeis.eis_rsc_ins.rcnh( 'Purchases and Accruals -  Summary',682,'FISCAL_YEAR IN :Fiscal Year ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','FISCAL_YEAR','','Fiscal Year','','','','','XXEIS_PURCH_ACCR_SUM_V','','','','','','IN','Y','Y','','','','','1',682,'Purchases and Accruals -  Summary','FISCAL_YEAR IN :Fiscal Year ');
xxeis.eis_rsc_ins.rcnh( 'Purchases and Accruals -  Summary',682,'LOB IN :Lob ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','LOB','','Lob','','','','','XXEIS_PURCH_ACCR_SUM_V','','','','','','IN','Y','Y','','','','','1',682,'Purchases and Accruals -  Summary','LOB IN :Lob ');
xxeis.eis_rsc_ins.rcnh( 'Purchases and Accruals -  Summary',682,'VENDOR_NAME IN :Vendor Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Vendor Name','','','','','XXEIS_PURCH_ACCR_SUM_V','','','','','','IN','Y','Y','','','','','1',682,'Purchases and Accruals -  Summary','VENDOR_NAME IN :Vendor Name ');
xxeis.eis_rsc_ins.rcnh( 'Purchases and Accruals -  Summary',682,'X1UV.VENDOR_NAME IN Vendor','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','VENDOR_NAME','','Vendor','','','','','XXEIS_PURCH_ACCR_SUM_V','','','','','','IN','Y','Y','','','','','1',682,'Purchases and Accruals -  Summary','X1UV.VENDOR_NAME IN Vendor');
--Inserting Report Sorts - Purchases and Accruals -  Summary
xxeis.eis_rsc_ins.rs( 'Purchases and Accruals -  Summary',682,'AGREEMENT_YEAR','','MC027824','1','');
xxeis.eis_rsc_ins.rs( 'Purchases and Accruals -  Summary',682,'CALENDAR_YEAR','','MC027824','2','');
xxeis.eis_rsc_ins.rs( 'Purchases and Accruals -  Summary',682,'FISCAL_YEAR','','MC027824','3','');
xxeis.eis_rsc_ins.rs( 'Purchases and Accruals -  Summary',682,'FISCAL_PERIOD','','MC027824','4','');
xxeis.eis_rsc_ins.rs( 'Purchases and Accruals -  Summary',682,'LOB','','MC027824','5','');
xxeis.eis_rsc_ins.rs( 'Purchases and Accruals -  Summary',682,'BU','','MC027824','6','');
xxeis.eis_rsc_ins.rs( 'Purchases and Accruals -  Summary',682,'MVID','','MC027824','7','');
--Inserting Report Triggers - Purchases and Accruals -  Summary
--inserting report templates - Purchases and Accruals -  Summary
--Inserting Report Portals - Purchases and Accruals -  Summary
--inserting report dashboards - Purchases and Accruals -  Summary
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'Purchases and Accruals -  Summary','682','XXEIS_PURCH_ACCR_SUM_V','XXEIS_PURCH_ACCR_SUM_V','N','');
--inserting report security - Purchases and Accruals -  Summary
xxeis.eis_rsc_ins.rsec( 'Purchases and Accruals -  Summary','682','','XXCUS_TM_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'Purchases and Accruals -  Summary','682','','XXCUS_TM_ADMIN_USER',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'Purchases and Accruals -  Summary','682','','XXCUS_TM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'Purchases and Accruals -  Summary','682','','XXCUS_TM_ADM_FORMS_RESP',682,'MC027824','','','');
xxeis.eis_rsc_ins.rsec( 'Purchases and Accruals -  Summary','20005','','XXWC_VIEW_ALL_EIS_REPORTS',682,'MC027824','','','');
--Inserting Report Pivots - Purchases and Accruals -  Summary
xxeis.eis_rsc_ins.rpivot( 'Purchases and Accruals -  Summary',682,'Calendar Year','1','0,0|1,1,0','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Calendar Year
xxeis.eis_rsc_ins.rpivot_dtls( 'Purchases and Accruals -  Summary',682,'Calendar Year','MVID','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Purchases and Accruals -  Summary',682,'Calendar Year','VENDOR_NAME','ROW_FIELD','','','2','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Purchases and Accruals -  Summary',682,'Calendar Year','LOB','ROW_FIELD','','','3','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Purchases and Accruals -  Summary',682,'Calendar Year','BU','ROW_FIELD','','','4','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Purchases and Accruals -  Summary',682,'Calendar Year','PURCHASES','DATA_FIELD','SUM','','1','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Purchases and Accruals -  Summary',682,'Calendar Year','REBATE','DATA_FIELD','SUM','','2','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Purchases and Accruals -  Summary',682,'Calendar Year','COOP','DATA_FIELD','SUM','','3','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Purchases and Accruals -  Summary',682,'Calendar Year','TOTAL','DATA_FIELD','SUM','','4','','');
xxeis.eis_rsc_ins.rpivot_dtls( 'Purchases and Accruals -  Summary',682,'Calendar Year','AGREEMENT_YEAR','PAGE_FIELD','','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Calendar Year
--Inserting Report   Version details- Purchases and Accruals -  Summary
xxeis.eis_rsc_ins.rv( 'Purchases and Accruals -  Summary','','Purchases and Accruals -  Summary','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 682');
END IF;
END;
/
