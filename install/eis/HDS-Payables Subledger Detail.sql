--Report Name            : HDS-Payables Subledger Detail
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXHDS_GL_SL_180_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.v( 'EIS_XXHDS_GL_SL_180_V',101,'','','','','MT063505','XXEIS','Eis Xxhds Gl Sl 180 V','EXGS_V','','','VIEW','US','','');
--Delete Object Columns for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXHDS_GL_SL_180_V',101,FALSE);
--Inserting Object Columns for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#FUTURE_USE',101,'Gcc#50368#Future Use','GCC#50368#FUTURE_USE','','','','MT063505','VARCHAR2','','','Gcc#50368#Future Use','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DIVISION#DESCR',101,'Gcc#50368#Division#Descr','GCC#50368#DIVISION#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Division#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DIVISION',101,'Gcc#50368#Division','GCC#50368#DIVISION','','','','MT063505','VARCHAR2','','','Gcc#50368#Division','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DEPARTMENT#DESCR',101,'Gcc#50368#Department#Descr','GCC#50368#DEPARTMENT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Department#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#DEPARTMENT',101,'Gcc#50368#Department','GCC#50368#DEPARTMENT','','','','MT063505','VARCHAR2','','','Gcc#50368#Department','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#ACCOUNT#DESCR',101,'Gcc#50368#Account#Descr','GCC#50368#ACCOUNT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#ACCOUNT',101,'Gcc#50368#Account','GCC#50368#ACCOUNT','','','','MT063505','VARCHAR2','','','Gcc#50368#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PROJECT_CODE#DESCR',101,'Gcc#50328#Project Code#Descr','GCC#50328#PROJECT_CODE#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Project Code#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PROJECT_CODE',101,'Gcc#50328#Project Code','GCC#50328#PROJECT_CODE','','','','MT063505','VARCHAR2','','','Gcc#50328#Project Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PRODUCT#DESCR',101,'Gcc#50328#Product#Descr','GCC#50328#PRODUCT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#PRODUCT',101,'Gcc#50328#Product','GCC#50328#PRODUCT','','','','MT063505','VARCHAR2','','','Gcc#50328#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#LOCATION#DESCR',101,'Gcc#50328#Location#Descr','GCC#50328#LOCATION#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Location#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#LOCATION',101,'Gcc#50328#Location','GCC#50328#LOCATION','','','','MT063505','VARCHAR2','','','Gcc#50328#Location','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FUTURE_USE_2#DESCR',101,'Gcc#50328#Future Use 2#Descr','GCC#50328#FUTURE_USE_2#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Future Use 2#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FUTURE_USE_2',101,'Gcc#50328#Future Use 2','GCC#50328#FUTURE_USE_2','','','','MT063505','VARCHAR2','','','Gcc#50328#Future Use 2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#COST_CENTER#DESCR',101,'Gcc#50328#Cost Center#Descr','GCC#50328#COST_CENTER#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Cost Center#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#COST_CENTER',101,'Gcc#50328#Cost Center','GCC#50328#COST_CENTER','','','','MT063505','VARCHAR2','','','Gcc#50328#Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#ACCOUNT#DESCR',101,'Gcc#50328#Account#Descr','GCC#50328#ACCOUNT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Account#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#ACCOUNT',101,'Gcc#50328#Account','GCC#50328#ACCOUNT','','','','MT063505','VARCHAR2','','','Gcc#50328#Account','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#BRANCH',101,'Gcc#Branch','GCC#BRANCH','','','','MT063505','VARCHAR2','','','Gcc#Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ENTERED_NET',101,'Sla Line Entered Net','SLA_LINE_ENTERED_NET','','','','MT063505','NUMBER','','','Sla Line Entered Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ACCOUNTED_NET',101,'Sla Line Accounted Net','SLA_LINE_ACCOUNTED_NET','','','','MT063505','NUMBER','','','Sla Line Accounted Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT5',101,'Segment5','SEGMENT5','','','','MT063505','VARCHAR2','','','Segment5','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT4',101,'Segment4','SEGMENT4','','','','MT063505','VARCHAR2','','','Segment4','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT3',101,'Segment3','SEGMENT3','','','','MT063505','VARCHAR2','','','Segment3','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT2',101,'Segment2','SEGMENT2','','','','MT063505','VARCHAR2','','','Segment2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEGMENT1',101,'Segment1','SEGMENT1','','','','MT063505','VARCHAR2','','','Segment1','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','CODE_COMBINATION_ID',101,'Code Combination Id','CODE_COMBINATION_ID','','','','MT063505','NUMBER','','','Code Combination Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','BATCH_NAME',101,'Batch Name','BATCH_NAME','','','','MT063505','VARCHAR2','','','Batch Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','NAME',101,'Name','NAME','','','','MT063505','VARCHAR2','','','Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','EFFECTIVE_PERIOD_NUM',101,'Effective Period Num','EFFECTIVE_PERIOD_NUM','','','','MT063505','NUMBER','','','Effective Period Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','TYPE',101,'Type','TYPE','','','','MT063505','VARCHAR2','','','Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','PERIOD_NAME',101,'Period Name','PERIOD_NAME','','','','MT063505','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','CURRENCY_CODE',101,'Currency Code','CURRENCY_CODE','','','','MT063505','VARCHAR2','','','Currency Code','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GL_ACCOUNT_STRING',101,'Gl Account String','GL_ACCOUNT_STRING','','','','MT063505','VARCHAR2','','','Gl Account String','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','TRANSACTION_NUM',101,'Transaction Num','TRANSACTION_NUM','','','','MT063505','VARCHAR2','','','Transaction Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ACCOUNTED_NET',101,'Sla Dist Accounted Net','SLA_DIST_ACCOUNTED_NET','','','','MT063505','NUMBER','','','Sla Dist Accounted Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ACCOUNTED_DR',101,'Sla Dist Accounted Dr','SLA_DIST_ACCOUNTED_DR','','','','MT063505','NUMBER','','','Sla Dist Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ACCOUNTED_CR',101,'Sla Dist Accounted Cr','SLA_DIST_ACCOUNTED_CR','','','','MT063505','NUMBER','','','Sla Dist Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ENTERED_NET',101,'Sla Dist Entered Net','SLA_DIST_ENTERED_NET','','','','MT063505','NUMBER','','','Sla Dist Entered Net','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ENTERED_DR',101,'Sla Dist Entered Dr','SLA_DIST_ENTERED_DR','','','','MT063505','NUMBER','','','Sla Dist Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_DIST_ENTERED_CR',101,'Sla Dist Entered Cr','SLA_DIST_ENTERED_CR','','','','MT063505','NUMBER','','','Sla Dist Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ENTERED_CR',101,'Sla Line Entered Cr','SLA_LINE_ENTERED_CR','','','','MT063505','NUMBER','','','Sla Line Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ENTERED_DR',101,'Sla Line Entered Dr','SLA_LINE_ENTERED_DR','','','','MT063505','NUMBER','','','Sla Line Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ACCOUNTED_CR',101,'Sla Line Accounted Cr','SLA_LINE_ACCOUNTED_CR','','','','MT063505','NUMBER','','','Sla Line Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SLA_LINE_ACCOUNTED_DR',101,'Sla Line Accounted Dr','SLA_LINE_ACCOUNTED_DR','','','','MT063505','NUMBER','','','Sla Line Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ACCOUNTED_DR',101,'Accounted Dr','ACCOUNTED_DR','','','','MT063505','NUMBER','','','Accounted Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ACCOUNTED_CR',101,'Accounted Cr','ACCOUNTED_CR','','','','MT063505','NUMBER','','','Accounted Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ENTERED_CR',101,'Entered Cr','ENTERED_CR','','','','MT063505','NUMBER','','','Entered Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ENTERED_DR',101,'Entered Dr','ENTERED_DR','','','','MT063505','NUMBER','','','Entered Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','CUSTOMER_OR_VENDOR',101,'Customer Or Vendor','CUSTOMER_OR_VENDOR','','','','MT063505','VARCHAR2','','','Customer Or Vendor','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GL_SL_LINK_ID',101,'Gl Sl Link Id','GL_SL_LINK_ID','','','','MT063505','NUMBER','','','Gl Sl Link Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','H_SEQ_ID',101,'H Seq Id','H_SEQ_ID','','','','MT063505','NUMBER','','','H Seq Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEQ_NUM',101,'Seq Num','SEQ_NUM','','','','MT063505','NUMBER','','','Seq Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SEQ_ID',101,'Seq Id','SEQ_ID','','','','MT063505','NUMBER','','','Seq Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ENT_CR',101,'Line Ent Cr','LINE_ENT_CR','','','','MT063505','NUMBER','','','Line Ent Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ENT_DR',101,'Line Ent Dr','LINE_ENT_DR','','','','MT063505','NUMBER','','','Line Ent Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ACCTD_CR',101,'Line Acctd Cr','LINE_ACCTD_CR','','','','MT063505','NUMBER','','','Line Acctd Cr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_ACCTD_DR',101,'Line Acctd Dr','LINE_ACCTD_DR','','','','MT063505','NUMBER','','','Line Acctd Dr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LLINE',101,'Lline','LLINE','','','','MT063505','NUMBER','','','Lline','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','HNUMBER',101,'Hnumber','HNUMBER','','','','MT063505','NUMBER','','','Hnumber','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LSEQUENCE',101,'Lsequence','LSEQUENCE','','','','MT063505','VARCHAR2','','','Lsequence','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','LINE_DESCR',101,'Line Descr','LINE_DESCR','','','','MT063505','VARCHAR2','','','Line Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ENTRY',101,'Entry','ENTRY','','','','MT063505','VARCHAR2','','','Entry','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ACC_DATE',101,'Acc Date','ACC_DATE','','','','MT063505','DATE','','','Acc Date','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','JE_LINE_NUM',101,'Je Line Num','JE_LINE_NUM','','','','MT063505','NUMBER','','','Je Line Num','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','JE_HEADER_ID',101,'Je Header Id','JE_HEADER_ID','','','','MT063505','NUMBER','','','Je Header Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','JE_CATEGORY',101,'Je Category','JE_CATEGORY','','','','MT063505','VARCHAR2','','','Je Category','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','SOURCE',101,'Source','SOURCE','','','','MT063505','VARCHAR2','','','Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#SUBACCOUNT#DESCR',101,'Gcc#50368#Subaccount#Descr','GCC#50368#SUBACCOUNT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Subaccount#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#SUBACCOUNT',101,'Gcc#50368#Subaccount','GCC#50368#SUBACCOUNT','','','','MT063505','VARCHAR2','','','Gcc#50368#Subaccount','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#PRODUCT#DESCR',101,'Gcc#50368#Product#Descr','GCC#50368#PRODUCT#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Product#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#PRODUCT',101,'Gcc#50368#Product','GCC#50368#PRODUCT','','','','MT063505','VARCHAR2','','','Gcc#50368#Product','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50368#FUTURE_USE#DESCR',101,'Gcc#50368#Future Use#Descr','GCC#50368#FUTURE_USE#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50368#Future Use#Descr','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','BATCH',101,'Batch','BATCH','','','','MT063505','NUMBER','','','Batch','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FURTURE_USE',101,'Gcc#50328#Furture Use','GCC#50328#FURTURE_USE','','','','MT063505','VARCHAR2','','','Gcc#50328#Furture Use','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','GCC#50328#FURTURE_USE#DESCR',101,'Gcc#50328#Furture Use#Descr','GCC#50328#FURTURE_USE#DESCR','','','','MT063505','VARCHAR2','','','Gcc#50328#Furture Use#Descr','','','','');
xxeis.eis_rsc_ins.vc( 'EIS_XXHDS_GL_SL_180_V','ASSOCIATE_NUM',101,'Associate Num','ASSOCIATE_NUM','','','','MT063505','VARCHAR2','','','Associate Num','','','','');
--Inserting Object Components for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_JE_LINES',101,'GL_JE_LINES','GJL','GJL','MT063505','MT063505','140887333','Journal Entry Lines','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_JE_HEADERS',101,'GL_JE_HEADERS','GJH','GJH','MT063505','MT063505','140887333','Journal Entry Headers','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_CODE_COMBINATIONS',101,'GL_CODE_COMBINATIONS','GCC','GCC','MT063505','MT063505','140887333','Account Combinations','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_LEDGERS',101,'GL_LEDGERS','GL','GL','MT063505','MT063505','140887333','Ledger Definition','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES',101,'GL_PERIOD_STATUSES','GPS','GPS','MT063505','MT063505','140887333','Calendar Period Statuses','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXHDS_GL_SL_180_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_JE_LINES','GJL',101,'EXGS_V.JE_HEADER_ID','=','GJL.JE_HEADER_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_JE_LINES','GJL',101,'EXGS_V.JE_LINE_NUM','=','GJL.JE_LINE_NUM(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_JE_HEADERS','GJH',101,'EXGS_V.JE_HEADER_ID','=','GJH.JE_HEADER_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_CODE_COMBINATIONS','GCC',101,'EXGS_V.CODE_COMBINATION_ID','=','GCC.CODE_COMBINATION_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_LEDGERS','GL',101,'EXGS_V.NAME','=','GL.NAME(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES','GPS',101,'EXGS_V.GPS_APPLICATION_ID','=','GPS.APPLICATION_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES','GPS',101,'EXGS_V.GPS_SET_OF_BOOKS_ID','=','GPS.SET_OF_BOOKS_ID(+)','','','','Y','MT063505');
xxeis.eis_rsc_ins.vcj( 'EIS_XXHDS_GL_SL_180_V','GL_PERIOD_STATUSES','GPS',101,'EXGS_V.GPS_PERIOD_NAME','=','GPS.PERIOD_NAME(+)','','','','Y','MT063505');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report LOV Data for HDS-Payables Subledger Detail
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - HDS-Payables Subledger Detail
xxeis.eis_rsc_ins.lov( 101,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','null','EIS_GL_PERIOD_LOV','Derives GL Periods based on the corresponding setups in the sets of books','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PRODUCT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT1'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_PRODUCT','XXCUS_GL_PRODUCT','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_LOCATION'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT2'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_LOCATION','XXCUS_GL_LOCATION','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_COSTCENTER'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT3'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_COSTCENTER','XXCUS_GL_COSTCENTER','MM050208',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					apps.fnd_flex_value_sets ffvs , 
					apps.fnd_flex_values ffv, 
					apps.fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_ACCOUNT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
	 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT4'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value','','XXCUS_GL_ACCOUNT','XXCUS_GL_ACCOUNT','MM050208',NULL,'N','','','N','N','','','US');
xxeis.eis_rsc_ins.lov( '','SELECT ffv.flex_value, ffvtl.description, decode(ffv.summary_flag,''Y'', ''Parent'',''N'', ''Child'') Type 
				FROM 
					fnd_flex_value_sets ffvs , 
					fnd_flex_values ffv, 
					fnd_flex_values_tl ffvtl 
				WHERE   upper(ffvs.flex_value_set_name) = upper(''XXCUS_GL_PROJECT'') 
				 and ffv.flex_value_set_id = ffvs.flex_value_set_id 
				 and ffv.FLEX_VALUE_ID = ffvtl.FLEX_VALUE_ID 
				AND ffv.enabled_flag = upper(''Y'') 
				AND ffv.summary_flag in (''Y'',''N'') 
				AND ffvtl.LANGUAGE = USERENV(''LANG'') 
				 AND xxeis.eis_gl_security_pkg.validate_segment_value( ''SEGMENT5'' , ffv.flex_value)=''TRUE'' 
				order by ffv.flex_value	','','XXCUS_GL_PROJECT','XXCUS_GL_PROJECT','MM050208',NULL,'','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
prompt Creating Report Data for HDS-Payables Subledger Detail
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(101);
IF mod_exist = 'Y' THEN 
--Deleting Report data - HDS-Payables Subledger Detail
xxeis.eis_rsc_utility.delete_report_rows( 'HDS-Payables Subledger Detail' );
--Inserting Report - HDS-Payables Subledger Detail
xxeis.eis_rsc_ins.r( 101,'HDS-Payables Subledger Detail','','USD data only','','','','KP012542','EIS_XXHDS_GL_SL_180_V','Y','','','KP012542','','N','HDS Standard Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - HDS-Payables Subledger Detail
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'JE_LINE_NUM','Je Line Num','Je Line Num','','~T~D~0','default','','8','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'ACCOUNTED_CR','Total Accounted Debit','Accounted Cr','','~T~D~2','default','','23','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'ACCOUNTED_DR','Total Accounted Credit','Accounted Dr','','~T~D~2','default','','24','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'SEGMENT1','Product','Key flexfield segment','','','default','','10','N','','','','','','','','KP012542','N','N','','GL_CODE_COMBINATIONS_KFV','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'SEGMENT2','Location','Key flexfield segment','','','default','','11','N','','','','','','','','KP012542','N','N','','GL_CODE_COMBINATIONS_KFV','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'SEGMENT3','Cost Center','Key flexfield segment','','','default','','12','N','','','','','','','','KP012542','N','N','','GL_CODE_COMBINATIONS_KFV','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'SEGMENT4','Account','Key flexfield segment','','','default','','13','N','','','','','','','','KP012542','N','N','','GL_CODE_COMBINATIONS_KFV','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'SEGMENT5','Project Code','Key flexfield segment','','','default','','15','N','','','','','','','','KP012542','N','N','','GL_CODE_COMBINATIONS_KFV','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'ACC_DATE','Acc Date','Acc Date','','','default','','5','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'BATCH_NAME','Batch Name','Batch Name','','','default','','6','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'CURRENCY_CODE','Currency Code','Currency Code','','','default','','2','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'CUSTOMER_OR_VENDOR','Vendor Name','Customer Or Vendor','','','default','','17','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'ENTRY','Description','Entry','','','default','','7','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'GCC#50328#ACCOUNT#DESCR','Account Description','Gcc#50328#Account#Descr','','','default','','14','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'GL_SL_LINK_ID','Subledger Link ID','Gl Sl Link Id','','~T~D~0','default','','18','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'JE_CATEGORY','Je Category','Je Category','','','default','','3','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'LINE_DESCR','Line Descr','Line Descr','','','default','','9','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'PERIOD_NAME','Period Name','Period Name','','','default','','4','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'SOURCE','Source','Source','','','default','','1','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'TRANSACTION_NUM','Invoice Number','Transaction Num','','','default','','16','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'TYPE','Line Type','Type','','','default','','19','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'SLA_DIST_ACCOUNTED_CR','Subledger Line Credit','Sla Dist Accounted Cr','','~T~D~2','default','','21','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'SLA_DIST_ACCOUNTED_DR','Subledger Line Debit','Sla Dist Accounted Dr','','~T~D~2','default','','20','N','','','','','','','','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( 'HDS-Payables Subledger Detail',101,'ACCOUNTED_NET','Subledger_Net','','NUMBER','~T~D~2','default','','22','Y','','','','','','','SLA_DIST_ACCOUNTED_NET*-1','KP012542','N','N','','EIS_XXHDS_GL_SL_180_V','','','SUM','US','');
--Inserting Report Parameters - HDS-Payables Subledger Detail
xxeis.eis_rsc_ins.rp( 'HDS-Payables Subledger Detail',101,'Product','Product','SEGMENT1','IN','XXCUS_GL_PRODUCT','','VARCHAR2','N','Y','2','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Payables Subledger Detail',101,'Location','Location','SEGMENT2','IN','XXCUS_GL_LOCATION','','VARCHAR2','N','Y','3','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Payables Subledger Detail',101,'Cost Center','Cost Center','SEGMENT3','IN','XXCUS_GL_COSTCENTER','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Payables Subledger Detail',101,'Account','Account','SEGMENT4','IN','XXCUS_GL_ACCOUNT','','VARCHAR2','Y','Y','5','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Payables Subledger Detail',101,'Project Code','Project Code','SEGMENT5','IN','XXCUS_GL_PROJECT','','VARCHAR2','N','Y','6','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
xxeis.eis_rsc_ins.rp( 'HDS-Payables Subledger Detail',101,'Period Name','Period Name','PERIOD_NAME','IN','EIS_GL_PERIOD_LOV','','VARCHAR2','Y','Y','1','Y','Y','CONSTANT','KP012542','Y','N','','','','EIS_XXHDS_GL_SL_180_V','','','US','');
--Inserting Dependent Parameters - HDS-Payables Subledger Detail
--Inserting Report Conditions - HDS-Payables Subledger Detail
xxeis.eis_rsc_ins.rcnh( 'HDS-Payables Subledger Detail',101,'EXGS_V.SOURCE IN ''Payables'' ','ADVANCED','','  1#$# ','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SOURCE','','','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','N','','''Payables''','','','1',101,'HDS-Payables Subledger Detail','EXGS_V.SOURCE IN ''Payables'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Payables Subledger Detail',101,'NAME IN ''HD Supply USD'' ','ADVANCED','','1#$#','Y','');
xxeis.eis_rsc_ins.rcnd( '','','NAME','','','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','N','','''HD Supply USD''','','','1',101,'HDS-Payables Subledger Detail','NAME IN ''HD Supply USD'' ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Payables Subledger Detail',101,'PERIOD_NAME IN :Period Name ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','PERIOD_NAME','','Period Name','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Payables Subledger Detail','PERIOD_NAME IN :Period Name ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Payables Subledger Detail',101,'SEGMENT1 IN :Product ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT1','','Product','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Payables Subledger Detail','SEGMENT1 IN :Product ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Payables Subledger Detail',101,'SEGMENT2 IN :Location ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT2','','Location','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Payables Subledger Detail','SEGMENT2 IN :Location ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Payables Subledger Detail',101,'SEGMENT3 IN :Cost Center ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT3','','Cost Center','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Payables Subledger Detail','SEGMENT3 IN :Cost Center ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Payables Subledger Detail',101,'SEGMENT4 IN :Account ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT4','','Account','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Payables Subledger Detail','SEGMENT4 IN :Account ');
xxeis.eis_rsc_ins.rcnh( 'HDS-Payables Subledger Detail',101,'SEGMENT5 IN :Project Code ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SEGMENT5','','Project Code','','','','','EIS_XXHDS_GL_SL_180_V','','','','','','IN','Y','Y','','','','','1',101,'HDS-Payables Subledger Detail','SEGMENT5 IN :Project Code ');
--Inserting Report Sorts - HDS-Payables Subledger Detail
--Inserting Report Triggers - HDS-Payables Subledger Detail
--inserting report templates - HDS-Payables Subledger Detail
--Inserting Report Portals - HDS-Payables Subledger Detail
--inserting report dashboards - HDS-Payables Subledger Detail
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'HDS-Payables Subledger Detail','101','EIS_XXHDS_GL_SL_180_V','EIS_XXHDS_GL_SL_180_V','N','');
xxeis.eis_rsc_ins.rviews( 'HDS-Payables Subledger Detail','101','GL_CODE_COMBINATIONS_KFV','GL_CODE_COMBINATIONS_KFV','N','');
--inserting report security - HDS-Payables Subledger Detail
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','HDS_GNRL_LDGR_SPR_USR',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','HDS_GNRL_LDGR_CAD',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','GNRL_LDGR_LTMR_FSS',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','GNRL_LDGR_FSS',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXWC_GL_SETUP',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','HDS_CAD_MNTH_END_PROCS',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_MANAGER_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_MANAGER_GLOBAL',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_MANAGER',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_INQUIRY_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','GNRL_LDGR_LTMR_NQR',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','HDS GL INQUIRY',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_INQUIRY',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_ACCOUNTANT_USD_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_ACCOUNTANT_USD',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','GNRL_LDGR_LTMR_ACCNTNT',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_ACCOUNTANT_GLOBAL',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_ACCOUNTANT_CAD_PVF',101,'KP012542','','','');
xxeis.eis_rsc_ins.rsec( 'HDS-Payables Subledger Detail','101','','XXCUS_GL_ACCOUNTANT_CAD',101,'KP012542','','','');
--Inserting Report Pivots - HDS-Payables Subledger Detail
--Inserting Report   Version details- HDS-Payables Subledger Detail
xxeis.eis_rsc_ins.rv( 'HDS-Payables Subledger Detail','','HDS-Payables Subledger Detail','MT063505');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 101');
END IF;
END;
/
