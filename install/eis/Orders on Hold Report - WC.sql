--Report Name            : Orders on Hold Report - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Orders on Hold Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_WC_OM_ORD_HLD_V
xxeis.eis_rs_ins.v( 'EIS_WC_OM_ORD_HLD_V',660,'','','','','MR020532','XXEIS','Eis Wc Om Ord Hld V','EWOOHV','','');
--Delete View Columns for EIS_WC_OM_ORD_HLD_V
xxeis.eis_rs_utility.delete_view_rows('EIS_WC_OM_ORD_HLD_V',660,FALSE);
--Inserting View Columns for EIS_WC_OM_ORD_HLD_V
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ADJUST_GM_PER',660,'Adjust Gm Per','ADJUST_GM_PER','','','','MR020532','NUMBER','','','Adjust Gm Per','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','SPECIAL_VQN_COST',660,'Special Vqn Cost','SPECIAL_VQN_COST','','','','MR020532','NUMBER','','','Special Vqn Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','GM$',660,'Gm$','GM$','','','','MR020532','NUMBER','','','Gm$','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','EXTENDED_COST',660,'Extended Cost','EXTENDED_COST','','','','MR020532','NUMBER','','','Extended Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','UNIT_COST',660,'Unit Cost','UNIT_COST','','','','MR020532','NUMBER','','','Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','MARGIN_PER',660,'Margin Per','MARGIN_PER','','','','MR020532','NUMBER','','','Margin Per','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','EXTENDED_PRICE',660,'Extended Price','EXTENDED_PRICE','','','','MR020532','NUMBER','','','Extended Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','UNIT_SELLING_PRICE',660,'Unit Selling Price','UNIT_SELLING_PRICE','','','','MR020532','NUMBER','','','Unit Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','QUANTITY',660,'Quantity','QUANTITY','','','','MR020532','NUMBER','','','Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','PART_DESCRIPTION',660,'Part Description','PART_DESCRIPTION','','','','MR020532','VARCHAR2','','','Part Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','MR020532','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','HOLD_DATE',660,'Hold Date','HOLD_DATE','','','','MR020532','DATE','','','Hold Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ORDER_LINE_HOLD_NAME',660,'Order Line Hold Name','ORDER_LINE_HOLD_NAME','','','','MR020532','VARCHAR2','','','Order Line Hold Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ORDER_LINE_HOLD',660,'Order Line Hold','ORDER_LINE_HOLD','','','','MR020532','VARCHAR2','','','Order Line Hold','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ORDER_HEADER_HOLD_NAME',660,'Order Header Hold Name','ORDER_HEADER_HOLD_NAME','','','','MR020532','VARCHAR2','','','Order Header Hold Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ORDER_HEADER_HOLD',660,'Order Header Hold','ORDER_HEADER_HOLD','','','','MR020532','VARCHAR2','','','Order Header Hold','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','LINE_TYPE',660,'Line Type','LINE_TYPE','','','','MR020532','VARCHAR2','','','Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ORDER_LINE_NUMBER',660,'Order Line Number','ORDER_LINE_NUMBER','','','','MR020532','VARCHAR2','','','Order Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ORDER_LINE_STATUS',660,'Order Line Status','ORDER_LINE_STATUS','','','','MR020532','VARCHAR2','','','Order Line Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ORDERED_DATE',660,'Ordered Date','ORDERED_DATE','','','','MR020532','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ORDER_TYPE',660,'Order Type','ORDER_TYPE','','','','MR020532','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','ORDER_HEADER_STATUS',660,'Order Header Status','ORDER_HEADER_STATUS','','','','MR020532','VARCHAR2','','','Order Header Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','CREATED_BY',660,'Created By','CREATED_BY','','','','MR020532','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','SALES_ORDER_NUMBER',660,'Sales Order Number','SALES_ORDER_NUMBER','','','','MR020532','NUMBER','','','Sales Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','LOC',660,'Loc','LOC','','','','MR020532','VARCHAR2','','','Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','CUSTOMER_NAME',660,'Customer Name','CUSTOMER_NAME','','','','MR020532','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','CUSTOMER_NUMBER',660,'Customer Number','CUSTOMER_NUMBER','','','','MR020532','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_WC_OM_ORD_HLD_V','VQN_MODIFIER_NAME',660,'Vqn Modifier Name','VQN_MODIFIER_NAME','','','','MR020532','VARCHAR2','','','Vqn Modifier Name','','','');
--Inserting View Components for EIS_WC_OM_ORD_HLD_V
--Inserting View Component Joins for EIS_WC_OM_ORD_HLD_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Orders on Hold Report - WC
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Orders on Hold Report - WC
xxeis.eis_rs_ins.lov( 660,'select distinct ott.name order_type,ott.description description,ott.transaction_type_id order_type_id from oe_transaction_types_tl ott','','OM ORDER TYPE','This gives the Order Type','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( '','SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME
FROM ORG_ORGANIZATION_DEFINITIONS OOD
WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' FROM DUAL','','XXWC All ORG LIST','XXWC All ORG LIST','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Orders on Hold Report - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Orders on Hold Report - WC
xxeis.eis_rs_utility.delete_report_rows( 'Orders on Hold Report - WC' );
--Inserting Report - Orders on Hold Report - WC
xxeis.eis_rs_ins.r( 660,'Orders on Hold Report - WC','','Order Holds and Not Invoiced Report','','','','MR020532','EIS_WC_OM_ORD_HLD_V','Y','','','MR020532','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Orders on Hold Report - WC
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ADJUST_GM_PER','Adjust GM%','Adjust Gm Per','','~T~D~2','default','','27','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'CREATED_BY','Created By','Created By','','','default','','5','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','2','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','1','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'EXTENDED_COST','Extended Cost','Extended Cost','','~T~D~2','default','','24','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'EXTENDED_PRICE','Extended Price','Extended Price','','~T~D~2','default','','21','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'GM$','GM$','Gm$','','~T~D~2','default','','25','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'HOLD_DATE','Hold Date','Hold Date','','','default','','16','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'LINE_TYPE','Line Type','Line Type','','','default','','11','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'LOC','Loc','Loc','','','default','','3','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'MARGIN_PER','Margin%','Margin Per','','~T~D~2','default','','22','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','8','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ORDER_HEADER_HOLD','Order Header Hold','Order Header Hold','','','default','','12','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ORDER_HEADER_HOLD_NAME','Order Header Hold Name','Order Header Hold Name','','','default','','13','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ORDER_HEADER_STATUS','Order Header Status','Order Header Status','','','default','','6','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ORDER_LINE_HOLD','Order Line Hold','Order Line Hold','','','default','','14','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ORDER_LINE_HOLD_NAME','Order Line Hold Name','Order Line Hold Name','','','default','','15','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ORDER_LINE_NUMBER','Order Line Number','Order Line Number','','','default','','10','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ORDER_LINE_STATUS','Order Line Status','Order Line Status','','','default','','9','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'ORDER_TYPE','Order Type','Order Type','','','default','','7','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'PART_DESCRIPTION','Part Description','Part Description','','','default','','18','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'PART_NUMBER','Part Number','Part Number','','','default','','17','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'QUANTITY','Quantity','Quantity','','~T~D~2','default','','19','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'SALES_ORDER_NUMBER','Sales Order Number','Sales Order Number','','~~~','default','','4','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'SPECIAL_VQN_COST','Special VQN Cost','Special Vqn Cost','','~T~D~2','default','','26','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'UNIT_COST','Unit Cost','Unit Cost','','~T~D~2','default','','23','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','~T~D~2','default','','20','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
xxeis.eis_rs_ins.rc( 'Orders on Hold Report - WC',660,'VQN_MODIFIER_NAME','Modifier Name','Vqn Modifier Name','','','default','','28','N','','','','','','','','MR020532','N','N','','EIS_WC_OM_ORD_HLD_V','','');
--Inserting Report Parameters - Orders on Hold Report - WC
xxeis.eis_rs_ins.rp( 'Orders on Hold Report - WC',660,'Warehouse','Warehouse','LOC','IN','XXWC All ORG LIST','','VARCHAR2','Y','Y','1','','N','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report - WC',660,'Ordered Date From','Ordered Date From','ORDERED_DATE','>=','','','DATE','N','Y','2','','Y','CONSTANT','MR020532','Y','N','','Start Date','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report - WC',660,'Ordered Date To','Ordered Date To','ORDERED_DATE','<=','','','DATE','N','Y','3','','Y','CONSTANT','MR020532','Y','N','','End Date','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report - WC',660,'Order Type','Order Type','ORDER_TYPE','IN','OM ORDER TYPE','','VARCHAR2','N','Y','4','','Y','CONSTANT','MR020532','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Orders on Hold Report - WC',660,'Hold Type','Hold Type','ORDER_LINE_HOLD_NAME','IN','','','VARCHAR2','N','Y','5','','Y','CONSTANT','MR020532','Y','N','','','');
--Inserting Report Conditions - Orders on Hold Report - WC
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report - WC',660,'ORDERED_DATE','>=',':Ordered Date From','','','Y','2','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report - WC',660,'ORDERED_DATE','<=',':Ordered Date To','','','Y','3','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report - WC',660,'ORDER_TYPE','IN',':Order Type','','','Y','4','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report - WC',660,'ORDER_LINE_HOLD_NAME','IN',':Hold Type','','','Y','5','Y','MR020532');
xxeis.eis_rs_ins.rcn( 'Orders on Hold Report - WC',660,'','','','','AND ( ''All'' IN (:Warehouse) OR (LOC IN (:Warehouse)))','Y','0','','MR020532');
--Inserting Report Sorts - Orders on Hold Report - WC
xxeis.eis_rs_ins.rs( 'Orders on Hold Report - WC',660,'SALES_ORDER_NUMBER','ASC','MR020532','1','');
--Inserting Report Triggers - Orders on Hold Report - WC
--Inserting Report Templates - Orders on Hold Report - WC
--Inserting Report Portals - Orders on Hold Report - WC
--Inserting Report Dashboards - Orders on Hold Report - WC
--Inserting Report Security - Orders on Hold Report - WC
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','50869',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','50871',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','50870',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','50901',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','50860',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','50886',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','50859',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','50858',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','50857',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','20005','','50900',660,'MR020532','','');
xxeis.eis_rs_ins.rsec( 'Orders on Hold Report - WC','660','','51044',660,'MR020532','','');
--Inserting Report Pivots - Orders on Hold Report - WC
xxeis.eis_rs_ins.rpivot( 'Orders on Hold Report - WC',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report - WC',660,'Pivot','CUSTOMER_NAME','PAGE_FIELD','','','1','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report - WC',660,'Pivot','CUSTOMER_NUMBER','PAGE_FIELD','','','2','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report - WC',660,'Pivot','PART_NUMBER','PAGE_FIELD','','','3','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report - WC',660,'Pivot','PART_DESCRIPTION','PAGE_FIELD','','','4','','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report - WC',660,'Pivot','EXTENDED_PRICE','DATA_FIELD','SUM','','1','','xlNormal');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report - WC',660,'Pivot','SALES_ORDER_NUMBER','ROW_FIELD','','','1','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report - WC',660,'Pivot','ORDER_HEADER_HOLD_NAME','ROW_FIELD','','','2','1','');
xxeis.eis_rs_ins.rpivot_dtls( 'Orders on Hold Report - WC',660,'Pivot','ORDER_LINE_HOLD_NAME','ROW_FIELD','','','3','1','');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
