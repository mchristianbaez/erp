create or replace
package body XXWC_VENDOR_QUOTE_TMP_PKG  as
    procedure GET_HEADER_ID (P_PROCESS_ID in number,P_INV_START_DATE in date,P_INV_END_DATE in date) is
  --  L_STMT long;
  
  cursor xxcur_header_id is 
  SELECT DISTINCT PROCESS_ID,HEADER_ID
  FROM XXEIS.XXWC_VENDOR_QUOTE_TEMP
  WHERE PROCESS_ID = P_PROCESS_ID;
  
  CURSOR XX_CURR_DET(P_HEADER_ID NUMBER) 
IS 
SELECT NAME,
    salesperson_name,
    salespeson_number,
    master_account_name,
    master_account_number,
    job_name,
    job_number,
    vendor_number,
    vendor_name,
    oracle_quote_number,
    invoice_number,
    invoice_date,
    part_number,
    uom,
    description,
    bpa,
    po_cost po_cost,
    average_cost average_cost,
    special_cost special_cost,
    (po_cost - special_cost) unit_claim_value,
    (po_cost - special_cost) * qty rebate,
    gl_coding,
    gl_string,
    loc,
    created_by,
    customer_trx_id,
    order_number,
    line_number,
    creation_date,
    location,
    QTY
  FROM
    (SELECT 
      /*+ index (QPL XXWC_QP_LIST_LINES_N2) */
      qll.start_date_active ,
      qlh.start_date_active ,
      qll.end_date_active,
      qlh.end_date_active,
      rct.trx_date,
      qlh. NAME,
      jrs.NAME salesperson_name,
      jrs.salesrep_number salespeson_number,
      hcs.account_name master_account_name,
      hcs.account_number master_account_number,
      hcsu.LOCATION job_name,
      hps.party_site_number job_number,
      pov.segment1 vendor_number,
      pov.vendor_name vendor_name,
      xxcpl.vendor_quote_number oracle_quote_number,
      rct.trx_number invoice_number,
      TRUNC(rct.trx_date) invoice_date,
      msi.concatenated_segments part_number,
      msi.primary_unit_of_measure uom,
      msi.description description,
      CASE
        WHEN upper (pov.vendor_name) LIKE 'SIMPSON%'
        THEN xxeis.eis_rs_xxwc_com_util_pkg.get_best_buy ( msi.inventory_item_id, msi.organization_id, pov.vendor_id)
        ELSE NULL
      END bpa,
      NVL ( xxeis.eis_rs_xxwc_com_util_pkg.get_order_line_po_cost ( msi.inventory_item_id, msi.organization_id, ol.creation_date), msi.list_price_per_unit) po_cost,
      xxcpl.average_cost average_cost,
      xxcpl.special_cost special_cost,
      '400-900'
      || '-'
      || gcc.segment2 gl_coding,
      gcc.concatenated_segments gl_string,
      gcc.segment2 loc,
      ppf1.full_name created_by,
      rct.customer_trx_id customer_trx_id,
      oh.order_number order_number,
      ol.line_number line_number,
      oh.creation_date creation_date,
      mp.organization_code location,
      NVL(rctl.quantity_invoiced, rctl.quantity_credited) QTY
    FROM oe_order_headers oh,
      oe_order_lines ol,
      qp_qualifiers qq,
      qp_list_headers qlh,
      apps.xxwc_om_contract_pricing_hdr xxcph,
      apps.xxwc_om_contract_pricing_lines xxcpl,
      qp_list_lines qll,
      mtl_system_items_b_kfv msi,
      fnd_user fu,
      per_all_people_f ppf1,
      po_vendors pov,
      ra_salesreps jrs,
      ra_customer_trx rct,
      ra_customer_trx_lines rctl,
      ra_cust_trx_line_gl_dist rctlgl,
      gl_code_combinations_kfv gcc,
      hz_cust_accounts hcs,
      hz_parties hp,
      hz_cust_site_uses hcsu,
      hz_cust_acct_sites hcas,
      hz_party_sites hps,
      MTL_PARAMETERS MP
      --      XXEIS.XXWC_VENDOR_QUOTE_TEMP TEMP
    WHERE 1                         =1
    AND oh.header_id                = ol.header_id
    AND ol.flow_status_code         = 'CLOSED'
    AND qq.qualifier_context        = 'CUSTOMER'
    AND qq.comparison_operator_code = '='
    AND qq.active_flag              = 'Y'
    AND ( (qlh.attribute10          = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.ship_to_org_id)) )
    OR (qlh.attribute10             = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) )
    OR (qlh.attribute10             = ('CSP-NATIONAL')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) ) )
    AND qlh.list_header_id          = qq.list_header_id
    AND qlh.active_flag             = 'Y'
    AND xxcph.agreement_id          = TO_CHAR(qlh.attribute14)
      --AND xxcph.revision_number           = TO_CHAR(qlh.attribute15)
    AND xxcpl.agreement_id              = xxcph.agreement_id
    AND xxcpl.agreement_type            = 'VQN'
    AND qlh.list_header_id              = qll.list_header_id
    AND xxcpl.product_value             = msi.segment1
    AND xxcpl.agreement_line_id         = TO_CHAR(qll.attribute2)
    AND msi.inventory_item_id           = ol.inventory_item_id
    AND msi.organization_id             = ol.ship_from_org_id
    AND fu.user_id                      = oh.created_by
    AND fu.employee_id(+)               = ppf1.person_id
    AND pov.vendor_id                   = xxcpl.vendor_id
    AND oh.salesrep_id                  = jrs.salesrep_id(+)
    AND oh.org_id                       = jrs.org_id(+)
    AND rct.interface_header_context    = 'ORDER ENTRY'
    AND rct.interface_header_attribute1 = TO_CHAR(oh.order_number)
    AND rctl.interface_line_context     = 'ORDER ENTRY'
    AND rctl.interface_line_attribute6  = TO_CHAR (ol.line_id)
      --AND rctl.sales_order                = oh.order_number
    AND rctl.customer_trx_id        = rct.customer_trx_id
    AND rctl.line_type              = 'LINE'
    AND rctlgl.customer_trx_id      = rctl.customer_trx_id
    AND rctlgl.customer_trx_line_id = rctl.customer_trx_line_id
    AND rctlgl.account_class        = 'REV'
    AND gcc.code_combination_id     = rctlgl.code_combination_id
    AND hcs.cust_account_id         = rct.ship_to_customer_id
    AND hcs.party_id                = hp.party_id
    AND hcsu.site_use_id            = rct.ship_to_site_use_id
    AND hcas.cust_acct_site_id      = hcsu.cust_acct_site_id
    AND hcas.party_site_id          = hps.party_site_id
    AND MP.ORGANIZATION_ID          = MSI.ORGANIZATION_ID
      --AND QLL.START_DATE_ACTIVE          <= NVL(QLL.END_DATE_ACTIVE,RCT.TRX_DATE)
    AND (TRUNC(NVL(QLL.START_DATE_ACTIVE,RCT.TRX_DATE))<=TRUNC(RCT.TRX_DATE)
    AND TRUNC(NVL(QLL.END_DATE_ACTIVE,RCT.TRX_DATE))   >= TRUNC(RCT.TRX_DATE))
      /*AND (TRUNC(QLL.END_DATE_ACTIVE)  >=TRUNC(RCT.TRX_DATE)
      OR QLL.END_DATE_ACTIVE           IS NULL)*/
    AND OH.HEADER_ID = P_HEADER_ID
    );
l_count number;
begin    
    begin 

    INSERT INTO XXEIS.XXWC_VENDOR_QUOTE_TEMP
    (PROCESS_ID,HEADER_ID)
SELECT P_PROCESS_ID,  OH.HEADER_ID 
       FROM oe_order_headers oh,
      OE_ORDER_LINES OL,
      RA_CUSTOMER_TRX RCT,
      RA_CUSTOMER_TRX_LINES RCTL,
      mtl_system_items_b_kfv msi,
      RA_SALESREPS JRS,
      FND_USER FU,
      PER_ALL_PEOPLE_F PPF1,
      ra_cust_trx_line_gl_dist rctlgl,
      GL_CODE_COMBINATIONS_KFV GCC,
      hz_cust_accounts hcs,
      hz_parties hp,
      hz_cust_site_uses hcsu,
      HZ_CUST_ACCT_SITES HCAS,
      HZ_PARTY_SITES HPS,
      MTL_PARAMETERS MP
     WHERE 1                             =1
    AND oh.header_id                    = ol.header_id
    AND ol.flow_status_code             = 'CLOSED'
    AND rct.interface_header_context    = 'ORDER ENTRY'
    AND rct.interface_header_attribute1 = TO_CHAR(oh.order_number)
    AND rctl.interface_line_context     = 'ORDER ENTRY'
    AND rctl.interface_line_attribute6  = TO_CHAR (ol.line_id)
    AND rctl.sales_order                = oh.order_number
    AND rctl.customer_trx_id            = rct.customer_trx_id
    AND RCTL.LINE_TYPE                  = 'LINE'
    AND MSI.INVENTORY_ITEM_ID       = OL.INVENTORY_ITEM_ID
    AND msi.organization_id             = ol.ship_from_org_id
    AND OH.SALESREP_ID                  = JRS.SALESREP_ID(+)
    AND OH.ORG_ID                       = JRS.ORG_ID(+)
    AND fu.user_id                      = oh.created_by
    AND FU.EMPLOYEE_ID(+)               = PPF1.PERSON_ID
    AND rctlgl.customer_trx_id      = rctl.customer_trx_id
    AND rctlgl.customer_trx_line_id = rctl.customer_trx_line_id
    AND rctlgl.account_class        = 'REV'
    AND GCC.CODE_COMBINATION_ID     = RCTLGL.CODE_COMBINATION_ID
    AND hcs.cust_account_id         = rct.ship_to_customer_id
    AND hcs.party_id                = hp.party_id
    AND hcsu.site_use_id            = rct.ship_to_site_use_id
    AND HCAS.CUST_ACCT_SITE_ID      = HCSU.CUST_ACCT_SITE_ID
    AND HCAS.PARTY_SITE_ID          = HPS.PARTY_SITE_ID
    and MP.ORGANIZATION_ID          = MSI.ORGANIZATION_ID
   -- and TRUNC(RCT.TRX_DATE) between '26-Sep-2013' and '04-Oct-2013';
    and (RCT.TRX_DATE) between P_INV_START_DATE and P_INV_END_DATE;
 
 commit; 
--            FND_FILE.PUT_LINE(FND_FILE.log,'END time'||to_char(sysdate,'DD-MON-YYYY HH24-MI-SS'));
  EXCEPTION
  when OTHERS then
    FND_FILE.PUT_LINE(FND_FILE.LOG,SQLERRM);
    end;
	
 BEGIN
for i in xxcur_header_id
LOOP
 l_count:=0;

 SELECT count(oh.header_id)
    into l_count
    FROM    QP_QUALIFIERS QQ,
      QP_LIST_HEADERS QLH,
      QP_LIST_LINES QLL,
      APPS.XXWC_OM_CONTRACT_PRICING_LINES XXCPL,
      APPS.XXWC_OM_CONTRACT_PRICING_HDR XXCPH,
      mtl_system_items_b_kfv msi,
      OE_ORDER_HEADERS OH,
      OE_ORDER_LINES OL,
      PO_VENDORS POV,
    --  FND_USER FU,
     -- per_all_people_f ppf1,        
       MTL_PARAMETERS MP
    WHERE 1=1
        AND qq.qualifier_context        = 'CUSTOMER'
    AND qq.comparison_operator_code = '='
    AND qq.active_flag              = 'Y'
    AND ( (qlh.attribute10          = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE11'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.ship_to_org_id)) )
    OR (qlh.attribute10             = ('Contract Pricing')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) )
    OR (qlh.attribute10             = ('CSP-NATIONAL')
    AND qq.qualifier_attribute      = 'QUALIFIER_ATTRIBUTE32'
    AND (qq.qualifier_attr_value    = TO_CHAR(oh.sold_to_org_id)) ) )
    AND QLH.LIST_HEADER_ID          = QQ.LIST_HEADER_ID
    AND QLH.ACTIVE_FLAG             = 'Y'
    AND XXCPH.AGREEMENT_ID          = TO_CHAR(QLH.ATTRIBUTE14)
    AND xxcph.revision_number           = TO_CHAR(qlh.attribute15)
    AND xxcpl.agreement_id              = xxcph.agreement_id
    AND XXCPL.AGREEMENT_TYPE            = 'VQN'
    AND QLH.LIST_HEADER_ID              = QLL.LIST_HEADER_ID
    AND xxcpl.product_value             = ol.ordered_item
    AND XXCPL.AGREEMENT_LINE_ID         = TO_CHAR(QLL.ATTRIBUTE2)
    AND MSI.INVENTORY_ITEM_ID           = OL.INVENTORY_ITEM_ID
    AND MSI.ORGANIZATION_ID             = OL.SHIP_FROM_ORG_ID
    AND OH.HEADER_ID                = OL.HEADER_ID
    AND POV.VENDOR_ID                   = XXCPL.VENDOR_ID
     AND MP.ORGANIZATION_ID          = MSI.ORGANIZATION_ID
--    AND FU.USER_ID                      = OH.CREATED_BY
--    AND fu.employee_id(+)               = ppf1.person_id
    AND oh.HEADER_id =i.header_id;

	
  if  l_count<>0 
  then
 FOR XX_REC_DET IN XX_curr_DET(i.header_id)
 LOOP
 begin
 insert into xxeis.EIS_XXWC_OM_VENDOR_QUOTE_V 
	  values(
	  xx_rec_det.NAME,
    xx_rec_det.salesperson_name,
    xx_rec_det.salespeson_number,
    xx_rec_det.master_account_name,
    xx_rec_det.master_account_number,
    xx_rec_det.job_name,
    xx_rec_det.job_number,
    xx_rec_det.vendor_number,
    xx_rec_det.vendor_name,
    xx_rec_det.oracle_quote_number,
    xx_rec_det.invoice_number,
    xx_rec_det.invoice_date,
    xx_rec_det.part_number,
    xx_rec_det.uom,
    xx_rec_det.description,
    xx_rec_det.bpa,
    xx_rec_det.po_cost,
    xx_rec_det.average_cost ,
    xx_rec_det.special_cost ,
    xx_rec_det.unit_claim_value,
    xx_rec_det. rebate,
    xx_rec_det.gl_coding,
    xx_rec_det.gl_string,
    xx_rec_det.loc,
    xx_rec_det.created_by,
    xx_rec_det.customer_trx_id,
    xx_rec_det.order_number,
    xx_rec_det.line_number,
    xx_rec_det.creation_date,
    xx_rec_det.location,
    XX_REC_DET.QTY,
    P_PROCESS_ID,
    xxeis.eis_rs_common_outputs_s.nextval
	);
COMMIT;
EXCEPTION
WHEN OTHERS THEN
FND_FILE.PUT_LINE(FND_FILE.LOG,SQLERRM);
end;
END LOOP;
end if;
END LOOP;
EXCEPTION WHEN OTHERS THEN
FND_FILE.PUT_LINE(FND_FILE.LOG,SQLERRM);
END; 
end;
end XXWC_VENDOR_QUOTE_TMP_PKG;
/
GRANT EXECUTE ON XXWC_VENDOR_QUOTE_TMP_PKG TO apps
/
GRANT DEBUG ON XXWC_VENDOR_QUOTE_TMP_PKG TO apps
/