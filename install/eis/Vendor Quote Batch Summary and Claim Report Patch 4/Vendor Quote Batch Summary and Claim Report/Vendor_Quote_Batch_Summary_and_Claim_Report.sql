--Report Name            : Vendor Quote Batch Summary and Claim Report
--Import Instructions : 1. If RTF template needs to be uploaded to report automatically then move the RTF template to physical path of APPS_DATA_FILE_DIR oracle directory. Check dba_directories view to get the physical path of oracle directory. 
--                         (If you want to use any other oracle directory other than APPS_DATA_FILE_DIR then replace APPS_DATA_FILE_DIR with your oracle directory name below.)
--                      2. Make sure XXEIS schema has read/write permissions to above used oracle directory and give full permissions to the directory at OS level.
--                      3. Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
exec xxeis.eis_rs_ins.g_rtf_dir := 'APPS_DATA_FILE_DIR' ;
prompt Creating View Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_OM_VENDOR_QUOTE_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_OM_VENDOR_QUOTE_V',660,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Om Vendor Batch V','EXOVBV','','');
--Delete View Columns for EIS_XXWC_OM_VENDOR_QUOTE_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_OM_VENDOR_QUOTE_V',660,FALSE);
--Inserting View Columns for EIS_XXWC_OM_VENDOR_QUOTE_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','CREATED_BY',660,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LOC',660,'Loc','LOC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Loc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','GL_STRING',660,'Gl String','GL_STRING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl String','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','GL_CODING',660,'Gl Coding','GL_CODING','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Gl Coding','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','REBATE',660,'Rebate','REBATE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Rebate','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','UNIT_CLAIM_VALUE',660,'Unit Claim Value','UNIT_CLAIM_VALUE','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Unit Claim Value','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PO_COST',660,'Po Cost','PO_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Po Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','DESCRIPTION',660,'Description','DESCRIPTION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','UOM',660,'Uom','UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PART_NUMBER',660,'Part Number','PART_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Part Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','INVOICE_DATE',660,'Invoice Date','INVOICE_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','INVOICE_NUMBER',660,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','VENDOR_NAME',660,'Vendor Name','VENDOR_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','VENDOR_NUMBER',660,'Vendor Number','VENDOR_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Vendor Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','JOB_NUMBER',660,'Job Number','JOB_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','JOB_NAME',660,'Job Name','JOB_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Job Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','MASTER_ACCOUNT_NUMBER',660,'Master Account Number','MASTER_ACCOUNT_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','MASTER_ACCOUNT_NAME',660,'Master Account Name','MASTER_ACCOUNT_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Master Account Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SALESPESON_NUMBER',660,'Salespeson Number','SALESPESON_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salespeson Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SALESPERSON_NAME',660,'Salesperson Name','SALESPERSON_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Salesperson Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','SPECIAL_COST',660,'Special Cost','SPECIAL_COST','','~~2','','XXEIS_RS_ADMIN','NUMBER','','','Special Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LOCATION',660,'Location','LOCATION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','QTY',660,'Qty','QTY','','','','XXEIS_RS_ADMIN','NUMBER','','','Qty','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','ORACLE_QUOTE_NUMBER',660,'Oracle Quote Number','ORACLE_QUOTE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Oracle Quote Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','BPA',660,' Bpa','BPA','','','','XXEIS_RS_ADMIN','NUMBER','','',' Bpa','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','ORDER_NUMBER',660,'Order Number','ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','AVERAGE_COST',660,'Average Cost','AVERAGE_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','CREATION_DATE',660,'Creation Date','CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','NAME',660,'Name','NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','CUSTOMER_TRX_ID',660,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','LINE_NUMBER',660,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_OM_VENDOR_QUOTE_V','PROCESS_ID',660,'Process Id','PROCESS_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Process Id','','','');
--Inserting View Components for EIS_XXWC_OM_VENDOR_QUOTE_V
--Inserting View Component Joins for EIS_XXWC_OM_VENDOR_QUOTE_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.lov( 660,'select nvl( party.party_name,CUST_ACCT.account_name) customer_name,CUST_ACCT.ACCOUNT_NUMBER
from  HZ_PARTIES PARTY, HZ_CUST_ACCOUNTS CUST_ACCT where CUST_ACCT.PARTY_ID = PARTY.PARTY_ID AND  PARTY.status=''A''
','','OM CUSTOMER NAME','This gives the Customer Name','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'SELECT DISTINCT VENDOR_NAME 
    FROM PO_VENDORS POV','','OM Vendor Name LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
xxeis.eis_rs_ins.lov( 660,'select distinct segment1 Vendor_Number from po_vendors','','OM Vendor Number LOV','','XXEIS_RS_ADMIN',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Vendor Quote Batch Summary and Claim Report
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_utility.delete_report_rows( 'Vendor Quote Batch Summary and Claim Report' );
--Inserting Report - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.r( 660,'Vendor Quote Batch Summary and Claim Report','','Provides order, credit customer and vendor details for all closed orders associated with a Vendor Quote.','','','','XXEIS_RS_ADMIN','EIS_XXWC_OM_VENDOR_QUOTE_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'CREATED_BY','Created By','Created By','','','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'DESCRIPTION','Description','Description','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_DATE','Invoice Date','Invoice Date','','','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'JOB_NAME','Job Name','Job Name','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'JOB_NUMBER','Site Number','Job Number','','','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LOC','Loc','Loc','','','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NAME','Master Account Name','Master Account Name','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NUMBER','Master Account Number','Master Account Number','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'PART_NUMBER','Part Number','Part Number','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'REBATE','Rebate','Rebate','','~T~D~5','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SALESPERSON_NAME','Salesperson Name','Salesperson Name','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SALESPESON_NUMBER','Salespeson Number','Salespeson Number','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'UOM','Uom','Uom','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NAME','Vendor Name','Vendor Name','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NUMBER','Vendor Number','Vendor Number','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'GL_CODING','GL Coding','Gl Coding','','','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'PO_COST','Po Cost','Po Cost','','~T~D~5','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'UNIT_CLAIM_VALUE','Unit Claim Value','Unit Claim Value','','~T~D~5','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'GL_STRING','GL String','Gl String','','','default','','26','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'LOCATION','Location','Location','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'QTY','Qty','Qty','','~~~','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'ORACLE_QUOTE_NUMBER','Vendor Quote Number','Oracle Quote Number','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'SPECIAL_COST','Special Cost','Special Cost','','~T~D~5','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'BPA','Best Buy',' Bpa','','~T~D~5','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'AVERAGE_COST','Average Cost','Average Cost','','','','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
xxeis.eis_rs_ins.rc( 'Vendor Quote Batch Summary and Claim Report',660,'ORDER_NUMBER','Order Number','Order Number','','','','','27','','Y','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_OM_VENDOR_QUOTE_V','','');
--Inserting Report Parameters - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Date Range From','Date Range From','INVOICE_DATE','>=','','','DATE','Y','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Vendor Name','Vendor Name','VENDOR_NAME','IN','OM Vendor Name LOV','','VARCHAR2','N','Y','3','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Vendor Number','Vendor Number','VENDOR_NUMBER','IN','OM Vendor Number LOV','','VARCHAR2','N','Y','4','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Date Range To','Date Range To','INVOICE_DATE','<=','','','DATE','Y','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Vendor Quote Batch Summary and Claim Report',660,'Bill To Customer','Bill To Customer','MASTER_ACCOUNT_NAME','IN','OM CUSTOMER NAME','','VARCHAR2','N','Y','5','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NAME','IN',':Vendor Name','','','Y','3','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'VENDOR_NUMBER','IN',':Vendor Number','','','Y','4','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'MASTER_ACCOUNT_NAME','IN',':Bill To Customer','','','Y','6','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'PROCESS_ID','IN',':SYSTEM.PROCESS_ID','','','Y','5','N','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_DATE','>=',':Date Range From','','','Y','1','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Vendor Quote Batch Summary and Claim Report',660,'INVOICE_DATE','<=',':Date Range To','','','Y','2','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Vendor Quote Batch Summary and Claim Report
--Inserting Report Triggers - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rt( 'Vendor Quote Batch Summary and Claim Report',660,'begin
XXEIS.XXWC_VENDOR_QUOTE_TMP_PKG.GET_HEADER_ID(P_PROCESS_ID=>:SYSTEM.PROCESS_ID,
P_INV_START_DATE=>:Date Range From,
P_INV_END_DATE=>:Date Range To);
END;
','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.R_Tem( 'Vendor Quote Batch Summary and Claim Report','Vendor Quote Batch Summary and Claim Report','Seeded Template for Vendor Quote Batch Summary and Claim Report','','','','','','','','','','','','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.R_Tem( 'Vendor Quote Batch Summary and Claim Report','Vendor Quote Report','Seeded template for Vendor Quote Report','','','','','','','','','','','Vendor Quote Report.rtf','XXEIS_RS_ADMIN');
--Inserting Report Portals - Vendor Quote Batch Summary and Claim Report
--Inserting Report Dashboards - Vendor Quote Batch Summary and Claim Report
--Inserting Report Security - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50856',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50857',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50858',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50859',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50860',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50861',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','20005','','50880',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','21623',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50886',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50901',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50870',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50871',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','50869',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','20005','','50900',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','701','','50546',660,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Vendor Quote Batch Summary and Claim Report','660','','51044',660,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Vendor Quote Batch Summary and Claim Report
xxeis.eis_rs_ins.rpivot( 'Vendor Quote Batch Summary and Claim Report',660,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
--Inserting Report Summary Calculation Columns For Pivot- Pivot
END;
/
set scan on define on
