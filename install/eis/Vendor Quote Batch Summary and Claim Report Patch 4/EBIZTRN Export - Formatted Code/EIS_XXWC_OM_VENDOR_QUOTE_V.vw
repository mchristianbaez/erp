CREATE OR REPLACE FORCE VIEW xxeis.eis_xxwc_om_vendor_quote_v
(
   name
  ,salesperson_name
  ,salespeson_number
  ,master_account_name
  ,master_account_number
  ,job_name
  ,job_number
  ,vendor_number
  ,vendor_name
  ,oracle_quote_number
  ,invoice_number
  ,invoice_date
  ,part_number
  ,uom
  ,description
  ,bpa
  ,po_cost
  ,average_cost
  ,special_cost
  ,unit_claim_value
  ,rebate
  ,gl_coding
  ,gl_string
  ,loc
  ,created_by
  ,customer_trx_id
  ,order_number
  ,line_number
  ,creation_date
  ,location
  ,qty
  ,process_id
  ,common_output_id
)
AS
   SELECT varchar2_col1 name
         ,varchar2_col2 salesperson_name
         ,varchar2_col3 salespeson_number
         ,varchar2_col4 master_account_name
         ,varchar2_col5 master_account_number
         ,varchar2_col6 job_name
         ,varchar2_col7 job_number
         ,varchar2_col8 vendor_number
         ,varchar2_col9 vendor_name
         ,varchar2_col10 oracle_quote_number
         ,varchar2_col11 invoice_number
         ,date_col1 invoice_date
         ,varchar2_col12 part_number
         ,varchar2_col13 uom
         ,varchar2_col14 description
         ,number_col1 bpa
         ,number_col2 po_cost
         ,number_col3 average_cost
         ,number_col4 special_cost
         ,number_col5 unit_claim_value
         ,number_col6 rebate
         ,varchar2_col15 gl_coding
         ,varchar2_col16 gl_string
         ,varchar2_col17 loc
         ,varchar2_col18 created_by
         ,number_col7 customer_trx_id
         ,number_col8 order_number
         ,number_col9 line_number
         ,date_col2 creation_date
         ,varchar2_col19 location
         ,number_col10 qty
         ,process_id process_id
         ,common_output_id common_output_id
     FROM xxeis.eis_rs_common_outputs;