prompt Creating View Data for 2 - Legal Indicator Query for Bad Debt Write Off
set scan off define off
DECLARE
BEGIN 
--Inserting View XXEIS_1231490_GTVGAS_V
xxeis.eis_rs_ins.v( 'XXEIS_1231490_GTVGAS_V',222,'Paste SQL View for Legal Indicator Query for Bad Debt Write Off','1.0','','','DM027741','APPS','Legal Indicator Query for Bad Debt Write Off View','X1GV12','','');
--Delete View Columns for XXEIS_1231490_GTVGAS_V
xxeis.eis_rs_utility.delete_view_rows('XXEIS_1231490_GTVGAS_V',222,FALSE);
--Inserting View Columns for XXEIS_1231490_GTVGAS_V
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','ACCOUNT_NUMBER',222,'','','','','','DM027741','VARCHAR2','','','Account Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','ACCOUNT_NAME',222,'','','','','','DM027741','VARCHAR2','','','Account Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','NAME',222,'','','','','','DM027741','VARCHAR2','','','Name','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','ACCOUNT_STATUS',222,'','','','','','DM027741','VARCHAR2','','','Account Status','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','ATTRIBUTE5',222,'','','','','','DM027741','VARCHAR2','','','Attribute5','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','ACCOUNT_CREATION_DATE',222,'','','','','','DM027741','DATE','','','Account Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','PARTY_NUMBER',222,'','','','','','DM027741','VARCHAR2','','','Party Number','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','PRIMARY_PHONE_AREA_CODE',222,'','','','','','DM027741','VARCHAR2','','','Primary Phone Area Code','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','CREATION_DATE',222,'','','','','','DM027741','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'XXEIS_1231490_GTVGAS_V','ATTRIBUTE10',222,'','','','','','DM027741','VARCHAR2','','','Attribute10','','','');
--Inserting View Components for XXEIS_1231490_GTVGAS_V
--Inserting View Component Joins for XXEIS_1231490_GTVGAS_V
END;
/
set scan on define on
prompt Creating Report Data for 2 - Legal Indicator Query for Bad Debt Write Off
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - 2 - Legal Indicator Query for Bad Debt Write Off
xxeis.eis_rs_utility.delete_report_rows( '2 - Legal Indicator Query for Bad Debt Write Off' );
--Inserting Report - 2 - Legal Indicator Query for Bad Debt Write Off
xxeis.eis_rs_ins.r( 222,'2 - Legal Indicator Query for Bad Debt Write Off','','','','','','DM027741','XXEIS_1231490_GTVGAS_V','Y','','SELECT 
  DISTINCT A.ACCOUNT_NUMBER,
  A.ACCOUNT_NAME,
  D.NAME,
  C.ACCOUNT_STATUS,
  A.ATTRIBUTE5,
  A.ACCOUNT_ESTABLISHED_DATE ACCOUNT_CREATION_DATE,
  B.PARTY_NUMBER,
  B.PRIMARY_PHONE_AREA_CODE,
  a.creation_date,
a. attribute10
  
FROM 
  AR.HZ_CUST_ACCOUNTS A,  
  AR.HZ_PARTIES B,
  AR.HZ_CUSTOMER_PROFILES C,
    ( SELECT DISTINCT COLLECTOR_ID, NAME
    FROM AR.AR_COLLECTORS) D,
    AR.HZ_CUST_ACCT_SITES_ALL  F
    
WHERE 1=1
  AND A.PARTY_ID = B.PARTY_ID
  AND A.CUST_ACCOUNT_ID = C.CUST_ACCOUNT_ID
  AND A.CUST_ACCOUNT_ID = F.CUST_ACCOUNT_ID
  AND C.COLLECTOR_ID = D.COLLECTOR_ID
  and c.site_use_id is null
  AND F.ORG_ID  in ''162''
','DM027741','','N','Conversion Reports','PDF,','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - 2 - Legal Indicator Query for Bad Debt Write Off
xxeis.eis_rs_ins.rc( '2 - Legal Indicator Query for Bad Debt Write Off',222,'NAME','Name','','','','default','','3','N','','','','','','','','DM027741','N','N','','XXEIS_1231490_GTVGAS_V','','');
xxeis.eis_rs_ins.rc( '2 - Legal Indicator Query for Bad Debt Write Off',222,'ACCOUNT_NAME','Account Name','','','','default','','2','N','','','','','','','','DM027741','N','N','','XXEIS_1231490_GTVGAS_V','','');
xxeis.eis_rs_ins.rc( '2 - Legal Indicator Query for Bad Debt Write Off',222,'ACCOUNT_STATUS','Account Status','','','','default','','4','N','','','','','','','','DM027741','N','N','','XXEIS_1231490_GTVGAS_V','','');
xxeis.eis_rs_ins.rc( '2 - Legal Indicator Query for Bad Debt Write Off',222,'ACCOUNT_NUMBER','Account Number','','','','default','','1','N','','','','','','','','DM027741','N','N','','XXEIS_1231490_GTVGAS_V','','');
xxeis.eis_rs_ins.rc( '2 - Legal Indicator Query for Bad Debt Write Off',222,'ACCOUNT_CREATION_DATE','Account Creation Date','','','','default','','6','N','','','','','','','','DM027741','N','N','','XXEIS_1231490_GTVGAS_V','','');
xxeis.eis_rs_ins.rc( '2 - Legal Indicator Query for Bad Debt Write Off',222,'ATTRIBUTE5','Legal Collection Indicator','','','','default','','5','N','','','','','','','','DM027741','N','N','','XXEIS_1231490_GTVGAS_V','','');
xxeis.eis_rs_ins.rc( '2 - Legal Indicator Query for Bad Debt Write Off',222,'PRIMARY_PHONE_AREA_CODE','Primary Phone Area Code','','','','default','','8','N','','','','','','','','DM027741','N','N','','XXEIS_1231490_GTVGAS_V','','');
xxeis.eis_rs_ins.rc( '2 - Legal Indicator Query for Bad Debt Write Off',222,'PARTY_NUMBER','Party Number','','','','default','','7','N','','','','','','','','DM027741','N','N','','XXEIS_1231490_GTVGAS_V','','');
xxeis.eis_rs_ins.rc( '2 - Legal Indicator Query for Bad Debt Write Off',222,'ATTRIBUTE10','Legal Placement','','','','default','','9','','Y','','','','','','','DM027741','N','N','','XXEIS_1231490_GTVGAS_V','','');
--Inserting Report Parameters - 2 - Legal Indicator Query for Bad Debt Write Off
xxeis.eis_rs_ins.rp( '2 - Legal Indicator Query for Bad Debt Write Off',222,'Account Status','','ACCOUNT_STATUS','IN','','''LEGAL'',''BANKRUPTCY''','VARCHAR2','Y','Y','1','','Y','CONSTANT','DM027741','Y','N','','','');
xxeis.eis_rs_ins.rp( '2 - Legal Indicator Query for Bad Debt Write Off',222,'Creation Date From','','CREATION_DATE','>=','','01-MAY-2012','DATE','Y','Y','2','','Y','CONSTANT','DM027741','Y','N','','','');
--Inserting Report Conditions - 2 - Legal Indicator Query for Bad Debt Write Off
xxeis.eis_rs_ins.rcn( '2 - Legal Indicator Query for Bad Debt Write Off',222,'ACCOUNT_STATUS','IN',':Account Status','','','Y','1','Y','DM027741');
xxeis.eis_rs_ins.rcn( '2 - Legal Indicator Query for Bad Debt Write Off',222,'CREATION_DATE','>=',':Creation Date From','','','Y','2','Y','DM027741');
--Inserting Report Sorts - 2 - Legal Indicator Query for Bad Debt Write Off
xxeis.eis_rs_ins.rs( '2 - Legal Indicator Query for Bad Debt Write Off',222,'ACCOUNT_NUMBER','','DM027741','','');
--Inserting Report Triggers - 2 - Legal Indicator Query for Bad Debt Write Off
--Inserting Report Templates - 2 - Legal Indicator Query for Bad Debt Write Off
--Inserting Report Portals - 2 - Legal Indicator Query for Bad Debt Write Off
--Inserting Report Dashboards - 2 - Legal Indicator Query for Bad Debt Write Off
--Inserting Report Security - 2 - Legal Indicator Query for Bad Debt Write Off
xxeis.eis_rs_ins.rsec( '2 - Legal Indicator Query for Bad Debt Write Off','222','','50993',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( '2 - Legal Indicator Query for Bad Debt Write Off','222','','50854',222,'DM027741','','');
xxeis.eis_rs_ins.rsec( '2 - Legal Indicator Query for Bad Debt Write Off','222','','50879',222,'DM027741','','');
--Inserting Report Pivots - 2 - Legal Indicator Query for Bad Debt Write Off
END;
/
set scan on define on