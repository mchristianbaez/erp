--Report Name            : White Cap End of Month Commissions Report
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating Object Data EIS_XXWC_AR_EOM_COMM_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Object EIS_XXWC_AR_EOM_COMM_V
xxeis.eis_rsc_ins.v( 'EIS_XXWC_AR_EOM_COMM_V',222,'','','','','ANONYMOUS','XXEIS','EIS XXWC AR EOM Comm V','EXAECV','','','VIEW','US','','');
--Delete Object Columns for EIS_XXWC_AR_EOM_COMM_V
xxeis.eis_rsc_utility.delete_view_rows('EIS_XXWC_AR_EOM_COMM_V',222,FALSE);
--Inserting Object Columns for EIS_XXWC_AR_EOM_COMM_V
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','OPERATING_UNIT',222,'Operating Unit','OPERATING_UNIT','','','','ANONYMOUS','VARCHAR2','','','Operating Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','AVERAGECOST',222,'Averagecost','AVERAGECOST','','~T~D~2','','ANONYMOUS','NUMBER','','','Averagecost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','DIRECTFLAG',222,'Directflag','DIRECTFLAG','','','','ANONYMOUS','NUMBER','','','Directflag','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','CATCLASS',222,'Catclass','CATCLASS','','','','ANONYMOUS','VARCHAR2','','','Catclass','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','PARTNO',222,'Partno','PARTNO','','','','ANONYMOUS','VARCHAR2','','','Partno','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','REGIONNAME',222,'Regionname','REGIONNAME','','','','ANONYMOUS','VARCHAR2','','','Regionname','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','LOC',222,'Loc','LOC','','','','ANONYMOUS','VARCHAR2','','','Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','SALESREPNAME',222,'Salesrepname','SALESREPNAME','','','','ANONYMOUS','VARCHAR2','','','Salesrepname','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','SALESREPNUMBER',222,'Salesrepnumber','SALESREPNUMBER','','','','ANONYMOUS','VARCHAR2','','','Salesrepnumber','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','CUSTOMERNAME',222,'Customername','CUSTOMERNAME','','','','ANONYMOUS','VARCHAR2','','','Customername','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','JOBNUMBER',222,'Jobnumber','JOBNUMBER','','','','ANONYMOUS','VARCHAR2','','','Jobnumber','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','MASTERNAME',222,'Mastername','MASTERNAME','','','','ANONYMOUS','VARCHAR2','','','Mastername','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','MASTERNUMBER',222,'Masternumber','MASTERNUMBER','','','','ANONYMOUS','VARCHAR2','','','Masternumber','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','EXTSALE',222,'Extsale','EXTSALE','','','','ANONYMOUS','NUMBER','','','Extsale','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','UNITPRICE',222,'Unitprice','UNITPRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','Unitprice','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','QTY',222,'Qty','QTY','','~T~D~2','','ANONYMOUS','NUMBER','','','Qty','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','DESCRIPTION',222,'Description','DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','Description','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','LINENO',222,'Lineno','LINENO','','','','ANONYMOUS','NUMBER','','','Lineno','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','INVOICENUMBER',222,'Invoicenumber','INVOICENUMBER','','','','ANONYMOUS','VARCHAR2','','','Invoicenumber','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','BUSINESSDATE',222,'Businessdate','BUSINESSDATE','','','','ANONYMOUS','DATE','','','Businessdate','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','TRADER',222,'Trader','TRADER','','','','ANONYMOUS','VARCHAR2','','','Trader','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','INVOICE_SOURCE',222,'Invoice Source','INVOICE_SOURCE','','','','ANONYMOUS','VARCHAR2','','','Invoice Source','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','REPORT_TYPE',222,'Report Type','REPORT_TYPE','','','','ANONYMOUS','VARCHAR2','','','Report Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','PERIOD_NAME',222,'Period Name','PERIOD_NAME','','','','ANONYMOUS','VARCHAR2','','','Period Name','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','SHIPPED_QUANTITY',222,'Shipped Quantity','SHIPPED_QUANTITY','','~T~D~2','','ANONYMOUS','NUMBER','','','Shipped Quantity','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','ORDER_NUMBER',222,'Order Number','ORDER_NUMBER','','','','ANONYMOUS','NUMBER','','','Order Number','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','MFGADJUST',222,'Mfgadjust','MFGADJUST','','','','ANONYMOUS','NUMBER','','','Mfgadjust','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','SALESREP_TYPE',222,'Salesrep Type','SALESREP_TYPE','','','','ANONYMOUS','VARCHAR2','','','Salesrep Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','INTERFACE_LINE_ATTRIBUTE2',222,'Interface Line Attribute2','INTERFACE_LINE_ATTRIBUTE2','','','','ANONYMOUS','VARCHAR2','','','Interface Line Attribute2','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','INSIDESALESREP',222,'Insidesalesrep','INSIDESALESREP','','','','ANONYMOUS','VARCHAR2','','','Insidesalesrep','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','ANONYMOUS','NUMBER','','','Customer Trx Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','ORG_ID',222,'Org Id','ORG_ID','','','','ANONYMOUS','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','PARTY_ID',222,'Party Id','PARTY_ID','','','','ANONYMOUS','NUMBER','','','Party Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','PRISMSRNUM',222,'Prismsrnum','PRISMSRNUM','','','','ANONYMOUS','VARCHAR2','','','Prismsrnum','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','LINETYPE',222,'Linetype','LINETYPE','','','','ANONYMOUS','VARCHAR2','','','Linetype','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','HRINSIDESRNAME',222,'Hrinsidesrname','HRINSIDESRNAME','','','','ANONYMOUS','VARCHAR2','','','Hrinsidesrname','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','HRINSIDESRNUM',222,'Hrinsidesrnum','HRINSIDESRNUM','','','','ANONYMOUS','VARCHAR2','','','Hrinsidesrnum','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','INVENTORY_ITEM_ID',222,'Inventory Item Id','INVENTORY_ITEM_ID','','','','ANONYMOUS','NUMBER','','','Inventory Item Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','MP_ORGANIZATION_ID',222,'Mp Organization Id','MP_ORGANIZATION_ID','','','','ANONYMOUS','NUMBER','','','Mp Organization Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','SPECIAL_COST',222,'Special Cost','SPECIAL_COST','','~T~D~2','','ANONYMOUS','NUMBER','','','Special Cost','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','SALES_TYPE',222,'Sales Type','SALES_TYPE','','','','ANONYMOUS','VARCHAR2','','','Sales Type','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','LIST_PRICE',222,'List Price','LIST_PRICE','','~T~D~2','','ANONYMOUS','NUMBER','','','List Price','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','INSIDESRNUM',222,'Insidesrnum','INSIDESRNUM','','','','ANONYMOUS','NUMBER','','','Insidesrnum','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','PROCESS_ID',222,'Process Id','PROCESS_ID','','','','ANONYMOUS','NUMBER','','','Process Id','','','','US');
xxeis.eis_rsc_ins.vc( 'EIS_XXWC_AR_EOM_COMM_V','USER_ITEM_DESCRIPTION',222,'User Item Description','USER_ITEM_DESCRIPTION','','','','ANONYMOUS','VARCHAR2','','','User Item Description','','','','');
--Inserting Object Components for EIS_XXWC_AR_EOM_COMM_V
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_EOM_COMM_V','RA_CUSTOMER_TRX',222,'RA_CUSTOMER_TRX_ALL','CT','CT','ANONYMOUS','ANONYMOUS','-1','Header-Level Information About Invoices, Debit Mem','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_EOM_COMM_V','MTL_SYSTEM_ITEMS_KFV',222,'MTL_SYSTEM_ITEMS_B','MSI','MSI','ANONYMOUS','ANONYMOUS','-1','Inventory Item Definitions','','','','','','',' ',' ',' ',' ',' ');
xxeis.eis_rsc_ins.vcomp( 'EIS_XXWC_AR_EOM_COMM_V','HZ_PARTIES',222,'HZ_PARTIES','HP','HP','ANONYMOUS','ANONYMOUS','-1','Party Information','','','','','','',' ',' ',' ',' ',' ');
--Inserting Object Component Joins for EIS_XXWC_AR_EOM_COMM_V
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_EOM_COMM_V','RA_CUSTOMER_TRX','CT',222,'EXAECV.CUSTOMER_TRX_ID','=','CT.CUSTOMER_TRX_ID(+)','','','','Y','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_EOM_COMM_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXAECV.INVENTORY_ITEM_ID','=','MSI.INVENTORY_ITEM_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_EOM_COMM_V','MTL_SYSTEM_ITEMS_KFV','MSI',222,'EXAECV.ORGANIZATION_ID','=','MSI.ORGANIZATION_ID(+)','','','','','ANONYMOUS');
xxeis.eis_rsc_ins.vcj( 'EIS_XXWC_AR_EOM_COMM_V','HZ_PARTIES','HP',222,'EXAECV.PARTY_ID','=','HP.PARTY_ID(+)','','','','','ANONYMOUS');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report LOV Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Inserting Report LOVs - White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.lov( 222,'select  per.period_name , led.name ledger_name, per.period_year, per.period_num, per.start_date, per.end_date
from    gl_periods per,gl_ledgers led
where  per.period_set_name = led.period_set_name
and     GL_SECURITY_PKG.VALIDATE_ACCESS(led.ledger_ID) = ''TRUE''
and     led.accounted_period_type = per.period_type','','AR_PERIOD_NAMES','AR_PERIOD_NAMES','XXEIS_RS_ADMIN',NULL,'','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'SELECT hou.NAME organization_name,
       TO_CHAR (date_from,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_from,
       TO_CHAR (date_to,
                NVL (fnd_profile.VALUE (''ICX_DATE_FORMAT_MASK''),
                     ''DD-MON-YYYY'')
               ) date_to
  FROM hr_operating_units hou,
       mo_glob_org_access_tmp tmp
 WHERE hou.organization_id = tmp.organization_id','','EIS_MULTI_OPERATING_UNIT_LOV','This LOV shows all the operating units, to which user has access.','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select DISTINCT NAME from RA_BATCH_SOURCES_ALL','','AR Batch Source Name LOV','Displays Batch Sources information','XXEIS_RS_ADMIN',NULL,'N','','','','','','','US');
xxeis.eis_rsc_ins.lov( 222,'select distinct DECODE(B.category,''EMPLOYEE'',''Employee'',''OTHER'',''House Acct'',''House Acct'') type , B.category
from  JTF.JTF_RS_Resource_Extns_TL b','','SalesRepType','','ANONYMOUS',NULL,'N','','','','','','','US');
null;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
prompt Creating Report Data for White Cap End of Month Commissions Report
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(222);
IF mod_exist = 'Y' THEN 
--Deleting Report data - White Cap End of Month Commissions Report
xxeis.eis_rsc_utility.delete_report_rows( 'White Cap End of Month Commissions Report' );
--Inserting Report - White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.r( 222,'White Cap End of Month Commissions Report','','The purpose of this report is to extract the necessary information on posted sales to enable the accurate calculation of commissions to White Cap Sales Representatives for a defined period of time (e.g. one month). Note:  The parameter ?GL Period to Report? correlates to the range of dates that will be reported (e.g. select APR-20yy) will report all transactions within the respective GL period.
','','','','SA059956','EIS_XXWC_AR_EOM_COMM_V','Y','','','SA059956','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N','','','','','','','','US','','','','');
--Inserting Report Columns - White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'BUSINESSDATE','BusinessDate','Businessdate','','','default','','2','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'CATCLASS','CatClass','Catclass','','','default','','11','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'CUSTOMERNAME','CustomerName','Customername','','','default','','29','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'DESCRIPTION','Description','Description','','','default','','14','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'DIRECTFLAG','DirectFlag','Directflag','','~~~','default','','28','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'INVOICENUMBER','InvoiceNumber','Invoicenumber','','','default','','5','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'JOBNUMBER','JobNumber','Jobnumber','','','default','','4','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'LINENO','LineNo','Lineno','','~~~','default','','12','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'LOC','Loc','Loc','','','default','','9','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'MASTERNAME','MasterName','Mastername','','','default','','30','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'MASTERNUMBER','MasterNumber','Masternumber','','','default','','3','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'PARTNO','PartNo','Partno','','','default','','13','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'QTY','QTY','Qty','','~T~D~0','default','','15','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'REGIONNAME','RegionName','Regionname','','','default','','10','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREPNAME','SalesRepName','Salesrepname','','','default','','8','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREPNUMBER','SalesRepNumber','Salesrepnumber','','','default','','7','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'UNITPRICE','UnitPrice','Unitprice','','~T~D~2','default','','16','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'TRADER','Trader','Trader','','','default','','27','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'REPORT_TYPE','ReportTypeName','Report Type','','','default','','1','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'EXT_AVG_COST','ExtAvgCost','Report Type','NUMBER','~T~D~2','default','','21','Y','Y','','','','','','((CASE WHEN EXAECV.AVERAGECOST = 0 AND EXAECV.REPORT_TYPE =''Product'' THEN nvl(EXAECV.list_price,0) ELSE EXAECV.AVERAGECOST END)   * EXAECV.QTY)','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'GM_AVG_COST','GMAvgCost','Report Type','NUMBER','~T~D~2','default','','23','Y','Y','','','','','','decode(EXAECV.ExtSale ,0,0,((EXAECV.ExtSale - ((CASE WHEN EXAECV.AVERAGECOST = 0 AND EXAECV.REPORT_TYPE =''Product'' THEN nvl(EXAECV.list_price,0) ELSE EXAECV.AVERAGECOST END) * EXAECV.QTY)) / EXAECV.ExtSale))','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'EXT_SPECIAL_COST','ExtSpecialCost','Report Type','NUMBER','~T~D~9','default','','24','Y','Y','','','','','','(EXAECV.SPECIAL_COST * EXAECV.QTY)','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'GPS_SPECIAL_COST','GPSpecialCost','Report Type','NUMBER','~T~D~2','default','','25','Y','Y','','','','','','DECODE((EXAECV.SPECIAL_COST * EXAECV.QTY),0,0,(EXAECV.ExtSale - (EXAECV.SPECIAL_COST * EXAECV.QTY)))','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'GMS_SPECIAL_COST','GMSpecialCost','Report Type','NUMBER','~T~D~2','default','','26','Y','Y','','','','','','DECODE((EXAECV.SPECIAL_COST * EXAECV.QTY),0,0,decode(EXAECV.ExtSale ,0,0,((EXAECV.ExtSale - (EXAECV.SPECIAL_COST * EXAECV.QTY))/(EXAECV.ExtSale))))','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'GP_AVG_COST','GPAvgCost','Report Type','NUMBER','~T~D~2','default','','22','Y','Y','','','','','','(EXAECV.ExtSale - ((CASE WHEN EXAECV.AVERAGECOST = 0 AND EXAECV.REPORT_TYPE =''Product'' THEN nvl(EXAECV.list_price,0) ELSE EXAECV.AVERAGECOST END) * EXAECV.QTY))','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'MFGADJUST','Mfgadjust','Mfgadjust','','~T~D~2','default','','17','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'EXTSALE','Extsale','Extsale','','~T~D~2','default','','18','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'ORDER_NUMBER','Order Number','Order Number','','~~~','default','','31','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'SALESREP_TYPE','SalesRep Type','Salesrep Type','','','default','','6','N','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'AVERAGECOST','AverageCost','Averagecost','NUMBER','~T~D~9','default','','20','Y','Y','','','','','','(CASE WHEN EXAECV.AVERAGECOST = 0 AND EXAECV.REPORT_TYPE =''Product'' THEN nvl(EXAECV.list_price,0) ELSE EXAECV.AVERAGECOST END)','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'INTERFACE_LINE_ATTRIBUTE2','SalesOrder Type','Interface Line Attribute2','','','default','','32','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'INSIDESALESREP','InsideSalesRep','Insidesalesrep','','','default','','34','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
xxeis.eis_rsc_ins.rc( 'White Cap End of Month Commissions Report',222,'USER_ITEM_DESCRIPTION','User Item Description','User Item Description','','','default','','35','','Y','','','','','','','SA059956','N','N','','EIS_XXWC_AR_EOM_COMM_V','','','','US','');
--Inserting Report Parameters - White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.rp( 'White Cap End of Month Commissions Report',222,'Operating Unit','Operating Unit','OPERATING_UNIT','IN','EIS_MULTI_OPERATING_UNIT_LOV','select name from hr_operating_units where organization_id=fnd_profile.value(''ORG_ID'')','VARCHAR2','Y','Y','1','','N','SQL','SA059956','Y','N','','','','EIS_XXWC_AR_EOM_COMM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap End of Month Commissions Report',222,'GL Period to Report','GL Period to Report','PERIOD_NAME','=','AR_PERIOD_NAMES','select gps.period_name from gl_period_statuses gps where gps.application_id = 101  and gps.set_of_books_id = fnd_profile.value(''GL_SET_OF_BKS_ID'') and trunc(sysdate) between gps.start_date and gps.end_date','VARCHAR2','Y','Y','3','','N','SQL','SA059956','Y','N','','','','EIS_XXWC_AR_EOM_COMM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Quantity Shipped','Order Line Quantity Shipped','SHIPPED_QUANTITY','IN','','','NUMERIC','N','N','5','Y','Y','CONSTANT','SA059956','N','N','','','','EIS_XXWC_AR_EOM_COMM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Extended Amount','Order Line Extended Amount','EXTSALE','IN','','','NUMERIC','N','N','6','Y','Y','CONSTANT','SA059956','N','N','','','','EIS_XXWC_AR_EOM_COMM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap End of Month Commissions Report',222,'Order Line Extended Cost','Order Line Extended Cost','AVERAGECOST','IN','','','NUMERIC','N','N','7','Y','Y','CONSTANT','SA059956','N','N','','','','EIS_XXWC_AR_EOM_COMM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap End of Month Commissions Report',222,'Invoice Source','Invoice Source','INVOICE_SOURCE','IN','AR Batch Source Name LOV','''ORDER MANAGEMENT'',''STANDARD OM SOURCE'',''REPAIR OM SOURCE'',''WC MANUAL'',''REBILL-CM'',''REBILL''','VARCHAR2','Y','Y','2','','N','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_EOM_COMM_V','','','US','');
xxeis.eis_rsc_ins.rp( 'White Cap End of Month Commissions Report',222,'SalesRepType','SalesRepType','SALESREP_TYPE','IN','SalesRepType','','VARCHAR2','N','Y','4','Y','Y','CONSTANT','SA059956','Y','N','','','','EIS_XXWC_AR_EOM_COMM_V','','','US','');
--Inserting Dependent Parameters - White Cap End of Month Commissions Report
--Inserting Report Conditions - White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.rcnh( 'White Cap End of Month Commissions Report',222,'AVERAGECOST IN :Order Line Extended Cost ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','AVERAGECOST','','Order Line Extended Cost','','','','','EIS_XXWC_AR_EOM_COMM_V','','','','','','IN','Y','Y','','','','','1',222,'White Cap End of Month Commissions Report','AVERAGECOST IN :Order Line Extended Cost ');
xxeis.eis_rsc_ins.rcnh( 'White Cap End of Month Commissions Report',222,'EXTSALE IN :Order Line Extended Amount ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','EXTSALE','','Order Line Extended Amount','','','','','EIS_XXWC_AR_EOM_COMM_V','','','','','','IN','Y','Y','','','','','1',222,'White Cap End of Month Commissions Report','EXTSALE IN :Order Line Extended Amount ');
xxeis.eis_rsc_ins.rcnh( 'White Cap End of Month Commissions Report',222,'SALESREP_TYPE IN :SalesRepType ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SALESREP_TYPE','','SalesRepType','','','','','EIS_XXWC_AR_EOM_COMM_V','','','','','','IN','Y','Y','','','','','1',222,'White Cap End of Month Commissions Report','SALESREP_TYPE IN :SalesRepType ');
xxeis.eis_rsc_ins.rcnh( 'White Cap End of Month Commissions Report',222,'SHIPPED_QUANTITY IN :Order Line Quantity Shipped ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','SHIPPED_QUANTITY','','Order Line Quantity Shipped','','','','','EIS_XXWC_AR_EOM_COMM_V','','','','','','IN','Y','Y','','','','','1',222,'White Cap End of Month Commissions Report','SHIPPED_QUANTITY IN :Order Line Quantity Shipped ');
xxeis.eis_rsc_ins.rcnh( 'White Cap End of Month Commissions Report',222,'Free Text ','FREE_TEXT','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','','','','','','','','','','','','','','','Y','','','','','and PROCESS_ID= :SYSTEM.PROCESS_ID','1',222,'White Cap End of Month Commissions Report','Free Text ');
--Inserting Report Sorts - White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.rs( 'White Cap End of Month Commissions Report',222,'REPORT_TYPE','ASC','SA059956','1','');
xxeis.eis_rsc_ins.rs( 'White Cap End of Month Commissions Report',222,'SALESREPNUMBER','ASC','SA059956','2','');
--Inserting Report Triggers - White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.rt( 'White Cap End of Month Commissions Report',222,'begin
xxeis.EIS_RS_XXWC_EOM_COMM_REP_PKG.comm_rep_par(
    P_PROCESS_ID => :SYSTEM.PROCESS_ID,
    p_period_name => :GL Period to Report,
    p_operating_unit => :Operating Unit,
    p_invoice_source => :Invoice Source);
end;','B','Y','SA059956','AQ');
--inserting report templates - White Cap End of Month Commissions Report
--Inserting Report Portals - White Cap End of Month Commissions Report
--inserting report dashboards - White Cap End of Month Commissions Report
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( 'White Cap End of Month Commissions Report','222','EIS_XXWC_AR_EOM_COMM_V','EIS_XXWC_AR_EOM_COMM_V','N','');
--inserting report security - White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','401','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','222','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','20005','','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','10010432','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','RB054040','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','RV003897','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','SS084202','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','SE012733','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','SG019472','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','20005','','XXWC_IT_OPERATIONS_ANALYST',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','GG050582','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','JT021060','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','PP018915','',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','20005','','XXWC_VIEW_ALL_EIS_REPORTS',222,'SA059956','','','');
xxeis.eis_rsc_ins.rsec( 'White Cap End of Month Commissions Report','','EG022422','',222,'SA059956','','','');
--Inserting Report Pivots - White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.rpivot( 'White Cap End of Month Commissions Report',222,'Pivot','1','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Pivot
xxeis.eis_rsc_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Pivot','LOC','COLUMN_FIELD','','Branch Id','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Pivot','SALESREPNUMBER','ROW_FIELD','','','','1','xlNormal');
xxeis.eis_rsc_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Pivot','EXTSALE','DATA_FIELD','SUM','','','1','xlNormal');
--Inserting Report Summary Calculation Columns For Pivot- Pivot
xxeis.eis_rsc_ins.rpivot( 'White Cap End of Month Commissions Report',222,'Total Sales by SalesRep and Bra','2','1,0|1,2,1','1,1,0,0|None|2');
--Inserting Report Pivot Details For Pivot - Total Sales by SalesRep and Bra
xxeis.eis_rsc_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Total Sales by SalesRep and Bra','LOC','ROW_FIELD','','','1','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Total Sales by SalesRep and Bra','SALESREPNUMBER','ROW_FIELD','','','2','1','');
xxeis.eis_rsc_ins.rpivot_dtls( 'White Cap End of Month Commissions Report',222,'Total Sales by SalesRep and Bra','EXTSALE','DATA_FIELD','SUM','','1','','');
--Inserting Report Summary Calculation Columns For Pivot- Total Sales by SalesRep and Bra
--Inserting Report   Version details- White Cap End of Month Commissions Report
xxeis.eis_rsc_ins.rv( 'White Cap End of Month Commissions Report','','White Cap End of Month Commissions Report','SA059956');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 222');
END IF;
END;
/
