--Report Name            : Zero ($) Costed Sales Transactions
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View EIS_XXWC_COST_SALES_TXN_V
set scan off define off
prompt Creating View XXEIS.EIS_XXWC_COST_SALES_TXN_V
Create or replace View XXEIS.EIS_XXWC_COST_SALES_TXN_V
 AS 
SELECT mp.attribute9 Region,
    ood.organization_name Branch,
    rctl.creation_date Invoice_Creation_Date,
    trx_number Invoice_Number,
    oh.order_number Sales_Order_Number,
    TRUNC(oh.ordered_date) ordered_date,
    otlh.name order_type,
    oh.attribute7 created_by,
    rep.name Account_Manager_Name,
    rep.salesrep_number Acct_Mngr_Sales_Rep_Number,
    ol.line_number sales_order_line_number,
    otl.name Sales_Order_Line_Type,
    ol.SOURCE_TYPE_CODE Fulfillment_Source,
    ol.FREIGHT_CARRIER_CODE Shipping_Method,
    --       ol.ordered_item,
    msi.concatenated_segments item,
    msi.description item_desc,
    msi.item_type user_item_type,
    msi.inventory_item_status_code item_status,
    xxeis.eis_rs_xxwc_com_util_pkg.Get_inv_cat_class(msi.inventory_item_id,msi.organization_id) Item_Category,
    NVL(rctl.quantity_invoiced, rctl.quantity_credited) Invoiced_Quantity,
    ol.PRICING_QUANTITY_UOM Pricing_Qty_UOM,
    rctl.unit_selling_price,
    ol.ORDERED_QUANTITY,
    (RCTL.UNIT_SELLING_PRICE * NVL(RCTL.QUANTITY_INVOICED, RCTL.QUANTITY_CREDITED)) EXTENDED_SALE_AMOUNT,
    MMT.ACTUAL_COST ACTUAL_UNIT_COST,
    --NVL (APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST (OL.LINE_ID) , 0) Actual_Unit_Cost,
    NVL(apps.Cst_Cost_Api.Get_Item_Cost(1,Msi.Inventory_Item_Id,Msi.Organization_Id),0) Current_Item_average_cost,
    --          (NVL(rctl.quantity_invoiced, rctl.quantity_credited)*
    --          NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id, msi.organization_id), 0)) Projected_Extended_Cost,
    --          (rctl.unit_selling_price*ol.ORDERED_QUANTITY) - (NVL(rctl.quantity_invoiced, rctl.quantity_credited)*
    --          NVL(xxeis.eis_rs_xxwc_com_util_pkg.get_item_cost(msi.inventory_item_id, msi.organization_id), 0)) Projected_GP,
    NVL(hca.account_name, hp.party_name) customer_name,
    hca.account_number customer_number,
    MP.ORGANIZATION_CODE WAREHOUSE,
    rct.customer_trx_id,
    rctl.line_number,
    ol.line_id,
    msi.organization_id
  FROM oe_order_headers oh,
    oe_order_lines ol,
    ra_customer_trx rct,
    ra_customer_trx_lines rctl,
    mtl_parameters mp,
    ORG_ORGANIZATION_DEFINITIONS ood,
    oe_transaction_types_vl otlh,
    oe_transaction_types_vl otl,
    ra_salesreps rep,
    mtl_system_items_kfv msi,
    mtl_material_transactions mmt,
    hz_cust_accounts hca,
    hz_parties hp,
    gl_code_combinations gcc
  WHERE 1                          =1
  AND rct.interface_header_context = 'ORDER ENTRY'
    --AND mp.organization_code = '017'
  AND TO_CHAR(oh.order_number)    = rct.interface_header_attribute1
  AND rctl.customer_trx_id        = rct.customer_trx_id
  AND TO_CHAR(oh.order_number)    = rctl.interface_line_attribute1
  AND TO_CHAR(ol.line_id)         = rctl.interface_line_attribute6
  AND otlh.name                   = rctl.interface_line_attribute2
  AND TO_CHAR(mp.organization_id) = rctl.interface_line_attribute10
  AND otlh.transaction_type_id    = oh.order_type_id
  AND oh.org_id                   = otlh.org_id
  AND oh.salesrep_id              = rep.salesrep_id(+)
  AND oh.org_id                   = rep.org_id(+)
  AND ol.line_type_id             = otl.transaction_type_id
  AND ol.org_id                   = otl.org_id
  AND mp.organization_id          = msi.organization_id
  AND RCTL.INVENTORY_ITEM_ID      = MSI.INVENTORY_ITEM_ID
  AND mmt.trx_source_line_id      = ol.line_id
  AND rct.bill_to_customer_id                                              = hca.cust_account_id
  AND hp.party_id                                                          = hca.party_id
  AND MP.ORGANIZATION_ID                                                   = OOD.ORGANIZATION_ID
  AND nvl(MMT.ACTUAL_COST,0) =0
  --AND NVL (APPS.XXWC_MV_ROUTINES_PKG.GET_ORDER_LINE_COST (OL.LINE_ID) , 0) =0
     AND mmt.source_code             ='ORDER ENTRY'
  AND gcc.code_combination_id = msi.COST_OF_SALES_ACCOUNT
    -- and rct.customer_trx_id in (1858956,584979)
  --  AND OH.ORDER_NUMBER = '12342487'
  AND otlh.name NOT                                                                                   IN ('WC SHORT TERM RENTAL','WC LONG TERM RENTAL','INTERNAL ORDER')
  AND otl.name NOT                                                                                    IN('CREDIT ONLY','BILL ONLY')
  AND UPPER(msi.item_type)                                                                            <> UPPER('Intangible')
  AND UPPER(msi.inventory_item_status_code)                                                           <> UPPER('Intangible')
  AND xxeis.eis_rs_xxwc_com_util_pkg.Get_inv_cat_class(msi.inventory_item_id,msi.organization_id) NOT IN( 'PR.PRMO','GC.GC10','DC.DC99','ML.ML99','99.99RR','99.99RT','RE.READ')
  AND rctl.description                                                                                <> 'Delivery Charge'
  AND GCC.SEGMENT4                                                                                    <> '646080'
  AND rctl.creation_date >= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_from, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 0.25
  AND rctl.creation_date <= TO_DATE ( TO_CHAR ( xxeis.eis_rs_xxwc_com_util_pkg.get_date_TO, xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS'), xxeis.eis_rs_utility.get_date_format
    || ' HH24:MI:SS') + 1.25
    --    AND NOT EXISTS
    --    (SELECT 'Y'
    --    FROM MTL_SYSTEM_ITEMS_B MSI,
    --      GL_CODE_COMBINATIONS_KFV GCC
    --    WHERE 1                     =1
    --    AND MSI.INVENTORY_ITEM_ID   = OL.INVENTORY_ITEM_ID
    --    AND MSI.ORGANIZATION_ID     = OL.SHIP_FROM_ORG_ID
    --    AND GCC.CODE_COMBINATION_ID = MSI.COST_OF_SALES_ACCOUNT
    --    AND GCC.SEGMENT4            ='646080'
    --    )
  --   and oh.header_id = 2215672 --566180
    -- and mp.organization_code ='711'
  ORDER BY OH.HEADER_ID,
    OL.LINE_NUMBER
/
set scan on define on
prompt Creating View Data for Zero ($) Costed Sales Transactions
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_COST_SALES_TXN_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_COST_SALES_TXN_V',222,'','','','','XXEIS_RS_ADMIN','XXEIS','Eis Xxwc Cost Sales Txn V','EXCSTV','','');
--Delete View Columns for EIS_XXWC_COST_SALES_TXN_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_COST_SALES_TXN_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_COST_SALES_TXN_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','BRANCH',222,'Branch','BRANCH','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Branch','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','REGION',222,'Region','REGION','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Region','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','CUSTOMER_NUMBER',222,'Customer Number','CUSTOMER_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','CURRENT_ITEM_AVERAGE_COST',222,'Current Item Average Cost','CURRENT_ITEM_AVERAGE_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Current Item Average Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ACTUAL_UNIT_COST',222,'Actual Unit Cost','ACTUAL_UNIT_COST','','','','XXEIS_RS_ADMIN','NUMBER','','','Actual Unit Cost','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','EXTENDED_SALE_AMOUNT',222,'Extended Sale Amount','EXTENDED_SALE_AMOUNT','','','','XXEIS_RS_ADMIN','NUMBER','','','Extended Sale Amount','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ORDERED_QUANTITY',222,'Ordered Quantity','ORDERED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Ordered Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','UNIT_SELLING_PRICE',222,'Unit Selling Price','UNIT_SELLING_PRICE','','','','XXEIS_RS_ADMIN','NUMBER','','','Unit Selling Price','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','PRICING_QTY_UOM',222,'Pricing Qty Uom','PRICING_QTY_UOM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Pricing Qty Uom','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','INVOICED_QUANTITY',222,'Invoiced Quantity','INVOICED_QUANTITY','','','','XXEIS_RS_ADMIN','NUMBER','','','Invoiced Quantity','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ITEM_CATEGORY',222,'Item Category','ITEM_CATEGORY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Category','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ITEM_STATUS',222,'Item Status','ITEM_STATUS','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','USER_ITEM_TYPE',222,'User Item Type','USER_ITEM_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','User Item Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ITEM_DESC',222,'Item Desc','ITEM_DESC','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item Desc','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ITEM',222,'Item','ITEM','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Item','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','SHIPPING_METHOD',222,'Shipping Method','SHIPPING_METHOD','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Shipping Method','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','FULFILLMENT_SOURCE',222,'Fulfillment Source','FULFILLMENT_SOURCE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Fulfillment Source','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','SALES_ORDER_LINE_TYPE',222,'Sales Order Line Type','SALES_ORDER_LINE_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Sales Order Line Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','SALES_ORDER_LINE_NUMBER',222,'Sales Order Line Number','SALES_ORDER_LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Order Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ACCT_MNGR_SALES_REP_NUMBER',222,'Acct Mngr Sales Rep Number','ACCT_MNGR_SALES_REP_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Acct Mngr Sales Rep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ACCOUNT_MANAGER_NAME',222,'Account Manager Name','ACCOUNT_MANAGER_NAME','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Account Manager Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','CREATED_BY',222,'Created By','CREATED_BY','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Created By','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ORDER_TYPE',222,'Order Type','ORDER_TYPE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Order Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ORDERED_DATE',222,'Ordered Date','ORDERED_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Ordered Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','SALES_ORDER_NUMBER',222,'Sales Order Number','SALES_ORDER_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Sales Order Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','ORGANIZATION_ID',222,'Organization Id','ORGANIZATION_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Organization Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','LINE_ID',222,'Line Id','LINE_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','LINE_NUMBER',222,'Line Number','LINE_NUMBER','','','','XXEIS_RS_ADMIN','NUMBER','','','Line Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','XXEIS_RS_ADMIN','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','INVOICE_CREATION_DATE',222,'Invoice Creation Date','INVOICE_CREATION_DATE','','','','XXEIS_RS_ADMIN','DATE','','','Invoice Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_COST_SALES_TXN_V','WAREHOUSE',222,'Warehouse','WAREHOUSE','','','','XXEIS_RS_ADMIN','VARCHAR2','','','Warehouse','','','');
--Inserting View Components for EIS_XXWC_COST_SALES_TXN_V
--Inserting View Component Joins for EIS_XXWC_COST_SALES_TXN_V
END;
/
set scan on define on
prompt Creating Report LOV Data for Zero ($) Costed Sales Transactions
set scan off define off
DECLARE
BEGIN 
--Inserting Report LOVs - Zero ($) Costed Sales Transactions
xxeis.eis_rs_ins.lov( 222,'select distinct ATTRIBUTE9 region from mtl_parameters','','Region Lov','','ANONYMOUS',NULL,'N','','');
xxeis.eis_rs_ins.lov( 222,'SELECT ORGANIZATION_CODE WAREHOUSE,ORGANIZATION_NAME FROM ORG_ORGANIZATION_DEFINITIONS OOD WHERE SYSDATE < NVL(OOD.DISABLE_DATE,SYSDATE+1) AND EXISTS (SELECT 1 FROM XXEIS.EIS_ORG_ACCESS_V  WHERE organization_id = ood.organization_id )
UNION
SELECT ''All'', ''All Organizations'' from Dual','','OM Warehouse All','','ANONYMOUS',NULL,'N','','');
END;
/
set scan on define on
prompt Creating Report Data for Zero ($) Costed Sales Transactions
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Zero ($) Costed Sales Transactions
xxeis.eis_rs_utility.delete_report_rows( 'Zero ($) Costed Sales Transactions' );
--Inserting Report - Zero ($) Costed Sales Transactions
xxeis.eis_rs_ins.r( 222,'Zero ($) Costed Sales Transactions','','This report lists the sales transactions with COGS = $0, but should have had a COGS >$0.','','','','XXEIS_RS_ADMIN','EIS_XXWC_COST_SALES_TXN_V','Y','','','XXEIS_RS_ADMIN','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','');
--Inserting Report Columns - Zero ($) Costed Sales Transactions
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ACCOUNT_MANAGER_NAME','Account Manager Name','Account Manager Name','','','default','','9','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ACCT_MNGR_SALES_REP_NUMBER','Acct Mngr Sales Rep Number','Acct Mngr Sales Rep Number','','','default','','10','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ACTUAL_UNIT_COST','Actual Unit Cost','Actual Unit Cost','','~T~D~2','default','','24','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'BRANCH','Branch','Branch','','','default','','2','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'CREATED_BY','Created By','Created By','','','default','','8','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'CURRENT_ITEM_AVERAGE_COST','Current Item Average Cost','Current Item Average Cost','','~,~.~5','default','','25','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','29','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'CUSTOMER_NUMBER','Customer Number','Customer Number','','','default','','30','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'EXTENDED_SALE_AMOUNT','Extended Sale Amount','Extended Sale Amount','','~T~D~2','default','','23','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'FULFILLMENT_SOURCE','Fulfillment Source','Fulfillment Source','','','default','','13','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'INVOICED_QUANTITY','Invoiced Quantity','Invoiced Quantity','','~~~','default','','20','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','4','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ITEM','Item','Item','','','default','','15','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ITEM_CATEGORY','Item Category','Item Category','','','default','','19','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ITEM_DESC','Item Desc','Item Desc','','','default','','16','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ITEM_STATUS','Item Status','Item Status','','','default','','18','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ORDERED_DATE','Ordered Date','Ordered Date','','','default','','6','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ORDERED_QUANTITY','Ordered Quantity','Ordered Quantity','','~~~','default','','31','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'ORDER_TYPE','Order Type','Order Type','','','default','','7','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'PRICING_QTY_UOM','Pricing Qty Uom','Pricing Qty Uom','','','default','','21','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'REGION','Region','Region','','','default','','1','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'SALES_ORDER_LINE_NUMBER','Sales Order Line Number','Sales Order Line Number','','~~~','default','','11','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'SALES_ORDER_LINE_TYPE','Sales Order Line Type','Sales Order Line Type','','','default','','12','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'SALES_ORDER_NUMBER','Sales Order Number','Sales Order Number','','~~~','default','','5','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'UNIT_SELLING_PRICE','Unit Selling Price','Unit Selling Price','','~T~D~2','default','','22','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'USER_ITEM_TYPE','User Item Type','User Item Type','','','default','','17','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'INVOICE_CREATION_DATE','Invoice Creation Date','Invoice Creation Date','','','default','','3','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'PROJECTED EXTENDED COST','Projected Extended Cost','Invoice Creation Date','NUMBER','~T~D~2','default','','26','Y','','','','','','','EXCSTV.Invoiced_Quantity*EXCSTV.Current_Item_average_cost','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'SHIPPING_METHOD','Shipping Method','Shipping Method','','','default','','14','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'WAREHOUSE','Warehouse','Warehouse','','','default','','32','N','','','','','','','','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'PROJECTED GP','Projected GP','Warehouse','NUMBER','~,~.~2','default','','27','Y','','','','','','','TO_CHAR((EXCSTV.Extended_Sale_Amount) -(EXCSTV.Invoiced_Quantity*EXCSTV.Current_Item_average_cost))','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
xxeis.eis_rs_ins.rc( 'Zero ($) Costed Sales Transactions',222,'PROJECTED GM','Projected GM(%)','Warehouse','NUMBER','~,~.~2','default','','28','Y','','','','','','','case when nvl(EXCSTV.Extended_Sale_Amount,0)!=0 then (((EXCSTV.Extended_Sale_Amount -(EXCSTV.Invoiced_Quantity*EXCSTV.Current_Item_average_cost))/EXCSTV.Extended_Sale_Amount)*100) else 0  end','XXEIS_RS_ADMIN','N','N','','EIS_XXWC_COST_SALES_TXN_V','','');
--Inserting Report Parameters - Zero ($) Costed Sales Transactions
xxeis.eis_rs_ins.rp( 'Zero ($) Costed Sales Transactions',222,'Region','Region','REGION','IN','Region Lov','','VARCHAR2','N','Y','2','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Zero ($) Costed Sales Transactions',222,'Sales From Date','Sales From Date','','IN','','','DATE','Y','Y','3','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Zero ($) Costed Sales Transactions',222,'Sales To Date','Sales To Date','','IN','','','DATE','Y','Y','4','','N','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
xxeis.eis_rs_ins.rp( 'Zero ($) Costed Sales Transactions',222,'Warehouse','Warehouse','WAREHOUSE','IN','OM Warehouse All','','VARCHAR2','N','Y','1','','Y','CONSTANT','XXEIS_RS_ADMIN','Y','N','','','');
--Inserting Report Conditions - Zero ($) Costed Sales Transactions
xxeis.eis_rs_ins.rcn( 'Zero ($) Costed Sales Transactions',222,'REGION','IN',':Region','','','Y','2','Y','XXEIS_RS_ADMIN');
xxeis.eis_rs_ins.rcn( 'Zero ($) Costed Sales Transactions',222,'WAREHOUSE','IN',':Warehouse','','','Y','1','Y','XXEIS_RS_ADMIN');
--Inserting Report Sorts - Zero ($) Costed Sales Transactions
--Inserting Report Triggers - Zero ($) Costed Sales Transactions
xxeis.eis_rs_ins.rt( 'Zero ($) Costed Sales Transactions',222,'begin
xxeis.eis_rs_xxwc_com_util_pkg.set_date_from(:Sales From Date);
xxeis.eis_rs_xxwc_com_util_pkg.set_date_to(:Sales To Date);
end;','B','Y','XXEIS_RS_ADMIN');
--Inserting Report Templates - Zero ($) Costed Sales Transactions
--Inserting Report Portals - Zero ($) Costed Sales Transactions
--Inserting Report Dashboards - Zero ($) Costed Sales Transactions
--Inserting Report Security - Zero ($) Costed Sales Transactions
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','20005','','50900',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','51045',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50901',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','51025',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50860',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50886',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50859',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50858',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50857',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','51208',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50622',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50638',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','51330',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50894',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','401','','50848',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','401','','50866',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','222','','50847',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','401','','50864',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','401','','50863',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','401','','50862',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','401','','50865',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','401','','50846',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','401','','50845',222,'XXEIS_RS_ADMIN','','');
xxeis.eis_rs_ins.rsec( 'Zero ($) Costed Sales Transactions','401','','50844',222,'XXEIS_RS_ADMIN','','');
--Inserting Report Pivots - Zero ($) Costed Sales Transactions
END;
/
set scan on define on
