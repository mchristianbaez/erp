/* Following file contains below Objects Source/Objects Meta Data/Reports Meta Data/ LOVs: 
*/
/*Running the file from XXEIS works only the reports creation and views that owned by XXEIS.*/
--Report Name            : 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXEIS_1416490_SIEICV_V
prompt Table type Objects cannot be imported
set scan off define off
prompt Creating Object APPS.XXEIS_1416490_SIEICV_V
set scan off define off
DECLARE
mod_exist varchar2(1);
stmt CLOB;
v_objlength INTEGER := 0;
v_offset INTEGER := 1;
v_sql_long DBMS_SQL.varchar2s;
v_sql_long_count INTEGER := 1;
c NUMBER;
r NUMBER;
l_stmt varchar2(32000);
l_view_cur pls_integer;
l_view_rows NUMBER;
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
dbms_lob.createtemporary(stmt,TRUE);
dbms_lob.write( lob_loc   => stmt,amount    => length('create or replace view APPS.XXEIS_1416490_SIEICV_V
 ("OPN_ID","OPN_CODE","OPN_TYPE","HDS_FRU_SECTION","HDS_LOC_LOB_ABB","HDS_PROP_FRU","HDS_PROP_SUBLOB_ABB","LOCATION_CODE","LOCATION_TYPE","BLDG_LAND_ID","BLDG_LAND_CODE","BLDG_LAND_TYPE","FLOOR_PARCEL_ID","FLOOR_PARCEL_CODE","FLOOR_PARCEL_TYPE","HDS_EMP_ASSIGN_CURRENT_FLAG","HDS_FRU_SECTION_CURRENT_FLAG","SPACE_TYPE","SPACE_TAG","SITE_TYPE","ASSIGNMENT_TYPE","OPN_COST_CENTER","OPN_FRU","LC_FRU","LC_FRU_DESCRIPTION","HDS_PRI_FRU_FLAG","HDS_LOC_BU_PRI_OPS_FRU","PRIMARY_PHONE_NUMBER","HDS_PROP_FRU_PRI_FLAG","HDS_PROP_FRU_PRI_OPS","HDS_LOC_BU_PRI_OPS","HDS_PROP_BU_PRI_OPS","OPN_HR_BASED_FRU","HDS_OPS_STATUS","LOC_SUITE_NAME","EMP_ASSIGN_START_DATE","EMP_ASSIGN_END_DATE","LC_INACTIVE_FLAG","LC_LOB_BRANCH","LC_LOB_DEPT","LC_ENTRP_ENTITY","LC_ENTRP_LOC","LC_ENTRP_CC","LC_AP_DOCUM_FLAG","LC_HC_FLAG","LC_CREATION_DT","LC_SYSTEM_CD","LC_IEXP_FRU_OVERRIDE","LC_COUNTRY","LC_STATE","LC_EFFECTIVE_DATE","LC_BUSINESS_UNIT","BU_ID_MS","FPS_ID","FPS_DESC","SBU_ID","SBU_DESC","BU_ID","BU_DESC","TAX_ID","LEGAL_ENTITY","EVP","ENTITY_CLASSIFICATION","FPS_ACTIVE_FLAG","HDS_LOB_MS","LOC_ACTIVE_START_DATE","LOC_ACTIVE_END_DATE","EMP_NUMBER","EMP_LAST_NAME","EMP_FIRST_NAME","EMP_FULL_NAME","UOM_CODE","AREA_PCT","AREA_ALLOCATED","AREA_UTILIZED","EMP_SPACE_COMMENTS","ATTRIBUTE_CATEGORY","SPACE_TYPE_LOOKUP_CODE","LOC_TYPE_LOOKUP_CODE","LOCATION_ID","LOC_LOCN_ID","EMP_SPACE_ASSIGN_ID","PERSON_ID","PA_PROJECT_ID","PA_TASK_ID","ORG_ID","LOC_BU","LOB_ABB","LOB_DBA","SUBLOB_ABB","SUBLOB_DBA","HDS_CURRENT_FLAG","SITE_TYPE_CODE","ASSIGNMENT_TYPE_CODE","PRIMARY_SUB_BU_RANK","HDS_FRU_LOC","HDS_FEED_TO_EXTENDED","HDS_FEED_TO_EXTERNAL","FAX_NUMBER") as '),offset    => 1, BUFFER    => 'create or replace view APPS.XXEIS_1416490_SIEICV_V
 ("OPN_ID","OPN_CODE","OPN_TYPE","HDS_FRU_SECTION","HDS_LOC_LOB_ABB","HDS_PROP_FRU","HDS_PROP_SUBLOB_ABB","LOCATION_CODE","LOCATION_TYPE","BLDG_LAND_ID","BLDG_LAND_CODE","BLDG_LAND_TYPE","FLOOR_PARCEL_ID","FLOOR_PARCEL_CODE","FLOOR_PARCEL_TYPE","HDS_EMP_ASSIGN_CURRENT_FLAG","HDS_FRU_SECTION_CURRENT_FLAG","SPACE_TYPE","SPACE_TAG","SITE_TYPE","ASSIGNMENT_TYPE","OPN_COST_CENTER","OPN_FRU","LC_FRU","LC_FRU_DESCRIPTION","HDS_PRI_FRU_FLAG","HDS_LOC_BU_PRI_OPS_FRU","PRIMARY_PHONE_NUMBER","HDS_PROP_FRU_PRI_FLAG","HDS_PROP_FRU_PRI_OPS","HDS_LOC_BU_PRI_OPS","HDS_PROP_BU_PRI_OPS","OPN_HR_BASED_FRU","HDS_OPS_STATUS","LOC_SUITE_NAME","EMP_ASSIGN_START_DATE","EMP_ASSIGN_END_DATE","LC_INACTIVE_FLAG","LC_LOB_BRANCH","LC_LOB_DEPT","LC_ENTRP_ENTITY","LC_ENTRP_LOC","LC_ENTRP_CC","LC_AP_DOCUM_FLAG","LC_HC_FLAG","LC_CREATION_DT","LC_SYSTEM_CD","LC_IEXP_FRU_OVERRIDE","LC_COUNTRY","LC_STATE","LC_EFFECTIVE_DATE","LC_BUSINESS_UNIT","BU_ID_MS","FPS_ID","FPS_DESC","SBU_ID","SBU_DESC","BU_ID","BU_DESC","TAX_ID","LEGAL_ENTITY","EVP","ENTITY_CLASSIFICATION","FPS_ACTIVE_FLAG","HDS_LOB_MS","LOC_ACTIVE_START_DATE","LOC_ACTIVE_END_DATE","EMP_NUMBER","EMP_LAST_NAME","EMP_FIRST_NAME","EMP_FULL_NAME","UOM_CODE","AREA_PCT","AREA_ALLOCATED","AREA_UTILIZED","EMP_SPACE_COMMENTS","ATTRIBUTE_CATEGORY","SPACE_TYPE_LOOKUP_CODE","LOC_TYPE_LOOKUP_CODE","LOCATION_ID","LOC_LOCN_ID","EMP_SPACE_ASSIGN_ID","PERSON_ID","PA_PROJECT_ID","PA_TASK_ID","ORG_ID","LOC_BU","LOB_ABB","LOB_DBA","SUBLOB_ABB","SUBLOB_DBA","HDS_CURRENT_FLAG","SITE_TYPE_CODE","ASSIGNMENT_TYPE_CODE","PRIMARY_SUB_BU_RANK","HDS_FRU_LOC","HDS_FEED_TO_EXTENDED","HDS_FEED_TO_EXTERNAL","FAX_NUMBER") as ');
l_stmt :=  'select "OPN_ID","OPN_CODE","OPN_TYPE","HDS_FRU_SECTION","HDS_LOC_LOB_ABB","HDS_PROP_FRU","HDS_PROP_SUBLOB_ABB","LOCATION_CODE","LOCATION_TYPE","BLDG_LAND_ID","BLDG_LAND_CODE","BLDG_LAND_TYPE","FLOOR_PARCEL_ID","FLOOR_PARCEL_CODE","FLOOR_PARCEL_TYPE","HDS_EMP_ASSIGN_CURRENT_FLAG","HDS_FRU_SECTION_CURRENT_FLAG","SPACE_TYPE","SPACE_TAG","SITE_TYPE","ASSIGNMENT_TYPE","OPN_COST_CENTER","OPN_FRU","LC_FRU","LC_FRU_DESCRIPTION","HDS_PRI_FRU_FLAG","HDS_LOC_BU_PRI_OPS_FRU","PRIMARY_PHONE_NUMBER","HDS_PROP_FRU_PRI_FLAG","HDS_PROP_FRU_PRI_OPS","HDS_LOC_BU_PRI_OPS","HDS_PROP_BU_PRI_OPS","OPN_HR_BASED_FRU","HDS_OPS_STATUS","LOC_SUITE_NAME","EMP_ASSIGN_START_DATE","EMP_ASSIGN_END_DATE","LC_INACTIVE_FLAG","LC_LOB_BRANCH","LC_LOB_DEPT","LC_ENTRP_ENTITY","LC_ENTRP_LOC","LC_ENTRP_CC","LC_AP_DOCUM_FLAG","LC_HC_FLAG","LC_CREATION_DT","LC_SYSTEM_CD","LC_IEXP_FRU_OVERRIDE","LC_COUNTRY","LC_STATE","LC_EFFECTIVE_DATE","LC_BUSINESS_UNIT","BU_ID_MS","FPS_ID","FPS_DESC","SBU_ID","SBU_DESC","BU_ID","BU_DESC","TAX_ID","LEGAL_ENTITY","EVP","ENTITY_CLASSIFICATION","FPS_ACTIVE_FLAG","HDS_LOB_MS","LOC_ACTIVE_START_DATE","LOC_ACTIVE_END_DATE","EMP_NUMBER","EMP_LAST_NAME","EMP_FIRST_NAME","EMP_FULL_NAME","UOM_CODE","AREA_PCT","AREA_ALLOCATED","AREA_UTILIZED","EMP_SPACE_COMMENTS","ATTRIBUTE_CATEGORY","SPACE_TYPE_LOOKUP_CODE","LOC_TYPE_LOOKUP_CODE","LOCATION_ID","LOC_LOCN_ID","EMP_SPACE_ASSIGN_ID","PERSON_ID","PA_PROJECT_ID","PA_TASK_ID","ORG_ID","LOC_BU","LOB_ABB","LOB_DBA","SUBLOB_ABB","SUBLOB_DBA","HDS_CURRENT_FLAG","SITE_TYPE_CODE","ASSIGNMENT_TYPE_CODE","PRIMARY_SUB_BU_RANK","HDS_FRU_LOC","HDS_FEED_TO_EXTENDED","HDS_FEED_TO_EXTERNAL","FAX_NUMBER"
from XXCUS.XXCUS_OPN_FRU_LOC_ALL

';
dbms_lob.APPEND( dest_lob  => stmt,      src_lob   => l_stmt );
v_objlength := DBMS_LOB.getlength (stmt);
IF v_objlength <= 32000 THEN 
EXECUTE IMMEDIATE to_char(stmt);
ELSE
v_offset := 1; 
WHILE v_offset <= v_objlength
LOOP
-- each record is 256 bytes from the LOB
v_sql_long (v_sql_long_count) := DBMS_LOB.SUBSTR (stmt, 256, v_offset);
-- increment the record count
v_sql_long_count := v_sql_long_count + 1;
-- figure out the new offset
v_offset := v_offset + 256;
END LOOP;
BEGIN
  -- open cursor
 c := DBMS_SQL.open_cursor;
 -- parse VARCHAR2S table
 DBMS_SQL.parse (c, v_sql_long, 1, v_sql_long_count - 1, FALSE, 1);
 -- Execute the cursor
r := DBMS_SQL.EXECUTE (c);
-- Close the cursor
 DBMS_SQL.close_cursor (c);
 END;
END IF;
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
prompt Creating Object Data XXEIS_1416490_SIEICV_V
set scan off define off
DECLARE
mod_exist VARCHAR2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
--Inserting Object XXEIS_1416490_SIEICV_V
xxeis.eis_rsc_ins.v( 'XXEIS_1416490_SIEICV_V',240,'Paste SQL View for 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','1.0','','','DV003828','APPS','2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table View','X1SV','','','VIEW','US','','');
--Delete Object Columns for XXEIS_1416490_SIEICV_V
xxeis.eis_rsc_utility.delete_view_rows('XXEIS_1416490_SIEICV_V',240,FALSE);
--Inserting Object Columns for XXEIS_1416490_SIEICV_V
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','OPN_ID',240,'','','','','','DV003828','NUMBER','','','Opn Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','OPN_CODE',240,'','','','','','DV003828','VARCHAR2','','','Opn Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','OPN_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Opn Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOC_BU',240,'','','','','','DV003828','VARCHAR2','','','Loc Bu','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOCATION_CODE',240,'','','','','','DV003828','VARCHAR2','','','Location Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOCATION_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Location Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','BLDG_LAND_ID',240,'','','','','','DV003828','NUMBER','','','Bldg Land Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','BLDG_LAND_CODE',240,'','','','','','DV003828','VARCHAR2','','','Bldg Land Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','FLOOR_PARCEL_ID',240,'','','','','','DV003828','NUMBER','','','Floor Parcel Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','FLOOR_PARCEL_CODE',240,'','','','','','DV003828','VARCHAR2','','','Floor Parcel Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','SPACE_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Space Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','SPACE_TAG',240,'','','','','','DV003828','NUMBER','','','Space Tag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','OPN_COST_CENTER',240,'','','','','','DV003828','VARCHAR2','','','Opn Cost Center','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','OPN_FRU',240,'','','','','','DV003828','VARCHAR2','','','Opn Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_FRU',240,'','','','','','DV003828','VARCHAR2','','','Lc Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_FRU_DESCRIPTION',240,'','','','','','DV003828','VARCHAR2','','','Lc Fru Description','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','PRIMARY_PHONE_NUMBER',240,'','','','','','DV003828','VARCHAR2','','','Primary Phone Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','OPN_HR_BASED_FRU',240,'','','','','','DV003828','VARCHAR2','','','Opn Hr Based Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_OPS_STATUS',240,'','','','','','DV003828','VARCHAR2','','','Hds Ops Status','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOC_SUITE_NAME',240,'','','','','','DV003828','VARCHAR2','','','Loc Suite Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','EMP_ASSIGN_START_DATE',240,'','','','','','DV003828','DATE','','','Emp Assign Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','EMP_ASSIGN_END_DATE',240,'','','','','','DV003828','DATE','','','Emp Assign End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_INACTIVE_FLAG',240,'','','','','','DV003828','VARCHAR2','','','Lc Inactive Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_LOB_BRANCH',240,'','','','','','DV003828','VARCHAR2','','','Lc Lob Branch','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_LOB_DEPT',240,'','','','','','DV003828','VARCHAR2','','','Lc Lob Dept','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_ENTRP_ENTITY',240,'','','','','','DV003828','VARCHAR2','','','Lc Entrp Entity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_ENTRP_LOC',240,'','','','','','DV003828','VARCHAR2','','','Lc Entrp Loc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_ENTRP_CC',240,'','','','','','DV003828','VARCHAR2','','','Lc Entrp Cc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_AP_DOCUM_FLAG',240,'','','','','','DV003828','VARCHAR2','','','Lc Ap Docum Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_HC_FLAG',240,'','','','','','DV003828','VARCHAR2','','','Lc Hc Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_CREATION_DT',240,'','','','','','DV003828','DATE','','','Lc Creation Dt','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_SYSTEM_CD',240,'','','','','','DV003828','VARCHAR2','','','Lc System Cd','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_IEXP_FRU_OVERRIDE',240,'','','','','','DV003828','VARCHAR2','','','Lc Iexp Fru Override','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_COUNTRY',240,'','','','','','DV003828','VARCHAR2','','','Lc Country','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_STATE',240,'','','','','','DV003828','VARCHAR2','','','Lc State','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_EFFECTIVE_DATE',240,'','','','','','DV003828','VARCHAR2','','','Lc Effective Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LC_BUSINESS_UNIT',240,'','','','','','DV003828','VARCHAR2','','','Lc Business Unit','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','BU_ID_MS',240,'','','','','','DV003828','VARCHAR2','','','Bu Id Ms','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','FPS_ID',240,'','','','','','DV003828','VARCHAR2','','','Fps Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','FPS_DESC',240,'','','','','','DV003828','VARCHAR2','','','Fps Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','SBU_ID',240,'','','','','','DV003828','VARCHAR2','','','Sbu Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','SBU_DESC',240,'','','','','','DV003828','VARCHAR2','','','Sbu Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','BU_ID',240,'','','','','','DV003828','VARCHAR2','','','Bu Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','BU_DESC',240,'','','','','','DV003828','VARCHAR2','','','Bu Desc','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','TAX_ID',240,'','','','','','DV003828','VARCHAR2','','','Tax Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LEGAL_ENTITY',240,'','','','','','DV003828','VARCHAR2','','','Legal Entity','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','EVP',240,'','','','','','DV003828','VARCHAR2','','','Evp','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','ENTITY_CLASSIFICATION',240,'','','','','','DV003828','VARCHAR2','','','Entity Classification','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','FPS_ACTIVE_FLAG',240,'','','','','','DV003828','VARCHAR2','','','Fps Active Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOC_ACTIVE_START_DATE',240,'','','','','','DV003828','DATE','','','Loc Active Start Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOC_ACTIVE_END_DATE',240,'','','','','','DV003828','DATE','','','Loc Active End Date','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','EMP_NUMBER',240,'','','','','','DV003828','VARCHAR2','','','Emp Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','EMP_LAST_NAME',240,'','','','','','DV003828','VARCHAR2','','','Emp Last Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','EMP_FIRST_NAME',240,'','','','','','DV003828','VARCHAR2','','','Emp First Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','EMP_FULL_NAME',240,'','','','','','DV003828','VARCHAR2','','','Emp Full Name','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','UOM_CODE',240,'','','','','','DV003828','VARCHAR2','','','Uom Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','AREA_PCT',240,'','','','','','DV003828','NUMBER','','','Area Pct','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','AREA_ALLOCATED',240,'','','','','','DV003828','NUMBER','','','Area Allocated','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','AREA_UTILIZED',240,'','','','','','DV003828','NUMBER','','','Area Utilized','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','EMP_SPACE_COMMENTS',240,'','','','','','DV003828','VARCHAR2','','','Emp Space Comments','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','ATTRIBUTE_CATEGORY',240,'','','','','','DV003828','VARCHAR2','','','Attribute Category','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','SPACE_TYPE_LOOKUP_CODE',240,'','','','','','DV003828','VARCHAR2','','','Space Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOC_TYPE_LOOKUP_CODE',240,'','','','','','DV003828','VARCHAR2','','','Loc Type Lookup Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOCATION_ID',240,'','','','','','DV003828','NUMBER','','','Location Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOC_LOCN_ID',240,'','','','','','DV003828','NUMBER','','','Loc Locn Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','EMP_SPACE_ASSIGN_ID',240,'','','','','','DV003828','NUMBER','','','Emp Space Assign Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','PERSON_ID',240,'','','','','','DV003828','NUMBER','','','Person Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','PA_PROJECT_ID',240,'','','','','','DV003828','NUMBER','','','Pa Project Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','PA_TASK_ID',240,'','','','','','DV003828','NUMBER','','','Pa Task Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','ORG_ID',240,'','','','','','DV003828','NUMBER','','','Org Id','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','ASSIGNMENT_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Assignment Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','ASSIGNMENT_TYPE_CODE',240,'','','','','','DV003828','VARCHAR2','','','Assignment Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','BLDG_LAND_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Bldg Land Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','FLOOR_PARCEL_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Floor Parcel Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_CURRENT_FLAG',240,'','','','','','DV003828','VARCHAR2','','','Hds Current Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_EMP_ASSIGN_CURRENT_FLAG',240,'','','','','','DV003828','VARCHAR2','','','Hds Emp Assign Current Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_FRU_SECTION',240,'','','','','','DV003828','VARCHAR2','','','Hds Fru Section','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_FRU_SECTION_CURRENT_FLAG',240,'','','','','','DV003828','VARCHAR2','','','Hds Fru Section Current Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_LOB_MS',240,'','','','','','DV003828','VARCHAR2','','','Hds Lob Ms','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_LOC_BU_PRI_OPS',240,'','','','','','DV003828','VARCHAR2','','','Hds Loc Bu Pri Ops','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_LOC_BU_PRI_OPS_FRU',240,'','','','','','DV003828','VARCHAR2','','','Hds Loc Bu Pri Ops Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_LOC_LOB_ABB',240,'','','','','','DV003828','VARCHAR2','','','Hds Loc Lob Abb','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_PRI_FRU_FLAG',240,'','','','','','DV003828','VARCHAR2','','','Hds Pri Fru Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_PROP_BU_PRI_OPS',240,'','','','','','DV003828','VARCHAR2','','','Hds Prop Bu Pri Ops','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_PROP_FRU',240,'','','','','','DV003828','VARCHAR2','','','Hds Prop Fru','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_PROP_FRU_PRI_FLAG',240,'','','','','','DV003828','VARCHAR2','','','Hds Prop Fru Pri Flag','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_PROP_FRU_PRI_OPS',240,'','','','','','DV003828','VARCHAR2','','','Hds Prop Fru Pri Ops','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_PROP_SUBLOB_ABB',240,'','','','','','DV003828','VARCHAR2','','','Hds Prop Sublob Abb','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOB_ABB',240,'','','','','','DV003828','VARCHAR2','','','Lob Abb','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','LOB_DBA',240,'','','','','','DV003828','VARCHAR2','','','Lob Dba','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','PRIMARY_SUB_BU_RANK',240,'','','','','','DV003828','NUMBER','','','Primary Sub Bu Rank','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','SITE_TYPE',240,'','','','','','DV003828','VARCHAR2','','','Site Type','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','SITE_TYPE_CODE',240,'','','','','','DV003828','VARCHAR2','','','Site Type Code','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','SUBLOB_ABB',240,'','','','','','DV003828','VARCHAR2','','','Sublob Abb','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','SUBLOB_DBA',240,'','','','','','DV003828','VARCHAR2','','','Sublob Dba','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','FAX_NUMBER',240,'','','','','','DV003828','VARCHAR2','','','Fax Number','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_FEED_TO_EXTENDED',240,'','','','','','DV003828','VARCHAR2','','','Hds Feed To Extended','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_FEED_TO_EXTERNAL',240,'','','','','','DV003828','VARCHAR2','','','Hds Feed To External','','','','US');
xxeis.eis_rsc_ins.vc( 'XXEIS_1416490_SIEICV_V','HDS_FRU_LOC',240,'','','','','','DV003828','VARCHAR2','','','Hds Fru Loc','','','','US');
--Inserting Object Components for XXEIS_1416490_SIEICV_V
--Inserting Object Component Joins for XXEIS_1416490_SIEICV_V
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
--There are no LOV/LOV TL entries for the report 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
prompt Creating Report Data for 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
set scan off define off
DECLARE
mod_exist varchar2(1);
BEGIN 
mod_exist := XXEIS.EIS_RSC_INSERT.check_if_module_exist(240);
IF mod_exist = 'Y' THEN 
--Deleting Report data - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
xxeis.eis_rsc_utility.delete_report_rows( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table' );
--Inserting Report - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
xxeis.eis_rsc_ins.r( 240,'2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','','','','','','AM033286','XXEIS_1416490_SIEICV_V','Y','','select *
from XXCUS.XXCUS_OPN_FRU_LOC_ALL
','AM033286','','N','Tables On Demand','RTF,PDF,','CSV,HTML,Html Summary,XML,Pivot Excel,EXCEL,','N','','','','','','','APPS','US','','','','');
--Inserting Report Columns - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'OPN_CODE','Opn Code','','','','','','1','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_COUNTRY','Lc Country','','','','','','2','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOCATION_ID','Location Id','','','','','','3','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'BLDG_LAND_ID','Bldg Land Id','','','','','','4','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'AREA_UTILIZED','Area Utilized','','','','','','5','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'OPN_FRU','Opn Fru','','','','','','6','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'AREA_ALLOCATED','Area Allocated','','','','','','7','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_OPS_STATUS','Hds Ops Status','','','','','','8','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'UOM_CODE','Uom Code','','','','','','9','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_LOB_BRANCH','Lc Lob Branch','','','','','','10','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'EMP_ASSIGN_END_DATE','Emp Assign End Date','','','','','','11','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'BU_DESC','Bu Desc','','','','','','12','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'FLOOR_PARCEL_CODE','Floor Parcel Code','','','','','','13','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOC_BU','Loc Bu','','','','','','14','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_EFFECTIVE_DATE','Lc Effective Date','','','','','','15','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_ENTRP_CC','Lc Entrp Cc','','','','','','16','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'AREA_PCT','Area Pct','','','','','','17','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOC_TYPE_LOOKUP_CODE','Loc Type Lookup Code','','','','','','18','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'EMP_NUMBER','Emp Number','','','','','','19','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'EMP_LAST_NAME','Emp Last Name','','','','','','20','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'PA_TASK_ID','Pa Task Id','','','','','','21','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'ORG_ID','Org Id','','','','','','22','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_FRU','Lc Fru','','','','','','23','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'SBU_ID','Sbu Id','','','','','','24','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'FLOOR_PARCEL_ID','Floor Parcel Id','','','','','','25','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_FRU_DESCRIPTION','Lc Fru Description','','','','','','26','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'PRIMARY_PHONE_NUMBER','Primary Phone Number','','','','','','27','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOC_ACTIVE_END_DATE','Loc Active End Date','','','','','','28','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_ENTRP_LOC','Lc Entrp Loc','','','','','','29','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'EMP_FIRST_NAME','Emp First Name','','','','','','30','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOC_LOCN_ID','Loc Locn Id','','','','','','31','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'EVP','Evp','','','','','','32','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'TAX_ID','Tax Id','','','','','','33','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_SYSTEM_CD','Lc System Cd','','','','','','34','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_HC_FLAG','Lc Hc Flag','','','','','','35','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_LOB_DEPT','Lc Lob Dept','','','','','','36','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'PA_PROJECT_ID','Pa Project Id','','','','','','37','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOC_ACTIVE_START_DATE','Loc Active Start Date','','','','','','38','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'SPACE_TYPE_LOOKUP_CODE','Space Type Lookup Code','','','','','','39','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_AP_DOCUM_FLAG','Lc Ap Docum Flag','','','','','','40','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'BU_ID_MS','Bu Id Ms','','','','','','41','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_IEXP_FRU_OVERRIDE','Lc Iexp Fru Override','','','','','','42','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'OPN_COST_CENTER','Opn Cost Center','','','','','','43','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_INACTIVE_FLAG','Lc Inactive Flag','','','','','','44','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'EMP_SPACE_ASSIGN_ID','Emp Space Assign Id','','','','','','45','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_BUSINESS_UNIT','Lc Business Unit','','','','','','46','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOCATION_TYPE','Location Type','','','','','','47','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'BU_ID','Bu Id','','','','','','48','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'SBU_DESC','Sbu Desc','','','','','','49','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_CREATION_DT','Lc Creation Dt','','','','','','50','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'PERSON_ID','Person Id','','','','','','51','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'OPN_HR_BASED_FRU','Opn Hr Based Fru','','','','','','52','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'BLDG_LAND_CODE','Bldg Land Code','','','','','','53','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'EMP_SPACE_COMMENTS','Emp Space Comments','','','','','','54','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'ATTRIBUTE_CATEGORY','Attribute Category','','','','','','55','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'ENTITY_CLASSIFICATION','Entity Classification','','','','','','56','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LEGAL_ENTITY','Legal Entity','','','','','','57','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'EMP_ASSIGN_START_DATE','Emp Assign Start Date','','','','','','58','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOC_SUITE_NAME','Loc Suite Name','','','','','','59','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'SPACE_TAG','Space Tag','','','','','','60','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'FPS_ACTIVE_FLAG','Fps Active Flag','','','','','','61','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'EMP_FULL_NAME','Emp Full Name','','','','','','62','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'OPN_ID','Opn Id','','','','','','63','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'OPN_TYPE','Opn Type','','','','','','64','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_ENTRP_ENTITY','Lc Entrp Entity','','','','','','65','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'FPS_ID','Fps Id','','','','','','66','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'SPACE_TYPE','Space Type','','','','','','67','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOCATION_CODE','Location Code','','','','','','68','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LC_STATE','Lc State','','','','','','69','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'FPS_DESC','Fps Desc','','','','','','70','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'ASSIGNMENT_TYPE','Assignment Type','','','','','','71','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'ASSIGNMENT_TYPE_CODE','Assignment Type Code','','','','','','72','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'BLDG_LAND_TYPE','Bldg Land Type','','','','','','73','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'FLOOR_PARCEL_TYPE','Floor Parcel Type','','','','','','74','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_CURRENT_FLAG','Hds Current Flag','','','','','','75','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_EMP_ASSIGN_CURRENT_FLAG','Hds Emp Assign Current Flag','','','','','','76','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_FRU_SECTION','Hds Fru Section','','','','','','77','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_FRU_SECTION_CURRENT_FLAG','Hds Fru Section Current Flag','','','','','','78','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_LOB_MS','Hds Lob Ms','','','','','','79','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_LOC_BU_PRI_OPS','Hds Loc Bu Pri Ops','','','','','','80','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_LOC_BU_PRI_OPS_FRU','Hds Loc Bu Pri Ops Fru','','','','','','81','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_LOC_LOB_ABB','Hds Loc Lob Abb','','','','','','82','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_PRI_FRU_FLAG','Hds Pri Fru Flag','','','','','','83','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_PROP_BU_PRI_OPS','Hds Prop Bu Pri Ops','','','','','','84','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_PROP_FRU','Hds Prop Fru','','','','','','85','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_PROP_FRU_PRI_FLAG','Hds Prop Fru Pri Flag','','','','','','86','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_PROP_FRU_PRI_OPS','Hds Prop Fru Pri Ops','','','','','','87','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_PROP_SUBLOB_ABB','Hds Prop Sublob Abb','','','','','','88','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOB_ABB','Lob Abb','','','','','','89','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'LOB_DBA','Lob Dba','','','','','','90','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'PRIMARY_SUB_BU_RANK','Primary Sub Bu Rank','','','','','','91','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','SUM','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'SITE_TYPE','Site Type','','','','','','92','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'SITE_TYPE_CODE','Site Type Code','','','','','','93','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'SUBLOB_ABB','Sublob Abb','','','','','','94','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'SUBLOB_DBA','Sublob Dba','','','','','','95','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'FAX_NUMBER','Fax Number','','','','','','96','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_FEED_TO_EXTENDED','Hds Feed To Extended','','','','','','97','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_FEED_TO_EXTERNAL','Hds Feed To External','','','','','','98','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
xxeis.eis_rsc_ins.rc( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_FRU_LOC','Hds Fru Loc','','','','','','99','N','','','','','','','','AM033286','N','N','','XXEIS_1416490_SIEICV_V','','','GROUP_BY','US','');
--Inserting Report Parameters - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
xxeis.eis_rsc_ins.rp( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'Hds Fru Section Current Flag','','HDS_FRU_SECTION_CURRENT_FLAG','IN','','''Y''','VARCHAR2','N','Y','1','Y','Y','CONSTANT','AM033286','Y','N','','','','XXEIS_1416490_SIEICV_V','','','US','');
--Inserting Dependent Parameters - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
--Inserting Report Conditions - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
xxeis.eis_rsc_ins.rcnh( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table',240,'HDS_FRU_SECTION_CURRENT_FLAG IN :Hds Fru Section Current Flag ','SIMPLE','','','Y','');
xxeis.eis_rsc_ins.rcnd( '','','HDS_FRU_SECTION_CURRENT_FLAG','','Hds Fru Section Current Flag','','','','','XXEIS_1416490_SIEICV_V','','','','','','IN','Y','Y','','','','','1',240,'2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','HDS_FRU_SECTION_CURRENT_FLAG IN :Hds Fru Section Current Flag ');
--Inserting Report Sorts - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
--Inserting Report Triggers - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
--inserting report templates - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
--Inserting Report Portals - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
--inserting report dashboards - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
--Exporting report views table information -  
xxeis.eis_rsc_ins.rviews( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','240','XXEIS_1416490_SIEICV_V','XXEIS_1416490_SIEICV_V','N','');
--inserting report security - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
xxeis.eis_rsc_ins.rsec( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','20003','','XXCUS_OPN_PROP_MGR',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','20003','','XXCUS_OPN_ADF_ONLY',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','20003','','XXCUS_OPN_GENL_MOD',240,'AM033286','','','');
xxeis.eis_rsc_ins.rsec( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','20005','','XXWC_VIEW_ALL_EIS_REPORTS',240,'AM033286','','','');
--Inserting Report Pivots - 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
--Inserting Report   Version details- 2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table
xxeis.eis_rsc_ins.rv( '2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','','2 - XXCUS.XXCUS_OPN_FRU_LOC_ALL table','AB063501');
ELSE
RAISE_APPLICATION_ERROR (-20001,'Module does not exist. Import/Create the module with id 240');
END IF;
END;
/
