--Report Name            : WC Sourcing Rule Listing
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
--Creating View XXWC_SOURCING_RULES_LISTING_V
set scan off define off
prompt Creating View XXEIS.XXWC_SOURCING_RULES_LISTING_V
Create or replace View XXEIS.XXWC_SOURCING_RULES_LISTING_V
 AS 
SELECT msr.sourcing_rule_name ,
    msr.description ,
    asa.segment1 supplier_number ,
    asa.vendor_name supplier_name ,
    assa.vendor_site_code supplier_site ,
    NVL(
    (SELECT COUNT(*)
    FROM apps.mrp_sr_assignments msa
    WHERE msa.sourcing_rule_id = msr.sourcing_rule_id
    ),0) assignments ,
    assa.vendor_site_id ,
    assa.ADDRESS_LINE1 ,
    assa.ADDRESS_LINE2 ,
    assa.city ,
    assa.state ,
    assa.zip ,
    (SELECT COUNT(*)
    FROM apps.po_headers_all
    WHERE vendor_site_id = assa.vendor_site_id
    ) PO_Count ,
    assa.inactive_date,
    (SELECT organization_code
    FROM apps.mtl_parameters
    WHERE organization_id = msr.organization_id
    ) Org
  FROM apps.mrp_sourcing_rules msr ,
    apps.mrp_sr_receipt_org msro ,
    apps.mrp_sr_source_org msso ,
    apps.ap_suppliers asa ,
    apps.ap_supplier_sites_all assa
  WHERE asa.vendor_id           = assa.vendor_id
  AND assa.org_id               = 162
  AND assa.purchasing_site_flag = 'Y'
  AND assa.vendor_id            = msso.vendor_id(+)
  AND assa.vendor_site_id       = msso.vendor_site_id(+)
  AND msso.source_type(+)       = 3
  AND msso.sr_receipt_id        = msro.sr_receipt_id(+)
  AND TRUNC (SYSDATE) BETWEEN TRUNC (msro.effective_date(+)) AND NVL (TRUNC (msro.disable_date(+)) , TRUNC (SYSDATE))
  AND msro.sourcing_rule_id = msr.sourcing_rule_id(+)
  ORDER BY asa.segment1,
    assa.vendor_site_code/
set scan on define on
prompt Creating View Data for WC Sourcing Rule Listing
set scan off define off
DECLARE
BEGIN 
--Inserting View XXWC_SOURCING_RULES_LISTING_V
xxeis.eis_rs_ins.v( 'XXWC_SOURCING_RULES_LISTING_V',201,'','','','','HT038687','XXEIS','Xxwc Sourcing Rules Listing V','XSRLV','','');
--Delete View Columns for XXWC_SOURCING_RULES_LISTING_V
xxeis.eis_rs_utility.delete_view_rows('XXWC_SOURCING_RULES_LISTING_V',201,FALSE);
--Inserting View Columns for XXWC_SOURCING_RULES_LISTING_V
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','SOURCING_RULE_NAME',201,'Sourcing Rule Name','SOURCING_RULE_NAME','','','','HT038687','VARCHAR2','','','Sourcing Rule Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','PO_COUNT',201,'Po Count','PO_COUNT','','','','HT038687','NUMBER','','','Po Count','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','ZIP',201,'Zip','ZIP','','','','HT038687','VARCHAR2','','','Zip','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','STATE',201,'State','STATE','','','','HT038687','VARCHAR2','','','State','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','CITY',201,'City','CITY','','','','HT038687','VARCHAR2','','','City','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','ADDRESS_LINE2',201,'Address Line2','ADDRESS_LINE2','','','','HT038687','VARCHAR2','','','Address Line2','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','ADDRESS_LINE1',201,'Address Line1','ADDRESS_LINE1','','','','HT038687','VARCHAR2','','','Address Line1','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','VENDOR_SITE_ID',201,'Vendor Site Id','VENDOR_SITE_ID','','','','HT038687','NUMBER','','','Vendor Site Id','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','ASSIGNMENTS',201,'Assignments','ASSIGNMENTS','','','','HT038687','NUMBER','','','Assignments','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','SUPPLIER_SITE',201,'Supplier Site','SUPPLIER_SITE','','','','HT038687','VARCHAR2','','','Supplier Site','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','SUPPLIER_NAME',201,'Supplier Name','SUPPLIER_NAME','','','','HT038687','VARCHAR2','','','Supplier Name','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','SUPPLIER_NUMBER',201,'Supplier Number','SUPPLIER_NUMBER','','','','HT038687','VARCHAR2','','','Supplier Number','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','DESCRIPTION',201,'Description','DESCRIPTION','','','','HT038687','VARCHAR2','','','Description','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','ORG',201,'Org','ORG','','','','HT038687','VARCHAR2','','','Org','','','');
xxeis.eis_rs_ins.vc( 'XXWC_SOURCING_RULES_LISTING_V','INACTIVE_DATE',201,'Inactive Date','INACTIVE_DATE','','','','HT038687','DATE','','','Inactive Date','','','');
--Inserting View Components for XXWC_SOURCING_RULES_LISTING_V
--Inserting View Component Joins for XXWC_SOURCING_RULES_LISTING_V
END;
/
set scan on define on
prompt Creating Report Data for WC Sourcing Rule Listing
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - WC Sourcing Rule Listing
xxeis.eis_rs_utility.delete_report_rows( 'WC Sourcing Rule Listing' );
--Inserting Report - WC Sourcing Rule Listing
xxeis.eis_rs_ins.r( 201,'WC Sourcing Rule Listing','','WC Sourcing Rules Listing','','','','HT038687','XXWC_SOURCING_RULES_LISTING_V','Y','','','HT038687','','N','White Cap Reports','','CSV,Pivot Excel,EXCEL,','N');
--Inserting Report Columns - WC Sourcing Rule Listing
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'ADDRESS_LINE1','Address Line1','Address Line1','','','default','','8','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'ADDRESS_LINE2','Address Line2','Address Line2','','','default','','9','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'ASSIGNMENTS','Assignments','Assignments','','~~~','default','','6','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'CITY','City','City','','','default','','10','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'DESCRIPTION','Description','Description','','','default','','2','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'PO_COUNT','Po Count','Po Count','','~~~','default','','13','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'SOURCING_RULE_NAME','Sourcing Rule Name','Sourcing Rule Name','','','default','','1','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'STATE','State','State','','','default','','11','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'SUPPLIER_NAME','Supplier Name','Supplier Name','','','default','','4','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'SUPPLIER_NUMBER','Supplier Number','Supplier Number','','','default','','3','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'SUPPLIER_SITE','Supplier Site','Supplier Site','','','default','','5','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'VENDOR_SITE_ID','Vendor Site Id','Vendor Site Id','','~~~','default','','7','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'ZIP','Zip','Zip','','','default','','12','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'ORG','Org','Org','','','default','','15','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
xxeis.eis_rs_ins.rc( 'WC Sourcing Rule Listing',201,'INACTIVE_DATE','Inactive Date','Inactive Date','','','default','','14','N','','','','','','','','HT038687','N','N','','XXWC_SOURCING_RULES_LISTING_V','','');
--Inserting Report Parameters - WC Sourcing Rule Listing
--Inserting Report Conditions - WC Sourcing Rule Listing
--Inserting Report Sorts - WC Sourcing Rule Listing
--Inserting Report Triggers - WC Sourcing Rule Listing
--Inserting Report Templates - WC Sourcing Rule Listing
--Inserting Report Portals - WC Sourcing Rule Listing
--Inserting Report Dashboards - WC Sourcing Rule Listing
--Inserting Report Security - WC Sourcing Rule Listing
xxeis.eis_rs_ins.rsec( 'WC Sourcing Rule Listing','20005','','50900',201,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC Sourcing Rule Listing','201','','50893',201,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC Sourcing Rule Listing','201','','50910',201,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC Sourcing Rule Listing','20005','','50897',201,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC Sourcing Rule Listing','20005','','50843',201,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC Sourcing Rule Listing','20005','','50861',201,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC Sourcing Rule Listing','401','','50981',201,'HT038687','','');
xxeis.eis_rs_ins.rsec( 'WC Sourcing Rule Listing','200','','50904',201,'HT038687','','');
--Inserting Report Pivots - WC Sourcing Rule Listing
END;
/
set scan on define on
