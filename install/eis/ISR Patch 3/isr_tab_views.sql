drop table xxeis.EIS_XXWC_PO_ISR_TAB;

CREATE TABLE "XXEIS"."EIS_XXWC_PO_ISR_TAB"
  (
    "ORG"                   VARCHAR2(240 BYTE),
    "PRE"                   VARCHAR2(240 BYTE),
    "ITEM_NUMBER"           VARCHAR2(240 BYTE),
    "VENDOR_NUM"            VARCHAR2(240 BYTE),
    "VENDOR_NAME"           VARCHAR2(240 BYTE),
    "SOURCE"                VARCHAR2(240 BYTE),
    "ST"                    VARCHAR2(240 BYTE),
    "DESCRIPTION"           VARCHAR2(450 BYTE),
    "CAT"                   VARCHAR2(240 BYTE),
    "PPLT"                  NUMBER,
    "PLT"                   NUMBER,
    "UOM"                   VARCHAR2(240 BYTE),
    "CL"                    VARCHAR2(240 BYTE),
    "STK_FLAG"              VARCHAR2(10 BYTE),
    "PM"                    VARCHAR2(240 BYTE),
    "MINN"                  NUMBER,
    "MAXN"                  NUMBER,
    "AMU"                   VARCHAR2(240 BYTE),
    "MF_FLAG"               VARCHAR2(10 BYTE),
    "HIT6_STORE_SALES"      NUMBER,
    "HIT6_OTHER_INV_SALES"  NUMBER,
    "AVER_COST"             NUMBER,
    "ITEM_COST"             NUMBER,
    "BPA"                   VARCHAR2(240 BYTE),
    "QOH"                   NUMBER,
    "AVAILABLE"             NUMBER,
    "AVAILABLEDOLLAR"       NUMBER,
    "ONE_STORE_SALE"        NUMBER,
    "SIX_STORE_SALE"        NUMBER,
    "TWELVE_STORE_SALE"     NUMBER,
    "ONE_OTHER_INV_SALE"    NUMBER,
    "SIX_OTHER_INV_SALE"    NUMBER,
    "TWELVE_OTHER_INV_SALE" NUMBER,
    "BIN_LOC"               VARCHAR2(240 BYTE),
    "MC"                    VARCHAR2(240 BYTE),
    "FI_FLAG"               VARCHAR2(10 BYTE),
    "FREEZE_DATE" DATE,
    "RES"                  VARCHAR2(240 BYTE),
    "THIRTEEN_WK_AVG_INV"  NUMBER,
    "THIRTEEN_WK_AN_COGS"  NUMBER,
    "TURNS"                NUMBER,
    "BUYER"                VARCHAR2(240 BYTE),
    "TS"                   NUMBER,
    "JAN_STORE_SALE"       NUMBER,
    "FEB_STORE_SALE"       NUMBER,
    "MAR_STORE_SALE"       NUMBER,
    "APR_STORE_SALE"       NUMBER,
    "MAY_STORE_SALE"       NUMBER,
    "JUN_STORE_SALE"       NUMBER,
    "JUL_STORE_SALE"       NUMBER,
    "AUG_STORE_SALE"       NUMBER,
    "SEP_STORE_SALE"       NUMBER,
    "OCT_STORE_SALE"       NUMBER,
    "NOV_STORE_SALE"       NUMBER,
    "DEC_STORE_SALE"       NUMBER,
    "JAN_OTHER_INV_SALE"   NUMBER,
    "FEB_OTHER_INV_SALE"   NUMBER,
    "MAR_OTHER_INV_SALE"   NUMBER,
    "APR_OTHER_INV_SALE"   NUMBER,
    "MAY_OTHER_INV_SALE"   NUMBER,
    "JUN_OTHER_INV_SALE"   NUMBER,
    "JUL_OTHER_INV_SALE"   NUMBER,
    "AUG_OTHER_INV_SALE"   NUMBER,
    "SEP_OTHER_INV_SALE"   NUMBER,
    "OCT_OTHER_INV_SALE"   NUMBER,
    "NOV_OTHER_INV_SALE"   NUMBER,
    "DEC_OTHER_INV_SALE"   NUMBER,
    "HIT4_STORE_SALES"     NUMBER,
    "HIT4_OTHER_INV_SALES" NUMBER,
    "INVENTORY_ITEM_ID"    NUMBER,
    "ORGANIZATION_ID"      NUMBER,
    "SET_OF_BOOKS_ID"      NUMBER,
    "ORG_NAME"             VARCHAR2(150 BYTE),
    "DISTRICT"             VARCHAR2(100 BYTE),
    "REGION"               VARCHAR2(100 BYTE),
    "COMMON_OUTPUT_ID"     NUMBER,
    "PROCESS_ID"           NUMBER,
    "INV_CAT_SEG1"         VARCHAR2(240 BYTE),
    "ON_ORD"               NUMBER,
    "BPA_COST"             NUMBER,
    "OPEN_REQ"             NUMBER,
    "WT"                   NUMBER,
    "FML"                  NUMBER,
    "SOURCING_RULE"        VARCHAR2(450 BYTE),
    "SO"                   NUMBER,
    "SS"                   NUMBER,
    "CLT"                  NUMBER,
    "AVAIL2"               NUMBER,
    "INT_REQ"              NUMBER,
    "DIR_REQ"              NUMBER,
    "DEMAND"		   NUMBER			
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 131072 NEXT 131072 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "XXEIS_DATA" ;
/

CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_PO_ISR_RPT_V" ("ORG", "PRE", "ITEM_NUMBER", "VENDOR_NUM", "VENDOR_NAME", "SOURCE", "ST", "DESCRIPTION", "CAT", "PPLT", "PLT", "UOM", "CL", "STK_FLAG", "PM", "MINN", "MAXN", "AMU", "MF_FLAG", "HIT6_SALES", "AVER_COST", "ITEM_COST", "BPA_COST", "BPA", "QOH", "AVAILABLE", "AVAILABLEDOLLAR", "JAN_SALES", "FEB_SALES", "MAR_SALES", "APR_SALES", "MAY_SALES", "JUNE_SALES", "JUL_SALES", "AUG_SALES", "SEP_SALES", "OCT_SALES", "NOV_SALES", "DEC_SALES", "HIT4_SALES", "ONE_SALES", "SIX_SALES", "TWELVE_SALES", "BIN_LOC", "MC", "FI_FLAG", "FREEZE_DATE", "RES", "THIRTEEN_WK_AVG_INV", "THIRTEEN_WK_AN_COGS", "TURNS", "BUYER", "TS", "SO", "INVENTORY_ITEM_ID", "ORGANIZATION_ID", "SET_OF_BOOKS_ID", "ON_ORD", "WT", "SS", "FML", "OPEN_REQ", "ORG_NAME", "DISTRICT", "REGION", "SOURCING_RULE", "CLT", "COMMON_OUTPUT_ID", "PROCESS_ID", "AVAIL2", "INT_REQ", "DIR_REQ","DEMAND")
AS
  SELECT Varchar2_Col1 "ORG",
    Varchar2_Col2 "PRE",
    Varchar2_Col3 "ITEM_NUMBER",
    Varchar2_Col4 "VENDOR_NUM",
    Varchar2_Col5 "VENDOR_NAME",
    Varchar2_Col6 "SOURCE",
    Varchar2_Col7 "ST",
    Varchar2_Col8 "DESCRIPTION",
    Varchar2_Col9 "CAT",
    Varchar2_Col10 "PPLT",
    Varchar2_Col11 "PLT",
    Varchar2_Col12 "UOM",
    Varchar2_Col13 "CL",
    Varchar2_Col14 "STK_FLAG",
    Varchar2_Col15 "PM",
    Varchar2_Col16 "MINN",
    Varchar2_Col17 "MAXN",
    Varchar2_Col18 "AMU",
    Varchar2_Col19 "MF_FLAG",
    Number_Col1 Hit6_Sales,
    Number_Col2 "AVER_COST",
    Number_Col3 "ITEM_COST",
    Number_Col4 "BPA_COST",
    Varchar2_Col20 "BPA",
    Number_Col5 "QOH",
    Number_Col6 "AVAILABLE",
    Number_Col7 "AVAILABLEDOLLAR",
    Number_Col8 Jan_Sales,
    Number_Col9 Feb_Sales,
    Number_Col10 Mar_Sales,
    Number_Col11 Apr_Sales,
    Number_Col12 May_Sales,
    Number_Col13 June_Sales,
    Number_Col14 Jul_Sales,
    Number_Col15 Aug_Sales,
    Number_Col16 Sep_Sales,
    Number_Col17 Oct_Sales,
    Number_Col18 Nov_Sales,
    Number_Col19 Dec_Sales,
    Number_Col20 Hit4_Sales,
    Number_Col21 One_Sales,
    Number_Col22 Six_Sales,
    Number_Col23 Twelve_Sales,
    Varchar2_Col21 "BIN_LOC",
    Varchar2_Col22 "MC",
    Varchar2_Col23 "FI_FLAG",
    Date_Col1 "FREEZE_DATE",
    Varchar2_Col24 "RES",
    Number_Col24 "THIRTEEN_WK_AVG_INV",
    Number_Col25 "THIRTEEN_WK_AN_COGS",
    Number_Col26 "TURNS",
    Varchar2_Col25 "BUYER",
    Varchar2_Col26 "TS",
    Number_Col27 "SO",
    Number_Col28 "INVENTORY_ITEM_ID",
    Number_Col29 "ORGANIZATION_ID",
    Number_Col30 "SET_OF_BOOKS_ID",
    number_col31 "ON_ORD",
    Number_Col32 "WT",
    Number_Col33 "SS",
    Number_Col34 "FML",
    Number_Col35 "OPEN_REQ",
    Varchar2_Col27 "ORG_NAME",
    Varchar2_Col28 "DISTRICT",
    varchar2_col29 "REGION",
    Varchar2_Col30 "SOURCING_RULE",
    Number_Col36 "CLT",
    common_output_id,
    process_id,
    Number_Col37 "AVAIL2",
    Number_Col38 "INT_REQ",
    Number_Col39 "DIR_REQ",
    Number_Col40 "DEMAND"
  FROM XXEIS.EIS_RS_COMMON_OUTPUTS;
/

CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_PO_ISR_TAB_V" ("ORG", "ORGANIZATION_NAME", "DISTRICT", "REGION", "PRE", "ITEM_NUMBER", "VENDOR_NAME", "VENDOR_NUMBER", "SOURCE", "ST", "DESCRIPTION", "CAT_CLASS", "INV_CAT_SEG1", "PPLT", "PLT", "UOM", "CL", "STK", "PM", "MIN", "MAX", "HIT_6", "AVER_COST", "ITEM_COST", "BPA_COST", "BPA", "QOH", "ON_ORD", "AVAIL", "AVAIL_D", "BIN_LOC", "MC", "FI", "FREEZE_DATE", "RES", "THIRTEEN_WK_AVG_INV", "THIRTEEN_WK_AN_COGS", "TURNS", "BUYER", "TS", "AMU", "SO", "MF_FLAG", "WT", "SS", "SOURCING_RULE", "FML", "OPEN_REQ", "INVENTORY_ITEM_ID", "ORGANIZATION_ID", "SET_OF_BOOKS_ID", "CLT", "AVAIL2", "SUPPLY", "DEMAND", "INT_REQ", "DIR_REQ")
AS
  SELECT ood.organization_code org,
    ood.organization_name organization_name,
    mtp.attribute8 district,
    mtp.attribute9 region,
    SUBSTR (msi.segment1, 1, 3) pre,
    msi.segment1 item_number,
    xxeis.eis_po_xxwc_isr_util_pkg.get_vendor_name ( msi.inventory_item_id, msi.organization_id) vendor_name,
    xxeis.eis_po_xxwc_isr_util_pkg.get_vendor_number ( msi.inventory_item_id, msi.organization_id) vendor_number,
    CASE
      WHEN item_source_type.meaning = 'Supplier'
      THEN xxeis.eis_po_xxwc_isr_util_pkg.get_vendor_number ( msi.inventory_item_id, msi.organization_id)
      WHEN item_source_type.meaning = 'Inventory'
      THEN
        (SELECT organization_code
        FROM org_organization_definitions source_org
        WHERE source_org.organization_id = msi.source_organization_id
        )
      ELSE NULL
    END source,
    CASE
      WHEN item_source_type.meaning = 'Supplier'
      THEN 'S'
      WHEN item_source_type.meaning = 'Inventory'
      THEN 'I'
      ELSE NULL
    END st,
    msi.description description,
    xxeis.eis_po_xxwc_isr_util_pkg.get_inv_cat_class ( msi.inventory_item_id, msi.organization_id) cat_class,
    xxeis.eis_po_xxwc_isr_util_pkg.get_inv_cat_seg1 ( msi.inventory_item_id, msi.organization_id) inv_cat_seg1,
    preprocessing_lead_time pplt,
    msi.full_lead_time plt,
    msi.primary_uom_code uom,
    xxeis.eis_po_xxwc_isr_util_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) cl,
    CASE
      WHEN (xxeis.eis_po_xxwc_isr_util_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) IN ('1', '2', '3', '4', '5', '6', '7', '8', '9', 'C','B'))
        --AND ITEM_TYPE                                                                                           ='STOCK ITEM')
      THEN 'Y'
      WHEN ( xxeis.eis_po_xxwc_isr_util_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) IN ('E')
      AND (min_minmax_quantity                                                                                      = 0
      AND max_minmax_quantity                                                                                       = 0))
      THEN 'N'
      WHEN ( xxeis.eis_po_xxwc_isr_util_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) IN ('E')
      AND (min_minmax_quantity                                                                                      > 0
      AND max_minmax_quantity                                                                                       > 0))
      THEN 'Y'
      WHEN (xxeis.eis_po_xxwc_isr_util_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) IN ('N', 'Z'))
        --AND ITEM_TYPE                                                                                           ='NON-STOCK')
      THEN 'N'
      ELSE 'N'
    END stk,
    mrp_planning_code.meaning pm,
    min_minmax_quantity MIN,
    max_minmax_quantity MAX,
    NULL hit_6,
    NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) aver_cost,
    NVL ( xxeis.eis_po_xxwc_isr_util_pkg.get_isr_item_cost ( msi.inventory_item_id, msi.organization_id), list_price_per_unit) item_cost,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_item_cost ( msi.inventory_item_id, msi.organization_id) bpa_cost,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_bpa_doc ( msi.inventory_item_id, msi.organization_id) bpa,
    xxeis.eis_po_xxwc_isr_util_pkg.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)QOH,
    --XXEIS.eis_po_xxwc_isr_util_pkg.GET_ONHAND_INV ( MSI.INVENTORY_ITEM_ID, MSI.ORGANIZATION_ID) QOH,
    NVL(XXEIS.eis_po_xxwc_isr_util_pkg.GET_SUPPLY_QTY(MSI.ORGANIZATION_ID, NULL,MSI.INVENTORY_ITEM_ID,MSI.postprocessing_lead_time,'WC STD',-1,1,sysdate,1,1,'Y',2,1,1,'Y'),0) - NVL(xxeis.eis_po_xxwc_isr_util_pkg.get_isr_open_req_qty ( msi.inventory_item_id, msi.organization_id,'BOTH'),0) ON_ORD,
    --XXEIS.eis_po_xxwc_isr_util_pkg.GET_ISR_OPEN_PO_QTY ( MSI.INVENTORY_ITEM_ID, MSI.ORGANIZATION_ID) ON_ORD
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_avail_qty ( msi.inventory_item_id, msi.organization_id) avail,
    --APPS.INV_CONSIGNED_VALIDATIONS.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)avail,
    ( xxeis.eis_po_xxwc_isr_util_pkg.get_isr_avail_qty ( msi.inventory_item_id, msi.organization_id) * NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)) avail_d,
    xxeis.eis_po_xxwc_isr_util_pkg.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) bin_loc,
    NULL mc,
    msi.purchasing_enabled_flag fi,
    NULL freeze_date,
    msi.attribute21 res,
    NULL thirteen_wk_avg_inv,
    NULL thirteen_wk_an_cogs,
    NULL turns,
    ppf.full_name buyer,
    shelf_life_days ts,
    msi.attribute20 amu,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_ss_cnt ( msi.inventory_item_id, msi.organization_id) so,
    NULL mf_flag,
    msi.unit_weight wt,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_ss (msi.inventory_item_id, msi.organization_id) ss,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_sourcing_rule ( msi.inventory_item_id, msi.organization_id) sourcing_rule,
    msi.fixed_lot_multiplier fml,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_open_req_qty ( msi.inventory_item_id, msi.organization_id,'VENDOR') open_req,
    msi.inventory_item_id,
    msi.organization_id,
    ood.set_of_books_id,
    MSI.ATTRIBUTE30 CLT,
    -- xxeis.eis_po_xxwc_isr_util_pkg.GET_INT_REQ_SO_QTY(msi.inventory_item_id, msi.organization_id)avail2
    --0 avail2,
    (xxeis.eis_po_xxwc_isr_util_pkg.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)                           - XXEIS.eis_po_xxwc_isr_util_pkg.GET_DEMAND_QTY( MSI.ORGANIZATION_ID,NULL,1,MSI.INVENTORY_ITEM_ID,sysdate,2,1,1,1,'Y'))avail2,
    XXEIS.eis_po_xxwc_isr_util_pkg.GET_SUPPLY_QTY(MSI.ORGANIZATION_ID, NULL,MSI.INVENTORY_ITEM_ID,MSI.postprocessing_lead_time,'WC STD',-1,1,sysdate,1,1,'Y',2,1,1,'Y') supply,
    XXEIS.eis_po_xxwc_isr_util_pkg.GET_DEMAND_QTY( MSI.ORGANIZATION_ID,NULL,1,MSI.INVENTORY_ITEM_ID,sysdate,2,1,1,1,'Y') demand,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_open_req_qty (msi.inventory_item_id, msi.organization_id,'INVENTORY') int_req,
    xxeis.eis_po_xxwc_isr_util_pkg.get_isr_open_req_qty (msi.inventory_item_id, msi.organization_id,'DIRECT') dir_req
  FROM mtl_system_items_kfv msi,
    -- Mtl_Category_Sets True_Supplier_Cat_Set,
    mtl_category_sets sales_velocity_cat_set,
    org_organization_definitions ood,
    mfg_lookups mrp_planning_code,
    mfg_lookups item_source_type,
    --  Mtl_Item_Locations mil,
    per_people_f ppf,
    mtl_parameters mtp,
    mfg_lookups sfty_stk
  WHERE msi.organization_id = ood.organization_id(+)
    -- And True_Supplier_Cat_Set.Structure_Id    = 50448
    --  and true_supplier_cat_set.category_set_id = 1100000082
  AND sales_velocity_cat_set.structure_id      = 50410
  AND sales_velocity_cat_set.category_set_name = 'Sales Velocity' --1100000045
  AND msi.buyer_id                             = ppf.person_id(+)
    --AND Mil.Inventory_Item_Id (+)               = Msi.Inventory_Item_Id
    -- AND Mil.Organization_Id (+)                 = Msi.Organization_Id
    -- AND mil.segment1                          like  '1-%'
  AND mrp_planning_code.lookup_type = 'MTL_MATERIAL_PLANNING'
  AND mrp_planning_code.lookup_code = msi.inventory_planning_code
  AND item_source_type.lookup_type  = 'MTL_SOURCE_TYPES'
  AND item_source_type.lookup_code  = msi.source_type
  AND msi.organization_id           = mtp.organization_id
  AND sfty_stk.lookup_type          = 'MTL_SAFETY_STOCK_TYPE'
  AND SFTY_STK.LOOKUP_CODE          = MSI.MRP_SAFETY_STOCK_CODE;
/

CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_PO_ISR_V" ("ORG", "PRE", "ITEM_NUMBER", "VENDOR_NUM", "VENDOR_NAME", "SOURCE", "ST", "DESCRIPTION", "CAT", "PPLT", "PLT", "UOM", "CL", "STK_FLAG", "PM", "MINN", "MAXN", "AMU", "MF_FLAG", "HIT6_SALES", "AVER_COST", "ITEM_COST", "BPA_COST", "BPA", "QOH", "AVAILABLE", "AVAILABLEDOLLAR", "JAN_SALES", "FEB_SALES", "MAR_SALES", "APR_SALES", "MAY_SALES", "JUNE_SALES", "JUL_SALES", "AUG_SALES", "SEP_SALES", "OCT_SALES", "NOV_SALES", "DEC_SALES", "HIT4_SALES", "ONE_SALES", "SIX_SALES", "TWELVE_SALES", "BIN_LOC", "MC", "FI_FLAG", "FREEZE_DATE", "RES", "THIRTEEN_WK_AVG_INV", "THIRTEEN_WK_AN_COGS", "TURNS", "BUYER", "TS", "SO", "INVENTORY_ITEM_ID", "ORGANIZATION_ID", "SET_OF_BOOKS_ID", "ORG_NAME", "DISTRICT", "REGION", "ON_ORD", "INV_CAT_SEG1", "WT", "SS", "FML", "OPEN_REQ", "SOURCING_RULE", "CLT", "COMMON_OUTPUT_ID", "PROCESS_ID", "AVAIL2", "INT_REQ", "DIR_REQ","DEMAND")
AS
  SELECT "ORG",
    "PRE",
    "ITEM_NUMBER",
    "VENDOR_NUM",
    "VENDOR_NAME",
    "SOURCE",
    "ST",
    "DESCRIPTION",
    "CAT",
    "PPLT",
    "PLT",
    "UOM",
    "CL",
    "STK_FLAG",
    "PM",
    "MINN",
    "MAXN",
    "AMU",
    "MF_FLAG",
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(HIT6_STORE_SALES,0)+NVL(HIT6_OTHER_INV_SALES,0)),NVL(HIT6_STORE_SALES,0)) hit6_sales,
    "AVER_COST",
    "ITEM_COST",
    "BPA_COST",
    "BPA",
    "QOH",
    "AVAILABLE",
    "AVAILABLEDOLLAR",
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(JAN_STORE_SALE,0)   +NVL(JAN_OTHER_INV_SALE,0)),NVL(JAN_STORE_SALE,0)) jan_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(FEB_STORE_SALE,0)   +NVL(FEB_OTHER_INV_SALE,0)),NVL(FEB_STORE_SALE,0)) feb_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(MAR_STORE_SALE,0)   +NVL(MAR_OTHER_INV_SALE,0)),NVL(MAR_STORE_SALE,0)) mar_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(APR_STORE_SALE,0)   +NVL(APR_OTHER_INV_SALE,0)),NVL(APR_STORE_SALE,0)) apr_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(MAY_STORE_SALE,0)   +NVL(MAY_OTHER_INV_SALE,0)),NVL(MAY_STORE_SALE,0)) may_sales,
    DECODE(XXEIS.eis_po_xxwc_isr_util_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(JUN_STORE_SALE,0)   +NVL(JUN_OTHER_INV_SALE,0)),NVL(JUN_STORE_SALE,0)) JUNE_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(JUL_STORE_SALE,0)   +NVL(JUL_OTHER_INV_SALE,0)),NVL(JUL_STORE_SALE,0)) JUL_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(AUG_STORE_SALE,0)   +NVL(AUG_OTHER_INV_SALE,0)),NVL(AUG_STORE_SALE,0)) AUG_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(SEP_STORE_SALE,0)   +NVL(SEP_OTHER_INV_SALE,0)),NVL(SEP_STORE_SALE,0)) SEP_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(OCT_STORE_SALE,0)   +NVL(OCT_OTHER_INV_SALE,0)),NVL(OCT_STORE_SALE,0)) OCT_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(NOV_STORE_SALE,0)   +NVL(NOV_OTHER_INV_SALE,0)),NVL(NOV_STORE_SALE,0)) NOV_SALES,
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(DEC_STORE_SALE,0)   +NVL(DEC_OTHER_INV_SALE,0)),NVL(DEC_STORE_SALE,0)) dec_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(HIT4_STORE_SALES,0) +NVL(HIT4_OTHER_INV_SALES,0)),NVL(HIT4_STORE_SALES,0)) hit4_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(ONE_STORE_SALE,0)   +NVL(ONE_OTHER_INV_SALE,0)),NVL(ONE_STORE_SALE,0)) one_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(SIX_STORE_SALE,0)   +NVL(SIX_OTHER_INV_SALE,0)),NVL(SIX_STORE_SALE,0)) six_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(TWELVE_STORE_SALE,0)+NVL(TWELVE_OTHER_INV_SALE,0)),NVL(TWELVE_STORE_SALE,0)) twelve_sales,
    "BIN_LOC",
    "MC",
    "FI_FLAG",
    "FREEZE_DATE",
    "RES",
    "THIRTEEN_WK_AVG_INV",
    "THIRTEEN_WK_AN_COGS",
    "TURNS",
    "BUYER",
    "TS",
    "SO",
    "INVENTORY_ITEM_ID",
    "ORGANIZATION_ID",
    "SET_OF_BOOKS_ID",
    "ORG_NAME",
    "DISTRICT",
    "REGION",
    "ON_ORD",
    "INV_CAT_SEG1",
    wt,
    ss,
    fml,
    open_req,
    sourcing_rule,
    clt,
    common_output_id,
    process_id,
    avail2,
    int_req,
    dir_req,
    demand
    --  xxeis.eis_rs_common_outputs_s.NEXTVAL common_output_id
  FROM xxeis.EIS_XXWC_PO_ISR_TAB
  WHERE 1 =1;
/
alter table xxeis.EIS_XXWC_PO_ISR_QA_TAB add(demand NUMBER);
/
CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_PO_ISR_RPT_QA_V" ("ORG", "PRE", "ITEM_NUMBER", "VENDOR_NUM", "VENDOR_NAME", "SOURCE", "ST", "DESCRIPTION", "CAT", "PPLT", "PLT", "UOM", "CL", "STK_FLAG", "PM", "MINN", "MAXN", "AMU", "MF_FLAG", "HIT6_SALES", "AVER_COST", "ITEM_COST", "BPA_COST", "BPA", "QOH", "AVAILABLE", "AVAILABLEDOLLAR", "JAN_SALES", "FEB_SALES", "MAR_SALES", "APR_SALES", "MAY_SALES", "JUNE_SALES", "JUL_SALES", "AUG_SALES", "SEP_SALES", "OCT_SALES", "NOV_SALES", "DEC_SALES", "HIT4_SALES", "ONE_SALES", "SIX_SALES", "TWELVE_SALES", "BIN_LOC", "MC", "FI_FLAG", "FREEZE_DATE", "RES", "THIRTEEN_WK_AVG_INV", "THIRTEEN_WK_AN_COGS", "TURNS", "BUYER", "TS", "SO", "INVENTORY_ITEM_ID", "ORGANIZATION_ID", "SET_OF_BOOKS_ID", "ON_ORD", "WT", "SS", "FML", "OPEN_REQ", "ORG_NAME", "DISTRICT", "REGION", "SOURCING_RULE", "CLT", "COMMON_OUTPUT_ID", "PROCESS_ID", "AVAIL2", "INT_REQ", "DIR_REQ","DEMAND")
AS
  SELECT Varchar2_Col1 "ORG",
    Varchar2_Col2 "PRE",
    Varchar2_Col3 "ITEM_NUMBER",
    Varchar2_Col4 "VENDOR_NUM",
    Varchar2_Col5 "VENDOR_NAME",
    Varchar2_Col6 "SOURCE",
    Varchar2_Col7 "ST",
    Varchar2_Col8 "DESCRIPTION",
    Varchar2_Col9 "CAT",
    Varchar2_Col10 "PPLT",
    Varchar2_Col11 "PLT",
    Varchar2_Col12 "UOM",
    Varchar2_Col13 "CL",
    Varchar2_Col14 "STK_FLAG",
    Varchar2_Col15 "PM",
    Varchar2_Col16 "MINN",
    Varchar2_Col17 "MAXN",
    Varchar2_Col18 "AMU",
    Varchar2_Col19 "MF_FLAG",
    Number_Col1 Hit6_Sales,
    Number_Col2 "AVER_COST",
    Number_Col3 "ITEM_COST",
    Number_Col4 "BPA_COST",
    Varchar2_Col20 "BPA",
    Number_Col5 "QOH",
    Number_Col6 "AVAILABLE",
    Number_Col7 "AVAILABLEDOLLAR",
    Number_Col8 Jan_Sales,
    Number_Col9 Feb_Sales,
    Number_Col10 Mar_Sales,
    Number_Col11 Apr_Sales,
    Number_Col12 May_Sales,
    Number_Col13 June_Sales,
    Number_Col14 Jul_Sales,
    Number_Col15 Aug_Sales,
    Number_Col16 Sep_Sales,
    Number_Col17 Oct_Sales,
    Number_Col18 Nov_Sales,
    Number_Col19 Dec_Sales,
    Number_Col20 Hit4_Sales,
    Number_Col21 One_Sales,
    Number_Col22 Six_Sales,
    Number_Col23 Twelve_Sales,
    Varchar2_Col21 "BIN_LOC",
    Varchar2_Col22 "MC",
    Varchar2_Col23 "FI_FLAG",
    Date_Col1 "FREEZE_DATE",
    Varchar2_Col24 "RES",
    Number_Col24 "THIRTEEN_WK_AVG_INV",
    Number_Col25 "THIRTEEN_WK_AN_COGS",
    Number_Col26 "TURNS",
    Varchar2_Col25 "BUYER",
    Varchar2_Col26 "TS",
    Number_Col27 "SO",
    Number_Col28 "INVENTORY_ITEM_ID",
    Number_Col29 "ORGANIZATION_ID",
    Number_Col30 "SET_OF_BOOKS_ID",
    number_col31 "ON_ORD",
    Number_Col32 "WT",
    Number_Col33 "SS",
    Number_Col34 "FML",
    Number_Col35 "OPEN_REQ",
    Varchar2_Col27 "ORG_NAME",
    Varchar2_Col28 "DISTRICT",
    varchar2_col29 "REGION",
    Varchar2_Col30 "SOURCING_RULE",
    Number_Col36 "CLT",
    common_output_id,
    process_id,
    Number_Col37 "AVAIL2",
    Number_Col38 "INT_REQ",
    Number_Col39 "DIR_REQ",
    Number_Col40 "DEMAND"
  FROM XXEIS.EIS_RS_COMMON_OUTPUTS;
/

CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_PO_ISR_TAB_QA_V" ("ORG", "ORGANIZATION_NAME", "DISTRICT", "REGION", "PRE", "ITEM_NUMBER", "VENDOR_NAME", "VENDOR_NUMBER", "SOURCE", "ST", "DESCRIPTION", "CAT_CLASS", "INV_CAT_SEG1", "PPLT", "PLT", "UOM", "CL", "STK", "PM", "MIN", "MAX", "HIT_6", "AVER_COST", "ITEM_COST", "BPA_COST", "BPA", "QOH", "ON_ORD", "AVAIL", "AVAIL_D", "BIN_LOC", "MC", "FI", "FREEZE_DATE", "RES", "THIRTEEN_WK_AVG_INV", "THIRTEEN_WK_AN_COGS", "TURNS", "BUYER", "TS", "AMU", "SO", "MF_FLAG", "WT", "SS", "SOURCING_RULE", "FML", "OPEN_REQ", "INVENTORY_ITEM_ID", "ORGANIZATION_ID", "SET_OF_BOOKS_ID", "CLT", "AVAIL2", "SUPPLY", "DEMAND", "INT_REQ", "DIR_REQ")
AS
  SELECT ood.organization_code org,
    ood.organization_name organization_name,
    mtp.attribute8 district,
    mtp.attribute9 region,
    SUBSTR (msi.segment1, 1, 3) pre,
    msi.segment1 item_number,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_vendor_name ( msi.inventory_item_id, msi.organization_id) vendor_name,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_vendor_number ( msi.inventory_item_id, msi.organization_id) vendor_number,
    CASE
      WHEN item_source_type.meaning = 'Supplier'
      THEN xxeis.eis_po_xxwc_isr_util_qa_pkg.get_vendor_number ( msi.inventory_item_id, msi.organization_id)
      WHEN item_source_type.meaning = 'Inventory'
      THEN
        (SELECT organization_code
        FROM org_organization_definitions source_org
        WHERE source_org.organization_id = msi.source_organization_id
        )
      ELSE NULL
    END source,
    CASE
      WHEN item_source_type.meaning = 'Supplier'
      THEN 'S'
      WHEN item_source_type.meaning = 'Inventory'
      THEN 'I'
      ELSE NULL
    END st,
    msi.description description,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_inv_cat_class ( msi.inventory_item_id, msi.organization_id) cat_class,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_inv_cat_seg1 ( msi.inventory_item_id, msi.organization_id) inv_cat_seg1,
    preprocessing_lead_time pplt,
    msi.full_lead_time plt,
    msi.primary_uom_code uom,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) cl,
    CASE
      WHEN (xxeis.eis_po_xxwc_isr_util_qa_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) IN ('1', '2', '3', '4', '5', '6', '7', '8', '9', 'C','B'))
        --AND ITEM_TYPE                                                                                           ='STOCK ITEM')
      THEN 'Y'
      WHEN ( xxeis.eis_po_xxwc_isr_util_qa_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) IN ('E')
      AND (min_minmax_quantity                                                                                      = 0
      AND max_minmax_quantity                                                                                       = 0))
      THEN 'N'
      WHEN ( xxeis.eis_po_xxwc_isr_util_qa_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) IN ('E')
      AND (min_minmax_quantity                                                                                      > 0
      AND max_minmax_quantity                                                                                       > 0))
      THEN 'Y'
      WHEN (xxeis.eis_po_xxwc_isr_util_qa_pkg.get_inv_vel_cat_class ( msi.inventory_item_id, msi.organization_id) IN ('N', 'Z'))
        --AND ITEM_TYPE                                                                                           ='NON-STOCK')
      THEN 'N'
      ELSE 'N'
    END stk,
    mrp_planning_code.meaning pm,
    min_minmax_quantity MIN,
    max_minmax_quantity MAX,
    NULL hit_6,
    NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0) aver_cost,
    NVL ( xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_item_cost ( msi.inventory_item_id, msi.organization_id), list_price_per_unit) item_cost,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_item_cost ( msi.inventory_item_id, msi.organization_id) bpa_cost,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_bpa_doc ( msi.inventory_item_id, msi.organization_id) bpa,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)QOH,
    --XXEIS.EIS_PO_XXWC_ISR_UTIL_QA_PKG.GET_ONHAND_INV ( MSI.INVENTORY_ITEM_ID, MSI.ORGANIZATION_ID) QOH,
    NVL(XXEIS.EIS_PO_XXWC_ISR_UTIL_QA_PKG.GET_SUPPLY_QTY(MSI.ORGANIZATION_ID, NULL,MSI.INVENTORY_ITEM_ID,MSI.postprocessing_lead_time,'WC STD',-1,1,sysdate,1,1,'Y',2,1,1,'Y'),0) - NVL(xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_open_req_qty ( msi.inventory_item_id, msi.organization_id,'BOTH'),0) ON_ORD,
    --XXEIS.EIS_PO_XXWC_ISR_UTIL_QA_PKG.GET_ISR_OPEN_PO_QTY ( MSI.INVENTORY_ITEM_ID, MSI.ORGANIZATION_ID) ON_ORD
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_avail_qty ( msi.inventory_item_id, msi.organization_id) avail,
    --APPS.INV_CONSIGNED_VALIDATIONS.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)avail,
    ( xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_avail_qty ( msi.inventory_item_id, msi.organization_id) * NVL ( apps.cst_cost_api.get_item_cost (1, msi.inventory_item_id, msi.organization_id), 0)) avail_d,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_primary_bin_loc ( msi.inventory_item_id, msi.organization_id) bin_loc,
    NULL mc,
    msi.purchasing_enabled_flag fi,
    NULL freeze_date,
    msi.attribute21 res,
    NULL thirteen_wk_avg_inv,
    NULL thirteen_wk_an_cogs,
    NULL turns,
    ppf.full_name buyer,
    shelf_life_days ts,
    msi.attribute20 amu,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_ss_cnt ( msi.inventory_item_id, msi.organization_id) so,
    NULL mf_flag,
    msi.unit_weight wt,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_ss (msi.inventory_item_id, msi.organization_id) ss,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_sourcing_rule ( msi.inventory_item_id, msi.organization_id) sourcing_rule,
    msi.fixed_lot_multiplier fml,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_open_req_qty ( msi.inventory_item_id, msi.organization_id,'VENDOR') open_req,
    msi.inventory_item_id,
    msi.organization_id,
    ood.set_of_books_id,
    MSI.ATTRIBUTE30 CLT,
    -- xxeis.eis_po_xxwc_isr_util_qa_pkg.GET_INT_REQ_SO_QTY(msi.inventory_item_id, msi.organization_id)avail2
    --0 avail2,
    (xxeis.eis_po_xxwc_isr_util_qa_pkg.GET_PLANNING_QUANTITY(2,1,MSI.ORGANIZATION_ID,NULL,MSI.INVENTORY_ITEM_ID)                           - XXEIS.EIS_PO_XXWC_ISR_UTIL_QA_PKG.GET_DEMAND_QTY( MSI.ORGANIZATION_ID,NULL,1,MSI.INVENTORY_ITEM_ID,sysdate,2,1,1,1,'Y'))avail2,
    XXEIS.EIS_PO_XXWC_ISR_UTIL_QA_PKG.GET_SUPPLY_QTY(MSI.ORGANIZATION_ID, NULL,MSI.INVENTORY_ITEM_ID,MSI.postprocessing_lead_time,'WC STD',-1,1,sysdate,1,1,'Y',2,1,1,'Y') supply,
    XXEIS.EIS_PO_XXWC_ISR_UTIL_QA_PKG.GET_DEMAND_QTY( MSI.ORGANIZATION_ID,NULL,1,MSI.INVENTORY_ITEM_ID,sysdate,2,1,1,1,'Y') demand,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_open_req_qty (msi.inventory_item_id, msi.organization_id,'INVENTORY') int_req,
    xxeis.eis_po_xxwc_isr_util_qa_pkg.get_isr_open_req_qty (msi.inventory_item_id, msi.organization_id,'DIRECT') dir_req
  FROM mtl_system_items_kfv msi,
    -- Mtl_Category_Sets True_Supplier_Cat_Set,
    mtl_category_sets sales_velocity_cat_set,
    org_organization_definitions ood,
    mfg_lookups mrp_planning_code,
    mfg_lookups item_source_type,
    --  Mtl_Item_Locations mil,
    per_people_f ppf,
    mtl_parameters mtp,
    mfg_lookups sfty_stk
  WHERE msi.organization_id = ood.organization_id(+)
    -- And True_Supplier_Cat_Set.Structure_Id    = 50448
    --  and true_supplier_cat_set.category_set_id = 1100000082
  AND sales_velocity_cat_set.structure_id      = 50410
  AND sales_velocity_cat_set.category_set_name = 'Sales Velocity' --1100000045
  AND msi.buyer_id                             = ppf.person_id(+)
    --AND Mil.Inventory_Item_Id (+)               = Msi.Inventory_Item_Id
    -- AND Mil.Organization_Id (+)                 = Msi.Organization_Id
    -- AND mil.segment1                          like  '1-%'
  AND mrp_planning_code.lookup_type = 'MTL_MATERIAL_PLANNING'
  AND mrp_planning_code.lookup_code = msi.inventory_planning_code
  AND item_source_type.lookup_type  = 'MTL_SOURCE_TYPES'
  AND item_source_type.lookup_code  = msi.source_type
  AND msi.organization_id           = mtp.organization_id
  AND sfty_stk.lookup_type          = 'MTL_SAFETY_STOCK_TYPE'
  AND SFTY_STK.LOOKUP_CODE          = MSI.MRP_SAFETY_STOCK_CODE;
/

CREATE OR REPLACE FORCE VIEW "XXEIS"."EIS_XXWC_PO_ISR_QA_V" ("ORG", "PRE", "ITEM_NUMBER", "VENDOR_NUM", "VENDOR_NAME", "SOURCE", "ST", "DESCRIPTION", "CAT", "PPLT", "PLT", "UOM", "CL", "STK_FLAG", "PM", "MINN", "MAXN", "AMU", "MF_FLAG", "HIT6_SALES", "AVER_COST", "ITEM_COST", "BPA_COST", "BPA", "QOH", "AVAILABLE", "AVAILABLEDOLLAR", "JAN_SALES", "FEB_SALES", "MAR_SALES", "APR_SALES", "MAY_SALES", "JUNE_SALES", "JUL_SALES", "AUG_SALES", "SEP_SALES", "OCT_SALES", "NOV_SALES", "DEC_SALES", "HIT4_SALES", "ONE_SALES", "SIX_SALES", "TWELVE_SALES", "BIN_LOC", "MC", "FI_FLAG", "FREEZE_DATE", "RES", "THIRTEEN_WK_AVG_INV", "THIRTEEN_WK_AN_COGS", "TURNS", "BUYER", "TS", "SO", "INVENTORY_ITEM_ID", "ORGANIZATION_ID", "SET_OF_BOOKS_ID", "ORG_NAME", "DISTRICT", "REGION", "ON_ORD", "INV_CAT_SEG1", "WT", "SS", "FML", "OPEN_REQ", "SOURCING_RULE", "CLT", "COMMON_OUTPUT_ID", "PROCESS_ID", "AVAIL2", "INT_REQ", "DIR_REQ","DEMAND")
AS
  SELECT "ORG",
    "PRE",
    "ITEM_NUMBER",
    "VENDOR_NUM",
    "VENDOR_NAME",
    "SOURCE",
    "ST",
    "DESCRIPTION",
    "CAT",
    "PPLT",
    "PLT",
    "UOM",
    "CL",
    "STK_FLAG",
    "PM",
    "MINN",
    "MAXN",
    "AMU",
    "MF_FLAG",
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(HIT6_STORE_SALES,0)+NVL(HIT6_OTHER_INV_SALES,0)),NVL(HIT6_STORE_SALES,0)) hit6_sales,
    "AVER_COST",
    "ITEM_COST",
    "BPA_COST",
    "BPA",
    "QOH",
    "AVAILABLE",
    "AVAILABLEDOLLAR",
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(JAN_STORE_SALE,0)   +NVL(JAN_OTHER_INV_SALE,0)),NVL(JAN_STORE_SALE,0)) jan_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(FEB_STORE_SALE,0)   +NVL(FEB_OTHER_INV_SALE,0)),NVL(FEB_STORE_SALE,0)) feb_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(MAR_STORE_SALE,0)   +NVL(MAR_OTHER_INV_SALE,0)),NVL(MAR_STORE_SALE,0)) mar_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(APR_STORE_SALE,0)   +NVL(APR_OTHER_INV_SALE,0)),NVL(APR_STORE_SALE,0)) apr_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(MAY_STORE_SALE,0)   +NVL(MAY_OTHER_INV_SALE,0)),NVL(MAY_STORE_SALE,0)) may_sales,
    DECODE(XXEIS.eis_po_xxwc_isr_util_qa_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(JUN_STORE_SALE,0)   +NVL(JUN_OTHER_INV_SALE,0)),NVL(JUN_STORE_SALE,0)) JUNE_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_qa_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(JUL_STORE_SALE,0)   +NVL(JUL_OTHER_INV_SALE,0)),NVL(JUL_STORE_SALE,0)) JUL_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_qa_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(AUG_STORE_SALE,0)   +NVL(AUG_OTHER_INV_SALE,0)),NVL(AUG_STORE_SALE,0)) AUG_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_qa_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(SEP_STORE_SALE,0)   +NVL(SEP_OTHER_INV_SALE,0)),NVL(SEP_STORE_SALE,0)) SEP_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_qa_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(OCT_STORE_SALE,0)   +NVL(OCT_OTHER_INV_SALE,0)),NVL(OCT_STORE_SALE,0)) OCT_SALES,
    DECODE(XXEIS.eis_po_xxwc_isr_util_qa_pkg.GET_ISR_RPT_DC_MOD_SUB,'Yes',(NVL(NOV_STORE_SALE,0)   +NVL(NOV_OTHER_INV_SALE,0)),NVL(NOV_STORE_SALE,0)) NOV_SALES,
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(DEC_STORE_SALE,0)   +NVL(DEC_OTHER_INV_SALE,0)),NVL(DEC_STORE_SALE,0)) dec_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(HIT4_STORE_SALES,0) +NVL(HIT4_OTHER_INV_SALES,0)),NVL(HIT4_STORE_SALES,0)) hit4_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(ONE_STORE_SALE,0)   +NVL(ONE_OTHER_INV_SALE,0)),NVL(ONE_STORE_SALE,0)) one_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(SIX_STORE_SALE,0)   +NVL(SIX_OTHER_INV_SALE,0)),NVL(SIX_STORE_SALE,0)) six_sales,
    DECODE(xxeis.eis_po_xxwc_isr_util_qa_pkg.Get_isr_rpt_dc_mod_sub,'Yes',(NVL(TWELVE_STORE_SALE,0)+NVL(TWELVE_OTHER_INV_SALE,0)),NVL(TWELVE_STORE_SALE,0)) twelve_sales,
    "BIN_LOC",
    "MC",
    "FI_FLAG",
    "FREEZE_DATE",
    "RES",
    "THIRTEEN_WK_AVG_INV",
    "THIRTEEN_WK_AN_COGS",
    "TURNS",
    "BUYER",
    "TS",
    "SO",
    "INVENTORY_ITEM_ID",
    "ORGANIZATION_ID",
    "SET_OF_BOOKS_ID",
    "ORG_NAME",
    "DISTRICT",
    "REGION",
    "ON_ORD",
    "INV_CAT_SEG1",
    wt,
    ss,
    fml,
    open_req,
    sourcing_rule,
    clt,
    common_output_id,
    process_id,
    avail2,
    int_req,
    dir_req,
    demand
    --  xxeis.eis_rs_common_outputs_s.NEXTVAL common_output_id
  FROM xxeis.EIS_XXWC_PO_ISR_QA_TAB
  WHERE 1 =1;
/
