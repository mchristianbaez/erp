--Report Name            : Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
--Import Instructions    : Run this file in XXEIS user from SQL*Plus or any tool like Toad, SQL Navigator. 
prompt Creating View Data for Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
set scan off define off
DECLARE
BEGIN 
--Inserting View EIS_XXWC_AR_AGING_RECLASS_V
xxeis.eis_rs_ins.v( 'EIS_XXWC_AR_AGING_RECLASS_V',222,'','','','','SA059956','XXEIS','Eis Xxwc Ar Aging Reclass V','EXAARV','','');
--Delete View Columns for EIS_XXWC_AR_AGING_RECLASS_V
xxeis.eis_rs_utility.delete_view_rows('EIS_XXWC_AR_AGING_RECLASS_V',222,FALSE);
--Inserting View Columns for EIS_XXWC_AR_AGING_RECLASS_V
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CREATION_DATE',222,'Creation Date','CREATION_DATE','','','','SA059956','DATE','','','Creation Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','SEGMENT2',222,'Segment2','SEGMENT2','','','','SA059956','VARCHAR2','','','Segment2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_ADDRESS',222,'Trx Address','TRX_ADDRESS','','','','SA059956','VARCHAR2','','','Trx Address','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_TO_COUNTRY',222,'Trx Bill To Country','TRX_BILL_TO_COUNTRY','','','','SA059956','VARCHAR2','','','Trx Bill To Country','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_TO_ZIP_CODE',222,'Trx Bill To Zip Code','TRX_BILL_TO_ZIP_CODE','','','','SA059956','VARCHAR2','','','Trx Bill To Zip Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_TO_CITY_PROV',222,'Trx Bill To City Prov','TRX_BILL_TO_CITY_PROV','','','','SA059956','VARCHAR2','','','Trx Bill To City Prov','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_TO_CITY',222,'Trx Bill To City','TRX_BILL_TO_CITY','','','','SA059956','VARCHAR2','','','Trx Bill To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_TO_ADDRESS4',222,'Trx Bill To Address4','TRX_BILL_TO_ADDRESS4','','','','SA059956','VARCHAR2','','','Trx Bill To Address4','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_TO_ADDRESS3',222,'Trx Bill To Address3','TRX_BILL_TO_ADDRESS3','','','','SA059956','VARCHAR2','','','Trx Bill To Address3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_TO_ADDRESS2',222,'Trx Bill To Address2','TRX_BILL_TO_ADDRESS2','','','','SA059956','VARCHAR2','','','Trx Bill To Address2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_TO_ADDRESS1',222,'Trx Bill To Address1','TRX_BILL_TO_ADDRESS1','','','','SA059956','VARCHAR2','','','Trx Bill To Address1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TERRITORY',222,'Territory','TERRITORY','','','','SA059956','VARCHAR2','','','Territory','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','ACCOUNT_STATUS',222,'Account Status','ACCOUNT_STATUS','','','','SA059956','VARCHAR2','','','Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','AMOUNT_PAID',222,'Amount Paid','AMOUNT_PAID','','','','SA059956','NUMBER','','','Amount Paid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','AVERAGE_DAYS',222,'Average Days','AVERAGE_DAYS','','','','SA059956','NUMBER','','','Average Days','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','LARGEST_BALANCE',222,'Largest Balance','LARGEST_BALANCE','','','','SA059956','NUMBER','','','Largest Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','IN_PROCESS',222,'In Process','IN_PROCESS','','','','SA059956','VARCHAR2','','','In Process','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','PHONE_NUMBER',222,'Phone Number','PHONE_NUMBER','','','','SA059956','VARCHAR2','','','Phone Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TERMS',222,'Terms','TERMS','','','','SA059956','VARCHAR2','','','Terms','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TWELVE_MONTHS_SALES',222,'Twelve Months Sales','TWELVE_MONTHS_SALES','','','','SA059956','NUMBER','','','Twelve Months Sales','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CREDIT_LIMIT',222,'Credit Limit','CREDIT_LIMIT','','','','SA059956','NUMBER','','','Credit Limit','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','SALESREP_NUMBER',222,'Salesrep Number','SALESREP_NUMBER','','','','SA059956','VARCHAR2','','','Salesrep Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','PMT_STATUS',222,'Pmt Status','PMT_STATUS','','','','SA059956','VARCHAR2','','','Pmt Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CUSTOMER_PO_NUMBER',222,'Customer Po Number','CUSTOMER_PO_NUMBER','','','','SA059956','VARCHAR2','','','Customer Po Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_SITE_USE_ID',222,'Trx Bill Site Use Id','TRX_BILL_SITE_USE_ID','','','','SA059956','NUMBER','','','Trx Bill Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_CUSTOMER_ID',222,'Trx Customer Id','TRX_CUSTOMER_ID','','','','SA059956','NUMBER','','','Trx Customer Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','SEND_CREDIT_BAL_FLAG',222,'Send Credit Bal Flag','SEND_CREDIT_BAL_FLAG','','','','SA059956','VARCHAR2','','','Send Credit Bal Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','SEND_STATEMENT_FLAG',222,'Send Statement Flag','SEND_STATEMENT_FLAG','','','','SA059956','VARCHAR2','','','Send Statement Flag','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','STMT_BY_JOB',222,'Stmt By Job','STMT_BY_JOB','','','','SA059956','VARCHAR2','','','Stmt By Job','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','REMIT_TO_ADDRESS_CODE',222,'Remit To Address Code','REMIT_TO_ADDRESS_CODE','','','','SA059956','VARCHAR2','','','Remit To Address Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CUST_PAYMENT_TERM',222,'Cust Payment Term','CUST_PAYMENT_TERM','','','','SA059956','VARCHAR2','','','Cust Payment Term','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','ACCOUNT_BALANCE',222,'Account Balance','ACCOUNT_BALANCE','','','','SA059956','NUMBER','','','Account Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','ACCOUNT_MANAGER',222,'Account Manager','ACCOUNT_MANAGER','','','','SA059956','VARCHAR2','','','Account Manager','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CREDIT_ANALYST',222,'Credit Analyst','CREDIT_ANALYST','','','','SA059956','VARCHAR2','','','Credit Analyst','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','COLLECTOR_NAME',222,'Collector Name','COLLECTOR_NAME','','','','SA059956','VARCHAR2','','','Collector Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CUSTOMER_PROFILE_CLASS',222,'Customer Profile Class','CUSTOMER_PROFILE_CLASS','','','','SA059956','VARCHAR2','','','Customer Profile Class','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','SITE_CREDIT_HOLD',222,'Site Credit Hold','SITE_CREDIT_HOLD','','','','SA059956','VARCHAR2','','','Site Credit Hold','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CUSTOMER_ACCOUNT_STATUS',222,'Customer Account Status','CUSTOMER_ACCOUNT_STATUS','','','','SA059956','VARCHAR2','','','Customer Account Status','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','LAST_PAYMENT_DATE',222,'Last Payment Date','LAST_PAYMENT_DATE','','','','SA059956','DATE','','','Last Payment Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','OVER_THREE_SIXTY_DAYS_BAL',222,'Over Three Sixty Days Bal','OVER_THREE_SIXTY_DAYS_BAL','','','','SA059956','NUMBER','','','Over Three Sixty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','THREE_SIXTY_DAYS_BAL',222,'Three Sixty Days Bal','THREE_SIXTY_DAYS_BAL','','','','SA059956','NUMBER','','','Three Sixty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','ONE_EIGHTY_DAYS_BAL',222,'One Eighty Days Bal','ONE_EIGHTY_DAYS_BAL','','','','SA059956','NUMBER','','','One Eighty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','NINETY_DAYS_BAL',222,'Ninety Days Bal','NINETY_DAYS_BAL','','','','SA059956','NUMBER','','','Ninety Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','SIXTY_DAYS_BAL',222,'Sixty Days Bal','SIXTY_DAYS_BAL','','','','SA059956','NUMBER','','','Sixty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','THIRTY_DAYS_BAL',222,'Thirty Days Bal','THIRTY_DAYS_BAL','','','','SA059956','NUMBER','','','Thirty Days Bal','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CURRENT_BALANCE',222,'Current Balance','CURRENT_BALANCE','','','','SA059956','NUMBER','','','Current Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','AGE',222,'Age','AGE','','','','SA059956','NUMBER','','','Age','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRANSACTION_REMAINING_BALANCE',222,'Transaction Remaining Balance','TRANSACTION_REMAINING_BALANCE','','','','SA059956','NUMBER','','','Transaction Remaining Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRANSATION_BALANCE',222,'Transation Balance','TRANSATION_BALANCE','','','','SA059956','NUMBER','','','Transation Balance','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_TYPE',222,'Trx Type','TRX_TYPE','','','','SA059956','VARCHAR2','','','Trx Type','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','DUE_DATE',222,'Due Date','DUE_DATE','','','','SA059956','DATE','','','Due Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_DATE',222,'Trx Date','TRX_DATE','','','','SA059956','DATE','','','Trx Date','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_NUMBER',222,'Trx Number','TRX_NUMBER','','','','SA059956','VARCHAR2','','','Trx Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BRANCH_LOCATION',222,'Branch Location','BRANCH_LOCATION','','','','SA059956','VARCHAR2','','','Branch Location','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','RECEIPT_NUMBER',222,'Receipt Number','RECEIPT_NUMBER','','','','SA059956','VARCHAR2','','','Receipt Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','INVOICE_NUMBER',222,'Invoice Number','INVOICE_NUMBER','','','','SA059956','VARCHAR2','','','Invoice Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','ACRA_CCID',222,'Acra Ccid','ACRA_CCID','','','','SA059956','NUMBER','','','Acra Ccid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CASH_RECEIPT_ID',222,'Cash Receipt Id','CASH_RECEIPT_ID','','','','SA059956','NUMBER','','','Cash Receipt Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','RCTA_CCID',222,'Rcta Ccid','RCTA_CCID','','','','SA059956','NUMBER','','','Rcta Ccid','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CUSTOMER_TRX_ID',222,'Customer Trx Id','CUSTOMER_TRX_ID','','','','SA059956','NUMBER','','','Customer Trx Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','PAYMENT_SCHEDULE_ID',222,'Payment Schedule Id','PAYMENT_SCHEDULE_ID','','','','SA059956','NUMBER','','','Payment Schedule Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_COUNTRY',222,'Bill To Country','BILL_TO_COUNTRY','','','','SA059956','VARCHAR2','','','Bill To Country','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_ZIP_CODE',222,'Bill To Zip Code','BILL_TO_ZIP_CODE','','','','SA059956','VARCHAR2','','','Bill To Zip Code','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_CITY_PROVINCE',222,'Bill To City Province','BILL_TO_CITY_PROVINCE','','','','SA059956','VARCHAR2','','','Bill To City Province','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_CITY',222,'Bill To City','BILL_TO_CITY','','','','SA059956','VARCHAR2','','','Bill To City','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_ADDRESS4',222,'Bill To Address4','BILL_TO_ADDRESS4','','','','SA059956','VARCHAR2','','','Bill To Address4','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_ADDRESS3',222,'Bill To Address3','BILL_TO_ADDRESS3','','','','SA059956','VARCHAR2','','','Bill To Address3','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_ADDRESS2',222,'Bill To Address2','BILL_TO_ADDRESS2','','','','SA059956','VARCHAR2','','','Bill To Address2','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_ADDRESS1',222,'Bill To Address1','BILL_TO_ADDRESS1','','','','SA059956','VARCHAR2','','','Bill To Address1','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_PARTY_SITE_NAME',222,'Trx Party Site Name','TRX_PARTY_SITE_NAME','','','','SA059956','VARCHAR2','','','Trx Party Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_PARTY_SITE_NUMBER',222,'Trx Party Site Number','TRX_PARTY_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Trx Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','TRX_BILL_TO_SITE_NAME',222,'Trx Bill To Site Name','TRX_BILL_TO_SITE_NAME','','','','SA059956','VARCHAR2','','','Trx Bill To Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_SITE_NAME',222,'Bill To Site Name','BILL_TO_SITE_NAME','','','','SA059956','VARCHAR2','','','Bill To Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_PARTY_SITE_NAME',222,'Bill To Party Site Name','BILL_TO_PARTY_SITE_NAME','','','','SA059956','VARCHAR2','','','Bill To Party Site Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_PRISM_SITE_NUMBER',222,'Bill To Prism Site Number','BILL_TO_PRISM_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Bill To Prism Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_PARTY_SITE_NUMBER',222,'Bill To Party Site Number','BILL_TO_PARTY_SITE_NUMBER','','','','SA059956','VARCHAR2','','','Bill To Party Site Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','BILL_TO_SITE_USE_ID',222,'Bill To Site Use Id','BILL_TO_SITE_USE_ID','','','','SA059956','NUMBER','','','Bill To Site Use Id','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CUSTOMER_NAME',222,'Customer Name','CUSTOMER_NAME','','','','SA059956','VARCHAR2','','','Customer Name','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','PRISM_CUSTOMER_NUMBER',222,'Prism Customer Number','PRISM_CUSTOMER_NUMBER','','','','SA059956','VARCHAR2','','','Prism Customer Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CUSTOMER_ACCOUNT_NUMBER',222,'Customer Account Number','CUSTOMER_ACCOUNT_NUMBER','','','','SA059956','VARCHAR2','','','Customer Account Number','','','');
xxeis.eis_rs_ins.vc( 'EIS_XXWC_AR_AGING_RECLASS_V','CUST_ACCOUNT_ID',222,'Cust Account Id','CUST_ACCOUNT_ID','','','','SA059956','NUMBER','','','Cust Account Id','','','');
--Inserting View Components for EIS_XXWC_AR_AGING_RECLASS_V
--Inserting View Component Joins for EIS_XXWC_AR_AGING_RECLASS_V
END;
/
set scan on define on
prompt Creating Report Data for Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
set scan off define off
DECLARE
BEGIN 
--Deleting Report data - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
xxeis.eis_rs_utility.delete_report_rows( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC' );
--Inserting Report - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
xxeis.eis_rs_ins.r( 222,'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','','Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','','','','SA059956','EIS_XXWC_AR_AGING_RECLASS_V','Y','','','SA059956','','N','White Cap Reports','','CSV,EXCEL,','N');
--Inserting Report Columns - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'ACCOUNT_BALANCE','Account Total Balance','Account Balance','','~~~','default','','30','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'ACCOUNT_MANAGER','Salesrep Name','Account Manager','','','default','','29','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'AGE','Age','Age','','~~~','default','','13','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'BRANCH_LOCATION','Branch Location','Branch Location','','','default','','5','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'COLLECTOR_NAME','Collector','Collector Name','','','default','','24','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'CREATION_DATE','Creation Date','Creation Date','','','default','','32','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'CREDIT_ANALYST','Credit Analyst','Credit Analyst','','','default','','27','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'CURRENT_BALANCE','Current','Current Balance','','~~~','default','','15','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'CUSTOMER_ACCOUNT_NUMBER','Customer Number','Customer Account Number','','','default','','2','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'CUSTOMER_ACCOUNT_STATUS','Customer Account Status','Customer Account Status','','','default','','23','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'CUSTOMER_NAME','Customer Name','Customer Name','','','default','','1','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'CUSTOMER_PROFILE_CLASS','Profile Class','Customer Profile Class','','','default','','25','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'DUE_DATE','Due Date','Due Date','','','default','','10','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'INVOICE_NUMBER','Invoice Number','Invoice Number','','','default','','6','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'LAST_PAYMENT_DATE','Last Payment Date','Last Payment Date','','','default','','22','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'NINETY_DAYS_BAL','61 To 90 Days','Ninety Days Bal','','~~~','default','','18','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'ONE_EIGHTY_DAYS_BAL','91 To 180 Days','One Eighty Days Bal','','~~~','default','','19','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'OVER_THREE_SIXTY_DAYS_BAL','361+Days Past Due','Over Three Sixty Days Bal','','~~~','default','','21','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'RECEIPT_NUMBER','Receipt Number','Receipt Number','','','default','','9','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'SALESREP_NUMBER','Salesrep Number','Salesrep Number','','','default','','28','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'SEGMENT2','Revenue Branch','Segment2','','','default','','31','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'SITE_CREDIT_HOLD','Site Credit Hold Status','Site Credit Hold','','','default','','26','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'SIXTY_DAYS_BAL','31 To 60 Days','Sixty Days Bal','','~~~','default','','17','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'THIRTY_DAYS_BAL','1 To 30 Days','Thirty Days Bal','','~~~','default','','16','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'THREE_SIXTY_DAYS_BAL','181 To 360 Days','Three Sixty Days Bal','','~~~','default','','20','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'TRANSACTION_REMAINING_BALANCE','Transaction Remaining Balance','Transaction Remaining Balance','','~~~','default','','12','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'TRANSATION_BALANCE','Transation Balance','Transation Balance','','~~~','default','','11','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'TRX_BILL_TO_SITE_NAME','Site Name','Trx Bill To Site Name','','','default','','4','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'TRX_DATE','Invoice Date','Trx Date','','','default','','7','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'TRX_PARTY_SITE_NUMBER','Site Number','Trx Party Site Number','','','default','','3','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'TRX_TYPE','Transaction Type','Trx Type','','','default','','8','N','','','','','','','','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
xxeis.eis_rs_ins.rc( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'OUTSTANDING_BALANCE','Outstanding Balance','Trx Type','NUMBER','~~~','default','','14','Y','','','','','','','TRANSACTION_REMAINING_BALANCE','SA059956','N','N','','EIS_XXWC_AR_AGING_RECLASS_V','','');
--Inserting Report Parameters - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
xxeis.eis_rs_ins.rp( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'Creation Date','Creation Date','CREATION_DATE','IN','','','DATE','N','Y','1','','Y','CONSTANT','SA059956','Y','N','','','');
--Inserting Report Conditions - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
xxeis.eis_rs_ins.rcn( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC',222,'CREATION_DATE','IN',':Creation Date','','','Y','1','Y','SA059956');
--Inserting Report Sorts - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
--Inserting Report Triggers - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
--Inserting Report Templates - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
--Inserting Report Portals - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
--Inserting Report Dashboards - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
--Inserting Report Security - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','222','','50894',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','222','','51289',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','222','','50993',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','222','','50854',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','222','','50880',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','222','','50853',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','222','','50879',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','222','','50877',222,'SA059956','','');
xxeis.eis_rs_ins.rsec( 'Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC','222','','50878',222,'SA059956','','');
--Inserting Report Pivots - Accounts Receivable Aging - BW080 Open AR Reclass - MonthEnd - WC
END;
/
set scan on define on
