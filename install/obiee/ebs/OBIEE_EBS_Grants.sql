grant EXECUTE on APPS.XXWC_OBIEE_ETL_PRE_PROCESS to interface_obi;

------ UC4 Grants

-- XXEIS
connect xxeis
grant EXECUTE on XXEIS.EIS_PO_XXWC_ISR_PKG_V2 to interface_prism;
grant SELECT on xxeis.eis_xxwc_po_isr_tab to interface_prism;

-- APPS
connect xxwc_dev_admin
grant EXECUTE on APPS.XXWC_OBIEE_ETL_PRE_PROCESS to interface_prism;

-- XXWC
connect xxwc
grant SELECT on XXWC.OBIEE_ETL_INV_STAGE to interface_prism;
grant SELECT on XXWC.OBIEE_ETL_INV_SNAPSHOT_TBL to interface_prism;