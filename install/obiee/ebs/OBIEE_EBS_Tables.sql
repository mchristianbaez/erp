-- Create table for the OBIEE ETL Inventory Staging table process.

CREATE TABLE xxwc.obiee_etl_inv_stage
     (
         inventory_item_id        NUMBER
        ,organization_id          NUMBER
        ,safety_qty               NUMBER
        ,prim_bin_loc             VARCHAR2 (128 BYTE)
        ,vendor_name              VARCHAR2 (128 BYTE)
        ,velosity_item_category   VARCHAR2 (128 BYTE)
        ,item_qp_list_price       NUMBER
        ,item_cost                NUMBER
        ,ast_pay_site             VARCHAR2 (128 BYTE)
        ,org_id                   NUMBER
        ,on_ord_qty               NUMBER
        ,qoh                      NUMBER
        ,item_type                VARCHAR2 (512 BYTE)
        ,vendor_num               VARCHAR2 (128 BYTE)
        ,item_number              VARCHAR2 (128 BYTE)
     );