
CREATE OR REPLACE FORCE VIEW XXWC.XXWC_LONGVIEW_BRANCH_HIER_VW
(
  "ORGANIZATION"
,"REGION_CODE"
,"REGION_DESC"
,"MARKET_CODE"
,"MARKET_DESC"
,"DSTRT_CODE"
,"DSTRT_DESC"
,"BW_NUM"
,"BW_DESC"
,"FRU_NUM"
,"BRANCH_NUM"
,"CLOSED_BR"
)
AS
  SELECT 'WHITECAP' organization
        ,lbh.sym_nm4 region_code
        ,lbh.sym_desc_e4 region_desc
        ,NVL (TRIM (lbh.sym_nm5), 'Unspecified')        AS market_code
        ,NVL (TRIM (lbh.sym_desc_e5), 'Unspecified')    AS market_desc
        ,NVL (TRIM (lbh.sym_nm6), 'Unspecified')        AS dstrt_code
        ,NVL (TRIM (lbh.sym_desc_e6), 'Unspecified')    AS dstrt_desc
        ,NVL (TRIM (lbh.leaf_nm), 'Unspecified')        AS bw_num
        ,NVL (TRIM (lbh.leaf_desc), 'Unspecified')      AS bw_desc
        ,NVL (TRIM (leaf_fru), 'Unspecified')           AS fru_num
        ,NVL (TRIM (lct.lob_branch), 'Unspecified')     AS branch_num
        ,NVL (TRIM (lct.inactive), 'Y')                 AS closed_br
    FROM 
        xxwc.xxwc_longview_branch_hierarchy lbh
    LEFT OUTER JOIN 
        xxcus.xxcus_location_code_tbl lct ON lbh.leaf_nm = lct.entrp_loc
   WHERE     leaf_nm LIKE 'BW%'
         AND sym_nm4 LIKE 'RG%'
  UNION
  SELECT 'WHITECAP' organization
        ,lbh.sym_nm3 region_code
        ,lbh.sym_desc_e3 region_desc
        , CASE WHEN TRIM (lbh.sym_nm4) LIKE 'CB%' 
                   THEN TRIM (lbh.sym_nm4) 
                   ELSE 'CB999' 
          END                                           AS market_code
        , CASE WHEN TRIM (lbh.sym_nm4) LIKE 'CB%' 
                    THEN TRIM (lbh.sym_desc_e3) 
                    ELSE 'CORPORATE MARKET' 
          END                                           AS market_desc
        , CASE WHEN TRIM (lbh.sym_nm5) LIKE 'DT%' 
                    THEN TRIM (lbh.sym_nm5) 
                    ELSE 'DT999' 
          END AS dstrt_code
        , CASE WHEN TRIM (lbh.sym_nm5) LIKE 'DT%' 
                    THEN TRIM (lbh.sym_desc_e5) 
                    ELSE 'CORPORATE DISTRICT' 
          END                                           AS dstrt_desc
        ,NVL (TRIM (lbh.leaf_nm), 'Unspecified')        AS bw_num
        ,NVL (TRIM (lbh.leaf_desc), 'Unspecified')      AS bw_desc
        ,NVL (TRIM (leaf_fru), 'Unspecified')           AS fru_num
        ,NVL (TRIM (lct.lob_branch), 'Unspecified')     AS branch_num
        ,NVL (TRIM (lct.inactive), 'Y')                 AS closed_br
    FROM 
        xxwc.xxwc_longview_branch_hierarchy lbh
    LEFT OUTER JOIN 
        xxcus.xxcus_location_code_tbl lct ON lbh.leaf_nm = lct.entrp_loc
   WHERE     leaf_nm LIKE 'BW%'
         AND sym_nm3 LIKE 'RG%'
         AND sym_desc_e3 = 'WHITE CAP - CORPORATE REGION';
