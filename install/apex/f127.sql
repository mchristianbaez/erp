set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2016.08.24'
,p_release=>'5.1.0.00.45'
,p_default_workspace_id=>2222210991411699
,p_default_application_id=>127
,p_default_owner=>'EA_APEX'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 127 - MASTER_ADMINISTRATION
--
-- Application Export:
--   Application:     127
--   Name:            MASTER_ADMINISTRATION
--   Date and Time:   14:54 Tuesday July 18, 2017
--   Exported By:     GP050872
--   Flashback:       0
--   Export Type:     Application Export
--   Version:         5.1.0.00.45
--   Instance ID:     69420199397069
--

-- Application Statistics:
--   Pages:                     12
--     Items:                   18
--     Computations:             2
--     Validations:              4
--     Processes:               20
--     Regions:                 41
--     Buttons:                 27
--     Dynamic Actions:         11
--   Shared Components:
--     Logic:
--       Items:                  2
--     Navigation:
--       Tab Sets:               1
--         Tabs:                 3
--       Lists:                  2
--       Breadcrumbs:            1
--         Entries:             10
--       NavBar Entries:         1
--     Security:
--       Authentication:         7
--       Authorization:          1
--     User Interface:
--       Themes:                 1
--       Templates:
--         Page:                15
--         Region:              23
--         Label:                5
--         List:                17
--         Popup LOV:            1
--         Calendar:             3
--         Breadcrumb:           2
--         Button:               4
--         Report:               9
--       LOVs:                   6
--       Shortcuts:              2
--     Globalization:
--     Reports:
--   Supporting Objects:  Included

prompt --application/delete_application
begin
wwv_flow_api.remove_flow(wwv_flow.g_flow_id);
end;
/
prompt --application/ui_types
begin
null;
end;
/
prompt --application/create_application
begin
wwv_flow_api.create_flow(
 p_id=>wwv_flow.g_flow_id
,p_display_id=>nvl(wwv_flow_application_install.get_application_id,127)
,p_owner=>nvl(wwv_flow_application_install.get_schema,'EA_APEX')
,p_name=>nvl(wwv_flow_application_install.get_application_name,'MASTER_ADMINISTRATION')
,p_alias=>nvl(wwv_flow_application_install.get_application_alias,'F129131127')
,p_page_view_logging=>'YES'
,p_page_protection_enabled_y_n=>'Y'
,p_checksum_salt_last_reset=>'20170613174322'
,p_bookmark_checksum_function=>'MD5'
,p_max_session_length_sec=>28800
,p_flow_language=>'en'
,p_flow_language_derived_from=>'0'
,p_allow_feedback_yn=>'Y'
,p_direction_right_to_left=>'N'
,p_flow_image_prefix => nvl(wwv_flow_application_install.get_image_prefix,'')
,p_authentication=>'PLUGIN'
,p_authentication_id=>wwv_flow_api.id(15771017878335428)
,p_logout_url=>'wwv_flow_custom_auth_std.logout?p_this_flow=&APP_ID.&amp;p_next_flow_page_sess=&APP_ID.:1'
,p_application_tab_set=>1
,p_public_user=>'APEX_PUBLIC_USER'
,p_proxy_server=> nvl(wwv_flow_application_install.get_proxy,'')
,p_flow_version=>'release 1.0'
,p_flow_status=>'AVAILABLE_W_EDIT_LINK'
,p_flow_unavailable_text=>'This application is currently unavailable at this time.'
,p_exact_substitutions_only=>'Y'
,p_deep_linking=>'Y'
,p_runtime_api_usage=>'T:O:W'
,p_authorize_public_pages_yn=>'Y'
,p_rejoin_existing_sessions=>'P'
,p_csv_encoding=>'Y'
,p_auto_time_zone=>'N'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20170613174322'
,p_file_prefix => nvl(wwv_flow_application_install.get_static_app_file_prefix,'')
,p_ui_type_name => null
);
end;
/
prompt --application/shared_components/navigation/lists
begin
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(9124834269655705)
,p_name=>'Link'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(9125048658655706)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'Link to Application'
,p_list_item_link_target=>'http://prdebiz.hdsupply.net:7777/pls/ebizprd/f?p=&P2_APP_ID.'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(15773317279392035)
,p_name=>'Security Main'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(15806821846573078)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'View Responsibilities'
,p_list_item_link_target=>'f?p=&APP_ID.:20:&SESSION.::&DEBUG.::P20_APP_ID:&P2_APP_ID.:'
,p_list_item_icon=>'menu/users_business_64.gif'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(15806993580574417)
,p_list_item_display_sequence=>20
,p_list_item_link_text=>'View Roles of all apps'
,p_list_item_link_target=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.::P30_APP_ID:&P2_APP_ID.:'
,p_list_item_icon=>'menu/addresses_wbg_64x64.png'
,p_list_item_disp_cond_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_list_item_disp_condition=>'P2_APP_ID'
,p_list_item_disp_condition2=>'127'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(15807210203579134)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'Maintain Role Responsibilities'
,p_list_item_link_target=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.::P40_APP_ID:&P2_APP_ID.:'
,p_list_item_icon=>'menu/discussion_wbg_64x64.png'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(15807416090580899)
,p_list_item_display_sequence=>40
,p_list_item_link_text=>'Maintain User Roles for all Apps'
,p_list_item_link_target=>'f?p=&APP_ID.:50:&SESSION.::&DEBUG.::P50_APP_ID:&P2_APP_ID.:'
,p_list_item_icon=>'menu/instructor_wbg_64x64.png'
,p_list_item_disp_cond_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_list_item_disp_condition=>'P2_APP_ID'
,p_list_item_disp_condition2=>'127'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(3895930122900263)
,p_list_item_display_sequence=>50
,p_list_item_link_text=>'AIS Dashboard Log lookup'
,p_list_item_link_target=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'menu/flashlight_wbg_64x64.png'
,p_list_item_current_type=>'TARGET_PAGE'
);
end;
/
prompt --application/shared_components/files
begin
null;
end;
/
prompt --application/plugin_settings
begin
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(2790132162459684)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_DISPLAY_SELECTOR'
,p_attribute_01=>'N'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(2790202109459684)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_IG'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(2790371755459685)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_COLOR_PICKER'
,p_attribute_01=>'classic'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(2790499898459685)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_IR'
,p_attribute_01=>'LEGACY'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(2790513140459685)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_CSS_CALENDAR'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(13456688313971051)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_YES_NO'
,p_attribute_01=>'Y'
,p_attribute_03=>'N'
,p_attribute_05=>'SELECT_LIST'
);
end;
/
prompt --application/shared_components/security/authorizations
begin
wwv_flow_api.create_security_scheme(
 p_id=>wwv_flow_api.id(9122048279448331)
,p_name=>'Super Admin'
,p_scheme_type=>'NATIVE_EXISTS'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.user_id',
'FROM  xxwc.xxwc_ms_user_roles_tbl a',
',     xxwc.xxwc_ms_roles_tbl b',
',     xxwc.xxwc_ms_role_resp_tbl c',
',     xxwc.xxwc_ms_responsibility_tbl d',
'WHERE a.role_id = b.role_id(+)',
'AND   b.role_id = c.role_id(+)',
'AND   c.responsibility_id = d.responsibility_id(+)',
'AND   a.user_id= nvl(v(''APP_USER''),USER)',
'and a.app_id =:app_id',
'and d.app_id = :app_id',
'and b.app_id =:app_id',
'and c.app_id = :app_id',
'AND   d.responsibility_name in (''Super Admin'')'))
,p_error_message=>'Not an authorized user to view this page'
,p_caching=>'BY_USER_BY_PAGE_VIEW'
);
end;
/
prompt --application/shared_components/navigation/navigation_bar
begin
wwv_flow_api.create_icon_bar_item(
 p_id=>wwv_flow_api.id(15768108839318726)
,p_icon_sequence=>200
,p_icon_subtext=>'Logout'
,p_icon_target=>'&LOGOUT_URL.'
,p_icon_image_alt=>'Logout'
,p_icon_height=>32
,p_icon_width=>32
,p_icon_height2=>24
,p_icon_width2=>24
,p_nav_entry_is_feedback_yn=>'N'
,p_cell_colspan=>1
);
end;
/
prompt --application/shared_components/logic/application_processes
begin
null;
end;
/
prompt --application/shared_components/logic/application_items
begin
wwv_flow_api.create_flow_item(
 p_id=>wwv_flow_api.id(15771221341336382)
,p_name=>'FSP_AFTER_LOGIN_URL'
);
wwv_flow_api.create_flow_item(
 p_id=>wwv_flow_api.id(13778127510641939)
,p_name=>'G_LASTPAGE'
,p_protection_level=>'N'
);
end;
/
prompt --application/shared_components/logic/application_computations
begin
null;
end;
/
prompt --application/shared_components/navigation/tabs/standard
begin
wwv_flow_api.create_tab(
 p_id=>wwv_flow_api.id(13470975469513712)
,p_tab_set=>'TS1'
,p_tab_sequence=>10
,p_tab_name=>'T_MAIN'
,p_tab_text=>'Main'
,p_tab_step=>1
,p_tab_also_current_for_pages=>'20'
);
wwv_flow_api.create_tab(
 p_id=>wwv_flow_api.id(15772494076385268)
,p_tab_set=>'TS1'
,p_tab_sequence=>20
,p_tab_name=>'T_TADMINISTRATION'
,p_tab_text=>'Administration'
,p_tab_step=>2
,p_tab_also_current_for_pages=>'30,40,50,60'
,p_tab_plsql_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.user_id',
'FROM  xxwc.xxwc_ms_user_roles_tbl a',
',     xxwc.xxwc_ms_roles_tbl b',
',     xxwc.xxwc_ms_role_resp_tbl c',
',     xxwc.xxwc_ms_responsibility_tbl d',
'WHERE a.role_id = b.role_id(+)',
'AND   b.role_id = c.role_id(+)',
'AND   c.responsibility_id = d.responsibility_id(+)',
'AND   a.user_id= :APP_USER',
'and d.app_id = :APP_ID',
'and c.app_id = :APP_ID',
'and b.role_name = ''Oracle Developers'''))
,p_display_condition_type=>'EXISTS'
);
wwv_flow_api.create_tab(
 p_id=>wwv_flow_api.id(3889213196771450)
,p_tab_set=>'TS1'
,p_tab_sequence=>30
,p_tab_name=>'T_AISLITEDASHBOARD'
,p_tab_text=>'AIS Lite Dashboard'
,p_tab_step=>4
,p_tab_parent_tabset=>'TS1'
);
end;
/
prompt --application/shared_components/navigation/tabs/parent
begin
null;
end;
/
prompt --application/shared_components/user_interface/lovs
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(15774710400418422)
,p_lov_name=>'EMAILS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select NAME display_value, EMAIL return_value ',
'from HDS_USER_PULL_MVW',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(3862603626910295)
,p_lov_name=>'REPORTS'
,p_lov_query=>'.'||wwv_flow_api.id(3862603626910295)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3862926382910295)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Threshold Fallouts'
,p_lov_return_value=>'TF'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3863228582910296)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'Metrics By Records Returned'
,p_lov_return_value=>'MBRR'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3863524716910296)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Metrics By Time Blocks - Today'
,p_lov_return_value=>'MBTBT'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3863819871910296)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Metrics By Time Blocks - Week'
,p_lov_return_value=>'MBTBW'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3864115549910296)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Metrics By Time Blocks - Month'
,p_lov_return_value=>'MBTBM'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3864422927910296)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Weekly View - Graph'
,p_lov_return_value=>'WVG'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(15774915594419877)
,p_lov_name=>'RESPONSIBILITIES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select RESPONSIBILITY_NAME display_value, RESPONSIBILITY_ID return_value ',
'from xxwc.xxwc_ms_responsibility_tbl',
'WHERE APP_ID =:APP_ID',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(15775120096421191)
,p_lov_name=>'ROLES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ROLE_NAME display_value, ROLE_ID return_value ',
'from xxwc.xxwc_ms_roles_tbl',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(15775292523422663)
,p_lov_name=>'USERS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select NAME display_value, EMPLOYEE_ID return_value ',
'from HDS_USER_PULL_MVW',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(4599703254718558)
,p_lov_name=>'YES/NO'
,p_lov_query=>'.'||wwv_flow_api.id(4599703254718558)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(4600324147718561)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'No'
,p_lov_return_value=>'N'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(4600030266718558)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
end;
/
prompt --application/shared_components/navigation/trees
begin
null;
end;
/
prompt --application/pages/page_groups
begin
null;
end;
/
prompt --application/comments
begin
null;
end;
/
prompt --application/shared_components/navigation/breadcrumbs/breadcrumb
begin
wwv_flow_api.create_menu(
 p_id=>wwv_flow_api.id(15769218604318760)
,p_name=>' Breadcrumb'
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(3889006897764343)
,p_short_name=>'AIS Lite Dashboard'
,p_link=>'f?p=&FLOW_ID.:4:&SESSION.'
,p_page_id=>4
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(3895532236893910)
,p_parent_id=>wwv_flow_api.id(15774317170404204)
,p_short_name=>'AIS dashboard lookup report'
,p_link=>'f?p=&FLOW_ID.:5:&SESSION.'
,p_page_id=>5
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(3896611300912043)
,p_parent_id=>wwv_flow_api.id(3895532236893910)
,p_short_name=>'AIS dashboard lookup form'
,p_link=>'f?p=&FLOW_ID.:6:&SESSION.'
,p_page_id=>6
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(15769723775318769)
,p_parent_id=>0
,p_short_name=>'Master Page'
,p_link=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:::'
,p_page_id=>1
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(15774317170404204)
,p_parent_id=>0
,p_short_name=>'Administration'
,p_link=>'f?p=&APP_ID.:2:&SESSION.::&DEBUG.::P2_APP_ID:&P20_APP_ID.'
,p_page_id=>2
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(15778699583448873)
,p_parent_id=>wwv_flow_api.id(15774317170404204)
,p_short_name=>'Update RESPONSIBILITY'
,p_link=>'f?p=&FLOW_ID.:20:&SESSION.'
,p_page_id=>20
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(15782216035465345)
,p_parent_id=>wwv_flow_api.id(15774317170404204)
,p_short_name=>'Update ROLES'
,p_link=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.::P2_APP_ID:&P30_APP_ID.'
,p_page_id=>30
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(15786197423496265)
,p_parent_id=>wwv_flow_api.id(15774317170404204)
,p_short_name=>'Update Role Responsibilities'
,p_link=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.::P2_APP_ID:&P40_APP_ID.'
,p_page_id=>40
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(15787808464525154)
,p_parent_id=>wwv_flow_api.id(15774317170404204)
,p_short_name=>'Users'
,p_link=>'f?p=&APP_ID.:50:&SESSION.::&DEBUG.::P2_APP_ID:&P50_APP_ID.'
,p_page_id=>50
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(15787919227525154)
,p_parent_id=>wwv_flow_api.id(15787808464525154)
,p_short_name=>'User Roles'
,p_link=>'f?p=&APP_ID.:60:&SESSION.'
,p_page_id=>60
);
end;
/
prompt --application/shared_components/user_interface/templates/page
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15759497602318660)
,p_theme_id=>20
,p_name=>'Login'
,p_internal_name=>'LOGIN'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody" height="70%" align="center" width="400">',
'<td width="100%" valign="top" height="100%" id="t20ContentBody" align="center">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_theme_class_id=>6
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15759613762318667)
,p_theme_id=>20
,p_name=>'No Tabs'
,p_internal_name=>'NO_TABS'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" height="100%" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>3
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15759706376318667)
,p_theme_id=>20
,p_name=>'No Tabs with Sidebar'
,p_internal_name=>'NO_TABS_WITH_SIDEBAR'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" height="100%" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" '
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>17
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15759820337318668)
,p_theme_id=>20
,p_name=>'One Level Tabs'
,p_internal_name=>'ONE_LEVEL_TABS'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#TAB_CELLS#</div>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>1
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15759906506318668)
,p_theme_id=>20
,p_name=>'One Level Tabs (Custom 1)'
,p_internal_name=>'ONE_LEVEL_TABS_CUSTOM_1'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table id="t20Tabs" border="0" cellpadding="0" cellspacing="0" summary=""><tr>#TAB_CELLS#</tr></table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_non_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>8
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760003515318668)
,p_theme_id=>20
,p_name=>'One Level Tabs (Custom 5)'
,p_internal_name=>'ONE_LEVEL_TABS_CUSTOM_5'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br /></td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#TAB_CELLS#</div>',
'</div>',
'#REGION_POSITION_08#',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>12
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760120792318669)
,p_theme_id=>20
,p_name=>'One Level Tabs Sidebar'
,p_internal_name=>'ONE_LEVEL_TABS_SIDEBAR'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#TAB_CELLS#</div>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>',
''))
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>16
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760213630318670)
,p_theme_id=>20
,p_name=>'One Level Tabs Sidebar  (Custom 2)'
,p_internal_name=>'ONE_LEVEL_TABS_SIDEBAR_CUSTOM_2'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table id="t20Tabs" border="0" cellpadding="0" cellspacing="0" summary=""><tr>#TAB_CELLS#</tr></table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_non_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>9
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
end;
/
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760302275318670)
,p_theme_id=>20
,p_name=>'One Level Tabs Sidebar (Custom 6)'
,p_internal_name=>'ONE_LEVEL_TABS_SIDEBAR_CUSTOM_6'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br /></td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#TAB_CELLS#</div>',
'</div>',
'#REGION_POSITION_08#',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>',
''))
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>13
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760413065318671)
,p_theme_id=>20
,p_name=>'Popup'
,p_internal_name=>'POPUP'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" cellpadding="0" width="100%" cellspacing="0" border="0">',
'<tr>',
'<td width="100%" valign="top">',
'<div style="border:1px solid black;">#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'#BODY##REGION_POSITION_04#</td>',
'<td valign="top">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>',
'#REGION_POSITION_05#',
''))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE##DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'<div class="t20NavigationBar">#BAR_BODY#</div>'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavigationBar">#TEXT#</a>'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_theme_class_id=>4
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760512316318672)
,p_theme_id=>20
,p_name=>'Printer Friendly'
,p_internal_name=>'PRINTER_FRIENDLY'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" width="100%">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table summary="" cellpadding="0" width="100%" cellspacing="0" border="0" height="70%">',
'<tr>',
'<td width="100%" valign="top"><div class="t20messages">#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'#BODY##REGION_POSITION_02##REGION_POSITION_04#</td>',
'<td valign="top">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'<div class="t20NavigationBar">#BAR_BODY#</div>'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavigationBar">#TEXT#</a>'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_theme_class_id=>5
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
,p_template_comment=>'3'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760622294318672)
,p_theme_id=>20
,p_name=>'Two Level Tabs'
,p_internal_name=>'TWO_LEVEL_TABS'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#PARENT_TAB_CELLS#</div>',
'</div>',
'<div id="t20tablist">#TAB_CELLS#</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="current">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_top_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_top_non_curr_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5" '
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>2
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760707851318672)
,p_theme_id=>20
,p_name=>'Two Level Tabs  (Custom 3)'
,p_internal_name=>'TWO_LEVEL_TABS_CUSTOM_3'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table id="t20Tabs" border="0" cellpadding="0" cellspacing="0" summary=""><tr>#PARENT_TAB_CELLS#</tr></table>',
'</div>',
'<div id="t20ChildTabs">#TAB_CELLS#</div>',
'<div style="background-color:none;">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#" class="t20Tab">#TAB_LABEL#</a>'
,p_top_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_top_non_curr_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>10
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760799873318673)
,p_theme_id=>20
,p_name=>'Two Level Tabs with Sidebar'
,p_internal_name=>'TWO_LEVEL_TABS_WITH_SIDEBAR'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#PARENT_TAB_CELLS#</div>',
'</div>',
'<div id="t20tablist">#TAB_CELLS#</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#" class="current">#TAB_LABEL#</a>',
''))
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_top_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>',
''))
,p_top_non_curr_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#">#TAB_LABEL#</a>',
''))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>18
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(15760917047318673)
,p_theme_id=>20
,p_name=>'Two Level Tabs with Sidebar  (Custom 4)'
,p_internal_name=>'TWO_LEVEL_TABS_WITH_SIDEBAR_CUSTOM_4'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table id="t20Tabs" border="0" cellpadding="0" cellspacing="0" summary=""><tr>#PARENT_TAB_CELLS#</tr></table>',
'</div>',
'<div id="t20ChildTabs">#TAB_CELLS#</div>',
'<div style="background-color:none;">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#" class="t20Tab">#TAB_LABEL#</a>'
,p_top_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_top_non_curr_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>11
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/button
begin
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(15760994264318674)
,p_template_name=>'Button'
,p_internal_name=>'BUTTON'
,p_template=>'<a href="#LINK#" class="t20Button">#LABEL#</a>'
,p_translate_this_template=>'N'
,p_theme_class_id=>1
,p_template_comment=>'Standard Button'
,p_theme_id=>20
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(15761102397318677)
,p_template_name=>'Button, Alternative 1'
,p_internal_name=>'BUTTON,_ALTERNATIVE_1'
,p_template=>'<a href="#LINK#" class="t20Button2">#LABEL#</a>'
,p_translate_this_template=>'N'
,p_theme_class_id=>4
,p_template_comment=>'XP Square FFFFFF'
,p_theme_id=>20
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(15761223832318677)
,p_template_name=>'Button, Alternative 2'
,p_internal_name=>'BUTTON,_ALTERNATIVE_2'
,p_template=>'<a href="#LINK#" class="t20Button">#LABEL#</a>'
,p_translate_this_template=>'N'
,p_theme_class_id=>5
,p_template_comment=>'Standard Button'
,p_theme_id=>20
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(15761310874318677)
,p_template_name=>'Button, Alternative 3'
,p_internal_name=>'BUTTON,_ALTERNATIVE_3'
,p_template=>'<a href="#LINK#" class="t20Button">#LABEL#</a>'
,p_translate_this_template=>'N'
,p_theme_class_id=>2
,p_template_comment=>'Standard Button'
,p_theme_id=>20
);
end;
/
prompt --application/shared_components/user_interface/templates/region
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15761411051318678)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20Borderless" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Borderless Region'
,p_internal_name=>'BORDERLESS_REGION'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>20
,p_theme_class_id=>7
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15761520417318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20Bracketed" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Bracketed Region'
,p_internal_name=>'BRACKETED_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>18
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15761622021318681)
,p_layout=>'TABLE'
,p_template=>'<div id="#REGION_STATIC_ID#" class="t20Breadcrumbs" #REGION_ATTRIBUTES#>#BODY#</div>'
,p_page_plug_template_name=>'Breadcrumb Region'
,p_internal_name=>'BREADCRUMB_REGION'
,p_theme_id=>20
,p_theme_class_id=>6
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15761699020318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ButtonRegionwithTitle" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'</tbody>',
'</table>#BODY#'))
,p_page_plug_template_name=>'Button Region with Title'
,p_internal_name=>'BUTTON_REGION_WITH_TITLE'
,p_theme_id=>20
,p_theme_class_id=>4
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15761797753318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ButtonRegionwithoutTitle" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'</tbody>',
'</table>#BODY#'))
,p_page_plug_template_name=>'Button Region without Title'
,p_internal_name=>'BUTTON_REGION_WITHOUT_TITLE'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>17
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15761905169318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ChartRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Chart Region'
,p_internal_name=>'CHART_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>30
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15761996439318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20FormRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Form Region'
,p_internal_name=>'FORM_REGION'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>20
,p_theme_class_id=>8
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762106842318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20HideShow" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header"><img src="#IMAGE_PREFIX#themes/theme_20/collapse_plus.gif" onclick="htmldb_ToggleWithImage(this,''#REGION_STATIC_ID#_body'')" class="pb" alt="" />#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body" style="display:none;">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Hide and Show Region'
,p_internal_name=>'HIDE_AND_SHOW_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>1
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762199152318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ListRegionwithIcon" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'List Region with Icon (Chart)'
,p_internal_name=>'LIST_REGION_WITH_ICON_CHART'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>29
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762309294318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t20Region t20NavRegion" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'<div class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</div>',
'<div id="#REGION_STATIC_ID#_body" class="t20RegionBody">#BODY#</div>',
'</div>'))
,p_page_plug_template_name=>'Navigation Region'
,p_internal_name=>'NAVIGATION_REGION'
,p_theme_id=>20
,p_theme_class_id=>5
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762423896318681)
,p_layout=>'TABLE'
,p_template=>'<div class="t20Region t20NavRegionAlt" id="#REGION_STATIC_ID#"#REGION_ATTRIBUTES#><div class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</div><div id="#REGION_STATIC_ID#_body" class="t20RegionBody">#BODY#</div></div>'
,p_page_plug_template_name=>'Navigation Region, Alternative 1'
,p_internal_name=>'NAVIGATION_REGION,_ALTERNATIVE_1'
,p_theme_id=>20
,p_theme_class_id=>16
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762515903318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20RegionwithoutButtonsandTitle" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Region without Buttons and Title'
,p_internal_name=>'REGION_WITHOUT_BUTTONS_AND_TITLE'
,p_theme_id=>20
,p_theme_class_id=>19
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762613243318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20RegionwithoutTitle" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Region without Title'
,p_internal_name=>'REGION_WITHOUT_TITLE'
,p_theme_id=>20
,p_theme_class_id=>11
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762708421318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="apex_finderbar" cellpadding="0" cellspacing="0" border="0" summary="" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'<tbody>',
'<tr>',
'<td class="apex_finderbar_left_top" valign="top"><img src="#IMAGE_PREFIX#1px_trans.gif" width="10" height="8" alt=""  class="spacer" alt="" /></td>',
'<td class="apex_finderbar_middle" rowspan="3" valign="middle"><img src="#IMAGE_PREFIX#htmldb/builder/builder_find.png" /></td>',
'<td class="apex_finderbar_middle" rowspan="3" valign="middle" style="">#BODY#</td>',
'<td class="apex_finderbar_left" rowspan="3" width="10"><br /></td>',
'<td class="apex_finderbar_buttons" rowspan="3" valign="middle" nowrap="nowrap"><span class="apex_close">#CLOSE#</span><span>#EDIT##CHANGE##DELETE##CREATE##CREATE2##COPY##PREVIOUS##NEXT##EXPAND##HELP#</span></td>',
'</tr>',
'<tr><td class="apex_finderbar_left_middle"><br /></td></tr>',
'<tr>',
'<td class="apex_finderbar_left_bottom" valign="bottom"><img src="#IMAGE_PREFIX#1px_trans.gif" width="10" height="8"  class="spacer" alt="" /></td>',
'</tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Report Filter - Single Row'
,p_internal_name=>'REPORT_FILTER_SINGLE_ROW'
,p_theme_id=>20
,p_theme_class_id=>31
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762805091318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ReportList" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Report List'
,p_internal_name=>'REPORT_LIST'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>29
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762904053318681)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ReportRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Reports Region'
,p_internal_name=>'REPORTS_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>9
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15762995582318682)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ReportsRegion100" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Reports Region 100% Width'
,p_internal_name=>'REPORTS_REGION_100%_WIDTH'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>13
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15763118972318682)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ReportsRegionAlt" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Reports Region, Alternative 1'
,p_internal_name=>'REPORTS_REGION,_ALTERNATIVE_1'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>10
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15763203843318682)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20SidebarRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Sidebar Region'
,p_internal_name=>'SIDEBAR_REGION'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>20
,p_theme_class_id=>2
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15763302460318682)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20SidebarRegionAlt" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Sidebar Region, Alternative 1'
,p_internal_name=>'SIDEBAR_REGION,_ALTERNATIVE_1'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>20
,p_theme_class_id=>3
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15763408848318682)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="rounded-corner-region-blank" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="rc-gray-top">',
'    <div class="rc-gray-top-r"></div>',
'  </div>',
'  <div class="rc-body">',
'    <div class="rc-body-r">',
'      <div class="rc-content-main">',
'        <div class="rc-left">',
'          #BODY#',
'        </div>',
'        <div class="rc-right">',
'          #CLOSE##COPY##DELETE##CHANGE##EDIT##PREVIOUS##NEXT##CREATE##EXPAND#',
'        </div>',
'        <div class="clear"></div>',
'      </div>',
'    </div>',
'  </div>',
'  <div class="rc-bottom">',
'    <div class="rc-bottom-r"></div>',
'  </div>',
'</div>'))
,p_page_plug_template_name=>'Top Bar'
,p_internal_name=>'TOP_BAR'
,p_theme_id=>20
,p_theme_class_id=>21
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15763515329318682)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20WizardRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Wizard Region'
,p_internal_name=>'WIZARD_REGION'
,p_theme_id=>20
,p_theme_class_id=>12
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(15763612173318682)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20WizardRegionIcon" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Wizard Region with Icon'
,p_internal_name=>'WIZARD_REGION_WITH_ICON'
,p_theme_id=>20
,p_theme_class_id=>20
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/list
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15763700649318682)
,p_list_template_current=>'<a href="#LINK#" class="t20Button t20current">#TEXT#</a>'
,p_list_template_noncurrent=>'<a href="#LINK#" class="t20Button">#TEXT#</a>'
,p_list_template_name=>'Button List'
,p_internal_name=>'BUTTON_LIST'
,p_theme_id=>20
,p_theme_class_id=>6
,p_list_template_before_rows=>'<div class="t20ButtonList">'
,p_list_template_after_rows=>'</div>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15763816892318686)
,p_list_template_current=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Hierarchical Expanded'
,p_internal_name=>'HIERARCHICAL_EXPANDED'
,p_theme_id=>20
,p_theme_class_id=>23
,p_list_template_before_rows=>'<ul class="htmlTree">'
,p_list_template_after_rows=>'</ul><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#">'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_sub_list_item_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_item_templ_noncurr_w_child=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_sub_templ_curr_w_child=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_sub_templ_noncurr_w_child=>'<li><a href="#LINK#">#TEXT#</a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15763909876318686)
,p_list_template_current=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/node.gif" align="middle" alt="" /><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/node.gif" align="middle"  alt="" /><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Hierarchical Expanding'
,p_internal_name=>'HIERARCHICAL_EXPANDING'
,p_theme_id=>20
,p_theme_class_id=>22
,p_list_template_before_rows=>'<ul class="dhtmlTree">'
,p_list_template_after_rows=>'</ul><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#" style="display:none;" class="dhtmlTree">'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/node.gif" align="middle"  alt="" /><a href="#LINK#">#TEXT#</a></li>'
,p_sub_list_item_noncurrent=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/node.gif"  align="middle" alt="" /><a href="#LINK#">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/plus.gif" align="middle"  onclick="htmldb_ToggleWithImage(this,''#LIST_ITEM_ID#'')" class="pseudoButtonInactive" /><a href="#LINK#">#TEXT#</a></li>'
,p_item_templ_noncurr_w_child=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/plus.gif" align="middle"  onclick="htmldb_ToggleWithImage(this,''#LIST_ITEM_ID#'')" class="pseudoButtonInactive" /><a href="#LINK#">#TEXT#</a></li>'
,p_sub_templ_curr_w_child=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/plus.gif" onclick="htmldb_ToggleWithImage(this,''#LIST_ITEM_ID#'')" align="middle" class="pseudoButtonInactive" /><a href="#LINK#">#TEXT#</a></li>'
,p_sub_templ_noncurr_w_child=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/plus.gif" onclick="htmldb_ToggleWithImage(this,''#LIST_ITEM_ID#'')" align="middle" class="pseudoButtonInactive" /><a href="#LINK#">#TEXT#</a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15763992192318686)
,p_list_template_current=>'<td class="t20current"><img src="#IMAGE_PREFIX##IMAGE#" border="0" #IMAGE_ATTR#/><br />#TEXT#</td>'
,p_list_template_noncurrent=>'<td><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" border="0" #IMAGE_ATTR#/></a><br /><a href="#LINK#">#TEXT#</a></td>'
,p_list_template_name=>'Horizontal Images with Label List'
,p_internal_name=>'HORIZONTAL_IMAGES_WITH_LABEL_LIST'
,p_theme_id=>20
,p_theme_class_id=>4
,p_list_template_before_rows=>'<table class="t20HorizontalImageswithLabelList" cellpadding="0" border="0" cellspacing="0" summary=""><tr>'
,p_list_template_after_rows=>'</tr></table>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15764105715318686)
,p_list_template_current=>'<a href="#LINK#" class="t20current">#TEXT#</a>'
,p_list_template_noncurrent=>'<a href="#LINK#">#TEXT#</a>'
,p_list_template_name=>'Horizontal Links List'
,p_internal_name=>'HORIZONTAL_LINKS_LIST'
,p_theme_id=>20
,p_theme_class_id=>3
,p_list_template_before_rows=>'<div class="t20HorizontalLinksList">'
,p_list_template_after_rows=>'</div>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15764218554318686)
,p_list_template_current=>'<li class="dhtmlMenuItem"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li class="dhtmlMenuItem"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Pull Down Menu'
,p_internal_name=>'PULL_DOWN_MENU'
,p_theme_id=>20
,p_theme_class_id=>20
,p_list_template_before_rows=>'<ul class="dhtmlMenuLG2">'
,p_list_template_after_rows=>'</ul><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#" class="dhtmlSubMenu2" style="display:none;">'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li class="dhtmlMenuSep2"><img src="#IMAGE_PREFIX#themes/theme_13/1px_trans.gif"  width="1" height="1" alt="" class="dhtmlMenuSep2" /></li>'
,p_sub_list_item_noncurrent=>'<li><a href="#LINK#" class="dhtmlSubMenuN" onmouseover="dhtml_CloseAllSubMenusL(this)">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<li class="dhtmlMenuItem1"><a href="#LINK#">#TEXT#</a><img src="#IMAGE_PREFIX#themes/theme_13/menu_small.gif" alt="Expand" onclick="app_AppMenuMultiOpenBottom2(this,''#LIST_ITEM_ID#'',false)" /></li>'
,p_item_templ_noncurr_w_child=>'<li class="dhtmlMenuItem1"><a href="#LINK#">#TEXT#</a><img src="#IMAGE_PREFIX#themes/theme_13/menu_small.gif" alt="Expand" onclick="app_AppMenuMultiOpenBottom2(this,''#LIST_ITEM_ID#'',false)" /></li>'
,p_sub_templ_curr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
,p_sub_templ_noncurr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15764320467318686)
,p_list_template_current=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#menu/drop_down.png" width="20" height="128" alt="" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_list_template_noncurrent=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#menu/drop_down.png" width="20" height="128" alt=""  /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_list_template_name=>'Pull Down Menu with Image'
,p_internal_name=>'PULL_DOWN_MENU_WITH_IMAGE'
,p_theme_id=>20
,p_theme_class_id=>21
,p_list_template_before_rows=>'<div class="dhtmlMenuLG">'
,p_list_template_after_rows=>'</div><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#" class="dhtmlSubMenu2" style="display:none;"><li class="dhtmlSubMenuP" onmouseover="dhtml_CloseAllSubMenusL(this)">#PARENT_TEXT#</li>'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li class="dhtmlMenuSep"><img src="#IMAGE_PREFIX#themes/theme_13/1px_trans.gif"  width="1" height="1" alt=""  class="dhtmlMenuSep" /></li>'
,p_sub_list_item_noncurrent=>'<li><a href="#LINK#" class="dhtmlSubMenuN" onmouseover="dhtml_CloseAllSubMenusL(this)">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#menu/drop_down.png" width="20" height="128" alt=""  class="dhtmlMenu" onclick="app_AppMenuMultiOpenBottom(this,''#LIST_ITEM_ID#'',fa'
||'lse)" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_item_templ_noncurr_w_child=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#menu/drop_down.png" width="20" height="128" alt=""  class="dhtmlMenu" onclick="app_AppMenuMultiOpenBottom(this,''#LIST_ITEM_ID#'',fa'
||'lse)" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_sub_templ_curr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
,p_sub_templ_noncurr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15764413530318687)
,p_list_template_current=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/generic_list.gif" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#themes/generic_nochild.gif" width="22" height="75" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_list_template_noncurrent=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/generic_list.gif" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#themes/generic_nochild.gif" width="22" height="75" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_list_template_name=>'Pull Down Menu with Image (Custom 1)'
,p_internal_name=>'PULL_DOWN_MENU_WITH_IMAGE_CUSTOM_1'
,p_theme_id=>20
,p_theme_class_id=>9
,p_list_template_before_rows=>'<div class="dhtmlMenuLG">'
,p_list_template_after_rows=>'</div><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#" class="dhtmlSubMenu2" style="display:none;"><li class="dhtmlSubMenuP" onmouseover="dhtml_CloseAllSubMenusL(this)">#PARENT_TEXT#</li>'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li class="dhtmlMenuSep"><img src="#IMAGE_PREFIX#themes/theme_13/1px_trans.gif"  width="1" height="1" alt=""  class="dhtmlMenuSep" /></li>'
,p_sub_list_item_noncurrent=>'<li><a href="#LINK#" class="dhtmlSubMenuN" onmouseover="dhtml_CloseAllSubMenusL(this)">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/generic_list.gif" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#themes/generic_open.gif" width="22" height="75" class="dhtmlMenu" onclick="app_AppMenuMultiOpenBottom(this,''#LIST_'
||'ITEM_ID#'',false)" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_item_templ_noncurr_w_child=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/generic_list.gif" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#themes/generic_open.gif" width="22" height="75" class="dhtmlMenu" onclick="app_AppMenuMultiOpenBottom(this,''#LIST_'
||'ITEM_ID#'',false)" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_sub_templ_curr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
,p_sub_templ_noncurr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15764521210318687)
,p_list_template_current=>'<a href="#LINK#" class="current">#TEXT#</a>'
,p_list_template_noncurrent=>'<a href="#LINK#">#TEXT#</a>'
,p_list_template_name=>'Tab List (Custom 3)'
,p_internal_name=>'TAB_LIST_CUSTOM_3'
,p_theme_id=>20
,p_theme_class_id=>11
,p_list_template_before_rows=>'<div id="t20tablist">'
,p_list_template_after_rows=>'</div>'
,p_list_template_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'This list matches the child tabs on the Two Level Tab templates.',
'Use in region position 8 of One Level Tabs (Custom 5) and/or One Level Tabs Sidebar (Custom 6).'))
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15764620522318687)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#LINK#">#TEXT#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#LINK#">#TEXT#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_list_template_name=>'Tabbed Navigation List'
,p_internal_name=>'TABBED_NAVIGATION_LIST'
,p_theme_id=>20
,p_theme_class_id=>7
,p_list_template_before_rows=>'<table class="t20Tabs t20TabbedNavigationList" border="0" cellpadding="0" cellspacing="0" summary=""><tr>'
,p_list_template_after_rows=>'</tr></table>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15764693926318687)
,p_list_template_current=>'<tr><td class="t20current"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# />#TEXT#</a></td></tr>'
,p_list_template_noncurrent=>'<tr><td><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# />#TEXT#</a></td></tr>'
,p_list_template_name=>'Vertical Images List'
,p_internal_name=>'VERTICAL_IMAGES_LIST'
,p_theme_id=>20
,p_theme_class_id=>5
,p_list_template_before_rows=>'<table border="0" cellpadding="0" cellspacing="0" summary="" class="t20VerticalImagesList">'
,p_list_template_after_rows=>'</table>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15764793789318687)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<tr><td align="left"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></td><td align="left"><a href="#LINK#">#TEXT#</a></td></tr>',
''))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<tr><td align="left"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></td><td align="left"><a href="#LINK#">#TEXT#</a></td></tr>',
''))
,p_list_template_name=>'Vertical Images List (Custom 2)'
,p_internal_name=>'VERTICAL_IMAGES_LIST_CUSTOM_2'
,p_theme_id=>20
,p_theme_class_id=>10
,p_list_template_before_rows=>'<table border="0" cellpadding="0" cellspacing="5" summary="" >'
,p_list_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
''))
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15764922520318687)
,p_list_template_current=>'<li class="t20current"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Ordered List'
,p_internal_name=>'VERTICAL_ORDERED_LIST'
,p_theme_id=>20
,p_theme_class_id=>2
,p_list_template_before_rows=>'<ol class="t20VerticalOrderedList">'
,p_list_template_after_rows=>'</ol>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15765020242318687)
,p_list_template_current=>'<a href="#LINK#" class="current">#TEXT#</a>'
,p_list_template_noncurrent=>'<a href="#LINK#">#TEXT#</a>'
,p_list_template_name=>'Vertical Sidebar List'
,p_internal_name=>'VERTICAL_SIDEBAR_LIST'
,p_theme_id=>20
,p_theme_class_id=>19
,p_list_template_before_rows=>'<div class="t20VerticalSidebarList">'
,p_list_template_after_rows=>'</div>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15765099949318687)
,p_list_template_current=>'<li class="t20current"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Unordered Links without Bullets'
,p_internal_name=>'VERTICAL_UNORDERED_LINKS_WITHOUT_BULLETS'
,p_theme_id=>20
,p_theme_class_id=>18
,p_list_template_before_rows=>'<ul class="t20VerticalUnorderedLinkswithoutBullets">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15765207619318687)
,p_list_template_current=>'<li class="t20current"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Unordered List with Bullets'
,p_internal_name=>'VERTICAL_UNORDERED_LIST_WITH_BULLETS'
,p_theme_id=>20
,p_theme_class_id=>1
,p_list_template_before_rows=>'<ul class="t20VerticalUnorderedListwithBullets">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(15765321901318687)
,p_list_template_current=>'<div class="t20current">#TEXT#</div>'
,p_list_template_noncurrent=>'<div>#TEXT#</div>'
,p_list_template_name=>'Wizard Progress List'
,p_internal_name=>'WIZARD_PROGRESS_LIST'
,p_theme_id=>20
,p_theme_class_id=>17
,p_list_template_before_rows=>'<div class="t20WizardProgressList">'
,p_list_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<center>&DONE.</center>',
'</div>'))
);
end;
/
prompt --application/shared_components/user_interface/templates/report
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(15765422425318687)
,p_row_template_name=>'APEX 4.0 - Value Attribute Pairs'
,p_internal_name=>'APEX_4.0_VALUE_ATTRIBUTE_PAIRS'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="report-row">',
'  <div class="report-col-hdr">#COLUMN_HEADER#</div>',
'  <div class="report-col-val">#COLUMN_VALUE#</div>',
'</div>'))
,p_row_template_before_rows=>'<div class="two-col-report-portlet">'
,p_row_template_after_rows=>'</div>'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_theme_id=>20
,p_theme_class_id=>6
,p_translate_this_template=>'N'
);
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(15765510343318690)
,p_row_template_name=>'Borderless'
,p_internal_name=>'BORDERLESS'
,p_row_template1=>'<td headers="#COLUMN_HEADER_NAME#" #ALIGNMENT# class="t20data">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table class="t20Borderless t20Report" cellpadding="0" border="0" cellspacing="0" summary="">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>',
''))
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"#ALIGNMENT# id="#COLUMN_HEADER_NAME#">#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_checked=>'#CCCCCC'
,p_theme_id=>20
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(15765510343318690)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(15765616542318690)
,p_row_template_name=>'Horizontal Border'
,p_internal_name=>'HORIZONTAL_BORDER'
,p_row_template1=>'<td headers="#COLUMN_HEADER_NAME#" #ALIGNMENT# class="t20data">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table class="t20HorizontalBorder t20Report" border="0" cellpadding="0" cellspacing="0" summary="">'))
,p_row_template_after_rows=>'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"  id="#COLUMN_HEADER_NAME#" #ALIGNMENT#>#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_checked=>'#CCCCCC'
,p_theme_id=>20
,p_theme_class_id=>2
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(15765616542318690)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(15765705061318690)
,p_row_template_name=>'One Column Unordered List'
,p_internal_name=>'ONE_COLUMN_UNORDERED_LIST'
,p_row_template1=>'#COLUMN_VALUE#'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">',
'#TOP_PAGINATION#<tr><td><ul class="t20OneColumnUnorderedList">'))
,p_row_template_after_rows=>'</ul><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>20
,p_theme_class_id=>3
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(15765705061318690)
,p_row_template_before_first=>'<li>'
,p_row_template_after_last=>'</li>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(15765798019318690)
,p_row_template_name=>'Standard'
,p_internal_name=>'STANDARD'
,p_row_template1=>'<td #ALIGNMENT# headers="#COLUMN_HEADER#" class="t20data">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report t20Standard">'))
,p_row_template_after_rows=>'</table><div class="t20CVS">#CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"#ALIGNMENT# id="#COLUMN_HEADER_NAME#">#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>20
,p_theme_class_id=>4
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(15765798019318690)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(15765903971318690)
,p_row_template_name=>'Standard (PPR)'
,p_internal_name=>'STANDARD_PPR'
,p_row_template1=>'<td #ALIGNMENT# headers="#COLUMN_HEADER#" class="t20data">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="report#REGION_ID#"><htmldb:#REGION_ID#><table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Standard t20Report">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table><script language=JavaScript type=text/javascript>',
'<!--',
'init_htmlPPRReport(''#REGION_ID#'');',
'',
'//-->',
'</script>',
'</htmldb:#REGION_ID#>',
'</div>'))
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"#ALIGNMENT# id="#COLUMN_HEADER_NAME#">#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="javascript:html_PPR_Report_Page(this,''#REGION_ID#'',''#LINK#'')" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="javascript:html_PPR_Report_Page(this,''#REGION_ID#'',''#LINK#'')" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="javascript:html_PPR_Report_Page(this,''#REGION_ID#'',''#LINK#'')" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="javascript:html_PPR_Report_Page(this,''#REGION_ID#'',''#LINK#'')" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_checked=>'#CCCCCC'
,p_theme_id=>20
,p_theme_class_id=>7
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(15765903971318690)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(15766017908318690)
,p_row_template_name=>'Standard, Alternating Row Colors'
,p_internal_name=>'STANDARD,_ALTERNATING_ROW_COLORS'
,p_row_template1=>'<td headers="#COLUMN_HEADER_NAME#" #ALIGNMENT# class="t20data">#COLUMN_VALUE#</td>'
,p_row_template2=>'<td headers="#COLUMN_HEADER_NAME#" #ALIGNMENT# class="t20dataalt">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table border="0" cellpadding="0" cellspacing="0" summary="" class="t20StandardAlternatingRowColors t20Report">'))
,p_row_template_after_rows=>'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"#ALIGNMENT# id="#COLUMN_HEADER_NAME#">#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'ODD_ROW_NUMBERS'
,p_row_template_display_cond2=>'NOT_CONDITIONAL'
,p_row_template_display_cond3=>'NOT_CONDITIONAL'
,p_row_template_display_cond4=>'ODD_ROW_NUMBERS'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_checked=>'#CCCCCC'
,p_theme_id=>20
,p_theme_class_id=>5
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(15766017908318690)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(15766096333318691)
,p_row_template_name=>'Two Column Portlet'
,p_internal_name=>'TWO_COLUMN_PORTLET'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="report-row">',
'  <div class="report-col-hdr">#1#</div>',
'  <div class="report-col-val">#2#</div>',
'</div>'))
,p_row_template_before_rows=>'<div class="two-col-report-portlet" #REPORT_ATTRIBUTES# id="#REGION_ID#">'
,p_row_template_after_rows=>'</div>'
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_theme_id=>20
,p_theme_class_id=>7
,p_translate_this_template=>'N'
);
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(15766205807318691)
,p_row_template_name=>'Value Attribute Pairs'
,p_internal_name=>'VALUE_ATTRIBUTE_PAIRS'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<tr>',
'<th class="t20ReportHeader">#COLUMN_HEADER#</th>',
'<td class="t20data">#COLUMN_VALUE#</td>',
'</tr>'))
,p_row_template_before_rows=>'<table cellpadding="0" cellspacing="0" border="0" summary=""#REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#<tr><td><table cellpadding="0" cellspacing="0" border="0" summary="" class="t20ValueAttributePairs">'
,p_row_template_after_rows=>'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>20
,p_theme_class_id=>6
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(15766205807318691)
,p_row_template_after_last=>'<tr><td colspan="2" class="t20seperate"><br /></td></tr>'
);
exception when others then null;
end;
end;
/
prompt --application/shared_components/user_interface/templates/label
begin
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(15766307244318691)
,p_template_name=>'No Label'
,p_internal_name=>'NO_LABEL'
,p_template_body1=>'<span class="t20NoLabel">'
,p_template_body2=>'</span>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>13
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(15766422138318693)
,p_template_name=>'Optional Label'
,p_internal_name=>'OPTIONAL_LABEL'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" tabindex="999"><span class="t20OptionalLabel">'
,p_template_body2=>'</span></label>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>3
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(15766501526318693)
,p_template_name=>'Optional Label with Help'
,p_internal_name=>'OPTIONAL_LABEL_WITH_HELP'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" tabindex="999"><a class="t20OptionalLabelwithHelp" href="javascript:popupFieldHelp(''#CURRENT_ITEM_ID#'',''&SESSION.'')" tabindex="999">'
,p_template_body2=>'</a></label>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(15766592270318693)
,p_template_name=>'Required Label'
,p_internal_name=>'REQUIRED_LABEL'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" tabindex="999"><span class="t20Req">*</span><span class="t20RequiredLabel">'
,p_template_body2=>'</span></label>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>4
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(15766691312318693)
,p_template_name=>'Required Label with Help'
,p_internal_name=>'REQUIRED_LABEL_WITH_HELP'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" tabindex="999"><span class="t20Req">*</span><a class="t20RequiredLabelwithHelp" href="javascript:popupFieldHelp(''#CURRENT_ITEM_ID#'',''&SESSION.'')" tabindex="999">'
,p_template_body2=>'</a></label>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>2
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/breadcrumb
begin
wwv_flow_api.create_menu_template(
 p_id=>wwv_flow_api.id(15766804687318693)
,p_name=>'Breadcrumb Menu'
,p_internal_name=>'BREADCRUMB_MENU'
,p_current_page_option=>'<a href="#LINK#" class="t20Current">#NAME#</a>'
,p_non_current_page_option=>'<a href="#LINK#">#NAME#</a>'
,p_between_levels=>'<b>&gt;</b>'
,p_max_levels=>12
,p_start_with_node=>'PARENT_TO_LEAF'
,p_theme_id=>20
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
wwv_flow_api.create_menu_template(
 p_id=>wwv_flow_api.id(15766919976318697)
,p_name=>'Hierarchical Menu'
,p_internal_name=>'HIERARCHICAL_MENU'
,p_before_first=>'<ul class="t20HierarchicalMenu">'
,p_current_page_option=>'<li class="t20current"><a href="#LINK#">#NAME#</a></li>'
,p_non_current_page_option=>'<li><a href="#LINK#">#NAME#</a></li>'
,p_after_last=>'</ul>'
,p_max_levels=>11
,p_start_with_node=>'CHILD_MENU'
,p_theme_id=>20
,p_theme_class_id=>2
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/popuplov
begin
wwv_flow_api.create_popup_lov_template(
 p_id=>wwv_flow_api.id(15767593960318701)
,p_popup_icon=>'#IMAGE_PREFIX#lov_16x16.gif'
,p_popup_icon_attr=>'width="16" height="16" alt="Popup Lov"'
,p_page_name=>'winlov'
,p_page_title=>'Search Dialog'
,p_page_html_head=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE.">',
'<head>',
'<title>#TITLE#</title>',
'#APEX_CSS#',
'#THEME_CSS#',
'#THEME_STYLE_CSS#',
'#APEX_JAVASCRIPT#',
'<link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon"><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_3_1.css" type="text/css">',
'',
'',
'</head>'))
,p_page_body_attr=>'onload="first_field()" style="background-color:#FFFFFF;margin:0;"'
,p_before_field_text=>'<div class="t20PopupHead">'
,p_filter_width=>'20'
,p_filter_max_width=>'100'
,p_find_button_text=>'Search'
,p_close_button_text=>'Close'
,p_next_button_text=>'Next >'
,p_prev_button_text=>'< Previous'
,p_after_field_text=>'</div>'
,p_scrollbars=>'1'
,p_resizable=>'1'
,p_width=>'400'
,p_height=>'450'
,p_result_row_x_of_y=>'<br /><div style="padding:2px; font-size:8pt;">Row(s) #FIRST_ROW# - #LAST_ROW#</div>'
,p_result_rows_per_pg=>500
,p_before_result_set=>'<div class="t20PopupBody">'
,p_theme_id=>20
,p_theme_class_id=>1
,p_translate_this_template=>'N'
,p_after_result_set=>'</div>'
);
end;
/
prompt --application/shared_components/user_interface/templates/calendar
begin
wwv_flow_api.create_calendar_template(
 p_id=>wwv_flow_api.id(15767014181318697)
,p_cal_template_name=>'Calendar'
,p_internal_name=>'CALENDAR'
,p_day_of_week_format=>'<th class="t20DayOfWeek">#IDAY#</th>'
,p_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellspacing="0" cellpadding="0" border="0" summary="" class="t20CalendarAlternative1Holder"> ',
' <tr>',
'   <td class="t20MonthTitle">#IMONTH# #YYYY#</td>',
' </tr>',
' <tr>',
' <td class="t20MonthBody">'))
,p_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="0" class="t20CalendarAlternative1">'
,p_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table></td>',
'</tr>',
'</table>',
''))
,p_day_title_format=>'<div class="t20DayTitle">#DD#</div><br />'
,p_day_open_format=>'<td class="t20Day" valign="top">#TITLE_FORMAT##DATA#'
,p_day_close_format=>'</td>'
,p_today_open_format=>'<td valign="top" class="t20Today">#TITLE_FORMAT##DATA#'
,p_weekend_title_format=>'<div class="t20WeekendDayTitle">#DD#</div><br />'
,p_weekend_open_format=>'<td valign="top" class="t20WeekendDay">#TITLE_FORMAT##DATA#'
,p_weekend_close_format=>'</td>'
,p_nonday_title_format=>'<div class="t20NonDayTitle">#DD#</div><br />'
,p_nonday_open_format=>'<td class="t20NonDay" valign="top">'
,p_nonday_close_format=>'</td>'
,p_week_open_format=>'<tr>'
,p_week_close_format=>'</tr> '
,p_daily_title_format=>'<th width="14%" class="calheader">#IDAY#</th>'
,p_daily_open_format=>'<tr>'
,p_daily_close_format=>'</tr>'
,p_month_data_format=>'#DAYS#'
,p_month_data_entry_format=>'#DATA#'
,p_theme_id=>20
,p_theme_class_id=>1
);
wwv_flow_api.create_calendar_template(
 p_id=>wwv_flow_api.id(15767195547318699)
,p_cal_template_name=>'Calendar, Alternative 1'
,p_internal_name=>'CALENDAR,_ALTERNATIVE_1'
,p_day_of_week_format=>'<th class="t20DayOfWeek">#IDAY#</th>'
,p_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellspacing="0" cellpadding="0" border="0" summary="" class="t20CalendarHolder"> ',
' <tr>',
'   <td class="t20MonthTitle">#IMONTH# #YYYY#</td>',
' </tr>',
' <tr>',
' <td class="t20MonthBody">'))
,p_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="0" class="t20Calendar">'
,p_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table></td>',
'</tr>',
'</table>',
''))
,p_day_title_format=>'<div class="t20DayTitle">#DD#</div><br />'
,p_day_open_format=>'<td class="t20Day" valign="top">#TITLE_FORMAT##DATA#'
,p_day_close_format=>'</td>'
,p_today_open_format=>'<td valign="top" class="t20Today">#TITLE_FORMAT##DATA#'
,p_weekend_title_format=>'<div class="t20WeekendDayTitle">#DD#</div><br />'
,p_weekend_open_format=>'<td valign="top" class="t20WeekendDay">#TITLE_FORMAT##DATA#'
,p_weekend_close_format=>'</td>'
,p_nonday_title_format=>'<div class="t20NonDayTitle">#DD#</div><br />'
,p_nonday_open_format=>'<td class="t20NonDay" valign="top">'
,p_nonday_close_format=>'</td>'
,p_week_open_format=>'<tr>'
,p_week_close_format=>'</tr> '
,p_daily_title_format=>'<th width="14%" class="calheader">#IDAY#</th>'
,p_daily_open_format=>'<tr>'
,p_daily_close_format=>'</tr>'
,p_month_data_format=>'#DAYS#'
,p_month_data_entry_format=>'#DATA#'
,p_theme_id=>20
,p_theme_class_id=>2
);
wwv_flow_api.create_calendar_template(
 p_id=>wwv_flow_api.id(15767406342318700)
,p_cal_template_name=>'Small Calender'
,p_internal_name=>'SMALL_CALENDER'
,p_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellspacing="0" cellpadding="0" border="0" summary="" class="t20SmallCalenderHolder"> ',
' <tr>',
'   <td class="t20MonthTitle">#IMONTH# #YYYY#</td>',
' </tr>',
' <tr>',
' <td class="t20MonthBody">'))
,p_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="0" class="t20SmallCalender">'
,p_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table></td>',
'</tr>',
'</table>',
''))
,p_day_title_format=>'<div class="t20DayTitle">#DD#</div>'
,p_day_open_format=>'<td class="t20Day" valign="top">#TITLE_FORMAT##DATA#'
,p_day_close_format=>'</td>'
,p_today_open_format=>'<td valign="top" class="t20Today">#TITLE_FORMAT##DATA#'
,p_weekend_title_format=>'<div class="t20WeekendDayTitle">#DD#</div>'
,p_weekend_open_format=>'<td valign="top" class="t20WeekendDay">#TITLE_FORMAT##DATA#'
,p_weekend_close_format=>'</td>'
,p_nonday_title_format=>'<div class="t20NonDayTitle">#DD#</div>'
,p_nonday_open_format=>'<td class="t20NonDay" valign="top">'
,p_nonday_close_format=>'</td>'
,p_week_open_format=>'<tr>'
,p_week_close_format=>'</tr> '
,p_daily_title_format=>'<th width="14%" class="calheader">#IDAY#</th>'
,p_daily_open_format=>'<tr>'
,p_daily_close_format=>'</tr>'
,p_month_data_format=>'#DAYS#'
,p_month_data_entry_format=>'#DATA#'
,p_theme_id=>20
,p_theme_class_id=>3
);
end;
/
prompt --application/shared_components/user_interface/themes
begin
wwv_flow_api.create_theme(
 p_id=>wwv_flow_api.id(15767703761318703)
,p_theme_id=>20
,p_theme_name=>'Traditional Blue'
,p_theme_internal_name=>'TRADITIONAL_BLUE'
,p_ui_type_name=>'DESKTOP'
,p_navigation_type=>'T'
,p_nav_bar_type=>'NAVBAR'
,p_is_locked=>false
,p_default_page_template=>wwv_flow_api.id(15759820337318668)
,p_error_template=>wwv_flow_api.id(15759820337318668)
,p_printer_friendly_template=>wwv_flow_api.id(15760512316318672)
,p_breadcrumb_display_point=>'REGION_POSITION_01'
,p_login_template=>wwv_flow_api.id(15759497602318660)
,p_default_button_template=>wwv_flow_api.id(15760994264318674)
,p_default_region_template=>wwv_flow_api.id(15762904053318681)
,p_default_chart_template=>wwv_flow_api.id(15761905169318681)
,p_default_form_template=>wwv_flow_api.id(15761996439318681)
,p_default_reportr_template=>wwv_flow_api.id(15762904053318681)
,p_default_tabform_template=>wwv_flow_api.id(15762904053318681)
,p_default_wizard_template=>wwv_flow_api.id(15763515329318682)
,p_default_menur_template=>wwv_flow_api.id(15761622021318681)
,p_default_listr_template=>wwv_flow_api.id(15762904053318681)
,p_default_report_template=>wwv_flow_api.id(15765798019318690)
,p_default_label_template=>wwv_flow_api.id(15766501526318693)
,p_default_menu_template=>wwv_flow_api.id(15766804687318693)
,p_default_calendar_template=>wwv_flow_api.id(15767014181318697)
,p_default_list_template=>wwv_flow_api.id(15765207619318687)
,p_default_option_label=>wwv_flow_api.id(15766501526318693)
,p_default_required_label=>wwv_flow_api.id(15766691312318693)
,p_default_page_transition=>'NONE'
,p_default_popup_transition=>'NONE'
,p_file_prefix => nvl(wwv_flow_application_install.get_static_theme_file_prefix(20),'')
,p_css_file_urls=>'#IMAGE_PREFIX#legacy_ui/css/5.0#MIN#.css?v=#APEX_VERSION#'
);
end;
/
prompt --application/shared_components/user_interface/theme_style
begin
null;
end;
/
prompt --application/shared_components/user_interface/theme_files
begin
null;
end;
/
prompt --application/shared_components/user_interface/theme_display_points
begin
null;
end;
/
prompt --application/shared_components/user_interface/template_opt_groups
begin
null;
end;
/
prompt --application/shared_components/user_interface/template_options
begin
null;
end;
/
prompt --application/shared_components/logic/build_options
begin
null;
end;
/
prompt --application/shared_components/globalization/language
begin
null;
end;
/
prompt --application/shared_components/globalization/translations
begin
null;
end;
/
prompt --application/shared_components/globalization/messages
begin
null;
end;
/
prompt --application/shared_components/globalization/dyntranslations
begin
null;
end;
/
prompt --application/shared_components/user_interface/shortcuts
begin
wwv_flow_api.create_shortcut(
 p_id=>wwv_flow_api.id(15777114129448854)
,p_shortcut_name=>'DELETE_CONFIRM_MSG'
,p_shortcut_type=>'TEXT_ESCAPE_JS'
,p_shortcut=>'Would you like to perform this delete action?'
);
wwv_flow_api.create_shortcut(
 p_id=>wwv_flow_api.id(15794803810525386)
,p_shortcut_name=>'OK_TO_GET_NEXT_PREV_PK_VALUE'
,p_shortcut_type=>'TEXT_ESCAPE_JS'
,p_shortcut=>'Are you sure you want to leave this page without saving?'
);
end;
/
prompt --application/shared_components/security/authentications
begin
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(3318331573878171)
,p_name=>'Web SSO - Entrust GetAccess'
,p_scheme_type=>'NATIVE_HTTP_HEADER_VARIABLE'
,p_attribute_01=>'USER'
,p_attribute_02=>'BUILTIN_URL'
,p_attribute_06=>'CALLBACK'
,p_logout_url=>'https://shdwsso1awpl.hdsupply.com:50002/GetAccess/Logout'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(3318628110894903)
,p_name=>'HDSOID'
,p_scheme_type=>'NATIVE_IAS_SSO'
,p_attribute_02=>'http://shdoam01lql.hsi.hughessupply.com:8000/pls/orasso/orasso.wwsso_app_admin.ls_logout'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(15767802763318724)
,p_name=>'Application Express'
,p_scheme_type=>'NATIVE_APEX_ACCOUNTS'
,p_attribute_15=>'2311411467347674'
,p_invalid_session_type=>'LOGIN'
,p_logout_url=>'f?p=&APP_ID.:1'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
,p_comments=>'Use internal Application Express account credentials and login page in this application.'
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(15767906775318725)
,p_name=>'DATABASE'
,p_scheme_type=>'NATIVE_DAD'
,p_attribute_15=>'2311515479347675'
,p_invalid_session_type=>'LOGIN'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
,p_comments=>'Use database authentication (user identified by DAD).'
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(15768002240318726)
,p_name=>'DATABASE ACCOUNT'
,p_scheme_type=>'NATIVE_DB_ACCOUNTS'
,p_attribute_15=>'2311610944347676'
,p_invalid_session_type=>'LOGIN'
,p_logout_url=>'f?p=&APP_ID.:1'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
,p_comments=>'Use database account credentials.'
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(15771017878335428)
,p_name=>'SSO'
,p_scheme_type=>'NATIVE_IAS_SSO'
,p_attribute_02=>'http://ofid.hsi.hughessupply.com:8000/pls/orasso/orasso.wwsso_app_admin.ls_logout'
,p_attribute_15=>'2314626582364378'
,p_logout_url=>'http://apps.hdsupply.net/pls/apex/f?p=&APP_ID.'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
,p_comments=>'Based on authentication scheme from gallery:Oracle Application Server Single Sign-On (Application Express as Partner Application)'
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(16258416179699431)
,p_name=>'Entrust GetAccess - Web SSO'
,p_scheme_type=>'NATIVE_HTTP_HEADER_VARIABLE'
,p_attribute_01=>'USER'
,p_attribute_02=>'BUILTIN_URL'
,p_attribute_06=>'CALLBACK'
,p_logout_url=>'	https://access.hdsupply.com/GetAccess/Logout?GAURI=http%3A%2F%2Feaapps.hdsupply.net%2Fpls%2Feaapxprd%2Ff%3Fp%3D102'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
);
end;
/
prompt --application/ui_types
begin
null;
end;
/
prompt --application/user_interfaces
begin
wwv_flow_api.create_user_interface(
 p_id=>wwv_flow_api.id(13520799422501392)
,p_ui_type_name=>'DESKTOP'
,p_display_name=>'Desktop'
,p_display_seq=>10
,p_use_auto_detect=>true
,p_is_default=>true
,p_theme_id=>20
,p_home_url=>'f?p=&APP_ID.:1:&SESSION.'
,p_theme_style_by_user_pref=>false
,p_global_page_id=>0
,p_nav_list_template_options=>'#DEFAULT#'
,p_include_legacy_javascript=>true
,p_include_jquery_migrate=>true
,p_nav_bar_type=>'NAVBAR'
,p_nav_bar_template_options=>'#DEFAULT#'
);
end;
/
prompt --application/user_interfaces/combined_files
begin
null;
end;
/
prompt --application/pages/page_00000
begin
wwv_flow_api.create_page(
 p_id=>0
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_name=>'0'
,p_page_mode=>'NORMAL'
,p_step_title=>'0'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_last_updated_by=>'MR024211'
,p_last_upd_yyyymmddhh24miss=>'20120329102913'
);
end;
/
prompt --application/pages/page_00001
begin
wwv_flow_api.create_page(
 p_id=>1
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'Main'
,p_page_mode=>'NORMAL'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Page 0'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20150919160638'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9062757797967391)
,p_plug_name=>'Master List of Application'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''2'' PAGETYPE,',
'workspace, ',
'application_id, ',
'application_name',
'from apex_applications',
'where availability_status != ''Unavailable''',
'and application_id not in (:APP_ID,101)'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.user_id',
'FROM  xxwc.xxwc_ms_user_roles_tbl a',
',     xxwc.xxwc_ms_roles_tbl b',
',     xxwc.xxwc_ms_role_resp_tbl c',
',     xxwc.xxwc_ms_responsibility_tbl d',
'WHERE a.role_id = b.role_id(+)',
'AND   b.role_id = c.role_id(+)',
'AND   c.responsibility_id = d.responsibility_id(+)',
'AND   a.user_id= :APP_USER',
'and d.app_id = :APP_ID',
'and c.app_id = :APP_ID',
'and b.role_name = ''Oracle Developers'''))
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(9062856867967391)
,p_name=>'Master List of Application'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML:EMAIL'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'GP050872'
,p_internal_uid=>4480425431581274
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(9063049612967394)
,p_db_column_name=>'WORKSPACE'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Workspace'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'WORKSPACE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(9063260026967394)
,p_db_column_name=>'APPLICATION_ID'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Application Id'
,p_column_link=>'f?p=&APP_ID.:#PAGETYPE#:&SESSION.::&DEBUG.:#PAGETYPE#:P#PAGETYPE#_APP_ID:#APPLICATION_ID#'
,p_column_linktext=>'#APPLICATION_ID#'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'APPLICATION_ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(9063350061967394)
,p_db_column_name=>'APPLICATION_NAME'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Application Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'APPLICATION_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(13778401199654234)
,p_db_column_name=>'PAGETYPE'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Pagetype'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PAGETYPE'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(9072846285983057)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'44905'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'APPLICATION_ID:WORKSPACE:APPLICATION_NAME:PAGETYPE'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15769621335318768)
,p_plug_name=>'Template'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15762904053318681)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_display_condition_type=>'NEVER'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15769796016318776)
,p_plug_name=>'Breadcrumbs'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_item_display_point=>'BELOW'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
end;
/
prompt --application/pages/page_00002
begin
wwv_flow_api.create_page(
 p_id=>2
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'Administration'
,p_alias=>'ADMINISTRATION'
,p_page_mode=>'NORMAL'
,p_step_title=>'Administration'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20150918155042'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9125347082659423)
,p_plug_name=>'Application Link'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>21
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_list_id=>wwv_flow_api.id(9124834269655705)
,p_plug_source_type=>'NATIVE_LIST'
,p_list_template_id=>wwv_flow_api.id(15765207619318687)
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15773995093404202)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_item_display_point=>'BELOW'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15807609988588516)
,p_plug_name=>'Super Administration'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15762904053318681)
,p_plug_display_sequence=>11
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_list_id=>wwv_flow_api.id(15773317279392035)
,p_plug_source_type=>'NATIVE_LIST'
,p_list_template_id=>wwv_flow_api.id(15763992192318686)
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9072445005972901)
,p_name=>'P2_APP_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(15807609988588516)
,p_use_cache_before_default=>'NO'
,p_item_default=>'127'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_computation(
 p_id=>wwv_flow_api.id(13779618428446884)
,p_computation_sequence=>10
,p_computation_item=>'G_LASTPAGE'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'STATIC_ASSIGNMENT'
,p_computation=>'2'
);
end;
/
prompt --application/pages/page_00004
begin
wwv_flow_api.create_page(
 p_id=>4
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'AIS Lite Dashboard'
,p_page_mode=>'NORMAL'
,p_step_title=>'AIS Lite Dashboard'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20170613174322'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3862420894886829)
,p_plug_name=>'Reports'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15762904053318681)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3865901997042236)
,p_name=>'Threshold Fallouts Old'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.organization_code,',
'       c.organization_name,',
'       d.description "User Name",',
'       a.item_desc,',
'       a.order_number,',
'       a.created_by,',
'       a.query_start_time,',
'       b.query_end_time,',
'       b.row_count,',
'       b.sequence_id,',
'      trunc(60* 60* 24 * (b.query_end_time - a.query_start_time),2) runtime,',
'      a.where_clause,',
'      a.order_by',
'  FROM (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_start_time,',
'               a.org_id,',
'               a.where_clause,',
'               a.order_by',
'          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'         WHERE a.creation_date BETWEEN SYSDATE - 9 AND SYSDATE ',
'         and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'       (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_end_time,',
'               a.org_id,',
'               a.row_count,',
'               a.where_clause',
'          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'         WHERE a.creation_date BETWEEN SYSDATE - 9 AND SYSDATE',
'         and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'       apps.org_organization_definitions c,',
'       apps.fnd_user d',
'WHERE     a.item_desc = b.item_desc',
'       AND NVL (a.order_number, 0) = NVL (b.order_number, 0)',
'       AND A.ORG_ID = B.ORG_ID',
'       AND A.CREATED_BY = B.CREATED_BY',
'       AND a.org_id = c.organization_id(+)',
'       AND a.created_by = d.user_id(+)',
'       AND B.sequence_id = A.sequence_id + 1',
'      and a.where_clause = b.where_clause',
'      and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'    and (b.query_end_time is not null and a.query_start_time is  not null)',
'order by organization_code,query_start_time'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3866210927042243)
,p_query_column_id=>1
,p_column_alias=>'ORGANIZATION_CODE'
,p_column_display_sequence=>13
,p_column_heading=>'Org Code'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3866305178042243)
,p_query_column_id=>2
,p_column_alias=>'ORGANIZATION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'ORGANIZATION_NAME'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3866426923042243)
,p_query_column_id=>3
,p_column_alias=>'User Name'
,p_column_display_sequence=>2
,p_column_heading=>'User Name'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3866531364042243)
,p_query_column_id=>4
,p_column_alias=>'ITEM_DESC'
,p_column_display_sequence=>3
,p_column_heading=>'Search String'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3866631899042243)
,p_query_column_id=>5
,p_column_alias=>'ORDER_NUMBER'
,p_column_display_sequence=>4
,p_column_heading=>'ORDER_NUMBER'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3866724943042243)
,p_query_column_id=>6
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>5
,p_column_heading=>'CREATED_BY'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3866805665042243)
,p_query_column_id=>7
,p_column_alias=>'QUERY_START_TIME'
,p_column_display_sequence=>8
,p_column_heading=>'Search TimeStamp'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH:MIPM'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3866916511042243)
,p_query_column_id=>8
,p_column_alias=>'QUERY_END_TIME'
,p_column_display_sequence=>6
,p_column_heading=>'QUERY_END_TIME'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3867018041042243)
,p_query_column_id=>9
,p_column_alias=>'ROW_COUNT'
,p_column_display_sequence=>7
,p_column_heading=>'# of Records Returned'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3867132703042243)
,p_query_column_id=>10
,p_column_alias=>'SEQUENCE_ID'
,p_column_display_sequence=>9
,p_column_heading=>'SEQUENCE_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3867230091042243)
,p_query_column_id=>11
,p_column_alias=>'RUNTIME'
,p_column_display_sequence=>10
,p_column_heading=>'Search Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3867330180042243)
,p_query_column_id=>12
,p_column_alias=>'WHERE_CLAUSE'
,p_column_display_sequence=>11
,p_column_heading=>'Where Clause'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3867411800042243)
,p_query_column_id=>13
,p_column_alias=>'ORDER_BY'
,p_column_display_sequence=>12
,p_column_heading=>'Order By'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3870825676408622)
,p_plug_name=>'Weekly View - Graph New'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761905169318681)
,p_plug_display_sequence=>60
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_source_type=>'NATIVE_FLASH_CHART5'
,p_plug_query_row_template=>1
);
wwv_flow_api.create_flash_chart5(
 p_id=>wwv_flow_api.id(3871010793408622)
,p_default_chart_type=>'2DLine'
,p_chart_title=>'Weekly View'
,p_chart_rendering=>'FLASH_PREFERRED'
,p_chart_name=>'chart_3871010793408622'
,p_chart_width=>700
,p_chart_height=>500
,p_chart_animation=>'N'
,p_display_attr=>':H:N:V:X:N:Bottom::V:Y:Circle:::N:::Default:::S'
,p_dial_tick_attr=>':::::::::::'
,p_gantt_attr=>'Y:Rhomb:Rhomb:Full:Rhomb:Rhomb:Full:Rhomb:Rhomb:Full:30:15:5:Y:I:N:S:E::'
,p_pie_attr=>'Outside:::'
,p_map_attr=>'Orthographic:RegionBounds:REGION_NAME'
,p_map_source=>'%'
,p_margins=>':::'
, p_omit_label_interval=> null
,p_bgtype=>'Trans'
,p_color_scheme=>'6'
,p_x_axis_title=>'Weeks'
,p_x_axis_label_font=>'Tahoma:10:#000000'
,p_y_axis_title=>'Time'
,p_y_axis_label_font=>'Tahoma:10:#000000'
,p_async_update=>'N'
,p_legend_title=>'Speed'
, p_names_font=> null
, p_names_rotation=> null
,p_values_font=>'Tahoma:10:#000000'
,p_hints_font=>'Tahoma:10:#000000'
,p_legend_font=>'Tahoma:10:#000000'
,p_grid_labels_font=>'Tahoma:10:#000000'
,p_chart_title_font=>'Tahoma:14:#000000'
,p_x_axis_title_font=>'Tahoma:14:#000000'
,p_y_axis_title_font=>'Tahoma:14:#000000'
,p_gauge_labels_font=>'Tahoma:10:#000000'
,p_use_chart_xml=>'N'
);
wwv_flow_api.create_flash_chart5_series(
 p_id=>wwv_flow_api.id(3871108937408624)
,p_chart_id=>wwv_flow_api.id(3871010793408622)
,p_series_seq=>10
,p_series_name=>'Series 1'
,p_series_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select  null link,CASE',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 7 and to_date(sysdate,''DD-MM-YYYY'') then ''Week1''',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 14 and to_date(sysdate,''DD-MM-YYYY'') - 7 then ''Week2''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 21 and to_date(sysdate,''DD-MM-YYYY'') - 14 then ''Week3''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'') - 21 then ''Week4''',
'                 END creation_date ,',
'trunc(AVG(60* 60* 24 * (query_end_time - query_start_time)),2) avg_time ,',
'max(trunc(60* 60* 24 * (query_end_time - query_start_time),2)) max_time',
'from (',
'SELECT c.organization_code,',
'       c.organization_name,',
'       d.description "User Name",',
'       a.item_desc,',
'       a.order_number,',
'       a.created_by,',
'       a.query_start_time,',
'       b.query_end_time,',
'       b.row_count,',
'       b.sequence_id,',
'      trunc(60* 60* 24 * (b.query_end_time - a.query_start_time),2) runtime,',
'      a.where_clause,',
'      a.order_by',
'  FROM (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_start_time,',
'               a.org_id,',
'               a.where_clause,',
'               a.order_by',
'          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'         WHERE to_date(a.creation_date,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'')',
'         and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'       (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_end_time,',
'               a.org_id,',
'               a.row_count,',
'               a.where_clause',
'          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'         WHERE to_date(a.creation_date,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'')',
'         and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'       apps.org_organization_definitions c,',
'       apps.fnd_user d',
'WHERE     a.item_desc = b.item_desc',
'       AND NVL (a.order_number, 0) = NVL (b.order_number, 0)',
'       AND A.ORG_ID = B.ORG_ID',
'       AND A.CREATED_BY = B.CREATED_BY',
'       AND a.org_id = c.organization_id(+)',
'       AND a.created_by = d.user_id(+)',
'       AND B.sequence_id = A.sequence_id',
'      and a.where_clause = b.where_clause',
'     -- and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'    and (b.query_end_time is not null and a.query_start_time is  not null)',
'',
'order by organization_code,query_start_time  )',
'--group by row_count',
'group by (CASE',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 7 and to_date(sysdate,''DD-MM-YYYY'') then ''Week1''',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 14 and to_date(sysdate,''DD-MM-YYYY'') - 7 then ''Week2''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 21 and to_date(sysdate,''DD-MM-YYYY'') - 14 then ''Week3''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'') - 21 then ''Week4''',
'                 END )',
'union ',
'select  null link,CASE',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 7 and to_date(sysdate,''DD-MM-YYYY'') then ''Week1''',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 14 and to_date(sysdate,''DD-MM-YYYY'') - 7 then ''Week2''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 21 and to_date(sysdate,''DD-MM-YYYY'') - 14 then ''Week3''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'') - 21 then ''Week4''',
'                 END creation_date ,',
'trunc(AVG(60* 60* 24 * (query_end_time - query_start_time)),2) avg_time ,',
'max(trunc(60* 60* 24 * (query_end_time - query_start_time),2)) max_time',
'from (',
'SELECT c.organization_code,',
'       c.organization_name,',
'       d.description "User Name",',
'       a.item_desc,',
'       a.order_number,',
'       a.created_by,',
'       a.query_start_time,',
'       b.query_end_time,',
'       b.row_count,',
'       b.sequence_id,',
'      trunc(60* 60* 24 * (b.query_end_time - a.query_start_time),2) runtime,',
'      a.where_clause,',
'      a.order_by',
'  FROM (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_start_time,',
'               a.org_id,',
'               a.where_clause,',
'               a.order_by',
'          FROM XXWC.XXWC_AIS_SEARCH_ARCH_TBL a',
'         WHERE to_date(a.creation_date,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'')',
'        and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'       (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_end_time,',
'               a.org_id,',
'               a.row_count,',
'               a.where_clause',
'          FROM XXWC.XXWC_AIS_SEARCH_ARCH_TBL a',
'         WHERE to_date(a.creation_date,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'')',
'         and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'       apps.org_organization_definitions c,',
'       apps.fnd_user d',
'WHERE     a.item_desc = b.item_desc',
'       AND NVL (a.order_number, 0) = NVL (b.order_number, 0)',
'       AND A.ORG_ID = B.ORG_ID',
'       AND A.CREATED_BY = B.CREATED_BY',
'       AND a.org_id = c.organization_id(+)',
'       AND a.created_by = d.user_id(+)',
'       AND B.sequence_id = A.sequence_id',
'      and a.where_clause = b.where_clause',
'     -- and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'    and (b.query_end_time is not null and a.query_start_time is  not null)',
'',
'order by organization_code,query_start_time  )',
'--group by row_count',
'group by (CASE',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 7 and to_date(sysdate,''DD-MM-YYYY'') then ''Week1''',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 14 and to_date(sysdate,''DD-MM-YYYY'') - 7 then ''Week2''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 21 and to_date(sysdate,''DD-MM-YYYY'') - 14 then ''Week3''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'') - 21 then ''Week4''',
'                 END )'))
,p_series_type=>'Line'
,p_series_query_type=>'SQL_QUERY'
,p_series_query_parse_opt=>'PARSE_CHART_QUERY'
,p_series_query_no_data_found=>'No data found.'
,p_series_query_row_count_max=>15
,p_show_action_link=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3874322410596820)
,p_name=>'Metrics By Time Blocks - Today'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COUNT_REC,',
'       sum_of_records,',
'       run_time,',
'       creation_date,',
'       max_time,',
'       timestamp,',
'       (SELECT SEQUENCE_ID',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date,''DD-MM-YYYY'') = to_date(sysdate,''DD-MM-YYYY'')) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date,''DD-MM-YYYY'') = to_date(sysdate,''DD-MM-YYYY'')) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time and rownum = 1) SEQUENCE_ID,',
'       (SELECT ITEM_DESC',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE  to_date(a.creation_date,''DD-MM-YYYY'') = to_date(sysdate,''DD-MM-YYYY'')) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE  to_date(a.creation_date,''DD-MM-YYYY'') = to_date(sysdate,''DD-MM-YYYY'')) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                      -- and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                ',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time and rownum = 1) ITEM_DESC',
'  FROM (select count(*) COUNT_REC,',
'               sum(row_count) sum_of_records,',
'               trunc(AVG(60 * 60 * 24 * (query_end_time - query_start_time)),',
'                     2) run_time,',
'               CASE',
'                 WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                  ''6am-10am''',
'                 WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                  ''10am-2pm''',
'                 when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                  ''2pm-6pm''',
'                 when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                  ''6pm-10pm''',
'               END creation_date,',
'               max(trunc(60 * 60 * 24 * (query_end_time - query_start_time),',
'                         2)) max_time,',
'               max(TRUNC(query_end_time - query_start_time, 5)) max_dts,',
'               max(query_start_time) timestamp',
'          from (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE  to_date(a.creation_date,''DD-MM-YYYY'') = to_date(sysdate,''DD-MM-YYYY''))  a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE  to_date(a.creation_date,''DD-MM-YYYY'') = to_date(sysdate,''DD-MM-YYYY''))  b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                ',
'                 order by organization_code, query_start_time)',
'         group by (CASE',
'                    WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                     ''6am-10am''',
'                    WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                     ''10am-2pm''',
'                    when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                     ''2pm-6pm''',
'                    when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                     ''6pm-10pm''',
'                  END))',
' order by timestamp desc'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3874627750596822)
,p_query_column_id=>1
,p_column_alias=>'COUNT_REC'
,p_column_display_sequence=>6
,p_column_heading=>'Count rec'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3874724515596822)
,p_query_column_id=>2
,p_column_alias=>'SUM_OF_RECORDS'
,p_column_display_sequence=>2
,p_column_heading=>'Sum of # of records returned'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3874814646596822)
,p_query_column_id=>3
,p_column_alias=>'RUN_TIME'
,p_column_display_sequence=>3
,p_column_heading=>'Avg Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3874922051596822)
,p_query_column_id=>4
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>1
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3875012225596822)
,p_query_column_id=>5
,p_column_alias=>'MAX_TIME'
,p_column_display_sequence=>4
,p_column_heading=>'Max Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3875118182596822)
,p_query_column_id=>6
,p_column_alias=>'TIMESTAMP'
,p_column_display_sequence=>5
,p_column_heading=>'Max Timestamp'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH:MIPM'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3875316998602530)
,p_query_column_id=>7
,p_column_alias=>'SEQUENCE_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Sequence Id'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3875426371602530)
,p_query_column_id=>8
,p_column_alias=>'ITEM_DESC'
,p_column_display_sequence=>8
,p_column_heading=>'Item Desc'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3875806526620599)
,p_name=>'Metrics By Records Returned New'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COUNT_REC,',
'       sum_of_records,',
'       run_time,',
'       -- creation_date,',
'       max_time,',
'       timestamp,',
'       (SELECT SEQUENCE_ID',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id ',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'                 and rownum = 1) SEQUENCE_ID,',
'       (SELECT ITEM_DESC',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id ',
'                   and a.where_clause = b.where_clause',
'                      -- and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                ',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time   and rownum = 1) ITEM_DESC',
'  FROM (select count(*) COUNT_REC,',
'               sum(row_count) sum_of_records,',
'               trunc(AVG(60 * 60 * 24 * (query_end_time - query_start_time)),',
'                     2) run_time,',
'               /* to_char(query_start_time, ''HH24'')  creation_date,*/',
'               CASE',
'                 WHEN row_count < 101 THEN',
'                  ''0-100''',
'                 WHEN row_count between 101 and 1000 THEN',
'                  ''101-1000''',
'                 when row_count > 1000 then',
'                  ''>1000''',
'               END row_count,',
'               max(trunc(60 * 60 * 24 * (query_end_time - query_start_time),',
'                         2)) max_time,',
'               max(TRUNC(query_end_time - query_start_time, 5)) max_dts,',
'               max(query_start_time) timestamp',
'          from (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id ',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                ',
'                 order by organization_code, query_start_time)',
'         group by (CASE',
'                    WHEN row_count < 101 THEN',
'                     ''0-100''',
'                    WHEN row_count between 101 and 1000 THEN',
'                     ''101-1000''',
'                    when row_count > 1000 then',
'                     ''>1000''',
'                  END))'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
end;
/
begin
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3876100040620602)
,p_query_column_id=>1
,p_column_alias=>'COUNT_REC'
,p_column_display_sequence=>6
,p_column_heading=>'Count rec'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3876216041620602)
,p_query_column_id=>2
,p_column_alias=>'SUM_OF_RECORDS'
,p_column_display_sequence=>1
,p_column_heading=>'Sum of # of records returned'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3876315697620602)
,p_query_column_id=>3
,p_column_alias=>'RUN_TIME'
,p_column_display_sequence=>2
,p_column_heading=>'Avg Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3876505538620602)
,p_query_column_id=>4
,p_column_alias=>'MAX_TIME'
,p_column_display_sequence=>3
,p_column_heading=>'Max Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3876620976620602)
,p_query_column_id=>5
,p_column_alias=>'TIMESTAMP'
,p_column_display_sequence=>4
,p_column_heading=>'Max Timestamp'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH:MIPM'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3876807281622275)
,p_query_column_id=>6
,p_column_alias=>'SEQUENCE_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Sequence Id'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3876901502622275)
,p_query_column_id=>7
,p_column_alias=>'ITEM_DESC'
,p_column_display_sequence=>7
,p_column_heading=>'Item Desc'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3877023495625528)
,p_name=>'Metrics By Time Blocks - Week New'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>45
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COUNT_REC,',
'       sum_of_records,',
'       run_time,',
'       creation_date,',
'       max_time,',
'       timestamp,',
'       (SELECT SEQUENCE_ID',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                                and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                                and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time and rownum = 1) SEQUENCE_ID,',
'       (SELECT ITEM_DESC',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                                and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                              and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id ',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                ',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time and rownum = 1) ITEM_DESC',
'  FROM (select count(*) COUNT_REC,',
'               sum(row_count) sum_of_records,',
'               trunc(AVG(60 * 60 * 24 * (query_end_time - query_start_time)),',
'                     2) run_time,',
'               CASE',
'                 WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                  ''6am-10am''',
'                 WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                  ''10am-2pm''',
'                 when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                  ''2pm-6pm''',
'                 when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                  ''6pm-10pm''',
'               END creation_date,',
'               max(trunc(60 * 60 * 24 * (query_end_time - query_start_time),',
'                         2)) max_time,',
'               max(TRUNC(query_end_time - query_start_time, 5)) max_dts,',
'               max(query_start_time) timestamp',
'          from (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                              and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                ',
'                 order by organization_code, query_start_time)',
'         group by (CASE',
'                    WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                     ''6am-10am''',
'                    WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                     ''10am-2pm''',
'                    when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                     ''2pm-6pm''',
'                    when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                     ''6pm-10pm''',
'                  END))',
' order by timestamp desc'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3877529051627346)
,p_query_column_id=>1
,p_column_alias=>'COUNT_REC'
,p_column_display_sequence=>6
,p_column_heading=>'Count Rec'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3877630659627346)
,p_query_column_id=>2
,p_column_alias=>'SUM_OF_RECORDS'
,p_column_display_sequence=>2
,p_column_heading=>'Sum of # of records returned'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3877725420627346)
,p_query_column_id=>3
,p_column_alias=>'RUN_TIME'
,p_column_display_sequence=>3
,p_column_heading=>'Avg Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3877803054627346)
,p_query_column_id=>4
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3877906492627346)
,p_query_column_id=>5
,p_column_alias=>'MAX_TIME'
,p_column_display_sequence=>4
,p_column_heading=>'Max Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3878006558627346)
,p_query_column_id=>6
,p_column_alias=>'TIMESTAMP'
,p_column_display_sequence=>5
,p_column_heading=>'Max Timestamp'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH:MIPM'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3878110127627346)
,p_query_column_id=>7
,p_column_alias=>'SEQUENCE_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Sequence Id'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3878214603627346)
,p_query_column_id=>8
,p_column_alias=>'ITEM_DESC'
,p_column_display_sequence=>8
,p_column_heading=>'Item Desc'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3878307350630348)
,p_name=>'Metrics By Time Blocks - Month New'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COUNT_REC,',
'       sum_of_records,',
'       run_time,',
'       creation_date,',
'       max_time,',
'       timestamp,',
'       (SELECT SEQUENCE_ID',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'           and rownum = 1) SEQUENCE_ID,',
'       (SELECT ITEM_DESC',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'           and rownum = 1) ITEM_DESC',
'  FROM (select count(*) COUNT_REC,',
'               sum(row_count) sum_of_records,',
'               trunc(AVG(60 * 60 * 24 * (query_end_time - query_start_time)),',
'                     2) run_time,',
'               CASE',
'                 WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                  ''6am-10am''',
'                 WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                  ''10am-2pm''',
'                 when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                  ''2pm-6pm''',
'                 when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                  ''6pm-10pm''',
'               END creation_date,',
'               max(trunc(60 * 60 * 24 * (query_end_time - query_start_time),',
'                         2)) max_time,',
'               max(TRUNC(query_end_time - query_start_time, 5)) max_dts,',
'               max(query_start_time) timestamp',
'          from (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time)',
'         group by (CASE',
'                    WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                     ''6am-10am''',
'                    WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                     ''10am-2pm''',
'                    when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                     ''2pm-6pm''',
'                    when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                     ''6pm-10pm''',
'                  END))',
'union',
'select COUNT_REC,',
'       sum_of_records,',
'       run_time,',
'       creation_date,',
'       max_time,',
'       timestamp,',
'       (SELECT SEQUENCE_ID',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and*/ a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE/* to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and*/ a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'           and rownum = 1) SEQUENCE_ID,',
'       (SELECT ITEM_DESC',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and */a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and*/ a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'           and rownum = 1) ITEM_DESC',
'  FROM (select count(*) COUNT_REC,',
'               sum(row_count) sum_of_records,',
'               trunc(AVG(60 * 60 * 24 * (query_end_time - query_start_time)),',
'                     2) run_time,',
'               CASE',
'                 WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                  ''6am-10am''',
'                 WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                  ''10am-2pm''',
'                 when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                  ''2pm-6pm''',
'                 when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                  ''6pm-10pm''',
'               END creation_date,',
'               max(trunc(60 * 60 * 24 * (query_end_time - query_start_time),',
'                         2)) max_time,',
'               max(TRUNC(query_end_time - query_start_time, 5)) max_dts,',
'               max(query_start_time) timestamp',
'          from (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and */ a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and*/ a.creation_date >',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time)',
'         group by (CASE',
'                    WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                     ''6am-10am''',
'                    WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                     ''10am-2pm''',
'                    when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                     ''2pm-6pm''',
'                    when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                     ''6pm-10pm''',
'                  END))',
' order by timestamp asc;'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
end;
/
begin
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3878810935635787)
,p_query_column_id=>1
,p_column_alias=>'COUNT_REC'
,p_column_display_sequence=>7
,p_column_heading=>'Count Rec'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3878924713635787)
,p_query_column_id=>2
,p_column_alias=>'SUM_OF_RECORDS'
,p_column_display_sequence=>2
,p_column_heading=>'Sum of # of records returned'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3879013713635787)
,p_query_column_id=>3
,p_column_alias=>'RUN_TIME'
,p_column_display_sequence=>3
,p_column_heading=>'Avg Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3879105846635787)
,p_query_column_id=>4
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>1
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3879218428635787)
,p_query_column_id=>5
,p_column_alias=>'MAX_TIME'
,p_column_display_sequence=>4
,p_column_heading=>'Max Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3879301968635787)
,p_query_column_id=>6
,p_column_alias=>'TIMESTAMP'
,p_column_display_sequence=>5
,p_column_heading=>'Max Timestamp'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH:MIPM'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3879421260635787)
,p_query_column_id=>7
,p_column_alias=>'SEQUENCE_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Sequence Id'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3879527154635787)
,p_query_column_id=>8
,p_column_alias=>'ITEM_DESC'
,p_column_display_sequence=>8
,p_column_heading=>'Item Desc'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3888712647764342)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3914225374604034)
,p_name=>'Threshold Fallouts New'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT c.organization_code,',
'       c.organization_name,',
'       d.description "User Name",',
'       a.item_desc,',
'       a.order_number,',
'       a.created_by,',
'       a.query_start_time,',
'       b.query_end_time,',
'       b.row_count,',
'       b.sequence_id,',
'      trunc(60* 60* 24 * (b.query_end_time - a.query_start_time),2) runtime,',
'      a.where_clause,',
'      a.order_by',
'  FROM (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_start_time,',
'               a.org_id,',
'               a.where_clause,',
'               a.order_by',
'          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'         WHERE a.creation_date BETWEEN SYSDATE - 9 AND SYSDATE ',
'         and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'       (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_end_time,',
'               a.org_id,',
'               a.row_count,',
'               a.where_clause',
'          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'         WHERE a.creation_date BETWEEN SYSDATE - 9 AND SYSDATE',
'         and a.creation_date > (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'       apps.org_organization_definitions c,',
'       apps.fnd_user d',
'WHERE     a.item_desc = b.item_desc',
'       AND NVL (a.order_number, 0) = NVL (b.order_number, 0)',
'       AND A.ORG_ID = B.ORG_ID',
'       AND A.CREATED_BY = B.CREATED_BY',
'       AND a.org_id = c.organization_id(+)',
'       AND a.created_by = d.user_id(+)',
'       AND B.sequence_id = A.sequence_id',
'      and a.where_clause = b.where_clause',
'      and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'    and (b.query_end_time is not null and a.query_start_time is  not null)',
'order by organization_code,query_start_time'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3914417331604035)
,p_query_column_id=>1
,p_column_alias=>'ORGANIZATION_CODE'
,p_column_display_sequence=>13
,p_column_heading=>'Org Code'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3914507421604036)
,p_query_column_id=>2
,p_column_alias=>'ORGANIZATION_NAME'
,p_column_display_sequence=>1
,p_column_heading=>'ORGANIZATION_NAME'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3914615671604036)
,p_query_column_id=>3
,p_column_alias=>'User Name'
,p_column_display_sequence=>2
,p_column_heading=>'User Name'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3914708790604036)
,p_query_column_id=>4
,p_column_alias=>'ITEM_DESC'
,p_column_display_sequence=>3
,p_column_heading=>'Search String'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3914806167604036)
,p_query_column_id=>5
,p_column_alias=>'ORDER_NUMBER'
,p_column_display_sequence=>4
,p_column_heading=>'ORDER_NUMBER'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3914900004604036)
,p_query_column_id=>6
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>5
,p_column_heading=>'CREATED_BY'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3915024349604036)
,p_query_column_id=>7
,p_column_alias=>'QUERY_START_TIME'
,p_column_display_sequence=>8
,p_column_heading=>'Search TimeStamp'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH:MIPM'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3915107140604036)
,p_query_column_id=>8
,p_column_alias=>'QUERY_END_TIME'
,p_column_display_sequence=>6
,p_column_heading=>'QUERY_END_TIME'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3915210126604036)
,p_query_column_id=>9
,p_column_alias=>'ROW_COUNT'
,p_column_display_sequence=>7
,p_column_heading=>'# of Records Returned'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3915316220604036)
,p_query_column_id=>10
,p_column_alias=>'SEQUENCE_ID'
,p_column_display_sequence=>9
,p_column_heading=>'SEQUENCE_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3915429902604036)
,p_query_column_id=>11
,p_column_alias=>'RUNTIME'
,p_column_display_sequence=>10
,p_column_heading=>'Search Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3915508968604036)
,p_query_column_id=>12
,p_column_alias=>'WHERE_CLAUSE'
,p_column_display_sequence=>11
,p_column_heading=>'Where Clause'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3915622166604036)
,p_query_column_id=>13
,p_column_alias=>'ORDER_BY'
,p_column_display_sequence=>12
,p_column_heading=>'Order By'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3917120788621673)
,p_name=>'Metrics By Records Returned Old'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COUNT_REC,',
'       sum_of_records,',
'       run_time,',
'       -- creation_date,',
'       max_time,',
'       timestamp,',
'       (SELECT SEQUENCE_ID',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'                 and rownum = 1) SEQUENCE_ID,',
'       (SELECT ITEM_DESC',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                      -- and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                ',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time   and rownum = 1) ITEM_DESC',
'  FROM (select count(*) COUNT_REC,',
'               sum(row_count) sum_of_records,',
'               trunc(AVG(60 * 60 * 24 * (query_end_time - query_start_time)),',
'                     2) run_time,',
'               /* to_char(query_start_time, ''HH24'')  creation_date,*/',
'               CASE',
'                 WHEN row_count < 101 THEN',
'                  ''0-100''',
'                 WHEN row_count between 101 and 1000 THEN',
'                  ''101-1000''',
'                 when row_count > 1000 then',
'                  ''>1000''',
'               END row_count,',
'               max(trunc(60 * 60 * 24 * (query_end_time - query_start_time),',
'                         2)) max_time,',
'               max(TRUNC(query_end_time - query_start_time, 5)) max_dts,',
'               max(query_start_time) timestamp',
'          from (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 9 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                ',
'                 order by organization_code, query_start_time)',
'         group by (CASE',
'                    WHEN row_count < 101 THEN',
'                     ''0-100''',
'                    WHEN row_count between 101 and 1000 THEN',
'                     ''101-1000''',
'                    when row_count > 1000 then',
'                     ''>1000''',
'                  END))'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3917326717621675)
,p_query_column_id=>1
,p_column_alias=>'COUNT_REC'
,p_column_display_sequence=>6
,p_column_heading=>'Count rec'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3917403176621675)
,p_query_column_id=>2
,p_column_alias=>'SUM_OF_RECORDS'
,p_column_display_sequence=>1
,p_column_heading=>'Sum of # of records returned'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3917507476621675)
,p_query_column_id=>3
,p_column_alias=>'RUN_TIME'
,p_column_display_sequence=>2
,p_column_heading=>'Avg Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3917629118621675)
,p_query_column_id=>4
,p_column_alias=>'MAX_TIME'
,p_column_display_sequence=>3
,p_column_heading=>'Max Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3917715276621675)
,p_query_column_id=>5
,p_column_alias=>'TIMESTAMP'
,p_column_display_sequence=>4
,p_column_heading=>'Max Timestamp'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH:MIPM'
,p_heading_alignment=>'LEFT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3917831132621675)
,p_query_column_id=>6
,p_column_alias=>'SEQUENCE_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Sequence Id'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3917917879621675)
,p_query_column_id=>7
,p_column_alias=>'ITEM_DESC'
,p_column_display_sequence=>7
,p_column_heading=>'Item Desc'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3919513908648007)
,p_name=>'Metrics By Time Blocks - Week Old'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>45
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COUNT_REC,',
'       sum_of_records,',
'       run_time,',
'       creation_date,',
'       max_time,',
'       timestamp,',
'       (SELECT SEQUENCE_ID',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                                and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                                and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time and rownum = 1) SEQUENCE_ID,',
'       (SELECT ITEM_DESC',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                                and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                              and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                      -- and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                ',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time and rownum = 1) ITEM_DESC',
'  FROM (select count(*) COUNT_REC,',
'               sum(row_count) sum_of_records,',
'               trunc(AVG(60 * 60 * 24 * (query_end_time - query_start_time)),',
'                     2) run_time,',
'               CASE',
'                 WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                  ''6am-10am''',
'                 WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                  ''10am-2pm''',
'                 when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                  ''2pm-6pm''',
'                 when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                  ''6pm-10pm''',
'               END creation_date,',
'               max(trunc(60 * 60 * 24 * (query_end_time - query_start_time),',
'                         2)) max_time,',
'               max(TRUNC(query_end_time - query_start_time, 5)) max_dts,',
'               max(query_start_time) timestamp',
'          from (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                               and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 7 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                              and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)               ',
'                 order by organization_code, query_start_time)',
'         group by (CASE',
'                    WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                     ''6am-10am''',
'                    WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                     ''10am-2pm''',
'                    when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                     ''2pm-6pm''',
'                    when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                     ''6pm-10pm''',
'                  END))',
'  order by timestamp desc'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
end;
/
begin
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3920010457648009)
,p_query_column_id=>1
,p_column_alias=>'COUNT_REC'
,p_column_display_sequence=>6
,p_column_heading=>'Count Rec'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3919911569648009)
,p_query_column_id=>2
,p_column_alias=>'SUM_OF_RECORDS'
,p_column_display_sequence=>2
,p_column_heading=>'Sum of # of records returned'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3920128940648009)
,p_query_column_id=>3
,p_column_alias=>'RUN_TIME'
,p_column_display_sequence=>3
,p_column_heading=>'Avg Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3920228672648009)
,p_query_column_id=>4
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3920324557648009)
,p_query_column_id=>5
,p_column_alias=>'MAX_TIME'
,p_column_display_sequence=>4
,p_column_heading=>'Max Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3920400790648009)
,p_query_column_id=>6
,p_column_alias=>'TIMESTAMP'
,p_column_display_sequence=>5
,p_column_heading=>'Max Timestamp'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH:MIPM'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3919708027648009)
,p_query_column_id=>7
,p_column_alias=>'SEQUENCE_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Sequence Id'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3919813192648009)
,p_query_column_id=>8
,p_column_alias=>'ITEM_DESC'
,p_column_display_sequence=>8
,p_column_heading=>'Item Desc'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3922010857826876)
,p_name=>'Metrics By Time Blocks - Month Old'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>55
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select COUNT_REC,',
'       sum_of_records,',
'       run_time,',
'       creation_date,',
'       max_time,',
'       timestamp,',
'       (SELECT SEQUENCE_ID',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'           and rownum = 1) SEQUENCE_ID,',
'       (SELECT ITEM_DESC',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'           and rownum = 1) ITEM_DESC',
'  FROM (select count(*) COUNT_REC,',
'               sum(row_count) sum_of_records,',
'               trunc(AVG(60 * 60 * 24 * (query_end_time - query_start_time)),',
'                     2) run_time,',
'               CASE',
'                 WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                  ''6am-10am''',
'                 WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                  ''10am-2pm''',
'                 when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                  ''2pm-6pm''',
'                 when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                  ''6pm-10pm''',
'               END creation_date,',
'               max(trunc(60 * 60 * 24 * (query_end_time - query_start_time),',
'                         2)) max_time,',
'               max(TRUNC(query_end_time - query_start_time, 5)) max_dts,',
'               max(query_start_time) timestamp',
'          from (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'                         WHERE to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time)',
'         group by (CASE',
'                    WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                     ''6am-10am''',
'                    WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                     ''10am-2pm''',
'                    when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                     ''2pm-6pm''',
'                    when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                     ''6pm-10pm''',
'                  END))',
'union',
'select COUNT_REC,',
'       sum_of_records,',
'       run_time,',
'       creation_date,',
'       max_time,',
'       timestamp,',
'       (SELECT SEQUENCE_ID',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and*/ a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and*/ a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'           and rownum = 1) SEQUENCE_ID,',
'       (SELECT ITEM_DESC',
'          FROM (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE/* to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and*/ a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and*/ a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time) inl',
'         WHERE trunc(60 * 60 * 24 * (query_end_time - query_start_time), 2) =',
'               max_time',
'           and rownum = 1) ITEM_DESC',
'  FROM (select count(*) COUNT_REC,',
'               sum(row_count) sum_of_records,',
'               trunc(AVG(60 * 60 * 24 * (query_end_time - query_start_time)),',
'                     2) run_time,',
'               CASE',
'                 WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                  ''6am-10am''',
'                 WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                  ''10am-2pm''',
'                 when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                  ''2pm-6pm''',
'                 when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                  ''6pm-10pm''',
'               END creation_date,',
'               max(trunc(60 * 60 * 24 * (query_end_time - query_start_time),',
'                         2)) max_time,',
'               max(TRUNC(query_end_time - query_start_time, 5)) max_dts,',
'               max(query_start_time) timestamp',
'          from (SELECT c.organization_code,',
'                       c.organization_name,',
'                       d.description "User Name",',
'                       a.item_desc,',
'                       a.order_number,',
'                       a.created_by,',
'                       a.query_start_time,',
'                       b.query_end_time,',
'                       b.row_count,',
'                       b.sequence_id,',
'                       trunc(60 * 60 * 24 *',
'                             (b.query_end_time - a.query_start_time),',
'                             2) runtime,',
'                       a.where_clause,',
'                       a.order_by',
'                  FROM (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_start_time,',
'                               a.org_id,',
'                               a.where_clause,',
'                               a.order_by',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and*/ a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'                       (SELECT sequence_id,',
'                               a.item_desc,',
'                               a.order_number,',
'                               a.created_by,',
'                               a.query_end_time,',
'                               a.org_id,',
'                               a.row_count,',
'                               a.where_clause',
'                          FROM APPS.XXWC_AIS_LOG_MV a',
'                         WHERE /*to_date(a.creation_date, ''DD-MM-YYYY'') between',
'                               to_date(sysdate, ''DD-MM-YYYY'') - 30 and',
'                               to_date(sysdate, ''DD-MM-YYYY'')',
'                           and */a.creation_date <',
'                               (select cutoff_date',
'                                  from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'                       apps.org_organization_definitions c,',
'                       apps.fnd_user d',
'                 WHERE a.item_desc = b.item_desc',
'                   AND NVL(a.order_number, 0) = NVL(b.order_number, 0)',
'                   AND A.ORG_ID = B.ORG_ID',
'                   AND A.CREATED_BY = B.CREATED_BY',
'                   AND a.org_id = c.organization_id(+)',
'                   AND a.created_by = d.user_id(+)',
'                   AND B.sequence_id = A.sequence_id + 1',
'                   and a.where_clause = b.where_clause',
'                   and (b.query_end_time is not null and',
'                       a.query_start_time is not null)',
'                 order by organization_code, query_start_time)',
'         group by (CASE',
'                    WHEN to_char(query_start_time, ''HH24'') between 06 and 09 then',
'                     ''6am-10am''',
'                    WHEN to_char(query_start_time, ''HH24'') between 10 and 13 then',
'                     ''10am-2pm''',
'                    when to_char(query_start_time, ''HH24'') between 14 and 17 then',
'                     ''2pm-6pm''',
'                    when to_char(query_start_time, ''HH24'') between 18 and 21 then',
'                     ''6pm-10pm''',
'                  END))',
' order by timestamp asc'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3922223373826877)
,p_query_column_id=>1
,p_column_alias=>'COUNT_REC'
,p_column_display_sequence=>7
,p_column_heading=>'Count Rec'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3922311559826877)
,p_query_column_id=>2
,p_column_alias=>'SUM_OF_RECORDS'
,p_column_display_sequence=>2
,p_column_heading=>'Sum of # of records returned'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3922406658826877)
,p_query_column_id=>3
,p_column_alias=>'RUN_TIME'
,p_column_display_sequence=>3
,p_column_heading=>'Avg Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3922516564826877)
,p_query_column_id=>4
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>1
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3922630674826877)
,p_query_column_id=>5
,p_column_alias=>'MAX_TIME'
,p_column_display_sequence=>4
,p_column_heading=>'Max Time'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3922715722826877)
,p_query_column_id=>6
,p_column_alias=>'TIMESTAMP'
,p_column_display_sequence=>5
,p_column_heading=>'Max Timestamp'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH:MIPM'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3922813339826877)
,p_query_column_id=>7
,p_column_alias=>'SEQUENCE_ID'
,p_column_display_sequence=>6
,p_column_heading=>'Sequence Id'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3922915930826878)
,p_query_column_id=>8
,p_column_alias=>'ITEM_DESC'
,p_column_display_sequence=>8
,p_column_heading=>'Item Desc'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3925131523974815)
,p_plug_name=>'Weekly View - Graph Old'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761905169318681)
,p_plug_display_sequence=>65
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_source_type=>'NATIVE_FLASH_CHART5'
,p_plug_query_row_template=>1
);
wwv_flow_api.create_flash_chart5(
 p_id=>wwv_flow_api.id(3925316973974815)
,p_default_chart_type=>'2DLine'
,p_chart_title=>'Weekly View'
,p_chart_rendering=>'FLASH_PREFERRED'
,p_chart_name=>'chart_3871010793408622'
,p_chart_width=>700
,p_chart_height=>500
,p_chart_animation=>'N'
,p_display_attr=>':H:N:V:X:N:Bottom::V:Y:Circle:::N:::Default:::S'
,p_dial_tick_attr=>':::::::::::'
,p_gantt_attr=>'Y:Rhomb:Rhomb:Full:Rhomb:Rhomb:Full:Rhomb:Rhomb:Full:30:15:5:Y:I:N:S:E::'
,p_pie_attr=>'Outside:::'
,p_map_attr=>'Orthographic:RegionBounds:REGION_NAME'
,p_map_source=>'%'
,p_margins=>':::'
, p_omit_label_interval=> null
,p_bgtype=>'Trans'
,p_color_scheme=>'6'
,p_x_axis_title=>'Weeks'
,p_x_axis_label_font=>'Tahoma:10:#000000'
,p_y_axis_title=>'Time'
,p_y_axis_label_font=>'Tahoma:10:#000000'
,p_async_update=>'N'
,p_legend_title=>'Speed'
, p_names_font=> null
, p_names_rotation=> null
,p_values_font=>'Tahoma:10:#000000'
,p_hints_font=>'Tahoma:10:#000000'
,p_legend_font=>'Tahoma:10:#000000'
,p_grid_labels_font=>'Tahoma:10:#000000'
,p_chart_title_font=>'Tahoma:14:#000000'
,p_x_axis_title_font=>'Tahoma:14:#000000'
,p_y_axis_title_font=>'Tahoma:14:#000000'
,p_gauge_labels_font=>'Tahoma:10:#000000'
,p_use_chart_xml=>'N'
);
wwv_flow_api.create_flash_chart5_series(
 p_id=>wwv_flow_api.id(3925428946974816)
,p_chart_id=>wwv_flow_api.id(3925316973974815)
,p_series_seq=>10
,p_series_name=>'Series 1'
,p_series_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select  null link,CASE',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 7 and to_date(sysdate,''DD-MM-YYYY'') then ''Week1''',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 14 and to_date(sysdate,''DD-MM-YYYY'') - 7 then ''Week2''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 21 and to_date(sysdate,''DD-MM-YYYY'') - 14 then ''Week3''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'') - 21 then ''Week4''',
'                 END creation_date ,',
'trunc(AVG(60* 60* 24 * (query_end_time - query_start_time)),2) avg_time ,',
'max(trunc(60* 60* 24 * (query_end_time - query_start_time),2)) max_time',
'from (',
'SELECT c.organization_code,',
'       c.organization_name,',
'       d.description "User Name",',
'       a.item_desc,',
'       a.order_number,',
'       a.created_by,',
'       a.query_start_time,',
'       b.query_end_time,',
'       b.row_count,',
'       b.sequence_id,',
'      trunc(60* 60* 24 * (b.query_end_time - a.query_start_time),2) runtime,',
'      a.where_clause,',
'      a.order_by',
'  FROM (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_start_time,',
'               a.org_id,',
'               a.where_clause,',
'               a.order_by',
'          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'         WHERE to_date(a.creation_date,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'')',
'         and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'       (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_end_time,',
'               a.org_id,',
'               a.row_count,',
'               a.where_clause',
'          FROM XXWC.XXWC_AIS_SEARCH_LOG_TBL a',
'         WHERE to_date(a.creation_date,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'')',
'         and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'       apps.org_organization_definitions c,',
'       apps.fnd_user d',
'WHERE     a.item_desc = b.item_desc',
'       AND NVL (a.order_number, 0) = NVL (b.order_number, 0)',
'       AND A.ORG_ID = B.ORG_ID',
'       AND A.CREATED_BY = B.CREATED_BY',
'       AND a.org_id = c.organization_id(+)',
'       AND a.created_by = d.user_id(+)',
'       AND B.sequence_id = A.sequence_id + 1',
'      and a.where_clause = b.where_clause',
'     -- and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'    and (b.query_end_time is not null and a.query_start_time is  not null)',
'',
'order by organization_code,query_start_time  )',
'--group by row_count',
'group by (CASE',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 7 and to_date(sysdate,''DD-MM-YYYY'') then ''Week1''',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 14 and to_date(sysdate,''DD-MM-YYYY'') - 7 then ''Week2''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 21 and to_date(sysdate,''DD-MM-YYYY'') - 14 then ''Week3''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'') - 21 then ''Week4''',
'                 END )',
'union ',
'select  null link,CASE',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 7 and to_date(sysdate,''DD-MM-YYYY'') then ''Week1''',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 14 and to_date(sysdate,''DD-MM-YYYY'') - 7 then ''Week2''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 21 and to_date(sysdate,''DD-MM-YYYY'') - 14 then ''Week3''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'') - 21 then ''Week4''',
'                 END creation_date ,',
'trunc(AVG(60* 60* 24 * (query_end_time - query_start_time)),2) avg_time ,',
'max(trunc(60* 60* 24 * (query_end_time - query_start_time),2)) max_time',
'from (',
'SELECT c.organization_code,',
'       c.organization_name,',
'       d.description "User Name",',
'       a.item_desc,',
'       a.order_number,',
'       a.created_by,',
'       a.query_start_time,',
'       b.query_end_time,',
'       b.row_count,',
'       b.sequence_id,',
'      trunc(60* 60* 24 * (b.query_end_time - a.query_start_time),2) runtime,',
'      a.where_clause,',
'      a.order_by',
'  FROM (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_start_time,',
'               a.org_id,',
'               a.where_clause,',
'               a.order_by',
'          FROM XXWC.XXWC_AIS_SEARCH_ARCH_TBL a',
'         WHERE to_date(a.creation_date,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'')',
'        and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) a,',
'       (SELECT sequence_id,',
'               a.item_desc,',
'               a.order_number,',
'               a.created_by,',
'               a.query_end_time,',
'               a.org_id,',
'               a.row_count,',
'               a.where_clause',
'          FROM XXWC.XXWC_AIS_SEARCH_ARCH_TBL a',
'         WHERE to_date(a.creation_date,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'')',
'         and a.creation_date < (select cutoff_date from xxwc.XXWC_AIS_LOOKUP_TBL)) b,',
'       apps.org_organization_definitions c,',
'       apps.fnd_user d',
'WHERE     a.item_desc = b.item_desc',
'       AND NVL (a.order_number, 0) = NVL (b.order_number, 0)',
'       AND A.ORG_ID = B.ORG_ID',
'       AND A.CREATED_BY = B.CREATED_BY',
'       AND a.org_id = c.organization_id(+)',
'       AND a.created_by = d.user_id(+)',
'       AND B.sequence_id = A.sequence_id + 1',
'      and a.where_clause = b.where_clause',
'     -- and       60* 60* 24 * (b.query_end_time - a.query_start_time) > 11',
'    and (b.query_end_time is not null and a.query_start_time is  not null)',
'',
'order by organization_code,query_start_time  )',
'--group by row_count',
'group by (CASE',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 7 and to_date(sysdate,''DD-MM-YYYY'') then ''Week1''',
'                   WHEN to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 14 and to_date(sysdate,''DD-MM-YYYY'') - 7 then ''Week2''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 21 and to_date(sysdate,''DD-MM-YYYY'') - 14 then ''Week3''',
'                   when to_date(query_start_time,''DD-MM-YYYY'') between to_date(sysdate,''DD-MM-YYYY'') - 30 and to_date(sysdate,''DD-MM-YYYY'') - 21 then ''Week4''',
'                 END )'))
,p_series_type=>'Line'
,p_series_query_type=>'SQL_QUERY'
,p_series_query_parse_opt=>'PARSE_CHART_QUERY'
,p_series_query_no_data_found=>'No data found.'
,p_series_query_row_count_max=>15
,p_show_action_link=>'N'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3864914320920106)
,p_name=>'P4_REPORTS'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(3862420894886829)
,p_prompt=>'Reports'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'REPORTS'
,p_lov=>'.'||wwv_flow_api.id(3862603626910295)||'.'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'- Select One -'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(15766501526318693)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3880413200679342)
,p_name=>'show hide reportsTF'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'TF'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3880726283679343)
,p_event_id=>wwv_flow_api.id(3880413200679342)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3865901997042236)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3880905483679344)
,p_event_id=>wwv_flow_api.id(3880413200679342)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3865901997042236)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3881208095682607)
,p_event_id=>wwv_flow_api.id(3880413200679342)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3865901997042236)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3881812423697988)
,p_name=>'show hide MBRR'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'MBRR'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3882308649697993)
,p_event_id=>wwv_flow_api.id(3881812423697988)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3917120788621673)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3882111532697993)
,p_event_id=>wwv_flow_api.id(3881812423697988)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3917120788621673)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3882502793697993)
,p_event_id=>wwv_flow_api.id(3881812423697988)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3917120788621673)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3882620388700318)
,p_name=>'show hide MBTBT'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'MBTBT'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3883102555700319)
,p_event_id=>wwv_flow_api.id(3882620388700318)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3874322410596820)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3882913398700318)
,p_event_id=>wwv_flow_api.id(3882620388700318)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3874322410596820)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3883307792700319)
,p_event_id=>wwv_flow_api.id(3882620388700318)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3874322410596820)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3883427314702299)
,p_name=>'show hide MBTBW'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'MBTBW'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3883910078702299)
,p_event_id=>wwv_flow_api.id(3883427314702299)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3919513908648007)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3883712171702299)
,p_event_id=>wwv_flow_api.id(3883427314702299)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3919513908648007)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3884113563702300)
,p_event_id=>wwv_flow_api.id(3883427314702299)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3919513908648007)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3884202165704515)
,p_name=>'show hide MBTBM'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'MBTBM'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3884708523704516)
,p_event_id=>wwv_flow_api.id(3884202165704515)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3922010857826876)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3884523928704515)
,p_event_id=>wwv_flow_api.id(3884202165704515)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3922010857826876)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3884929742704516)
,p_event_id=>wwv_flow_api.id(3884202165704515)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3922010857826876)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3885009783706748)
,p_name=>'show hide WVG'
,p_event_sequence=>60
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'WVG'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3885506968706749)
,p_event_id=>wwv_flow_api.id(3885009783706748)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3925131523974815)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3885302917706749)
,p_event_id=>wwv_flow_api.id(3885009783706748)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3925131523974815)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3885713153706749)
,p_event_id=>wwv_flow_api.id(3885009783706748)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3925131523974815)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3915925851613582)
,p_name=>'show hide reportsTF New'
,p_event_sequence=>70
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'TF'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3916431794613583)
,p_event_id=>wwv_flow_api.id(3915925851613582)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3914225374604034)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3916208085613582)
,p_event_id=>wwv_flow_api.id(3915925851613582)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3914225374604034)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3916608046613583)
,p_event_id=>wwv_flow_api.id(3915925851613582)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3914225374604034)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3918325421632392)
,p_name=>'show hide MBRR New'
,p_event_sequence=>80
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'MBRR'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3918830421632392)
,p_event_id=>wwv_flow_api.id(3918325421632392)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3875806526620599)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3918616210632392)
,p_event_id=>wwv_flow_api.id(3918325421632392)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3875806526620599)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3919003315632393)
,p_event_id=>wwv_flow_api.id(3918325421632392)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3875806526620599)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3920927910812887)
,p_name=>'show hide MBTBW New'
,p_event_sequence=>90
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'MBTBW'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3921429439812887)
,p_event_id=>wwv_flow_api.id(3920927910812887)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3877023495625528)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3921220149812887)
,p_event_id=>wwv_flow_api.id(3920927910812887)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3877023495625528)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3921608090812888)
,p_event_id=>wwv_flow_api.id(3920927910812887)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3877023495625528)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3923714376931974)
,p_name=>'show hide MBTBM New'
,p_event_sequence=>100
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'MBTBM'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3924230152931975)
,p_event_id=>wwv_flow_api.id(3923714376931974)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3878307350630348)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3924002778931975)
,p_event_id=>wwv_flow_api.id(3923714376931974)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3878307350630348)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3924429013931975)
,p_event_id=>wwv_flow_api.id(3923714376931974)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3878307350630348)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3925609799025284)
,p_name=>'show hide WVG New'
,p_event_sequence=>110
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P4_REPORTS'
,p_condition_element=>'P4_REPORTS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'WVG'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3926122292025285)
,p_event_id=>wwv_flow_api.id(3925609799025284)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3870825676408622)
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3925914345025284)
,p_event_id=>wwv_flow_api.id(3925609799025284)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3870825676408622)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3926318084025285)
,p_event_id=>wwv_flow_api.id(3925609799025284)
,p_event_result=>'FALSE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(3870825676408622)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
end;
/
prompt --application/pages/page_00005
begin
wwv_flow_api.create_page(
 p_id=>5
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'AIS dashboard lookup report'
,p_page_mode=>'NORMAL'
,p_step_title=>'AIS dashboard lookup report'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20170420160158'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3893701119816496)
,p_plug_name=>'AIS dashboard lookup report'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select "ID", ',
'"CUTOFF_DATE"',
'from "XXWC"."XXWC_AIS_LOOKUP_TBL" ',
'  ',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(3893929150816496)
,p_name=>'AIS dashboard lookup report'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'C'
,p_show_pivot=>'N'
,p_download_formats=>'CSV:HTML:EMAIL'
,p_detail_link=>'f?p=&APP_ID.:6:&APP_SESSION.::::P6_ID:#ID#'
,p_detail_link_text=>'<img src="#IMAGE_PREFIX#menu/pencil16x16.gif"  border="0">'
,p_owner=>'GP050872'
,p_internal_uid=>3893929150816496
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3894030223816501)
,p_db_column_name=>'ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Id'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_tz_dependent=>'N'
,p_static_id=>'ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3894130300816502)
,p_db_column_name=>'CUTOFF_DATE'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Cutoff Date'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'CUTOFF_DATE'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(3894531310816998)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'38946'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'ID:CUTOFF_DATE'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3895202707893910)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3894323513816503)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(3893701119816496)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Create'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:6::'
,p_button_condition=>'select ''x'' from XXWC.XXWC_AIS_LOOKUP_TBL;'
,p_button_condition_type=>'NOT_EXISTS'
,p_grid_new_grid=>false
);
end;
/
prompt --application/pages/page_00006
begin
wwv_flow_api.create_page(
 p_id=>6
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'AIS dashboard lookup form'
,p_page_mode=>'NORMAL'
,p_step_title=>'AIS dashboard lookup form'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20170420160245'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3890620744816478)
,p_plug_name=>'AIS dashboard lookup form'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761996439318681)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3896332621912042)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3890906175816479)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(3890620744816478)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Apply Changes'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P6_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3891224974816479)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(3890620744816478)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3890803829816479)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(3890620744816478)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P6_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_grid_new_grid=>false
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3891025262816479)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(3890620744816478)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'P6_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3891822574816480)
,p_branch_action=>'f?p=&APP_ID.:5:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3892024473816480)
,p_name=>'P6_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(3890620744816478)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Id'
,p_source=>'ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(15766501526318693)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3892209211816484)
,p_name=>'P6_CUTOFF_DATE'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(3890620744816478)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Cutoff Date'
,p_source=>'CUTOFF_DATE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>32
,p_cMaxlength=>255
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(15766501526318693)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3892619015816486)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from XXWC_AIS_LOOKUP_TBL'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_AIS_LOOKUP_TBL'
,p_attribute_03=>'P6_ID'
,p_attribute_04=>'ID'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3892807562816486)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get PK'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin ',
'    if :P6_ID is null then',
'        select XXWc."XXWC_AIS_LOOKUP_TBL_SEQ".nextval',
'          into :P6_ID',
'          from sys.dual;',
'    end if;',
'end;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(3890803829816479)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3893019955816486)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of XXWC_AIS_LOOKUP_TBL'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_AIS_LOOKUP_TBL'
,p_attribute_03=>'P6_ID'
,p_attribute_04=>'ID'
,p_attribute_11=>'I:U:D'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3893221857816486)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(3891025262816479)
);
end;
/
prompt --application/pages/page_00020
begin
wwv_flow_api.create_page(
 p_id=>20
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'Update RESPONSIBILITY'
,p_page_mode=>'NORMAL'
,p_step_title=>'Update RESPONSIBILITY'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20150918160231'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(4621809024079507)
,p_name=>'Responsibility Journal'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select XXWC_WC_RESP_JOURNAL.JOURNAL_ID as JOURNAL_ID,',
'    XXWC_WC_RESP_JOURNAL.RESP_ID as RESP_ID,',
'    XXWC_WC_RESP_JOURNAL.NOTES as NOTES,',
'    apps.XXWC_MS_MASTER_ADMIN_PKG.get_user_name(XXWC_WC_RESP_JOURNAL.CREATED_BY) as CREATED_BY,',
'    XXWC_WC_RESP_JOURNAL.CREATED_ON as CREATED_ON,',
'    XXWC_WC_RESP_JOURNAL.UPDATED_BY as UPDATED_BY,',
'    XXWC_WC_RESP_JOURNAL.UPDATED_ON as UPDATED_ON ',
' from xxwc.XXWC_MS_RESP_JOURNAL_TBL XXWC_WC_RESP_JOURNAL',
'where app_id = :P20_APP_ID',
'',
''))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4622108254079509)
,p_query_column_id=>1
,p_column_alias=>'JOURNAL_ID'
,p_column_display_sequence=>1
,p_column_heading=>'JOURNAL_ID'
,p_default_sort_column_sequence=>1
,p_default_sort_dir=>'desc'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4622206156079509)
,p_query_column_id=>2
,p_column_alias=>'RESP_ID'
,p_column_display_sequence=>2
,p_column_heading=>'RESP_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4622302890079509)
,p_query_column_id=>3
,p_column_alias=>'NOTES'
,p_column_display_sequence=>3
,p_column_heading=>'Notes'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4622416387079509)
,p_query_column_id=>4
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>4
,p_column_heading=>'Changed By'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4622506770079509)
,p_query_column_id=>5
,p_column_alias=>'CREATED_ON'
,p_column_display_sequence=>5
,p_column_heading=>'Date'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4622612209079509)
,p_query_column_id=>6
,p_column_alias=>'UPDATED_BY'
,p_column_display_sequence=>6
,p_column_heading=>'UPDATED_BY'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4622707180079509)
,p_query_column_id=>7
,p_column_alias=>'UPDATED_ON'
,p_column_display_sequence=>7
,p_column_heading=>'UPDATED_ON'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(15775818132448829)
,p_name=>'Maintain Responsibility'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"RESPONSIBILITY_ID",',
'"RESPONSIBILITY_ID" RESPONSIBILITY_ID_DISPLAY,',
'trim("RESPONSIBILITY_NAME") RESPONSIBILITY_NAME,',
'"APP_ID"',
'from xxwc.XXWC_MS_RESPONSIBILITY_TBL',
'WHERE APP_ID =:P20_APP_ID ',
''))
,p_source_type=>'NATIVE_TABFORM'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15778118764448862)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'&nbsp;'
,p_use_as_row_header=>'N'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15776096358448838)
,p_query_column_id=>2
,p_column_alias=>'RESPONSIBILITY_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Responsibility Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_pk_col_source=>'XXWC.XXWC_MS_RESPONSIBILITY_TBL'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_RESPONSIBILITY_TBL'
,p_ref_column_name=>'RESPONSIBILITY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15776207880448842)
,p_query_column_id=>3
,p_column_alias=>'RESPONSIBILITY_ID_DISPLAY'
,p_column_display_sequence=>2
,p_column_heading=>'Responsibility Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_RESPONSIBILITY_TBL'
,p_ref_column_name=>'RESPONSIBILITY_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3081100482344006)
,p_query_column_id=>4
,p_column_alias=>'RESPONSIBILITY_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Responsibility Name'
,p_use_as_row_header=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3082128539348776)
,p_query_column_id=>5
,p_column_alias=>'APP_ID'
,p_column_display_sequence=>5
,p_column_heading=>'App Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_column_default=>'P20_APP_ID'
,p_column_default_type=>'ITEM'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_RESPONSIBILITY_TBL'
,p_ref_column_name=>'APP_ID'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13459271631196415)
,p_column_sequence=>1
,p_query_column_name=>'RESPONSIBILITY_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13459368980196418)
,p_column_sequence=>2
,p_query_column_name=>'RESPONSIBILITY_ID_DISPLAY'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(3081023976344005)
,p_column_sequence=>3
,p_query_column_name=>'RESPONSIBILITY_NAME'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(3082016121348776)
,p_column_sequence=>4
,p_query_column_name=>'APP_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15778417558448870)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_item_display_point=>'BELOW'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15776718385448846)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(15775818132448829)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:addRow();'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15776498840448846)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(15775818132448829)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Submit'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15776398356448846)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(15775818132448829)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:&G_LASTPAGE.:&SESSION.::&DEBUG.::P2_APP_ID:&P20_APP_ID.'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15776601449448846)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(15775818132448829)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(15778211563448862)
,p_branch_action=>'f?p=&APP_ID.:20:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9073638931021329)
,p_name=>'P20_APP_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(15775818132448829)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P20_APP_ID'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15777692209448858)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(15775818132448829)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_RESPONSIBILITY_TBL'
,p_attribute_03=>'RESPONSIBILITY_ID'
,p_process_error_message=>'Unable to process update.'
,p_process_when_button_id=>wwv_flow_api.id(15776498840448846)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15777894295448859)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(15775818132448829)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_RESPONSIBILITY_TBL'
,p_attribute_03=>'RESPONSIBILITY_ID'
,p_process_error_message=>'Unable to process delete.'
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
end;
/
prompt --application/pages/page_00030
begin
wwv_flow_api.create_page(
 p_id=>30
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'Update ROLES'
,p_page_mode=>'NORMAL'
,p_step_title=>'Update ROLES'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20150918155042'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3317627627393275)
,p_plug_name=>'Verbaige'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_source=>'<font color = "red"><b>Please do not delete any role unless you are sure about it. As it will impact other applications.</b></font>'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(4620001228689347)
,p_name=>'Role Journal'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select xxwc_wc_roles_JOURNAL.JOURNAL_ID as JOURNAL_ID,',
'    xxwc_wc_roles_JOURNAL.ROLES_ID as ROLES_ID,',
'    xxwc_wc_roles_JOURNAL.NOTES as NOTES,',
'    xxwc_wc_roles_JOURNAL.CREATED_ON as CREATED_ON,',
'    apps.XXWC_MS_MASTER_ADMIN_PKG.get_user_name(xxwc_wc_roles_JOURNAL.CREATED_BY) as CREATED_BY,',
'    xxwc_wc_roles_JOURNAL.UPDATED_BY as UPDATED_BY,',
'    xxwc_wc_roles_JOURNAL.UPDATED_ON as UPDATED_ON ',
' from xxwc.xxwc_ms_roles_JOURNAL_tbl xxwc_wc_roles_JOURNAL'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4620305186689368)
,p_query_column_id=>1
,p_column_alias=>'JOURNAL_ID'
,p_column_display_sequence=>1
,p_column_heading=>'JOURNAL_ID'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_default_sort_dir=>'desc'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4620400528689368)
,p_query_column_id=>2
,p_column_alias=>'ROLES_ID'
,p_column_display_sequence=>2
,p_column_heading=>'ROLES_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4620519536689368)
,p_query_column_id=>3
,p_column_alias=>'NOTES'
,p_column_display_sequence=>3
,p_column_heading=>'Notes'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4620625939689368)
,p_query_column_id=>4
,p_column_alias=>'CREATED_ON'
,p_column_display_sequence=>5
,p_column_heading=>'Date'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4620717816689368)
,p_query_column_id=>5
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>4
,p_column_heading=>'Changed By'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4620819003689368)
,p_query_column_id=>6
,p_column_alias=>'UPDATED_BY'
,p_column_display_sequence=>6
,p_column_heading=>'UPDATED_BY'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4620914343689368)
,p_query_column_id=>7
,p_column_alias=>'UPDATED_ON'
,p_column_display_sequence=>7
,p_column_heading=>'UPDATED_ON'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(15779406451465333)
,p_name=>'Roles'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"ROLE_ID",',
'"ROLE_ID" ROLE_ID_DISPLAY,',
'"ROLE_NAME"',
'from XXWC.xxwc_ms_roles_tbl'))
,p_source_type=>'NATIVE_TABFORM'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15781608542465343)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'&nbsp;'
,p_use_as_row_header=>'N'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15779702618465337)
,p_query_column_id=>2
,p_column_alias=>'ROLE_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Role Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_pk_col_source=>'XXWC.XXWC_MS_ROLES_TRG'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLES_TBL'
,p_ref_column_name=>'ROLE_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15779814534465337)
,p_query_column_id=>3
,p_column_alias=>'ROLE_ID_DISPLAY'
,p_column_display_sequence=>2
,p_column_heading=>'Role Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLES_TBL'
,p_ref_column_name=>'ROLE_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15779899496465337)
,p_query_column_id=>4
,p_column_alias=>'ROLE_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Role Name'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13460169247212736)
,p_column_sequence=>1
,p_query_column_name=>'ROLE_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13460268363212736)
,p_column_sequence=>2
,p_query_column_name=>'ROLE_ID_DISPLAY'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13460388944212736)
,p_column_sequence=>3
,p_query_column_name=>'ROLE_NAME'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15781912759465344)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_item_display_point=>'BELOW'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15780523928465338)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(15779406451465333)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:addRow();'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15780291428465338)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(15779406451465333)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Submit'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15780209719465337)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(15779406451465333)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:&G_LASTPAGE.:&SESSION.::&DEBUG.::P2_APP_ID:&P30_APP_ID.'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15780401282465338)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(15779406451465333)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(15781708898465343)
,p_branch_action=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9073834618023276)
,p_name=>'P30_APP_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(15779406451465333)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P30_APP_ID'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15781216379465342)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(15779406451465333)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'xxwc_ms_roles_tbl'
,p_attribute_03=>'ROLE_ID'
,p_process_error_message=>'Unable to process update.'
,p_process_when_button_id=>wwv_flow_api.id(15780291428465338)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15781393382465343)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(15779406451465333)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_ROLES_TBL'
,p_attribute_03=>'ROLE_ID'
,p_process_error_message=>'Unable to process delete.'
,p_process_when_button_id=>wwv_flow_api.id(15780401282465338)
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
end;
/
prompt --application/pages/page_00040
begin
wwv_flow_api.create_page(
 p_id=>40
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'Update Role Responsibilities'
,p_page_mode=>'NORMAL'
,p_step_title=>'Update Role Responsibilities'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20150918155812'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(4623227949151115)
,p_name=>'Roles Responsibilites Journal'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select xxwc_wc_roles_RESP_JOURNAL.JOURNAL_ID as JOURNAL_ID,',
'    xxwc_wc_roles_RESP_JOURNAL.ROLES_RESP_ID as ROLES_RESP_ID,',
'    xxwc_wc_roles_RESP_JOURNAL.NOTES as NOTES,',
'    apps.xxwc_ms_master_admin_pkg.get_user_name(xxwc_wc_roles_RESP_JOURNAL.CREATED_BY) as CREATED_BY,',
'    xxwc_wc_roles_RESP_JOURNAL.CREATED_ON as CREATED_ON,',
'    xxwc_wc_roles_RESP_JOURNAL.UPDATED_BY as UPDATED_BY,',
'    xxwc_wc_roles_RESP_JOURNAL.UPDATED_ON as UPDATED_ON ',
' from XXWC.XXWC_MS_ROLES_RESP_JOURNAL_TBL xxwc_wc_roles_RESP_JOURNAL',
'where APP_ID = :P40_APP_ID',
'',
''))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4623526520151117)
,p_query_column_id=>1
,p_column_alias=>'JOURNAL_ID'
,p_column_display_sequence=>1
,p_column_heading=>'JOURNAL_ID'
,p_default_sort_column_sequence=>1
,p_default_sort_dir=>'desc'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4623612430151117)
,p_query_column_id=>2
,p_column_alias=>'ROLES_RESP_ID'
,p_column_display_sequence=>2
,p_column_heading=>'ROLES_RESP_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4623725192151117)
,p_query_column_id=>3
,p_column_alias=>'NOTES'
,p_column_display_sequence=>3
,p_column_heading=>'Notes'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4623825196151117)
,p_query_column_id=>4
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>4
,p_column_heading=>'Changed By'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4623918706151118)
,p_query_column_id=>5
,p_column_alias=>'CREATED_ON'
,p_column_display_sequence=>5
,p_column_heading=>'Date'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4624004654151118)
,p_query_column_id=>6
,p_column_alias=>'UPDATED_BY'
,p_column_display_sequence=>6
,p_column_heading=>'UPDATED_BY'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4624105024151118)
,p_query_column_id=>7
,p_column_alias=>'UPDATED_ON'
,p_column_display_sequence=>7
,p_column_heading=>'UPDATED_ON'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(15783095428496250)
,p_name=>'Role Responsibilities'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"ROLE_RESP_ID",',
'"ROLE_RESP_ID" ROLE_RESP_ID_DISPLAY,',
'"RESPONSIBILITY_ID",',
'"ROLE_ID",',
':P40_APP_ID "APP_ID"',
'from xxwc.xxwc_ms_role_resp_tbl',
'WHERE APP_ID =:P40_APP_ID'))
,p_source_type=>'NATIVE_TABFORM'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15785606528496264)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'&nbsp;'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15783416366496252)
,p_query_column_id=>2
,p_column_alias=>'ROLE_RESP_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Role Resp Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLE_RESP_TBL'
,p_ref_column_name=>'ROLE_RESP_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15783513134496252)
,p_query_column_id=>3
,p_column_alias=>'ROLE_RESP_ID_DISPLAY'
,p_column_display_sequence=>3
,p_column_heading=>'Role Resp Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLE_RESP_TBL'
,p_ref_column_name=>'ROLE_RESP_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15783601415496252)
,p_query_column_id=>4
,p_column_alias=>'RESPONSIBILITY_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Responsibility Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select RESPONSIBILITY_NAME display_value, RESPONSIBILITY_ID return_value ',
'from XXWC.XXWC_MS_RESPONSIBILITY_TBL',
'WHERE APP_ID =:P40_APP_ID',
'order by 1'))
,p_lov_show_nulls=>'NO'
,p_column_width=>40
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLE_RESP_TBL'
,p_ref_column_name=>'RESPONSIBILITY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3084109216363050)
,p_query_column_id=>5
,p_column_alias=>'ROLE_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Role Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ROLE_NAME display_value, ROLE_ID return_value ',
'from XXWC.XXWC_MS_ROLES_TBL',
'order by 1'))
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLE_RESP_TBL'
,p_ref_column_name=>'ROLE_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3365308982289254)
,p_query_column_id=>6
,p_column_alias=>'APP_ID'
,p_column_display_sequence=>6
,p_column_heading=>'App Id'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_column_default=>'P40_APP_ID'
,p_column_default_type=>'ITEM'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13460882389221793)
,p_column_sequence=>1
,p_query_column_name=>'ROLE_RESP_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13460972843221793)
,p_column_sequence=>2
,p_query_column_name=>'ROLE_RESP_ID_DISPLAY'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13461073530221793)
,p_column_sequence=>3
,p_query_column_name=>'RESPONSIBILITY_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(3083919703363049)
,p_column_sequence=>4
,p_query_column_name=>'ROLE_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(3365218565289253)
,p_column_sequence=>5
,p_query_column_name=>'APP_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15785902553496265)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_item_display_point=>'BELOW'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15784092039496253)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(15783095428496250)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:addRow();'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15783923271496253)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(15783095428496250)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Submit'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15783796647496253)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(15783095428496250)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:&G_LASTPAGE.:&SESSION.::&DEBUG.::P2_APP_ID:&P40_APP_ID.'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15784003338496253)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(15783095428496250)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(15785723613496264)
,p_branch_action=>'f?p=&APP_ID.:40:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9074064151024851)
,p_name=>'P40_APP_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(15783095428496250)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P40_APP_ID'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(15784909048496257)
,p_tabular_form_region_id=>wwv_flow_api.id(15783095428496250)
,p_validation_name=>'RESPONSIBILITY_ID must be numeric'
,p_validation_sequence=>30
,p_validation=>'RESPONSIBILITY_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(15783923271496253)
,p_associated_column=>'RESPONSIBILITY_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(15785118152496262)
,p_tabular_form_region_id=>wwv_flow_api.id(15783095428496250)
,p_validation_name=>'ROLE_ID must be numeric'
,p_validation_sequence=>40
,p_validation=>'ROLE_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(15783923271496253)
,p_associated_column=>'ROLE_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15785215202496262)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(15783095428496250)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'xxwc_ms_role_resp_tbl'
,p_attribute_03=>'ROLE_RESP_ID'
,p_process_error_message=>'Unable to process update.'
,p_process_when_button_id=>wwv_flow_api.id(15783923271496253)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15785391944496263)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(15783095428496250)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_ROLE_RESP_TBL'
,p_attribute_03=>'ROLE_RESP_ID'
,p_process_error_message=>'Unable to process delete.'
,p_process_when_button_id=>wwv_flow_api.id(15784003338496253)
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
end;
/
prompt --application/pages/page_00050
begin
wwv_flow_api.create_page(
 p_id=>50
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'Users'
,p_page_mode=>'NORMAL'
,p_step_title=>'Users'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Users'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20150918160856'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15787519806525154)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_item_display_point=>'BELOW'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15801908230550713)
,p_plug_name=>'Users'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15762995582318682)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY_3'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'   "HDS_USER_PULL_MVW"."EMPLOYEE_ID" "EMPLOYEE_ID", ',
'   "HDS_USER_PULL_MVW"."FIRST_NAME" "FIRST_NAME", ',
'   "HDS_USER_PULL_MVW"."LAST_NAME" "LAST_NAME", ',
'   "HDS_USER_PULL_MVW"."NAME" "NAME", ',
'   "HDS_USER_PULL_MVW"."EMAIL" "EMAIL",',
':P50_APP_ID',
'FROM ',
'   APPS.XXWC_HDS_USER_PULL_MV "HDS_USER_PULL_MVW"'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(15802000607550713)
,p_name=>'Users'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_show_display_row_count=>'Y'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'N'
,p_show_rows_per_page=>'N'
,p_show_pivot=>'N'
,p_show_notify=>'Y'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML:EMAIL'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MR024211'
,p_internal_uid=>1
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(15802209202550723)
,p_db_column_name=>'EMPLOYEE_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Employee Id'
,p_column_link=>'f?p=&APP_ID.:60:&SESSION.::&DEBUG.::P60_EMPLOYEE_ID,P60_APP_ID:#EMPLOYEE_ID#,#:P50_APP_ID#'
,p_column_linktext=>'#EMPLOYEE_ID#'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_tz_dependent=>'N'
,p_static_id=>'EMPLOYEE_ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(15802293764550730)
,p_db_column_name=>'FIRST_NAME'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'First Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FIRST_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(15802422541550730)
,p_db_column_name=>'LAST_NAME'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Last Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'LAST_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(15802493006550731)
,p_db_column_name=>'NAME'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(15802609451550731)
,p_db_column_name=>'EMAIL'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Email'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'EMAIL'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(9116860721260324)
,p_db_column_name=>':P50_APP_ID'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'App Id'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>':P50_APP_ID'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(15802724008554723)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'23464'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'EMPLOYEE_ID:FIRST_NAME:LAST_NAME:NAME:EMAIL'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15790701885525361)
,p_button_sequence=>10
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_redirect_url=>'f?p=&APP_ID.:60:&SESSION.::&DEBUG.:60'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9112749211098509)
,p_name=>'P50_APP_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(15801908230550713)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P50_APP_ID'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_computation(
 p_id=>wwv_flow_api.id(15790914582525362)
,p_computation_sequence=>1
,p_computation_item=>'P60_EMPLOYEE_ID'
,p_computation_type=>'STATIC_ASSIGNMENT'
,p_compute_when=>'CREATE'
,p_compute_when_type=>'REQUEST_EQUALS_CONDITION'
);
end;
/
prompt --application/pages/page_00060
begin
wwv_flow_api.create_page(
 p_id=>60
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_tab_set=>'TS1'
,p_name=>'User Roles'
,p_page_mode=>'NORMAL'
,p_step_title=>'User Roles'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';',
'var htmldb_ch_message=''"OK_TO_GET_NEXT_PREV_PK_VALUE"'';'))
,p_step_template=>wwv_flow_api.id(15759906506318668)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20150918160946'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3317001308385676)
,p_plug_name=>'Verbaige'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_source=>'<font color = "red"><b>Please do not delete any role from user unless you are sure about it. As it can impact other applications.</b></font>'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(13775522058556303)
,p_name=>'User Roles Journal'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select xxwc_wc_user_roles_JOURNAL.JOURNAL_ID as JOURNAL_ID,',
'    xxwc_wc_user_roles_JOURNAL.USER_ROLES_ID as USER_ROLES_ID,',
'    xxwc_wc_user_roles_JOURNAL.NOTES as NOTES,',
'apps.xxwc_ms_master_admin_pkg.get_user_name(xxwc_wc_user_roles_JOURNAL.CREATED_BY) as CREATED_BY,',
'      xxwc_wc_user_roles_JOURNAL.CREATED_ON as CREATED_ON,',
'    xxwc_wc_user_roles_JOURNAL.UPDATED_BY as UPDATED_BY,',
'    xxwc_wc_user_roles_JOURNAL.UPDATED_ON as UPDATED_ON ',
' from XXWC.XXWC_MS_USER_ROLES_JOURNAL_TBL xxwc_wc_user_roles_JOURNAL',
'where user_id = :P60_EMPLOYEE_ID'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(13775825796556392)
,p_query_column_id=>1
,p_column_alias=>'JOURNAL_ID'
,p_column_display_sequence=>1
,p_column_heading=>'JOURNAL_ID'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_default_sort_dir=>'desc'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(13775927615556402)
,p_query_column_id=>2
,p_column_alias=>'USER_ROLES_ID'
,p_column_display_sequence=>2
,p_column_heading=>'USER_ROLES_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(13776017363556402)
,p_query_column_id=>3
,p_column_alias=>'NOTES'
,p_column_display_sequence=>3
,p_column_heading=>'Notes'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(13776130600556402)
,p_query_column_id=>4
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>4
,p_column_heading=>'Changed By'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(13776220035556402)
,p_query_column_id=>5
,p_column_alias=>'CREATED_ON'
,p_column_display_sequence=>5
,p_column_heading=>'Date'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(13776320810556402)
,p_query_column_id=>6
,p_column_alias=>'UPDATED_BY'
,p_column_display_sequence=>6
,p_column_heading=>'UPDATED_BY'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(13776417644556402)
,p_query_column_id=>7
,p_column_alias=>'UPDATED_ON'
,p_column_display_sequence=>7
,p_column_heading=>'UPDATED_ON'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15787597687525154)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761622021318681)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_item_display_point=>'BELOW'
,p_menu_id=>wwv_flow_api.id(15769218604318760)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(15766804687318693)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15791510502525366)
,p_plug_name=>'Employees'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15762904053318681)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(15797596559525396)
,p_name=>'Employee Roles'
,p_template=>wwv_flow_api.id(15762904053318681)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'u.USER_ROLE_ID,',
'u.USER_ID,',
'u.ROLE_ID,',
'u.OWNER_FLAG',
'from xxwc.xxwc_ms_user_roles_tbl u',
'where u.USER_ID = :P60_EMPLOYEE_ID'))
,p_source_type=>'NATIVE_TABFORM'
,p_display_when_condition=>'P60_EMPLOYEE_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(15765798019318690)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15799696920525409)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'&nbsp;'
,p_use_as_row_header=>'N'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15797919124525399)
,p_query_column_id=>2
,p_column_alias=>'USER_ROLE_ID'
,p_column_display_sequence=>3
,p_column_heading=>'User Role Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_pk_col_source=>'xxwc.xxwc_ms_user_roles_trg'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'xxwc_ms_user_roles_tbl'
,p_ref_column_name=>'USER_ROLE_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(15797994200525399)
,p_query_column_id=>3
,p_column_alias=>'USER_ID'
,p_column_display_sequence=>2
,p_column_heading=>'User Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_column_default=>'P60_EMPLOYEE_ID'
,p_column_default_type=>'ITEM'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_USER_ROLES_TBL'
,p_ref_column_name=>'USER_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4606227941820899)
,p_query_column_id=>4
,p_column_alias=>'ROLE_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Role Id'
,p_use_as_row_header=>'N'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ROLE_NAME display_value, ROLE_ID return_value ',
'from xxwc.xxwc_ms_roles_tbl',
'order by 1'))
,p_lov_show_nulls=>'YES'
,p_lov_null_text=>'-Select Role-'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4599131600707136)
,p_query_column_id=>5
,p_column_alias=>'OWNER_FLAG'
,p_column_display_sequence=>5
,p_column_heading=>'Owner Flag'
,p_use_as_row_header=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(4599703254718558)
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13464965543259988)
,p_column_sequence=>1
,p_query_column_name=>'USER_ROLE_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(13465089491259988)
,p_column_sequence=>2
,p_query_column_name=>'USER_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(4605903543820897)
,p_column_sequence=>3
,p_query_column_name=>'ROLE_ID'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_region_rpt_cols(
 p_id=>wwv_flow_api.id(4599020622707135)
,p_column_sequence=>4
,p_query_column_name=>'OWNER_FLAG'
,p_display_as=>'TEXT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15791813948525367)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(15797596559525396)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Apply Changes'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P60_EMPLOYEE_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15798516409525400)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(15797596559525396)
,p_button_name=>'APPLY_CHANGES_ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Add Row'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_redirect_url=>'javascript:addRow();'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15794596225525385)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(15791510502525366)
,p_button_name=>'GET_PREVIOUS_EMPLOYEE_ID'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'&lt;'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_redirect_url=>'javascript:htmldb_goSubmit(''GET_PREVIOUS_EMPLOYEE_ID'')'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
,p_button_comment=>'This button is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15794517597525385)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_api.id(15791510502525366)
,p_button_name=>'GET_NEXT_EMPLOYEE_ID'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'&gt;'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_redirect_url=>'javascript:htmldb_goSubmit(''GET_NEXT_EMPLOYEE_ID'')'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
,p_button_comment=>'This button is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15792100982525367)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(15791510502525366)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:50:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15791710421525367)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(15791510502525366)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P60_EMPLOYEE_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_grid_new_grid=>false
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15791909432525367)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(15791510502525366)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15798412632525400)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(15797596559525396)
,p_button_name=>'APPLY_CHANGES_MRD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Delete Checked'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''APPLY_CHANGES_MRD'');'
,p_button_execute_validations=>'N'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select 1 ',
'from xxwc.xxwc_ms_user_roles_tbl',
'where "USER_ID" = :P60_EMPLOYEE_ID'))
,p_button_condition_type=>'EXISTS'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(15796600745525392)
,p_branch_action=>'f?p=&APP_ID.:60:&SESSION.::&DEBUG.::P60_EMPLOYEE_ID:&P60_EMPLOYEE_ID_NEXT.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(15794517597525385)
,p_branch_sequence=>10
,p_branch_condition_type=>'NEVER'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'This button is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(15796814751525392)
,p_branch_action=>'f?p=&APP_ID.:60:&SESSION.::&DEBUG.::P60_EMPLOYEE_ID:&P60_EMPLOYEE_ID_PREV.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(15794596225525385)
,p_branch_sequence=>20
,p_branch_condition_type=>'NEVER'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'This button is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(15794320078525385)
,p_branch_action=>'f?p=&APP_ID.:60:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(15791813948525367)
,p_branch_sequence=>30
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(15799819710525409)
,p_branch_action=>'f?p=&APP_ID.:60:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>40
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9113433012136476)
,p_name=>'P60_APP_ID'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(15797596559525396)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P60_APP_ID'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15792694068525369)
,p_name=>'P60_EMPLOYEE_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_api.id(15791510502525366)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Employee Id'
,p_pre_element_text=>'<STRONG>'
,p_post_element_text=>'</STRONG>'
,p_source=>'EMPLOYEE_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(15766307244318691)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15792900121525379)
,p_name=>'P60_FIRST_NAME'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(15791510502525366)
,p_use_cache_before_default=>'NO'
,p_prompt=>'First Name'
,p_source=>'FIRST_NAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>32
,p_cMaxlength=>50
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(15766501526318693)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15793108097525380)
,p_name=>'P60_LAST_NAME'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(15791510502525366)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Last Name'
,p_source=>'LAST_NAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>32
,p_cMaxlength=>50
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(15766501526318693)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15793301412525382)
,p_name=>'P60_NAME'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(15791510502525366)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Employee Name'
,p_pre_element_text=>'<strong>'
,p_post_element_text=>'</strong>'
,p_source=>'NAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>32
,p_cMaxlength=>150
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(15766307244318691)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15793507872525382)
,p_name=>'P60_EMAIL'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(15791510502525366)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Email'
,p_source=>'EMAIL'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>32
,p_cMaxlength=>250
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(15766307244318691)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15795822738525388)
,p_name=>'P60_EMPLOYEE_ID_NEXT'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(15791510502525366)
,p_prompt=>'P60_EMPLOYEE_ID_NEXT'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_item_comment=>'This item is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15796020176525388)
,p_name=>'P60_EMPLOYEE_ID_PREV'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(15791510502525366)
,p_prompt=>'P60_EMPLOYEE_ID_PREV'
,p_display_as=>'NATIVE_HIDDEN'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_item_comment=>'This item is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(15799223959525404)
,p_tabular_form_region_id=>wwv_flow_api.id(15797596559525396)
,p_validation_name=>'ROLE_ID must be numeric'
,p_validation_sequence=>30
,p_validation=>'ROLE_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_always_execute=>'N'
,p_validation_condition=>':request like (''SAVE'') or :request like ''GET_NEXT%'' or :request like ''GET_PREV%'''
,p_validation_condition_type=>'PLSQL_EXPRESSION'
,p_associated_column=>'ROLE_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(3269309510267248)
,p_tabular_form_region_id=>wwv_flow_api.id(15797596559525396)
,p_validation_name=>'ROLE_ID'
,p_validation_sequence=>40
,p_validation=>'ROLE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Role Name cannot be blank'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(15791813948525367)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'ROLE_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15793802147525383)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from HDS_USER_PULL_MVW'
,p_attribute_01=>'APPS'
,p_attribute_02=>'XXWC_HDS_USER_PULL_MV'
,p_attribute_03=>'P60_EMPLOYEE_ID'
,p_attribute_04=>'EMPLOYEE_ID'
,p_process_error_message=>'Unable to fetch row.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15796396417525391)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_PAGINATION'
,p_process_name=>'Get Next or Previous Primary Key Value'
,p_attribute_01=>'APPS'
,p_attribute_02=>'XXWC_HDS_USER_PULL_MV'
,p_attribute_03=>'P60_EMPLOYEE_ID'
,p_attribute_04=>'EMPLOYEE_ID'
,p_attribute_09=>'P60_EMPLOYEE_ID_NEXT'
,p_attribute_10=>'P60_EMPLOYEE_ID_PREV'
,p_process_error_message=>'Unable to run Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15794011537525384)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of HDS_USER_PULL_MVW'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_USER_ROLES_TBL'
,p_attribute_03=>'P60_EMPLOYEE_ID'
,p_attribute_04=>'USER_ID'
,p_attribute_11=>'I:U:D'
,p_process_error_message=>'Unable to process row of table HDS_USER_PULL_MVW.'
,p_process_when_type=>'NEVER'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15799322858525408)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(15797596559525396)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_USER_ROLES_TBL'
,p_attribute_03=>'USER_ROLE_ID'
,p_process_error_message=>'Unable to process update.'
,p_process_when=>':request like (''SAVE'') or :request like ''GET_NEXT%'' or :request like ''GET_PREV%'''
,p_process_when_type=>'PLSQL_EXPRESSION'
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15799523271525408)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(15797596559525396)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_USER_ROLES_TBL'
,p_attribute_03=>'USER_ROLE_ID'
,p_process_error_message=>'Unable to process delete.'
,p_process_when_button_id=>wwv_flow_api.id(15798412632525400)
,p_process_when=>'APPLY_CHANGES_MRD'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15794198434525385)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_process_when_button_id=>wwv_flow_api.id(15791909432525367)
,p_process_when_type=>'NEVER'
);
end;
/
prompt --application/pages/page_00101
begin
wwv_flow_api.create_page(
 p_id=>101
,p_user_interface_id=>wwv_flow_api.id(13520799422501392)
,p_name=>'Login'
,p_alias=>'LOGIN'
,p_page_mode=>'NORMAL'
,p_step_title=>'Login'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_api.id(15759497602318660)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20150908094840'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15768397448318743)
,p_plug_name=>'Login'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(15761996439318681)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15768714752318755)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(15768397448318743)
,p_button_name=>'P101_LOGIN'
,p_button_static_id=>'P101_LOGIN'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(15760994264318674)
,p_button_image_alt=>'Login'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_request_source=>'LOGIN'
,p_request_source_type=>'STATIC'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column_span=>1
,p_grid_row_span=>1
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15768508829318751)
,p_name=>'P101_USERNAME'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(15768397448318743)
,p_prompt=>'Username'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>40
,p_cMaxlength=>100
,p_cHeight=>1
,p_colspan=>2
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(15766501526318693)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15768605248318755)
,p_name=>'P101_PASSWORD'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(15768397448318743)
,p_prompt=>'Password'
,p_display_as=>'NATIVE_PASSWORD'
,p_cSize=>40
,p_cMaxlength=>100
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(15766501526318693)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15768918932318759)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Set Username Cookie'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'owa_util.mime_header(''text/html'', FALSE);',
'owa_cookie.send(',
'    name=>''LOGIN_USERNAME_COOKIE'',',
'    value=>lower(:P101_USERNAME));',
'exception when others then null;',
'end;'))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15768803740318755)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Login'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'wwv_flow_custom_auth_std.login(',
'    P_UNAME       => :P101_USERNAME,',
'    P_PASSWORD    => :P101_PASSWORD,',
'    P_SESSION_ID  => v(''APP_SESSION''),',
'    P_FLOW_PAGE   => :APP_ID||'':1''',
'    );'))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15769117238318759)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'Clear Page(s) Cache'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15768997169318759)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get Username Cookie'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    v varchar2(255) := null;',
'    c owa_cookie.cookie;',
'begin',
'   c := owa_cookie.get(''LOGIN_USERNAME_COOKIE'');',
'   :P101_USERNAME := c.vals(1);',
'exception when others then null;',
'end;'))
);
end;
/
prompt --application/deployment/definition
begin
null;
end;
/
prompt --application/deployment/install
begin
null;
end;
/
prompt --application/deployment/checks
begin
null;
end;
/
prompt --application/deployment/buildoptions
begin
null;
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
