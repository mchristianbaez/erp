set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
--------------------------------------------------------------------------------
--
-- ORACLE Application Express (APEX) export file
--
-- You should run the script connected to SQL*Plus as the Oracle user
-- APEX_050100 or as the owner (parsing schema) of the application.
--
-- NOTE: Calls to apex_application_install override the defaults below.
--
--------------------------------------------------------------------------------
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2016.08.24'
,p_release=>'5.1.0.00.45'
,p_default_workspace_id=>2222210991411699
,p_default_application_id=>119
,p_default_owner=>'EA_APEX'
);
end;
/
prompt --application/set_environment
 
prompt APPLICATION 119 - WC B2B Trading Partner Maintenance
--
-- Application Export:
--   Application:     119
--   Name:            WC B2B Trading Partner Maintenance
--   Date and Time:   13:12 Thursday November 16, 2017
--   Exported By:     GP050872
--   Flashback:       0
--   Export Type:     Application Export
--   Version:         5.1.0.00.45
--   Instance ID:     69420199397069
--

-- Application Statistics:
--   Pages:                     22
--     Items:                  113
--     Computations:             1
--     Validations:             18
--     Processes:               46
--     Regions:                 65
--     Buttons:                 73
--     Dynamic Actions:         15
--   Shared Components:
--     Logic:
--       Items:                  2
--       Processes:              1
--     Navigation:
--       Parent Tabs:            1
--       Tab Sets:               1
--         Tabs:                 3
--       Lists:                  6
--       Breadcrumbs:            1
--         Entries:              5
--       NavBar Entries:         1
--     Security:
--       Authentication:         6
--     User Interface:
--       Themes:                 2
--       Templates:
--         Page:                32
--         Region:              41
--         Label:               12
--         List:                31
--         Popup LOV:            2
--         Calendar:             4
--         Breadcrumb:           3
--         Button:              11
--         Report:              20
--       LOVs:                  26
--       Shortcuts:              2
--       Plug-ins:               2
--     Globalization:
--     Reports:
--   Supporting Objects:  Included
--     Install scripts:          1

prompt --application/delete_application
begin
wwv_flow_api.remove_flow(wwv_flow.g_flow_id);
end;
/
prompt --application/ui_types
begin
null;
end;
/
prompt --application/create_application
begin
wwv_flow_api.create_flow(
 p_id=>wwv_flow.g_flow_id
,p_display_id=>nvl(wwv_flow_application_install.get_application_id,119)
,p_owner=>nvl(wwv_flow_application_install.get_schema,'EA_APEX')
,p_name=>nvl(wwv_flow_application_install.get_application_name,'WC B2B Trading Partner Maintenance')
,p_alias=>nvl(wwv_flow_application_install.get_application_alias,'TAXUPLOAD102199125116119')
,p_page_view_logging=>'YES'
,p_page_protection_enabled_y_n=>'Y'
,p_checksum_salt_last_reset=>'20171116130940'
,p_bookmark_checksum_function=>'MD5'
,p_max_session_length_sec=>28800
,p_flow_language=>'en'
,p_flow_language_derived_from=>'0'
,p_direction_right_to_left=>'N'
,p_flow_image_prefix => nvl(wwv_flow_application_install.get_image_prefix,'')
,p_authentication=>'PLUGIN'
,p_authentication_id=>wwv_flow_api.id(24622897202658595)
,p_logout_url=>'http://ofid.hsi.hughessupply.com:8000/pls/orasso/orasso.wwsso_app_admin.ls_logout?p_done_url=http://apps.hdsupply.net/pls/apex/f?p=&APP_ID.'
,p_application_tab_set=>0
,p_public_user=>'APEX_PUBLIC_USER'
,p_proxy_server=> nvl(wwv_flow_application_install.get_proxy,'')
,p_flow_version=>'release 1.0'
,p_flow_status=>'AVAILABLE_W_EDIT_LINK'
,p_flow_unavailable_text=>'This application is currently unavailable at this time.'
,p_exact_substitutions_only=>'Y'
,p_deep_linking=>'Y'
,p_runtime_api_usage=>'T:O:W'
,p_authorize_public_pages_yn=>'Y'
,p_rejoin_existing_sessions=>'P'
,p_csv_encoding=>'Y'
,p_auto_time_zone=>'N'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20171116130940'
,p_file_prefix => nvl(wwv_flow_application_install.get_static_app_file_prefix,'')
,p_ui_type_name => null
);
end;
/
prompt --application/shared_components/navigation/lists
begin
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(3248717209268655)
,p_name=>'Security Main1'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(3336530786730068)
,p_name=>'Security Main2'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(3338124607730074)
,p_name=>'Link'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(3338426313730074)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'Link to Application'
,p_list_item_link_target=>'http://devebiz.hdsupply.net:7777/pls/ebizdev/f?p=&P2_APP_ID.'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(3377400104919026)
,p_name=>'Security Main3'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(3377724132919027)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'View Responsibilities'
,p_list_item_link_target=>'f?p=&APP_ID.:92:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'menu/users_business_64.gif'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(3378013229919027)
,p_list_item_display_sequence=>20
,p_list_item_link_text=>'View Roles'
,p_list_item_link_target=>'f?p=&APP_ID.:95:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'menu/addresses_wbg_64x64.png'
,p_list_item_disp_cond_type=>'NEVER'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(3378311557919028)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'Maintain Role Responsibilities'
,p_list_item_link_target=>'f?p=&APP_ID.:93:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'menu/discussion_wbg_64x64.png'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(3378609216919028)
,p_list_item_display_sequence=>40
,p_list_item_link_text=>'Maintain User Roles'
,p_list_item_link_target=>'f?p=&APP_ID.:94:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'menu/instructor_wbg_64x64.png'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(13214764625505609)
,p_name=>'Data Load Process Train - Tax Upload'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(13215153370505612)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'Data Load Source'
,p_list_item_link_target=>'f?p=&APP_ID.:105:&SESSION.::&DEBUG.'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(13215477469505614)
,p_list_item_display_sequence=>20
,p_list_item_link_text=>'Data / Table Mapping'
,p_list_item_link_target=>'f?p=&APP_ID.:110:&SESSION.::&DEBUG.'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(13215750184505615)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'Data Validation'
,p_list_item_link_target=>'f?p=&APP_ID.:120:&SESSION.::&DEBUG.'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(13216079433505615)
,p_list_item_display_sequence=>40
,p_list_item_link_text=>'Data Load Results'
,p_list_item_link_target=>'f?p=&APP_ID.:130:&SESSION.::&DEBUG.'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(24628411718731906)
,p_name=>'Security Main'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(24661916285912949)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'View Responsibilities'
,p_list_item_link_target=>'f?p=&APP_ID.:20:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'menu/users_business_64.gif'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(24662088019914288)
,p_list_item_display_sequence=>20
,p_list_item_link_text=>'View Roles'
,p_list_item_link_target=>'f?p=&APP_ID.:30:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'menu/addresses_wbg_64x64.png'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(24662304642919005)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'Maintain Role Responsibilities'
,p_list_item_link_target=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'menu/discussion_wbg_64x64.png'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(24662510529920770)
,p_list_item_display_sequence=>40
,p_list_item_link_text=>'Maintain User Roles'
,p_list_item_link_target=>'f?p=&APP_ID.:50:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'menu/instructor_wbg_64x64.png'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
);
end;
/
prompt --application/shared_components/files
begin
null;
end;
/
prompt --application/plugin_settings
begin
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(225123977277980)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_DISPLAY_SELECTOR'
,p_attribute_01=>'N'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(240142094278185)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_IR'
,p_attribute_01=>'LEGACY'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(245352769278186)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_COLOR_PICKER'
,p_attribute_01=>'classic'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(1849938658278261)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_CSS_CALENDAR'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(1850121278278261)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_IG'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(9521552935878336)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_YES_NO'
,p_attribute_01=>'Y'
,p_attribute_03=>'N'
,p_attribute_05=>'SELECT_LIST'
);
end;
/
prompt --application/shared_components/security/authorizations
begin
null;
end;
/
prompt --application/shared_components/navigation/navigation_bar
begin
wwv_flow_api.create_icon_bar_item(
 p_id=>wwv_flow_api.id(24623203278658597)
,p_icon_sequence=>200
,p_icon_subtext=>'Logout'
,p_icon_target=>'&LOGOUT_URL.'
,p_icon_image_alt=>'Logout'
,p_icon_height=>32
,p_icon_width=>32
,p_icon_height2=>24
,p_icon_width2=>24
,p_nav_entry_is_feedback_yn=>'N'
,p_cell_colspan=>1
);
end;
/
prompt --application/shared_components/logic/application_processes
begin
wwv_flow_api.create_flow_process(
 p_id=>wwv_flow_api.id(3329111617952003)
,p_process_sequence=>1
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'JOB_LOV'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  l_sql VARCHAR2(32700);',
'BEGIN',
'',
'IF apex_application.g_x01 IS NOT NULL THEN',
'',
'   l_sql := ''SELECT DISTINCT hcsu.SITE_USE_ID RET, hcsu.LOCATION DIS',
'               FROM apps.hz_cust_site_uses_all  hcsu',
'                  , apps.hz_cust_acct_sites_all hcas',
'                  , apps.hz_party_sites         hps',
'              WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'                AND hps.party_site_id = hcas.party_site_id',
'                AND hcas.cust_account_id = ''||apex_application.g_x01 ||'' ',
'                AND hcas.STATUS = ''''A''''',
'                AND hcsu.site_use_code = ''''SHIP_TO'''''';',
'',
'    APEX_UTIL.JSON_FROM_SQL(l_sql);',
'ELSE',
'    HTP.prn(''{"row":[]}'');',
'END IF;',
'END;'))
,p_security_scheme=>'MUST_NOT_BE_PUBLIC_USER'
);
end;
/
prompt --application/shared_components/logic/application_items
begin
wwv_flow_api.create_flow_item(
 p_id=>wwv_flow_api.id(24626315780676253)
,p_name=>'FSP_AFTER_LOGIN_URL'
);
wwv_flow_api.create_flow_item(
 p_id=>wwv_flow_api.id(2743512489746040)
,p_name=>'FSP_PROCESS_STATE_2742327156684100'
);
end;
/
prompt --application/shared_components/logic/application_computations
begin
null;
end;
/
prompt --application/shared_components/navigation/tabs/standard
begin
wwv_flow_api.create_tab(
 p_id=>wwv_flow_api.id(3390098615998419)
,p_tab_set=>'TS1'
,p_tab_sequence=>5
,p_tab_name=>'T_MAIN'
,p_tab_text=>'Main'
,p_tab_step=>5
,p_tab_parent_tabset=>'TS1'
);
wwv_flow_api.create_tab(
 p_id=>wwv_flow_api.id(1892313296035688)
,p_tab_set=>'TS1'
,p_tab_sequence=>10
,p_tab_name=>'T_PODUPLOAD'
,p_tab_text=>'POD Upload'
,p_tab_step=>8
,p_tab_parent_tabset=>'TS1'
,p_tab_plsql_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.user_id',
'FROM  xxwc.xxwc_ms_user_roles_tbl a',
',     xxwc.xxwc_ms_roles_tbl b',
',     xxwc.xxwc_ms_role_resp_tbl c',
',     xxwc.xxwc_ms_responsibility_tbl d',
'WHERE a.role_id = b.role_id(+)',
'AND   b.role_id = c.role_id(+)',
'AND   c.responsibility_id = d.responsibility_id(+)',
'AND   a.user_id= :APP_USER',
'and d.app_id = :APP_ID',
'and c.app_id = :APP_ID',
'and b.role_name = ''B2B BSA'''))
,p_display_condition_type=>'EXISTS'
);
wwv_flow_api.create_tab(
 p_id=>wwv_flow_api.id(3384281948758544)
,p_tab_set=>'TS1'
,p_tab_sequence=>20
,p_tab_name=>'T_CUSTOMERREPORT'
,p_tab_text=>'Customer Report'
,p_tab_step=>7
,p_tab_parent_tabset=>'TS1'
);
end;
/
prompt --application/shared_components/navigation/tabs/parent
begin
wwv_flow_api.create_toplevel_tab(
 p_id=>wwv_flow_api.id(24626786307715023)
,p_tab_set=>'TS1'
,p_tab_sequence=>10
,p_tab_name=>'T_MAINPAGE'
,p_tab_text=>'Mainpage'
,p_tab_target=>'f?p=&APP_ID.:0:&SESSION.::&DEBUG.:::'
,p_current_on_tabset=>'TS1'
);
end;
/
prompt --application/shared_components/user_interface/lovs
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2413116515352887)
,p_lov_name=>'ACCOUNT_NAME'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ACCOUNT_NAME d, CUST_ACCOUNT_ID r',
'  FROM apps.hz_cust_accounts_all',
' WHERE status = ''A'' AND ACCOUNT_NAME IS NOT NULL',
'ORDER BY CUST_ACCOUNT_ID'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2412924279349217)
,p_lov_name=>'ACCOUNT_NUMBER'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ACCOUNT_NUMBER d, CUST_ACCOUNT_ID r',
'  FROM apps.hz_cust_accounts_all',
' WHERE status = ''A''',
'ORDER BY CUST_ACCOUNT_ID'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(3313406454865098)
,p_lov_name=>'ACCOUNT_NUMBER1'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ACCOUNT_NUMBER d, CUST_ACCOUNT_ID r',
'  FROM apps.hz_cust_accounts_all',
' WHERE status = ''A''',
'ORDER BY CUST_ACCOUNT_ID'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(13217874644505657)
,p_lov_name=>'DATA_LOAD_OPTION'
,p_lov_query=>'.'||wwv_flow_api.id(13217874644505657)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(13218177882505661)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Upload file, comma separated (*.csv) or tab delimited'
,p_lov_return_value=>'UPLOAD'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(13218378207505665)
,p_lov_disp_sequence=>20
,p_lov_disp_value=>'Copy and Paste'
,p_lov_return_value=>'PASTE'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(24629804839758293)
,p_lov_name=>'EMAILS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select NAME display_value, EMAIL return_value ',
'from HDS_USER_PULL_MVW',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(13220354519505686)
,p_lov_name=>'FIRST_ROW_OPTION'
,p_lov_query=>'.'||wwv_flow_api.id(13220354519505686)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(13220667489505687)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2521929649339500)
,p_lov_name=>'P1_Report Row Per Page'
,p_lov_query=>'.'||wwv_flow_api.id(2521929649339500)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2530203962339530)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'10'
,p_lov_return_value=>'10'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2530501137339539)
,p_lov_disp_sequence=>20
,p_lov_disp_value=>'15'
,p_lov_return_value=>'15'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2530829504339539)
,p_lov_disp_sequence=>30
,p_lov_disp_value=>'20'
,p_lov_return_value=>'20'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2531129574339540)
,p_lov_disp_sequence=>40
,p_lov_disp_value=>'30'
,p_lov_return_value=>'30'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2531425948339540)
,p_lov_disp_sequence=>50
,p_lov_disp_value=>'50'
,p_lov_return_value=>'50'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2531711161339541)
,p_lov_disp_sequence=>60
,p_lov_disp_value=>'100'
,p_lov_return_value=>'100'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2532009111339541)
,p_lov_disp_sequence=>70
,p_lov_disp_value=>'200'
,p_lov_return_value=>'200'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2532332590339542)
,p_lov_disp_sequence=>80
,p_lov_disp_value=>'500'
,p_lov_return_value=>'500'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2532610578339542)
,p_lov_disp_sequence=>90
,p_lov_disp_value=>'1000'
,p_lov_return_value=>'1000'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2532926330339542)
,p_lov_disp_sequence=>100
,p_lov_disp_value=>'5000'
,p_lov_return_value=>'5000'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(4901524425545800)
,p_lov_name=>'PARTY_NAME'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_name d, party_name r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION''',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2532531804639613)
,p_lov_name=>'PARTY_NAME1'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_name d, party_name r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION''',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(5796908191268319)
,p_lov_name=>'PARTY_NUMBER'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_number d, party_number r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION''',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2532704364639613)
,p_lov_name=>'PARTY_NUMBER1'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_number d, party_number r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION''',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(3314431500865103)
,p_lov_name=>'POD_FREQUENCY'
,p_lov_query=>'.'||wwv_flow_api.id(3314431500865103)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3314732352865103)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'DAILY'
,p_lov_return_value=>'DAILY'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3315026004865103)
,p_lov_disp_sequence=>3
,p_lov_disp_value=>'WEEKLY'
,p_lov_return_value=>'WEEKLY'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(3852302207961731)
,p_lov_name=>'PRINT_PRICE'
,p_lov_query=>'.'||wwv_flow_api.id(3852302207961731)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3852611755961737)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'No'
,p_lov_return_value=>'N'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3852900840961738)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2471412814557028)
,p_lov_name=>'REPORT NAME'
,p_lov_query=>'.'||wwv_flow_api.id(2471412814557028)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2471726662557029)
,p_lov_disp_sequence=>41
,p_lov_disp_value=>'Order Status Update - Summary Metrics'
,p_lov_return_value=>'41'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2472021148557031)
,p_lov_disp_sequence=>44
,p_lov_disp_value=>'Order Status Update - SetUp Metrics'
,p_lov_return_value=>'44'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2472319899557032)
,p_lov_disp_sequence=>45
,p_lov_disp_value=>'Order Status Update - POD Setup Metrics'
,p_lov_return_value=>'45'
,p_lov_disp_cond_type=>'NEVER'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(24630010033759748)
,p_lov_name=>'RESPONSIBILITIES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select RESPONSIBILITY_NAME display_value, RESPONSIBILITY_ID return_value ',
'from WC_RESPONSIBILITY',
'WHERE APP_ID =:APP_ID',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(24630214535761062)
,p_lov_name=>'ROLES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ROLE_NAME display_value, ROLE_ID return_value ',
'from WC_ROLES',
'WHERE APP_ID =:APP_ID',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2531224846639610)
,p_lov_name=>'SALESREP'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT REPLACE(jrre.source_name, '','', '''') d, jrre.user_name r',
'  FROM apps.hz_cust_acct_sites_all     hcas',
'     , apps.hz_cust_accounts_all       hca',
'     , apps.hz_cust_site_uses_all      hcsu',
'     , apps.jtf_rs_salesreps           jrs',
'     , apps.jtf_rs_resource_extns      jrre',
' WHERE 1 = 1',
'   AND hcas.cust_account_id     = hca.cust_account_id',
'   AND hca.party_id             = :P3_PARTY_ID',
'   AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id',
'   AND hcsu.primary_salesrep_id = jrs.salesrep_id',
'   AND jrs.resource_id          = jrre.resource_id',
'   AND jrs.org_id               = hcas.org_id',
'   AND jrre.user_name           IS NOT NULL',
'   AND jrre.source_name         IS NOT NULL',
'   AND hcas.status              = ''A''',
'   AND hcsu.status              = ''A''',
'   AND hcas.org_id              = 162',
'   AND hcsu.org_id              = 162',
'   AND jrs.org_id              = 162',
'   AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2570215092394475)
,p_lov_name=>'TP_REQUEST_STATUS'
,p_lov_query=>'.'||wwv_flow_api.id(2570215092394475)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2570513775394478)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'DRAFT'
,p_lov_return_value=>'DRAFT'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2570813487394478)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'SUBMITTED'
,p_lov_return_value=>'SUBMITTED'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2571128403394478)
,p_lov_disp_sequence=>3
,p_lov_disp_value=>'APPROVED'
,p_lov_return_value=>'APPROVED'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2571418286394479)
,p_lov_disp_sequence=>4
,p_lov_disp_value=>'REJECTED'
,p_lov_return_value=>'REJECTED'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2783616224955846)
,p_lov_name=>'USER ROLE'
,p_lov_query=>'.'||wwv_flow_api.id(2783616224955846)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2814503506955855)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'READ ONLY'
,p_lov_return_value=>'1'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2814213040955854)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'SALES ACCOUNT MANAGER'
,p_lov_return_value=>'2'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2783915163955850)
,p_lov_disp_sequence=>3
,p_lov_disp_value=>'ADMIN'
,p_lov_return_value=>'3'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2544128722879480)
,p_lov_disp_sequence=>4
,p_lov_disp_value=>'APPROVER'
,p_lov_return_value=>'4'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(24630386962762534)
,p_lov_name=>'USERS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select NAME display_value, EMPLOYEE_ID return_value ',
'from HDS_USER_PULL_MVW',
'order by 1'))
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(3368729304748334)
,p_lov_name=>'YES/NO'
,p_lov_query=>'.'||wwv_flow_api.id(3368729304748334)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3369019986748337)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'No'
,p_lov_return_value=>'N'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3369332556748339)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(4755526183037706)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Deactivate'
,p_lov_return_value=>'D'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(3313616255865101)
,p_lov_name=>'YES/NO1'
,p_lov_query=>'.'||wwv_flow_api.id(3313616255865101)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3313929201865101)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'No'
,p_lov_return_value=>'N'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3314229173865102)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(4897937324296850)
,p_lov_name=>'YN'
,p_lov_query=>'.'||wwv_flow_api.id(4897937324296850)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(4898213824296853)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(4898506441296855)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'No'
,p_lov_return_value=>'N'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2505301077635389)
,p_lov_name=>'YN1'
,p_lov_query=>'.'||wwv_flow_api.id(2505301077635389)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2505604442635391)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2505923068635392)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'No'
,p_lov_return_value=>'N'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(2531513372639611)
,p_lov_name=>'YN2'
,p_lov_query=>'.'||wwv_flow_api.id(2531513372639611)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2532103809639612)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'No'
,p_lov_return_value=>'N'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(2531820722639611)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(3315502872865103)
,p_lov_name=>'YN3'
,p_lov_query=>'.'||wwv_flow_api.id(3315502872865103)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3315816148865104)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Yes'
,p_lov_return_value=>'Y'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(3316117333865104)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'No'
,p_lov_return_value=>'N'
);
end;
/
prompt --application/shared_components/navigation/trees
begin
null;
end;
/
prompt --application/pages/page_groups
begin
null;
end;
/
prompt --application/comments
begin
null;
end;
/
prompt --application/shared_components/navigation/breadcrumbs/breadcrumb
begin
wwv_flow_api.create_menu(
 p_id=>wwv_flow_api.id(24624313043658631)
,p_name=>' Breadcrumb'
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(3377332110919026)
,p_short_name=>'Administration'
,p_link=>'f?p=&APP_ID.:91:&SESSION.'
,p_page_id=>91
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(3384530714921740)
,p_short_name=>'Update RESPONSIBILITY'
,p_link=>'f?p=&APP_ID.:92:&SESSION.'
,p_page_id=>92
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(3391413954924828)
,p_short_name=>'Update Role Responsibilities'
,p_link=>'f?p=&APP_ID.:93:&SESSION.'
,p_page_id=>93
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(3395915970927473)
,p_short_name=>'Users'
,p_link=>'f?p=&APP_ID.:94:&SESSION.'
,p_page_id=>94
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(3404810692930554)
,p_short_name=>'User Roles'
,p_link=>'f?p=&APP_ID.:95:&SESSION.'
,p_page_id=>95
);
end;
/
prompt --application/shared_components/user_interface/templates/page
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4910524365052781)
,p_theme_id=>25
,p_name=>'Login'
,p_internal_name=>'LOGIN'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD# id="uLogin">',
'  <!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'  #FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uOneCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_12">',
'      <div id="uLoginContainer">',
'      #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'      #BODY#',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion uWhiteRegion uMessageRegion clearfix" id="uSuccessMessage">',
'  <div class="uRegionContent clearfix">',
'    <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"></a>',
'    <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'    <div class="uMessageText">',
'      #SUCCESS_MESSAGE#',
'    </div>',
'  </div>',
'</section>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uSingleAlertMessage red" id="uNotificationMessage">',
'	<p>#MESSAGE#</p>',
'	<a href="javascript:void(0)" class="closeMessage" onclick="apex.jQuery(''#uNotificationMessage'').remove();"></a>',
'</section>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0"'
,p_theme_class_id=>6
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
,p_template_comment=>'18'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4910821975052791)
,p_page_template_id=>wwv_flow_api.id(4910524365052781)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>4
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4911113982052795)
,p_theme_id=>25
,p_name=>'No Tabs - Left Sidebar'
,p_internal_name=>'NO_TABS_LEFT_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <a href="#HOME_LINK#" id="uLogo">#LOGO#</a>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uTwoColumns" class="sideLeftCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_2" id="uLeftCol">',
'      <aside>',
'        #REGION_POSITION_02#',
'      </aside>',
'    </div>',
'    <div class="apex_cols apex_span_10" id="uMidCol">',
'    #BODY#',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #REGION_POSITION_05#',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'      <div class="uFooterBG">',
'        <div class="uLeft"></div>',
'        <div class="uRight"></div>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion uWhiteRegion uMessageRegion clearfix" id="uSuccessMessage">',
'  <div class="uRegionContent clearfix">',
'    <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"></a>',
'    <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'    <div class="uMessageText">',
'      #SUCCESS_MESSAGE#',
'    </div>',
'  </div>',
'</section>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion uWhiteRegion uMessageRegion clearfix" id="uNotificationMessage">',
'  <div class="uRegionContent clearfix">',
'    <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"></a>',
'    <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'    <div class="uMessageText">',
'      #MESSAGE#',
'    </div>',
'  </div>',
'</section>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>'summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>17
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4911522473052796)
,p_page_template_id=>wwv_flow_api.id(4911113982052795)
,p_name=>'Main Content'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>10
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4911905224052797)
,p_page_template_id=>wwv_flow_api.id(4911113982052795)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4912311211052797)
,p_page_template_id=>wwv_flow_api.id(4911113982052795)
,p_name=>'Left Column'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4912731374052797)
,p_page_template_id=>wwv_flow_api.id(4911113982052795)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4913126925052798)
,p_page_template_id=>wwv_flow_api.id(4911113982052795)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4913428640052798)
,p_theme_id=>25
,p_name=>'No Tabs - Left and Right Sidebar'
,p_internal_name=>'NO_TABS_LEFT_AND_RIGHT_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uThreeColumns">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_2" id="uLeftCol">',
'      <aside>',
'        #REGION_POSITION_02#',
'      </aside>',
'    </div>',
'    <div class="apex_cols apex_span_8" id="uMidCol">',
'    #BODY#',
'    </div>',
'    <div class="apex_cols apex_span_2" id="uRightCol">',
'      <aside>',
'        #REGION_POSITION_03#',
'      </aside>',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>'summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_03'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>17
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4913806797052798)
,p_page_template_id=>wwv_flow_api.id(4913428640052798)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4914218906052798)
,p_page_template_id=>wwv_flow_api.id(4913428640052798)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4914612417052799)
,p_page_template_id=>wwv_flow_api.id(4913428640052798)
,p_name=>'Left Column'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4915015933052799)
,p_page_template_id=>wwv_flow_api.id(4913428640052798)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4915424865052799)
,p_page_template_id=>wwv_flow_api.id(4913428640052798)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4915820538052799)
,p_page_template_id=>wwv_flow_api.id(4913428640052798)
,p_name=>'Right Column'
,p_placeholder=>'REGION_POSITOIN_03'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4916122028052799)
,p_theme_id=>25
,p_name=>'No Tabs - No Sidebar'
,p_internal_name=>'NO_TABS_NO_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uOneCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_12">',
'      #BODY#',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>3
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_12">',
'  <section class="uRegion uNoHeading uErrorRegion">',
'    <div class="uRegionContent">',
'      <p class="errorIcon"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" class="iconLarge error"/></p>',
'      <p><strong>#MESSAGE#</strong></p>',
'      <p>#ADDITIONAL_INFO#</p>',
'      <div class="uErrorTechInfo">#TECHNICAL_INFO#</div>',
'    </div>',
'    <div class="uRegionHeading">',
'      <span class="uButtonContainer">',
'        <button onclick="#BACK_LINK#" class="uButtonLarge uHotButton" type="button"><span>#OK#</span></button>',
'      </span>',
'    </div>',
'  </section>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4916525977052800)
,p_page_template_id=>wwv_flow_api.id(4916122028052799)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4916924341052800)
,p_page_template_id=>wwv_flow_api.id(4916122028052799)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4917329106052800)
,p_page_template_id=>wwv_flow_api.id(4916122028052799)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4917712100052801)
,p_page_template_id=>wwv_flow_api.id(4916122028052799)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4918016514052801)
,p_theme_id=>25
,p_name=>'No Tabs - Right Sidebar'
,p_internal_name=>'NO_TABS_RIGHT_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uTwoColumns" class="sideRightCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_10" id="uMidCol">',
'    #BODY#',
'    </div>',
'    <div class="apex_cols apex_span_2" id="uRightCol">',
'      <aside>',
'        #REGION_POSITION_03#',
'      </aside>',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_03'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>3
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
end;
/
begin
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4918431124052802)
,p_page_template_id=>wwv_flow_api.id(4918016514052801)
,p_name=>'Main Content'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>10
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4918812689052802)
,p_page_template_id=>wwv_flow_api.id(4918016514052801)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4919235811052802)
,p_page_template_id=>wwv_flow_api.id(4918016514052801)
,p_name=>'Right Column'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4919609674052802)
,p_page_template_id=>wwv_flow_api.id(4918016514052801)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4920035999052802)
,p_page_template_id=>wwv_flow_api.id(4918016514052801)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4920324281052803)
,p_theme_id=>25
,p_name=>'One Level Tabs - Content Frame'
,p_internal_name=>'ONE_LEVEL_TABS_CONTENT_FRAME'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_javascript_code_onload=>'initContentFrameTabs();'
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uOneCol">',
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uRegion uRegionNoPadding clearfix uRegionFrame">',
'      <div class="uRegionHeading">',
'        <h1>#TITLE#</h1>',
'        <span class="uButtonContainer">',
'        #REGION_POSITION_02#',
'        </span>',
'      </div>',
'      <div class="uFrameContent">',
'        <div class="uFrameMain">',
'            #BODY#',
'        </div>',
'        <div class="uFrameSide">',
'          <div class="apex_cols apex_span_2 alpha omega">',
'            #REGION_POSITION_03#',
'          </div>',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>'
,p_sidebar_def_reg_pos=>'BODY_3'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>10
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4920716573052804)
,p_page_template_id=>wwv_flow_api.id(4920324281052803)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>10
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4921118607052804)
,p_page_template_id=>wwv_flow_api.id(4920324281052803)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4921532078052805)
,p_page_template_id=>wwv_flow_api.id(4920324281052803)
,p_name=>'Content Frame Buttons'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4921911872052805)
,p_page_template_id=>wwv_flow_api.id(4920324281052803)
,p_name=>'Side Column'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4922329463052805)
,p_page_template_id=>wwv_flow_api.id(4920324281052803)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4922716038052806)
,p_page_template_id=>wwv_flow_api.id(4920324281052803)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4923114084052806)
,p_page_template_id=>wwv_flow_api.id(4920324281052803)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4923514848052806)
,p_page_template_id=>wwv_flow_api.id(4920324281052803)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4923817325052806)
,p_theme_id=>25
,p_name=>'One Level Tabs - Left Sidebar'
,p_internal_name=>'ONE_LEVEL_TABS_LEFT_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uTwoColumns" class="sideLeftCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_2" id="uLeftCol">',
'      <aside>',
'        #REGION_POSITION_02#',
'      </aside>',
'    </div>',
'    <div class="apex_cols apex_span_10" id="uMidCol">',
'    #BODY#',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>16
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4924210590052807)
,p_page_template_id=>wwv_flow_api.id(4923817325052806)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>10
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4924622668052808)
,p_page_template_id=>wwv_flow_api.id(4923817325052806)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4925035241052808)
,p_page_template_id=>wwv_flow_api.id(4923817325052806)
,p_name=>'Left Column'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4925429631052808)
,p_page_template_id=>wwv_flow_api.id(4923817325052806)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4925810829052808)
,p_page_template_id=>wwv_flow_api.id(4923817325052806)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4926223648052809)
,p_page_template_id=>wwv_flow_api.id(4923817325052806)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4926612526052809)
,p_page_template_id=>wwv_flow_api.id(4923817325052806)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4926912616052809)
,p_theme_id=>25
,p_name=>'One Level Tabs - Left and Right Sidebar'
,p_internal_name=>'ONE_LEVEL_TABS_LEFT_AND_RIGHT_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uThreeColumns">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_2" id="uLeftCol">',
'      <aside>',
'        #REGION_POSITION_02#',
'      </aside>',
'    </div>',
'    <div class="apex_cols apex_span_8" id="uMidCol">',
'    #BODY#',
'    </div>',
'    <div class="apex_cols apex_span_2" id="uRightCol">',
'      <aside>',
'        #REGION_POSITION_03#',
'      </aside>',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a class="active" href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_03'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>16
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4927315740052810)
,p_page_template_id=>wwv_flow_api.id(4926912616052809)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4927720970052810)
,p_page_template_id=>wwv_flow_api.id(4926912616052809)
,p_name=>'Left Column'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4928110282052810)
,p_page_template_id=>wwv_flow_api.id(4926912616052809)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITON_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4928534904052810)
,p_page_template_id=>wwv_flow_api.id(4926912616052809)
,p_name=>'Right Column'
,p_placeholder=>'REGION_POSITON_03'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4928936093052811)
,p_page_template_id=>wwv_flow_api.id(4926912616052809)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITON_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4929319260052811)
,p_page_template_id=>wwv_flow_api.id(4926912616052809)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITON_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4929711070052811)
,p_page_template_id=>wwv_flow_api.id(4926912616052809)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITON_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4930131967052811)
,p_page_template_id=>wwv_flow_api.id(4926912616052809)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITON_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4930418112052812)
,p_theme_id=>25
,p_name=>'One Level Tabs - No Sidebar'
,p_internal_name=>'ONE_LEVEL_TABS_NO_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uOneCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_12">',
'      #BODY#',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>'class="regionColumns"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>1
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
end;
/
begin
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4930816358052812)
,p_page_template_id=>wwv_flow_api.id(4930418112052812)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4931217915052812)
,p_page_template_id=>wwv_flow_api.id(4930418112052812)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4931607540052812)
,p_page_template_id=>wwv_flow_api.id(4930418112052812)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4932020769052813)
,p_page_template_id=>wwv_flow_api.id(4930418112052812)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4932431019052813)
,p_page_template_id=>wwv_flow_api.id(4930418112052812)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4932822866052813)
,p_page_template_id=>wwv_flow_api.id(4930418112052812)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4933126622052813)
,p_theme_id=>25
,p_name=>'One Level Tabs - Right Sidebar'
,p_internal_name=>'ONE_LEVEL_TABS_RIGHT_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uTwoColumns" class="sideRightCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_10" id="uMidCol">',
'    #BODY#',
'    </div>',
'    <div class="apex_cols apex_span_2" id="uRightCol">',
'      <aside>',
'        #REGION_POSITION_03#',
'      </aside>',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_sidebar_def_reg_pos=>'REGION_POSITION_03'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>16
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'// show / hide grid',
'function showGrid() {',
'  apex.jQuery(''.apex_grid_container'').addClass(''showGrid'');',
'};',
'function hideGrid() {',
'  apex.jQuery(''.apex_grid_container'').removeClass(''showGrid'');',
'};',
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4933527500052814)
,p_page_template_id=>wwv_flow_api.id(4933126622052813)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>10
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4933936257052814)
,p_page_template_id=>wwv_flow_api.id(4933126622052813)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4934325861052815)
,p_page_template_id=>wwv_flow_api.id(4933126622052813)
,p_name=>'Right Column'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4934713988052815)
,p_page_template_id=>wwv_flow_api.id(4933126622052813)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4935131867052815)
,p_page_template_id=>wwv_flow_api.id(4933126622052813)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4935533283052815)
,p_page_template_id=>wwv_flow_api.id(4933126622052813)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4935917148052816)
,p_page_template_id=>wwv_flow_api.id(4933126622052813)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4936217938052816)
,p_theme_id=>25
,p_name=>'One Level Tabs - Wizard Page'
,p_internal_name=>'ONE_LEVEL_TABS_WIZARD_PAGE'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_javascript_code_onload=>'loadWizardTrain();'
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uOneCol">',
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <div class="cWizard">',
'      <div class="cWizardHeader">',
'        #REGION_POSITION_02#',
'      </div>',
'      <div class="cWizardContentContainer">',
'        <div class="cWizardContent">',
'            #BODY#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'</div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #REGION_POSITION_05#',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>8
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4936633391052816)
,p_page_template_id=>wwv_flow_api.id(4936217938052816)
,p_name=>'Wizard Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>11
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4937032787052816)
,p_page_template_id=>wwv_flow_api.id(4936217938052816)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4937414112052817)
,p_page_template_id=>wwv_flow_api.id(4936217938052816)
,p_name=>'Wizard Header'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4937825904052817)
,p_page_template_id=>wwv_flow_api.id(4936217938052816)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4938228149052817)
,p_page_template_id=>wwv_flow_api.id(4936217938052816)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4938608469052817)
,p_page_template_id=>wwv_flow_api.id(4936217938052816)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4939024901052817)
,p_page_template_id=>wwv_flow_api.id(4936217938052816)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4939337310052818)
,p_theme_id=>25
,p_name=>'Popup'
,p_internal_name=>'POPUP'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD# id="uPopup">',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uOneCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_12">',
'      #BODY#',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_theme_class_id=>4
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4939723457052818)
,p_page_template_id=>wwv_flow_api.id(4939337310052818)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4940031438052818)
,p_theme_id=>25
,p_name=>'Printer Friendly'
,p_internal_name=>'PRINTER_FRIENDLY'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD# class="printerFriendly">',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uOneCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_12">',
'      #BODY#',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>5
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_12">',
'  <section class="uRegion uNoHeading uErrorRegion">',
'    <div class="uRegionContent">',
'      <p class="errorIcon"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" class="iconLarge error"/></p>',
'      <p><strong>#MESSAGE#</strong></p>',
'      <p>#ADDITIONAL_INFO#</p>',
'      <div class="uErrorTechInfo">#TECHNICAL_INFO#</div>',
'    </div>',
'    <div class="uRegionHeading">',
'      <span class="uButtonContainer">',
'        <button onclick="#BACK_LINK#" class="uButtonLarge uHotButton" type="button"><span>#OK#</span></button>',
'      </span>',
'    </div>',
'  </section>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4940422988052818)
,p_page_template_id=>wwv_flow_api.id(4940031438052818)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4940813295052819)
,p_page_template_id=>wwv_flow_api.id(4940031438052818)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4941230742052819)
,p_page_template_id=>wwv_flow_api.id(4940031438052818)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4941629729052820)
,p_page_template_id=>wwv_flow_api.id(4940031438052818)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4941927351052820)
,p_theme_id=>25
,p_name=>'Two Level Tabs - Left Sidebar'
,p_internal_name=>'TWO_LEVEL_TABS_LEFT_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<div class="uParentTabs">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <ul>',
'        #PARENT_TAB_CELLS#',
'      </ul>',
'    </div>',
'  </div>  ',
'</div>',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uTwoColumns" class="sideLeftCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_2" id="uLeftCol">',
'      <aside>',
'        #REGION_POSITION_02#',
'      </aside>',
'    </div>',
'    <div class="apex_cols apex_span_10" id="uMidCol">',
'    #BODY#',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_top_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_top_non_curr_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>18
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
end;
/
begin
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4942313407052821)
,p_page_template_id=>wwv_flow_api.id(4941927351052820)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>10
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4942712227052821)
,p_page_template_id=>wwv_flow_api.id(4941927351052820)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4943116667052821)
,p_page_template_id=>wwv_flow_api.id(4941927351052820)
,p_name=>'Left Column'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4943535182052822)
,p_page_template_id=>wwv_flow_api.id(4941927351052820)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4943935449052822)
,p_page_template_id=>wwv_flow_api.id(4941927351052820)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4944326814052822)
,p_page_template_id=>wwv_flow_api.id(4941927351052820)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4944730672052822)
,p_page_template_id=>wwv_flow_api.id(4941927351052820)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4945004997052822)
,p_theme_id=>25
,p_name=>'Two Level Tabs - Left and Right Sidebar'
,p_internal_name=>'TWO_LEVEL_TABS_LEFT_AND_RIGHT_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<div class="uParentTabs">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <ul>',
'        #PARENT_TAB_CELLS#',
'      </ul>',
'    </div>',
'  </div>  ',
'</div>',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uThreeColumns">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_2" id="uLeftCol">',
'      <aside>',
'        #REGION_POSITION_02#',
'      </aside>',
'    </div>',
'    <div class="apex_cols apex_span_8" id="uMidCol">',
'    #BODY#',
'    </div>',
'    <div class="apex_cols apex_span_2" id="uRightCol">',
'      <aside>',
'        #REGION_POSITION_03#',
'      </aside>',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a class="active" href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_top_current_tab=>'<li><a class="active" href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_top_non_curr_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_03'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>18
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4945419773052823)
,p_page_template_id=>wwv_flow_api.id(4945004997052822)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4945825439052823)
,p_page_template_id=>wwv_flow_api.id(4945004997052822)
,p_name=>'Left Column'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4946215461052823)
,p_page_template_id=>wwv_flow_api.id(4945004997052822)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITON_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4946624174052823)
,p_page_template_id=>wwv_flow_api.id(4945004997052822)
,p_name=>'Right Column'
,p_placeholder=>'REGION_POSITON_03'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4947026456052824)
,p_page_template_id=>wwv_flow_api.id(4945004997052822)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITON_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4947421019052824)
,p_page_template_id=>wwv_flow_api.id(4945004997052822)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITON_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4947834153052824)
,p_page_template_id=>wwv_flow_api.id(4945004997052822)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITON_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4948226500052824)
,p_page_template_id=>wwv_flow_api.id(4945004997052822)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITON_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4948518691052824)
,p_theme_id=>25
,p_name=>'Two Level Tabs - No Sidebar'
,p_internal_name=>'TWO_LEVEL_TABS_NO_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<div class="uParentTabs">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <ul>',
'        #PARENT_TAB_CELLS#',
'      </ul>',
'    </div>',
'  </div>  ',
'</div>',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uOneCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_12">',
'      #BODY#',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_top_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_top_non_curr_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>2
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4948927032052825)
,p_page_template_id=>wwv_flow_api.id(4948518691052824)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4949307065052825)
,p_page_template_id=>wwv_flow_api.id(4948518691052824)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4949708523052825)
,p_page_template_id=>wwv_flow_api.id(4948518691052824)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4950128928052825)
,p_page_template_id=>wwv_flow_api.id(4948518691052824)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4950529308052826)
,p_page_template_id=>wwv_flow_api.id(4948518691052824)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4950926794052826)
,p_page_template_id=>wwv_flow_api.id(4948518691052824)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(4951212461052826)
,p_theme_id=>25
,p_name=>'Two Level Tabs - Right Sidebar'
,p_internal_name=>'TWO_LEVEL_TABS_RIGHT_SIDEBAR'
,p_is_popup=>false
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#MODERNIZR_URL#',
'[if lt IE 9]#IMAGE_PREFIX#libraries/respond-js/1.1.0/respond.min.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/js/4_2#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#themes/theme_25/css/4_2.css?v=#APEX_VERSION#',
'#IMAGE_PREFIX#themes/theme_25/css/responsive_grid.css?v=#APEX_VERSION#'))
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!--[if HTML5]><![endif]-->',
'<!doctype html>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<!--[if lt IE 7 ]> <html class="ie6 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 7 ]>    <html class="ie7 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 8 ]>    <html class="ie8 no-css3 no-js" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if IE 9 ]>    <html class="ie9" lang="&BROWSER_LANGUAGE."> <![endif]-->',
'<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" lang="&BROWSER_LANGUAGE."> <!--<![endif]-->',
'<head>',
'<!--[if !HTML5]>',
'  ',
'<![endif]-->',
'  <meta charset="UTF-8">',
'  <title>#TITLE#</title>',
'  <link rel="icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'  <link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'  <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'</head>',
'<body #ONLOAD#>',
'<!--[if lte IE 6]><div id="outdated-browser">#OUTDATED_BROWSER#</div><![endif]-->',
'#FORM_OPEN#',
'<div class="uParentTabs">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <ul>',
'        #PARENT_TAB_CELLS#',
'      </ul>',
'    </div>',
'  </div>  ',
'</div>',
'<header id="uHeader">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      #REGION_POSITION_07#',
'      <div class="logoBar">',
'        <h1><a href="#HOME_LINK#" id="uLogo">#LOGO#</a></h1>',
'        <div class="userBlock">',
'          <img src="#IMAGE_PREFIX#f_spacer.gif" class="navIcon user" alt="">',
'          <span>&APP_USER.</span>',
'          #NAVIGATION_BAR#',
'        </div>',
'      </div>',
'    </div>',
'  </div>',
'  <nav>',
'    <div class="apex_grid_container clearfix">',
'      <div class="apex_cols apex_span_12">',
'        <ul class="uMainNav">',
'          #TAB_CELLS#',
'          #REGION_POSITION_06#',
'        </ul>',
'        #REGION_POSITION_08#',
'      </div>',
'    </div>',
'  </nav>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="uBodyContainer">',
'#REGION_POSITION_01#',
'#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'<div id="uTwoColumns" class="sideRightCol">',
'  <div class="apex_grid_container">',
'    <div class="apex_cols apex_span_10" id="uMidCol">',
'    #BODY#',
'    </div>',
'    <div class="apex_cols apex_span_2" id="uRightCol">',
'      <aside>',
'        #REGION_POSITION_03#',
'      </aside>',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<footer id="uFooter">',
'  <div class="apex_grid_container clearfix">',
'    <div class="apex_cols apex_span_12">',
'      <div class="uFooterContent">',
'        #REGION_POSITION_05#',
'        <div id="customize">#CUSTOMIZE#</div>',
'        #SCREEN_READER_TOGGLE#',
'        <span class="uFooterVersion">',
'          #APP_VERSION#',
'        </span>',
'      </div>',
'    </div>',
'  </div>',
'</footer>',
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion successMessage clearfix" id="uSuccessMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uSuccessMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uCheckmarkIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#SUCCESS_MESSAGE_HEADING#</h2>',
'          #SUCCESS_MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_non_current_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_top_current_tab=>'<li><a href="#TAB_LINK#" class="active">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_top_non_curr_tab=>'<li><a href="#TAB_LINK#">#TAB_LABEL#</a>#TAB_INLINE_EDIT#</li>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <section class="uMessageRegion errorMessage clearfix" id="uNotificationMessage">',
'      <div class="uRegionContent clearfix">',
'        <a href="javascript:void(0)" onclick="apex.jQuery(''#uNotificationMessage'').remove();" class="uCloseMessage"><span class="visuallyhidden">#CLOSE_NOTIFICATION#</span></a>',
'        <img src="#IMAGE_PREFIX#f_spacer.gif" class="uWarningIcon" alt="" />',
'        <div class="uMessageText">',
'          <h2 class="visuallyhidden">#ERROR_MESSAGE_HEADING#</h2>',
'          #MESSAGE#',
'        </div>',
'      </div>',
'    </section>',
'  </div>',
'</div>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#">#TEXT#</a>#EDIT#'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_03'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>2
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>false
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_template=>'#ROWS#'
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_cols apex_span_#COLUMN_SPAN_NUMBER# #FIRST_LAST_COLUMN_ATTRIBUTES#">',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_grid_javascript_debug_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex.jQuery(document)',
'    .on("apex-devbar-grid-debug-on", showGrid)',
'    .on("apex-devbar-grid-debug-off", hideGrid);'))
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4951606621052826)
,p_page_template_id=>wwv_flow_api.id(4951212461052826)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>10
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4952013648052827)
,p_page_template_id=>wwv_flow_api.id(4951212461052826)
,p_name=>'Breadcrumb'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4952411443052827)
,p_page_template_id=>wwv_flow_api.id(4951212461052826)
,p_name=>'Right Column'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>2
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4952814527052827)
,p_page_template_id=>wwv_flow_api.id(4951212461052826)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4953210550052827)
,p_page_template_id=>wwv_flow_api.id(4951212461052826)
,p_name=>'Page Level Tabs'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4953629729052827)
,p_page_template_id=>wwv_flow_api.id(4951212461052826)
,p_name=>'Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(4954010312052828)
,p_page_template_id=>wwv_flow_api.id(4951212461052826)
,p_name=>'Icon Nav Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24614592041658531)
,p_theme_id=>20
,p_name=>'Login'
,p_internal_name=>'LOGIN'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody" height="70%" align="center" width="400">',
'<td width="100%" valign="top" height="100%" id="t20ContentBody" align="center">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_theme_class_id=>6
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
end;
/
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24614708201658538)
,p_theme_id=>20
,p_name=>'No Tabs'
,p_internal_name=>'NO_TABS'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" height="100%" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>3
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24614800815658538)
,p_theme_id=>20
,p_name=>'No Tabs with Sidebar'
,p_internal_name=>'NO_TABS_WITH_SIDEBAR'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" height="100%" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" '
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>17
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24614914776658539)
,p_theme_id=>20
,p_name=>'One Level Tabs'
,p_internal_name=>'ONE_LEVEL_TABS'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#TAB_CELLS#</div>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>1
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615000945658539)
,p_theme_id=>20
,p_name=>'One Level Tabs (Custom 1)'
,p_internal_name=>'ONE_LEVEL_TABS_CUSTOM_1'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table id="t20Tabs" border="0" cellpadding="0" cellspacing="0" summary=""><tr>#TAB_CELLS#</tr></table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_non_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>8
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615097954658539)
,p_theme_id=>20
,p_name=>'One Level Tabs (Custom 5)'
,p_internal_name=>'ONE_LEVEL_TABS_CUSTOM_5'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br /></td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#TAB_CELLS#</div>',
'</div>',
'#REGION_POSITION_08#',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>12
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615215231658540)
,p_theme_id=>20
,p_name=>'One Level Tabs Sidebar'
,p_internal_name=>'ONE_LEVEL_TABS_SIDEBAR'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#TAB_CELLS#</div>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>',
''))
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>16
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615308069658541)
,p_theme_id=>20
,p_name=>'One Level Tabs Sidebar  (Custom 2)'
,p_internal_name=>'ONE_LEVEL_TABS_SIDEBAR_CUSTOM_2'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table id="t20Tabs" border="0" cellpadding="0" cellspacing="0" summary=""><tr>#TAB_CELLS#</tr></table>',
'</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_non_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>9
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615396714658541)
,p_theme_id=>20
,p_name=>'One Level Tabs Sidebar (Custom 6)'
,p_internal_name=>'ONE_LEVEL_TABS_SIDEBAR_CUSTOM_6'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br /></td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#TAB_CELLS#</div>',
'</div>',
'#REGION_POSITION_08#',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>',
''))
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>13
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
end;
/
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615507504658542)
,p_theme_id=>20
,p_name=>'Popup'
,p_internal_name=>'POPUP'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" cellpadding="0" width="100%" cellspacing="0" border="0">',
'<tr>',
'<td width="100%" valign="top">',
'<div style="border:1px solid black;">#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'#BODY##REGION_POSITION_04#</td>',
'<td valign="top">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>',
'#REGION_POSITION_05#',
''))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE##DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'<div class="t20NavigationBar">#BAR_BODY#</div>'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavigationBar">#TEXT#</a>'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_theme_class_id=>4
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615606755658543)
,p_theme_id=>20
,p_name=>'Printer Friendly'
,p_internal_name=>'PRINTER_FRIENDLY'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" width="100%">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table summary="" cellpadding="0" width="100%" cellspacing="0" border="0" height="70%">',
'<tr>',
'<td width="100%" valign="top"><div class="t20messages">#SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'#BODY##REGION_POSITION_02##REGION_POSITION_04#</td>',
'<td valign="top">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'<div class="t20NavigationBar">#BAR_BODY#</div>'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavigationBar">#TEXT#</a>'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_theme_class_id=>5
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
,p_template_comment=>'3'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615716733658543)
,p_theme_id=>20
,p_name=>'Two Level Tabs'
,p_internal_name=>'TWO_LEVEL_TABS'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#PARENT_TAB_CELLS#</div>',
'</div>',
'<div id="t20tablist">#TAB_CELLS#</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="current">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_top_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_top_non_curr_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5" '
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>2
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615802290658543)
,p_theme_id=>20
,p_name=>'Two Level Tabs  (Custom 3)'
,p_internal_name=>'TWO_LEVEL_TABS_CUSTOM_3'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table id="t20Tabs" border="0" cellpadding="0" cellspacing="0" summary=""><tr>#PARENT_TAB_CELLS#</tr></table>',
'</div>',
'<div id="t20ChildTabs">#TAB_CELLS#</div>',
'<div style="background-color:none;">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_02##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#" class="t20Tab">#TAB_LABEL#</a>'
,p_top_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_top_non_curr_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>10
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24615894312658544)
,p_theme_id=>20
,p_name=>'Two Level Tabs with Sidebar'
,p_internal_name=>'TWO_LEVEL_TABS_WITH_SIDEBAR'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<div id="t20Tabs" class="tablight">#PARENT_TAB_CELLS#</div>',
'</div>',
'<div id="t20tablist">#TAB_CELLS#</div>',
'<div id="t20BreadCrumbsLeft">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td width="100%" valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#" class="current">#TAB_LABEL#</a>',
''))
,p_non_current_tab=>'<a href="#TAB_LINK#">#TAB_LABEL#</a>'
,p_top_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>',
''))
,p_top_non_curr_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#TAB_LINK#">#TAB_LABEL#</a>',
''))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>18
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(24616011486658544)
,p_theme_id=>20
,p_name=>'Two Level Tabs with Sidebar  (Custom 4)'
,p_internal_name=>'TWO_LEVEL_TABS_WITH_SIDEBAR_CUSTOM_4'
,p_is_popup=>false
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE." xmlns:htmldb="http://htmldb.oracle.com">',
'<head>',
'<meta http-equiv="x-ua-compatible" content="IE=edge" />',
'<title>#TITLE#</title>',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_4_0.css" type="text/css" />',
'<!--[if IE]><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/ie.css" type="text/css" /><![endif]-->',
'#APEX_CSS#',
'#THEME_CSS#',
'#TEMPLATE_CSS#',
'#THEME_STYLE_CSS#',
'#APPLICATION_CSS#',
'#PAGE_CSS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#',
'#HEAD#',
'</head>',
'<body #ONLOAD#>#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="t20PageHeader">',
'<table border="0" cellpadding="0" cellspacing="0" summary="">',
'<tr>',
'<td id="t20Logo" valign="top">#LOGO#<br />#REGION_POSITION_06#</td>',
'<td id="t20HeaderMiddle"  valign="top" width="100%">#REGION_POSITION_07#<br /></td>',
'<td id="t20NavBar" valign="top">#NAVIGATION_BAR#<br />#REGION_POSITION_08#</td>',
'</tr>',
'</table>',
'<table id="t20Tabs" border="0" cellpadding="0" cellspacing="0" summary=""><tr>#PARENT_TAB_CELLS#</tr></table>',
'</div>',
'<div id="t20ChildTabs">#TAB_CELLS#</div>',
'<div style="background-color:none;">#REGION_POSITION_01#</div>',
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageBody"  width="100%" height="70%">',
'<td valign="top" width="200" id="t20ContentLeft">#REGION_POSITION_02#<br /></td>',
'<td valign="top" id="t20ContentBody">',
'<div id="t20Messages">#GLOBAL_NOTIFICATION##SUCCESS_MESSAGE##NOTIFICATION_MESSAGE#</div>',
'<div id="t20ContentMiddle">#BODY##REGION_POSITION_04#</div>',
'</td>',
'<td valign="top" width="200" id="t20ContentRight">#REGION_POSITION_03#<br /></td>',
'</tr>',
'</table>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" id="t20PageFooter" width="100%">',
'<tr>',
'<td id="t20Left" valign="top"><span id="t20UserPrompt">&APP_USER.</span><br /></td>',
'<td id="t20Center" valign="top">#REGION_POSITION_05#</td>',
'<td id="t20Right" valign="top"><span id="t20Customize">#CUSTOMIZE#</span><br /></td>',
'</tr>',
'</table>',
'<br class="t20Break"/>',
'#FORM_CLOSE# ',
'#DEVELOPER_TOOLBAR#',
'#GENERATED_CSS#',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td><td class="tM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td></tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#SUCCESS_MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_current_tab=>'<a href="#TAB_LINK#" class="t20CurrentTab">#TAB_LABEL#</a>'
,p_non_current_tab=>'<a href="#TAB_LINK#" class="t20Tab">#TAB_LABEL#</a>'
,p_top_current_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_top_non_curr_tab=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#TAB_LINK#">#TAB_LABEL#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table summary="" border="0" cellpadding="0" cellspacing="0" id="t20Notification">',
'<tr>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-L.gif" alt="" /></td>',
'<td class="tM"></td>',
'<td valign="top"><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxTop-R.gif" alt="" /></td>',
'</tr>',
'<tr><td class="L"></td><td width="100%"><img src="#IMAGE_PREFIX#delete.gif" onclick="$x_Remove(''t20Notification'')"  style="float:right;" class="pb" alt="" />#MESSAGE#</td><td class="R"></td></tr>',
'<tr><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-L.gif" alt="" /></td><td class="bM"></td><td><img src="#IMAGE_PREFIX#themes/theme_20/msgBoxBtm-R.gif" alt="" /></td></tr>',
'</table>'))
,p_navigation_bar=>'#BAR_BODY#'
,p_navbar_entry=>'<a href="#LINK#" class="t20NavBar">#TEXT#</a> |'
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="5"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_02'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>11
,p_grid_type=>'TABLE'
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/button
begin
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(229841743278055)
,p_template_name=>'HTML button (legacy - APEX 5 migration)'
,p_internal_name=>'HTML_BUTTON_LEGACY_APEX_5_MIGRATION'
,p_template=>' <input type="button" value="#LABEL#" onclick="#JAVASCRIPT#" id="#BUTTON_ID#" class="#BUTTON_CSS_CLASSES#" #BUTTON_ATTRIBUTES#/>'
,p_hot_template=>' <input type="button" value="#LABEL#" onclick="#JAVASCRIPT#" id="#BUTTON_ID#" class="#BUTTON_CSS_CLASSES#" #BUTTON_ATTRIBUTES#/>'
,p_translate_this_template=>'N'
,p_theme_class_id=>13
,p_theme_id=>20
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(4969527198052860)
,p_template_name=>'Button'
,p_internal_name=>'BUTTON'
,p_template=>'<button class="uButton #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button"><span>#LABEL#</span></button> '
,p_hot_template=>'<button class="uButton uHotButton #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button"><span>#LABEL#</span></button> '
,p_translate_this_template=>'N'
,p_theme_class_id=>1
,p_theme_id=>25
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(4969707697052862)
,p_template_name=>'Button - Icon'
,p_internal_name=>'BUTTON_ICON'
,p_template=>'<button class="uButton iconButton #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button"><span><i class="iL"></i>#LABEL#<i class="iR"></i></span></button> '
,p_hot_template=>'<button class="uButton uHotButton iconButton #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button"><span><i class="iL"></i>#LABEL#<i class="iR"></i></span></button> '
,p_translate_this_template=>'N'
,p_theme_class_id=>6
,p_theme_id=>25
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(4969935128052862)
,p_template_name=>'Button - Icon Only'
,p_internal_name=>'BUTTON_ICON_ONLY'
,p_template=>'<button class="uButton iconOnly iconButton #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button" title="#LABEL#"><span><i></i></span></button> '
,p_hot_template=>'<button class="uButton uHotButton iconOnly iconButton #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button" title="#LABEL#"><span><i></i></span></button> '
,p_translate_this_template=>'N'
,p_theme_class_id=>7
,p_theme_id=>25
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(4970122196052862)
,p_template_name=>'Large Button'
,p_internal_name=>'LARGE_BUTTON'
,p_template=>'<button class="uButtonLarge #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button"><span>#LABEL#</span></button> '
,p_hot_template=>'<button class="uButtonLarge uHotButton #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button"><span>#LABEL#</span></button> '
,p_translate_this_template=>'N'
,p_theme_class_id=>5
,p_template_comment=>'Standard Button'
,p_theme_id=>25
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(4970322177052862)
,p_template_name=>'Large Button - Icon'
,p_internal_name=>'LARGE_BUTTON_ICON'
,p_template=>'<button class="uButtonLarge iconButton #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button"><span><i class="iL"></i>#LABEL#<i class="iR"></i></span></button> '
,p_hot_template=>'<button class="uButtonLarge uHotButton iconButton #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button"><span><i class="iL"></i>#LABEL#<i class="iR"></i></span></button> '
,p_translate_this_template=>'N'
,p_theme_class_id=>5
,p_theme_id=>25
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(4970535718052862)
,p_template_name=>'Large Button - Icon Only'
,p_internal_name=>'LARGE_BUTTON_ICON_ONLY'
,p_template=>'<button class="uButtonLarge iconButton iconOnly #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button" title="#LABEL#"><span><i></i></span></button> '
,p_hot_template=>'<button class="uButtonLarge uHotButton iconButton iconOnly #BUTTON_CSS_CLASSES#" onclick="#JAVASCRIPT#" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#" type="button" title="#LABEL#"><span><i></i></span></button> '
,p_translate_this_template=>'N'
,p_theme_class_id=>8
,p_theme_id=>25
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(24616088703658545)
,p_template_name=>'Button'
,p_internal_name=>'BUTTON'
,p_template=>'<a href="#LINK#" class="t20Button">#LABEL#</a>'
,p_translate_this_template=>'N'
,p_theme_class_id=>1
,p_template_comment=>'Standard Button'
,p_theme_id=>20
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(24616196836658548)
,p_template_name=>'Button, Alternative 1'
,p_internal_name=>'BUTTON,_ALTERNATIVE_1'
,p_template=>'<a href="#LINK#" class="t20Button2">#LABEL#</a>'
,p_translate_this_template=>'N'
,p_theme_class_id=>4
,p_template_comment=>'XP Square FFFFFF'
,p_theme_id=>20
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(24616318271658548)
,p_template_name=>'Button, Alternative 2'
,p_internal_name=>'BUTTON,_ALTERNATIVE_2'
,p_template=>'<a href="#LINK#" class="t20Button">#LABEL#</a>'
,p_translate_this_template=>'N'
,p_theme_class_id=>5
,p_template_comment=>'Standard Button'
,p_theme_id=>20
);
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(24616405313658548)
,p_template_name=>'Button, Alternative 3'
,p_internal_name=>'BUTTON,_ALTERNATIVE_3'
,p_template=>'<a href="#LINK#" class="t20Button">#LABEL#</a>'
,p_translate_this_template=>'N'
,p_theme_class_id=>2
,p_template_comment=>'Standard Button'
,p_theme_id=>20
);
end;
/
prompt --application/shared_components/user_interface/templates/region
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4954322690052829)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES# class="#REGION_CSS_CLASSES#">',
'  <h1 class="visuallyhidden">#TITLE#</h1>',
'  #BODY#',
'</section>'))
,p_page_plug_template_name=>'Accessible Region with Heading'
,p_internal_name=>'ACCESSIBLE_REGION_WITH_HEADING'
,p_theme_id=>25
,p_theme_class_id=>21
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4954616551052835)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion uWhiteRegion uAlertRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uRegionHeading">',
'    <h1>#TITLE#</h1>',
'  </div>',
'  <div class="uRegionContent clearfix">',
'    #BODY#',
'  </div>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'</section>'))
,p_page_plug_template_name=>'Alert Region'
,p_internal_name=>'ALERT_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>25
,p_theme_class_id=>10
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Used for alerts and confirmations.  Please use a region image for the success/warning icon'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4954925491052835)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion uBorderlessRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uRegionHeading">',
'    <h1>#TITLE#</h1>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'  </div>',
'  <div class="uRegionContent clearfix">',
'    #BODY#',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Borderless Region'
,p_internal_name=>'BORDERLESS_REGION'
,p_theme_id=>25
,p_theme_class_id=>7
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Use this region template when you want to contain content without a border.',
'',
'TITLE=YES',
'BUTTONS=YES',
'100% WIDTH=NO'))
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4955212930052835)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="uBreadcrumbsContainer #REGION_CSS_CLASSES#" #REGION_ATTRIBUTES# id="#REGION_STATIC_ID#">',
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <div class="uBreadcrumbs">',
'      #BODY#',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_page_plug_template_name=>'Breadcrumb Region'
,p_internal_name=>'BREADCRUMB_REGION'
,p_theme_id=>25
,p_theme_class_id=>6
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Use this region template to contain breadcrumb menus.  Breadcrumb menus are implemented using breadcrumbs.  Breadcrumb menus are designed to displayed in #REGION_POSITION_01#',
'',
'',
'',
'<div id="uBreadcrumbsContainer #REGION_CSS_CLASSES#" #REGION_ATTRIBUTES# id="#REGION_STATIC_ID#">',
'<div class="apex_grid_container">',
'  <div class="apex_cols apex_span_12">',
'    <div id="uBreadcrumbs">',
'      #BODY#',
'      <div class="uBreadcrumbsBG">',
'        <div class="uLeft"></div>',
'        <div class="uRight"></div>',
'      </div>',
'    </div>',
'  </div>',
'</div>',
'</div>'))
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4955506692052836)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uButtonRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uButtonRegionContentContainer">',
'    <h1>#TITLE#</h1>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Button Region with Title'
,p_internal_name=>'BUTTON_REGION_WITH_TITLE'
,p_theme_id=>25
,p_theme_class_id=>4
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4955832834052836)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uButtonRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uButtonRegionContentContainer">',
'    <div class="uButtonRegionContent">#BODY#</div>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Button Region without Title'
,p_internal_name=>'BUTTON_REGION_WITHOUT_TITLE'
,p_theme_id=>25
,p_theme_class_id=>17
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4956116839052836)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#SUB_REGION_HEADERS#',
'#BODY#',
'<div class="uFrameContainer" class="#REGION_CSS_CLASSES# #REGION_ATTRIBUTES# id="#REGION_STATIC_ID#">',
'#SUB_REGIONS#',
'</div>'))
,p_sub_plug_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="uFrameRegionSelector clearfix">',
'  <ul>',
'    <li><a href="javascript:void(0);" class="showAllLink active"><span>Show All</span></a></li>',
'    #ENTRIES#',
'  </ul>',
'</div>'))
,p_sub_plug_header_entry_templ=>'<li><a href="javascript:void(0);" id="sub_#SUB_REGION_ID#"><span>#SUB_REGION_TITLE#</span></a></li>'
,p_page_plug_template_name=>'Content Frame Body Container'
,p_internal_name=>'CONTENT_FRAME_BODY_CONTAINER'
,p_theme_id=>25
,p_theme_class_id=>7
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4956407635052837)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES# class="#REGION_CSS_CLASSES#"> ',
'#BODY#',
'#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'</div>'))
,p_page_plug_template_name=>'DIV Region with ID'
,p_internal_name=>'DIV_REGION_WITH_ID'
,p_theme_id=>25
,p_theme_class_id=>22
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4956708135052837)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uRegionHeading">',
'    <h1>#TITLE#</h1>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'  </div>',
'  <div class="uRegionContent clearfix">',
'    #BODY#',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Form Region'
,p_internal_name=>'FORM_REGION'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>25
,p_theme_class_id=>8
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(4957127534052839)
,p_plug_template_id=>wwv_flow_api.id(4956708135052837)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>-1
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4957433087052840)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion uHideShowRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uRegionHeading">',
'    <h1>',
'      <a href="javascript:void(0)" class="uRegionControl"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="Hide/Show"/></a>',
'      #TITLE#',
'    </h1>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'  </div>',
'  <div class="uRegionContent clearfix">',
'    #BODY#',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Hide and Show Region (Expanded)'
,p_internal_name=>'HIDE_AND_SHOW_REGION_EXPANDED'
,p_theme_id=>25
,p_theme_class_id=>1
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4957711314052840)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion uHideShowRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uRegionHeading">',
'    <h1>',
'      <a href="javascript:void(0)" class="uRegionControl uRegionCollapsed"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="Hide/Show"/></a>',
'      #TITLE#',
'    </h1>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'  </div>',
'  <div class="uRegionContent clearfix" style="display: none;">',
'    #BODY#',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Hide and Show Region (Hidden)'
,p_internal_name=>'HIDE_AND_SHOW_REGION_HIDDEN'
,p_theme_id=>25
,p_theme_class_id=>1
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4958007764052841)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uIRRegion" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES# class="#REGION_CSS_CLASSES#">',
'  <h1 class="visuallyhidden">#TITLE#</h1>',
'  #BODY#',
'</section>'))
,p_page_plug_template_name=>'Interactive Report Region'
,p_internal_name=>'INTERACTIVE_REPORT_REGION'
,p_theme_id=>25
,p_theme_class_id=>21
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4958329534052841)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="apex_grid_container modal_grid">',
'  <div class="apex_cols apex_span_8 modal_col">',
'    <section class="uRegion uWhiteRegion uModalRegion uAlertRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'      <div class="uRegionHeading">',
'        <h1>#TITLE#</h1>',
'      </div>',
'      <div class="uRegionContent clearfix">',
'        #BODY#',
'      </div>',
'        <span class="uButtonContainer">',
'          #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'        </span>',
'    </section>',
'  </div>',
'</div>'))
,p_page_plug_template_name=>'Modal Region'
,p_internal_name=>'MODAL_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>25
,p_theme_class_id=>9
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(4958736628052841)
,p_plug_template_id=>wwv_flow_api.id(4958329534052841)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>6
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4959017262052841)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion uNoHeading #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uRegionContent clearfix">',
'    #BODY#',
'#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Region without Buttons and Titles'
,p_internal_name=>'REGION_WITHOUT_BUTTONS_AND_TITLES'
,p_theme_id=>25
,p_theme_class_id=>19
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4959323197052842)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uRegionHeading">',
'    <h1>#TITLE#</h1>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'  </div>',
'  <div class="uRegionContent clearfix">',
'    #BODY#',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Sidebar Region'
,p_internal_name=>'SIDEBAR_REGION'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>25
,p_theme_class_id=>2
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0">',
'        <tr>',
'          <td rowspan="2" valign="top" width="4" bgcolor="#FF0000"><img src="#IMAGE_PREFIX#tl_img.gif" border="0" width="4" height="18" alt="" /></td>',
'          <td bgcolor="#000000" height="1"><img src="#IMAGE_PREFIX#stretch.gif" width="142" height="1" border="0" alt="" /></td>',
'          <td rowspan="2" valign="top" width="4" bgcolor="#FF0000"><img src="#IMAGE_PREFIX#tr_img.gif" border="0" width="4" height="18" alt="" /></td>',
'        </tr>',
'        <tr>',
'          <td bgcolor="#FF0000" height="16">',
'            <table border="0" cellpadding="0" cellspacing="0" width="100%">',
'              <tr>',
'                <td align=middle valign="top">',
'                  <div align="center">',
'                     <font color="#ffffff" face="Arial, Helvetica, sans-serif" size="1">',
'                      <b>#TITLE# </b></font></div>',
'                </td>',
'              </tr>',
'            </table>',
'          </td>',
'        </tr>',
'</table>',
'<table border="0" cellpadding="0" cellspacing="0">',
'   <tr>',
'   <td bgcolor="#000000" width="1" height="96"><img src="#IMAGE_PREFIX#stretch.gif" width="1" height="1" border="0" alt="" /></td>',
'   <td valign="top" height="96"><img src="#IMAGE_PREFIX#stretch.gif" width="146" height="1" border="0" alt="" /><br />',
'            <table border="0" cellpadding="1" cellspacing="0" width="146" summary="">',
'              <tr>',
'                <td colspan="2">',
'                  <table border="0" cellpadding="2" cellspacing="0" width="124" summary="">',
'                    <tr>',
'                      <td>&nbsp;</td>',
'                      <td valign="top" width="106">',
'                        <P><FONT face="arial, helvetica" size="1">',
'                            #BODY#',
'                           </font>',
'                        </P>',
'                      </td>',
'                    </tr>',
'                  </table>',
'            </table>',
'          </td>',
'          <td bgcolor="#000000" width="1" height="96"><img src="#IMAGE_PREFIX#stretch.gif" width="1" height="1" border="0" alt="" /></td>',
'          <td bgcolor="#9a9c9a" width="1" height="96"><img src="#IMAGE_PREFIX#stretch.gif" width="1" height="1" border="0" alt="" /></td>',
'          <td bgcolor="#b3b4b3" width="1" height="96"><img src="#IMAGE_PREFIX#stretch.gif" width="1" height="1" border="0" alt="" /></td>',
'        </tr>',
'      </table>',
'      <table border="0" cellpadding="0" cellspacing="0">',
'        <tr>',
'          <td rowspan="4" valign="top" width="4"><img src="#IMAGE_PREFIX#bl_img.gif" border="0" width="4" height="6" alt="" /></td>',
'          <td bgcolor="#ffffff" height="2"><img src="#IMAGE_PREFIX#stretch.gif" width="142" height="1" border="0" alt="" /></td>',
'          <td rowspan="4" valign="top" width="4"><img src="#IMAGE_PREFIX#br_img.gif" border="0" width="4" height="6" alt="" /></td>',
'        </tr>',
'        <tr>',
'          <td bgcolor="#000000" width="1"><img src="#IMAGE_PREFIX#stretch.gif" width="1" height="1" border="0" alt="" /></td>',
'        </tr>',
'        <tr>',
'          <td bgcolor="#9a9c9a" width="1"><img src="#IMAGE_PREFIX#stretch.gif" width="1" height="1" border="0" alt="" /></td>',
'        </tr>',
'        <tr>',
'          <td bgcolor="#b3b4b3" width="1" height="2"><img src="#IMAGE_PREFIX#stretch.gif" width="1" height="1" border="0" alt="" /></td>',
'        </tr>',
'</table>',
''))
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4959633248052842)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uRegionHeading">',
'    <h1>#TITLE#</h1>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'  </div>',
'  <div class="uRegionContent clearfix">',
'    #BODY#',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Standard Region'
,p_internal_name=>'STANDARD_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>25
,p_theme_class_id=>9
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(4960033113052842)
,p_plug_template_id=>wwv_flow_api.id(4959633248052842)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>-1
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(4960423946052842)
,p_plug_template_id=>wwv_flow_api.id(4959633248052842)
,p_name=>'Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>-1
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4960729278052843)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<section class="uRegion uRegionNoPadding #REGION_CSS_CLASSES# clearfix" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="uRegionHeading">',
'    <h1>#TITLE#</h1>',
'    <span class="uButtonContainer">',
'      #CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'    </span>',
'  </div>',
'  <div class="uRegionContent clearfix">',
'    #BODY#',
'  </div>',
'</section>'))
,p_page_plug_template_name=>'Standard Region - No Padding'
,p_internal_name=>'STANDARD_REGION_NO_PADDING'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>25
,p_theme_class_id=>13
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(4961024264052843)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="cWizardButtons cWizardButtonsLeft">',
'#PREVIOUS##CLOSE#',
'</div>',
'<div class="cWizardButtons cWizardButtonsRight">',
'#NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#',
'</div>'))
,p_page_plug_template_name=>'Wizard Buttons'
,p_internal_name=>'WIZARD_BUTTONS'
,p_theme_id=>25
,p_theme_class_id=>28
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24616505490658549)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20Borderless" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Borderless Region'
,p_internal_name=>'BORDERLESS_REGION'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>20
,p_theme_class_id=>7
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24616614856658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20Bracketed" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Bracketed Region'
,p_internal_name=>'BRACKETED_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>18
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24616716460658552)
,p_layout=>'TABLE'
,p_template=>'<div id="#REGION_STATIC_ID#" class="t20Breadcrumbs" #REGION_ATTRIBUTES#>#BODY#</div>'
,p_page_plug_template_name=>'Breadcrumb Region'
,p_internal_name=>'BREADCRUMB_REGION'
,p_theme_id=>20
,p_theme_class_id=>6
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24616793459658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ButtonRegionwithTitle" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'</tbody>',
'</table>#BODY#'))
,p_page_plug_template_name=>'Button Region with Title'
,p_internal_name=>'BUTTON_REGION_WITH_TITLE'
,p_theme_id=>20
,p_theme_class_id=>4
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24616892192658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ButtonRegionwithoutTitle" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'</tbody>',
'</table>#BODY#'))
,p_page_plug_template_name=>'Button Region without Title'
,p_internal_name=>'BUTTON_REGION_WITHOUT_TITLE'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>17
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24616999608658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ChartRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Chart Region'
,p_internal_name=>'CHART_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>30
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617090878658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20FormRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Form Region'
,p_internal_name=>'FORM_REGION'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>20
,p_theme_class_id=>8
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617201281658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20HideShow" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header"><img src="#IMAGE_PREFIX#themes/theme_20/collapse_plus.gif" onclick="htmldb_ToggleWithImage(this,''#REGION_STATIC_ID#_body'')" class="pb" alt="" />#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body" style="display:none;">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Hide and Show Region'
,p_internal_name=>'HIDE_AND_SHOW_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>1
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617293591658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ListRegionwithIcon" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'List Region with Icon (Chart)'
,p_internal_name=>'LIST_REGION_WITH_ICON_CHART'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>29
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617403733658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t20Region t20NavRegion" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'<div class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</div>',
'<div id="#REGION_STATIC_ID#_body" class="t20RegionBody">#BODY#</div>',
'</div>'))
,p_page_plug_template_name=>'Navigation Region'
,p_internal_name=>'NAVIGATION_REGION'
,p_theme_id=>20
,p_theme_class_id=>5
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617518335658552)
,p_layout=>'TABLE'
,p_template=>'<div class="t20Region t20NavRegionAlt" id="#REGION_STATIC_ID#"#REGION_ATTRIBUTES#><div class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</div><div id="#REGION_STATIC_ID#_body" class="t20RegionBody">#BODY#</div></div>'
,p_page_plug_template_name=>'Navigation Region, Alternative 1'
,p_internal_name=>'NAVIGATION_REGION,_ALTERNATIVE_1'
,p_theme_id=>20
,p_theme_class_id=>16
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617610342658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20RegionwithoutButtonsandTitle" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Region without Buttons and Title'
,p_internal_name=>'REGION_WITHOUT_BUTTONS_AND_TITLE'
,p_theme_id=>20
,p_theme_class_id=>19
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617707682658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20RegionwithoutTitle" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Region without Title'
,p_internal_name=>'REGION_WITHOUT_TITLE'
,p_theme_id=>20
,p_theme_class_id=>11
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617802860658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="apex_finderbar" cellpadding="0" cellspacing="0" border="0" summary="" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'<tbody>',
'<tr>',
'<td class="apex_finderbar_left_top" valign="top"><img src="#IMAGE_PREFIX#1px_trans.gif" width="10" height="8" alt=""  class="spacer" alt="" /></td>',
'<td class="apex_finderbar_middle" rowspan="3" valign="middle"><img src="#IMAGE_PREFIX#htmldb/builder/builder_find.png" /></td>',
'<td class="apex_finderbar_middle" rowspan="3" valign="middle" style="">#BODY#</td>',
'<td class="apex_finderbar_left" rowspan="3" width="10"><br /></td>',
'<td class="apex_finderbar_buttons" rowspan="3" valign="middle" nowrap="nowrap"><span class="apex_close">#CLOSE#</span><span>#EDIT##CHANGE##DELETE##CREATE##CREATE2##COPY##PREVIOUS##NEXT##EXPAND##HELP#</span></td>',
'</tr>',
'<tr><td class="apex_finderbar_left_middle"><br /></td></tr>',
'<tr>',
'<td class="apex_finderbar_left_bottom" valign="bottom"><img src="#IMAGE_PREFIX#1px_trans.gif" width="10" height="8"  class="spacer" alt="" /></td>',
'</tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Report Filter - Single Row'
,p_internal_name=>'REPORT_FILTER_SINGLE_ROW'
,p_theme_id=>20
,p_theme_class_id=>31
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
end;
/
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617899530658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ReportList" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Report List'
,p_internal_name=>'REPORT_LIST'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>29
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24617998492658552)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ReportRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Reports Region'
,p_internal_name=>'REPORTS_REGION'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>9
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24618090021658553)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ReportsRegion100" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Reports Region 100% Width'
,p_internal_name=>'REPORTS_REGION_100%_WIDTH'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>13
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24618213411658553)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20ReportsRegionAlt" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Reports Region, Alternative 1'
,p_internal_name=>'REPORTS_REGION,_ALTERNATIVE_1'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>20
,p_theme_class_id=>10
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24618298282658553)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20SidebarRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Sidebar Region'
,p_internal_name=>'SIDEBAR_REGION'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>20
,p_theme_class_id=>2
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24618396899658553)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20SidebarRegionAlt" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Sidebar Region, Alternative 1'
,p_internal_name=>'SIDEBAR_REGION,_ALTERNATIVE_1'
,p_plug_table_bgcolor=>'#f7f7e7'
,p_theme_id=>20
,p_theme_class_id=>3
,p_plug_heading_bgcolor=>'#f7f7e7'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24618503287658553)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="rounded-corner-region-blank" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="rc-gray-top">',
'    <div class="rc-gray-top-r"></div>',
'  </div>',
'  <div class="rc-body">',
'    <div class="rc-body-r">',
'      <div class="rc-content-main">',
'        <div class="rc-left">',
'          #BODY#',
'        </div>',
'        <div class="rc-right">',
'          #CLOSE##COPY##DELETE##CHANGE##EDIT##PREVIOUS##NEXT##CREATE##EXPAND#',
'        </div>',
'        <div class="clear"></div>',
'      </div>',
'    </div>',
'  </div>',
'  <div class="rc-bottom">',
'    <div class="rc-bottom-r"></div>',
'  </div>',
'</div>'))
,p_page_plug_template_name=>'Top Bar'
,p_internal_name=>'TOP_BAR'
,p_theme_id=>20
,p_theme_class_id=>21
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24618609768658553)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20WizardRegion" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Wizard Region'
,p_internal_name=>'WIZARD_REGION'
,p_theme_id=>20
,p_theme_class_id=>12
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(24618706612658553)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="t20Region t20WizardRegionIcon" id="#REGION_STATIC_ID#" border="0" cellpadding="0" cellspacing="0" summary="" #REGION_ATTRIBUTES#>',
'<thead><tr><th class="t20RegionHeader" id="#REGION_STATIC_ID#_header">#TITLE#</th></tr></thead>',
'<tbody id="#REGION_STATIC_ID#_body">',
'<tr><td class="t20ButtonHolder">#CLOSE##PREVIOUS##NEXT##DELETE##EDIT##CHANGE##CREATE##CREATE2##EXPAND##COPY##HELP#</td></tr>',
'<tr><td class="t20RegionBody">#BODY#</td></tr>',
'</tbody>',
'</table>'))
,p_page_plug_template_name=>'Wizard Region with Icon'
,p_internal_name=>'WIZARD_REGION_WITH_ICON'
,p_theme_id=>20
,p_theme_class_id=>20
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/list
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4964627710052851)
,p_list_template_current=>'<button onclick="javascript:location.href=''#LINK#''" class="uButton uHotButton #A01#" type="button"><span>#TEXT#</span></a> '
,p_list_template_noncurrent=>'<button onclick="javascript:location.href=''#LINK#''" class="uButton #A01#" type="button"><span>#TEXT#</span></a> '
,p_list_template_name=>'Button List'
,p_internal_name=>'BUTTON_LIST'
,p_theme_id=>25
,p_theme_class_id=>6
,p_list_template_before_rows=>'<div class="uButtonList">'
,p_list_template_after_rows=>'</div>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4964908195052854)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li>',
'  <a href="#LINK#">',
'    <h3>#TEXT#</h3>',
'    <p>#A01#</p>',
'  </a>',
'</li>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li>',
'  <a href="#LINK#">',
'    <h3>#TEXT#</h3>',
'    <p>#A01#</p>',
'  </a>',
'</li>'))
,p_list_template_name=>'Featured List with Subtext'
,p_internal_name=>'FEATURED_LIST_WITH_SUBTEXT'
,p_theme_id=>25
,p_theme_class_id=>1
,p_list_template_before_rows=>'<ul class="featuredLinksList">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4965237313052854)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    <li class="active">',
'      <a href="#LINK#">',
'        <img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# alt="" />',
'        <span>#TEXT#</span>',
'      </a>',
'    </li>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    <li>',
'      <a href="#LINK#">',
'        <img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# alt="" />',
'        <span>#TEXT#</span>',
'      </a>',
'    </li>'))
,p_list_template_name=>'Horizontal Images with Label List'
,p_internal_name=>'HORIZONTAL_IMAGES_WITH_LABEL_LIST'
,p_theme_id=>25
,p_theme_class_id=>4
,p_list_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="uImagesList uHorizontalImagesList clearfix">',
'  <ul>'))
,p_list_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  </ul>',
'</div>'))
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4965532321052854)
,p_list_template_current=>'<li class="active"><a href="#LINK#">#TEXT#</a></li> '
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li> '
,p_list_template_name=>'Horizontal Links List'
,p_internal_name=>'HORIZONTAL_LINKS_LIST'
,p_theme_id=>25
,p_theme_class_id=>3
,p_list_template_before_rows=>'<ul class="uHorizontalLinksList">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4965804916052854)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    <li class="#LIST_STATUS#">',
'      <span>#TEXT#</span>',
'    </li>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    <li class="#LIST_STATUS#">',
'      <span>#TEXT#</span>',
'    </li>'))
,p_list_template_name=>'Horizontal Wizard Progress List'
,p_internal_name=>'HORIZONTAL_WIZARD_PROGRESS_LIST'
,p_theme_id=>25
,p_theme_class_id=>17
,p_list_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="uHorizontalProgressList hidden-phone">',
'  <ul>'))
,p_list_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  </ul>',
'</div>'))
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4966107072052855)
,p_list_template_current=>'<li><a href="#LINK#" class="active">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Page Level Tabs List'
,p_internal_name=>'PAGE_LEVEL_TABS_LIST'
,p_theme_id=>25
,p_theme_class_id=>7
,p_list_template_before_rows=>' '
,p_list_template_after_rows=>' '
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4966431824052856)
,p_list_template_current=>'<li class="active"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Tabbed Navigation List'
,p_internal_name=>'TABBED_NAVIGATION_LIST'
,p_theme_id=>25
,p_theme_class_id=>7
,p_list_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="uHorizontalTabs">',
'<ul>'))
,p_list_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'</div>'))
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4966731221052856)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    <li class="active">',
'      <a href="#LINK#">',
'        <img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# alt="" />',
'        <span>#TEXT#</span>',
'      </a>',
'    </li>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'    <li>',
'      <a href="#LINK#">',
'        <img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# alt="" />',
'        <span>#TEXT#</span>',
'      </a>',
'    </li>'))
,p_list_template_name=>'Vertical Images List'
,p_internal_name=>'VERTICAL_IMAGES_LIST'
,p_theme_id=>25
,p_theme_class_id=>5
,p_list_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="uImagesList uVerticalImagesList clearfix">',
'  <ul>'))
,p_list_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  </ul>',
'</div>'))
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4967018784052856)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li>',
'    <a href="#LINK#">',
'      <img src="#IMAGE_PREFIX#f_spacer.gif" class="#A02#" alt="#LIST_LABEL#"/>',
'      <h3>#TEXT#</h3>',
'      <h4>#A01#</h4>',
'    </a>',
'  </li>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li>',
'    <a href="#LINK#">',
'      <img src="#IMAGE_PREFIX#f_spacer.gif" class="#A02#" alt="#LIST_LABEL#"/>',
'      <h3>#TEXT#</h3>',
'      <h4>#A01#</h4>',
'    </a>',
'  </li>'))
,p_list_template_name=>'Vertical List with Subtext and Icon'
,p_internal_name=>'VERTICAL_LIST_WITH_SUBTEXT_AND_ICON'
,p_theme_id=>25
,p_theme_class_id=>1
,p_list_template_before_rows=>'<ul class="largeLinkList">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4967327223052856)
,p_list_template_current=>'<li class="active"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Ordered List'
,p_internal_name=>'VERTICAL_ORDERED_LIST'
,p_theme_id=>25
,p_theme_class_id=>2
,p_list_template_before_rows=>'<ol class="uNumberedList">'
,p_list_template_after_rows=>'</ol>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4967636643052856)
,p_list_template_current=>'<li class="active"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Sidebar List'
,p_internal_name=>'VERTICAL_SIDEBAR_LIST'
,p_theme_id=>25
,p_theme_class_id=>19
,p_list_template_before_rows=>'<ul class="uVerticalSidebarList">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4967908050052857)
,p_list_template_current=>'<li class="active"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Unordered List with Bullets'
,p_internal_name=>'VERTICAL_UNORDERED_LIST_WITH_BULLETS'
,p_theme_id=>25
,p_theme_class_id=>1
,p_list_template_before_rows=>'<ul class="uVerticalList">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4968235872052857)
,p_list_template_current=>'<li class="active"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Unordered List without Bullets'
,p_internal_name=>'VERTICAL_UNORDERED_LIST_WITHOUT_BULLETS'
,p_theme_id=>25
,p_theme_class_id=>18
,p_list_template_before_rows=>'<ul class="uVerticalList noBullets">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(4968516457052857)
,p_list_template_current=>'<li class="#LIST_STATUS#"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /><span>#TEXT#</span></li>'
,p_list_template_noncurrent=>'<li class="#LIST_STATUS#"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /><span>#TEXT#</span></li>'
,p_list_template_name=>'Wizard Progress List - Vertical'
,p_internal_name=>'WIZARD_PROGRESS_LIST_VERTICAL'
,p_theme_id=>25
,p_theme_class_id=>17
,p_list_template_before_rows=>'<div class="uVerticalProgressList" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#><ul>'
,p_list_template_after_rows=>'</ul></div>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24618795088658553)
,p_list_template_current=>'<a href="#LINK#" class="t20Button t20current">#TEXT#</a>'
,p_list_template_noncurrent=>'<a href="#LINK#" class="t20Button">#TEXT#</a>'
,p_list_template_name=>'Button List'
,p_internal_name=>'BUTTON_LIST'
,p_theme_id=>20
,p_theme_class_id=>6
,p_list_template_before_rows=>'<div class="t20ButtonList">'
,p_list_template_after_rows=>'</div>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24618911331658557)
,p_list_template_current=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Hierarchical Expanded'
,p_internal_name=>'HIERARCHICAL_EXPANDED'
,p_theme_id=>20
,p_theme_class_id=>23
,p_list_template_before_rows=>'<ul class="htmlTree">'
,p_list_template_after_rows=>'</ul><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#">'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_sub_list_item_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_item_templ_noncurr_w_child=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_sub_templ_curr_w_child=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_sub_templ_noncurr_w_child=>'<li><a href="#LINK#">#TEXT#</a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619004315658557)
,p_list_template_current=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/node.gif" align="middle" alt="" /><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/node.gif" align="middle"  alt="" /><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Hierarchical Expanding'
,p_internal_name=>'HIERARCHICAL_EXPANDING'
,p_theme_id=>20
,p_theme_class_id=>22
,p_list_template_before_rows=>'<ul class="dhtmlTree">'
,p_list_template_after_rows=>'</ul><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#" style="display:none;" class="dhtmlTree">'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/node.gif" align="middle"  alt="" /><a href="#LINK#">#TEXT#</a></li>'
,p_sub_list_item_noncurrent=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/node.gif"  align="middle" alt="" /><a href="#LINK#">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/plus.gif" align="middle"  onclick="htmldb_ToggleWithImage(this,''#LIST_ITEM_ID#'')" class="pseudoButtonInactive" /><a href="#LINK#">#TEXT#</a></li>'
,p_item_templ_noncurr_w_child=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/plus.gif" align="middle"  onclick="htmldb_ToggleWithImage(this,''#LIST_ITEM_ID#'')" class="pseudoButtonInactive" /><a href="#LINK#">#TEXT#</a></li>'
,p_sub_templ_curr_w_child=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/plus.gif" onclick="htmldb_ToggleWithImage(this,''#LIST_ITEM_ID#'')" align="middle" class="pseudoButtonInactive" /><a href="#LINK#">#TEXT#</a></li>'
,p_sub_templ_noncurr_w_child=>'<li><img src="#IMAGE_PREFIX#themes/theme_13/plus.gif" onclick="htmldb_ToggleWithImage(this,''#LIST_ITEM_ID#'')" align="middle" class="pseudoButtonInactive" /><a href="#LINK#">#TEXT#</a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619086631658557)
,p_list_template_current=>'<td class="t20current"><img src="#IMAGE_PREFIX##IMAGE#" border="0" #IMAGE_ATTR#/><br />#TEXT#</td>'
,p_list_template_noncurrent=>'<td><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" border="0" #IMAGE_ATTR#/></a><br /><a href="#LINK#">#TEXT#</a></td>'
,p_list_template_name=>'Horizontal Images with Label List'
,p_internal_name=>'HORIZONTAL_IMAGES_WITH_LABEL_LIST'
,p_theme_id=>20
,p_theme_class_id=>4
,p_list_template_before_rows=>'<table class="t20HorizontalImageswithLabelList" cellpadding="0" border="0" cellspacing="0" summary=""><tr>'
,p_list_template_after_rows=>'</tr></table>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619200154658557)
,p_list_template_current=>'<a href="#LINK#" class="t20current">#TEXT#</a>'
,p_list_template_noncurrent=>'<a href="#LINK#">#TEXT#</a>'
,p_list_template_name=>'Horizontal Links List'
,p_internal_name=>'HORIZONTAL_LINKS_LIST'
,p_theme_id=>20
,p_theme_class_id=>3
,p_list_template_before_rows=>'<div class="t20HorizontalLinksList">'
,p_list_template_after_rows=>'</div>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619312993658557)
,p_list_template_current=>'<li class="dhtmlMenuItem"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li class="dhtmlMenuItem"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Pull Down Menu'
,p_internal_name=>'PULL_DOWN_MENU'
,p_theme_id=>20
,p_theme_class_id=>20
,p_list_template_before_rows=>'<ul class="dhtmlMenuLG2">'
,p_list_template_after_rows=>'</ul><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#" class="dhtmlSubMenu2" style="display:none;">'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li class="dhtmlMenuSep2"><img src="#IMAGE_PREFIX#themes/theme_13/1px_trans.gif"  width="1" height="1" alt="" class="dhtmlMenuSep2" /></li>'
,p_sub_list_item_noncurrent=>'<li><a href="#LINK#" class="dhtmlSubMenuN" onmouseover="dhtml_CloseAllSubMenusL(this)">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<li class="dhtmlMenuItem1"><a href="#LINK#">#TEXT#</a><img src="#IMAGE_PREFIX#themes/theme_13/menu_small.gif" alt="Expand" onclick="app_AppMenuMultiOpenBottom2(this,''#LIST_ITEM_ID#'',false)" /></li>'
,p_item_templ_noncurr_w_child=>'<li class="dhtmlMenuItem1"><a href="#LINK#">#TEXT#</a><img src="#IMAGE_PREFIX#themes/theme_13/menu_small.gif" alt="Expand" onclick="app_AppMenuMultiOpenBottom2(this,''#LIST_ITEM_ID#'',false)" /></li>'
,p_sub_templ_curr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
,p_sub_templ_noncurr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619414906658557)
,p_list_template_current=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#menu/drop_down.png" width="20" height="128" alt="" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_list_template_noncurrent=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#menu/drop_down.png" width="20" height="128" alt=""  /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_list_template_name=>'Pull Down Menu with Image'
,p_internal_name=>'PULL_DOWN_MENU_WITH_IMAGE'
,p_theme_id=>20
,p_theme_class_id=>21
,p_list_template_before_rows=>'<div class="dhtmlMenuLG">'
,p_list_template_after_rows=>'</div><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#" class="dhtmlSubMenu2" style="display:none;"><li class="dhtmlSubMenuP" onmouseover="dhtml_CloseAllSubMenusL(this)">#PARENT_TEXT#</li>'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li class="dhtmlMenuSep"><img src="#IMAGE_PREFIX#themes/theme_13/1px_trans.gif"  width="1" height="1" alt=""  class="dhtmlMenuSep" /></li>'
,p_sub_list_item_noncurrent=>'<li><a href="#LINK#" class="dhtmlSubMenuN" onmouseover="dhtml_CloseAllSubMenusL(this)">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#menu/drop_down.png" width="20" height="128" alt=""  class="dhtmlMenu" onclick="app_AppMenuMultiOpenBottom(this,''#LIST_ITEM_ID#'',fa'
||'lse)" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_item_templ_noncurr_w_child=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#menu/drop_down.png" width="20" height="128" alt=""  class="dhtmlMenu" onclick="app_AppMenuMultiOpenBottom(this,''#LIST_ITEM_ID#'',fa'
||'lse)" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_sub_templ_curr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
,p_sub_templ_noncurr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619507969658558)
,p_list_template_current=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/generic_list.gif" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#themes/generic_nochild.gif" width="22" height="75" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_list_template_noncurrent=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/generic_list.gif" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#themes/generic_nochild.gif" width="22" height="75" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_list_template_name=>'Pull Down Menu with Image (Custom 1)'
,p_internal_name=>'PULL_DOWN_MENU_WITH_IMAGE_CUSTOM_1'
,p_theme_id=>20
,p_theme_class_id=>9
,p_list_template_before_rows=>'<div class="dhtmlMenuLG">'
,p_list_template_after_rows=>'</div><br style="clear:both;"/><br style="clear:both;"/>'
,p_before_sub_list=>'<ul id="#PARENT_LIST_ITEM_ID#" htmldb:listlevel="#LEVEL#" class="dhtmlSubMenu2" style="display:none;"><li class="dhtmlSubMenuP" onmouseover="dhtml_CloseAllSubMenusL(this)">#PARENT_TEXT#</li>'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li class="dhtmlMenuSep"><img src="#IMAGE_PREFIX#themes/theme_13/1px_trans.gif"  width="1" height="1" alt=""  class="dhtmlMenuSep" /></li>'
,p_sub_list_item_noncurrent=>'<li><a href="#LINK#" class="dhtmlSubMenuN" onmouseover="dhtml_CloseAllSubMenusL(this)">#TEXT#</a></li>'
,p_item_templ_curr_w_child=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/generic_list.gif" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#themes/generic_open.gif" width="22" height="75" class="dhtmlMenu" onclick="app_AppMenuMultiOpenBottom(this,''#LIST_'
||'ITEM_ID#'',false)" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_item_templ_noncurr_w_child=>'<div class="dhtmlMenuItem"><a href="#LINK#"><img src="#IMAGE_PREFIX#themes/generic_list.gif" #IMAGE_ATTR# /></a><img src="#IMAGE_PREFIX#themes/generic_open.gif" width="22" height="75" class="dhtmlMenu" onclick="app_AppMenuMultiOpenBottom(this,''#LIST_'
||'ITEM_ID#'',false)" /><a href="#LINK#" class="dhtmlBottom">#TEXT#</a></div>'
,p_sub_templ_curr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
,p_sub_templ_noncurr_w_child=>'<li class="dhtmlSubMenuS"><a href="#LINK#" class="dhtmlSubMenuS" onmouseover="dhtml_MenuOpen(this,''#LIST_ITEM_ID#'',true,''Left'')"><span style="float:left;">#TEXT#</span><img class="t13MIMG" src="#IMAGE_PREFIX#menu_open_right2.gif" /></a></li>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619615649658558)
,p_list_template_current=>'<a href="#LINK#" class="current">#TEXT#</a>'
,p_list_template_noncurrent=>'<a href="#LINK#">#TEXT#</a>'
,p_list_template_name=>'Tab List (Custom 3)'
,p_internal_name=>'TAB_LIST_CUSTOM_3'
,p_theme_id=>20
,p_theme_class_id=>11
,p_list_template_before_rows=>'<div id="t20tablist">'
,p_list_template_after_rows=>'</div>'
,p_list_template_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'This list matches the child tabs on the Two Level Tab templates.',
'Use in region position 8 of One Level Tabs (Custom 5) and/or One Level Tabs Sidebar (Custom 6).'))
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619714961658558)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabL.gif" /></td>',
'<td class="t20CurrentTab"><a href="#LINK#">#TEXT#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabL.gif" /></td>',
'<td class="t20Tab"><a href="#LINK#">#TEXT#</a></td>',
'<td><img src="#IMAGE_PREFIX#themes/theme_20/topDimTabR.gif" /></td>',
'<td>&nbsp;</td>'))
,p_list_template_name=>'Tabbed Navigation List'
,p_internal_name=>'TABBED_NAVIGATION_LIST'
,p_theme_id=>20
,p_theme_class_id=>7
,p_list_template_before_rows=>'<table class="t20Tabs t20TabbedNavigationList" border="0" cellpadding="0" cellspacing="0" summary=""><tr>'
,p_list_template_after_rows=>'</tr></table>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619788365658558)
,p_list_template_current=>'<tr><td class="t20current"><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# />#TEXT#</a></td></tr>'
,p_list_template_noncurrent=>'<tr><td><a href="#LINK#"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# />#TEXT#</a></td></tr>'
,p_list_template_name=>'Vertical Images List'
,p_internal_name=>'VERTICAL_IMAGES_LIST'
,p_theme_id=>20
,p_theme_class_id=>5
,p_list_template_before_rows=>'<table border="0" cellpadding="0" cellspacing="0" summary="" class="t20VerticalImagesList">'
,p_list_template_after_rows=>'</table>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24619888228658558)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<tr><td align="left"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></td><td align="left"><a href="#LINK#">#TEXT#</a></td></tr>',
''))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<tr><td align="left"><img src="#IMAGE_PREFIX##IMAGE#" #IMAGE_ATTR# /></td><td align="left"><a href="#LINK#">#TEXT#</a></td></tr>',
''))
,p_list_template_name=>'Vertical Images List (Custom 2)'
,p_internal_name=>'VERTICAL_IMAGES_LIST_CUSTOM_2'
,p_theme_id=>20
,p_theme_class_id=>10
,p_list_template_before_rows=>'<table border="0" cellpadding="0" cellspacing="5" summary="" >'
,p_list_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
''))
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24620016959658558)
,p_list_template_current=>'<li class="t20current"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Ordered List'
,p_internal_name=>'VERTICAL_ORDERED_LIST'
,p_theme_id=>20
,p_theme_class_id=>2
,p_list_template_before_rows=>'<ol class="t20VerticalOrderedList">'
,p_list_template_after_rows=>'</ol>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24620114681658558)
,p_list_template_current=>'<a href="#LINK#" class="current">#TEXT#</a>'
,p_list_template_noncurrent=>'<a href="#LINK#">#TEXT#</a>'
,p_list_template_name=>'Vertical Sidebar List'
,p_internal_name=>'VERTICAL_SIDEBAR_LIST'
,p_theme_id=>20
,p_theme_class_id=>19
,p_list_template_before_rows=>'<div class="t20VerticalSidebarList">'
,p_list_template_after_rows=>'</div>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24620194388658558)
,p_list_template_current=>'<li class="t20current"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Unordered Links without Bullets'
,p_internal_name=>'VERTICAL_UNORDERED_LINKS_WITHOUT_BULLETS'
,p_theme_id=>20
,p_theme_class_id=>18
,p_list_template_before_rows=>'<ul class="t20VerticalUnorderedLinkswithoutBullets">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24620302058658558)
,p_list_template_current=>'<li class="t20current"><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_noncurrent=>'<li><a href="#LINK#">#TEXT#</a></li>'
,p_list_template_name=>'Vertical Unordered List with Bullets'
,p_internal_name=>'VERTICAL_UNORDERED_LIST_WITH_BULLETS'
,p_theme_id=>20
,p_theme_class_id=>1
,p_list_template_before_rows=>'<ul class="t20VerticalUnorderedListwithBullets">'
,p_list_template_after_rows=>'</ul>'
);
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(24620416340658558)
,p_list_template_current=>'<div class="t20current">#TEXT#</div>'
,p_list_template_noncurrent=>'<div>#TEXT#</div>'
,p_list_template_name=>'Wizard Progress List'
,p_internal_name=>'WIZARD_PROGRESS_LIST'
,p_theme_id=>20
,p_theme_class_id=>17
,p_list_template_before_rows=>'<div class="t20WizardProgressList">'
,p_list_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<center>&DONE.</center>',
'</div>'))
);
end;
/
prompt --application/shared_components/user_interface/templates/report
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4961322623052844)
,p_row_template_name=>'Borderless Report'
,p_internal_name=>'BORDERLESS_REPORT'
,p_row_template1=>'<td #ALIGNMENT# headers="#COLUMN_HEADER_NAME#">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="uReportContainer uBorderlessReportContainer" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#" summary="">',
'<tbody class="uReportPagination">',
'#TOP_PAGINATION#',
'</tbody>',
'<tbody class="uReportBody">',
'<tr><td>',
'<table summary="#REGION_TITLE#" class="uReport uReportBorderless">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</tbody>',
'</table>',
'</td>',
'</tr>',
'</tbody>',
'<tbody class="uReportPagination">',
'#PAGINATION#',
'</tbody>',
'</table>',
'<div class="uReportDownloadLinks">#EXTERNAL_LINK##CSV_LINK#</div>'))
,p_row_template_type=>'GENERIC_COLUMNS'
,p_before_column_heading=>'<thead>'
,p_column_heading_template=>'<th #ALIGNMENT# id="#COLUMN_HEADER_NAME#" #COLUMN_WIDTH#>#COLUMN_HEADER#</th>'
,p_after_column_heading=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</thead>',
'<tbody>'))
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_page_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT_SET# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_set_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_mouse_over=>'#F0F0F0'
,p_row_style_checked=>'#E8E8E8'
,p_theme_id=>25
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(4961322623052844)
,p_row_template_before_first=>'<tr>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4961622159052848)
,p_row_template_name=>'Comment Bubbles'
,p_internal_name=>'COMMENT_BUBBLES'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="#1#">',
'<div>',
'	<em>#2#</em>',
'	#3##4##5##6##7#',
'</div>',
'<span>',
'	#8# (#9#) #10#',
'</span>',
'</li>'))
,p_row_template_before_rows=>'<ul class="commentBubbles">'
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'<table class="uPaginationTable">',
'#PAGINATION#',
'</table>'))
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="sPaginationNext">#PAGINATION_NEXT#</a>'
,p_previous_page_template=>'<a href="#LINK#" class="sPaginationPrev">#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="sPaginationNext">#PAGINATION_NEXT_SET#</a>'
,p_previous_set_template=>'<a href="#LINK#" class="sPaginationPrev">#PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>25
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4961924254052849)
,p_row_template_name=>'Fixed Headers'
,p_internal_name=>'FIXED_HEADERS'
,p_row_template1=>'<td #ALIGNMENT# headers="#COLUMN_HEADER_NAME#" #COLUMN_WIDTH#>#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="uReportContainer" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#" summary="">',
'<tbody class="uReportPagination">',
'#TOP_PAGINATION#',
'</tbody>',
'<tbody class="uReportBody">',
'<tr><td>',
'<div class="uFixedHeadersContainer">',
'<table summary="#REGION_TITLE#" class="uReport uReportFixedHeaders">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</tbody>',
'</table>',
'</div>',
'</td>',
'</tr>',
'</tbody>',
'<tbody class="uReportPagination">',
'#PAGINATION#',
'</tbody>',
'</table>',
'<div class="uReportDownloadLinks">#EXTERNAL_LINK##CSV_LINK#</div>'))
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_before_column_heading=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<thead>',
''))
,p_column_heading_template=>'<th #ALIGNMENT# id="#COLUMN_HEADER_NAME#" #COLUMN_WIDTH#>#COLUMN_HEADER#</th>'
,p_after_column_heading=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</thead>',
'<tbody>',
''))
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_page_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT_SET# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_set_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_mouse_over=>'#F0F0F0'
,p_row_style_checked=>'#E8E8E8'
,p_theme_id=>25
,p_theme_class_id=>7
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(4961924254052849)
,p_row_template_before_first=>'<tr>'
,p_row_template_after_last=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</tr>',
''))
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4962221312052849)
,p_row_template_name=>'Horizontal Border'
,p_internal_name=>'HORIZONTAL_BORDER'
,p_row_template1=>'<td #ALIGNMENT# headers="#COLUMN_HEADER_NAME#">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="uReportContainer" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#" summary="">',
'<tbody class="uReportPagination">',
'#TOP_PAGINATION#',
'</tbody>',
'<tbody class="uReportBody">',
'<tr><td>',
'<table summary="#REGION_TITLE#" class="uReport uReportHorizontal">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</tbody>',
'</table>',
'</td>',
'</tr>',
'</tbody>',
'<tbody class="uReportPagination">',
'#PAGINATION#',
'</tbody>',
'</table>',
'<div class="uReportDownloadLinks">#EXTERNAL_LINK##CSV_LINK#</div>'))
,p_row_template_type=>'GENERIC_COLUMNS'
,p_before_column_heading=>'<thead>'
,p_column_heading_template=>'<th #ALIGNMENT# id="#COLUMN_HEADER_NAME#" #COLUMN_WIDTH#>#COLUMN_HEADER#</th>'
,p_after_column_heading=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</thead>',
'<tbody>'))
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_page_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT_SET# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_set_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_mouse_over=>'#F0F0F0'
,p_row_style_checked=>'#E8E8E8'
,p_theme_id=>25
,p_theme_class_id=>2
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(4962221312052849)
,p_row_template_before_first=>'<tr>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4962515267052849)
,p_row_template_name=>'One Column Unordered List'
,p_internal_name=>'ONE_COLUMN_UNORDERED_LIST'
,p_row_template1=>'<li>#COLUMN_VALUE#</li>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="uReportList" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#" summary="">',
'<tbody class="uReportPagination">',
'#TOP_PAGINATION#',
'</tbody>',
'<tbody class="uReportBody">',
'<tr><td>',
'<ul class="uReportList">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'</td>',
'</tr>',
'</tbody>',
'<tbody class="uReportPagination">',
'#PAGINATION#',
'</tbody>',
'</table>',
'<div class="uReportDownloadLinks">#EXTERNAL_LINK##CSV_LINK#</div>'))
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'NOT_CONDITIONAL'
,p_row_template_display_cond2=>'NOT_CONDITIONAL'
,p_row_template_display_cond3=>'NOT_CONDITIONAL'
,p_row_template_display_cond4=>'NOT_CONDITIONAL'
,p_next_page_template=>'<a href="#LINK#" class="sPaginationNext">#PAGINATION_NEXT#</a>'
,p_previous_page_template=>'<a href="#LINK#" class="sPaginationPrev">#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="sPaginationNext">#PAGINATION_NEXT_SET#</a>'
,p_previous_set_template=>'<a href="#LINK#" class="sPaginationPrev">#PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>25
,p_theme_class_id=>3
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(4962515267052849)
,p_row_template_before_first=>'OMIT'
,p_row_template_after_last=>'OMIT'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4962813910052849)
,p_row_template_name=>'Search Results Report (SELECT link_text, link_target, detail1, detail2, last_modified)'
,p_internal_name=>'SEARCH_RESULTS_REPORT_SELECT_LINK_TEXT,_LINK_TARGET,_DETAIL1,_DETAIL2,_LAST_MODIFIED'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li>',
'<span class="title"><a href="#2#">#1#</a></span>',
'<span class="description"><span class="last_modified">#5#</span>#3#</span>',
'<span class="type">#4#</span>',
'</li>'))
,p_row_template_before_rows=>'<ul class="sSearchResultsReport">'
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'<table class="uPaginationTable">',
'#PAGINATION#',
'</table>'))
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="sPaginationNext">#PAGINATION_NEXT#</a>'
,p_previous_page_template=>'<a href="#LINK#" class="sPaginationPrev">#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="sPaginationNext">#PAGINATION_NEXT_SET#</a>'
,p_previous_set_template=>'<a href="#LINK#" class="sPaginationPrev">#PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>25
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4963119453052849)
,p_row_template_name=>'Standard'
,p_internal_name=>'STANDARD'
,p_row_template1=>'<td #ALIGNMENT# headers="#COLUMN_HEADER_NAME#">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="uReportContainer" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#" summary="">',
'<tbody class="uReportPagination">',
'#TOP_PAGINATION#',
'</tbody>',
'<tbody class="uReportBody">',
'<tr><td>',
'<table summary="#REGION_TITLE#" class="uReport uReportStandard">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</tbody>',
'</table>',
'</td>',
'</tr>',
'</tbody>',
'<tbody class="uReportPagination">',
'#PAGINATION#',
'</tbody>',
'</table>',
'<div class="uReportDownloadLinks">#EXTERNAL_LINK##CSV_LINK#</div>'))
,p_row_template_type=>'GENERIC_COLUMNS'
,p_before_column_heading=>'<thead>'
,p_column_heading_template=>'<th #ALIGNMENT# id="#COLUMN_HEADER_NAME#" #COLUMN_WIDTH#>#COLUMN_HEADER#</th>'
,p_after_column_heading=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</thead>',
'<tbody>'))
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_page_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT_SET# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_set_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_mouse_over=>'#F0F0F0'
,p_row_style_checked=>'#E8E8E8'
,p_theme_id=>25
,p_theme_class_id=>4
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(4963119453052849)
,p_row_template_before_first=>'<tr>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4963405885052850)
,p_row_template_name=>'Standard - Alternative'
,p_internal_name=>'STANDARD_ALTERNATIVE'
,p_row_template1=>'<td #ALIGNMENT# headers="#COLUMN_HEADER_NAME#">#COLUMN_VALUE#</td>'
,p_row_template3=>'<td #ALIGNMENT# headers="#COLUMN_HEADER_NAME#" class="uOddRow">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table class="uReportContainer" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#" summary="">',
'<tbody class="uReportPagination">',
'#TOP_PAGINATION#',
'</tbody>',
'<tbody class="uReportBody">',
'<tr><td>',
'<table summary="#REGION_TITLE#" class="uReport uReportAlternative">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</tbody>',
'</table>',
'</td>',
'</tr>',
'</tbody>',
'<tbody class="uReportPagination">',
'#PAGINATION#',
'</tbody>',
'</table>',
'<div class="uReportDownloadLinks">#EXTERNAL_LINK##CSV_LINK#</div>'))
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_before_column_heading=>'<thead>'
,p_column_heading_template=>'<th #ALIGNMENT# id="#COLUMN_HEADER_NAME#" #COLUMN_WIDTH#>#COLUMN_HEADER#</th>'
,p_after_column_heading=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</thead>',
'<tbody>'))
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'ODD_ROW_NUMBERS'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_page_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="uPaginationNext">#PAGINATION_NEXT_SET# <img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /></a>'
,p_previous_set_template=>'<a href="#LINK#" class="uPaginationPrev"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" /> #PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>25
,p_theme_class_id=>5
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(4963405885052850)
,p_row_template_before_first=>'<tr>'
,p_row_template_after_last=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</tr>',
''))
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4963710775052850)
,p_row_template_name=>'Two Column Portlet'
,p_internal_name=>'TWO_COLUMN_PORTLET'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li>',
'  <span class="uValueHeading">',
'    #1#',
'  </span>',
'  <span class="uValue">',
'    #2#',
'  </span>',
'</li>'))
,p_row_template_before_rows=>'<ul class="uValuePairs" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">'
,p_row_template_after_rows=>'</ul>'
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_theme_id=>25
,p_theme_class_id=>7
,p_translate_this_template=>'N'
);
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4964018068052850)
,p_row_template_name=>'Value Attribute Pairs'
,p_internal_name=>'VALUE_ATTRIBUTE_PAIRS'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li>',
'  <span class="uValueHeading">',
'    #COLUMN_HEADER#',
'  </span>',
'  <span class="uValue">',
'    #COLUMN_VALUE#',
'  </span>',
'</li>'))
,p_row_template_before_rows=>'<ul class="uValuePairs" #REPORT_ATTRIBUTES#>'
,p_row_template_after_rows=>'</ul>'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_theme_id=>25
,p_theme_class_id=>6
,p_translate_this_template=>'N'
);
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(4964324461052850)
,p_row_template_name=>'Value Attribute Pairs - Left Aligned'
,p_internal_name=>'VALUE_ATTRIBUTE_PAIRS_LEFT_ALIGNED'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li>',
'  <label>',
'    #COLUMN_HEADER#',
'  </label>',
'  <span>',
'    #COLUMN_VALUE#',
'  </span>',
'</li>'))
,p_row_template_before_rows=>'<ul class="vapList tableBased" #REPORT_ATTRIBUTES# id="report_#REPORT_STATIC_ID#">'
,p_row_template_after_rows=>'</ul>'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_theme_id=>25
,p_theme_class_id=>6
,p_translate_this_template=>'N'
,p_row_template_comment=>'shrahman 03/12/2012 Making table based '
);
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(24620516864658558)
,p_row_template_name=>'APEX 4.0 - Value Attribute Pairs'
,p_internal_name=>'APEX_4.0_VALUE_ATTRIBUTE_PAIRS'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="report-row">',
'  <div class="report-col-hdr">#COLUMN_HEADER#</div>',
'  <div class="report-col-val">#COLUMN_VALUE#</div>',
'</div>'))
,p_row_template_before_rows=>'<div class="two-col-report-portlet">'
,p_row_template_after_rows=>'</div>'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_theme_id=>20
,p_theme_class_id=>6
,p_translate_this_template=>'N'
);
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(24620604782658561)
,p_row_template_name=>'Borderless'
,p_internal_name=>'BORDERLESS'
,p_row_template1=>'<td headers="#COLUMN_HEADER_NAME#" #ALIGNMENT# class="t20data">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table class="t20Borderless t20Report" cellpadding="0" border="0" cellspacing="0" summary="">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>',
''))
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"#ALIGNMENT# id="#COLUMN_HEADER_NAME#">#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_checked=>'#CCCCCC'
,p_theme_id=>20
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(24620604782658561)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(24620710981658561)
,p_row_template_name=>'Horizontal Border'
,p_internal_name=>'HORIZONTAL_BORDER'
,p_row_template1=>'<td headers="#COLUMN_HEADER_NAME#" #ALIGNMENT# class="t20data">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table class="t20HorizontalBorder t20Report" border="0" cellpadding="0" cellspacing="0" summary="">'))
,p_row_template_after_rows=>'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"  id="#COLUMN_HEADER_NAME#" #ALIGNMENT#>#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_checked=>'#CCCCCC'
,p_theme_id=>20
,p_theme_class_id=>2
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(24620710981658561)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(24620799500658561)
,p_row_template_name=>'One Column Unordered List'
,p_internal_name=>'ONE_COLUMN_UNORDERED_LIST'
,p_row_template1=>'#COLUMN_VALUE#'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">',
'#TOP_PAGINATION#<tr><td><ul class="t20OneColumnUnorderedList">'))
,p_row_template_after_rows=>'</ul><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>20
,p_theme_class_id=>3
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(24620799500658561)
,p_row_template_before_first=>'<li>'
,p_row_template_after_last=>'</li>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(24620892458658561)
,p_row_template_name=>'Standard'
,p_internal_name=>'STANDARD'
,p_row_template1=>'<td #ALIGNMENT# headers="#COLUMN_HEADER#" class="t20data">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report t20Standard">'))
,p_row_template_after_rows=>'</table><div class="t20CVS">#CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"#ALIGNMENT# id="#COLUMN_HEADER_NAME#">#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>20
,p_theme_class_id=>4
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(24620892458658561)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(24620998410658561)
,p_row_template_name=>'Standard (PPR)'
,p_internal_name=>'STANDARD_PPR'
,p_row_template1=>'<td #ALIGNMENT# headers="#COLUMN_HEADER#" class="t20data">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="report#REGION_ID#"><htmldb:#REGION_ID#><table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table cellpadding="0" border="0" cellspacing="0" summary="" class="t20Standard t20Report">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table><script language=JavaScript type=text/javascript>',
'<!--',
'init_htmlPPRReport(''#REGION_ID#'');',
'',
'//-->',
'</script>',
'</htmldb:#REGION_ID#>',
'</div>'))
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"#ALIGNMENT# id="#COLUMN_HEADER_NAME#">#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="javascript:html_PPR_Report_Page(this,''#REGION_ID#'',''#LINK#'')" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="javascript:html_PPR_Report_Page(this,''#REGION_ID#'',''#LINK#'')" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="javascript:html_PPR_Report_Page(this,''#REGION_ID#'',''#LINK#'')" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="javascript:html_PPR_Report_Page(this,''#REGION_ID#'',''#LINK#'')" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_checked=>'#CCCCCC'
,p_theme_id=>20
,p_theme_class_id=>7
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(24620998410658561)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(24621112347658561)
,p_row_template_name=>'Standard, Alternating Row Colors'
,p_internal_name=>'STANDARD,_ALTERNATING_ROW_COLORS'
,p_row_template1=>'<td headers="#COLUMN_HEADER_NAME#" #ALIGNMENT# class="t20data">#COLUMN_VALUE#</td>'
,p_row_template2=>'<td headers="#COLUMN_HEADER_NAME#" #ALIGNMENT# class="t20dataalt">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table border="0" cellpadding="0" cellspacing="0" summary="" class="t20Report" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#',
'<tr><td><table border="0" cellpadding="0" cellspacing="0" summary="" class="t20StandardAlternatingRowColors t20Report">'))
,p_row_template_after_rows=>'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_column_heading_template=>'<th class="t20ReportHeader"#ALIGNMENT# id="#COLUMN_HEADER_NAME#">#COLUMN_HEADER#</th>'
,p_row_template_display_cond1=>'ODD_ROW_NUMBERS'
,p_row_template_display_cond2=>'NOT_CONDITIONAL'
,p_row_template_display_cond3=>'NOT_CONDITIONAL'
,p_row_template_display_cond4=>'ODD_ROW_NUMBERS'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_row_style_checked=>'#CCCCCC'
,p_theme_id=>20
,p_theme_class_id=>5
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(24621112347658561)
,p_row_template_before_first=>'<tr #HIGHLIGHT_ROW#>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(24621190772658562)
,p_row_template_name=>'Two Column Portlet'
,p_internal_name=>'TWO_COLUMN_PORTLET'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="report-row">',
'  <div class="report-col-hdr">#1#</div>',
'  <div class="report-col-val">#2#</div>',
'</div>'))
,p_row_template_before_rows=>'<div class="two-col-report-portlet" #REPORT_ATTRIBUTES# id="#REGION_ID#">'
,p_row_template_after_rows=>'</div>'
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_theme_id=>20
,p_theme_class_id=>7
,p_translate_this_template=>'N'
);
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(24621300246658562)
,p_row_template_name=>'Value Attribute Pairs'
,p_internal_name=>'VALUE_ATTRIBUTE_PAIRS'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<tr>',
'<th class="t20ReportHeader">#COLUMN_HEADER#</th>',
'<td class="t20data">#COLUMN_VALUE#</td>',
'</tr>'))
,p_row_template_before_rows=>'<table cellpadding="0" cellspacing="0" border="0" summary=""#REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#">#TOP_PAGINATION#<tr><td><table cellpadding="0" cellspacing="0" border="0" summary="" class="t20ValueAttributePairs">'
,p_row_template_after_rows=>'</table><div class="t20CVS">#EXTERNAL_LINK##CSV_LINK#</div></td></tr>#PAGINATION#</table>'
,p_row_template_table_attr=>'OMIT'
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_next_page_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT# &gt;</a>'
,p_previous_page_template=>'<a href="#LINK#" class="t20pagination">&lt;#PAGINATION_PREVIOUS#</a>'
,p_next_set_template=>'<a href="#LINK#" class="t20pagination">#PAGINATION_NEXT_SET#&gt;&gt;</a>'
,p_previous_set_template=>'<a href="#LINK#" class="t20pagination">&lt;&lt;#PAGINATION_PREVIOUS_SET#</a>'
,p_theme_id=>20
,p_theme_class_id=>6
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(24621300246658562)
,p_row_template_after_last=>'<tr><td colspan="2" class="t20seperate"><br /></td></tr>'
);
exception when others then null;
end;
end;
/
prompt --application/shared_components/user_interface/templates/label
begin
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(4968826426052858)
,p_template_name=>'Hidden Label (Read by Screen Readers)'
,p_internal_name=>'HIDDEN_LABEL_READ_BY_SCREEN_READERS'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="visuallyhidden">'
,p_template_body2=>'</label>'
,p_before_item=>'<div id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>'</div>'
,p_on_error_after_label=>'<span class="uLabelError">#ERROR_MESSAGE#</span>'
,p_theme_id=>25
,p_theme_class_id=>13
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(4968930112052859)
,p_template_name=>'Optional (Horizontal - Left Aligned)'
,p_internal_name=>'OPTIONAL_HORIZONTAL_LEFT_ALIGNED'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="uOptional">'
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</label>',
'<div class="fieldControls">'))
,p_before_item=>'<div class="fieldContainer horizontal" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<button class="uButton iconButton iconOnly altButton help itemHelpButton" onclick="uShowItemHelp(''#CURRENT_ITEM_NAME#'');return false;" id="hb_#CURRENT_ITEM_NAME#" type="button" title="#CURRENT_ITEM_HELP_LABEL#"><span><i></i></span></button>',
'<span class="uItemHelp" data-item-id="#CURRENT_ITEM_NAME#">#CURRENT_ITEM_HELP_TEXT#</span>',
'</div>',
'</div>'))
,p_on_error_after_label=>'<span class="uLabelError">#ERROR_MESSAGE#</span>'
,p_theme_id=>25
,p_theme_class_id=>3
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(4969021775052860)
,p_template_name=>'Optional (Horizontal - Right Aligned)'
,p_internal_name=>'OPTIONAL_HORIZONTAL_RIGHT_ALIGNED'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="uOptional">'
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</label>',
'<div class="fieldControls">'))
,p_before_item=>'<div class="fieldContainer horizontal rightlabels" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<button class="uButton iconButton iconOnly altButton help itemHelpButton" onclick="uShowItemHelp(''#CURRENT_ITEM_NAME#'');return false;" id="hb_#CURRENT_ITEM_NAME#" type="button"  title="#CURRENT_ITEM_HELP_LABEL#"><span><i></i></span></button>',
'<span class="uItemHelp" data-item-id="#CURRENT_ITEM_NAME#">#CURRENT_ITEM_HELP_TEXT#</span>',
'</div>',
'</div>'))
,p_on_error_after_label=>'<span class="uLabelError">#ERROR_MESSAGE#</span>'
,p_theme_id=>25
,p_theme_class_id=>3
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(4969134962052860)
,p_template_name=>'Optional (Label Above)'
,p_internal_name=>'OPTIONAL_LABEL_ABOVE'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="uOptional">'
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</label>',
'<button class="uButton iconButton iconOnly altButton help itemHelpButton" onclick="uShowItemHelp(''#CURRENT_ITEM_NAME#'');return false;" id="hb_#CURRENT_ITEM_NAME#" type="button" title="#CURRENT_ITEM_HELP_LABEL#"><span><i></i></span></button>',
'<span class="uItemHelp" data-item-id="#CURRENT_ITEM_NAME#">#CURRENT_ITEM_HELP_TEXT#</span>',
'<div class="fieldControls">'))
,p_before_item=>'<div class="fieldContainer vertical" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</div>',
'</div>'))
,p_on_error_after_label=>'<span class="uLabelError">#ERROR_MESSAGE#</span>'
,p_theme_id=>25
,p_theme_class_id=>3
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(4969230813052860)
,p_template_name=>'Required (Horizontal - Left Aligned)'
,p_internal_name=>'REQUIRED_HORIZONTAL_LEFT_ALIGNED'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="uRequired"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" class="uAsterisk" />'
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<span class="visuallyhidden">(#VALUE_REQUIRED#)</span></label>',
'<div class="fieldControls">'))
,p_before_item=>'<div class="fieldContainer horizontal" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<button class="uButton iconButton iconOnly altButton help itemHelpButton" onclick="uShowItemHelp(''#CURRENT_ITEM_NAME#'');return false;" id="hb_#CURRENT_ITEM_NAME#" type="button" title="#CURRENT_ITEM_HELP_LABEL#"><span><i></i></span></button>',
'<span class="uItemHelp" data-item-id="#CURRENT_ITEM_NAME#">#CURRENT_ITEM_HELP_TEXT#</span>',
'</div>',
'</div>'))
,p_on_error_after_label=>'<span class="uLabelError">#ERROR_MESSAGE#</span>'
,p_theme_id=>25
,p_theme_class_id=>4
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(4969326332052860)
,p_template_name=>'Required (Horizontal - Right Aligned)'
,p_internal_name=>'REQUIRED_HORIZONTAL_RIGHT_ALIGNED'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="uRequired"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" class="uAsterisk" />'
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
' <span class="visuallyhidden">(#VALUE_REQUIRED#)</span></label>',
'<div class="fieldControls">'))
,p_before_item=>'<div class="fieldContainer horizontal rightlabels" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<button class="uButton iconButton iconOnly altButton help itemHelpButton" onclick="uShowItemHelp(''#CURRENT_ITEM_NAME#'');return false;" id="hb_#CURRENT_ITEM_NAME#" type="button" title="#CURRENT_ITEM_HELP_LABEL#"><span><i></i></span></button>',
'<span class="uItemHelp" data-item-id="#CURRENT_ITEM_NAME#">#CURRENT_ITEM_HELP_TEXT#</span>',
'</div>',
'</div>'))
,p_on_error_after_label=>'<span class="uLabelError">#ERROR_MESSAGE#</span>'
,p_theme_id=>25
,p_theme_class_id=>4
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(4969412933052860)
,p_template_name=>'Required (Label Above)'
,p_internal_name=>'REQUIRED_LABEL_ABOVE'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="uRequired"><img src="#IMAGE_PREFIX#f_spacer.gif" alt="" class="uAsterisk" />'
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<span class="visuallyhidden">(#VALUE_REQUIRED#)</span></label>',
'<button class="uButton iconButton iconOnly altButton help itemHelpButton" onclick="uShowItemHelp(''#CURRENT_ITEM_NAME#'');return false;" id="hb_#CURRENT_ITEM_NAME#" type="button" title="#CURRENT_ITEM_HELP_LABEL#"><span><i></i></span></button>',
'<span class="uItemHelp" data-item-id="#CURRENT_ITEM_NAME#">#CURRENT_ITEM_HELP_TEXT#</span>',
'<div class="fieldControls">'))
,p_before_item=>'<div class="fieldContainer vertical" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</div>',
'</div>'))
,p_on_error_after_label=>'<span class="uLabelError">#ERROR_MESSAGE#</span>'
,p_theme_id=>25
,p_theme_class_id=>4
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(24621401683658562)
,p_template_name=>'No Label'
,p_internal_name=>'NO_LABEL'
,p_template_body1=>'<span class="t20NoLabel">'
,p_template_body2=>'</span>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>13
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(24621516577658564)
,p_template_name=>'Optional Label'
,p_internal_name=>'OPTIONAL_LABEL'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" tabindex="999"><span class="t20OptionalLabel">'
,p_template_body2=>'</span></label>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>3
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(24621595965658564)
,p_template_name=>'Optional Label with Help'
,p_internal_name=>'OPTIONAL_LABEL_WITH_HELP'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" tabindex="999"><a class="t20OptionalLabelwithHelp" href="javascript:popupFieldHelp(''#CURRENT_ITEM_ID#'',''&SESSION.'')" tabindex="999">'
,p_template_body2=>'</a></label>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(24621686709658564)
,p_template_name=>'Required Label'
,p_internal_name=>'REQUIRED_LABEL'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" tabindex="999"><span class="t20Req">*</span><span class="t20RequiredLabel">'
,p_template_body2=>'</span></label>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>4
,p_translate_this_template=>'N'
);
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(24621785751658564)
,p_template_name=>'Required Label with Help'
,p_internal_name=>'REQUIRED_LABEL_WITH_HELP'
,p_template_body1=>'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" tabindex="999"><span class="t20Req">*</span><a class="t20RequiredLabelwithHelp" href="javascript:popupFieldHelp(''#CURRENT_ITEM_ID#'',''&SESSION.'')" tabindex="999">'
,p_template_body2=>'</a></label>'
,p_on_error_before_label=>'<div class="t20InlineError">'
,p_on_error_after_label=>'<br/>#ERROR_MESSAGE#</div>'
,p_theme_id=>20
,p_theme_class_id=>2
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/breadcrumb
begin
wwv_flow_api.create_menu_template(
 p_id=>wwv_flow_api.id(4970708249052863)
,p_name=>'Breadcrumb Menu'
,p_internal_name=>'BREADCRUMB_MENU'
,p_before_first=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul>',
'<li class="uStartCap"><span></span></li>'))
,p_current_page_option=>'<li class="active"><span>#NAME#</span></li> '
,p_non_current_page_option=>'<li><a href="#LINK#">#NAME#</a></li> '
,p_between_levels=>'<li class="uSeparator"><span></span></li>'
,p_after_last=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="uEndCap"><span></span></li>',
'</ul>'))
,p_max_levels=>12
,p_start_with_node=>'PARENT_TO_LEAF'
,p_theme_id=>25
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
wwv_flow_api.create_menu_template(
 p_id=>wwv_flow_api.id(24621899126658564)
,p_name=>'Breadcrumb Menu'
,p_internal_name=>'BREADCRUMB_MENU'
,p_current_page_option=>'<a href="#LINK#" class="t20Current">#NAME#</a>'
,p_non_current_page_option=>'<a href="#LINK#">#NAME#</a>'
,p_between_levels=>'<b>&gt;</b>'
,p_max_levels=>12
,p_start_with_node=>'PARENT_TO_LEAF'
,p_theme_id=>20
,p_theme_class_id=>1
,p_translate_this_template=>'N'
);
wwv_flow_api.create_menu_template(
 p_id=>wwv_flow_api.id(24622014415658568)
,p_name=>'Hierarchical Menu'
,p_internal_name=>'HIERARCHICAL_MENU'
,p_before_first=>'<ul class="t20HierarchicalMenu">'
,p_current_page_option=>'<li class="t20current"><a href="#LINK#">#NAME#</a></li>'
,p_non_current_page_option=>'<li><a href="#LINK#">#NAME#</a></li>'
,p_after_last=>'</ul>'
,p_max_levels=>11
,p_start_with_node=>'CHILD_MENU'
,p_theme_id=>20
,p_theme_class_id=>2
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/popuplov
begin
wwv_flow_api.create_popup_lov_template(
 p_id=>wwv_flow_api.id(4970918631052874)
,p_popup_icon=>'#IMAGE_PREFIX#f_spacer.gif'
,p_popup_icon_attr=>'alt="#LIST_OF_VALUES#" title="#LIST_OF_VALUES#" class="uPopupLOVIcon"'
,p_page_name=>'winlov'
,p_page_title=>'Search Dialog'
,p_page_html_head=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE.">',
'<head>',
'<title>#TITLE#</title>',
'#APEX_CSS#',
'#THEME_CSS#',
'#THEME_STYLE_CSS#',
'#APEX_JAVASCRIPT#',
'<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />',
'<link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon">',
'<link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_25/css/4_2.css" type="text/css" media="all"/>',
'',
'</head>'))
,p_page_body_attr=>'class="uPopUpLOV"'
,p_before_field_text=>'<div class="uActionBar">'
,p_filter_width=>'20'
,p_filter_max_width=>'100'
,p_filter_text_attr=>'class="searchField"'
,p_find_button_text=>'Search'
,p_find_button_attr=>'class="lovButton hotButton"'
,p_close_button_text=>'Close'
,p_close_button_attr=>'class="lovButton"'
,p_next_button_text=>'Next >'
,p_next_button_attr=>'class="lovButton"'
,p_prev_button_text=>'< Previous'
,p_prev_button_attr=>'class="lovButton"'
,p_after_field_text=>'</div>'
,p_scrollbars=>'1'
,p_resizable=>'1'
,p_width=>'400'
,p_height=>'450'
,p_result_row_x_of_y=>'<div class="lovPagination">Row(s) #FIRST_ROW# - #LAST_ROW#</div>'
,p_result_rows_per_pg=>500
,p_before_result_set=>'<div class="lovLinks">'
,p_theme_id=>25
,p_theme_class_id=>1
,p_translate_this_template=>'N'
,p_after_result_set=>'</div>'
);
wwv_flow_api.create_popup_lov_template(
 p_id=>wwv_flow_api.id(24622688399658572)
,p_popup_icon=>'#IMAGE_PREFIX#lov_16x16.gif'
,p_popup_icon_attr=>'width="16" height="16" alt="Popup Lov"'
,p_page_name=>'winlov'
,p_page_title=>'Search Dialog'
,p_page_html_head=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<html lang="&BROWSER_LANGUAGE.">',
'<head>',
'<title>#TITLE#</title>',
'#APEX_CSS#',
'#THEME_CSS#',
'#THEME_STYLE_CSS#',
'#APEX_JAVASCRIPT#',
'<link rel="shortcut icon" href="#IMAGE_PREFIX#favicon.ico" type="image/x-icon"><link rel="stylesheet" href="#IMAGE_PREFIX#themes/theme_20/theme_3_1.css" type="text/css">',
'',
'',
'</head>'))
,p_page_body_attr=>'onload="first_field()" style="background-color:#FFFFFF;margin:0;"'
,p_before_field_text=>'<div class="t20PopupHead">'
,p_filter_width=>'20'
,p_filter_max_width=>'100'
,p_find_button_text=>'Search'
,p_close_button_text=>'Close'
,p_next_button_text=>'Next >'
,p_prev_button_text=>'< Previous'
,p_after_field_text=>'</div>'
,p_scrollbars=>'1'
,p_resizable=>'1'
,p_width=>'400'
,p_height=>'450'
,p_result_row_x_of_y=>'<br /><div style="padding:2px; font-size:8pt;">Row(s) #FIRST_ROW# - #LAST_ROW#</div>'
,p_result_rows_per_pg=>500
,p_before_result_set=>'<div class="t20PopupBody">'
,p_theme_id=>20
,p_theme_class_id=>1
,p_translate_this_template=>'N'
,p_after_result_set=>'</div>'
);
end;
/
prompt --application/shared_components/user_interface/templates/calendar
begin
wwv_flow_api.create_calendar_template(
 p_id=>wwv_flow_api.id(4970827676052869)
,p_cal_template_name=>'Calendar'
,p_internal_name=>'CALENDAR'
,p_day_of_week_format=>'<th id="#DY#" scope="col" class="uCalDayCol">#IDAY#</th>'
,p_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="uCal">',
'<h1 class="uMonth">#IMONTH# <span>#YYYY#</span></h1>'))
,p_month_open_format=>'<table class="uCal" cellpadding="0" cellspacing="0" border="0" summary="#IMONTH# #YYYY#">'
,p_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
'<div class="uCalFooter"></div>',
'</div>',
''))
,p_day_title_format=>'<span class="uDayTitle">#DD#</span>'
,p_day_open_format=>'<td class="uDay" headers="#DY#">#TITLE_FORMAT#<div class="uDayData">#DATA#</div>'
,p_day_close_format=>'</td>'
,p_today_open_format=>'<td class="uDay today" headers="#DY#">#TITLE_FORMAT#<div class="uDayData">#DATA#</div>'
,p_weekend_title_format=>'<span class="uDayTitle weekendday">#DD#</span>'
,p_weekend_open_format=>'<td class="uDay" headers="#DY#">#TITLE_FORMAT#<div class="uDayData">#DATA#</div>'
,p_weekend_close_format=>'</td>'
,p_nonday_title_format=>'<span class="uDayTitle">#DD#</span>'
,p_nonday_open_format=>'<td class="uDay nonday" headers="#DY#">'
,p_nonday_close_format=>'</td>'
,p_week_open_format=>'<tr>'
,p_week_close_format=>'</tr> '
,p_daily_title_format=>'<table cellspacing="0" cellpadding="0" border="0" summary="" class="t1DayCalendarHolder"> <tr> <td class="t1MonthTitle">#IMONTH# #DD#, #YYYY#</td> </tr> <tr> <td>'
,p_daily_open_format=>'<tr>'
,p_daily_close_format=>'</tr>'
,p_weekly_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="uCal uCalWeekly">',
'<h1 class="uMonth">#WTITLE#</h1>'))
,p_weekly_day_of_week_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<th scope="col" class="aCalDayCol" id="#DY#">',
'  <span class="visible-desktop">#DD# #IDAY#</span>',
'  <span class="hidden-desktop">#DD# <em>#IDY#</em></span>',
'</th>'))
,p_weekly_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="#CALENDAR_TITLE# #START_DL# - #END_DL#" class="uCal">'
,p_weekly_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
'<div class="uCalFooter"></div>',
'</div>'))
,p_weekly_day_open_format=>'<td class="uDay" headers="#DY#"><div class="uDayData">'
,p_weekly_day_close_format=>'</div></td>'
,p_weekly_today_open_format=>'<td class="uDay today" headers="#DY#"><div class="uDayData">'
,p_weekly_weekend_open_format=>'<td class="uDay weekend" headers="#DY#"><div class="uDayData">'
,p_weekly_weekend_close_format=>'</div></td>'
,p_weekly_time_open_format=>'<th scope="row" class="uCalHour">'
,p_weekly_time_close_format=>'</th>'
,p_weekly_time_title_format=>'#TIME#'
,p_weekly_hour_open_format=>'<tr>'
,p_weekly_hour_close_format=>'</tr>'
,p_daily_day_of_week_format=>'<th scope="col" id="#DY#" class="aCalDayCol">#IDAY#</th>'
,p_daily_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="uCal uCalWeekly uCalDaily">',
'<h1 class="uMonth">#IMONTH# #DD#, #YYYY#</h1>'))
,p_daily_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="#CALENDAR_TITLE# #START_DL#" class="uCal">'
,p_daily_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
'<div class="uCalFooter"></div>',
'</div>'))
,p_daily_day_open_format=>'<td class="uDay" headers="#DY#"><div class="uDayData">'
,p_daily_day_close_format=>'</div></td>'
,p_daily_today_open_format=>'<td class="uDay today" headers="#DY#"><div class="uDayData">'
,p_daily_time_open_format=>'<th scope="row" class="uCalHour" id="#TIME#">'
,p_daily_time_close_format=>'</th>'
,p_daily_time_title_format=>'#TIME#'
,p_daily_hour_open_format=>'<tr>'
,p_daily_hour_close_format=>'</tr>'
,p_agenda_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul class="listCalendar">',
'  <li class="monthHeader">',
'    <h1>#IMONTH# #YYYY#</h1>',
'  </li>',
'  #DAYS#',
'  <li class="listEndCap"></li>',
'</ul>'))
,p_agenda_past_day_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="dayHeader past">',
'    <h2>#IDAY# <span>#IMONTH# #DD#</span></h2>',
'  </li>'))
,p_agenda_today_day_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="dayHeader today">',
'    <h2>#IDAY# <span>#IMONTH# #DD#</span></h2>',
'  </li>'))
,p_agenda_future_day_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="dayHeader future">',
'    <h2>#IDAY# <span>#IMONTH# #DD#</span></h2>',
'  </li>'))
,p_agenda_past_entry_format=>'  <li class="dayData past">#DATA#</li>'
,p_agenda_today_entry_format=>'  <li class="dayData today">#DATA#</li>'
,p_agenda_future_entry_format=>'  <li class="dayData future">#DATA#</li>'
,p_month_data_format=>'#DAYS#'
,p_month_data_entry_format=>'#DATA#'
,p_theme_id=>25
,p_theme_class_id=>1
);
wwv_flow_api.create_calendar_template(
 p_id=>wwv_flow_api.id(24622108620658568)
,p_cal_template_name=>'Calendar'
,p_internal_name=>'CALENDAR'
,p_day_of_week_format=>'<th class="t20DayOfWeek">#IDAY#</th>'
,p_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellspacing="0" cellpadding="0" border="0" summary="" class="t20CalendarAlternative1Holder"> ',
' <tr>',
'   <td class="t20MonthTitle">#IMONTH# #YYYY#</td>',
' </tr>',
' <tr>',
' <td class="t20MonthBody">'))
,p_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="0" class="t20CalendarAlternative1">'
,p_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table></td>',
'</tr>',
'</table>',
''))
,p_day_title_format=>'<div class="t20DayTitle">#DD#</div><br />'
,p_day_open_format=>'<td class="t20Day" valign="top">#TITLE_FORMAT##DATA#'
,p_day_close_format=>'</td>'
,p_today_open_format=>'<td valign="top" class="t20Today">#TITLE_FORMAT##DATA#'
,p_weekend_title_format=>'<div class="t20WeekendDayTitle">#DD#</div><br />'
,p_weekend_open_format=>'<td valign="top" class="t20WeekendDay">#TITLE_FORMAT##DATA#'
,p_weekend_close_format=>'</td>'
,p_nonday_title_format=>'<div class="t20NonDayTitle">#DD#</div><br />'
,p_nonday_open_format=>'<td class="t20NonDay" valign="top">'
,p_nonday_close_format=>'</td>'
,p_week_open_format=>'<tr>'
,p_week_close_format=>'</tr> '
,p_daily_title_format=>'<th width="14%" class="calheader">#IDAY#</th>'
,p_daily_open_format=>'<tr>'
,p_daily_close_format=>'</tr>'
,p_month_data_format=>'#DAYS#'
,p_month_data_entry_format=>'#DATA#'
,p_theme_id=>20
,p_theme_class_id=>1
);
wwv_flow_api.create_calendar_template(
 p_id=>wwv_flow_api.id(24622289986658570)
,p_cal_template_name=>'Calendar, Alternative 1'
,p_internal_name=>'CALENDAR,_ALTERNATIVE_1'
,p_day_of_week_format=>'<th class="t20DayOfWeek">#IDAY#</th>'
,p_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellspacing="0" cellpadding="0" border="0" summary="" class="t20CalendarHolder"> ',
' <tr>',
'   <td class="t20MonthTitle">#IMONTH# #YYYY#</td>',
' </tr>',
' <tr>',
' <td class="t20MonthBody">'))
,p_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="0" class="t20Calendar">'
,p_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table></td>',
'</tr>',
'</table>',
''))
,p_day_title_format=>'<div class="t20DayTitle">#DD#</div><br />'
,p_day_open_format=>'<td class="t20Day" valign="top">#TITLE_FORMAT##DATA#'
,p_day_close_format=>'</td>'
,p_today_open_format=>'<td valign="top" class="t20Today">#TITLE_FORMAT##DATA#'
,p_weekend_title_format=>'<div class="t20WeekendDayTitle">#DD#</div><br />'
,p_weekend_open_format=>'<td valign="top" class="t20WeekendDay">#TITLE_FORMAT##DATA#'
,p_weekend_close_format=>'</td>'
,p_nonday_title_format=>'<div class="t20NonDayTitle">#DD#</div><br />'
,p_nonday_open_format=>'<td class="t20NonDay" valign="top">'
,p_nonday_close_format=>'</td>'
,p_week_open_format=>'<tr>'
,p_week_close_format=>'</tr> '
,p_daily_title_format=>'<th width="14%" class="calheader">#IDAY#</th>'
,p_daily_open_format=>'<tr>'
,p_daily_close_format=>'</tr>'
,p_month_data_format=>'#DAYS#'
,p_month_data_entry_format=>'#DATA#'
,p_theme_id=>20
,p_theme_class_id=>2
);
wwv_flow_api.create_calendar_template(
 p_id=>wwv_flow_api.id(24622500781658571)
,p_cal_template_name=>'Small Calender'
,p_internal_name=>'SMALL_CALENDER'
,p_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<table cellspacing="0" cellpadding="0" border="0" summary="" class="t20SmallCalenderHolder"> ',
' <tr>',
'   <td class="t20MonthTitle">#IMONTH# #YYYY#</td>',
' </tr>',
' <tr>',
' <td class="t20MonthBody">'))
,p_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="0" class="t20SmallCalender">'
,p_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table></td>',
'</tr>',
'</table>',
''))
,p_day_title_format=>'<div class="t20DayTitle">#DD#</div>'
,p_day_open_format=>'<td class="t20Day" valign="top">#TITLE_FORMAT##DATA#'
,p_day_close_format=>'</td>'
,p_today_open_format=>'<td valign="top" class="t20Today">#TITLE_FORMAT##DATA#'
,p_weekend_title_format=>'<div class="t20WeekendDayTitle">#DD#</div>'
,p_weekend_open_format=>'<td valign="top" class="t20WeekendDay">#TITLE_FORMAT##DATA#'
,p_weekend_close_format=>'</td>'
,p_nonday_title_format=>'<div class="t20NonDayTitle">#DD#</div>'
,p_nonday_open_format=>'<td class="t20NonDay" valign="top">'
,p_nonday_close_format=>'</td>'
,p_week_open_format=>'<tr>'
,p_week_close_format=>'</tr> '
,p_daily_title_format=>'<th width="14%" class="calheader">#IDAY#</th>'
,p_daily_open_format=>'<tr>'
,p_daily_close_format=>'</tr>'
,p_month_data_format=>'#DAYS#'
,p_month_data_entry_format=>'#DATA#'
,p_theme_id=>20
,p_theme_class_id=>3
);
end;
/
prompt --application/shared_components/user_interface/themes
begin
wwv_flow_api.create_theme(
 p_id=>wwv_flow_api.id(4971117702052883)
,p_theme_id=>25
,p_theme_name=>'Blue Responsive'
,p_theme_internal_name=>'BLUE_RESPONSIVE'
,p_ui_type_name=>'DESKTOP'
,p_navigation_type=>'T'
,p_nav_bar_type=>'NAVBAR'
,p_is_locked=>false
,p_default_page_template=>wwv_flow_api.id(4930418112052812)
,p_error_template=>wwv_flow_api.id(4916122028052799)
,p_printer_friendly_template=>wwv_flow_api.id(4940031438052818)
,p_breadcrumb_display_point=>'REGION_POSITION_01'
,p_sidebar_display_point=>'REGION_POSITION_02'
,p_login_template=>wwv_flow_api.id(4910524365052781)
,p_default_button_template=>wwv_flow_api.id(4969527198052860)
,p_default_region_template=>wwv_flow_api.id(4959633248052842)
,p_default_chart_template=>wwv_flow_api.id(4959633248052842)
,p_default_form_template=>wwv_flow_api.id(4956708135052837)
,p_default_reportr_template=>wwv_flow_api.id(4959633248052842)
,p_default_tabform_template=>wwv_flow_api.id(4959633248052842)
,p_default_wizard_template=>wwv_flow_api.id(4956708135052837)
,p_default_menur_template=>wwv_flow_api.id(4955212930052835)
,p_default_listr_template=>wwv_flow_api.id(4959633248052842)
,p_default_irr_template=>wwv_flow_api.id(4958007764052841)
,p_default_report_template=>wwv_flow_api.id(4963119453052849)
,p_default_label_template=>wwv_flow_api.id(4969021775052860)
,p_default_menu_template=>wwv_flow_api.id(4970708249052863)
,p_default_calendar_template=>wwv_flow_api.id(4970827676052869)
,p_default_list_template=>wwv_flow_api.id(4967908050052857)
,p_default_option_label=>wwv_flow_api.id(4969021775052860)
,p_default_required_label=>wwv_flow_api.id(4969326332052860)
,p_default_page_transition=>'NONE'
,p_default_popup_transition=>'NONE'
,p_file_prefix => nvl(wwv_flow_application_install.get_static_theme_file_prefix(25),'')
,p_css_file_urls=>'#IMAGE_PREFIX#legacy_ui/css/5.0#MIN#.css?v=#APEX_VERSION#'
);
wwv_flow_api.create_theme(
 p_id=>wwv_flow_api.id(24622798200658574)
,p_theme_id=>20
,p_theme_name=>'Traditional Blue'
,p_theme_internal_name=>'TRADITIONAL_BLUE'
,p_ui_type_name=>'DESKTOP'
,p_navigation_type=>'T'
,p_nav_bar_type=>'NAVBAR'
,p_is_locked=>false
,p_default_page_template=>wwv_flow_api.id(24614914776658539)
,p_error_template=>wwv_flow_api.id(24614914776658539)
,p_printer_friendly_template=>wwv_flow_api.id(24615606755658543)
,p_breadcrumb_display_point=>'REGION_POSITION_01'
,p_login_template=>wwv_flow_api.id(24614592041658531)
,p_default_button_template=>wwv_flow_api.id(24616088703658545)
,p_default_region_template=>wwv_flow_api.id(24617998492658552)
,p_default_chart_template=>wwv_flow_api.id(24616999608658552)
,p_default_form_template=>wwv_flow_api.id(24617090878658552)
,p_default_reportr_template=>wwv_flow_api.id(24617998492658552)
,p_default_tabform_template=>wwv_flow_api.id(24617998492658552)
,p_default_wizard_template=>wwv_flow_api.id(24618609768658553)
,p_default_menur_template=>wwv_flow_api.id(24616716460658552)
,p_default_listr_template=>wwv_flow_api.id(24617998492658552)
,p_default_report_template=>wwv_flow_api.id(24620892458658561)
,p_default_label_template=>wwv_flow_api.id(24621595965658564)
,p_default_menu_template=>wwv_flow_api.id(24621899126658564)
,p_default_calendar_template=>wwv_flow_api.id(24622108620658568)
,p_default_list_template=>wwv_flow_api.id(24620302058658558)
,p_default_option_label=>wwv_flow_api.id(24621595965658564)
,p_default_required_label=>wwv_flow_api.id(24621785751658564)
,p_default_page_transition=>'NONE'
,p_default_popup_transition=>'NONE'
,p_file_prefix => nvl(wwv_flow_application_install.get_static_theme_file_prefix(20),'')
,p_css_file_urls=>'#IMAGE_PREFIX#legacy_ui/css/5.0#MIN#.css?v=#APEX_VERSION#'
);
end;
/
prompt --application/shared_components/user_interface/theme_style
begin
null;
end;
/
prompt --application/shared_components/user_interface/theme_files
begin
null;
end;
/
prompt --application/shared_components/user_interface/theme_display_points
begin
null;
end;
/
prompt --application/shared_components/user_interface/template_opt_groups
begin
null;
end;
/
prompt --application/shared_components/user_interface/template_options
begin
null;
end;
/
prompt --application/shared_components/logic/build_options
begin
null;
end;
/
prompt --application/shared_components/globalization/language
begin
null;
end;
/
prompt --application/shared_components/globalization/translations
begin
null;
end;
/
prompt --application/shared_components/globalization/messages
begin
null;
end;
/
prompt --application/shared_components/globalization/dyntranslations
begin
null;
end;
/
prompt --application/shared_components/user_interface/shortcuts
begin
wwv_flow_api.create_shortcut(
 p_id=>wwv_flow_api.id(24632208568788725)
,p_shortcut_name=>'DELETE_CONFIRM_MSG'
,p_shortcut_type=>'TEXT_ESCAPE_JS'
,p_shortcut=>'Would you like to perform this delete action?'
);
wwv_flow_api.create_shortcut(
 p_id=>wwv_flow_api.id(24649898249865257)
,p_shortcut_name=>'OK_TO_GET_NEXT_PREV_PK_VALUE'
,p_shortcut_type=>'TEXT_ESCAPE_JS'
,p_shortcut=>'Are you sure you want to leave this page without saving?'
);
end;
/
prompt --application/shared_components/security/authentications
begin
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(2464428764204209)
,p_name=>'none'
,p_scheme_type=>'NATIVE_DAD'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(2762406842056297)
,p_name=>'HDS_AD_AUTH'
,p_scheme_type=>'NATIVE_LDAP'
,p_attribute_01=>'appauth.hdsupply.net'
,p_attribute_02=>'389'
,p_attribute_03=>'NO_SSL'
,p_attribute_04=>'HSI\%LDAP_USER%'
,p_attribute_05=>'Y'
,p_attribute_08=>'STD'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(3209026811232302)
,p_name=>'HDSOID'
,p_scheme_type=>'NATIVE_IAS_SSO'
,p_attribute_02=>'http://eoid.hsi.hughessupply.com:8000/pls/orasso/orasso.wwsso_app_admin.ls_logout'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(3829224062608793)
,p_name=>'SSO'
,p_scheme_type=>'NATIVE_IAS_SSO'
,p_attribute_02=>'http://ofid.hsi.hughessupply.com:8000/pls/orasso/orasso.wwsso_app_admin.ls_logout'
,p_attribute_15=>'2314626582364378'
,p_logout_url=>'http://apps.hdsupply.net/pls/apex/f?p=&APP_ID.'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
,p_comments=>'Based on authentication scheme from gallery:Oracle Application Server Single Sign-On (Application Express as Partner Application)'
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(10953461189425287)
,p_name=>'Entrust GetAccess - Web SSO'
,p_scheme_type=>'NATIVE_HTTP_HEADER_VARIABLE'
,p_attribute_01=>'USER'
,p_attribute_02=>'BUILTIN_URL'
,p_attribute_06=>'CALLBACK'
,p_logout_url=>'https://access.hdsupply.com/GetAccess/Logout?GAURI=http://devebiz.hdsupply.net/pls/ebizdev/f?p=119'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
);
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(24622897202658595)
,p_name=>'Application Express'
,p_scheme_type=>'NATIVE_APEX_ACCOUNTS'
,p_attribute_15=>'15101648994780327'
,p_invalid_session_type=>'LOGIN'
,p_logout_url=>'f?p=&APP_ID.:1'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
,p_comments=>'Use internal Application Express account credentials and login page in this application.'
);
end;
/
prompt --application/ui_types
begin
null;
end;
/
prompt --application/shared_components/plugins/item_type/item_type_plugin_com_enkitec_sigpad
begin
wwv_flow_api.create_plugin(
 p_id=>wwv_flow_api.id(66369261679382330)
,p_plugin_type=>'ITEM TYPE'
,p_name=>'ITEM_TYPE_PLUGIN_COM_ENKITEC_SIGPAD'
,p_display_name=>'SignaturePad'
,p_supported_ui_types=>'DESKTOP'
,p_supported_component_types=>'APEX_APPLICATION_PAGE_ITEMS'
,p_image_prefix => nvl(wwv_flow_application_install.get_static_plugin_file_prefix('ITEM TYPE','ITEM_TYPE_PLUGIN_COM_ENKITEC_SIGPAD'),'')
,p_plsql_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'--================================================================================',
'--  Render Function',
'--================================================================================',
'function render_signature_pad (',
'    p_item                in apex_plugin.t_page_item,',
'    p_plugin              in apex_plugin.t_plugin,',
'    p_value               in varchar2,',
'    p_is_readonly         in boolean,',
'    p_is_printer_friendly in boolean )',
'    return apex_plugin.t_page_item_render_result',
'IS',
'   l_retval            APEX_PLUGIN.T_PAGE_ITEM_RENDER_RESULT;',
'   l_onload_code       varchar2(32767);',
'   l_pk_name           varchar2(32767) := p_item.attribute_01;',
'   l_height            varchar2(32767) := p_item.attribute_02;',
'   l_width             varchar2(32767) := p_item.attribute_03;',
'   l_canvas_color      varchar2(32767) := p_item.attribute_04;',
'   l_pen_color         varchar2(32767) := p_item.attribute_05;',
'   l_pen_width         varchar2(32767) := p_item.attribute_06;',
'   l_line_color        varchar2(32767) := p_item.attribute_07;',
'   l_line_width        varchar2(32767) := p_item.attribute_08;',
'   l_line_margin       varchar2(32767) := p_item.attribute_09;',
'   l_line_top          varchar2(32767) := p_item.attribute_10;',
'',
'-- Its easer to reference things using a local name rather than the APEX Attribute Number',
'BEGIN',
'--',
'-- If we''re in debug mode, then turn on debugging for the region.',
'--',
'  IF apex_application.g_debug ',
'    THEN',
'      apex_plugin_util.debug_page_item ( p_plugin => p_plugin, ',
'                                         p_page_item => p_item );',
'  END IF;',
'--',
'-- Add the signaturepad JavaScript',
'--',
'   apex_javascript.add_library (',
'      p_name      => ''jquery.signaturepad.min'',',
'      p_directory => p_plugin.file_prefix,',
'      p_version   => NULL',
'   );',
'--',
'-- Add the signaturepad CSS',
'--',
'   apex_css.add_file(',
'      p_name      => ''jquery.signaturepad'',',
'      p_directory => p_plugin.file_prefix,',
'      p_version   => NULL',
'   );',
'--',
'-- Add the json2.js to',
'--',
'   apex_javascript.add_library (',
'      p_name      => ''json2.min'',',
'      p_directory => p_plugin.file_prefix,',
'      p_version   => NULL',
'   );',
'--',
'-- Add the flashcanvas.js to',
'--',
'--   apex_javascript.add_library(',
'--      p_name      => ''flashcanvas'',',
'--      p_directory => p_plugin.file_prefix,',
'--      p_version   => NULL',
'--   );',
'--',
'-- Emit the Base Javascript for the SignaturePad DIV and the underlying HIDDEN filed that will hold the Signature JSON object',
'--',
'htp.p(q''!',
'<div class="sig sigWrapper" style="display: inherit">',
' <canvas class="pad" width="!''||l_width ||q''!" height="!''||l_height ||q''!"></canvas>',
'</div>',
'<input type="hidden" id="!''||p_item.name ||q''!" name="!''||apex_plugin.get_input_name_for_page_item (p_is_multi_value => FALSE) ||q''!" value="!''||p_value ||q''!">',
'!'');',
'--',
'-- Emit the javascript functions necessary to ',
'--',
'htp.p(q''!',
'<script type="text/javascript">',
'apex.ajax.clob = function (pReturn) {',
'    var that = this;',
'    this.ajax = new htmldb_Get(null, $x(''pFlowId'').value, ''APXWGT'', $x(''pFlowStepId'').value);',
'    this.ajax.addParam(''p_widget_name'', ''apex_utility'');',
'    this.ajax.addParam(''x04'', ''CLOB_CONTENT'');',
'    this._get = _get;',
'    this._set = _set;',
'    this._return = !! pReturn ? pReturn : _return;',
'    return;',
'',
'    function _get(pValue) {',
'       that.ajax.addParam(''x05'', ''GET'');',
'       that.ajax.GetAsync(that._return);',
'    }',
'',
'    function _set(pValue) {',
'       that.ajax.addParam(''x05'', ''SET'');',
'       that.ajax.AddArrayClob(pValue, 1);',
'       that.ajax.GetAsync(that._return);',
'    }',
'',
'    function _return() {',
'       if (p.readyState == 1) {} else if (p.readyState == 2) {} else if (p.readyState == 3) {} else if (p.readyState == 4) {',
'          return p;',
'       } else {',
'          return false;',
'       }',
'    }',
' }',
'',
'function clob_get_from_db(){',
'// Call the on demand procedure to Get the signature clob',
'//  alert("clob_get_from_db");',
'  var get = new htmldb_Get(null,$v(''pFlowId''),''PLUGIN=!''|| apex_plugin.get_ajax_identifier ||q''!'',$x(''pFlowStepId'').value);',
'  get.addParam(''x01'', $v(''!''||l_pk_name||q''!''));',
'  var gReturn= get.get();',
'  $x(''!''||p_item.name ||q''!'').value = gReturn;',
'  get=null;',
'}',
'//',
'function clob_set(clobString){  ',
' var clob_ob = new apex.ajax.clob(  ',
'  function(){  ',
'   var rs = p.readyState  ',
'   if(rs == 1||rs == 2||rs == 3){  ',
'    $x_Show(''AjaxLoading'');  ',
'   }else if(rs == 4){  ',
'    $x_Hide(''AjaxLoading'');  ',
'   }else{return false;}  ',
'  }  ',
' );  ',
' // ',
' clob_ob._set(clobString);  ',
'}  ',
'</script>',
'!'');',
'-- ',
'-- Build the ONLOAD javascript including the OnDrawEnd callback ',
'--',
'l_onload_code := q''!',
'  $(document).ready(function(){',
'    clob_get_from_db(); // ',
'    if($v_IsEmpty(''!''||p_item.name||q''!'')){',
'      $("#wwvFlowForm").signaturePad({drawOnly:       true,',
'                                      validateFields: false, ',
'                                      output:         ''#!''||p_item.name||q''!'', ',
'                                      onDrawEnd:       function(){ var api = $("#wwvFlowForm").signaturePad(); ',
'                                                                   var sig64 = api.getSignatureImage(); ',
'                                                                   clob_set(sig64);',
'                                                                 },',
'                                      bgColour:        ''!''||l_canvas_color||q''!'',',
'                                      penColour:       ''!''||l_pen_color||q''!'',',
'                                      penWidth:        ''!''||l_pen_width||q''!'',',
'                                      lineColour:      ''!''||l_line_color||q''!'',',
'                                      lineWidth:       ''!''||l_line_width||q''!'',',
'                                      lineMargin:      ''!''||l_line_margin||q''!'',',
'                                      lineTop:         ''!''||l_line_top||q''!''',
'                                    });',
'    }',
'    else{',
'      $("#wwvFlowForm").signaturePad({displayOnly:true ,lineColour:''#FFFFFF'', lineTop:100}).regenerate($v(''!''||p_item.name||q''!''));',
'    }',
'  });',
'!'';',
'--',
'apex_javascript.add_onload_code(',
'      p_code => l_onload_code',
'   );',
'--',
'return l_retval;',
'END;',
'-- =================================================================',
'-- sigpad_ajax_callback',
'-- =================================================================',
'function sigpad_ajax_callback (',
'    p_item   in apex_plugin.t_page_item,',
'    p_plugin in apex_plugin.t_plugin )',
'    return apex_plugin.t_page_item_ajax_result',
'   ',
'IS',
'  l_return     apex_plugin.t_page_item_ajax_result;',
'  l_SQL        varchar2(32767);',
'  c_sig_cursor SYS_REFCURSOR;',
'  l_code clob := empty_clob;',
'  l_DB_COLUMN_NAME varchar2(30);',
'  l_DB_TABLE_NAME  varchar2(30);',
'  l_DB_PK_COLUMN_NAME varchar2(30);',
'  l_error_msg varchar2(32767);',
'  ',
'BEGIN',
'IF apex_application.g_x01 is not null THEN',
'      --',
'      -- Get the name of the DB Column and table that are related to this item (if any)',
'      --',
'          BEGIN',
'          --',
'          -- This gets the Column and Table to be used in the dynamic cursor.',
'          --',
'          l_error_msg:= ''ERROR: unable to successfully retrieve Column Values'';',
'          select db_column_name, db_table_name',
'            into l_db_column_name, l_db_table_name ',
'            from apex_application_page_db_items',
'           where item_name = p_item.name',
'            and  application_id = v(''APP_ID'');',
'          --',
'          -- This gets the underlying DB Column for the Primary Key as identified in the attributes.',
'          --',
'          l_error_msg:= ''ERROR: unable to successfully retrieve PK COLUMN Values'';',
'          select db_column_name',
'            into l_db_pk_column_name ',
'            from apex_application_page_db_items',
'           where item_name = p_item.attribute_01',
'            and  application_id = v(''APP_ID'');',
'          --',
'          EXCEPTION WHEN NO_DATA_FOUND THEN ',
'              -- When the item isn''t a database item, then don''t output anything',
'              htp.prn(l_error_msg);',
'              RETURN L_RETURN;',
'          END;',
'      --',
'      -- Create the select statement that will be used to return the JSON',
'      --',
'        l_sql := ''select ''||l_db_column_name||'' from ''||l_db_table_name||'' where ''||l_db_pk_column_name||'' =  ''||apex_application.g_x01;',
'      --',
'      -- Execute the select statement',
'      --',
'        OPEN c_sig_cursor FOR l_sql;  ',
'        FETCH c_sig_cursor INTO l_code;',
'        IF c_sig_cursor%FOUND THEN',
'          DECLARE',
'            l_clob_source2 clob;',
'            offset         int:=1;',
'          BEGIN',
'            LOOP',
'              l_clob_source2 := dbms_lob.substr(l_code,4000,offset);',
'              htp.prn(l_clob_source2);',
'',
'            EXIT WHEN',
'              offset + 4000 >= nvl(dbms_lob.getlength (l_code),0);',
'              offset := offset + 4000;',
'            END LOOP;',
'          END;',
'        ELSE',
'          htp.prn(''ERROR: Unable to retrieve clob from dynamic SQL'');',
'        END IF;',
'        CLOSE c_sig_cursor;',
' END IF;',
'return l_return;',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    -- Log error here',
'    RAISE;',
'END; '))
,p_api_version=>1
,p_render_function=>'render_signature_pad'
,p_ajax_function=>'sigpad_ajax_callback'
,p_standard_attributes=>'VISIBLE:SOURCE:ELEMENT:WIDTH:HEIGHT:ELEMENT_OPTION'
,p_substitute_attributes=>true
,p_subscribe_plugin_settings=>true
,p_version_identifier=>'1.0'
,p_about_url=>'http://www.enkitec.com/products/plugins/signaturepad'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46600152374092551)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>1
,p_display_sequence=>10
,p_prompt=>'Primary Key Column Name'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_display_length=>30
,p_max_length=>30
,p_is_translatable=>false
,p_help_text=>'Enter the name of the Page Item that holds the Primary Key for the table to which the signature will be saved.'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46601339532211882)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>2
,p_display_sequence=>20
,p_prompt=>'Signature Pad Height (px)'
,p_attribute_type=>'INTEGER'
,p_is_required=>true
,p_default_value=>'70'
,p_display_length=>5
,p_max_length=>5
,p_is_translatable=>false
,p_help_text=>'Height (in pixels) of the signature pad canvas.'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46601949229214685)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>3
,p_display_sequence=>30
,p_prompt=>'Signature Pad Width (px)'
,p_attribute_type=>'INTEGER'
,p_is_required=>true
,p_default_value=>'400'
,p_display_length=>5
,p_max_length=>5
,p_is_translatable=>false
,p_help_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Width (in pixels) of the signature pad canvas.',
''))
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46603146907346448)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>4
,p_display_sequence=>40
,p_prompt=>'Canvas Color'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_default_value=>'#FFFFFF'
,p_display_length=>30
,p_max_length=>30
,p_is_translatable=>false
,p_help_text=>'Background Color for the canvas. Use Hex values such as ''#FFFFFF'' or color names such as ''white''. You may also use the word ''transparent'' for a transparent background.'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46603757643349507)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>5
,p_display_sequence=>50
,p_prompt=>'Pen Color'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_default_value=>'#145394'
,p_display_length=>10
,p_max_length=>10
,p_is_translatable=>false
,p_help_text=>'Color for the ink of the SignaturePad''s pen. Use Hex colors such as ''#145394'' or color names such as ''blue''.'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46604340458354049)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>6
,p_display_sequence=>60
,p_prompt=>'Pen Width (px)'
,p_attribute_type=>'INTEGER'
,p_is_required=>true
,p_default_value=>'2'
,p_is_translatable=>false
,p_help_text=>'Thickness (in pixels) of the line drawn by the "pen cursor". '
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46606461499379085)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>7
,p_display_sequence=>70
,p_prompt=>'Signature Line Color'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_default_value=>'#ccc'
,p_display_length=>6
,p_max_length=>6
,p_is_translatable=>false
,p_help_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Color of the line which represent where the user is to sign their name. Setting the color to the same as the canvas color will get rid of the line.',
'',
'NOTE: The signature line will not be captured as part of the image if you are using the SignaturePad IMG Saver Process Plugin.'))
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46607047778384509)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>8
,p_display_sequence=>80
,p_prompt=>'Signature Line Width (px)'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_default_value=>'2'
,p_display_length=>5
,p_max_length=>5
,p_is_translatable=>false
,p_help_text=>'Thickness (in pixels) of the signature line.'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46607657128387284)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>9
,p_display_sequence=>90
,p_prompt=>'Signature Line Margin (px)'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_default_value=>'2'
,p_display_length=>5
,p_max_length=>5
,p_is_translatable=>false
,p_help_text=>'The margin (in pixels) on the left and the right of signature line.'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(46608237519391057)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>10
,p_display_sequence=>100
,p_prompt=>'Signature Line Top (px)'
,p_attribute_type=>'INTEGER'
,p_is_required=>true
,p_default_value=>'35'
,p_display_length=>5
,p_max_length=>5
,p_is_translatable=>false
,p_help_text=>'Distance (in pixels) to draw the signature line from the top of the canvas.'
);
end;
/
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
wwv_flow_api.g_varchar2_table(1) := '40666F6E742D66616365207B0A2020666F6E742D66616D696C793A202754696D6573204E657720526F6D616E272C47656F726769612C53657269663B3B0A2020666F6E742D7765696768743A206E6F726D616C3B0A2020666F6E742D7374796C653A206E';
wwv_flow_api.g_varchar2_table(2) := '6F726D616C3B0A7D0A0A2E736967506164207B0A20206D617267696E3A20303B0A202070616464696E673A20303B0A202077696474683A2032303070783B0A7D0A0A2E736967506164206C6162656C207B0A2020646973706C61793A20626C6F636B3B0A';
wwv_flow_api.g_varchar2_table(3) := '20206D617267696E3A2030203020302E353135656D3B0A202070616464696E673A20303B0A0A2020636F6C6F723A20233030303B0A2020666F6E743A206974616C6963206E6F726D616C2031656D2F312E3337352047656F726769612C54696D65732C73';
wwv_flow_api.g_varchar2_table(4) := '657269663B0A7D0A0A2E736967506164206C6162656C2E6572726F72207B0A2020636F6C6F723A20236633333B0A7D0A0A2E73696750616420696E707574207B0A20206D617267696E3A20303B0A202070616464696E673A20302E32656D20303B0A2020';
wwv_flow_api.g_varchar2_table(5) := '77696474683A2031393870783B0A0A2020626F726465723A2031707820736F6C696420233636363B0A0A2020666F6E742D73697A653A2031656D3B0A7D0A0A2E73696750616420696E7075742E6572726F72207B0A2020626F726465722D636F6C6F723A';
wwv_flow_api.g_varchar2_table(6) := '20236633333B0A7D0A0A2E73696750616420627574746F6E207B0A20206D617267696E3A2031656D2030203020303B0A202070616464696E673A20302E36656D20302E36656D20302E37656D3B0A0A20206261636B67726F756E642D636F6C6F723A2023';
wwv_flow_api.g_varchar2_table(7) := '6363633B0A2020626F726465723A20303B0A20202D6D6F7A2D626F726465722D7261646975733A203870783B0A20202D7765626B69742D626F726465722D7261646975733A203870783B0A2020626F726465722D7261646975733A203870783B0A0A2020';
wwv_flow_api.g_varchar2_table(8) := '637572736F723A20706F696E7465723B0A0A2020636F6C6F723A20233535353B0A2020666F6E743A20626F6C642031656D2F312E3337352073616E732D73657269663B0A2020746578742D616C69676E3A206C6566743B0A7D0A0A2E7369675061642062';
wwv_flow_api.g_varchar2_table(9) := '7574746F6E3A686F766572207B0A20206261636B67726F756E642D636F6C6F723A20233333333B0A0A2020636F6C6F723A20236666663B0A7D0A0A2E736967207B0A2020646973706C61793A206E6F6E653B0A7D0A0A2E7369674E6176207B0A20206469';
wwv_flow_api.g_varchar2_table(10) := '73706C61793A206E6F6E653B0A20206865696768743A20322E3235656D3B0A20206D617267696E3A20303B0A202070616464696E673A20303B0A2020706F736974696F6E3A2072656C61746976653B0A0A20206C6973742D7374796C652D747970653A20';
wwv_flow_api.g_varchar2_table(11) := '6E6F6E653B0A7D0A0A2E7369674E6176206C69207B0A2020646973706C61793A20696E6C696E653B0A2020666C6F61743A206C6566743B0A20206D617267696E3A20303B0A202070616464696E673A20303B0A7D0A0A2E7369674E617620612C0A2E7369';
wwv_flow_api.g_varchar2_table(12) := '674E617620613A6C696E6B2C0A2E7369674E617620613A76697369746564207B0A2020646973706C61793A20626C6F636B3B0A20206D617267696E3A20303B0A202070616464696E673A203020302E36656D3B0A0A2020626F726465723A20303B0A0A20';
wwv_flow_api.g_varchar2_table(13) := '20636F6C6F723A20233333333B0A2020666F6E742D7765696768743A20626F6C643B0A20206C696E652D6865696768743A20322E3235656D3B0A2020746578742D6465636F726174696F6E3A20756E6465726C696E653B0A7D0A0A2E7369674E61762061';
wwv_flow_api.g_varchar2_table(14) := '2E63757272656E742C0A2E7369674E617620612E63757272656E743A6C696E6B2C0A2E7369674E617620612E63757272656E743A76697369746564207B0A20206261636B67726F756E642D636F6C6F723A20233636363B0A20202D6D6F7A2D626F726465';
wwv_flow_api.g_varchar2_table(15) := '722D7261646975732D746F706C6566743A203870783B0A20202D6D6F7A2D626F726465722D7261646975732D746F7072696768743A203870783B0A20202D7765626B69742D626F726465722D746F702D6C6566742D7261646975733A203870783B0A2020';
wwv_flow_api.g_varchar2_table(16) := '2D7765626B69742D626F726465722D746F702D72696768742D7261646975733A203870783B0A2020626F726465722D7261646975733A2038707820387078203020303B0A0A2020636F6C6F723A20236666663B0A2020746578742D6465636F726174696F';
wwv_flow_api.g_varchar2_table(17) := '6E3A206E6F6E653B0A7D0A0A2E7369674E6176202E74797065497420612E63757272656E742C0A2E7369674E6176202E74797065497420612E63757272656E743A6C696E6B2C0A2E7369674E6176202E74797065497420612E63757272656E743A766973';
wwv_flow_api.g_varchar2_table(18) := '69746564207B0A20206261636B67726F756E642D636F6C6F723A20236363633B0A0A2020636F6C6F723A20233535353B0A7D0A0A2E7369674E6176202E636C656172427574746F6E207B0A2020626F74746F6D3A20302E32656D3B0A2020646973706C61';
wwv_flow_api.g_varchar2_table(19) := '793A206E6F6E653B0A2020706F736974696F6E3A206162736F6C7574653B0A202072696768743A20303B0A0A2020666F6E742D73697A653A20302E3735656D3B0A20206C696E652D6865696768743A20312E3337353B0A7D0A0A2E736967577261707065';
wwv_flow_api.g_varchar2_table(20) := '72207B0A2020636C6561723A20626F74683B0A20206865696768743A20353570783B0A0A2020626F726465723A2031707820736F6C696420236363633B0A7D0A0A2E736967577261707065722E63757272656E74207B0A2020626F726465722D636F6C6F';
wwv_flow_api.g_varchar2_table(21) := '723A20233636363B0A7D0A0A2E7369676E6564202E73696757726170706572207B0A2020626F726465723A20303B0A7D0A0A2E706164207B0A2020706F736974696F6E3A2072656C61746976653B0A0A20202F2A2A0A2020202A20466F722063726F7373';
wwv_flow_api.g_varchar2_table(22) := '2062726F7773657220636F6D7061746962696C6974792C20746869732073686F756C6420626520616E206162736F6C7574652055524C0A2020202A20496E2049452074686520637572736F722069732072656C617469766520746F207468652048544D4C';
wwv_flow_api.g_varchar2_table(23) := '20646F63756D656E740A2020202A20496E20616C6C206F746865722062726F77736572732074686520637572736F722069732072656C617469766520746F20746865204353532066696C650A2020202A0A2020202A20687474703A2F2F7777772E757365';
wwv_flow_api.g_varchar2_table(24) := '726167656E746D616E2E636F6D2F626C6F672F323031312F31322F32312F63726F73732D62726F777365722D6373732D637572736F722D696D616765732D696E2D64657074682F0A2020202A2F0A2020637572736F723A2063726F7373686169723B0A20';
wwv_flow_api.g_varchar2_table(25) := '202F2A2A0A2020202A2049452077696C6C2069676E6F72652074686973206C696E652062656361757365206F662074686520686F7473706F7420706F736974696F6E0A2020202A20556E666F7274756E6174656C79207765206E65656420746869732074';
wwv_flow_api.g_varchar2_table(26) := '776963652C206265636175736520736F6D652062726F77736572732069676E6F72652074686520686F7473706F7420696E7369646520746865202E6375720A2020202A2F0A2020637572736F723A2063726F7373686169723B0A0A20202D6D732D746F75';
wwv_flow_api.g_varchar2_table(27) := '63682D616374696F6E3A206E6F6E653B0A20202D7765626B69742D757365722D73656C6563743A206E6F6E653B0A20202D6D6F7A2D757365722D73656C6563743A206E6F6E653B0A20202D6D732D757365722D73656C6563743A206E6F6E653B0A20202D';
wwv_flow_api.g_varchar2_table(28) := '6F2D757365722D73656C6563743A206E6F6E653B0A2020757365722D73656C6563743A206E6F6E653B0A7D0A0A2E7479706564207B0A20206865696768743A20353570783B0A20206D617267696E3A20303B0A202070616464696E673A2030203570783B';
wwv_flow_api.g_varchar2_table(29) := '0A2020706F736974696F6E3A206162736F6C7574653B0A20207A2D696E6465783A2039303B0A0A2020637572736F723A2064656661756C743B0A0A2020636F6C6F723A20233134353339343B0A2020666F6E743A206E6F726D616C20312E383735656D2F';
wwv_flow_api.g_varchar2_table(30) := '3530707820224A6F75726E616C222C47656F726769612C54696D65732C73657269663B0A7D0A0A2E747970654974446573632C0A2E64726177497444657363207B0A2020646973706C61793A206E6F6E653B0A20206D617267696E3A20302E3735656D20';
wwv_flow_api.g_varchar2_table(31) := '3020302E353135656D3B0A202070616464696E673A20302E353135656D203020303B0A0A2020626F726465722D746F703A2033707820736F6C696420236363633B0A0A2020636F6C6F723A20233030303B0A2020666F6E743A206974616C6963206E6F72';
wwv_flow_api.g_varchar2_table(32) := '6D616C2031656D2F312E3337352047656F726769612C54696D65732C73657269663B0A7D0A0A702E6572726F72207B0A2020646973706C61793A20626C6F636B3B0A20206D617267696E3A20302E35656D20303B0A202070616464696E673A20302E3465';
wwv_flow_api.g_varchar2_table(33) := '6D3B0A0A20206261636B67726F756E642D636F6C6F723A20236633333B0A0A2020636F6C6F723A20236666663B0A2020666F6E742D7765696768743A20626F6C643B0A7D0A';
null;
end;
/
begin
wwv_flow_api.create_plugin_file(
 p_id=>wwv_flow_api.id(46617239969937111)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_file_name=>'jquery.signaturepad.css'
,p_mime_type=>'text/css'
,p_file_content=>wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
);
end;
/
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
wwv_flow_api.g_varchar2_table(1) := '2F2A2A210A202A205369676E61747572655061643A2041206A517565727920706C7567696E20666F7220617373697374696E6720696E20746865206372656174696F6E206F6620616E2048544D4C352063616E766173206261736564207369676E617475';
wwv_flow_api.g_varchar2_table(2) := '7265207061642E205265636F7264732074686520647261776E207369676E617475726520696E204A534F4E20666F72206C6174657220726567656E65726174696F6E2E0A202A204070726F6A656374207369676E61747572652D7061640A202A20406175';
wwv_flow_api.g_varchar2_table(3) := '74686F722054686F6D6173204A20427261646C6579203C6865794074686F6D61736A627261646C65792E63613E0A202A20406C696E6B20687474703A2F2F74686F6D61736A627261646C65792E63612F6C61622F7369676E61747572652D7061640A202A';
wwv_flow_api.g_varchar2_table(4) := '20406C696E6B2068747470733A2F2F6769746875622E636F6D2F74686F6D61736A627261646C65792F7369676E61747572652D7061640A202A2040636F7079726967687420323031332054686F6D6173204A20427261646C65790A202A20406C6963656E';
wwv_flow_api.g_varchar2_table(5) := '7365204253442D332D434C415553450A202A204076657273696F6E20322E352E300A202A2F0A2166756E6374696F6E2824297B66756E6374696F6E205369676E61747572655061642873656C6563746F722C6F7074696F6E73297B66756E6374696F6E20';
wwv_flow_api.g_varchar2_table(6) := '636C6561724D6F7573654C6561766554696D656F757428297B636C65617254696D656F7574286D6F7573654C6561766554696D656F7574292C6D6F7573654C6561766554696D656F75743D21312C6D6F757365427574746F6E446F776E3D21317D66756E';
wwv_flow_api.g_varchar2_table(7) := '6374696F6E20647261774C696E6528652C6E6577594F6666736574297B766172206F66667365742C6E6577582C6E6577593B72657475726E20652E70726576656E7444656661756C7428292C6F66667365743D2428652E746172676574292E6F66667365';
wwv_flow_api.g_varchar2_table(8) := '7428292C636C65617254696D656F7574286D6F7573654C6561766554696D656F7574292C6D6F7573654C6561766554696D656F75743D21312C22756E646566696E656422213D747970656F6620652E746172676574546F75636865733F286E6577583D4D';
wwv_flow_api.g_varchar2_table(9) := '6174682E666C6F6F7228652E746172676574546F75636865735B305D2E70616765582D6F66667365742E6C656674292C6E6577593D4D6174682E666C6F6F7228652E746172676574546F75636865735B305D2E70616765592D6F66667365742E746F7029';
wwv_flow_api.g_varchar2_table(10) := '293A286E6577583D4D6174682E666C6F6F7228652E70616765582D6F66667365742E6C656674292C6E6577593D4D6174682E666C6F6F7228652E70616765592D6F66667365742E746F7029292C70726576696F75732E783D3D3D6E657758262670726576';
wwv_flow_api.g_varchar2_table(11) := '696F75732E793D3D3D6E6577593F21303A286E756C6C3D3D3D70726576696F75732E7826262870726576696F75732E783D6E657758292C6E756C6C3D3D3D70726576696F75732E7926262870726576696F75732E793D6E657759292C6E6577594F666673';
wwv_flow_api.g_varchar2_table(12) := '65742626286E6577592B3D6E6577594F6666736574292C63616E766173436F6E746578742E626567696E5061746828292C63616E766173436F6E746578742E6D6F7665546F2870726576696F75732E782C70726576696F75732E79292C63616E76617343';
wwv_flow_api.g_varchar2_table(13) := '6F6E746578742E6C696E65546F286E6577582C6E657759292C63616E766173436F6E746578742E6C696E654361703D73657474696E67732E70656E4361702C63616E766173436F6E746578742E7374726F6B6528292C63616E766173436F6E746578742E';
wwv_flow_api.g_varchar2_table(14) := '636C6F73655061746828292C6F75747075742E70757368287B6C783A6E6577582C6C793A6E6577592C6D783A70726576696F75732E782C6D793A70726576696F75732E797D292C70726576696F75732E783D6E6577582C70726576696F75732E793D6E65';
wwv_flow_api.g_varchar2_table(15) := '77592C73657474696E67732E6F6E4472617726262266756E6374696F6E223D3D747970656F662073657474696E67732E6F6E44726177262673657474696E67732E6F6E447261772E6170706C792873656C66292C766F69642030297D66756E6374696F6E';
wwv_flow_api.g_varchar2_table(16) := '2073746F7044726177696E675772617070657228297B73746F7044726177696E6728297D66756E6374696F6E2073746F7044726177696E672865297B653F647261774C696E6528652C31293A28746F75636861626C653F63616E7661732E656163682866';
wwv_flow_api.g_varchar2_table(17) := '756E6374696F6E28297B746869732E72656D6F76654576656E744C697374656E65722822746F7563686D6F7665222C647261774C696E65297D293A63616E7661732E756E62696E6428226D6F7573656D6F76652E7369676E617475726570616422292C6F';
wwv_flow_api.g_varchar2_table(18) := '75747075742E6C656E6774683E30262673657474696E67732E6F6E44726177456E6426262266756E6374696F6E223D3D747970656F662073657474696E67732E6F6E44726177456E64262673657474696E67732E6F6E44726177456E642E6170706C7928';
wwv_flow_api.g_varchar2_table(19) := '73656C6629292C70726576696F75732E783D6E756C6C2C70726576696F75732E793D6E756C6C2C73657474696E67732E6F757470757426266F75747075742E6C656E6774683E302626242873657474696E67732E6F75747075742C636F6E74657874292E';
wwv_flow_api.g_varchar2_table(20) := '76616C284A534F4E2E737472696E67696679286F757470757429297D66756E6374696F6E20647261775369674C696E6528297B72657475726E2073657474696E67732E6C696E6557696474683F2863616E766173436F6E746578742E626567696E506174';
wwv_flow_api.g_varchar2_table(21) := '6828292C63616E766173436F6E746578742E6C696E6557696474683D73657474696E67732E6C696E6557696474682C63616E766173436F6E746578742E7374726F6B655374796C653D73657474696E67732E6C696E65436F6C6F75722C63616E76617343';
wwv_flow_api.g_varchar2_table(22) := '6F6E746578742E6D6F7665546F2873657474696E67732E6C696E654D617267696E2C73657474696E67732E6C696E65546F70292C63616E766173436F6E746578742E6C696E65546F28656C656D656E742E77696474682D73657474696E67732E6C696E65';
wwv_flow_api.g_varchar2_table(23) := '4D617267696E2C73657474696E67732E6C696E65546F70292C63616E766173436F6E746578742E7374726F6B6528292C63616E766173436F6E746578742E636C6F73655061746828292C766F69642030293A21317D66756E6374696F6E20636C65617243';
wwv_flow_api.g_varchar2_table(24) := '616E76617328297B63616E766173436F6E746578742E636C6561725265637428302C302C656C656D656E742E77696474682C656C656D656E742E686569676874292C63616E766173436F6E746578742E66696C6C5374796C653D73657474696E67732E62';
wwv_flow_api.g_varchar2_table(25) := '67436F6C6F75722C63616E766173436F6E746578742E66696C6C5265637428302C302C656C656D656E742E77696474682C656C656D656E742E686569676874292C73657474696E67732E646973706C61794F6E6C797C7C647261775369674C696E652829';
wwv_flow_api.g_varchar2_table(26) := '2C63616E766173436F6E746578742E6C696E6557696474683D73657474696E67732E70656E57696474682C63616E766173436F6E746578742E7374726F6B655374796C653D73657474696E67732E70656E436F6C6F75722C242873657474696E67732E6F';
wwv_flow_api.g_varchar2_table(27) := '75747075742C636F6E74657874292E76616C282222292C6F75747075743D5B5D2C73746F7044726177696E6728297D66756E6374696F6E206F6E4D6F7573654D6F766528652C6F297B6E756C6C3D3D70726576696F75732E783F647261774C696E652865';
wwv_flow_api.g_varchar2_table(28) := '2C31293A647261774C696E6528652C6F297D66756E6374696F6E20737461727444726177696E6728652C746F7563684F626A656374297B746F75636861626C653F746F7563684F626A6563742E6164644576656E744C697374656E65722822746F756368';
wwv_flow_api.g_varchar2_table(29) := '6D6F7665222C6F6E4D6F7573654D6F76652C2131293A63616E7661732E62696E6428226D6F7573656D6F76652E7369676E6174757265706164222C6F6E4D6F7573654D6F7665292C647261774C696E6528652C31297D66756E6374696F6E206469736162';
wwv_flow_api.g_varchar2_table(30) := '6C6543616E76617328297B6576656E7473426F756E643D21312C63616E7661732E656163682866756E6374696F6E28297B746869732E72656D6F76654576656E744C697374656E6572262628746869732E72656D6F76654576656E744C697374656E6572';
wwv_flow_api.g_varchar2_table(31) := '2822746F756368656E64222C73746F7044726177696E6757726170706572292C746869732E72656D6F76654576656E744C697374656E65722822746F75636863616E63656C222C73746F7044726177696E6757726170706572292C746869732E72656D6F';
wwv_flow_api.g_varchar2_table(32) := '76654576656E744C697374656E65722822746F7563686D6F7665222C647261774C696E6529292C746869732E6F6E746F7563687374617274262628746869732E6F6E746F75636873746172743D6E756C6C297D292C2428646F63756D656E74292E756E62';
wwv_flow_api.g_varchar2_table(33) := '696E6428226D6F75736575702E7369676E617475726570616422292C63616E7661732E756E62696E6428226D6F757365646F776E2E7369676E617475726570616422292C63616E7661732E756E62696E6428226D6F7573656D6F76652E7369676E617475';
wwv_flow_api.g_varchar2_table(34) := '726570616422292C63616E7661732E756E62696E6428226D6F7573656C656176652E7369676E617475726570616422292C242873657474696E67732E636C6561722C636F6E74657874292E756E62696E642822636C69636B2E7369676E61747572657061';
wwv_flow_api.g_varchar2_table(35) := '6422297D66756E6374696F6E20696E6974447261774576656E74732865297B72657475726E206576656E7473426F756E643F21313A286576656E7473426F756E643D21302C242822696E70757422292E626C757228292C22756E646566696E656422213D';
wwv_flow_api.g_varchar2_table(36) := '747970656F6620652E746172676574546F7563686573262628746F75636861626C653D2130292C746F75636861626C653F2863616E7661732E656163682866756E6374696F6E28297B746869732E6164644576656E744C697374656E65722822746F7563';
wwv_flow_api.g_varchar2_table(37) := '68656E64222C73746F7044726177696E67577261707065722C2131292C746869732E6164644576656E744C697374656E65722822746F75636863616E63656C222C73746F7044726177696E67577261707065722C2131297D292C63616E7661732E756E62';
wwv_flow_api.g_varchar2_table(38) := '696E6428226D6F757365646F776E2E7369676E61747572657061642229293A282428646F63756D656E74292E62696E6428226D6F75736575702E7369676E6174757265706164222C66756E6374696F6E28297B6D6F757365427574746F6E446F776E2626';
wwv_flow_api.g_varchar2_table(39) := '2873746F7044726177696E6728292C636C6561724D6F7573654C6561766554696D656F75742829297D292C63616E7661732E62696E6428226D6F7573656C656176652E7369676E6174757265706164222C66756E6374696F6E2865297B6D6F7573654275';
wwv_flow_api.g_varchar2_table(40) := '74746F6E446F776E262673746F7044726177696E672865292C6D6F757365427574746F6E446F776E2626216D6F7573654C6561766554696D656F75742626286D6F7573654C6561766554696D656F75743D73657454696D656F75742866756E6374696F6E';
wwv_flow_api.g_varchar2_table(41) := '28297B73746F7044726177696E6728292C636C6561724D6F7573654C6561766554696D656F757428297D2C35303029297D292C63616E7661732E656163682866756E6374696F6E28297B746869732E6F6E746F75636873746172743D6E756C6C7D29292C';
wwv_flow_api.g_varchar2_table(42) := '766F69642030297D66756E6374696F6E2064726177497428297B242873657474696E67732E74797065642C636F6E74657874292E6869646528292C636C65617243616E76617328292C63616E7661732E656163682866756E6374696F6E28297B74686973';
wwv_flow_api.g_varchar2_table(43) := '2E6F6E746F75636873746172743D66756E6374696F6E2865297B652E70726576656E7444656661756C7428292C6D6F757365427574746F6E446F776E3D21302C696E6974447261774576656E74732865292C737461727444726177696E6728652C746869';
wwv_flow_api.g_varchar2_table(44) := '73297D7D292C63616E7661732E62696E6428226D6F757365646F776E2E7369676E6174757265706164222C66756E6374696F6E2865297B72657475726E20652E70726576656E7444656661756C7428292C652E77686963683E313F21313A286D6F757365';
wwv_flow_api.g_varchar2_table(45) := '427574746F6E446F776E3D21302C696E6974447261774576656E74732865292C737461727444726177696E672865292C766F69642030297D292C242873657474696E67732E636C6561722C636F6E74657874292E62696E642822636C69636B2E7369676E';
wwv_flow_api.g_varchar2_table(46) := '6174757265706164222C66756E6374696F6E2865297B652E70726576656E7444656661756C7428292C636C65617243616E76617328297D292C242873657474696E67732E7479706549742C636F6E74657874292E62696E642822636C69636B2E7369676E';
wwv_flow_api.g_varchar2_table(47) := '6174757265706164222C66756E6374696F6E2865297B652E70726576656E7444656661756C7428292C74797065497428297D292C242873657474696E67732E6472617749742C636F6E74657874292E756E62696E642822636C69636B2E7369676E617475';
wwv_flow_api.g_varchar2_table(48) := '726570616422292C242873657474696E67732E6472617749742C636F6E74657874292E62696E642822636C69636B2E7369676E6174757265706164222C66756E6374696F6E2865297B652E70726576656E7444656661756C7428297D292C242873657474';
wwv_flow_api.g_varchar2_table(49) := '696E67732E7479706549742C636F6E74657874292E72656D6F7665436C6173732873657474696E67732E63757272656E74436C617373292C242873657474696E67732E6472617749742C636F6E74657874292E616464436C6173732873657474696E6773';
wwv_flow_api.g_varchar2_table(50) := '2E63757272656E74436C617373292C242873657474696E67732E7369672C636F6E74657874292E616464436C6173732873657474696E67732E63757272656E74436C617373292C242873657474696E67732E747970654974446573632C636F6E74657874';
wwv_flow_api.g_varchar2_table(51) := '292E6869646528292C242873657474696E67732E647261774974446573632C636F6E74657874292E73686F7728292C242873657474696E67732E636C6561722C636F6E74657874292E73686F7728297D66756E6374696F6E2074797065497428297B636C';
wwv_flow_api.g_varchar2_table(52) := '65617243616E76617328292C64697361626C6543616E76617328292C242873657474696E67732E74797065642C636F6E74657874292E73686F7728292C242873657474696E67732E6472617749742C636F6E74657874292E62696E642822636C69636B2E';
wwv_flow_api.g_varchar2_table(53) := '7369676E6174757265706164222C66756E6374696F6E2865297B652E70726576656E7444656661756C7428292C64726177497428297D292C242873657474696E67732E7479706549742C636F6E74657874292E756E62696E642822636C69636B2E736967';
wwv_flow_api.g_varchar2_table(54) := '6E617475726570616422292C242873657474696E67732E7479706549742C636F6E74657874292E62696E642822636C69636B2E7369676E6174757265706164222C66756E6374696F6E2865297B652E70726576656E7444656661756C7428297D292C2428';
wwv_flow_api.g_varchar2_table(55) := '73657474696E67732E6F75747075742C636F6E74657874292E76616C282222292C242873657474696E67732E6472617749742C636F6E74657874292E72656D6F7665436C6173732873657474696E67732E63757272656E74436C617373292C2428736574';
wwv_flow_api.g_varchar2_table(56) := '74696E67732E7479706549742C636F6E74657874292E616464436C6173732873657474696E67732E63757272656E74436C617373292C242873657474696E67732E7369672C636F6E74657874292E72656D6F7665436C6173732873657474696E67732E63';
wwv_flow_api.g_varchar2_table(57) := '757272656E74436C617373292C242873657474696E67732E647261774974446573632C636F6E74657874292E6869646528292C242873657474696E67732E636C6561722C636F6E74657874292E6869646528292C242873657474696E67732E7479706549';
wwv_flow_api.g_varchar2_table(58) := '74446573632C636F6E74657874292E73686F7728292C74797065497443757272656E74466F6E7453697A653D74797065497444656661756C74466F6E7453697A653D242873657474696E67732E74797065642C636F6E74657874292E6373732822666F6E';
wwv_flow_api.g_varchar2_table(59) := '742D73697A6522292E7265706C616365282F70782F2C2222297D66756E6374696F6E20747970652876616C297B7661722074797065643D242873657474696E67732E74797065642C636F6E74657874292C636C65616E656456616C3D76616C2E7265706C';
wwv_flow_api.g_varchar2_table(60) := '616365282F3E2F672C222667743B22292E7265706C616365282F3C2F672C22266C743B22292E7472696D28292C6F6C644C656E6774683D7479706549744E756D43686172732C656467654F66667365743D2E352A74797065497443757272656E74466F6E';
wwv_flow_api.g_varchar2_table(61) := '7453697A653B6966287479706549744E756D43686172733D636C65616E656456616C2E6C656E6774682C74797065642E68746D6C28636C65616E656456616C292C21636C65616E656456616C2972657475726E2074797065642E6373732822666F6E742D';
wwv_flow_api.g_varchar2_table(62) := '73697A65222C74797065497444656661756C74466F6E7453697A652B22707822292C766F696420303B6966287479706549744E756D43686172733E6F6C644C656E677468262674797065642E6F75746572576964746828293E656C656D656E742E776964';
wwv_flow_api.g_varchar2_table(63) := '746829666F72283B74797065642E6F75746572576964746828293E656C656D656E742E77696474683B2974797065497443757272656E74466F6E7453697A652D2D2C74797065642E6373732822666F6E742D73697A65222C74797065497443757272656E';
wwv_flow_api.g_varchar2_table(64) := '74466F6E7453697A652B22707822293B6966286F6C644C656E6774683E7479706549744E756D4368617273262674797065642E6F75746572576964746828292B656467654F66667365743C656C656D656E742E7769647468262674797065497444656661';
wwv_flow_api.g_varchar2_table(65) := '756C74466F6E7453697A653E74797065497443757272656E74466F6E7453697A6529666F72283B74797065642E6F75746572576964746828292B656467654F66667365743C656C656D656E742E7769647468262674797065497444656661756C74466F6E';
wwv_flow_api.g_varchar2_table(66) := '7453697A653E74797065497443757272656E74466F6E7453697A653B2974797065497443757272656E74466F6E7453697A652B2B2C74797065642E6373732822666F6E742D73697A65222C74797065497443757272656E74466F6E7453697A652B227078';
wwv_flow_api.g_varchar2_table(67) := '22297D66756E6374696F6E206F6E4265666F726556616C696461746528636F6E746578742C73657474696E6773297B242822702E222B73657474696E67732E6572726F72436C6173732C636F6E74657874292E72656D6F766528292C636F6E746578742E';
wwv_flow_api.g_varchar2_table(68) := '72656D6F7665436C6173732873657474696E67732E6572726F72436C617373292C242822696E7075742C206C6162656C222C636F6E74657874292E72656D6F7665436C6173732873657474696E67732E6572726F72436C617373297D66756E6374696F6E';
wwv_flow_api.g_varchar2_table(69) := '206F6E466F726D4572726F72286572726F72732C636F6E746578742C73657474696E6773297B6572726F72732E6E616D65496E76616C6964262628636F6E746578742E70726570656E64285B273C7020636C6173733D22272C73657474696E67732E6572';
wwv_flow_api.g_varchar2_table(70) := '726F72436C6173732C27223E272C73657474696E67732E6572726F724D6573736167652C223C2F703E225D2E6A6F696E28222229292C242873657474696E67732E6E616D652C636F6E74657874292E666F63757328292C242873657474696E67732E6E61';
wwv_flow_api.g_varchar2_table(71) := '6D652C636F6E74657874292E616464436C6173732873657474696E67732E6572726F72436C617373292C2428226C6162656C5B666F723D222B242873657474696E67732E6E616D65292E617474722822696422292B225D222C636F6E74657874292E6164';
wwv_flow_api.g_varchar2_table(72) := '64436C6173732873657474696E67732E6572726F72436C61737329292C6572726F72732E64726177496E76616C69642626636F6E746578742E70726570656E64285B273C7020636C6173733D22272C73657474696E67732E6572726F72436C6173732C27';
wwv_flow_api.g_varchar2_table(73) := '223E272C73657474696E67732E6572726F724D657373616765447261772C223C2F703E225D2E6A6F696E28222229297D66756E6374696F6E2076616C6964617465466F726D28297B7661722076616C69643D21302C6572726F72733D7B64726177496E76';
wwv_flow_api.g_varchar2_table(74) := '616C69643A21312C6E616D65496E76616C69643A21317D2C6F6E4265666F7265417267756D656E74733D5B636F6E746578742C73657474696E67735D2C6F6E4572726F72417267756D656E74733D5B6572726F72732C636F6E746578742C73657474696E';
wwv_flow_api.g_varchar2_table(75) := '67735D3B72657475726E2073657474696E67732E6F6E4265666F726556616C696461746526262266756E6374696F6E223D3D747970656F662073657474696E67732E6F6E4265666F726556616C69646174653F73657474696E67732E6F6E4265666F7265';
wwv_flow_api.g_varchar2_table(76) := '56616C69646174652E6170706C792873656C662C6F6E4265666F7265417267756D656E7473293A6F6E4265666F726556616C69646174652E6170706C792873656C662C6F6E4265666F7265417267756D656E7473292C73657474696E67732E647261774F';
wwv_flow_api.g_varchar2_table(77) := '6E6C7926266F75747075742E6C656E6774683C312626286572726F72732E64726177496E76616C69643D21302C76616C69643D2131292C22223D3D3D242873657474696E67732E6E616D652C636F6E74657874292E76616C28292626286572726F72732E';
wwv_flow_api.g_varchar2_table(78) := '6E616D65496E76616C69643D21302C76616C69643D2131292C73657474696E67732E6F6E466F726D4572726F7226262266756E6374696F6E223D3D747970656F662073657474696E67732E6F6E466F726D4572726F723F73657474696E67732E6F6E466F';
wwv_flow_api.g_varchar2_table(79) := '726D4572726F722E6170706C792873656C662C6F6E4572726F72417267756D656E7473293A6F6E466F726D4572726F722E6170706C792873656C662C6F6E4572726F72417267756D656E7473292C76616C69647D66756E6374696F6E2064726177536967';
wwv_flow_api.g_varchar2_table(80) := '6E61747572652870617468732C636F6E746578742C736176654F7574707574297B666F7228766172206920696E20706174687329226F626A656374223D3D747970656F662070617468735B695D262628636F6E746578742E626567696E5061746828292C';
wwv_flow_api.g_varchar2_table(81) := '636F6E746578742E6D6F7665546F2870617468735B695D2E6D782C70617468735B695D2E6D79292C636F6E746578742E6C696E65546F2870617468735B695D2E6C782C70617468735B695D2E6C79292C636F6E746578742E6C696E654361703D73657474';
wwv_flow_api.g_varchar2_table(82) := '696E67732E70656E4361702C636F6E746578742E7374726F6B6528292C636F6E746578742E636C6F73655061746828292C736176654F757470757426266F75747075742E70757368287B6C783A70617468735B695D2E6C782C6C793A70617468735B695D';
wwv_flow_api.g_varchar2_table(83) := '2E6C792C6D783A70617468735B695D2E6D782C6D793A70617468735B695D2E6D797D29297D66756E6374696F6E20696E697428297B7061727365466C6F617428282F4350552E2B4F5320285B302D395F5D7B337D292E2A4170706C655765626B69742E2A';
wwv_flow_api.g_varchar2_table(84) := '4D6F62696C652F692E65786563286E6176696761746F722E757365724167656E74297C7C5B302C22345F32225D295B315D2E7265706C61636528225F222C222E2229293C342E31262628242E666E2E4F6C646F66667365743D242E666E2E6F6666736574';
wwv_flow_api.g_varchar2_table(85) := '2C242E666E2E6F66667365743D66756E6374696F6E28297B76617220726573756C743D242874686973292E4F6C646F666673657428293B72657475726E20726573756C742E746F702D3D77696E646F772E7363726F6C6C592C726573756C742E6C656674';
wwv_flow_api.g_varchar2_table(86) := '2D3D77696E646F772E7363726F6C6C582C726573756C747D292C242873657474696E67732E74797065642C636F6E74657874292E62696E64282273656C65637473746172742E7369676E6174757265706164222C66756E6374696F6E2865297B72657475';
wwv_flow_api.g_varchar2_table(87) := '726E202428652E746172676574292E697328223A696E70757422297D292C63616E7661732E62696E64282273656C65637473746172742E7369676E6174757265706164222C66756E6374696F6E2865297B72657475726E202428652E746172676574292E';
wwv_flow_api.g_varchar2_table(88) := '697328223A696E70757422297D292C21656C656D656E742E676574436F6E746578742626466C61736843616E7661732626466C61736843616E7661732E696E6974456C656D656E7428656C656D656E74292C656C656D656E742E676574436F6E74657874';
wwv_flow_api.g_varchar2_table(89) := '26262863616E766173436F6E746578743D656C656D656E742E676574436F6E746578742822326422292C242873657474696E67732E7369672C636F6E74657874292E73686F7728292C73657474696E67732E646973706C61794F6E6C797C7C2873657474';
wwv_flow_api.g_varchar2_table(90) := '696E67732E647261774F6E6C797C7C28242873657474696E67732E6E616D652C636F6E74657874292E62696E6428226B657975702E7369676E6174757265706164222C66756E6374696F6E28297B7479706528242874686973292E76616C2829297D292C';
wwv_flow_api.g_varchar2_table(91) := '242873657474696E67732E6E616D652C636F6E74657874292E62696E642822626C75722E7369676E6174757265706164222C66756E6374696F6E28297B7479706528242874686973292E76616C2829297D292C242873657474696E67732E647261774974';
wwv_flow_api.g_varchar2_table(92) := '2C636F6E74657874292E62696E642822636C69636B2E7369676E6174757265706164222C66756E6374696F6E2865297B652E70726576656E7444656661756C7428292C64726177497428297D29292C73657474696E67732E647261774F6E6C797C7C2264';
wwv_flow_api.g_varchar2_table(93) := '7261774974223D3D3D73657474696E67732E64656661756C74416374696F6E3F64726177497428293A74797065497428292C73657474696E67732E76616C69646174654669656C6473262628242873656C6563746F72292E69732822666F726D22293F24';
wwv_flow_api.g_varchar2_table(94) := '2873656C6563746F72292E62696E6428227375626D69742E7369676E6174757265706164222C66756E6374696F6E28297B72657475726E2076616C6964617465466F726D28297D293A242873656C6563746F72292E706172656E74732822666F726D2229';
wwv_flow_api.g_varchar2_table(95) := '2E62696E6428227375626D69742E7369676E6174757265706164222C66756E6374696F6E28297B72657475726E2076616C6964617465466F726D28297D29292C242873657474696E67732E7369674E61762C636F6E74657874292E73686F77282929297D';
wwv_flow_api.g_varchar2_table(96) := '7661722073656C663D746869732C73657474696E67733D242E657874656E64287B7D2C242E666E2E7369676E61747572655061642E64656661756C74732C6F7074696F6E73292C636F6E746578743D242873656C6563746F72292C63616E7661733D2428';
wwv_flow_api.g_varchar2_table(97) := '73657474696E67732E63616E7661732C636F6E74657874292C656C656D656E743D63616E7661732E6765742830292C63616E766173436F6E746578743D6E756C6C2C70726576696F75733D7B783A6E756C6C2C793A6E756C6C7D2C6F75747075743D5B5D';
wwv_flow_api.g_varchar2_table(98) := '2C6D6F7573654C6561766554696D656F75743D21312C6D6F757365427574746F6E446F776E3D21312C746F75636861626C653D21312C6576656E7473426F756E643D21312C74797065497444656661756C74466F6E7453697A653D33302C747970654974';
wwv_flow_api.g_varchar2_table(99) := '43757272656E74466F6E7453697A653D74797065497444656661756C74466F6E7453697A652C7479706549744E756D43686172733D303B242E657874656E642873656C662C7B7369676E61747572655061643A22322E352E30222C696E69743A66756E63';
wwv_flow_api.g_varchar2_table(100) := '74696F6E28297B696E697428297D2C7570646174654F7074696F6E733A66756E6374696F6E286F7074696F6E73297B242E657874656E642873657474696E67732C6F7074696F6E73297D2C726567656E65726174653A66756E6374696F6E287061746873';
wwv_flow_api.g_varchar2_table(101) := '297B73656C662E636C65617243616E76617328292C242873657474696E67732E74797065642C636F6E74657874292E6869646528292C22737472696E67223D3D747970656F6620706174687326262870617468733D4A534F4E2E70617273652870617468';
wwv_flow_api.g_varchar2_table(102) := '7329292C647261775369676E61747572652870617468732C63616E766173436F6E746578742C2130292C73657474696E67732E6F75747075742626242873657474696E67732E6F75747075742C636F6E74657874292E6C656E6774683E30262624287365';
wwv_flow_api.g_varchar2_table(103) := '7474696E67732E6F75747075742C636F6E74657874292E76616C284A534F4E2E737472696E67696679286F757470757429297D2C636C65617243616E7661733A66756E6374696F6E28297B636C65617243616E76617328297D2C6765745369676E617475';
wwv_flow_api.g_varchar2_table(104) := '72653A66756E6374696F6E28297B72657475726E206F75747075747D2C6765745369676E6174757265537472696E673A66756E6374696F6E28297B72657475726E204A534F4E2E737472696E67696679286F7574707574297D2C6765745369676E617475';
wwv_flow_api.g_varchar2_table(105) := '7265496D6167653A66756E6374696F6E28297B76617220746D7043616E7661733D646F63756D656E742E637265617465456C656D656E74282263616E76617322292C746D70436F6E746578743D6E756C6C2C646174613D6E756C6C3B72657475726E2074';
wwv_flow_api.g_varchar2_table(106) := '6D7043616E7661732E7374796C652E706F736974696F6E3D226162736F6C757465222C746D7043616E7661732E7374796C652E746F703D222D393939656D222C746D7043616E7661732E77696474683D656C656D656E742E77696474682C746D7043616E';
wwv_flow_api.g_varchar2_table(107) := '7661732E6865696768743D656C656D656E742E6865696768742C646F63756D656E742E626F64792E617070656E644368696C6428746D7043616E766173292C21746D7043616E7661732E676574436F6E746578742626466C61736843616E766173262646';
wwv_flow_api.g_varchar2_table(108) := '6C61736843616E7661732E696E6974456C656D656E7428746D7043616E766173292C746D70436F6E746578743D746D7043616E7661732E676574436F6E746578742822326422292C746D70436F6E746578742E66696C6C5374796C653D73657474696E67';
wwv_flow_api.g_varchar2_table(109) := '732E6267436F6C6F75722C746D70436F6E746578742E66696C6C5265637428302C302C656C656D656E742E77696474682C656C656D656E742E686569676874292C746D70436F6E746578742E6C696E6557696474683D73657474696E67732E70656E5769';
wwv_flow_api.g_varchar2_table(110) := '6474682C746D70436F6E746578742E7374726F6B655374796C653D73657474696E67732E70656E436F6C6F75722C647261775369676E6174757265286F75747075742C746D70436F6E74657874292C646174613D746D7043616E7661732E746F44617461';
wwv_flow_api.g_varchar2_table(111) := '55524C2E6170706C7928746D7043616E7661732C617267756D656E7473292C646F63756D656E742E626F64792E72656D6F76654368696C6428746D7043616E766173292C746D7043616E7661733D6E756C6C2C646174617D2C76616C6964617465466F72';
wwv_flow_api.g_varchar2_table(112) := '6D3A66756E6374696F6E28297B72657475726E2076616C6964617465466F726D28297D7D297D242E666E2E7369676E61747572655061643D66756E6374696F6E286F7074696F6E73297B766172206170693D6E756C6C3B72657475726E20746869732E65';
wwv_flow_api.g_varchar2_table(113) := '6163682866756E6374696F6E28297B242E6461746128746869732C22706C7567696E2D7369676E617475726550616422293F286170693D242E6461746128746869732C22706C7567696E2D7369676E617475726550616422292C6170692E757064617465';
wwv_flow_api.g_varchar2_table(114) := '4F7074696F6E73286F7074696F6E7329293A286170693D6E6577205369676E617475726550616428746869732C6F7074696F6E73292C6170692E696E697428292C242E6461746128746869732C22706C7567696E2D7369676E6174757265506164222C61';
wwv_flow_api.g_varchar2_table(115) := '706929297D292C6170697D2C242E666E2E7369676E61747572655061642E64656661756C74733D7B64656661756C74416374696F6E3A22747970654974222C646973706C61794F6E6C793A21312C647261774F6E6C793A21312C63616E7661733A226361';
wwv_flow_api.g_varchar2_table(116) := '6E766173222C7369673A222E736967222C7369674E61763A222E7369674E6176222C6267436F6C6F75723A2223666666666666222C70656E436F6C6F75723A2223313435333934222C70656E57696474683A322C70656E4361703A22726F756E64222C6C';
wwv_flow_api.g_varchar2_table(117) := '696E65436F6C6F75723A2223636363222C6C696E6557696474683A322C6C696E654D617267696E3A352C6C696E65546F703A33352C6E616D653A222E6E616D65222C74797065643A222E7479706564222C636C6561723A222E636C656172427574746F6E';
wwv_flow_api.g_varchar2_table(118) := '222C7479706549743A222E7479706549742061222C6472617749743A222E6472617749742061222C747970654974446573633A222E74797065497444657363222C647261774974446573633A222E64726177497444657363222C6F75747075743A222E6F';
wwv_flow_api.g_varchar2_table(119) := '7574707574222C63757272656E74436C6173733A2263757272656E74222C76616C69646174654669656C64733A21302C6572726F72436C6173733A226572726F72222C6572726F724D6573736167653A22506C6561736520656E74657220796F7572206E';
wwv_flow_api.g_varchar2_table(120) := '616D65222C6572726F724D657373616765447261773A22506C65617365207369676E2074686520646F63756D656E74222C6F6E4265666F726556616C69646174653A6E756C6C2C6F6E466F726D4572726F723A6E756C6C2C6F6E447261773A6E756C6C2C';
wwv_flow_api.g_varchar2_table(121) := '6F6E44726177456E643A6E756C6C7D7D286A5175657279293B';
null;
end;
/
begin
wwv_flow_api.create_plugin_file(
 p_id=>wwv_flow_api.id(66369541844386227)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_file_name=>'jquery.signaturepad.min.js'
,p_mime_type=>'text/javascript'
,p_file_content=>wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
);
end;
/
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
wwv_flow_api.g_varchar2_table(1) := '69662821746869732E4A534F4E297B746869732E4A534F4E3D7B7D3B7D0A2866756E6374696F6E28297B66756E6374696F6E2066286E297B72657475726E206E3C31303F2730272B6E3A6E3B7D0A696628747970656F6620446174652E70726F746F7479';
wwv_flow_api.g_varchar2_table(2) := '70652E746F4A534F4E213D3D2766756E6374696F6E27297B446174652E70726F746F747970652E746F4A534F4E3D66756E6374696F6E286B6579297B72657475726E20697346696E69746528746869732E76616C75654F662829293F746869732E676574';
wwv_flow_api.g_varchar2_table(3) := '55544346756C6C5965617228292B272D272B0A6628746869732E6765745554434D6F6E746828292B31292B272D272B0A6628746869732E676574555443446174652829292B2754272B0A6628746869732E676574555443486F7572732829292B273A272B';
wwv_flow_api.g_varchar2_table(4) := '0A6628746869732E6765745554434D696E757465732829292B273A272B0A6628746869732E6765745554435365636F6E64732829292B275A273A6E756C6C3B7D3B537472696E672E70726F746F747970652E746F4A534F4E3D4E756D6265722E70726F74';
wwv_flow_api.g_varchar2_table(5) := '6F747970652E746F4A534F4E3D426F6F6C65616E2E70726F746F747970652E746F4A534F4E3D66756E6374696F6E286B6579297B72657475726E20746869732E76616C75654F6628293B7D3B7D0A7661722063783D2F5B5C75303030305C75303061645C';
wwv_flow_api.g_varchar2_table(6) := '75303630302D5C75303630345C75303730665C75313762345C75313762355C75323030632D5C75323030665C75323032382D5C75323032665C75323036302D5C75323036665C75666566665C75666666302D5C75666666665D2F672C657363617061626C';
wwv_flow_api.g_varchar2_table(7) := '653D2F5B5C5C5C225C7830302D5C7831665C7837662D5C7839665C75303061645C75303630302D5C75303630345C75303730665C75313762345C75313762355C75323030632D5C75323030665C75323032382D5C75323032665C75323036302D5C753230';
wwv_flow_api.g_varchar2_table(8) := '36665C75666566665C75666666302D5C75666666665D2F672C6761702C696E64656E742C6D6574613D7B275C62273A275C5C62272C275C74273A275C5C74272C275C6E273A275C5C6E272C275C66273A275C5C66272C275C72273A275C5C72272C272227';
wwv_flow_api.g_varchar2_table(9) := '3A275C5C22272C275C5C273A275C5C5C5C277D2C7265703B66756E6374696F6E2071756F746528737472696E67297B657363617061626C652E6C617374496E6465783D303B72657475726E20657363617061626C652E7465737428737472696E67293F27';
wwv_flow_api.g_varchar2_table(10) := '22272B737472696E672E7265706C61636528657363617061626C652C66756E6374696F6E2861297B76617220633D6D6574615B615D3B72657475726E20747970656F6620633D3D3D27737472696E67273F633A275C5C75272B282730303030272B612E63';
wwv_flow_api.g_varchar2_table(11) := '686172436F646541742830292E746F537472696E6728313629292E736C696365282D34293B7D292B2722273A2722272B737472696E672B2722273B7D0A66756E6374696F6E20737472286B65792C686F6C646572297B76617220692C6B2C762C6C656E67';
wwv_flow_api.g_varchar2_table(12) := '74682C6D696E643D6761702C7061727469616C2C76616C75653D686F6C6465725B6B65795D3B69662876616C75652626747970656F662076616C75653D3D3D276F626A656374272626747970656F662076616C75652E746F4A534F4E3D3D3D2766756E63';
wwv_flow_api.g_varchar2_table(13) := '74696F6E27297B76616C75653D76616C75652E746F4A534F4E286B6579293B7D0A696628747970656F66207265703D3D3D2766756E6374696F6E27297B76616C75653D7265702E63616C6C28686F6C6465722C6B65792C76616C7565293B7D0A73776974';
wwv_flow_api.g_varchar2_table(14) := '636828747970656F662076616C7565297B6361736527737472696E67273A72657475726E2071756F74652876616C7565293B63617365276E756D626572273A72657475726E20697346696E6974652876616C7565293F537472696E672876616C7565293A';
wwv_flow_api.g_varchar2_table(15) := '276E756C6C273B6361736527626F6F6C65616E273A63617365276E756C6C273A72657475726E20537472696E672876616C7565293B63617365276F626A656374273A6966282176616C7565297B72657475726E276E756C6C273B7D0A6761702B3D696E64';
wwv_flow_api.g_varchar2_table(16) := '656E743B7061727469616C3D5B5D3B6966284F626A6563742E70726F746F747970652E746F537472696E672E6170706C792876616C7565293D3D3D275B6F626A6563742041727261795D27297B6C656E6774683D76616C75652E6C656E6774683B666F72';
wwv_flow_api.g_varchar2_table(17) := '28693D303B693C6C656E6774683B692B3D31297B7061727469616C5B695D3D73747228692C76616C7565297C7C276E756C6C273B7D0A763D7061727469616C2E6C656E6774683D3D3D303F275B5D273A6761703F275B5C6E272B6761702B0A7061727469';
wwv_flow_api.g_varchar2_table(18) := '616C2E6A6F696E28272C5C6E272B676170292B275C6E272B0A6D696E642B275D273A275B272B7061727469616C2E6A6F696E28272C27292B275D273B6761703D6D696E643B72657475726E20763B7D0A6966287265702626747970656F66207265703D3D';
wwv_flow_api.g_varchar2_table(19) := '3D276F626A65637427297B6C656E6774683D7265702E6C656E6774683B666F7228693D303B693C6C656E6774683B692B3D31297B6B3D7265705B695D3B696628747970656F66206B3D3D3D27737472696E6727297B763D737472286B2C76616C7565293B';
wwv_flow_api.g_varchar2_table(20) := '69662876297B7061727469616C2E707573682871756F7465286B292B286761703F273A20273A273A27292B76293B7D7D7D7D656C73657B666F72286B20696E2076616C7565297B6966284F626A6563742E6861734F776E50726F70657274792E63616C6C';
wwv_flow_api.g_varchar2_table(21) := '2876616C75652C6B29297B763D737472286B2C76616C7565293B69662876297B7061727469616C2E707573682871756F7465286B292B286761703F273A20273A273A27292B76293B7D7D7D7D0A763D7061727469616C2E6C656E6774683D3D3D303F277B';
wwv_flow_api.g_varchar2_table(22) := '7D273A6761703F277B5C6E272B6761702B7061727469616C2E6A6F696E28272C5C6E272B676170292B275C6E272B0A6D696E642B277D273A277B272B7061727469616C2E6A6F696E28272C27292B277D273B6761703D6D696E643B72657475726E20763B';
wwv_flow_api.g_varchar2_table(23) := '7D7D0A696628747970656F66204A534F4E2E737472696E67696679213D3D2766756E6374696F6E27297B4A534F4E2E737472696E676966793D66756E6374696F6E2876616C75652C7265706C616365722C7370616365297B76617220693B6761703D2727';
wwv_flow_api.g_varchar2_table(24) := '3B696E64656E743D27273B696628747970656F662073706163653D3D3D276E756D62657227297B666F7228693D303B693C73706163653B692B3D31297B696E64656E742B3D2720273B7D7D656C736520696628747970656F662073706163653D3D3D2773';
wwv_flow_api.g_varchar2_table(25) := '7472696E6727297B696E64656E743D73706163653B7D0A7265703D7265706C616365723B6966287265706C616365722626747970656F66207265706C61636572213D3D2766756E6374696F6E27262628747970656F66207265706C61636572213D3D276F';
wwv_flow_api.g_varchar2_table(26) := '626A656374277C7C747970656F66207265706C616365722E6C656E677468213D3D276E756D6265722729297B7468726F77206E6577204572726F7228274A534F4E2E737472696E6769667927293B7D0A72657475726E207374722827272C7B27273A7661';
wwv_flow_api.g_varchar2_table(27) := '6C75657D293B7D3B7D0A696628747970656F66204A534F4E2E7061727365213D3D2766756E6374696F6E27297B4A534F4E2E70617273653D66756E6374696F6E28746578742C72657669766572297B766172206A3B66756E6374696F6E2077616C6B2868';
wwv_flow_api.g_varchar2_table(28) := '6F6C6465722C6B6579297B766172206B2C762C76616C75653D686F6C6465725B6B65795D3B69662876616C75652626747970656F662076616C75653D3D3D276F626A65637427297B666F72286B20696E2076616C7565297B6966284F626A6563742E6861';
wwv_flow_api.g_varchar2_table(29) := '734F776E50726F70657274792E63616C6C2876616C75652C6B29297B763D77616C6B2876616C75652C6B293B69662876213D3D756E646566696E6564297B76616C75655B6B5D3D763B7D656C73657B64656C6574652076616C75655B6B5D3B7D7D7D7D0A';
wwv_flow_api.g_varchar2_table(30) := '72657475726E20726576697665722E63616C6C28686F6C6465722C6B65792C76616C7565293B7D0A63782E6C617374496E6465783D303B69662863782E74657374287465787429297B746578743D746578742E7265706C6163652863782C66756E637469';
wwv_flow_api.g_varchar2_table(31) := '6F6E2861297B72657475726E275C5C75272B0A282730303030272B612E63686172436F646541742830292E746F537472696E6728313629292E736C696365282D34293B7D293B7D0A6966282F5E5B5C5D2C3A7B7D5C735D2A242F2E746573742874657874';
wwv_flow_api.g_varchar2_table(32) := '2E7265706C616365282F5C5C283F3A5B225C5C5C2F62666E72745D7C755B302D39612D66412D465D7B347D292F672C274027292E7265706C616365282F225B5E225C5C5C6E5C725D2A227C747275657C66616C73657C6E756C6C7C2D3F5C642B283F3A5C';
wwv_flow_api.g_varchar2_table(33) := '2E5C642A293F283F3A5B65455D5B2B5C2D5D3F5C642B293F2F672C275D27292E7265706C616365282F283F3A5E7C3A7C2C29283F3A5C732A5C5B292B2F672C27272929297B6A3D6576616C282728272B746578742B272927293B72657475726E20747970';
wwv_flow_api.g_varchar2_table(34) := '656F6620726576697665723D3D3D2766756E6374696F6E273F77616C6B287B27273A6A7D2C2727293A6A3B7D0A7468726F77206E65772053796E7461784572726F7228274A534F4E2E706172736527293B7D3B7D7D2829293B';
null;
end;
/
begin
wwv_flow_api.create_plugin_file(
 p_id=>wwv_flow_api.id(66370270515388041)
,p_plugin_id=>wwv_flow_api.id(66369261679382330)
,p_file_name=>'json2.min.js'
,p_mime_type=>'text/javascript'
,p_file_content=>wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
);
end;
/
prompt --application/shared_components/plugins/process_type/process_plugin_com_enkitec_sigpadimgsvr
begin
wwv_flow_api.create_plugin(
 p_id=>wwv_flow_api.id(66457368648929269)
,p_plugin_type=>'PROCESS TYPE'
,p_name=>'PROCESS_PLUGIN_COM_ENKITEC_SIGPADIMGSVR'
,p_display_name=>'SignaturePad IMG Saver'
,p_supported_ui_types=>'DESKTOP'
,p_supported_component_types=>'APEX_APPLICATION_PAGE_ITEMS'
,p_image_prefix => nvl(wwv_flow_application_install.get_static_plugin_file_prefix('PROCESS TYPE','PROCESS_PLUGIN_COM_ENKITEC_SIGPADIMGSVR'),'')
,p_plsql_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function signature_pad_image_saver_exec (',
'    p_process in apex_plugin.t_process,',
'    p_plugin  in apex_plugin.t_plugin )',
'    return apex_plugin.t_process_exec_result',
'IS',
'-- VARIABLES',
'    l_return       apex_plugin.t_process_exec_result;',
'    L_CLOB         CLOB; -- BASE 64 CLOB string ',
'    L_BLOB         BLOB; -- BINARY Version of IMAGE',
'    L_SQL          varchar2(32767); -- UPDATE STATEMENT',
'    l_table        varchar2(32767) := p_process.attribute_01; -- Table to update',
'    l_pk_field     varchar2(32767) := p_process.attribute_02; -- Primary Key column',
'    l_image_col    varchar2(32767) := p_process.attribute_03; -- Image Column',
'    l_screen_pk    varchar2(32767) := p_process.attribute_04; -- Screen Field w/ PK',
'    l_screen_pk_name varchar2(32767) := substr(p_process.attribute_04,2,length(p_process.attribute_04)-2);',
'',
'BEGIN',
'-- DEBUG',
'IF apex_application.g_debug then ',
'   apex_plugin_util.debug_process( p_plugin => p_plugin,',
'                                   p_process => p_process);',
'END IF;',
'--',
'-- Initialize the return message;',
'--',
'   l_return.success_message := p_process.success_message;',
'--',
'-- We only want to process this if the CLOB_CONTENT collection exists.',
'--',
'IF APEX_COLLECTION.COLLECTION_EXISTS(''CLOB_CONTENT'') THEN',
'--',
'-- Get the BASE64 encoded image from the collection.',
'--',
'    SELECT clob001 ',
'      INTO L_CLOB',
'      FROM apex_collections ',
'     WHERE collection_name = ''CLOB_CONTENT'';',
'--',
'-- Take the BASE64 Encoded item and create a blob out of it.',
'--',
'   l_blob := APEX_WEB_SERVICE.CLOBBASE642BLOB(substr(L_CLOB,instr(L_CLOB,'','')+1));',
'--',
'-- Build and execute the update statement. But only if the value of the SCREEN_PK is not null',
'--',
'  IF L_SCREEN_PK IS NOT NULL THEN',
'     L_SQL := ''UPDATE ''|| l_table ||'' SET ''||l_image_col||'' = :MYBLOB WHERE ''||l_pk_field||'' = :MYPKVAL'';',
'     -- EXECUTE',
'     execute immediate l_sql USING l_blob,l_screen_pk;',
'  ELSE ',
'     raise_application_error(-20201,''SignaturePad IMG Saver - Null value in Primary Key Field supplied'');',
'  END IF;',
'--',
'-- Now delete the collection so it''s not there to cloud up the next time around...',
'--',
'  APEX_COLLECTION.DELETE_COLLECTION(''CLOB_CONTENT'');',
'--',
'ELSE',
'     raise_application_error(-20201,''SignaturePad IMG Saver - Collection "CLOB_CONTENT" does not exist.'');',
'END IF;',
'RETURN l_return;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      RAISE; ',
'END;'))
,p_api_version=>1
,p_execution_function=>'signature_pad_image_saver_exec'
,p_substitute_attributes=>true
,p_subscribe_plugin_settings=>true
,p_version_identifier=>'1.0'
,p_about_url=>'http://www.enkitec.com/products/plugins/signaturepad'
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(66457658521945302)
,p_plugin_id=>wwv_flow_api.id(66457368648929269)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>1
,p_display_sequence=>10
,p_prompt=>'Table Containing Image Column (BLOB)'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_display_length=>30
,p_max_length=>30
,p_is_translatable=>false
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(66458168217948113)
,p_plugin_id=>wwv_flow_api.id(66457368648929269)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>2
,p_display_sequence=>20
,p_prompt=>'Primary Key Column Name'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_display_length=>30
,p_max_length=>30
,p_is_translatable=>false
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(66458682070952097)
,p_plugin_id=>wwv_flow_api.id(66457368648929269)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>3
,p_display_sequence=>30
,p_prompt=>'Signature Image Column Name (BLOB)'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_display_length=>30
,p_max_length=>30
,p_is_translatable=>false
);
wwv_flow_api.create_plugin_attribute(
 p_id=>wwv_flow_api.id(66459270783034318)
,p_plugin_id=>wwv_flow_api.id(66457368648929269)
,p_attribute_scope=>'COMPONENT'
,p_attribute_sequence=>4
,p_display_sequence=>40
,p_prompt=>'Primary Key Value from Session State (&ITEM_NAME.)'
,p_attribute_type=>'TEXT'
,p_is_required=>true
,p_display_length=>30
,p_max_length=>30
,p_is_translatable=>false
);
end;
/
prompt --application/user_interfaces
begin
wwv_flow_api.create_user_interface(
 p_id=>wwv_flow_api.id(9587179846250856)
,p_ui_type_name=>'DESKTOP'
,p_display_name=>'Desktop'
,p_display_seq=>10
,p_use_auto_detect=>true
,p_is_default=>true
,p_theme_id=>20
,p_home_url=>'f?p=&APP_ID.:5:&SESSION.'
,p_login_url=>'f?p=&APP_ID.:LOGIN_DESKTOP:&SESSION.'
,p_theme_style_by_user_pref=>false
,p_nav_list_template_options=>'#DEFAULT#'
,p_include_legacy_javascript=>true
,p_include_jquery_migrate=>true
,p_nav_bar_type=>'NAVBAR'
,p_nav_bar_template_options=>'#DEFAULT#'
);
end;
/
prompt --application/user_interfaces/combined_files
begin
null;
end;
/
prompt --application/pages/page_00001
begin
wwv_flow_api.create_page(
 p_id=>1
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'WC B2B New Trading Partner'
,p_page_mode=>'NORMAL'
,p_step_title=>'WC B2B New Trading Partner'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'WC B2B New Trading Partner'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_browser_cache=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20161115132128'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2500909034635364)
,p_plug_name=>'Setup New B2B Trading Partners'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(2501504944635372)
,p_name=>'Customer Details'
,p_parent_plug_id=>wwv_flow_api.id(2500909034635364)
,p_template=>wwv_flow_api.id(24617707682658552)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT hca.account_number customer_number',
'     , hca.account_name customer_name',
'     , hp.party_number ',
'     , REPLACE(REPLACE(hp.party_name, '','' , '' ''), ''#'','''') party_name',
'     , hca.cust_account_id',
'     , hca.party_id',
'     , :P1_USER_ROLE user_role',
'     , APPS.XXWC_B2B_SO_IB_PKG.GET_SALES_REP(hca.cust_account_id) SALES_REP',
' FROM apps.hz_Cust_accounts_all hca',
'    , apps.hz_parties hp',
'WHERE 1 = 1',
'  AND :P1_SHOW_ALL_ACCTS = ''N''',
'  --And hca.account_number = ''10000024272''',
'  AND hca.party_id = hp.party_id',
'  AND (hca.account_number = :P1_CUSTOMER_NUMBER OR :P1_CUSTOMER_NUMBER = 0 OR :P1_CUSTOMER_NUMBER IS NULL)',
'  AND (hca.account_name like UPPER(:P1_CUSTOMER_NAME)||''%'' OR :P1_CUSTOMER_NAME = ''0'' OR :P1_CUSTOMER_NAME IS NULL)',
'  AND (:P1_PARTY_NUMBER = 0 OR :P1_PARTY_NUMBER IS NULL)',
'UNION',
'SELECT hca.account_number customer_number',
'     , hca.account_name customer_name',
'     , hp.party_number ',
'     , REPLACE(REPLACE(hp.party_name, '','' , '' ''), ''#'','''') party_name',
'     , hca.cust_account_id',
'     , hca.party_id',
'     , :P1_USER_ROLE user_role',
'     , APPS.XXWC_B2B_SO_IB_PKG.GET_SALES_REP(hca.cust_account_id) SALES_REP',
'FROM apps.hz_Cust_accounts_all hca',
'    , apps.hz_parties hp',
'    , apps.hz_Cust_accounts_all hca2',
'WHERE 1 = 1',
'  AND :P1_SHOW_ALL_ACCTS = ''Y''',
'  AND (hca2.account_number = :P1_CUSTOMER_NUMBER OR :P1_CUSTOMER_NUMBER = 0 OR :P1_CUSTOMER_NUMBER IS NULL) ',
'  AND (hca2.account_name like UPPER(:P1_CUSTOMER_NAME)||''%'' OR :P1_CUSTOMER_NAME = ''0'' OR :P1_CUSTOMER_NAME IS NULL)',
'  AND hp.party_id = hca2.party_id',
'  AND hca.party_id = hp.party_id',
'  AND (:P1_PARTY_NUMBER = 0 OR :P1_PARTY_NUMBER IS NULL)',
'UNION',
'SELECT hca.account_number customer_number',
'     , hca.account_name customer_name',
'     , hp.party_number ',
'     , REPLACE(REPLACE(hp.party_name, '','' , '' ''), ''#'','''') party_name',
'     , hca.cust_account_id',
'     , hca.party_id',
'     , :P1_USER_ROLE user_role',
'     , APPS.XXWC_B2B_SO_IB_PKG.GET_SALES_REP(hca.cust_account_id) SALES_REP',
' FROM apps.hz_Cust_accounts_all hca',
'    , apps.hz_parties hp',
'WHERE 1 = 1',
'  AND hca.party_id = hp.party_id',
'  AND hp.party_number = :P1_PARTY_NUMBER'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''1'' FROM DUAL WHERE :P1_CUSTOMER_NUMBER IS NOT NULL UNION',
'SELECT ''1'' FROM DUAL WHERE :P1_CUSTOMER_NAME IS NOT NULL UNION',
'SELECT ''1'' FROM DUAL WHERE :P1_PARTY_NUMBER IS NOT NULL'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'-- No Records Found --'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2501714172635374)
,p_query_column_id=>1
,p_column_alias=>'CUSTOMER_NUMBER'
,p_column_display_sequence=>3
,p_column_heading=>'Customer Number'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>2
,p_disable_sort_column=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2501815235635375)
,p_query_column_id=>2
,p_column_alias=>'CUSTOMER_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Customer Name'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2501900746635375)
,p_query_column_id=>3
,p_column_alias=>'PARTY_NUMBER'
,p_column_display_sequence=>1
,p_column_heading=>'Party Number'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:3:&SESSION.::&DEBUG.:3:P3_PARTY_ID,P3_PARTY_NAME,P3_PARTY_NUMBER,P3_USER_ROLE:#PARTY_ID#,#PARTY_NAME#,#PARTY_NUMBER#,#USER_ROLE#'
,p_column_linktext=>'#PARTY_NUMBER#'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2502008820635375)
,p_query_column_id=>4
,p_column_alias=>'PARTY_NAME'
,p_column_display_sequence=>2
,p_column_heading=>'Party Name'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2502114505635375)
,p_query_column_id=>5
,p_column_alias=>'CUST_ACCOUNT_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CUST_ACCOUNT_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2502219354635375)
,p_query_column_id=>6
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>6
,p_column_heading=>'PARTY_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2502323346635375)
,p_query_column_id=>7
,p_column_alias=>'USER_ROLE'
,p_column_display_sequence=>7
,p_column_heading=>'User Role'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2502420562635375)
,p_query_column_id=>8
,p_column_alias=>'SALES_REP'
,p_column_display_sequence=>8
,p_column_heading=>'Account Manager'
,p_use_as_row_header=>'N'
,p_column_css_style=>'width:400px; display:block; white-space:normal;'
,p_disable_sort_column=>'N'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2502522542635375)
,p_plug_name=>'Customer and Party Info'
,p_parent_plug_id=>wwv_flow_api.id(2500909034635364)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_grid_column_span=>20
,p_plug_display_point=>'BODY'
,p_translate_title=>'N'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2501107840635368)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(2500909034635364)
,p_button_name=>'CLEAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Clear'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:RP,3:P1_CUSTOMER_NUMBER,P1_CUSTOMER_NAME,P1_SHOW_ALL_ACCTS,P1_PARTY_NUMBER:,,N,'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2501303524635368)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(2500909034635364)
,p_button_name=>'FIND'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Find'
,p_button_position=>'BOTTOM'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(2505000249635385)
,p_branch_action=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'NEVER'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(2505225203635386)
,p_branch_action=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'NEVER'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2502723583635376)
,p_name=>'P1_PARTY_ID'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_api.id(2502522542635375)
,p_prompt=>'PARTY_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2502922866635378)
,p_name=>'P1_CUST_ACCOUNT_ID'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_api.id(2502522542635375)
,p_prompt=>'CUST_ACCOUNT_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2503128308635378)
,p_name=>'P1_CUSTOMER_NUMBER'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_api.id(2502522542635375)
,p_prompt=>'Customer Number'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2503300643635379)
,p_name=>'P1_CUSTOMER_NAME'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_api.id(2502522542635375)
,p_prompt=>'Customer Name'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2503518012635379)
,p_name=>'P1_PARTY_NAME'
,p_item_sequence=>11
,p_item_plug_id=>wwv_flow_api.id(2502522542635375)
,p_prompt=>'PARTY_NAME'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2503721004635380)
,p_name=>'P1_PARTY_NUMBER'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(2502522542635375)
,p_prompt=>'Party Number'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2503927499635380)
,p_name=>'P1_SHOW_ALL_ACCTS'
,p_is_required=>true
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(2502522542635375)
,p_item_default=>'N'
,p_prompt=>'Show All Customer Accounts'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'YN1'
,p_lov=>'.'||wwv_flow_api.id(2505301077635389)||'.'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621516577658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2504108724635381)
,p_name=>'P1_FILLER_SPACE'
,p_item_sequence=>31
,p_item_plug_id=>wwv_flow_api.id(2502522542635375)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2504301620635381)
,p_name=>'P1_USER_ROLE'
,p_item_sequence=>41
,p_item_plug_id=>wwv_flow_api.id(2502522542635375)
,p_item_default=>'1'
,p_prompt=>'User Responsibility'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621516577658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2504513594635383)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Find User Role'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'l_count NUMBER := 0;',
'BEGIN',
'',
'SELECT COUNT(1)',
'  INTO l_count',
'FROM  xxwc.xxwc_ms_user_roles_tbl a',
',     xxwc.xxwc_ms_roles_tbl b',
',     xxwc.xxwc_ms_role_resp_tbl c',
',     xxwc.xxwc_ms_responsibility_tbl d',
'WHERE a.role_id = b.role_id(+)',
'AND   b.role_id = c.role_id(+)',
'AND   c.responsibility_id = d.responsibility_id(+)',
'AND   a.user_id= :APP_USER',
'and d.app_id = :APP_ID',
'and c.app_id = :APP_ID',
'and b.role_name IN (''Oracle Developers'', ''WhiteCap Support'');',
'',
'IF l_count > 0 THEN',
'  :P1_USER_ROLE := ''3'';',
'END IF; ',
'',
'EXCEPTION',
'WHEN OTHERS THEN',
'  :P1_USER_ROLE := ''1'';',
'END;'))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2504703077635384)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Find User Role'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'l_salesrep_flag VARCHAR2(1);',
'BEGIN',
'',
'SELECT apps.xxwc_b2b_so_ib_pkg.is_sales_rep(:P1_PARTY_ID, nvl(v(''APP_USER''),USER))',
'  INTO l_salesrep_flag',
'  FROM dual;',
'',
'IF l_salesrep_flag IN (''Y'', ''0'') THEN ',
'   :P1_USER_ROLE := ''2'';',
'ELSE',
'   :P1_USER_ROLE := ''1'';',
'END IF;',
'',
'dbms_output.put_line(''l_salesrep_flag - ''||l_salesrep_flag);',
'',
'EXCEPTION',
'WHEN OTHERS THEN',
'dbms_output.put_line(''Error - ''||SQLERRM);',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(2501303524635368)
,p_process_when_type=>'NEVER'
);
end;
/
prompt --application/pages/page_00003
begin
wwv_flow_api.create_page(
 p_id=>3
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'WC B2B Trading Partner Maintenance Form'
,p_page_mode=>'NORMAL'
,p_step_title=>'WC B2B Trading Partner Maintenance Form'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'WC B2B Trading Partner Maintenance Form'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_browser_cache=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20170419223326'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(2509129456639552)
,p_name=>'SetUp new B2B Trading Partner - Approval'
,p_template=>wwv_flow_api.id(24617707682658552)
,p_display_sequence=>45
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"PARTY_ID",',
'"PARTY_ID" PARTY_ID_DISPLAY,',
'"PARTY_NAME",',
'"PARTY_NUMBER",',
'"TP_NAME",',
'DECODE(DELIVER_SOA, ''Y'', ''Yes'', ''No'')         "DELIVER_SOA",',
'DECODE(DELIVER_ASN, ''Y'', ''Yes'', ''No'')         "DELIVER_ASN",',
'DECODE(DELIVER_INVOICE, ''Y'', ''Yes'', ''No'')     "DELIVER_INVOICE",',
'DECODE(DELIVER_POA, ''Y'', ''Yes'', ''No'')         "DELIVER_POA",',
'"SOA_EMAIL",',
'"ASN_EMAIL",',
'''[SETUP_POD]'' "SETUP_POD",',
'DECODE(NOTIFY_ACCOUNT_MGR, ''Y'', ''Yes'', ''No'') "NOTIFY_ACCOUNT_MGR",',
'"NOTIFICATION_EMAIL",',
'NVL(STATUS, ''DRAFT'') "STATUS",',
'"COMMENTS",',
'"START_DATE_ACTIVE",',
'"END_DATE_ACTIVE",',
'"CREATION_DATE",',
'"CREATED_BY",',
'"LAST_UPDATE_DATE",',
'"LAST_UPDATED_BY"',
'from XXWC.XXWC_B2B_CUST_INFO_TBL',
'WHERE PARTY_ID = :P3_PARTY_ID'))
,p_source_type=>'NATIVE_TABFORM'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P3_USER_ROLE = ''4'''
,p_display_condition_type=>'EXISTS'
,p_translate_title=>'N'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>wwv_flow_string.join(wwv_flow_t_varchar2(
'B2B is not setup for this Trading Partner. ',
'Please click "Add Row" button to create.'))
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2509318347639555)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'Check$01'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2509417215639556)
,p_query_column_id=>2
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Party Id'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2509503887639556)
,p_query_column_id=>3
,p_column_alias=>'PARTY_ID_DISPLAY'
,p_column_display_sequence=>3
,p_column_heading=>'Party Id Display'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2509629850639556)
,p_query_column_id=>4
,p_column_alias=>'PARTY_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Party Name'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2532531804639613)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_name||'' ''||party_number d, party_name r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_column_width=>360
,p_column_default=>'P3_PARTY_NAME'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2509712544639556)
,p_query_column_id=>5
,p_column_alias=>'PARTY_NUMBER'
,p_column_display_sequence=>5
,p_column_heading=>'Party Number'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2532704364639613)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_number d, party_number r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'P3_PARTY_NUMBER'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NUMBER'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2509818102639556)
,p_query_column_id=>6
,p_column_alias=>'TP_NAME'
,p_column_display_sequence=>18
,p_column_heading=>'Tp Name'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2509912521639556)
,p_query_column_id=>7
,p_column_alias=>'DELIVER_SOA'
,p_column_display_sequence=>7
,p_column_heading=>'Deliver SOA'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'N'
,p_lov_null_value=>'N'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_SOA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510023503639556)
,p_query_column_id=>8
,p_column_alias=>'DELIVER_ASN'
,p_column_display_sequence=>9
,p_column_heading=>'Deliver ASN'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_ASN'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510105958639556)
,p_query_column_id=>9
,p_column_alias=>'DELIVER_INVOICE'
,p_column_display_sequence=>11
,p_column_heading=>'Deliver Invoice'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_INVOICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510225546639557)
,p_query_column_id=>10
,p_column_alias=>'DELIVER_POA'
,p_column_display_sequence=>6
,p_column_heading=>'Deliver POA'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_POA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510300239639557)
,p_query_column_id=>11
,p_column_alias=>'SOA_EMAIL'
,p_column_display_sequence=>8
,p_column_heading=>'SOA Email'
,p_disable_sort_column=>'N'
,p_print_col_width=>'2'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510421635639557)
,p_query_column_id=>12
,p_column_alias=>'ASN_EMAIL'
,p_column_display_sequence=>10
,p_column_heading=>'ASN Email'
,p_disable_sort_column=>'N'
,p_print_col_width=>'2'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3327412327883916)
,p_query_column_id=>13
,p_column_alias=>'SETUP_POD'
,p_column_display_sequence=>12
,p_column_heading=>'Setup POD'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:4:&SESSION.::&DEBUG.::P4_PARTY_ID:#PARTY_ID#'
,p_column_linktext=>'#SETUP_POD#'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510512262639557)
,p_query_column_id=>14
,p_column_alias=>'NOTIFY_ACCOUNT_MGR'
,p_column_display_sequence=>20
,p_column_heading=>'Notify Account Mgr'
,p_disable_sort_column=>'N'
,p_print_col_width=>'2'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510609829639557)
,p_query_column_id=>15
,p_column_alias=>'NOTIFICATION_EMAIL'
,p_column_display_sequence=>22
,p_column_heading=>'Notification Email'
,p_hidden_column=>'Y'
,p_print_col_width=>'2'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510730179639557)
,p_query_column_id=>16
,p_column_alias=>'STATUS'
,p_column_display_sequence=>21
,p_column_heading=>'Status'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510807652639557)
,p_query_column_id=>17
,p_column_alias=>'COMMENTS'
,p_column_display_sequence=>23
,p_column_heading=>'Comments'
,p_disable_sort_column=>'N'
,p_print_col_width=>'2'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2510904190639557)
,p_query_column_id=>18
,p_column_alias=>'START_DATE_ACTIVE'
,p_column_display_sequence=>13
,p_column_heading=>'Start Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'START_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2511003515639557)
,p_query_column_id=>19
,p_column_alias=>'END_DATE_ACTIVE'
,p_column_display_sequence=>14
,p_column_heading=>'End Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'2'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'END_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2511106569639557)
,p_query_column_id=>20
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>15
,p_column_heading=>'Creation Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATION_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2511201778639557)
,p_query_column_id=>21
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>16
,p_column_heading=>'Created By'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATED_BY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2511307467639558)
,p_query_column_id=>22
,p_column_alias=>'LAST_UPDATE_DATE'
,p_column_display_sequence=>17
,p_column_heading=>'Last Update Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATE_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2511427426639558)
,p_query_column_id=>23
,p_column_alias=>'LAST_UPDATED_BY'
,p_column_display_sequence=>19
,p_column_heading=>'Last Updated By'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATED_BY'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2512308133639560)
,p_plug_name=>'SetUp new B2B Trading Partner - Approval Button'
,p_parent_plug_id=>wwv_flow_api.id(2509129456639552)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>55
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2512702588639564)
,p_plug_name=>'SetUp new B2B Trading Partner'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM dual where :P3_USER_ROLE IN (''2'',''3'')'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(2514708470639569)
,p_name=>'SetUp new B2B Trading Partner - SalesRep'
,p_parent_plug_id=>wwv_flow_api.id(2512702588639564)
,p_template=>wwv_flow_api.id(24617707682658552)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"PARTY_ID",',
'"PARTY_ID" PARTY_ID_DISPLAY,',
'"PARTY_NAME",',
'"PARTY_NUMBER",',
'"TP_NAME",',
'"DELIVER_SOA",',
'"DELIVER_ASN",',
'''[SETUP_POD]'' "SETUP_POD",',
'DECODE(DELIVER_INVOICE, ''Y'', ''Yes'', ''No'')     "DELIVER_INVOICE",',
'DECODE(DELIVER_POA, ''Y'', ''Yes'', ''No'')         "DELIVER_POA",',
'"SOA_EMAIL",',
'"ASN_EMAIL",',
'"NOTIFY_ACCOUNT_MGR",',
'"NOTIFICATION_EMAIL",',
'NVL(STATUS, ''DRAFT'') "STATUS",',
'"START_DATE_ACTIVE",',
'"END_DATE_ACTIVE",',
'"CREATION_DATE",',
'"CREATED_BY",',
'"LAST_UPDATE_DATE",',
'"LAST_UPDATED_BY"',
'from XXWC.XXWC_B2B_CUST_INFO_TBL',
'WHERE PARTY_ID = :P3_PARTY_ID'))
,p_source_type=>'NATIVE_TABFORM'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P3_USER_ROLE = ''2'''
,p_display_condition_type=>'EXISTS'
,p_translate_title=>'N'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>wwv_flow_string.join(wwv_flow_t_varchar2(
'B2B is not setup for this Trading Partner. ',
'Please click "Add Row" button to create.'))
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2514919464639569)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'Check$01'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515004485639569)
,p_query_column_id=>2
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Party Id'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515105090639570)
,p_query_column_id=>3
,p_column_alias=>'PARTY_ID_DISPLAY'
,p_column_display_sequence=>5
,p_column_heading=>'Party Id Display'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515222475639570)
,p_query_column_id=>4
,p_column_alias=>'PARTY_NAME'
,p_column_display_sequence=>2
,p_column_heading=>'Party Name'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2532531804639613)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_name||'' ''||party_number d, party_name r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_column_width=>360
,p_column_default=>'P3_PARTY_NAME'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515331469639570)
,p_query_column_id=>5
,p_column_alias=>'PARTY_NUMBER'
,p_column_display_sequence=>4
,p_column_heading=>'Party Number'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2532704364639613)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_number d, party_number r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'P3_PARTY_NUMBER'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NUMBER'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515418031639570)
,p_query_column_id=>6
,p_column_alias=>'TP_NAME'
,p_column_display_sequence=>19
,p_column_heading=>'Tp Name'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515511263639570)
,p_query_column_id=>7
,p_column_alias=>'DELIVER_SOA'
,p_column_display_sequence=>7
,p_column_heading=>'Deliver SOA'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'N'
,p_lov_null_value=>'N'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_SOA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515601204639570)
,p_query_column_id=>8
,p_column_alias=>'DELIVER_ASN'
,p_column_display_sequence=>9
,p_column_heading=>'Deliver ASN'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_ASN'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3320104390878893)
,p_query_column_id=>9
,p_column_alias=>'SETUP_POD'
,p_column_display_sequence=>12
,p_column_heading=>'Setup POD'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:4:&SESSION.::&DEBUG.::P4_PARTY_ID:#PARTY_ID#'
,p_column_linktext=>'#SETUP_POD#'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515705872639570)
,p_query_column_id=>10
,p_column_alias=>'DELIVER_INVOICE'
,p_column_display_sequence=>11
,p_column_heading=>'Deliver Invoice'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_INVOICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515800576639570)
,p_query_column_id=>11
,p_column_alias=>'DELIVER_POA'
,p_column_display_sequence=>6
,p_column_heading=>'Deliver POA'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_POA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2515926233639570)
,p_query_column_id=>12
,p_column_alias=>'SOA_EMAIL'
,p_column_display_sequence=>8
,p_column_heading=>'SOA Email'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516012610639570)
,p_query_column_id=>13
,p_column_alias=>'ASN_EMAIL'
,p_column_display_sequence=>10
,p_column_heading=>'ASN Email'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516131584639571)
,p_query_column_id=>14
,p_column_alias=>'NOTIFY_ACCOUNT_MGR'
,p_column_display_sequence=>20
,p_column_heading=>'Notify Account Mgr'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'NOTIFY_ACCOUNT_MGR'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516231231639571)
,p_query_column_id=>15
,p_column_alias=>'NOTIFICATION_EMAIL'
,p_column_display_sequence=>21
,p_column_heading=>'Additional Email'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516300316639571)
,p_query_column_id=>16
,p_column_alias=>'STATUS'
,p_column_display_sequence=>22
,p_column_heading=>'Status'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516410098639571)
,p_query_column_id=>17
,p_column_alias=>'START_DATE_ACTIVE'
,p_column_display_sequence=>13
,p_column_heading=>'Start Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'START_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516505957639571)
,p_query_column_id=>18
,p_column_alias=>'END_DATE_ACTIVE'
,p_column_display_sequence=>14
,p_column_heading=>'End Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'END_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516614271639571)
,p_query_column_id=>19
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>15
,p_column_heading=>'Creation Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATION_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516731707639571)
,p_query_column_id=>20
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>16
,p_column_heading=>'Created By'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATED_BY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516803225639571)
,p_query_column_id=>21
,p_column_alias=>'LAST_UPDATE_DATE'
,p_column_display_sequence=>17
,p_column_heading=>'Last Update Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATE_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2516915312639571)
,p_query_column_id=>22
,p_column_alias=>'LAST_UPDATED_BY'
,p_column_display_sequence=>18
,p_column_heading=>'Last Updated By'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATED_BY'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2518832366639576)
,p_plug_name=>'Questionaire Region - SalesRep'
,p_parent_plug_id=>wwv_flow_api.id(2512702588639564)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P3_USER_ROLE = ''2'''
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2520028094639579)
,p_plug_name=>'Notes Region'
,p_parent_plug_id=>wwv_flow_api.id(2512702588639564)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_region_attributes=>'STYLE="border: 1.5px solid rgb(204, 204, 204); width:990px; padding: 2px;"'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>65
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid=>true
,p_plug_grid_column_span=>20
,p_plug_display_column=>2
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(2521225444639583)
,p_name=>'SetUp new B2B Trading Partner - Admin'
,p_parent_plug_id=>wwv_flow_api.id(2512702588639564)
,p_template=>wwv_flow_api.id(24617707682658552)
,p_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"PARTY_ID",',
'"PARTY_ID" PARTY_ID_DISPLAY,',
'"PARTY_NAME",',
'"PARTY_NUMBER",',
'"TP_NAME",',
'"DELIVER_SOA",',
'"DELIVER_ASN",',
'"DELIVER_INVOICE",',
'''[SETUP_POD]'' "SETUP_POD",',
'"DELIVER_POA",',
'"SOA_EMAIL",',
'"ASN_EMAIL",',
'"NOTIFY_ACCOUNT_MGR",',
'"NOTIFICATION_EMAIL",',
'NVL(STATUS, ''DRAFT'') "STATUS",',
'"START_DATE_ACTIVE",',
'"END_DATE_ACTIVE",',
'"CREATION_DATE",',
'"CREATED_BY",',
'"LAST_UPDATE_DATE",',
'"LAST_UPDATED_BY"',
'from XXWC.XXWC_B2B_CUST_INFO_TBL',
'WHERE PARTY_ID = :P3_PARTY_ID'))
,p_source_type=>'NATIVE_TABFORM'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P3_USER_ROLE = ''3'''
,p_display_condition_type=>'EXISTS'
,p_translate_title=>'N'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>wwv_flow_string.join(wwv_flow_t_varchar2(
'B2B is not setup for this Trading Partner. ',
'Please click "Add Row" button to create.'))
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
end;
/
begin
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2521419651639584)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'Check$01'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2521502568639584)
,p_query_column_id=>2
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Party Id'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2521601651639584)
,p_query_column_id=>3
,p_column_alias=>'PARTY_ID_DISPLAY'
,p_column_display_sequence=>4
,p_column_heading=>'Party Id Display'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2521717172639584)
,p_query_column_id=>4
,p_column_alias=>'PARTY_NAME'
,p_column_display_sequence=>2
,p_column_heading=>'Party Name'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2532531804639613)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_name||'' ''||party_number d, party_name r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_column_width=>360
,p_column_default=>'P3_PARTY_NAME'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2521803616639584)
,p_query_column_id=>5
,p_column_alias=>'PARTY_NUMBER'
,p_column_display_sequence=>5
,p_column_heading=>'Party Number'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2532704364639613)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_number d, party_number r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'P3_PARTY_NUMBER'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NUMBER'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2521911107639585)
,p_query_column_id=>6
,p_column_alias=>'TP_NAME'
,p_column_display_sequence=>18
,p_column_heading=>'Tp Name'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522014495639585)
,p_query_column_id=>7
,p_column_alias=>'DELIVER_SOA'
,p_column_display_sequence=>7
,p_column_heading=>'Deliver SOA'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'N'
,p_lov_null_value=>'N'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_SOA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522110426639585)
,p_query_column_id=>8
,p_column_alias=>'DELIVER_ASN'
,p_column_display_sequence=>9
,p_column_heading=>'Deliver ASN'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_ASN'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522221027639585)
,p_query_column_id=>9
,p_column_alias=>'DELIVER_INVOICE'
,p_column_display_sequence=>11
,p_column_heading=>'Deliver Invoice'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_INVOICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3322518100880531)
,p_query_column_id=>10
,p_column_alias=>'SETUP_POD'
,p_column_display_sequence=>12
,p_column_heading=>'Setup POD'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:4:&SESSION.::&DEBUG.::P4_PARTY_ID:#PARTY_ID#'
,p_column_linktext=>'#SETUP_POD#'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522324038639585)
,p_query_column_id=>11
,p_column_alias=>'DELIVER_POA'
,p_column_display_sequence=>6
,p_column_heading=>'Deliver POA'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_POA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522401153639585)
,p_query_column_id=>12
,p_column_alias=>'SOA_EMAIL'
,p_column_display_sequence=>8
,p_column_heading=>'SOA Email'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522502191639585)
,p_query_column_id=>13
,p_column_alias=>'ASN_EMAIL'
,p_column_display_sequence=>10
,p_column_heading=>'ASN Email'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522630180639585)
,p_query_column_id=>14
,p_column_alias=>'NOTIFY_ACCOUNT_MGR'
,p_column_display_sequence=>20
,p_column_heading=>'Notify Account Mgr'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'NOTIFY_ACCOUNT_MGR'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522717309639585)
,p_query_column_id=>15
,p_column_alias=>'NOTIFICATION_EMAIL'
,p_column_display_sequence=>21
,p_column_heading=>'Additional Email'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522826505639586)
,p_query_column_id=>16
,p_column_alias=>'STATUS'
,p_column_display_sequence=>22
,p_column_heading=>'Status'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2522920641639586)
,p_query_column_id=>17
,p_column_alias=>'START_DATE_ACTIVE'
,p_column_display_sequence=>13
,p_column_heading=>'Start Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'START_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2523001869639586)
,p_query_column_id=>18
,p_column_alias=>'END_DATE_ACTIVE'
,p_column_display_sequence=>14
,p_column_heading=>'End Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'END_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2523116130639586)
,p_query_column_id=>19
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>15
,p_column_heading=>'Creation Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'systimestamp'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATION_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2523230783639586)
,p_query_column_id=>20
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>16
,p_column_heading=>'Created By'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATED_BY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2523306627639586)
,p_query_column_id=>21
,p_column_alias=>'LAST_UPDATE_DATE'
,p_column_display_sequence=>17
,p_column_heading=>'Last Update Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATE_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2523407537639586)
,p_query_column_id=>22
,p_column_alias=>'LAST_UPDATED_BY'
,p_column_display_sequence=>19
,p_column_heading=>'Last Updated By'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATED_BY'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(2523730428639587)
,p_name=>'SetUp new B2B Trading Partner - ReadOnly'
,p_template=>wwv_flow_api.id(24617707682658552)
,p_display_sequence=>35
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"PARTY_ID",',
'"PARTY_ID" PARTY_ID_DISPLAY,',
'"PARTY_NAME",',
'"PARTY_NUMBER",',
'"TP_NAME",',
'DECODE(DELIVER_SOA, ''Y'', ''Yes'', ''No'')         "DELIVER_SOA",',
'DECODE(DELIVER_ASN, ''Y'', ''Yes'', ''No'')         "DELIVER_ASN",',
'DECODE(DELIVER_INVOICE, ''Y'', ''Yes'', ''No'')     "DELIVER_INVOICE",',
'DECODE(DELIVER_POA, ''Y'', ''Yes'', ''No'')         "DELIVER_POA",',
'''[SETUP_POD]'' "SETUP_POD",',
'"SOA_EMAIL",',
'"ASN_EMAIL",',
'DECODE(NOTIFY_ACCOUNT_MGR, ''Y'', ''Yes'', ''No'') "NOTIFY_ACCOUNT_MGR",',
'"NOTIFICATION_EMAIL",',
'NVL(STATUS, ''DRAFT'') "STATUS",',
'"START_DATE_ACTIVE",',
'"END_DATE_ACTIVE",',
'"CREATION_DATE",',
'"CREATED_BY",',
'"LAST_UPDATE_DATE",',
'"LAST_UPDATED_BY"',
'from XXWC.XXWC_B2B_CUST_INFO_TBL',
'WHERE PARTY_ID = :P3_PARTY_ID'))
,p_source_type=>'NATIVE_TABFORM'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P3_USER_ROLE = ''1'''
,p_display_condition_type=>'EXISTS'
,p_translate_title=>'N'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_break_cols=>'0'
,p_query_no_data_found=>wwv_flow_string.join(wwv_flow_t_varchar2(
'B2B is not setup for this Trading Partner. ',
'Please click "Add Row" button to create.'))
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525307148639589)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'Check$01'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525404213639589)
,p_query_column_id=>2
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Party Id'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525504133639589)
,p_query_column_id=>3
,p_column_alias=>'PARTY_ID_DISPLAY'
,p_column_display_sequence=>3
,p_column_heading=>'Party Id Display'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525623109639589)
,p_query_column_id=>4
,p_column_alias=>'PARTY_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Party Name'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2532531804639613)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_name||'' ''||party_number d, party_name r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_column_width=>360
,p_column_default=>'P3_PARTY_NAME'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525721201639589)
,p_query_column_id=>5
,p_column_alias=>'PARTY_NUMBER'
,p_column_display_sequence=>5
,p_column_heading=>'Party Number'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2532704364639613)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_number d, party_number r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'P3_PARTY_NUMBER'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NUMBER'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525814686639589)
,p_query_column_id=>6
,p_column_alias=>'TP_NAME'
,p_column_display_sequence=>18
,p_column_heading=>'Tp Name'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525906479639590)
,p_query_column_id=>7
,p_column_alias=>'DELIVER_SOA'
,p_column_display_sequence=>7
,p_column_heading=>'Deliver SOA'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'N'
,p_lov_null_value=>'N'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_SOA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2523912093639587)
,p_query_column_id=>8
,p_column_alias=>'DELIVER_ASN'
,p_column_display_sequence=>9
,p_column_heading=>'Deliver ASN'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_ASN'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524025185639588)
,p_query_column_id=>9
,p_column_alias=>'DELIVER_INVOICE'
,p_column_display_sequence=>11
,p_column_heading=>'Deliver Invoice'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_INVOICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524110894639588)
,p_query_column_id=>10
,p_column_alias=>'DELIVER_POA'
,p_column_display_sequence=>6
,p_column_heading=>'Deliver POA'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_POA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3324908859882753)
,p_query_column_id=>11
,p_column_alias=>'SETUP_POD'
,p_column_display_sequence=>12
,p_column_heading=>'Setup POD'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:4:&SESSION.::&DEBUG.::P4_PARTY_ID:#PARTY_ID#'
,p_column_linktext=>'#SETUP_POD#'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524208443639588)
,p_query_column_id=>12
,p_column_alias=>'SOA_EMAIL'
,p_column_display_sequence=>8
,p_column_heading=>'SOA Email'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524326038639588)
,p_query_column_id=>13
,p_column_alias=>'ASN_EMAIL'
,p_column_display_sequence=>10
,p_column_heading=>'ASN Email'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524429665639588)
,p_query_column_id=>14
,p_column_alias=>'NOTIFY_ACCOUNT_MGR'
,p_column_display_sequence=>20
,p_column_heading=>'Notify Account Mgr'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524521417639588)
,p_query_column_id=>15
,p_column_alias=>'NOTIFICATION_EMAIL'
,p_column_display_sequence=>22
,p_column_heading=>'Additional Email'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524603869639588)
,p_query_column_id=>16
,p_column_alias=>'STATUS'
,p_column_display_sequence=>21
,p_column_heading=>'Status'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524711899639588)
,p_query_column_id=>17
,p_column_alias=>'START_DATE_ACTIVE'
,p_column_display_sequence=>13
,p_column_heading=>'Start Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'START_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524804719639588)
,p_query_column_id=>18
,p_column_alias=>'END_DATE_ACTIVE'
,p_column_display_sequence=>14
,p_column_heading=>'End Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'3'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'END_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2524922513639588)
,p_query_column_id=>19
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>15
,p_column_heading=>'Creation Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATION_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525000235639589)
,p_query_column_id=>20
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>16
,p_column_heading=>'Created By'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATED_BY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525127838639589)
,p_query_column_id=>21
,p_column_alias=>'LAST_UPDATE_DATE'
,p_column_display_sequence=>17
,p_column_heading=>'Last Update Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>30
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATE_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2525209536639589)
,p_query_column_id=>22
,p_column_alias=>'LAST_UPDATED_BY'
,p_column_display_sequence=>19
,p_column_heading=>'Last Updated By'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATED_BY'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2523517918639586)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(2521225444639583)
,p_button_name=>'ADD_ADMIN'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_execute_validations=>'N'
,p_button_condition=>'SELECT ''1'' FROM xxwc.xxwc_b2b_cust_info_tbl where party_id = :P3_PARTY_ID'
,p_button_condition_type=>'NOT_EXISTS'
,p_grid_new_grid=>false
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2517016128639571)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_api.id(2514708470639569)
,p_button_name=>'ADD_SR'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_execute_validations=>'N'
,p_button_condition=>'SELECT ''1'' FROM xxwc.xxwc_b2b_cust_info_tbl where party_id = :P3_PARTY_ID'
,p_button_condition_type=>'NOT_EXISTS'
,p_grid_new_grid=>false
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2512902754639565)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(2512702588639564)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Submit'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2513101117639565)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(2512702588639564)
,p_button_name=>'CANCEL'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2526016171639590)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(2523730428639587)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:1::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2511513551639558)
,p_button_sequence=>70
,p_button_plug_id=>wwv_flow_api.id(2509129456639552)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:1::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2511727891639558)
,p_button_sequence=>90
,p_button_plug_id=>wwv_flow_api.id(2509129456639552)
,p_button_name=>'REJECT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Reject'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2511903327639559)
,p_button_sequence=>80
,p_button_plug_id=>wwv_flow_api.id(2509129456639552)
,p_button_name=>'APPROVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Approve'
,p_button_position=>'REGION_TEMPLATE_PREVIOUS'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(2530909430639609)
,p_branch_action=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:1::'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'REQUEST_EQUALS_CONDITION'
,p_branch_condition=>'CANCEL'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(2531127928639610)
,p_branch_action=>'f?p=&APP_ID.:3:&SESSION.::&DEBUG.::P3_PARTY_ID:&P3_PARTY_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2512127635639559)
,p_name=>'P3_USER_NAME_AR'
,p_item_sequence=>91
,p_item_plug_id=>wwv_flow_api.id(2509129456639552)
,p_use_cache_before_default=>'NO'
,p_source=>'SELECT REPLACE(DESCRIPTION,'','', '''') FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER)'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2512519369639560)
,p_name=>'P3_USER_COMMENTS'
,p_item_sequence=>78
,p_item_plug_id=>wwv_flow_api.id(2512308133639560)
,p_prompt=>'Comments'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>60
,p_cMaxlength=>4000
,p_cHeight=>6
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621516577658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2513308518639565)
,p_name=>'P3_END_DATE_ACTIVE'
,p_item_sequence=>82
,p_item_plug_id=>wwv_flow_api.id(2512702588639564)
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2513508490639566)
,p_name=>'P3_NTID'
,p_item_sequence=>83
,p_item_plug_id=>wwv_flow_api.id(2512702588639564)
,p_source=>'SELECT USER_NAME FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER)'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2513707426639566)
,p_name=>'P3_SYSDATE'
,p_item_sequence=>22
,p_item_plug_id=>wwv_flow_api.id(2512702588639564)
,p_use_cache_before_default=>'NO'
,p_item_default=>'SYSDATE'
,p_source=>'SELECT SYSDATE FROM DUAL'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2513914967639567)
,p_name=>'P3_PARTY_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(2512702588639564)
,p_use_cache_before_default=>'NO'
,p_source=>'PARTY_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2514114902639567)
,p_name=>'P3_USER_ROLE'
,p_item_sequence=>68
,p_item_plug_id=>wwv_flow_api.id(2512702588639564)
,p_item_default=>'3'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2514314555639568)
,p_name=>'P3_USER_NAME'
,p_item_sequence=>81
,p_item_plug_id=>wwv_flow_api.id(2512702588639564)
,p_source=>'SELECT REPLACE(DESCRIPTION,'','', '''') FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER)'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2514517536639568)
,p_name=>'P3_INSTANCE'
,p_item_sequence=>101
,p_item_plug_id=>wwv_flow_api.id(2512702588639564)
,p_use_cache_before_default=>'NO'
,p_source=>'SELECT SUBSTR (UPPER (global_name), 1, 7) FROM global_name'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2517209946639572)
,p_name=>'P3_PARTY_NUMBER'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(2514708470639569)
,p_source=>'PARTY_NUMBER'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2517419018639572)
,p_name=>'P3_PARTY_NAME'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(2514708470639569)
,p_source=>'PARTY_NAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2519023418639576)
,p_name=>'P3_SALESREP'
,p_item_sequence=>27
,p_item_plug_id=>wwv_flow_api.id(2518832366639576)
,p_prompt=>'Select Account Manager'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'SALESREP'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT REPLACE(jrre.source_name, '','', '''') d, jrre.user_name r',
'  FROM apps.hz_cust_acct_sites_all     hcas',
'     , apps.hz_cust_accounts_all       hca',
'     , apps.hz_cust_site_uses_all      hcsu',
'     , apps.jtf_rs_salesreps           jrs',
'     , apps.jtf_rs_resource_extns      jrre',
' WHERE 1 = 1',
'   AND hcas.cust_account_id     = hca.cust_account_id',
'   AND hca.party_id             = :P3_PARTY_ID',
'   AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id',
'   AND hcsu.primary_salesrep_id = jrs.salesrep_id',
'   AND jrs.resource_id          = jrre.resource_id',
'   AND jrs.org_id               = hcas.org_id',
'   AND jrre.user_name           IS NOT NULL',
'   AND jrre.source_name         IS NOT NULL',
'   AND hcas.status              = ''A''',
'   AND hcsu.status              = ''A''',
'   AND hcas.org_id              = 162',
'   AND hcsu.org_id              = 162',
'   AND jrs.org_id              = 162',
'   AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)'))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2519227730639577)
,p_name=>'P3_SALESREP_VALUE'
,p_item_sequence=>111
,p_item_plug_id=>wwv_flow_api.id(2518832366639576)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Salesrep Value'
,p_source=>'P3_SALESREP'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2519427906639577)
,p_name=>'P3_NOTIFY_ACCT_MGRS'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(2518832366639576)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P3_NOTIFY_ACCT_MGRS'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'If yes, have you notified All Account Managers ?'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'YN2'
,p_lov=>'.'||wwv_flow_api.id(2531513372639611)||'.'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'--'
,p_lov_null_value=>'-1'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'SELECT ''1'' FROM xxwc.xxwc_b2b_cust_info_tbl where party_id = :P3_PARTY_ID'
,p_display_when_type=>'NOT_EXISTS'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2519625075639578)
,p_name=>'P3_MULTIPLE_ACCT_MGRS'
,p_item_sequence=>35
,p_item_plug_id=>wwv_flow_api.id(2518832366639576)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P3_MULTIPLE_ACCT_MGRS'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'Is your account shared by multiple Account Managers ?'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'YN2'
,p_lov=>'.'||wwv_flow_api.id(2531513372639611)||'.'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'--'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'SELECT ''1'' FROM xxwc.xxwc_b2b_cust_info_tbl where party_id = :P3_PARTY_ID'
,p_display_when_type=>'NOT_EXISTS'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2519823799639578)
,p_name=>'P3_PARTY_ID_1'
,p_item_sequence=>58
,p_item_plug_id=>wwv_flow_api.id(2518832366639576)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2520209506639579)
,p_name=>'P3_REMINDER'
,p_item_sequence=>35
,p_item_plug_id=>wwv_flow_api.id(2520028094639579)
,p_pre_element_text=>'<BR><B><U> Reminder! </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>* Ensure email addresses exist for Bill To and Ship To contacts in the XXWC Customer Contacts Form.</BR>',
'<BR>* Visit the Sales Resource Toolbox, or 20/20 Training webpage, to learn how to add Oracle contact email address in the XXWC Contacts Form.</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2520416337639580)
,p_name=>'P3_ADDTL_EMAIL'
,p_item_sequence=>34
,p_item_plug_id=>wwv_flow_api.id(2520028094639579)
,p_pre_element_text=>'<BR><B><U> Additional Email Field </U></B></BR>'
,p_post_element_text=>'<BR>* This field is optional. Enter email addresses for other contacts (e.g. Inside Sales) to receive copies of every SOA/ASN Push Notification.</BR>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2520604149639580)
,p_name=>'P3_SOA_NOTE'
,p_item_sequence=>31
,p_item_plug_id=>wwv_flow_api.id(2520028094639579)
,p_pre_element_text=>'<BR><B><U> Sales Order Acknowledgment (SOA) </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>* Upon Sales Order booking, SOA is sent via email to Bill To contact and/or "SOA Email" </BR>',
'<BR>* If Bill To contact email does not exist and Ship To contact email does, SOA will be sent to Ship To contact.</BR>',
'<BR>* The "SOA Email" address is optional, and meant to be different than the customer''s usual Bill To contacts.</BR><BR></BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2520826806639580)
,p_name=>'P3_ASN_NOTE'
,p_item_sequence=>32
,p_item_plug_id=>wwv_flow_api.id(2520028094639579)
,p_pre_element_text=>'<BR><B><U> Advance Shipment Notice (ASN) </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>* Upon printing Deliver Documents, ASN is sent via email to the Ship To and Bill To contacts, and/or "ASN Email" </BR>',
'<BR>* If no email address exists in Oracle for a contact, or on the B2B Set-up Table, the document will not be sent. </BR>',
'<BR>* The "ASN Email" address is optional, and meant to be different than the customer''s usual Ship To contacts </BR><BR></BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2521026276639583)
,p_name=>'P3_ACCT_MGR'
,p_item_sequence=>33
,p_item_plug_id=>wwv_flow_api.id(2520028094639579)
,p_pre_element_text=>'<BR><B><U> Notify Account Manager </U></B></BR>'
,p_post_element_text=>'<BR>* This field is optional.  When select "yes" <U>all</U> AM''s associated with the Customer Account will receive copies of every SOA/ASN Push Notification.</BR><BR></BR>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(2517730364639573)
,p_tabular_form_region_id=>wwv_flow_api.id(2514708470639569)
,p_validation_name=>'PARTY_NAME is not NULL'
,p_validation_sequence=>30
,p_validation=>'PARTY_NAME'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'PARTY NAME must have a valid value.'
,p_always_execute=>'N'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_api.id(5220410291798163)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'PARTY_NAME'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(2517916180639574)
,p_tabular_form_region_id=>wwv_flow_api.id(2514708470639569)
,p_validation_name=>'PARTY_NUMBER is not null'
,p_validation_sequence=>40
,p_validation=>'PARTY_NUMBER'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'PARTY NUMBER must have a valid value.'
,p_always_execute=>'N'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_api.id(5220410291798163)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'PARTY_NUMBER'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(2518128488639574)
,p_tabular_form_region_id=>wwv_flow_api.id(2514708470639569)
,p_validation_name=>'START_DATE_ACTIVE must be a valid date'
,p_validation_sequence=>80
,p_validation=>'START_DATE_ACTIVE'
,p_validation_type=>'ITEM_IS_DATE'
,p_error_message=>'#COLUMN_HEADER# must be a valid date.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(5220410291798163)
,p_exec_cond_for_each_row=>'N'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'START_DATE_ACTIVE'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(2518324584639575)
,p_tabular_form_region_id=>wwv_flow_api.id(2514708470639569)
,p_validation_name=>'END_DATE_ACTIVE must be a valid date'
,p_validation_sequence=>90
,p_validation=>'END_DATE_ACTIVE'
,p_validation_type=>'ITEM_IS_DATE'
,p_error_message=>'#COLUMN_HEADER# must be a valid date.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(5220410291798163)
,p_exec_cond_for_each_row=>'N'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'END_DATE_ACTIVE'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(2527801258639602)
,p_name=>'Multiple Acct Managers - Yes'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P3_MULTIPLE_ACCT_MGRS'
,p_condition_element=>'P3_MULTIPLE_ACCT_MGRS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'Y'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2528118035639604)
,p_event_id=>wwv_flow_api.id(2527801258639602)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P3_NOTIFY_ACCT_MGRS'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2528310750639604)
,p_event_id=>wwv_flow_api.id(2527801258639602)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P3_NOTIFY_ACCT_MGRS'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(2528419014639604)
,p_name=>'Multiple Acct Managers - No'
,p_event_sequence=>15
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P3_MULTIPLE_ACCT_MGRS'
,p_condition_element=>'P3_MULTIPLE_ACCT_MGRS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'N'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2528721044639605)
,p_event_id=>wwv_flow_api.id(2528419014639604)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(2514708470639569)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2528903919639605)
,p_event_id=>wwv_flow_api.id(2528419014639604)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(2514708470639569)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(2529004135639606)
,p_name=>'Notify Acct Managers - Change'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P3_NOTIFY_ACCT_MGRS'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'$v(''P3_NOTIFY_ACCT_MGRS'') == $v(''P3_MULTIPLE_ACCT_MGRS'')'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2529315897639606)
,p_event_id=>wwv_flow_api.id(2529004135639606)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(2514708470639569)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2529512066639606)
,p_event_id=>wwv_flow_api.id(2529004135639606)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(2514708470639569)
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(2529629492639607)
,p_name=>'Find User Role'
,p_event_sequence=>30
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2529907364639607)
,p_event_id=>wwv_flow_api.id(2529629492639607)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'l_salesrep_flag            VARCHAR2(1);',
'l_user_name                VARCHAR2(30);',
'l_instance                 VARCHAR2 (50);',
'BEGIN',
'',
'IF :P3_INSTANCE IS NULL THEN',
'BEGIN',
'   SELECT SUBSTR (UPPER (global_name), 1, 7)',
'      INTO l_instance',
'     FROM global_name;',
'EXCEPTION',
'WHEN OTHERS THEN',
'   l_instance := NULL;',
'END;',
'',
':P3_INSTANCE := l_instance;',
'END IF;',
'',
'IF :P3_USER_ROLE IN (''1'', ''2'') THEN',
'',
'l_user_name := nvl(v(''APP_USER''),USER);',
'',
'SELECT apps.xxwc_b2b_so_ib_pkg.is_sales_rep(:P3_PARTY_ID, l_user_name)',
'  INTO l_salesrep_flag',
'  FROM dual;',
'',
'IF l_salesrep_flag IN (''Y'', ''0'') THEN ',
'   :P3_USER_ROLE := ''2'';',
'ELSE',
'   :P3_USER_ROLE := ''1'';',
'END IF;',
'',
'-- dbms_output.put_line(''l_salesrep_flag - ''||l_salesrep_flag);',
'END IF;',
'',
'EXCEPTION',
'WHEN OTHERS THEN',
'dbms_output.put_line(''Error - ''||SQLERRM);',
'END;'))
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(2530030790639607)
,p_name=>'Acct Managers - Change By User'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P3_SALESREP'
,p_condition_element=>'P3_SALESREP'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2530320674639608)
,p_event_id=>wwv_flow_api.id(2530030790639607)
,p_event_result=>'TRUE'
,p_action_sequence=>5
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   l_ntid         VARCHAR2(30);',
'   l_user_name    VARCHAR2(200);',
'BEGIN',
'   SELECT jrre.user_name',
'        , REPLACE(jrre.source_name, '','', '''')',
'     INTO l_ntid',
'        , l_user_name',
'     FROM APPS.jtf_rs_salesreps           jrs',
'        , APPS.jtf_rs_resource_extns      jrre',
'    WHERE jrs.salesrep_id               = :P3_SALESREP',
'      AND jrs.resource_id               = jrre.resource_id',
'      AND jrs.org_id                    = 162',
'      AND jrre.source_name             IS NOT NULL',
'      AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)',
'      AND ROWNUM = 1',
'      ;',
'   :P3_SALESREP_VALUE := :P3_SALESREP;',
'EXCEPTION',
'WHEN OTHERS THEN',
'  NULL;',
'END;'))
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2530532704639608)
,p_event_id=>wwv_flow_api.id(2530030790639607)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P3_MULTIPLE_ACCT_MGRS'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2530731031639609)
,p_event_id=>wwv_flow_api.id(2530030790639607)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P3_MULTIPLE_ACCT_MGRS'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2526219962639590)
,p_process_sequence=>85
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Find User Role Process'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   l_salesrep_flag VARCHAR2(1);',
'   l_user_name     VARCHAR2(30);',
'   l_end_date_active  DATE;',
'   l_salesrep_name VARCHAR2(200);',
'BEGIN',
'   l_user_name   := nvl(v(''APP_USER''),USER);',
'   ',
'   IF :P3_NTID IS NULL THEN',
'      :P3_NTID := l_user_name;',
'   END IF;',
'',
'   IF :P3_USER_NAME IS NULL THEN',
'      BEGIN',
'      SELECT REPLACE(DESCRIPTION,'','', '''') ',
'        INTO :P3_USER_NAME',
'        FROM APPS.FND_USER WHERE USER_NAME = l_user_name',
'         AND ROWNUM = 1; ',
'      EXCEPTION',
'      WHEN OTHERS THEN',
'         NULL;',
'      END;',
'   END IF;',
'',
'   IF :P3_USER_ROLE IN (''1'', ''2'') THEN',
'      SELECT apps.xxwc_b2b_so_ib_pkg.is_sales_rep(:P3_PARTY_ID, l_user_name)',
'        INTO l_salesrep_flag',
'        FROM dual;',
'',
'      IF l_salesrep_flag IN (''Y'', ''0'') THEN ',
'         :P3_USER_ROLE := ''2'';',
'',
'       IF l_salesrep_flag = ''Y'' THEN ',
'         :P3_SALESREP := l_user_name;',
'       END IF;',
'/*',
'         SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_rep_name(:P3_PARTY_ID, l_user_name)',
'           INTO l_salesrep_name',
'           FROM dual;',
'',
'         IF l_salesrep_name IS NOT NULL THEN',
'           :P3_SALESREP := l_salesrep_name;',
'         END IF;',
'*/',
'      ELSE',
'         :P3_USER_ROLE := ''1'';',
'      END IF;',
'   -- dbms_output.put_line(''l_salesrep_flag - ''||l_salesrep_flag);',
'   END IF;',
'',
'   BEGIN',
'     SELECT end_date_active',
'       INTO l_end_date_active',
'       FROM xxwc.xxwc_b2b_cust_info_tbl',
'      WHERE party_id = :P3_PARTY_ID;',
'',
'     :P3_END_DATE_ACTIVE := l_end_date_active;',
'   EXCEPTION',
'   WHEN OTHERS THEN',
'      NULL;',
'   END;',
'',
'EXCEPTION',
'WHEN OTHERS THEN',
'   dbms_output.put_line(''Error - ''||SQLERRM);',
'END;'))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2526404499639591)
,p_process_sequence=>5
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'MRU - Send Email'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   l_subject              VARCHAR2 (32767) DEFAULT NULL;',
'   l_body                 VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_header          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_detail          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_footer          VARCHAR2 (32767) DEFAULT NULL;',
'   l_instance             VARCHAR2(50);',
'   l_to_mgr_email_id      VARCHAR2 (32767) DEFAULT NULL;',
'   l_to_sr_email_id       VARCHAR2 (32767) DEFAULT NULL;',
'   l_status               VARCHAR2(50);',
'   l_end_date_active      DATE;',
'   l_user_name            VARCHAR2(250) := NVL(:P3_USER_NAME,:P3_NTID);',
'   l_log_in_user          VARCHAR2(250);',
'   l_url                  VARCHAR2(200);',
'   l_to_email             VARCHAR2(32767);',
'   l_dflt_email           VARCHAR2(100) := ''Gopi.Damuluri@HDSupply.com, Sha.Angel@hdsupply.com, Lorraine.Charles@hdsupply.com'';',
'   l_sr_setup_status      VARCHAR2(100);',
'   l_salesrep_name        VARCHAR2(200);',
'',
'BEGIN',
'',
'IF :P3_USER_ROLE = ''2'' AND :P3_NTID != :P3_SALESREP THEN',
'   IF :P3_USER_NAME IS NOT NULL AND :P3_SALESREP IS NOT NULL THEN',
'      l_salesrep_name := apps.xxwc_b2b_so_ib_pkg.get_sales_rep_name(NULL, :P3_SALESREP);',
'      l_user_name := :P3_USER_NAME||''</B> on behalf of - <B>''||l_salesrep_name;',
'   END IF;',
'END IF;',
'',
'/*',
'IF :P3_NTID != nvl(v(''APP_USER''),USER) THEN',
'   l_log_in_user := apps.xxwc_b2b_so_ib_pkg.get_sales_rep_name(NULL, nvl(v(''APP_USER''),USER));',
'   IF l_log_in_user IS NOT NULL THEN',
'      l_user_name := l_log_in_user||''</B> on behalf of - <B>''||l_user_name;',
'   END IF;',
'END IF;',
'*/',
'',
'l_instance  := :P3_INSTANCE;',
'',
'BEGIN',
'SELECT NVL(status, ''DRAFT'')',
'  INTO l_status',
'  FROM xxwc.xxwc_b2b_cust_info_tbl ',
' WHERE party_id = :P3_PARTY_ID',
'   AND ROWNUM = 1;',
'EXCEPTION',
'WHEN NO_DATA_FOUND THEN',
'  l_status := ''NO_DATA'';',
'WHEN OTHERS THEN',
'  l_status := ''DRAFT'';',
'END;',
'',
'-- TMS# 20160404-00040 > Start',
'/*',
'IF :P3_SALESREP IS NULL AND :P3_USER_ROLE = ''2'' THEN',
'   l_sr_setup_status := ''APPROVED'';',
'   l_status          := ''APPROVED'';',
'   apex_application.g_print_success_message := ''Trading Partner is added to B2B.'';',
'ELSE',
'   l_sr_setup_status := ''PENDING_APPROVAL'';',
'END IF;',
'*/',
'-- TMS# 20160404-00040 < End',
'',
'IF l_status = ''DRAFT'' THEN',
'   l_sr_setup_status := ''APPROVED'';',
'   -- l_status          := ''APPROVED'';',
'   apex_application.g_print_success_message := ''Trading Partner is added to B2B.'';',
'',
'-- TMS# 20160404-00040 > Start',
'/*',
'   IF :P3_USER_ROLE = ''3'' THEN',
'      apex_application.g_print_success_message := ''Trading Partner is added to B2B.'';',
'   ELSE',
'      apex_application.g_print_success_message := ''Trading Partner is added to B2B Pending Approval.'';',
'   END IF;',
'*/',
'-- TMS# 20160404-00040 < End',
'ELSE',
'  IF l_status = ''NO_DATA'' THEN',
'     apex_application.g_print_success_message := ''Cannot Proceed.'';',
'  END IF;',
'END IF;',
'',
'',
'/*',
'BEGIN',
'',
'SELECT REPLACE(DESCRIPTION,'','', '''') ',
'INTO l_user_name',
'FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER);',
'',
'EXCEPTION',
'WHEN OTHERS THEN',
'NULL;',
'END;',
'',
'IF l_user_name IS NULL THEN',
'   l_user_name := nvl(v(''APP_USER''),USER);',
'END IF;',
'*/',
'',
'IF :P3_USER_ROLE = ''3'' THEN -- ADMIN',
'     ',
'     l_end_date_active := apex_application.g_f10 (1);',
'     ',
'     ------------------------------------------',
'     -- Update XXWC.XXWC_B2B_CUST_INFO_TBL',
'     ------------------------------------------',
'     UPDATE XXWC.XXWC_B2B_CUST_INFO_TBL',
'        SET DELIVER_POA         = apex_application.g_f03 (1)',
'          , DELIVER_SOA         = apex_application.g_f04 (1)',
'          , SOA_EMAIL           = apex_application.g_f05 (1)',
'          , DELIVER_ASN         = apex_application.g_f06 (1)',
'          , ASN_EMAIL           = apex_application.g_f07 (1)',
'          , DELIVER_INVOICE     = apex_application.g_f08 (1)',
'          , START_DATE_ACTIVE   = apex_application.g_f09 (1)',
'          , END_DATE_ACTIVE     = apex_application.g_f10 (1)',
'          , TP_NAME             = apex_application.g_f11 (1)',
'          , NOTIFY_ACCOUNT_MGR  = apex_application.g_f12 (1)',
'          , NOTIFICATION_EMAIL  = apex_application.g_f13 (1)',
'          , STATUS              = ''APPROVED''',
'      WHERE party_id            = :P3_PARTY_ID;',
' ',
'END IF; -- :P3_USER_ROLE = ''3''',
'',
'IF :P3_USER_ROLE = ''2'' THEN -- SALES PERSON',
'     ',
'     l_end_date_active := apex_application.g_f08 (1);',
'  ',
'     ------------------------------------------',
'     -- Update Status to - PENDING_APPROVAL',
'     ------------------------------------------',
'     UPDATE XXWC.XXWC_B2B_CUST_INFO_TBL',
'        SET DELIVER_SOA         = apex_application.g_f03 (1)',
'          , SOA_EMAIL           = apex_application.g_f04 (1)',
'          , DELIVER_ASN         = apex_application.g_f05 (1)',
'          , ASN_EMAIL           = apex_application.g_f06 (1)',
'          , START_DATE_ACTIVE   = apex_application.g_f07 (1)',
'          , END_DATE_ACTIVE     = apex_application.g_f08 (1)',
'          , NOTIFY_ACCOUNT_MGR  = apex_application.g_f09 (1)',
'          , NOTIFICATION_EMAIL  = apex_application.g_f10 (1)',
'          , STATUS              = DECODE(status, ''APPROVED'', ''APPROVED'', l_sr_setup_status)',
'      WHERE party_id            = :P3_PARTY_ID;',
'',
'-- TMS# 20160404-00040 > Start',
'/*',
'     IF l_status != ''APPROVED'' THEN',
'        --------------------------------------------------',
'        -- Send Approval Email Notification - Manager',
'        --------------------------------------------------',
'        BEGIN',
'           SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_mgr_email (:P3_PARTY_ID, :P3_SALESREP)',
'             INTO l_to_mgr_email_id',
'             FROM DUAL;',
'        EXCEPTION',
'        WHEN OTHERS THEN',
'          l_to_mgr_email_id := NULL;',
'        END;',
'',
'        IF l_to_mgr_email_id IS NOT NULL THEN',
'',
'           --------------------------------------------------',
'           -- Derive Apex on EBS URL',
'           --------------------------------------------------',
'           l_url := apps.xxwc_b2b_so_ib_pkg.get_apex_url;',
'',
'           l_subject     := :P3_PARTY_NAME||'' is Ready for Your Approval for Order Status Update.'';',
'           l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;  color: navy;}</style><p class="style1">'';',
'           l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                            ''<BR> </BR> <BR> A "Order Status Update" request has been submitted for <B>'' ||:P3_PARTY_NAME|| ''</B> by <B>''||l_user_name||''</B>. </BR>''||',
'                            ''<BR> </BR> Please <a href="''||l_url||''&APP_ID.:3:0::NO::P3_PARTY_ID,P3_PARTY_NAME,P3_PARTY_NUMBER,P3_USER_ROLE:''||:P3_PARTY_ID||'',''||:P3_PARTY_NAME||'',''||:P3_PARTY_NUMBER||'',4">APPROVE / REJECT</a> the request.</BR>''',
'                            ;',
'',
'           IF l_instance = ''EBIZPRD'' THEN',
'              l_to_email    := l_to_mgr_email_id;',
'              l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                            ;',
'           ELSE',
'              l_to_email    := l_dflt_email;',
'              l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                               ||''<BR> Email Ids - ''||l_to_mgr_email_id||''</BR> ''',
'                            ;',
'           END IF;',
'',
'           l_body        := l_body_header || l_body_detail ||chr(10)|| l_body_footer;',
'',
'           APEX_MAIL.SEND(p_to                        => l_to_email,',
'                          p_from                      => ''no-reply@whitecap.net'',',
'                          p_body                      => l_body,',
'                          p_body_html                 => l_body,',
'                          p_subj                      => l_subject,',
'                          p_replyto                   => NULL);',
'        END IF;',
'        --------------------------------------------------',
'        -- End Manager Email',
'        --------------------------------------------------',
'     END IF; -- l_status != ''APPROVED''',
'*/',
'-- TMS# 20160404-00040 < End',
'',
'END IF; -- :P3_USER_ROLE = ''2''',
'',
'     --------------------------------------------------',
'     -- Send FYI. Email Notification - SalesRep',
'     --------------------------------------------------',
'     ',
'     BEGIN',
'        SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_rep_email (:P3_PARTY_ID)',
'          INTO l_to_sr_email_id',
'          FROM DUAL;',
'     EXCEPTION',
'     WHEN OTHERS THEN',
'       l_to_sr_email_id := NULL;',
'     END;',
'',
'     IF l_to_sr_email_id IS NOT NULL THEN        ',
'',
'        --------------------------------------------------',
'        -- New Account',
'        --------------------------------------------------',
'        IF l_status = ''DRAFT'' THEN',
'           l_subject     := :P3_PARTY_NAME||'' is Submitted for Order Status Update.'';',
'           l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'           l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                            ''<BR> </BR> <BR> A request to set-up <B>'' ||:P3_PARTY_NAME|| ''</B> for the "Order Status Update" service has been completed. ''',
'                            -- ''<BR> </BR> You will be notified once the request has been approved or rejected by your Manager.''',
'                            ||''<BR> </BR> Submitted by : ''||l_user_name	',
'                            ;',
'',
'        END IF;',
'',
'        --------------------------------------------------',
'        -- Modified Account',
'        --------------------------------------------------',
'        IF l_status != ''DRAFT'' AND TO_DATE(l_end_date_active,''DD-MON-YYYY'') = TO_DATE(:P3_END_DATE_ACTIVE,''DD-MON-YYYY'') ',
'        THEN',
'           l_subject     := :P3_PARTY_NAME||'' is Modified for Order Status Update.'';',
'           l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'           l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                            ''<BR> </BR> <BR> A request to modify <B>'' ||:P3_PARTY_NAME|| ''</B> for the "Order Status Update" service has been completed.''',
'                            ||''<BR> </BR> Modified by : ''||l_user_name',
'                            ;',
'',
'           apex_application.g_print_success_message := ''Trading Partner B2B setup is modified.'';',
'        ',
'        END IF;',
'',
'        --------------------------------------------------',
'        -- Deactivated Account',
'        --------------------------------------------------',
'        IF l_status != ''DRAFT'' AND TO_DATE(l_end_date_active,''DD-MON-YYYY'') != TO_DATE(:P3_END_DATE_ACTIVE,''DD-MON-YYYY'') THEN',
'           l_subject     := :P3_PARTY_NAME||'' is Modified for Order Status Update.'';',
'           l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'           l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                            ''<BR> </BR> <BR> A request to deactivate <B>'' ||:P3_PARTY_NAME|| ''</B> for the "Order Status Update" service has been completed.''',
'                            ||''<BR> </BR> Deactivated by : ''||l_user_name',
'                            ;',
'',
'           apex_application.g_print_success_message := ''Trading Partner B2B setup is deactivated.'';',
'',
'        END IF;',
'',
'        IF l_instance = ''EBIZPRD'' THEN',
'           l_to_email    := l_to_sr_email_id;',
'           l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                         ;',
'        ELSE',
'           l_to_email    := l_dflt_email;',
'           l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                            ||''<BR> Email Ids - ''||l_to_sr_email_id||''</BR> ''',
'                         ;',
'        END IF;',
'        l_body        := l_body_header || l_body_detail ||chr(10)|| l_body_footer;',
'',
'        APEX_MAIL.SEND(p_to                        => l_to_email,',
'                       p_from                      => ''no-reply@whitecap.net'',',
'                       p_body                      => l_body,',
'                       p_body_html                 => l_body,',
'                       p_subj                      => l_subject,',
'                       p_replyto                   => NULL);',
'     END IF;',
'',
'     --------------------------------------------------',
'     -- End Salesrep Email',
'     --------------------------------------------------',
'',
'COMMIT;',
'',
'EXCEPTION',
'WHEN OTHERS THEN',
'NULL;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(2512902754639565)
);
end;
/
begin
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2526610661639593)
,p_process_sequence=>45
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'ApplyMRI Admin'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_CUST_INFO_TBL'
,p_attribute_03=>'P3_PARTY_ID'
,p_attribute_04=>'PARTY_ID'
,p_attribute_11=>'I'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(2523517918639586)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2526808847639594)
,p_process_sequence=>48
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'ApplyMRI SR'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_CUST_INFO_TBL'
,p_attribute_03=>'P3_PARTY_ID'
,p_attribute_04=>'PARTY_ID'
,p_attribute_11=>'I'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(2517016128639571)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2527007270639594)
,p_process_sequence=>55
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Delete '
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'delete from xxwc.xxwc_b2b_cust_info_tbl where party_id = :P3_PARTY_ID AND NVL(status,''DRAFT'') = ''DRAFT'';',
'',
'COMMIT;',
'',
':P3_MULTIPLE_ACCT_MGRS := NULL;',
'',
':P3_NOTIFY_ACCT_MGRS := NULL;',
'',
':P3_USER_COMMENTS := NULL;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'CANCEL'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2527226973639594)
,p_process_sequence=>65
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Approve Button'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'   l_subject              VARCHAR2 (32767) DEFAULT NULL;',
'   l_body                 VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_header          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_detail          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_footer          VARCHAR2 (32767) DEFAULT NULL;',
'   l_instance             VARCHAR2(50) := ''EBZIDEV'';',
'   l_approver_email_id    VARCHAR2 (32767) DEFAULT NULL;',
'   l_user_name            VARCHAR2(250) := NVL(:P3_USER_NAME,:P3_NTID);',
'   l_to_email             VARCHAR2(32767);',
'   l_dflt_email           VARCHAR2(100) := ''Gopi.Damuluri@HDSupply.com, Sha.Angel@hdsupply.com, Lorraine.Charles@hdsupply.com'';',
'BEGIN',
'   l_instance  := :P3_INSTANCE;',
'',
'   UPDATE xxwc.xxwc_b2b_cust_info_tbl ',
'      SET comments = :P3_NTID||'': ''||:P3_USER_COMMENTS||chr(13)||comments',
'        , status = ''APPROVED''',
'    WHERE party_id = :P3_PARTY_ID;',
'',
'   COMMIT;',
'',
'/*',
'SELECT REPLACE(DESCRIPTION,'','', '''') ',
'INTO l_user_name',
'FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER);',
'*/',
'',
'   ------------------------------------------',
'   -- Send Email Notification',
'   ------------------------------------------',
'',
'   BEGIN',
'      SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_rep_email (:P3_PARTY_ID)',
'        INTO l_approver_email_id',
'        FROM DUAL;',
'   EXCEPTION',
'   WHEN OTHERS THEN',
'     l_approver_email_id := NULL;',
'   END;',
'',
'   l_subject     := :P3_PARTY_NAME||'' is Approved for Push Status'';',
'   l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'   l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                    ''<BR> </BR> <BR> Your request to set-up <B>'' ||:P3_PARTY_NAME|| ''</B> for the Push Status service has been APPROVED. </BR>''||',
'                    ''<BR> </BR> <BR> Please promptly notify all impacted branches and sales associates.''||',
'                    ''<BR> </BR> <BR> Approved By : <B> ''||l_user_name||'' </B>''||',
'                    ''<BR> </BR> <BR> Comments :'' ||:P3_USER_COMMENTS;',
'',
'   IF l_instance = ''EBIZPRD'' THEN',
'      l_to_email    := l_approver_email_id;',
'      l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                    ;',
'   ELSE',
'      l_to_email    := l_dflt_email;',
'      l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                       ||''<BR> Email Ids - ''||l_approver_email_id||''</BR> ''',
'                    ;',
'   END IF;',
'   l_body        := l_body_header || l_body_detail ||chr(10)|| l_body_footer;',
'',
'   IF l_to_email IS NOT NULL THEN',
'      APEX_MAIL.SEND(p_to                        => l_to_email,',
'                     p_from                      => ''no-reply@whitecap.net'',',
'                     p_body                      => l_body,',
'                     p_body_html                 => l_body,',
'                     p_subj                      => l_subject,',
'                     p_replyto                   => NULL);',
'   END IF;',
'',
'COMMIT;',
'',
':P3_USER_COMMENTS := NULL;',
'',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(2511903327639559)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2527419397639600)
,p_process_sequence=>75
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Reject Button'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'   l_subject              VARCHAR2 (32767) DEFAULT NULL;',
'   l_body                 VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_header          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_detail          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_footer          VARCHAR2 (32767) DEFAULT NULL;',
'   l_instance             VARCHAR2(50) := ''EBZIDEV'';',
'   l_approver_email_id    VARCHAR2 (32767) DEFAULT NULL;',
'   l_user_name            VARCHAR2(250) := NVL(:P3_USER_NAME,:P3_NTID);',
'   l_to_email             VARCHAR2(32767);',
'   l_dflt_email           VARCHAR2(100) := ''Gopi.Damuluri@HDSupply.com, Sha.Angel@hdsupply.com, Lorraine.Charles@hdsupply.com'';',
'BEGIN',
'   l_instance  := :P3_INSTANCE;',
'',
'   UPDATE xxwc.xxwc_b2b_cust_info_tbl ',
'      SET comments = :P3_NTID||'': ''||:P3_USER_COMMENTS||chr(13)||comments',
'        , status = ''REJECTED''',
'    WHERE party_id = :P3_PARTY_ID;',
'',
'   COMMIT;',
'',
'/*',
'SELECT REPLACE(DESCRIPTION,'','', '''') ',
'INTO l_user_name',
'FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER);',
'*/',
'',
'   ------------------------------------------',
'   -- Send Email Notification',
'   ------------------------------------------',
'',
'   BEGIN',
'      SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_rep_email (:P3_PARTY_ID)',
'        INTO l_approver_email_id',
'        FROM DUAL;',
'   EXCEPTION',
'   WHEN OTHERS THEN',
'     l_approver_email_id := NULL;',
'   END;',
'',
'   l_subject     := :P3_PARTY_NAME||'' is Rejected for Push Status'';',
'   l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'   l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                    ''<BR> </BR> <BR> Your request to set-up <B>'' ||:P3_PARTY_NAME|| ''</B> for the Push Status service has been REJECTED. </BR>''||',
'                    ''<BR> </BR> <BR> Rejected By : <B> ''||l_user_name||'' </B>''||',
'                    ''<BR> </BR> <BR> Comments :'' ||:P3_USER_COMMENTS;',
'',
'   IF l_instance = ''EBIZPRD'' THEN',
'      l_to_email    := l_approver_email_id;',
'      l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                    ;',
'   ELSE',
'      l_to_email    := l_dflt_email;',
'      l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                       ||''<BR> Email Ids - ''||l_approver_email_id||''</BR> ''',
'                    ;',
'   END IF;',
'   l_body        := l_body_header || l_body_detail ||chr(10)|| l_body_footer;',
'',
'   IF l_to_email IS NOT NULL THEN',
'      APEX_MAIL.SEND(p_to                        => l_to_email,',
'                     p_from                      => ''no-reply@whitecap.net'',',
'                     p_body                      => l_body,',
'                     p_body_html                 => l_body,',
'                     p_subj                      => l_subject,',
'                     p_replyto                   => NULL);',
'   END IF;',
'',
'COMMIT;',
'',
':P3_USER_COMMENTS := NULL;',
'',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(2511727891639558)
,p_process_success_message=>'&P3_COMMENTS.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2518423567639575)
,p_process_sequence=>90
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(2514708470639569)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_CUST_INFO_TBL'
,p_attribute_03=>'PARTY_ID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(5220410291798163)
,p_process_when_type=>'NEVER'
,p_process_success_message=>'Trading Partner has been added to B2B'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2518602015639575)
,p_process_sequence=>92
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(2514708470639569)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_CUST_INFO_TBL'
,p_attribute_03=>'PARTY_ID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_type=>'NEVER'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2527608551639601)
,p_process_sequence=>92
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'MRU - Send Email - Old'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'   l_subject              VARCHAR2 (32767) DEFAULT NULL;',
'   l_body                 VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_header          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_detail          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_footer          VARCHAR2 (32767) DEFAULT NULL;',
'   l_instance             VARCHAR2(50) := ''EBZIDEV'';',
'   l_to_mgr_email_id      VARCHAR2 (32767) DEFAULT NULL;',
'   l_to_sr_email_id       VARCHAR2 (32767) DEFAULT NULL;',
'',
'BEGIN',
'',
'IF :P3_USER_ROLE = ''3'' THEN',
'     ------------------------------------------',
'     -- Update Status to - APPROVED',
'     ------------------------------------------',
'     UPDATE xxwc.xxwc_b2b_cust_info_tbl ',
'        SET status = ''APPROVED''',
'      WHERE party_id = :P3_PARTY_ID',
'        AND NVL(status,''DRAFT'') != ''APPROVED'';',
'',
'     COMMIT;',
'END IF; -- :P3_USER_ROLE = ''3''',
'',
'IF :P3_USER_ROLE = ''2'' THEN',
'     ------------------------------------------',
'     -- Update Status to - PENDING_APPROVAL',
'     ------------------------------------------',
'     UPDATE xxwc.xxwc_b2b_cust_info_tbl ',
'        SET status = ''PENDING_APPROVAL''',
'      WHERE party_id = :P3_PARTY_ID',
'        AND NVL(status,''DRAFT'') NOT IN (''PENDING_APPROVAL'', ''APPROVED'');',
'',
'     COMMIT;',
'     --------------------------------------------------',
'     -- Send Approval Email Notification - Manager',
'     --------------------------------------------------',
'     ',
'     BEGIN',
'        SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_mgr_email (:P3_PARTY_ID)',
'          INTO l_to_mgr_email_id',
'          FROM DUAL;',
'     EXCEPTION',
'     WHEN OTHERS THEN',
'       l_to_mgr_email_id := NULL;',
'     END;',
'     ',
'     l_subject     := ''Trading Partner - ''||:P3_PARTY_NAME||'' is setup for B2B.'';',
'     l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;  color: navy;}</style><p class="style1">'';',
'     l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                      ''<BR> </BR> <BR> B2B The Trading Partner <B>'' ||:P3_PARTY_NAME|| ''</B> has been setup on B2B Maintenance Form by <B>''||:P3_USER_NAME||''</B>. </BR>''||',
'                      ''<BR> </BR> Please promptly notify all impacted branches and sales associates and then <a href="http://devebiz.hdsupply.net:7777/pls/ebizdev/f?p=&APP_ID.:3:0::NO::P3_PARTY_ID,P3_PARTY_NAME,P3_PARTY_NUMBER,P3_USER_ROLE:''||:P3_PAR'
||'TY_ID||'',''||:P3_PARTY_NAME||'',''||:P3_PARTY_NUMBER||'',4">APPROVE / REJECT</a> the request.</BR>''',
'                      ;',
'     l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport for any questions. </BR>''',
'                      ||''<BR> Email Ids - ''||l_to_mgr_email_id||''</BR> ''',
'                      ;',
'     l_body        := l_body_header || l_body_detail ||chr(10)|| l_body_footer;',
'',
'     APEX_MAIL.SEND(p_to                        => ''Gopi.Damuluri@HDSupply.com'',',
'                    p_from                      => ''no-reply@whitecap.net'',',
'                    p_body                      => l_body,',
'                    p_body_html                 => l_body,',
'                    p_subj                      => l_subject,',
'                    p_replyto                   => NULL);',
'',
'     --------------------------------------------------',
'     -- End Manager Email',
'     --------------------------------------------------',
'',
'END IF; -- :P3_USER_ROLE = ''2''',
'',
'     --------------------------------------------------',
'     -- Send FYI. Email Notification - SalesRep',
'     --------------------------------------------------',
'     ',
'     BEGIN',
'        SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_rep_email (:P3_PARTY_ID)',
'          INTO l_to_sr_email_id',
'          FROM DUAL;',
'     EXCEPTION',
'     WHEN OTHERS THEN',
'       l_to_sr_email_id := NULL;',
'     END;',
'     ',
'     l_subject     := ''Trading Partner - ''||:P3_PARTY_NAME||'' is setup for B2B.'';',
'     l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'     l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                      ''<BR> </BR> <BR> B2B The Trading Partner <B>'' ||:P3_PARTY_NAME|| ''</B> has been setup on B2B Maintenance Form by <B>''||:P3_USER_NAME||''</B>. </BR>''||',
'                      ''<BR> </BR> Please promptly notify all impacted branches and sales associates''',
'                      ;',
'     l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport for any questions. </BR>''',
'                      ||''<BR> Sale Rep Email Ids - ''||l_to_sr_email_id||''</BR> ''',
'                      ;',
'     l_body        := l_body_header || l_body_detail ||chr(10)|| l_body_footer;',
'',
'     APEX_MAIL.SEND(p_to                        => ''Gopi.Damuluri@HDSupply.com'',',
'                    p_from                      => ''no-reply@whitecap.net'',',
'                    p_body                      => l_body,',
'                    p_body_html                 => l_body,',
'                    p_subj                      => l_subject,',
'                    p_replyto                   => NULL);',
'',
'     --------------------------------------------------',
'     -- End Salesrep Email',
'     --------------------------------------------------',
'',
'EXCEPTION ',
'WHEN OTHERS THEN',
'   NULL;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(5220410291798163)
,p_process_when_type=>'NEVER'
);
end;
/
prompt --application/pages/page_00004
begin
wwv_flow_api.create_page(
 p_id=>4
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'WC B2B POD Maintenance Form'
,p_page_mode=>'NORMAL'
,p_step_title=>'WC B2B POD Maintenance Form'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'WC B2B POD Maintenance Form'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_html_page_header=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<script language="JavaScript" type="text/javascript">',
'<style type="text/css"> .ui-autocomplete-loading{background: url("#JQUERYUI_DIRECTORY#themes/base/images/ui-anim_basic_16x16.gif") no-repeat scroll right center transparent;} </style> ',
'</script>'))
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';',
'',
'(function($){ ',
'     /*** Cascading select list ***/ ',
'     $.fn.htmldbCascade=function(parent,onDemand,options){ ',
'           options=$.extend({ ',
'              trigger       : false, ',
'              extrVal       : false,  ',
'              nullShow      : false,',
'              nullValue     : "",',
'              nullDisplay   : "%",',
'              disAlias      : "DIS",',
'              retAlias      : "RET",',
'              loadingTxt    : "Loading ...",',
'              loadingCss    : {"width":"80px"}   },options);',
'        return this.each(function(i){ ',
'              var self=$(this);  ',
'              var lParent=$(parent).eq(i);',
'              var lSelfVal=self.val();  ',
'              if(!lSelfVal){lSelfVal="";};  ',
'              if(!lParent.data("htmldbCascade")){',
'                  lParent.change(function(){ ',
'                  var lParentVal=$v(this); ',
'                  if(!lParentVal){lParentVal="";};',
'                  $.extend(options.loadingCss,{"height":self.parent().height()});',
'                  self',
'                   .hide()',
'                   .empty()',
'                   .parent()',
'                   .find("div.ui-autocomplete-loading")',
'                   .remove()',
'                   .end()',
'                   .append($("<div/>",{"html":options.loadingTxt,"css":options.loadingCss})',
'                   .addClass("ui-autocomplete-loading"));',
'                   if(options.nullShow){',
'                     appendOpt(self,options.nullDisplay,options.nullValue);      ',
'                     };',
'                    $.post("wwv_flow.show",{',
'                      p_flow_id:"&APP_ID.",',
'                      p_flow_step_id:"&APP_PAGE_ID.",',
'                      p_instance:"&APP_SESSION.",',
'                      p_request:"APPLICATION_PROCESS="+onDemand,',
'                      x01:lParentVal ',
'                      },function(jd){',
'                        var lExists=false;',
'                        $.each(jd.row,function(i,d){',
'                         if(d[options.retAlias]===lSelfVal){lExists=true;};',
'                         appendOpt(self,d[options.disAlias],d[options.retAlias]);',
'                         });       ',
'                         if(options.extrVal&&!lExists){',
'                            appendOpt(self,lSelfVal,lSelfVal);       ',
'                          };       ',
'                          self  ',
'                            .val(lSelfVal)',
'                            .show() ',
'                            .parent() ',
'                            .find("div.ui-autocomplete-loading")',
'                             .remove();       ',
'                           if(options.trigger){self.trigger(options.trigger);};',
'                              },"json");',
'                              }).data("htmldbCascade",true).trigger("change");',
'                               }   ',
'                             });   ',
'                      /*** Append option ***/  ',
'                      function appendOpt(pThis,pDis,pRet){',
'                            pThis.append( $("<option/>",{"html":pDis,"value":pRet}) );   ',
'                            };  ',
'                           };     ',
'                          })(apex.jQuery);'))
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20161107141928'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3305302551865060)
,p_name=>'WC B2B POD Maintenance Form'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"ID",',
'"PARTY_ID",',
'"CUST_ACCOUNT_ID",',
'"ACCOUNT_NUMBER",',
'"ACCOUNT_NAME",',
'"SITE_USE_ID",',
'"NOTIFY_ACCOUNT_MGR",',
'"LOCATION",',
'"DELIVER_POD",',
'"POD_EMAIL",',
'"POD_FREQUENCY",',
'"START_DATE_ACTIVE",',
'"END_DATE_ACTIVE",',
'"POD_LAST_SENT_DATE",',
'"POD_NEXT_SEND_DATE",',
'"CREATION_DATE",',
'"CREATED_BY",',
'"LAST_UPDATE_DATE",',
'"LAST_UPDATED_BY",',
'"ID" ID_DISPLAY',
'from "XXWC"."XXWC_B2B_POD_INFO_TBL"',
'WHERE PARTY_ID = :P4_PARTY_ID OR :P4_PARTY_ID IS NULL'))
,p_source_type=>'NATIVE_TABFORM'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>wwv_flow_string.join(wwv_flow_t_varchar2(
'POD is not setup for this Trading Partner. ',
'Please click "Add Row" button to create.'))
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3305501194865064)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'Select Row'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3305607676865065)
,p_query_column_id=>2
,p_column_alias=>'ID'
,p_column_display_sequence=>2
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_pk_col_source_type=>'S'
,p_pk_col_source=>'XXWC_B2B_POD_INFO_ID_SEQ'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3305713439865065)
,p_query_column_id=>3
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Party Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_column_default=>'P4_PARTY_ID'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'PARTY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3305821735865065)
,p_query_column_id=>4
,p_column_alias=>'CUST_ACCOUNT_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Cust Name - Cust Num'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT hca.ACCOUNT_NAME||'' - ''||hca.ACCOUNT_NUMBER d, hca.CUST_ACCOUNT_ID r',
'FROM apps.hz_cust_accounts_all hca',
'WHERE party_id = :P4_PARTY_ID ',
'AND STATUS = ''A'' ',
'AND ROWNUM < 350',
'UNION',
'SELECT hca.ACCOUNT_NAME||'' - ''||hca.ACCOUNT_NUMBER d, hca.CUST_ACCOUNT_ID r',
'FROM apps.hz_cust_accounts_all hca, xxwc.xxwc_b2b_pod_info_tbl STG',
'WHERE stg.cust_account_id = hca.cust_account_id ',
'AND hca.STATUS = ''A'' ',
'AND :P4_PARTY_ID IS NULL'))
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_lov_display_extra=>'NO'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'CUST_ACCOUNT_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3305917365865065)
,p_query_column_id=>5
,p_column_alias=>'ACCOUNT_NUMBER'
,p_column_display_sequence=>5
,p_column_heading=>'Account Number'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_named_lov=>wwv_flow_api.id(3313406454865098)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT account_number r, account_number d ',
'FROM apps.hz_cust_accounts_all hca',
'WHERE party_id = :P4_PARTY_ID ',
'AND STATUS = ''A'' ',
'AND ROWNUM < 100',
'UNION',
'SELECT hca.account_number r, hca.account_number d ',
'FROM apps.hz_cust_accounts_all hca, xxwc.xxwc_b2b_pod_info_tbl STG',
'WHERE stg.cust_account_id = hca.cust_account_id ',
'AND hca.STATUS = ''A'' ',
'AND :P4_PARTY_ID IS NULL'))
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'ACCOUNT_NUMBER'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306011909865065)
,p_query_column_id=>6
,p_column_alias=>'ACCOUNT_NAME'
,p_column_display_sequence=>6
,p_column_heading=>'Account Name'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ACCOUNT_NAME r, ACCOUNT_NAME d ',
'FROM apps.hz_cust_accounts_all hca',
'WHERE party_id = :P4_PARTY_ID ',
'AND STATUS = ''A'' ',
'AND ROWNUM < 100',
'UNION',
'SELECT hca.ACCOUNT_NAME r, hca.ACCOUNT_NAME d ',
'FROM apps.hz_cust_accounts_all hca, xxwc.xxwc_b2b_pod_info_tbl STG',
'WHERE stg.cust_account_id = hca.cust_account_id ',
'AND hca.STATUS = ''A'' ',
'AND :P4_PARTY_ID IS NULL'))
,p_lov_show_nulls=>'NO'
,p_column_width=>20
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'ACCOUNT_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306105600865066)
,p_query_column_id=>7
,p_column_alias=>'SITE_USE_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Job Site'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT hcsu.LOCATION d, hcsu.SITE_USE_ID r ',
'  FROM apps.hz_cust_site_uses_all  hcsu',
'     , apps.hz_cust_acct_sites_all hcas',
'     , apps.hz_party_sites         hps',
' WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'   AND hps.party_site_id = hcas.party_site_id',
'   AND hps.party_id = :P4_PARTY_ID ',
'   AND hcas.STATUS = ''A'' ',
'   AND hcsu.site_use_code = ''SHIP_TO''',
'   AND ROWNUM < 100'))
,p_lov_show_nulls=>'YES'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'SITE_USE_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306228613865066)
,p_query_column_id=>8
,p_column_alias=>'NOTIFY_ACCOUNT_MGR'
,p_column_display_sequence=>12
,p_column_heading=>'Notify Account Mgr'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(3368729304748334)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'N'
,p_lov_null_value=>'N'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'NOTIFY_ACCOUNT_MGR'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306322753865066)
,p_query_column_id=>9
,p_column_alias=>'LOCATION'
,p_column_display_sequence=>8
,p_column_heading=>'Location'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>2
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT LOCATION d, LOCATION r ',
'  FROM apps.hz_cust_site_uses_all  hcsu',
'     , apps.hz_cust_acct_sites_all hcas',
'     , apps.hz_party_sites         hps',
' WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'   AND hps.party_site_id = hcas.party_site_id',
'   AND hps.party_id = :P4_PARTY_ID AND hcas.STATUS = ''A'' AND ROWNUM < 100'))
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'LOCATION'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306421923865066)
,p_query_column_id=>10
,p_column_alias=>'DELIVER_POD'
,p_column_display_sequence=>9
,p_column_heading=>'Deliver POD'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(4897937324296850)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'N'
,p_lov_null_value=>'N'
,p_column_width=>12
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'DELIVER_POD'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306500959865066)
,p_query_column_id=>11
,p_column_alias=>'POD_EMAIL'
,p_column_display_sequence=>11
,p_column_heading=>'POD Email'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'POD_EMAIL'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306627140865066)
,p_query_column_id=>12
,p_column_alias=>'POD_FREQUENCY'
,p_column_display_sequence=>10
,p_column_heading=>'POD Frequency'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(3314431500865103)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'DAILY'
,p_lov_null_value=>'DAILY'
,p_column_width=>16
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'POD_FREQUENCY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306723287865066)
,p_query_column_id=>13
,p_column_alias=>'START_DATE_ACTIVE'
,p_column_display_sequence=>13
,p_column_heading=>'Start Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_lov_show_nulls=>'NO'
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'START_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306831764865066)
,p_query_column_id=>14
,p_column_alias=>'END_DATE_ACTIVE'
,p_column_display_sequence=>14
,p_column_heading=>'End Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_lov_show_nulls=>'NO'
,p_column_default=>'to_date(''31-DEC-4099'',''DD-MON-YYYY'')'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'Y'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'END_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3306900287865066)
,p_query_column_id=>15
,p_column_alias=>'POD_LAST_SENT_DATE'
,p_column_display_sequence=>15
,p_column_heading=>'POD Last Sent Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_column_default=>'SYSDATE - 1'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'POD_LAST_SENT_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3307011160865066)
,p_query_column_id=>16
,p_column_alias=>'POD_NEXT_SEND_DATE'
,p_column_display_sequence=>16
,p_column_heading=>'POD Next Send Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'Y'
,p_print_col_width=>'12'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'POD_NEXT_SEND_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3307132119865066)
,p_query_column_id=>17
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>18
,p_column_heading=>'Creation Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'CREATION_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3307208741865067)
,p_query_column_id=>18
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>19
,p_column_heading=>'Created By'
,p_hidden_column=>'Y'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3307305932865067)
,p_query_column_id=>19
,p_column_alias=>'LAST_UPDATE_DATE'
,p_column_display_sequence=>20
,p_column_heading=>'Last Update Date'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATE_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3307409804865067)
,p_query_column_id=>20
,p_column_alias=>'LAST_UPDATED_BY'
,p_column_display_sequence=>21
,p_column_heading=>'Last Updated By'
,p_hidden_column=>'Y'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3307530313865067)
,p_query_column_id=>21
,p_column_alias=>'ID_DISPLAY'
,p_column_display_sequence=>17
,p_column_heading=>'Id'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_POD_INFO_TBL'
,p_ref_column_name=>'ID_DISPLAY'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3310026201865081)
,p_plug_name=>'Notes Region'
,p_parent_plug_id=>wwv_flow_api.id(3305302551865060)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_region_attributes=>'STYLE="border: 1.5px solid rgb(204, 204, 204); width:1450px; padding: 2px;"'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid=>true
,p_plug_grid_column_span=>20
,p_plug_display_column=>2
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3307631759865067)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(3305302551865060)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Cancel'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3307821309865069)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(3305302551865060)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Delete'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3308010128865069)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(3305302551865060)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Submit'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3308216276865069)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(3305302551865060)
,p_button_name=>'ADD'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3313300474865094)
,p_branch_action=>'f?p=&APP_ID.:4:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3308414026865072)
,p_name=>'P4_PARTY_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(3305302551865060)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3308601155865075)
,p_name=>'P4_PARTY_NUMBER'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(3305302551865060)
,p_prompt=>'Party Number :'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3308803168865076)
,p_name=>'P4_PARTY_NAME'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(3305302551865060)
,p_prompt=>'Party Name :'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3309018051865076)
,p_name=>'P4_STATUS'
,p_item_sequence=>35
,p_item_plug_id=>wwv_flow_api.id(3305302551865060)
,p_item_default=>'DRAFT'
,p_prompt=>'Status :'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3310231629865081)
,p_name=>'P4_NOTES_CUST'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(3310026201865081)
,p_pre_element_text=>'<BR><B><U> Cust Name - Cust Num </U></B></BR>'
,p_post_element_text=>'<BR>* Cust Name - Cust Num is a mandatory field and the List of Values (LOV) are auto-populated based on the Party ID selected. This field represents the Customer Account Name and Number.</BR>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3310401907865081)
,p_name=>'P4_NOTES_POD_FREQUENCY'
,p_item_sequence=>44
,p_item_plug_id=>wwv_flow_api.id(3310026201865081)
,p_pre_element_text=>'<BR><B><U> POD Frequency </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>* Three options are available:  Daily, Weekly and Monthly.</BR>',
'<BR>* When selecting weekly and monthly, the date entered in the Start Date Active field will drive the reoccurring delivery date. Example – When Start Date Active is 2/1/16, the weekly POD’s will be delivered on 2/8/16 and 2/15/16 (increments of 7) '
||'and the monthly POD’s will be delivered on the 1st of every month (same numeric day).  Avoid setups using the 31st to ensure consistent delivery.</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3310611033865082)
,p_name=>'P4_NOTES_POD_EMAIL'
,p_item_sequence=>46
,p_item_plug_id=>wwv_flow_api.id(3310026201865081)
,p_pre_element_text=>'<BR><B><U> POD Email </U></B></BR>'
,p_post_element_text=>'<BR>* Enter desired POD Recipient(s) email address.  This is the only email address(es) that will receive POD’s.  To enter multiple email addresses, separate them by a comma.</BR>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3310827354865082)
,p_name=>'P4_NOTES_NOTIF_ACCTMGR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(3310026201865081)
,p_pre_element_text=>'<BR><B><U> Notify Account Mgr </U></B></BR>'
,p_post_element_text=>'<BR>* This field is optional. When select "yes" <U>all</U> AM''s associated with the Customer Account will receive copies of every POD.</BR>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3311024528865082)
,p_name=>'P4_NOTES_LAST_NEXT_DATE'
,p_item_sequence=>54
,p_item_plug_id=>wwv_flow_api.id(3310026201865081)
,p_pre_element_text=>'<BR><B><U> POD Last/Next Sent Date </U></B></BR>'
,p_post_element_text=>'<BR>* These fields are view only and indicate the last date the POD was sent and the next date the POD will be sent.</BR>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3311212920865083)
,p_name=>'P4_NOTES_JOB_SITE'
,p_item_sequence=>42
,p_item_plug_id=>wwv_flow_api.id(3310026201865081)
,p_pre_element_text=>'<BR><B><U> Job Site </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>* Job Site is an optional field and the LOV are auto-populated based on the Cust Name - Cust Num selected.</BR>',
'<BR>* Use this field when POD Contacts vary by Job Site. Or use this field when attempting to send POD’s for a single Job Site only (and exclude all other Job Sites in the LOV).</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(3309311340865078)
,p_tabular_form_region_id=>wwv_flow_api.id(3305302551865060)
,p_validation_name=>'POD_EMAIL'
,p_validation_sequence=>10
,p_validation=>'POD_EMAIL'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'POD Email cannot be empty. Please enter a valid email address.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(3308010128865069)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'POD_EMAIL'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(3309530200865078)
,p_tabular_form_region_id=>wwv_flow_api.id(3305302551865060)
,p_validation_name=>'POD_CUSTOMER'
,p_validation_sequence=>20
,p_validation=>'CUST_ACCOUNT_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'"Cust Name - Cust Num" cannot be empty. Please select a valid Customer.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(3308010128865069)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'CUST_ACCOUNT_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(3311517160865085)
,p_validation_name=>'Duplicate Rec'
,p_validation_sequence=>30
,p_validation=>'SELECT ''1'' FROM XXWC.XXWC_B2B_POD_INFO_TBL STG WHERE STG.CUST_ACCOUNT_ID = CUST_ACCOUNT_ID AND NVL(STG.SITE_USE_ID,-1) = NVL(SITE_USE_ID, -1)'
,p_validation_type=>'EXISTS'
,p_error_message=>'Customer and Job-Site combination already setup for POD notification'
,p_always_execute=>'N'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_api.id(3308010128865069)
,p_only_for_changed_rows=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
begin
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3312021862865089)
,p_name=>'Set cascading LOV after refresh and onload - 2'
,p_event_sequence=>10
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'[name=f04]'
,p_bind_type=>'live'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3312321617865091)
,p_event_id=>wwv_flow_api.id(3312021862865089)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'$("[name=f07]").htmldbCascade(  ',
'  "[name=f04]",  ',
'  "JOB_LOV",',
'  {nullShow:true,   ',
'   nullDisplay:"Select"',
'});'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3312417290865092)
,p_name=>'Set cascading LOV for new row'
,p_event_sequence=>20
,p_triggering_element_type=>'JQUERY_SELECTOR'
,p_triggering_element=>'[name=f07]'
,p_bind_type=>'live'
,p_bind_event_type=>'focusin'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3312715912865092)
,p_event_id=>wwv_flow_api.id(3312417290865092)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'$("[name=f07]").htmldbCascade(  ',
'  "[name=f04]",  ',
'  "JOB_LOV",{   ',
'   nullShow:true,   ',
'   nullDisplay:"Select"',
'});'))
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3312811137865092)
,p_name=>'Add Button'
,p_event_sequence=>30
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(3308216276865069)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3313118476865093)
,p_event_id=>wwv_flow_api.id(3312811137865092)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'javascript:addRow();'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3311626744865085)
,p_process_sequence=>30
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Update Party Number and Party Name'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'BEGIN',
'',
'SELECT PARTY_NAME, PARTY_NUMBER ',
'INTO :P4_PARTY_NAME, :P4_PARTY_NUMBER',
'FROM apps.hz_parties ',
'WHERE party_id = :P4_PARTY_ID;',
'',
'END;'))
,p_process_when=>'SELECT ''1'' FROM DUAL WHERE :P4_PARTY_ID IS NOT NULL;'
,p_process_when_type=>'EXISTS'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3309610506865079)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(3305302551865060)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_POD_INFO_TBL'
,p_attribute_03=>'ID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(3308010128865069)
,p_process_success_message=>'Trading Partner is added to B2B POD notification.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3309823060865080)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(3305302551865060)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_POD_INFO_TBL'
,p_attribute_03=>'ID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3311812723865086)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Update Status - Submit'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'   l_subject              VARCHAR2 (32767) DEFAULT NULL;',
'   l_body                 VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_header          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_detail          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_footer          VARCHAR2 (32767) DEFAULT NULL;',
'   l_instance             VARCHAR2(50) := ''EBZIDEV'';',
'   l_to_sr_email_id       VARCHAR2 (32767) DEFAULT NULL;',
'   l_to_mgr_email_id      VARCHAR2 (32767) DEFAULT NULL;',
'   l_user_full_name            VARCHAR2(250);',
'   l_user_name                 VARCHAR2(50);',
'   l_to_email             VARCHAR2(32767);',
'   l_dflt_email           VARCHAR2(100) := ''Gopi.Damuluri@HDSupply.com'';',
'',
'',
'   l_cust_account_id      NUMBER;',
'   l_site_use_id          NUMBER;',
'',
'   l_account_name         VARCHAR2(200);',
'   l_location             VARCHAR2(200);',
'   l_checked_row          NUMBER;',
'   l_exception            EXCEPTION;',
'',
'BEGIN',
'',
'   :P4_STATUS := ''APPROVED'';',
'',
'    -------------------------------------------------------',
'    -- Derive User Name and NTID',
'    -------------------------------------------------------',
'   SELECT REPLACE(description,'','', '''') ',
'        , user_name',
'     INTO l_user_full_name',
'        , l_user_name',
'     FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER);',
'',
'      -------------------------------------------------------',
'      -- Derive Oracle Instance Name',
'      -------------------------------------------------------',
'     BEGIN',
'        SELECT SUBSTR (UPPER (global_name), 1, 7)',
'          INTO l_instance',
'          FROM global_name;',
'     EXCEPTION',
'     WHEN OTHERS THEN',
'        l_instance := NULL;',
'     END;',
'',
'     --------------------------------------------------',
'     -- Derive SalesRep Email-Id',
'     --------------------------------------------------',
'     BEGIN',
'        SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_rep_email (:P4_PARTY_ID)',
'          INTO l_to_sr_email_id',
'          FROM DUAL;',
'     EXCEPTION',
'     WHEN OTHERS THEN',
'       l_to_sr_email_id := NULL;',
'     END;',
'',
'     --------------------------------------------------',
'     -- Derive SalesRep Manager Email-Id',
'     --------------------------------------------------',
'     BEGIN',
'        SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_mgr_email (:P4_PARTY_ID, l_user_name)',
'          INTO l_to_mgr_email_id',
'          FROM DUAL;',
'     EXCEPTION',
'     WHEN OTHERS THEN',
'       l_to_mgr_email_id := NULL;',
'     END;',
'',
'     l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'',
'    l_to_sr_email_id := l_to_sr_email_id||'',''||l_to_mgr_email_id;',
'    IF l_instance = ''EBIZPRD'' THEN',
'       l_to_email    := l_to_sr_email_id;',
'       l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                     ;',
'    ELSE',
'       l_to_email    := l_dflt_email;',
'       l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                        ||''<BR> Email Ids - ''||l_to_sr_email_id||''</BR> ''',
'                     ;',
'    END IF;',
'',
'  FOR i IN 1..apex_application.g_f01.count LOOP',
'',
'    l_checked_row     := NULL;',
'    l_cust_account_id := NULL;',
'    l_site_use_id     := NULL;',
'    l_account_name    := NULL;',
'    l_location        := NULL;',
'',
'    BEGIN',
'    -------------------------------------------------------',
'    -- Identify the row-number that has been Checked',
'    -------------------------------------------------------',
'    l_checked_row := apex_application.g_f01(i);',
'',
'    IF NVL(l_checked_row,0) = 0 THEN',
'       SELECT COUNT(1)',
'         INTO l_checked_row',
'         FROM xxwc.xxwc_b2B_pod_info_tbl',
'        WHERE party_id = :P4_PARTY_ID;',
'    END IF;',
'',
'    l_cust_account_id    := apex_application.g_f04(l_checked_row);',
'',
'    BEGIN',
'    l_site_use_id        := apex_application.g_f07(l_checked_row);',
'    EXCEPTION ',
'    WHEN OTHERS THEN',
'    NULL;',
'    END;',
'',
'    -------------------------------------------------------',
'    -- Derive Customer Name',
'    -------------------------------------------------------',
'    IF l_cust_account_id IS NOT NULL THEN',
'       SELECT account_name||'' - ''||account_number',
'         INTO l_account_name',
'         FROM apps.hz_cust_accounts_all hca',
'        WHERE cust_account_id = l_cust_account_id',
'          AND ROWNUM = 1;',
'    END IF;',
'',
'    -------------------------------------------------------',
'    -- Derive Job Location Name',
'    -------------------------------------------------------',
'    IF l_site_use_id IS NOT NULL THEN',
'       SELECT location',
'         INTO l_location',
'         FROM apps.hz_cust_site_uses_all hcsu',
'        WHERE site_use_id = l_site_use_id',
'          AND ROWNUM = 1;',
'    END IF;',
'',
'    IF l_location IS NULL THEN',
'',
'       l_subject     := l_account_name||'' is setup for Proof Of Delivery notification'';',
'',
'       l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                        ''<BR> </BR> <BR> Your request to set-up <B>'' ||l_account_name|| ''</B> for Proof Of Delivery notification has been completed. </BR>''||',
'                        ''<BR> </BR> <BR> Please promptly notify all impacted branches and sales associates.''||',
'                        ''<BR> </BR> <BR> Submitted By : <B> ''||l_user_full_name',
'                        ;',
'    ELSE',
'',
'       l_subject     := l_account_name||'' is setup for Proof Of Delivery notification for JobSite - ''||l_location;',
'',
'       l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                        ''<BR> </BR> <BR> Your request to set-up <B>'' ||l_account_name|| ''</B> for Proof Of Delivery notification has been completed. </BR>''||',
'                        ''<BR> </BR> <BR> Job Site Name - <B>''||l_location||',
'                        ''</B><BR> </BR> <BR> Please promptly notify all impacted branches and sales associates.''||',
'                        ''<BR> </BR> <BR> Submitted By : <B> ''||l_user_full_name',
'                        ;',
'    END IF; -- IF location IS NULL THEN',
'',
'    l_body        := l_body_header || l_body_detail ||chr(10)|| l_body_footer;',
'',
'    IF l_to_email IS NOT NULL THEN',
'       APEX_MAIL.SEND(p_to                        => l_to_email,',
'                      p_from                      => ''no-reply@whitecap.net'',',
'                      p_body                      => l_body,',
'                      p_body_html                 => l_body,',
'                      p_subj                      => l_subject,',
'                      p_replyto                   => NULL);',
'    END IF;',
'    COMMIT;',
'  ',
'  EXCEPTION',
'  WHEN l_exception THEN',
'    NULL;',
'  END;',
'',
'END LOOP; --   FOR i IN 1..apex_application.g_f01.count LOOP',
'',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(3308010128865069)
);
end;
/
prompt --application/pages/page_00005
begin
wwv_flow_api.create_page(
 p_id=>5
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_tab_set=>'TS1'
,p_name=>'WC B2B New Trading Partner1'
,p_page_mode=>'NORMAL'
,p_step_title=>'WC B2B New Trading Partner1'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Copy of Page1'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_browser_cache=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20171116114038'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3804210221581223)
,p_plug_name=>'Setup New B2B Trading Partners'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3804802144581226)
,p_name=>'Customer Details'
,p_parent_plug_id=>wwv_flow_api.id(3804210221581223)
,p_template=>wwv_flow_api.id(24617707682658552)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT hca.account_number customer_number',
'     , hca.account_name customer_name',
'     , hp.party_number ',
'     , REPLACE(REPLACE(hp.party_name, '','' , '' ''), ''#'','''') party_name',
'     , hca.cust_account_id',
'     , hca.party_id',
'     , :P5_USER_ROLE user_role',
'     , APPS.XXWC_B2B_SO_IB_PKG.GET_SALES_REP(hca.cust_account_id) SALES_REP',
' FROM apps.hz_Cust_accounts_all hca',
'    , apps.hz_parties hp',
'WHERE 1 = 1',
'  AND :P5_SHOW_ALL_ACCTS = ''N''',
'  --And hca.account_number = ''10000024272''',
'  AND hca.party_id = hp.party_id',
'  AND (hca.account_number = :P5_CUSTOMER_NUMBER OR :P5_CUSTOMER_NUMBER = 0 OR :P5_CUSTOMER_NUMBER IS NULL)',
'  AND (hca.account_name like UPPER(:P5_CUSTOMER_NAME)||''%'' OR :P5_CUSTOMER_NAME = ''0'' OR :P5_CUSTOMER_NAME IS NULL)',
'  AND (:P5_PARTY_NUMBER = 0 OR :P5_PARTY_NUMBER IS NULL)',
'UNION',
'SELECT hca.account_number customer_number',
'     , hca.account_name customer_name',
'     , hp.party_number ',
'     , REPLACE(REPLACE(hp.party_name, '','' , '' ''), ''#'','''') party_name',
'     , hca.cust_account_id',
'     , hca.party_id',
'     , :P5_USER_ROLE user_role',
'     , APPS.XXWC_B2B_SO_IB_PKG.GET_SALES_REP(hca.cust_account_id) SALES_REP',
'FROM apps.hz_Cust_accounts_all hca',
'    , apps.hz_parties hp',
'    , apps.hz_Cust_accounts_all hca2',
'WHERE 1 = 1',
'  AND :P5_SHOW_ALL_ACCTS = ''Y''',
'  AND (hca2.account_number = :P5_CUSTOMER_NUMBER OR :P5_CUSTOMER_NUMBER = 0 OR :P5_CUSTOMER_NUMBER IS NULL) ',
'  AND (hca2.account_name like UPPER(:P5_CUSTOMER_NAME)||''%'' OR :P5_CUSTOMER_NAME = ''0'' OR :P5_CUSTOMER_NAME IS NULL)',
'  AND hp.party_id = hca2.party_id',
'  AND hca.party_id = hp.party_id',
'  AND (:P5_PARTY_NUMBER = 0 OR :P5_PARTY_NUMBER IS NULL)',
'UNION',
'SELECT hca.account_number customer_number',
'     , hca.account_name customer_name',
'     , hp.party_number ',
'     , REPLACE(REPLACE(hp.party_name, '','' , '' ''), ''#'','''') party_name',
'     , hca.cust_account_id',
'     , hca.party_id',
'     , :P5_USER_ROLE user_role',
'     , APPS.XXWC_B2B_SO_IB_PKG.GET_SALES_REP(hca.cust_account_id) SALES_REP',
' FROM apps.hz_Cust_accounts_all hca',
'    , apps.hz_parties hp',
'WHERE 1 = 1',
'  AND hca.party_id = hp.party_id',
'  AND hp.party_number = :P5_PARTY_NUMBER'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ''1'' FROM DUAL WHERE :P5_CUSTOMER_NUMBER IS NOT NULL UNION',
'SELECT ''1'' FROM DUAL WHERE :P5_CUSTOMER_NAME IS NOT NULL UNION',
'SELECT ''1'' FROM DUAL WHERE :P5_PARTY_NUMBER IS NOT NULL'))
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'-- No Records Found --'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3805013548581228)
,p_query_column_id=>1
,p_column_alias=>'CUSTOMER_NUMBER'
,p_column_display_sequence=>3
,p_column_heading=>'Customer Number'
,p_column_link=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:6:P6_USER_ROLE,P6_ACCOUNT_NUMBER,P6_STATUS_A_I_L:#USER_ROLE#,#CUSTOMER_NUMBER#,&P5_STATUS_I_A_ALL.'
,p_column_linktext=>'#CUSTOMER_NUMBER#'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>2
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3805100066581228)
,p_query_column_id=>2
,p_column_alias=>'CUSTOMER_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Customer Name'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3805230669581228)
,p_query_column_id=>3
,p_column_alias=>'PARTY_NUMBER'
,p_column_display_sequence=>1
,p_column_heading=>'Party Number'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_default_sort_column_sequence=>1
,p_disable_sort_column=>'N'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3805320458581228)
,p_query_column_id=>4
,p_column_alias=>'PARTY_NAME'
,p_column_display_sequence=>2
,p_column_heading=>'Party Name'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3805410296581228)
,p_query_column_id=>5
,p_column_alias=>'CUST_ACCOUNT_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CUST_ACCOUNT_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3805516054581228)
,p_query_column_id=>6
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>6
,p_column_heading=>'PARTY_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3805604656581228)
,p_query_column_id=>7
,p_column_alias=>'USER_ROLE'
,p_column_display_sequence=>7
,p_column_heading=>'User Role'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3805716401581229)
,p_query_column_id=>8
,p_column_alias=>'SALES_REP'
,p_column_display_sequence=>8
,p_column_heading=>'Account Manager'
,p_use_as_row_header=>'N'
,p_column_css_style=>'width:400px; display:block; white-space:normal;'
,p_disable_sort_column=>'N'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3805808678581229)
,p_plug_name=>'Customer and Party Info'
,p_parent_plug_id=>wwv_flow_api.id(3804210221581223)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_grid_column_span=>20
,p_plug_display_point=>'BODY'
,p_translate_title=>'N'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3804406977581224)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(3804210221581223)
,p_button_name=>'CLEAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Clear'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:RP,3:P5_CUSTOMER_NUMBER,P5_CUSTOMER_NAME,P5_SHOW_ALL_ACCTS,P5_PARTY_NUMBER:,,N,'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3804631562581225)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(3804210221581223)
,p_button_name=>'FIND'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'Find'
,p_button_position=>'BOTTOM'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3808309326581233)
,p_branch_action=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'NEVER'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3808531544581234)
,p_branch_action=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'NEVER'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3349546050973101)
,p_name=>'P5_STATUS_I_A_ALL'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_item_default=>'A'
,p_prompt=>'Job Site Status'
,p_source=>'A'
,p_source_type=>'STATIC'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'STATIC:Active;A,Inactive;I,ALL;L'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3806031142581229)
,p_name=>'P5_PARTY_ID'
,p_item_sequence=>8
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_prompt=>'PARTY_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cHeight=>1
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3806220763581230)
,p_name=>'P5_CUST_ACCOUNT_ID'
,p_item_sequence=>5
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_prompt=>'CUST_ACCOUNT_ID'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cHeight=>1
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3806428166581230)
,p_name=>'P5_CUSTOMER_NUMBER'
,p_item_sequence=>6
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_prompt=>'Customer Number'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3806611722581230)
,p_name=>'P5_CUSTOMER_NAME'
,p_item_sequence=>7
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_prompt=>'Customer Name'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3806817493581230)
,p_name=>'P5_PARTY_NAME'
,p_item_sequence=>11
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_prompt=>'PARTY_NAME'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cHeight=>1
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3807022604581231)
,p_name=>'P5_PARTY_NUMBER'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_prompt=>'Party Number'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3807219847581231)
,p_name=>'P5_SHOW_ALL_ACCTS'
,p_is_required=>true
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_item_default=>'N'
,p_prompt=>'Show All Customer Accounts'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'YN1'
,p_lov=>'.'||wwv_flow_api.id(2505301077635389)||'.'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621516577658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3807411838581231)
,p_name=>'P5_FILLER_SPACE'
,p_item_sequence=>31
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3807602169581231)
,p_name=>'P5_USER_ROLE'
,p_item_sequence=>41
,p_item_plug_id=>wwv_flow_api.id(3805808678581229)
,p_item_default=>'1'
,p_prompt=>'User Responsibility'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621516577658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3807804251581232)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Find User Role'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'l_count NUMBER := 0;',
'BEGIN',
'',
'SELECT COUNT(1)',
'  INTO l_count',
'FROM  xxwc.xxwc_ms_user_roles_tbl a',
',     xxwc.xxwc_ms_roles_tbl b',
',     xxwc.xxwc_ms_role_resp_tbl c',
',     xxwc.xxwc_ms_responsibility_tbl d',
'WHERE a.role_id = b.role_id(+)',
'AND   b.role_id = c.role_id(+)',
'AND   c.responsibility_id = d.responsibility_id(+)',
'AND   a.user_id= :APP_USER',
'and d.app_id = :APP_ID',
'and c.app_id = :APP_ID',
'and b.role_name IN (''Oracle Developers'', ''WhiteCap Support'');',
'',
'IF l_count > 0 THEN',
'  :P5_USER_ROLE := ''3'';',
'END IF; ',
'',
'EXCEPTION',
'WHEN OTHERS THEN',
'  :P5_USER_ROLE := ''1'';',
'END;'))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3808015147581233)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Find User Role'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'l_salesrep_flag VARCHAR2(1);',
'BEGIN',
'',
'SELECT apps.xxwc_b2b_so_ib_pkg.is_sales_rep(:P5_CUST_ACCOUNT_ID, nvl(v(''APP_USER''),USER))',
'  INTO l_salesrep_flag',
'  FROM dual;',
'',
'IF l_salesrep_flag IN (''Y'', ''0'') THEN ',
'   :P5_USER_ROLE := ''2'';',
'ELSE',
'   :P5_USER_ROLE := ''1'';',
'END IF;',
'',
'dbms_output.put_line(''l_salesrep_flag - ''||l_salesrep_flag);',
'',
'EXCEPTION',
'WHEN OTHERS THEN',
'dbms_output.put_line(''Error - ''||SQLERRM);',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(3804631562581225)
,p_process_when_type=>'NEVER'
);
end;
/
prompt --application/pages/page_00006
begin
wwv_flow_api.create_page(
 p_id=>6
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'B2B Configuration'
,p_page_mode=>'NORMAL'
,p_step_title=>'B2B Configuration'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';',
'',
'function pad(num, size) {',
'    var s = "000000000" + num;',
'    return s.substr(s.length-size);',
'} ',
'',
'function fn_CloneRow(pThis, row_number) ',
'{',
'var deliver_soa = document.getElementById("f07_"+pad(row_number,4)).value; ',
'var soa_email = document.getElementById("f08_"+pad(row_number,4)).value; ',
'var soa_print_price = document.getElementById("f09_"+pad(row_number,4)).value; ',
'var deliver_asn = document.getElementById("f11_"+pad(row_number,4)).value; ',
'var asn_email = document.getElementById("f12_"+pad(row_number,4)).value; ',
'var deliver_pod = document.getElementById("f13_"+pad(row_number,4)).value; ',
'var pod_frequency = document.getElementById("f14_"+pad(row_number,4)).value; ',
'var pod_email = document.getElementById("f15_"+pad(row_number,4)).value; ',
'var pod_print_price = document.getElementById("f16_"+pad(row_number,4)).value; ',
'var notify_acct_mgr = document.getElementById("f17_"+pad(row_number,4)).value; ',
'var notification_email = document.getElementById("f18_"+pad(row_number,4)).value; ',
'var cnt = document.getElementsByName("f01").length;',
'javascript:apex.widget.tabular.addRow();',
'$x(''f07_''+pad(cnt+1,4)).value = deliver_soa;',
'$x(''f08_''+pad(cnt+1,4)).value = soa_email;',
'$x(''f09_''+pad(cnt+1,4)).value = soa_print_price;',
'$x(''f11_''+pad(cnt+1,4)).value = deliver_asn;',
'$x(''f12_''+pad(cnt+1,4)).value = asn_email;',
'$x(''f13_''+pad(cnt+1,4)).value = deliver_pod;',
'$x(''f14_''+pad(cnt+1,4)).value = pod_frequency;',
'$x(''f15_''+pad(cnt+1,4)).value = pod_email;',
'$x(''f16_''+pad(cnt+1,4)).value = pod_print_price;',
'$x(''f17_''+pad(cnt+1,4)).value = notify_acct_mgr;',
'$x(''f18_''+pad(cnt+1,4)).value = notification_email;',
'}',
'  function f_fetch_emp_dtls(pThis) {',
'  var row_id=0;',
'//alert(pThis);',
'  $("[name=''f02'']").each(function(){',
'      ++row_id;',
'   if(row_id <= 9){ ',
'   if($x(''f02_000''+row_id).value == pThis){',
'     fn_CloneRow(pThis, row_id);',
'     return;',
'	 }}',
'    else if(row_id >= 10 && row_id <= 99){ ',
'        if($x(''f02_00''+row_id).value == pThis){',
'     fn_CloneRow(pThis, row_id);',
'     return;',
'	 }}',
' });  ',
'}',
''))
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20171116130940'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(4184728197137464)
,p_name=>'SetUp new B2B Trading Partner'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'null clone,',
'"ID",',
'"PARTY_ID",',
'"PARTY_NAME",',
'"PARTY_NUMBER",',
'"CUST_ACCOUNT_ID",',
'"ACCOUNT_NUMBER",',
'"DELIVER_SOA",',
'"SOA_EMAIL",',
'"SOA_PRINT_PRICE",',
'"DELIVER_ASN",',
'"ASN_EMAIL",',
'"DELIVER_INVOICE",',
'DECODE(DELIVER_INVOICE, ''Y'', ''Yes'', ''No'')     "DELIVER_INVOICE_SALES",',
'"POD_PRINT_PRICE",',
'"TP_NAME",',
'"TP_NAME" TP_NAME_DISPLAY,',
'"ACCOUNT_NAME",',
'"SITE_USE_ID",',
'"NOTIFY_ACCOUNT_MGR",',
'"LOCATION",',
'"DELIVER_POD",',
'"POD_EMAIL",',
'"POD_FREQUENCY",',
'"START_DATE_ACTIVE",',
'"END_DATE_ACTIVE",',
'"POD_LAST_SENT_DATE",',
'"POD_NEXT_SEND_DATE",',
'"CREATION_DATE",',
'"CREATED_BY",',
'"LAST_UPDATE_DATE",',
'"LAST_UPDATED_BY",',
'--"ID" ID_DISPLAY,',
'NVL(STATUS, ''APPROVED'') "STATUS",',
'-- "DELIVER_POA",',
'-- "DEFAULT_EMAIL",',
'"NOTIFICATION_EMAIL",',
'"BUSINESS_EVENT_ELIGIBLE"',
'--"COMMENTS",',
'--"APPROVED_DATE",',
'--"CUSTOMER_ID"',
'from "XXWC"."XXWC_B2B_CONFIG_TBL"',
'WHERE (ACCOUNT_NUMBER= :P6_ACCOUNT_NUMBER  ',
'and ((site_use_id IS NULL AND :P6_STATUS_A_I_L IN (''A'',''L'')) OR site_use_id in (SELECT  hcsu.SITE_USE_ID ',
'  FROM apps.hz_cust_site_uses_all  hcsu',
'     , apps.hz_cust_acct_sites_all hcas',
'     , apps.hz_party_sites         hps',
' WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'   AND hps.party_site_id = hcas.party_site_id',
'    AND hps.party_id = :P6_PARTY_ID',
'    AND hcas.STATUS = decode(:P6_STATUS_A_I_L,''A'',''A'',''I'',''I'',hcas.STATUS)',
'   AND hcsu.site_use_code = ''SHIP_TO''',
'   --AND ROWNUM < 100',
'   and cust_account_id = :P6_CUST_ACCOUNT_ID))) OR :P6_ACCOUNT_NUMBER IS NULL'))
,p_source_type=>'NATIVE_TABFORM'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P6_USER_ROLE in (''3'', ''2'')'
,p_display_condition_type=>'EXISTS'
,p_header=>'<div style="width=1000px;">'
,p_footer=>'</div>'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4231324485178711)
,p_query_column_id=>1
,p_column_alias=>'CLONE'
,p_column_display_sequence=>4
,p_column_heading=>'Copy'
,p_use_as_row_header=>'N'
,p_column_html_expression=>' <img src="#IMAGE_PREFIX#ws/small_page.gif" align="middle"  onclick= "javascript:f_fetch_emp_dtls(#ID#);"  class="pseudoButtonInactive" alt="" /><a href="#LINK#"><img src="#IMAGE_PREFIX#ws/small_page.gif"</a>'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4185005391137466)
,p_query_column_id=>2
,p_column_alias=>'ID'
,p_column_display_sequence=>2
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_pk_col_source_type=>'T'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4185129939137466)
,p_query_column_id=>3
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>5
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_derived_column=>'N'
,p_column_default=>'P6_PARTY_ID'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'PARTY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4185214783137466)
,p_query_column_id=>4
,p_column_alias=>'PARTY_NAME'
,p_column_display_sequence=>3
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_derived_column=>'N'
,p_column_default=>'P6_PARTY_NAME'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'PARTY_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4185320576137467)
,p_query_column_id=>5
,p_column_alias=>'PARTY_NUMBER'
,p_column_display_sequence=>6
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_derived_column=>'N'
,p_column_default=>'P6_PARTY_NUMBER'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'PARTY_NUMBER'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4187917890137467)
,p_query_column_id=>6
,p_column_alias=>'CUST_ACCOUNT_ID'
,p_column_display_sequence=>33
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_derived_column=>'N'
,p_column_default=>'P6_CUST_ACCOUNT_ID'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'CUST_ACCOUNT_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4187712157137467)
,p_query_column_id=>7
,p_column_alias=>'ACCOUNT_NUMBER'
,p_column_display_sequence=>34
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_derived_column=>'N'
,p_column_default=>'P6_ACCOUNT_NUMBER'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'ACCOUNT_NUMBER'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4185507337137467)
,p_query_column_id=>8
,p_column_alias=>'DELIVER_SOA'
,p_column_display_sequence=>8
,p_column_heading=>'Deliver SOA'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(3368729304748334)
,p_lov_show_nulls=>'NO'
,p_derived_column=>'N'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'DELIVER_SOA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4187001263137467)
,p_query_column_id=>9
,p_column_alias=>'SOA_EMAIL'
,p_column_display_sequence=>9
,p_column_heading=>'SOA Email'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'SOA_EMAIL'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4188527194137468)
,p_query_column_id=>10
,p_column_alias=>'SOA_PRINT_PRICE'
,p_column_display_sequence=>10
,p_column_heading=>'SOA Print Price'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_derived_column=>'N'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'SOA_PRINT_PRICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4185630977137467)
,p_query_column_id=>11
,p_column_alias=>'DELIVER_ASN'
,p_column_display_sequence=>12
,p_column_heading=>'Deliver ASN'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(3368729304748334)
,p_lov_show_nulls=>'NO'
,p_derived_column=>'N'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'DELIVER_ASN'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4187125781137467)
,p_query_column_id=>12
,p_column_alias=>'ASN_EMAIL'
,p_column_display_sequence=>13
,p_column_heading=>'ASN Email'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'ASN_EMAIL'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4185729119137467)
,p_query_column_id=>13
,p_column_alias=>'DELIVER_INVOICE'
,p_column_display_sequence=>14
,p_column_heading=>'Deliver Invoice'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXISTS'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P6_USER_ROLE = ''3'''
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(3368729304748334)
,p_lov_show_nulls=>'NO'
,p_derived_column=>'N'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'DELIVER_INVOICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4576719035461446)
,p_query_column_id=>14
,p_column_alias=>'DELIVER_INVOICE_SALES'
,p_column_display_sequence=>15
,p_column_heading=>'Deliver Invoice'
,p_use_as_row_header=>'N'
,p_display_when_cond_type=>'EXISTS'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P6_USER_ROLE = ''2'''
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4188615364137468)
,p_query_column_id=>15
,p_column_alias=>'POD_PRINT_PRICE'
,p_column_display_sequence=>19
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'POD_PRINT_PRICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4186405048137467)
,p_query_column_id=>16
,p_column_alias=>'TP_NAME'
,p_column_display_sequence=>21
,p_column_heading=>'TP Name'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXISTS'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P6_USER_ROLE = ''3'''
,p_display_as=>'TEXT'
,p_column_width=>16
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'TP_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4563101630342291)
,p_query_column_id=>17
,p_column_alias=>'TP_NAME_DISPLAY'
,p_column_display_sequence=>22
,p_column_heading=>'TP Name'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXISTS'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P6_USER_ROLE = ''2'''
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4187812294137467)
,p_query_column_id=>18
,p_column_alias=>'ACCOUNT_NAME'
,p_column_display_sequence=>35
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_derived_column=>'N'
,p_column_default=>'P6_CUSTOMER_NAME'
,p_column_default_type=>'ITEM'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'ACCOUNT_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4188428264137468)
,p_query_column_id=>19
,p_column_alias=>'SITE_USE_ID'
,p_column_display_sequence=>7
,p_column_heading=>'Job Site'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'POPUPKEY_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT hcsu.LOCATION d, hcsu.SITE_USE_ID r  ',
'  FROM apps.hz_cust_site_uses_all  hcsu',
'     , apps.hz_cust_acct_sites_all hcas',
'     , apps.hz_party_sites         hps',
' WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'   AND hps.party_site_id = hcas.party_site_id',
'   AND hps.party_id = :P6_PARTY_ID',
'    AND hcsu.site_use_code = ''SHIP_TO''',
'     and cust_account_id = :P6_CUST_ACCOUNT_ID'))
,p_lov_show_nulls=>'YES'
,p_lov_null_text=>'- All - '
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'SITE_USE_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4186918593137467)
,p_query_column_id=>20
,p_column_alias=>'NOTIFY_ACCOUNT_MGR'
,p_column_display_sequence=>20
,p_column_heading=>'Notify Account Mgr'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(2531513372639611)
,p_lov_show_nulls=>'NO'
,p_derived_column=>'N'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'NOTIFY_ACCOUNT_MGR'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4188317620137468)
,p_query_column_id=>21
,p_column_alias=>'LOCATION'
,p_column_display_sequence=>11
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'LOCATION'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4187427289137467)
,p_query_column_id=>22
,p_column_alias=>'DELIVER_POD'
,p_column_display_sequence=>16
,p_column_heading=>'Deliver POD'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(3368729304748334)
,p_lov_show_nulls=>'NO'
,p_derived_column=>'N'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'DELIVER_POD'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4187531848137467)
,p_query_column_id=>23
,p_column_alias=>'POD_EMAIL'
,p_column_display_sequence=>18
,p_column_heading=>'POD Email'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'POD_EMAIL'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4187609398137467)
,p_query_column_id=>24
,p_column_alias=>'POD_FREQUENCY'
,p_column_display_sequence=>17
,p_column_heading=>'POD Frequency'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(3314431500865103)
,p_lov_show_nulls=>'NO'
,p_derived_column=>'N'
,p_lov_display_extra=>'NO'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'POD_FREQUENCY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4185808115137467)
,p_query_column_id=>25
,p_column_alias=>'START_DATE_ACTIVE'
,p_column_display_sequence=>25
,p_column_heading=>'Start Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_derived_column=>'N'
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'START_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4185917379137467)
,p_query_column_id=>26
,p_column_alias=>'END_DATE_ACTIVE'
,p_column_display_sequence=>26
,p_column_heading=>'End Date Active'
,p_use_as_row_header=>'N'
,p_column_format=>'DD-MON-YYYY HH24:MI:SS'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_derived_column=>'N'
,p_column_default=>'to_date(''31-DEC-4099'',''DD-MON-YYYY'')'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'END_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4188018872137467)
,p_query_column_id=>27
,p_column_alias=>'POD_LAST_SENT_DATE'
,p_column_display_sequence=>27
,p_hidden_column=>'Y'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'POD_LAST_SENT_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4188103314137467)
,p_query_column_id=>28
,p_column_alias=>'POD_NEXT_SEND_DATE'
,p_column_display_sequence=>28
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_derived_column=>'N'
,p_column_default=>'SYSDATE'
,p_column_default_type=>'FUNCTION'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'POD_NEXT_SEND_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4186031223137467)
,p_query_column_id=>29
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>29
,p_hidden_column=>'Y'
,p_derived_column=>'N'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'CREATION_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4186116317137467)
,p_query_column_id=>30
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>30
,p_hidden_column=>'Y'
,p_derived_column=>'N'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'CREATED_BY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4186214770137467)
,p_query_column_id=>31
,p_column_alias=>'LAST_UPDATE_DATE'
,p_column_display_sequence=>31
,p_hidden_column=>'Y'
,p_derived_column=>'N'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'LAST_UPDATE_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4186325931137467)
,p_query_column_id=>32
,p_column_alias=>'LAST_UPDATED_BY'
,p_column_display_sequence=>32
,p_hidden_column=>'Y'
,p_derived_column=>'N'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'LAST_UPDATED_BY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4186703988137467)
,p_query_column_id=>33
,p_column_alias=>'STATUS'
,p_column_display_sequence=>24
,p_column_heading=>'Status'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'STATUS'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4186805320137467)
,p_query_column_id=>34
,p_column_alias=>'NOTIFICATION_EMAIL'
,p_column_display_sequence=>23
,p_column_heading=>'Additional Email'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_column_width=>16
,p_derived_column=>'N'
,p_include_in_export=>'Y'
,p_ref_schema=>'EA_APEX'
,p_ref_table_name=>'XXWC_B2B_CONFIG_TBL'
,p_ref_column_name=>'NOTIFICATION_EMAIL'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3489990270797902)
,p_query_column_id=>35
,p_column_alias=>'BUSINESS_EVENT_ELIGIBLE'
,p_column_display_sequence=>36
,p_column_heading=>'Business event eligible'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_when_cond_type=>'EXISTS'
,p_display_when_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT a.user_id',
'FROM  xxwc.xxwc_ms_user_roles_tbl a',
',     xxwc.xxwc_ms_roles_tbl b',
',     xxwc.xxwc_ms_role_resp_tbl c',
',     xxwc.xxwc_ms_responsibility_tbl d',
'WHERE a.role_id = b.role_id(+)',
'AND   b.role_id = c.role_id(+)',
'AND   c.responsibility_id = d.responsibility_id(+)',
'AND   a.user_id= :APP_USER',
'and d.app_id = :APP_ID',
'and c.app_id = :APP_ID',
'and b.role_name = ''B2B BSA'''))
,p_display_as=>'SELECT_LIST'
,p_inline_lov=>'STATIC:Yes;Y,No;N'
,p_lov_show_nulls=>'YES'
,p_derived_column=>'N'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4191319714137481)
,p_query_column_id=>36
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'Select Row'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(4192228474156124)
,p_plug_name=>'SetUp new B2B Trading Partner'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows=>15
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM dual where :P6_USER_ROLE IN (''2'',''3'')'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_plug_header=>'<div style="width=1530px;">'
,p_plug_footer=>'</div>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(4193610297188777)
,p_plug_name=>'Notes Region'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid=>true
,p_plug_grid_column_span=>20
,p_plug_display_column=>2
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_plug_header=>'<div style="width=750px;height:500px;">'
,p_plug_footer=>'</div>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(4195018262191082)
,p_plug_name=>'Notes2 Region'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_plug_header=>'<div style="width=750px;height:500px;">'
,p_plug_footer=>'</div>'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(4489108371753126)
,p_name=>'SetUp new B2B Trading Partner - ReadOnly'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>12
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"ID",',
'"PARTY_ID",',
'"PARTY_NAME",',
'"PARTY_NUMBER",',
'"CUST_ACCOUNT_ID",',
'"ACCOUNT_NUMBER",',
'DECODE(DELIVER_SOA, ''Y'', ''Yes'', ''No'')         "DELIVER_SOA",',
'"SOA_EMAIL",',
'"SOA_PRINT_PRICE",',
'DECODE(DELIVER_ASN, ''Y'', ''Yes'', ''No'')         "DELIVER_ASN",',
'"ASN_EMAIL",',
'DECODE(DELIVER_INVOICE, ''Y'', ''Yes'', ''No'')     "DELIVER_INVOICE",',
'"POD_PRINT_PRICE",',
'"TP_NAME",',
'"ACCOUNT_NAME",',
'"SITE_USE_ID",',
'DECODE(NOTIFY_ACCOUNT_MGR, ''Y'', ''Yes'', ''No'') "NOTIFY_ACCOUNT_MGR",',
'"LOCATION",',
'"DELIVER_POD",',
'"POD_EMAIL",',
'"POD_FREQUENCY",',
'"START_DATE_ACTIVE",',
'"END_DATE_ACTIVE",',
'"POD_LAST_SENT_DATE",',
'"POD_NEXT_SEND_DATE",',
'"CREATION_DATE",',
'"CREATED_BY",',
'"LAST_UPDATE_DATE",',
'"LAST_UPDATED_BY",',
'--"ID" ID_DISPLAY,',
'NVL(STATUS, ''APPROVED'') "STATUS",',
'-- "DELIVER_POA",',
'-- "DEFAULT_EMAIL",',
'"NOTIFICATION_EMAIL"',
'--"COMMENTS",',
'--"APPROVED_DATE",',
'--"CUSTOMER_ID"',
'from "XXWC"."XXWC_B2B_CONFIG_TBL"',
'WHERE (ACCOUNT_NUMBER= :P6_ACCOUNT_NUMBER  ',
'and ((site_use_id IS NULL AND :P6_STATUS_A_I_L IN (''A'',''L'')) OR site_use_id in (SELECT  hcsu.SITE_USE_ID ',
'  FROM apps.hz_cust_site_uses_all  hcsu',
'     , apps.hz_cust_acct_sites_all hcas',
'     , apps.hz_party_sites         hps',
' WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'   AND hps.party_site_id = hcas.party_site_id',
'    AND hps.party_id = :P6_PARTY_ID',
'    AND hcas.STATUS = decode(:P6_STATUS_A_I_L,''A'',''A'',''I'',''I'',hcas.STATUS)',
'   AND hcsu.site_use_code = ''SHIP_TO''',
'   --AND ROWNUM < 100',
'   and cust_account_id = :P6_CUST_ACCOUNT_ID))) OR :P6_ACCOUNT_NUMBER IS NULL'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P6_USER_ROLE = ''1'''
,p_display_condition_type=>'EXISTS'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4489421070753132)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_column_heading=>'ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4489513658753133)
,p_query_column_id=>2
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>2
,p_column_heading=>'PARTY_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4489604529753133)
,p_query_column_id=>3
,p_column_alias=>'PARTY_NAME'
,p_column_display_sequence=>3
,p_column_heading=>'PARTY_NAME'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4489709067753133)
,p_query_column_id=>4
,p_column_alias=>'PARTY_NUMBER'
,p_column_display_sequence=>4
,p_column_heading=>'PARTY_NUMBER'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4489809118753133)
,p_query_column_id=>5
,p_column_alias=>'CUST_ACCOUNT_ID'
,p_column_display_sequence=>5
,p_column_heading=>'CUST_ACCOUNT_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4489919230753133)
,p_query_column_id=>6
,p_column_alias=>'ACCOUNT_NUMBER'
,p_column_display_sequence=>6
,p_column_heading=>'ACCOUNT_NUMBER'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490006209753133)
,p_query_column_id=>7
,p_column_alias=>'DELIVER_SOA'
,p_column_display_sequence=>10
,p_column_heading=>'DELIVER_SOA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490115094753133)
,p_query_column_id=>8
,p_column_alias=>'SOA_EMAIL'
,p_column_display_sequence=>11
,p_column_heading=>'SOA_EMAIL'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490228485753133)
,p_query_column_id=>9
,p_column_alias=>'SOA_PRINT_PRICE'
,p_column_display_sequence=>12
,p_column_heading=>'SOA_PRINT_PRICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490321201753133)
,p_query_column_id=>10
,p_column_alias=>'DELIVER_ASN'
,p_column_display_sequence=>13
,p_column_heading=>'DELIVER_ASN'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490424650753133)
,p_query_column_id=>11
,p_column_alias=>'ASN_EMAIL'
,p_column_display_sequence=>14
,p_column_heading=>'ASN_EMAIL'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490511961753133)
,p_query_column_id=>12
,p_column_alias=>'DELIVER_INVOICE'
,p_column_display_sequence=>15
,p_column_heading=>'DELIVER_INVOICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490624574753133)
,p_query_column_id=>13
,p_column_alias=>'POD_PRINT_PRICE'
,p_column_display_sequence=>7
,p_column_heading=>'POD_PRINT_PRICE'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490700496753133)
,p_query_column_id=>14
,p_column_alias=>'TP_NAME'
,p_column_display_sequence=>20
,p_column_heading=>'TP_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490823360753133)
,p_query_column_id=>15
,p_column_alias=>'ACCOUNT_NAME'
,p_column_display_sequence=>8
,p_column_heading=>'ACCOUNT_NAME'
,p_hidden_column=>'Y'
);
end;
/
begin
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4490902805753133)
,p_query_column_id=>16
,p_column_alias=>'SITE_USE_ID'
,p_column_display_sequence=>9
,p_column_heading=>'SITE_USE_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491024470753133)
,p_query_column_id=>17
,p_column_alias=>'NOTIFY_ACCOUNT_MGR'
,p_column_display_sequence=>19
,p_column_heading=>'NOTIFY_ACCOUNT_MGR'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491111324753133)
,p_query_column_id=>18
,p_column_alias=>'LOCATION'
,p_column_display_sequence=>23
,p_column_heading=>'LOCATION'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491229911753133)
,p_query_column_id=>19
,p_column_alias=>'DELIVER_POD'
,p_column_display_sequence=>16
,p_column_heading=>'DELIVER_POD'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491316056753133)
,p_query_column_id=>20
,p_column_alias=>'POD_EMAIL'
,p_column_display_sequence=>18
,p_column_heading=>'POD_EMAIL'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491421978753133)
,p_query_column_id=>21
,p_column_alias=>'POD_FREQUENCY'
,p_column_display_sequence=>17
,p_column_heading=>'POD_FREQUENCY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491512956753133)
,p_query_column_id=>22
,p_column_alias=>'START_DATE_ACTIVE'
,p_column_display_sequence=>24
,p_column_heading=>'START_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491603726753133)
,p_query_column_id=>23
,p_column_alias=>'END_DATE_ACTIVE'
,p_column_display_sequence=>25
,p_column_heading=>'END_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491722488753133)
,p_query_column_id=>24
,p_column_alias=>'POD_LAST_SENT_DATE'
,p_column_display_sequence=>26
,p_column_heading=>'POD_LAST_SENT_DATE'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491823488753133)
,p_query_column_id=>25
,p_column_alias=>'POD_NEXT_SEND_DATE'
,p_column_display_sequence=>27
,p_column_heading=>'POD_NEXT_SEND_DATE'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4491900823753133)
,p_query_column_id=>26
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>28
,p_column_heading=>'CREATION_DATE'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4492003453753133)
,p_query_column_id=>27
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>29
,p_column_heading=>'CREATED_BY'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4492101586753133)
,p_query_column_id=>28
,p_column_alias=>'LAST_UPDATE_DATE'
,p_column_display_sequence=>30
,p_column_heading=>'LAST_UPDATE_DATE'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4492211989753133)
,p_query_column_id=>29
,p_column_alias=>'LAST_UPDATED_BY'
,p_column_display_sequence=>31
,p_column_heading=>'LAST_UPDATED_BY'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4492326317753133)
,p_query_column_id=>30
,p_column_alias=>'STATUS'
,p_column_display_sequence=>22
,p_column_heading=>'STATUS'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(4492426208753133)
,p_query_column_id=>31
,p_column_alias=>'NOTIFICATION_EMAIL'
,p_column_display_sequence=>21
,p_column_heading=>'NOTIFICATION_EMAIL'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(4510604026388532)
,p_plug_name=>'Questionaire Region - SalesRep'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P6_USER_ROLE = ''2'''
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(4188922359137478)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(4184728197137464)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Delete'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'SELECT ''1'' FROM DUAL WHERE :P6_USER_ROLE = ''3'''
,p_button_condition_type=>'EXISTS'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(4189023140137478)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(4184728197137464)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BOTTOM'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'javascript:apex.widget.tabular.addRow();'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(4188830384137478)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(4192228474156124)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Submit'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(4188717825137478)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(4192228474156124)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(4510006117955419)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(4489108371753126)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(4191423362137481)
,p_branch_action=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3289111544158016)
,p_name=>'P6_TEST_ITEM'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(4184728197137464)
,p_use_cache_before_default=>'NO'
,p_source=>'P6_TEST_ITEM'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3349677691973102)
,p_name=>'P6_STATUS_A_I_L'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(4192228474156124)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3848000312640164)
,p_name=>'P6_SETUP_INSTRUCTION'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(4193610297188777)
,p_pre_element_text=>'<BR><B><U> Setup Instructions </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Be Sure to Visit the Sales Resource Toolbox to View All Order Status Update Training Materials.</BR>',
'<BR>HD Supply Intranet > Our Work > Sales, Marketing & Communications > Sales Resource Toolbox </BR>',
'<BR>See Below for XXWC B2B Trading Partner Setup Instructions.</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3848419049661839)
,p_name=>'P6_ADD_ROWS'
,p_item_sequence=>31
,p_item_plug_id=>wwv_flow_api.id(4193610297188777)
,p_pre_element_text=>'<BR><B><U> Add Row </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Click “Add Row” to Begin the Activation Process. </BR> ',
'<BR>Also, to Activate Customers by Specific Jobs (Instead of “All” Jobs), use “Add Row” for Each Additional Job Site You Wish to Activate.',
'</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3850700939928477)
,p_name=>'P6_SUBMIT'
,p_item_sequence=>224
,p_item_plug_id=>wwv_flow_api.id(4195018262191082)
,p_pre_element_text=>'<BR><B><U> Submit </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Be Sure to Click Submit for Oracle to Recognize Any New Activations, or Changes Made to any Already Activated Accounts.',
'</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4192831454156125)
,p_name=>'P6_CUSTOMER_NAME'
,p_item_sequence=>64
,p_item_plug_id=>wwv_flow_api.id(4192228474156124)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P6_CUSTOMER_NAME'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'<b>Customer Name:</b>'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select hc.account_name from apps.hz_Cust_accounts_all hc, apps.hz_parties hp  ',
'where hc.account_number = :P6_ACCOUNT_NUMBER ',
'and hc.party_id = hp.party_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4193026341156125)
,p_name=>'P6_ACCOUNT_NUMBER'
,p_item_sequence=>84
,p_item_plug_id=>wwv_flow_api.id(4192228474156124)
,p_prompt=>'<b>Customer Number:</b>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4193217153156125)
,p_name=>'P6_USER_NAME'
,p_item_sequence=>64
,p_item_plug_id=>wwv_flow_api.id(4192228474156124)
,p_prompt=>'<b>User Name:</b>'
,p_source=>'SELECT REPLACE(DESCRIPTION,'','', '''') FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER)'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_grid_column=>7
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4193407946156125)
,p_name=>'P6_NTID'
,p_item_sequence=>84
,p_item_plug_id=>wwv_flow_api.id(4192228474156124)
,p_prompt=>'<b>NT Id:</b>'
,p_source=>'SELECT USER_NAME FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER)'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_colspan=>7
,p_grid_column=>7
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4193813390188777)
,p_name=>'P6_SOA_EMAIL'
,p_item_sequence=>42
,p_item_plug_id=>wwv_flow_api.id(4193610297188777)
,p_pre_element_text=>'<BR><B><U> SOA Email </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Optional Field.  Enter the Recipient Email Address Here if You Desire the Contact to Receive Every SOA.</BR>',
'<BR>To Enter Multiple Recipient Email Addresses, Simply Separate Each Email Address with a Comma.</BR>',
'<BR>To Vary and Send SOAs to the Bill To Contacts Entered into Sales Orders, Leave this Field Blank. Instead, Input the Customer Email Addresses in the XXWC Customer Contacts Form (Accessed through the Tools Menu in the Sales Order Form).</BR>',
'<BR>***Upon Sales Order Booking, SOAs are Sent via Email to the Contacts in the “SOA Email” Field Above, and/or the Bill To Contacts in Sales Orders, if Email Addresses Exist in the XXWC Customer Contacts Form.</BR>',
'</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4194029242188778)
,p_name=>'P6_ASN_EMAIL'
,p_item_sequence=>122
,p_item_plug_id=>wwv_flow_api.id(4195018262191082)
,p_pre_element_text=>'<BR><B><U> ASN Email</U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR> Optional Field.  Enter the Recipient Email Address Here if You Desire the Contact to Receive Every ASN.</BR>',
'<BR>To Enter Multiple Recipient Email Addresses, Simply Separate Each Email Address with a Comma.</BR>',
'<BR>To Vary and Send ASNs to the Bill To/Ship To Contacts Entered into Sales Orders, Leave this Field Blank. </BR><BR>Instead, Input the Customer Email Addresses in the XXWC Customer Contacts Form (Accessed through the Tools Menu in the Sales Order F'
||'orm).</BR>',
'<BR>***When the Delivery Docs are Printed, ASNs are Sent via Email to the Contacts in the “SOA Email” and “ASN Email” Fields Above, and/or the Ship To/Bill To Contacts in Sales Orders, if Email Addresses Exist in the XXWC Customer Contacts Form.',
'</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4194206621188778)
,p_name=>'P6_NOTES_POD_EMAIL'
,p_item_sequence=>123
,p_item_plug_id=>wwv_flow_api.id(4195018262191082)
,p_pre_element_text=>'<BR><B><U> POD Email </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Enter the Recipient Email Address Here.</BR>',
'<BR>To Enter Multiple Recipient Email Addresses, Simply Separate Each Email Address with a Comma.</BR>',
'<BR>***PODs are Sent in the Very Early Morning, After Invoicing has Occurred.  </BR> This Allows the Invoice Number (Just the Number, Not the Actual Invoice) to be Included in the Email for Better Reconciliation.</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4194409515188778)
,p_name=>'P6_NOTES_NOTIF_ACCTMGR'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(4193610297188777)
,p_pre_element_text=>'<BR><B><U> Notify Account Mgr </U></B></BR>'
,p_post_element_text=>'<BR>* This field is optional. When select "yes" <U>all</U> AM''s associated with the Customer Account will receive copies of every POD.</BR>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4194614409188778)
,p_name=>'P6_NOTES_LAST_NEXT_DATE'
,p_item_sequence=>54
,p_item_plug_id=>wwv_flow_api.id(4193610297188777)
,p_pre_element_text=>'<BR><B><U> POD Last/Next Sent Date </U></B></BR>'
,p_post_element_text=>'<BR>* These fields are view only and indicate the last date the POD was sent and the next date the POD will be sent.</BR>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_display_when_type=>'NEVER'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4194830528188778)
,p_name=>'P6_NOTES_JOB_SITE'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(4193610297188777)
,p_pre_element_text=>'<BR><B><U> Job Site </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Optional field.  Only Change the Setting from “All” When:</BR>',
'<BR>* Customer Contacts Vary by Job Site</BR>',
'<BR>* Or, When Attempting to Send Documents for Select Job Sites',
'<BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4195200839191083)
,p_name=>'P6_DELIVER_SOA'
,p_item_sequence=>41
,p_item_plug_id=>wwv_flow_api.id(4193610297188777)
,p_pre_element_text=>'<BR><B><U> Deliver SOA </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR> Select “Yes” if You Wish to Send a Sales Order Acknowledgment to Your Customer for Every Counter & Standard Order.</BR>',
'<BR> If the Customer is Already Active, Select “Deactivate” and then “Submit” to Turn Off the SOA.</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4195410111191083)
,p_name=>'P6_DELIVER_ASN'
,p_item_sequence=>43
,p_item_plug_id=>wwv_flow_api.id(4193610297188777)
,p_pre_element_text=>'<BR><B><U> Deliver ASN </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Select “Yes” if You Wish to Send an Advance Ship Notice to Your Customer for Every Standard Order.</BR>',
'<BR>If the Customer is Already Active, Select “Deactivate” and then “Submit” to Turn Off the ASN.</BR>',
'<BR>The Pricing Display for the ASN Follows the Customer Print Price Flag Settings.',
'</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4195618238191083)
,p_name=>'P6_ACCT_MGR'
,p_item_sequence=>124
,p_item_plug_id=>wwv_flow_api.id(4195018262191082)
,p_pre_element_text=>'<BR><B><U> Notify Account Manager </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Optional Field. </BR>',
'<BR>Select “Yes” When there is Only One Account Manager Assigned to the Entire Account, and He or She Wishes to Receive Copies of Every Activated Order Status Update Document.</BR>',
'<BR> Do Not Select This Field if There are Multiple AMs Servicing the Account. </BR>',
'<BR>If Multiple AMs are Assigned to an Account and Only One AM Wants to be Copied on the Emails, Simply Type Their Email Address Into the SOA Email, ASN Email and/or POD Email Fields.',
'</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4195829336191083)
,p_name=>'P6_DEVLIVER_POD'
,p_item_sequence=>122
,p_item_plug_id=>wwv_flow_api.id(4195018262191082)
,p_pre_element_text=>'<BR><B><U> Deliver POD </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Select “Yes” if You Wish to Send a Proof of Delivery to Your Customer for Every Counter & Standard Order (Note:  PODs are Not Sent for Salesperson Deliveries and Direct Shipments).</BR>',
'<BR>If the Customer is Already Active, Select “Deactivate” and then “Submit” to Turn Off the POD.</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4196029896191083)
,p_name=>'P6_POD_FREQUENCY'
,p_item_sequence=>122
,p_item_plug_id=>wwv_flow_api.id(4195018262191082)
,p_pre_element_text=>'<BR><B><U> POD Frequency </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Select “Daily” to Send PODs Each Day Invoices are Created.',
'</BR>',
'<BR>Select “Weekly” to Batch and Send PODs Once Every Seven Days. If Weekly is Selected, PODs will be Sent on the Same Weekday as the Start Date.</BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4196327879197649)
,p_name=>'P6_PARTY_ID'
,p_item_sequence=>154
,p_item_plug_id=>wwv_flow_api.id(4184728197137464)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P6_PARTY_ID'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select hc.party_id from apps.hz_Cust_accounts_all hc, apps.hz_parties hp  ',
'where hc.account_number = :P6_ACCOUNT_NUMBER ',
'and hc.party_id = hp.party_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4196520546201014)
,p_name=>'P6_PARTY_NUMBER'
,p_item_sequence=>164
,p_item_plug_id=>wwv_flow_api.id(4184728197137464)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P6_PARTY_NUMBER'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'Party Number :'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select hp.party_number from apps.hz_Cust_accounts_all hc, apps.hz_parties hp  ',
'where hc.account_number = :P6_ACCOUNT_NUMBER ',
'and hc.party_id = hp.party_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4196714292203962)
,p_name=>'P6_PARTY_NAME'
,p_item_sequence=>174
,p_item_plug_id=>wwv_flow_api.id(4184728197137464)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P6_PARTY_NAME'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'Party Name :'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select hp.party_name ',
'from apps.hz_Cust_accounts_all hc, apps.hz_parties hp  ',
'where hc.account_number = :P6_ACCOUNT_NUMBER ',
'and hc.party_id = hp.party_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4196907822206974)
,p_name=>'P6_CUST_ACCOUNT_ID'
,p_item_sequence=>184
,p_item_plug_id=>wwv_flow_api.id(4184728197137464)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P6_CUST_ACCOUNT_ID'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select hc.cust_account_id ',
'from apps.hz_Cust_accounts_all hc, apps.hz_parties hp  ',
'where hc.account_number = :P6_ACCOUNT_NUMBER ',
'and hc.party_id = hp.party_id;'))
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4197102647209329)
,p_name=>'P6_USER_ROLE'
,p_item_sequence=>194
,p_item_plug_id=>wwv_flow_api.id(4184728197137464)
,p_item_default=>'3'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4197329807211895)
,p_name=>'P6_INSTANCE'
,p_item_sequence=>204
,p_item_plug_id=>wwv_flow_api.id(4184728197137464)
,p_use_cache_before_default=>'NO'
,p_source=>'SELECT SUBSTR (UPPER (global_name), 1, 7) FROM global_name'
,p_source_type=>'QUERY'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4227400780144420)
,p_name=>'P6_STATUS'
,p_item_sequence=>214
,p_item_plug_id=>wwv_flow_api.id(4184728197137464)
,p_item_default=>'DRAFT'
,p_prompt=>'Status :'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4477110426163406)
,p_name=>'P6_SOA_PRINT_PRICE_NOTE'
,p_item_sequence=>42
,p_item_plug_id=>wwv_flow_api.id(4193610297188777)
,p_pre_element_text=>'<BR><B><U> SOA Print Price </U></B></BR>'
,p_post_element_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<BR>Select “Yes” or “No”.  When “Yes” is selected, the SOA will Include Selling Prices at the Time of Order Booking.</BR>',
'<BR> When “No” is selected, SOA will Not Include Selling Prices.  </BR>'))
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'LEFT-TOP'
,p_field_alignment=>'LEFT-TOP'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4510811590388534)
,p_name=>'P6_SALESREP'
,p_item_sequence=>27
,p_item_plug_id=>wwv_flow_api.id(4184728197137464)
,p_prompt=>'Select Account Manager'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT DISTINCT REPLACE(jrre.source_name, '','', '''') d, jrre.user_name r',
'  FROM apps.hz_cust_acct_sites_all     hcas',
'     , apps.hz_cust_accounts_all       hca',
'     , apps.hz_cust_site_uses_all      hcsu',
'     , apps.jtf_rs_salesreps           jrs',
'     , apps.jtf_rs_resource_extns      jrre',
' WHERE 1 = 1',
'   AND hcas.cust_account_id     = hca.cust_account_id',
'-- AND hca.party_id             = :P6_PARTY_ID',
'   AND hcas.cust_acct_site_id   = hcsu.cust_acct_site_id',
'   AND hcsu.primary_salesrep_id = jrs.salesrep_id',
'   AND jrs.resource_id          = jrre.resource_id',
'   AND jrs.org_id               = hcas.org_id',
'   AND jrre.user_name           IS NOT NULL',
'   AND jrre.source_name         IS NOT NULL',
'   AND hcas.status              = ''A''',
'   AND hcsu.status              = ''A''',
'   AND hcas.org_id              = 162',
'   AND hcsu.org_id              = 162',
'   AND jrs.org_id              = 162',
'   AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)',
'and hca.account_number = :P6_ACCOUNT_NUMBER '))
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'SUBMIT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4511020102388535)
,p_name=>'P6_SALESREP_VALUE'
,p_item_sequence=>111
,p_item_plug_id=>wwv_flow_api.id(4510604026388532)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Salesrep Value'
,p_source=>'P6_SALESREP'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4511220050388535)
,p_name=>'P6_NOTIFY_ACCT_MGRS'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(4510604026388532)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P6_NOTIFY_ACCT_MGRS'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'If yes, have you notified All Account Managers ?'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'YN2'
,p_lov=>'.'||wwv_flow_api.id(2531513372639611)||'.'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'--'
,p_lov_null_value=>'-1'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'SELECT ''1'' FROM xxwc.xxwc_b2b_config_tbl where ACCOUNT_NUMBER = :P6_ACCOUNT_NUMBER;'
,p_display_when_type=>'NOT_EXISTS'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
end;
/
begin
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4511422049388535)
,p_name=>'P6_MULTIPLE_ACCT_MGRS'
,p_item_sequence=>35
,p_item_plug_id=>wwv_flow_api.id(4510604026388532)
,p_use_cache_before_default=>'NO'
,p_item_default=>':P6_MULTIPLE_ACCT_MGRS'
,p_item_default_type=>'PLSQL_EXPRESSION'
,p_prompt=>'Is your account shared by multiple Account Managers ?'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'YN2'
,p_lov=>'.'||wwv_flow_api.id(2531513372639611)||'.'
,p_lov_display_null=>'YES'
,p_lov_null_text=>'--'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_field_alignment=>'LEFT-CENTER'
,p_display_when=>'SELECT ''1'' FROM xxwc.xxwc_b2b_config_tbl where  ACCOUNT_NUMBER = :P6_ACCOUNT_NUMBER;'
,p_display_when_type=>'NOT_EXISTS'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(4511625977388535)
,p_name=>'P6_PARTY_ID_1'
,p_item_sequence=>58
,p_item_plug_id=>wwv_flow_api.id(4510604026388532)
,p_display_as=>'NATIVE_HIDDEN'
,p_cMaxlength=>4000
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(4457603397532145)
,p_tabular_form_region_id=>wwv_flow_api.id(4184728197137464)
,p_validation_name=>'POD_EMAIL'
,p_validation_sequence=>10
,p_validation=>'POD_EMAIL'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'POD Email cannot be empty. Please enter a valid email address.'
,p_always_execute=>'N'
,p_validation_condition=>'DELIVER_POD'
,p_validation_condition2=>'Y'
,p_validation_condition_type=>'VAL_OF_ITEM_IN_COND_EQ_COND2'
,p_when_button_pressed=>wwv_flow_api.id(4188830384137478)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'POD_EMAIL'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(4457830773534655)
,p_tabular_form_region_id=>wwv_flow_api.id(4184728197137464)
,p_validation_name=>'POD_CUSTOMER'
,p_validation_sequence=>20
,p_validation=>'CUST_ACCOUNT_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'"Cust Name - Cust Num" cannot be empty. Please select a valid Customer.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(4188830384137478)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'CUST_ACCOUNT_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(4461832018594838)
,p_tabular_form_region_id=>wwv_flow_api.id(4184728197137464)
,p_validation_name=>'SITE_USE_ID'
,p_validation_sequence=>30
,p_validation=>'SITE_USE_ID'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Job Site cannot be empty. Please enter a valid Job Site.'
,p_always_execute=>'N'
,p_validation_condition_type=>'NEVER'
,p_when_button_pressed=>wwv_flow_api.id(4188830384137478)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'SITE_USE_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(4198213723237028)
,p_name=>'Find User Role'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
,p_display_when_type=>'NEVER'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4198506407237029)
,p_event_id=>wwv_flow_api.id(4198213723237028)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  l_salesrep_flag   VARCHAR2(1);',
'  l_user_name       VARCHAR2(30);',
'  l_instance        VARCHAR2(50);',
'  l_end_date_active DATE;',
'  l_salesrep_name   VARCHAR2(200);',
'BEGIN',
' IF :P6_INSTANCE IS NULL THEN',
'    BEGIN',
'      SELECT SUBSTR(UPPER(global_name), 1, 7)',
'        INTO l_instance',
'        FROM global_name;',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        l_instance := NULL;',
'    END;',
'  ',
'    :P6_INSTANCE := l_instance;',
'  END IF;',
'  ',
'  IF :P6_USER_ROLE IN (''1'', ''2'') THEN',
'    l_user_name := nvl(v(''APP_USER''), USER);',
'    SELECT apps.xxwc_b2b_so_ib_pkg.is_sales_rep(:P6_CUST_ACCOUNT_ID, l_user_name)',
'      INTO l_salesrep_flag',
'      FROM dual;',
'  ',
'    IF l_salesrep_flag IN (''Y'', ''0'') THEN',
'      :P6_USER_ROLE := ''2'';',
'      IF l_salesrep_flag = ''Y'' THEN',
'        :P6_SALESREP := l_user_name;',
'      END IF;',
'    ',
'    ELSE',
'      :P6_USER_ROLE := ''1'';',
'    END IF;',
'  ',
'  END IF;',
'   ',
'  l_user_name := nvl(v(''APP_USER''), USER);',
'',
'  IF :P6_NTID IS NULL THEN',
'    :P6_NTID := l_user_name;',
'  END IF;',
'',
'  IF :P6_USER_NAME IS NULL THEN',
'    BEGIN',
'      SELECT REPLACE(DESCRIPTION, '','', '''')',
'        INTO :P6_USER_NAME',
'        FROM APPS.FND_USER',
'       WHERE USER_NAME = l_user_name',
'         AND ROWNUM = 1;',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        NULL;',
'    END;',
'  END IF;',
'  BEGIN',
'    SELECT end_date_active',
'      INTO l_end_date_active',
'      FROM xxwc.xxwc_b2b_config_tbl',
'     WHERE ACCOUNT_NUMBER = :P6_ACCOUNT_NUMBER;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      NULL;',
'  END;',
'    :P6_END_DATE_ACTIVE := l_end_date_active;',
'  ',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    dbms_output.put_line(''Error - '' || SQLERRM);',
'END;'))
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(4513823222255048)
,p_name=>'Multiple Acct Managers - Yes'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6_MULTIPLE_ACCT_MGRS'
,p_condition_element=>'P6_MULTIPLE_ACCT_MGRS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'Y'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4514129105255049)
,p_event_id=>wwv_flow_api.id(4513823222255048)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6_NOTIFY_ACCT_MGRS'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4514330582255049)
,p_event_id=>wwv_flow_api.id(4513823222255048)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6_NOTIFY_ACCT_MGRS'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(4514431879257504)
,p_name=>'Multiple Acct Managers - No'
,p_event_sequence=>30
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6_MULTIPLE_ACCT_MGRS'
,p_condition_element=>'P6_MULTIPLE_ACCT_MGRS'
,p_triggering_condition_type=>'EQUALS'
,p_triggering_expression=>'N'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4514711187257504)
,p_event_id=>wwv_flow_api.id(4514431879257504)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4514915410257504)
,p_event_id=>wwv_flow_api.id(4514431879257504)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(4515010539260851)
,p_name=>'Notify Acct Managers - Change'
,p_event_sequence=>40
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6_NOTIFY_ACCT_MGRS'
,p_triggering_condition_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_expression=>'$v(''P6_NOTIFY_ACCT_MGRS'') == $v(''P6_MULTIPLE_ACCT_MGRS'')'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4515312439260851)
,p_event_id=>wwv_flow_api.id(4515010539260851)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'REGION'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4515529461260851)
,p_event_id=>wwv_flow_api.id(4515010539260851)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(4515623353264501)
,p_name=>'Acct Managers - Change By User'
,p_event_sequence=>50
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P6_SALESREP'
,p_condition_element=>'P6_SALESREP'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4515906892264501)
,p_event_id=>wwv_flow_api.id(4515623353264501)
,p_event_result=>'TRUE'
,p_action_sequence=>5
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'   l_ntid         VARCHAR2(30);',
'   l_user_name    VARCHAR2(200);',
'BEGIN',
'   SELECT jrre.user_name',
'        , REPLACE(jrre.source_name, '','', '''')',
'     INTO l_ntid',
'        , l_user_name',
'     FROM APPS.jtf_rs_salesreps           jrs',
'        , APPS.jtf_rs_resource_extns      jrre',
'    WHERE jrs.salesrep_id               = :P6_SALESREP',
'      AND jrs.resource_id               = jrre.resource_id',
'      AND jrs.org_id                    = 162',
'      AND jrre.source_name             IS NOT NULL',
'      AND SYSDATE BETWEEN jrre.start_Date_active and NVL(jrre.end_date_active, SYSDATE+1)',
'      AND ROWNUM = 1',
'      ;',
'   :P6_SALESREP_VALUE := :P6_SALESREP;',
'EXCEPTION',
'WHEN OTHERS THEN',
'  NULL;',
'END;'))
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4516125357264501)
,p_event_id=>wwv_flow_api.id(4515623353264501)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SHOW'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6_MULTIPLE_ACCT_MGRS'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(4516305887264501)
,p_event_id=>wwv_flow_api.id(4515623353264501)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6_MULTIPLE_ACCT_MGRS'
,p_attribute_01=>'N'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(3289251618158017)
,p_name=>'fill value'
,p_event_sequence=>60
,p_condition_element=>'P6_TEST_ITEM'
,p_triggering_condition_type=>'NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
,p_display_when_type=>'ITEM_IS_NULL'
,p_display_when_cond=>'P6_TEST_ITEM'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3354477718362801)
,p_event_id=>wwv_flow_api.id(3289251618158017)
,p_event_result=>'FALSE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CANCEL_EVENT'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3324104743373406)
,p_event_id=>wwv_flow_api.id(3289251618158017)
,p_event_result=>'TRUE'
,p_action_sequence=>40
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6_TEST_ITEM'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'2'
,p_attribute_09=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3354662782362803)
,p_event_id=>wwv_flow_api.id(3289251618158017)
,p_event_result=>'TRUE'
,p_action_sequence=>60
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_SUBMIT_PAGE'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3354739184362804)
,p_event_id=>wwv_flow_api.id(3289251618158017)
,p_event_result=>'TRUE'
,p_action_sequence=>70
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_SET_VALUE'
,p_affected_elements_type=>'ITEM'
,p_affected_elements=>'P6_TEST_ITEM'
,p_attribute_01=>'STATIC_ASSIGNMENT'
,p_attribute_02=>'2'
,p_attribute_09=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(3354502206362802)
,p_event_id=>wwv_flow_api.id(3289251618158017)
,p_event_result=>'TRUE'
,p_action_sequence=>80
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_CANCEL_EVENT'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(4190918433137480)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(4184728197137464)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_CONFIG_TBL'
,p_attribute_03=>'ID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(4188830384137478)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(4191129020137481)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(4184728197137464)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_CONFIG_TBL'
,p_attribute_03=>'ID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(4197826059231177)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Update Status - Submit'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'',
'   l_subject              VARCHAR2 (32767) DEFAULT NULL;',
'   l_body                 VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_header          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_detail          VARCHAR2 (32767) DEFAULT NULL;',
'   l_body_footer          VARCHAR2 (32767) DEFAULT NULL;',
'   l_instance             VARCHAR2(50) := ''EBZIDEV'';',
'   l_to_sr_email_id       VARCHAR2 (32767) DEFAULT NULL;',
'   l_to_mgr_email_id      VARCHAR2 (32767) DEFAULT NULL;',
'   l_user_full_name            VARCHAR2(250);',
'   l_user_name                 VARCHAR2(50);',
'   l_to_email             VARCHAR2(32767);',
'  -- l_dflt_email           VARCHAR2(100) := ''Gopi.Damuluri@HDSupply.com'';',
'',
'   l_dflt_email           VARCHAR2(100) := ''perry.njuguna@hdsupply.com'';',
'   l_cust_account_id      NUMBER;',
'   l_site_use_id          NUMBER;',
'',
'   l_account_name         VARCHAR2(200);',
'   l_location             VARCHAR2(200);',
'   l_checked_row          NUMBER;',
'   l_exception            EXCEPTION;',
'',
'BEGIN',
'',
'   :P6_STATUS := ''APPROVED'';',
'',
'    -------------------------------------------------------',
'    -- Derive User Name and NTID',
'    -------------------------------------------------------',
'   SELECT REPLACE(description,'','', '''') ',
'        , user_name',
'     INTO l_user_full_name',
'        , l_user_name',
'     FROM APPS.FND_USER WHERE USER_NAME = nvl(v(''APP_USER''),USER);',
'',
'      -------------------------------------------------------',
'      -- Derive Oracle Instance Name',
'      -------------------------------------------------------',
'     BEGIN',
'        SELECT SUBSTR (UPPER (global_name), 1, 7)',
'          INTO l_instance',
'          FROM global_name;',
'     EXCEPTION',
'     WHEN OTHERS THEN',
'        l_instance := NULL;',
'     END;',
'',
'     --------------------------------------------------',
'     -- Derive SalesRep Email-Id',
'     --------------------------------------------------',
'     BEGIN',
'        SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_rep_email (:P6_CUST_ACCOUNT_ID)',
'          INTO l_to_sr_email_id',
'          FROM DUAL;',
'     EXCEPTION',
'     WHEN OTHERS THEN',
'       l_to_sr_email_id := NULL;',
'     END;',
'',
'     --------------------------------------------------',
'     -- Derive SalesRep Manager Email-Id',
'     --------------------------------------------------',
'     BEGIN',
'        SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_mgr_email (:P6_CUST_ACCOUNT_ID, l_user_name)',
'          INTO l_to_mgr_email_id',
'          FROM DUAL;',
'     EXCEPTION',
'     WHEN OTHERS THEN',
'       l_to_mgr_email_id := NULL;',
'     END;',
'',
'     l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'',
'    l_to_sr_email_id := l_to_sr_email_id||'',''||l_to_mgr_email_id;',
'    IF l_instance = ''EBIZPRD'' THEN',
'       l_to_email    := l_to_sr_email_id;',
'       l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                     ;',
'    ELSE',
'       l_to_email    := l_dflt_email;',
'       l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>''',
'                        ||''<BR> Email Ids - ''||l_to_sr_email_id||''</BR> ''',
'                     ;',
'    END IF;',
'',
'  FOR i IN 1..apex_application.g_f01.count LOOP',
'',
'    l_checked_row     := NULL;',
'    l_cust_account_id := NULL;',
'    l_site_use_id     := NULL;',
'    l_account_name    := NULL;',
'    l_location        := NULL;',
'',
'    BEGIN',
'    -------------------------------------------------------',
'    -- Identify the row-number that has been Checked',
'    -------------------------------------------------------',
'    l_checked_row := apex_application.g_f01(i);',
'',
'    IF NVL(l_checked_row,0) = 0 THEN',
'       SELECT COUNT(1)',
'         INTO l_checked_row',
'         FROM xxwc.XXWC_B2B_CONFIG_TBL',
'        WHERE party_id = :P6_PARTY_ID;',
'    END IF;',
'',
'    l_cust_account_id    := apex_application.g_f24(l_checked_row);',
'',
'    BEGIN',
'    l_site_use_id        := apex_application.g_f06(l_checked_row);',
'    EXCEPTION ',
'    WHEN OTHERS THEN',
'    NULL;',
'    END;',
'',
'    -------------------------------------------------------',
'    -- Derive Customer Name',
'    -------------------------------------------------------',
'    IF l_cust_account_id IS NOT NULL THEN',
'       SELECT account_name||'' - ''||account_number',
'         INTO l_account_name',
'         FROM apps.hz_cust_accounts_all hca',
'        WHERE cust_account_id = l_cust_account_id',
'          AND ROWNUM = 1;',
'    END IF;',
'',
'    -------------------------------------------------------',
'    -- Derive Job Location Name',
'    -------------------------------------------------------',
'    IF l_site_use_id IS NOT NULL THEN',
'       SELECT location',
'         INTO l_location',
'         FROM apps.hz_cust_site_uses_all hcsu',
'        WHERE site_use_id = l_site_use_id',
'          AND ROWNUM = 1;',
'    END IF;',
'',
'    IF l_location IS NULL THEN',
'',
'       l_subject     := l_account_name||'' is setup for Proof Of Delivery notification'';',
'',
'       l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                        ''<BR> </BR> <BR> Your request to set-up <B>'' ||l_account_name|| ''</B> for Proof Of Delivery notification has been completed. </BR>''||',
'                        ''<BR> </BR> <BR> Please promptly notify all impacted branches and sales associates.''||',
'                        ''<BR> </BR> <BR> Submitted By : <B> ''||l_user_full_name',
'                        ;',
'    ELSE',
'',
'       l_subject     := l_account_name||'' is setup for Proof Of Delivery notification for JobSite - ''||l_location;',
'',
'       l_body_detail := ''<BR> <B> ''||l_instance||''</B> ''||',
'                        ''<BR> </BR> <BR> Your request to set-up <B>'' ||l_account_name|| ''</B> for Proof Of Delivery notification has been completed. </BR>''||',
'                        ''<BR> </BR> <BR> Job Site Name - <B>''||l_location||',
'                        ''</B><BR> </BR> <BR> Please promptly notify all impacted branches and sales associates.''||',
'                        ''<BR> </BR> <BR> Submitted By : <B> ''||l_user_full_name',
'                        ;',
'    END IF; -- IF location IS NULL THEN',
'',
'    l_body        := l_body_header || l_body_detail ||chr(10)|| l_body_footer;',
'',
'    IF l_to_email IS NOT NULL THEN',
'       APEX_MAIL.SEND(p_to                        => l_to_email,',
'                      p_from                      => ''no-reply@whitecap.net'',',
'                      p_body                      => l_body,',
'                      p_body_html                 => l_body,',
'                      p_subj                      => l_subject,',
'                      p_replyto                   => NULL);',
'    END IF;',
'    COMMIT;',
'  ',
'  EXCEPTION',
'  WHEN l_exception THEN',
'    NULL;',
'  END;',
'',
'END LOOP; --   FOR i IN 1..apex_application.g_f01.count LOOP',
'',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(4188830384137478)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(4198030561232459)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'MRU - Send Email'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  l_subject         VARCHAR2(32767) DEFAULT NULL;',
'  l_body            VARCHAR2(32767) DEFAULT NULL;',
'  l_body_header     VARCHAR2(32767) DEFAULT NULL;',
'  l_body_detail     VARCHAR2(32767) DEFAULT NULL;',
'  l_body_footer     VARCHAR2(32767) DEFAULT NULL;',
'  l_instance        VARCHAR2(50);',
'  l_to_mgr_email_id VARCHAR2(32767) DEFAULT NULL;',
'  l_to_sr_email_id  VARCHAR2(32767) DEFAULT NULL;',
'  l_status          VARCHAR2(50);',
'  l_end_date_active DATE;',
'  l_user_name       VARCHAR2(250) := NVL(:P6_USER_NAME, :P6_NTID);',
'  l_log_in_user     VARCHAR2(250);',
'  l_url             VARCHAR2(200);',
'  l_to_email        VARCHAR2(32767);',
'  -- l_dflt_email           VARCHAR2(100) := ''Gopi.Damuluri@HDSupply.com, Sha.Angel@hdsupply.com, Lorraine.Charles@hdsupply.com'';',
'  l_dflt_email      VARCHAR2(100) := ''perry.njuguna@hdsupply.com'';',
'  l_sr_setup_status VARCHAR2(100);',
'  l_salesrep_name   VARCHAR2(200);',
'BEGIN',
'  IF :P6_USER_ROLE = ''2'' AND :P6_NTID != :P6_SALESREP THEN',
'    IF :P6_USER_NAME IS NOT NULL AND :P6_SALESREP IS NOT NULL THEN',
'      l_salesrep_name := apps.xxwc_b2b_so_ib_pkg.get_sales_rep_name(NULL,',
'                                                                    :P6_SALESREP);',
'      l_user_name     := :P6_USER_NAME || ''</B> on behalf of - <B>'' ||',
'                         l_salesrep_name;',
'    END IF;',
'  END IF;',
'  l_instance := :P6_INSTANCE;',
'  BEGIN',
'    SELECT NVL(status, ''DRAFT'')',
'      INTO l_status',
'      FROM xxwc.XXWC_B2B_CONFIG_TBL',
'     WHERE party_id = :P6_PARTY_ID',
'       AND ROWNUM = 1;',
'  EXCEPTION',
'    WHEN NO_DATA_FOUND THEN',
'      l_status := ''NO_DATA'';',
'    WHEN OTHERS THEN',
'      l_status := ''DRAFT'';',
'  END;',
'  IF l_status = ''DRAFT'' THEN',
'    l_sr_setup_status                        := ''APPROVED'';',
'    apex_application.g_print_success_message := ''Trading Partner is added to B2B.'';',
'  ELSE',
'    IF l_status = ''NO_DATA'' THEN',
'      apex_application.g_print_success_message := ''Cannot Proceed.'';',
'    END IF;',
'  END IF;',
'  IF :P6_USER_ROLE = ''3'' THEN',
'    -- ADMIN',
'    l_end_date_active := apex_application.g_f23(1);',
'    ------------------------------------------',
'    -- Update XXWC.xxwc_b2b_config_tbl',
'    ------------------------------------------',
'    UPDATE XXWC.XXWC_B2B_CONFIG_TBL',
'       SET /*DELIVER_POA         = apex_application.g_f03 (1)',
'           , */        DELIVER_SOA = apex_application.g_f07(1),',
'           SOA_EMAIL          = apex_application.g_f08(1),',
'           DELIVER_ASN        = apex_application.g_f11(1),',
'           ASN_EMAIL          = apex_application.g_f12(1),',
'           DELIVER_INVOICE    = apex_application.g_f14(1),',
'           START_DATE_ACTIVE  = apex_application.g_f21(1),',
'           END_DATE_ACTIVE    = apex_application.g_f23(1),',
'           TP_NAME            = apex_application.g_f19(1),',
'           NOTIFY_ACCOUNT_MGR = apex_application.g_f18(1),',
'           NOTIFICATION_EMAIL = apex_application.g_f20(1),',
'           STATUS             = ''APPROVED''',
'     WHERE party_id = :P6_PARTY_ID;',
'  END IF; -- :P6_USER_ROLE = ''3''',
'  IF :P6_USER_ROLE = ''2'' THEN',
'    -- SALES PERSON',
'    l_end_date_active := apex_application.g_f23(1);',
'    ------------------------------------------',
'    -- Update Status to - PENDING_APPROVAL',
'    ------------------------------------------',
'    UPDATE XXWC.XXWC_B2B_CONFIG_TBL',
'       SET DELIVER_SOA        = apex_application.g_f07(1),',
'           SOA_EMAIL          = apex_application.g_f08(1),',
'           DELIVER_ASN        = apex_application.g_f11(1),',
'           ASN_EMAIL          = apex_application.g_f12(1),',
'           START_DATE_ACTIVE  = apex_application.g_f21(1),',
'           END_DATE_ACTIVE    = apex_application.g_f23(1),',
'           NOTIFY_ACCOUNT_MGR = apex_application.g_f18(1),',
'           NOTIFICATION_EMAIL = apex_application.g_f19(1),',
'           STATUS             = DECODE(status,',
'                                       ''APPROVED'',',
'                                       ''APPROVED'',',
'                                       l_sr_setup_status)',
'     WHERE party_id = :P6_PARTY_ID;',
'  END IF; -- :P6_USER_ROLE = ''2''',
'  --------------------------------------------------',
'  -- Send FYI. Email Notification - SalesRep',
'  --------------------------------------------------',
'  BEGIN',
'    SELECT apps.xxwc_b2b_so_ib_pkg.get_sales_rep_email(:P6_CUST_ACCOUNT_ID)',
'      INTO l_to_sr_email_id',
'      FROM DUAL;',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      l_to_sr_email_id := NULL;',
'  END;',
'  IF l_to_sr_email_id IS NOT NULL THEN',
'    --------------------------------------------------',
'    -- New Account',
'    --------------------------------------------------',
'    IF l_status = ''DRAFT'' THEN',
'      l_subject     := :P6_PARTY_NAME ||',
'                       '' is Submitted for Order Status Update.'';',
'      l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'      l_body_detail := ''<BR> <B> '' || l_instance || ''</B> '' ||',
'                       ''<BR> </BR> <BR> A request to set-up <B>'' ||',
'                       :P6_PARTY_NAME ||',
'                       ''</B> for the "Order Status Update" service has been completed. ''',
'                      -- ''<BR> </BR> You will be notified once the request has been approved or rejected by your Manager.''',
'                       || ''<BR> </BR> Submitted by : '' || l_user_name;',
'    END IF;',
'    --------------------------------------------------',
'    -- Modified Account',
'    --------------------------------------------------',
'    IF l_status != ''DRAFT'' AND TO_DATE(l_end_date_active, ''DD-MON-YYYY'') =',
'       TO_DATE(:P6_END_DATE_ACTIVE, ''DD-MON-YYYY'') THEN',
'      l_subject                                := :P6_PARTY_NAME ||',
'                                                  '' is Modified for Order Status Update.'';',
'      l_body_header                            := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'      l_body_detail                            := ''<BR> <B> '' || l_instance ||',
'                                                  ''</B> '' ||',
'                                                  ''<BR> </BR> <BR> A request to modify <B>'' ||',
'                                                  :P6_PARTY_NAME ||',
'                                                  ''</B> for the "Order Status Update" service has been completed.'' ||',
'                                                  ''<BR> </BR> Modified by : '' ||',
'                                                  l_user_name;',
'      apex_application.g_print_success_message := ''Trading Partner B2B setup is modified.'';',
'    END IF;',
'    --------------------------------------------------',
'    -- Deactivated Account',
'    --------------------------------------------------',
'    IF l_status != ''DRAFT'' AND TO_DATE(l_end_date_active, ''DD-MON-YYYY'') !=',
'       TO_DATE(:P6_END_DATE_ACTIVE, ''DD-MON-YYYY'') THEN',
'      l_subject                                := :P6_PARTY_NAME ||',
'                                                  '' is Modified for Order Status Update.'';',
'      l_body_header                            := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif; color: navy;}</style><p class="style1">'';',
'      l_body_detail                            := ''<BR> <B> '' || l_instance ||',
'                                                  ''</B> '' ||',
'                                                  ''<BR> </BR> <BR> A request to deactivate <B>'' ||',
'                                                  :P6_PARTY_NAME ||',
'                                                  ''</B> for the "Order Status Update" service has been completed.'' ||',
'                                                  ''<BR> </BR> Deactivated by : '' ||',
'                                                  l_user_name;',
'      apex_application.g_print_success_message := ''Trading Partner B2B setup is deactivated.'';',
'    END IF;',
'    IF l_instance = ''EBIZPRD'' THEN',
'      l_to_email    := l_to_sr_email_id;',
'      l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>'';',
'    ELSE',
'      l_to_email    := l_dflt_email;',
'      l_body_footer := ''<BR></BR> <BR></BR> <BR> Please contact WC2020SalesSupport with any questions. </BR>'' ||',
'                       ''<BR> Email Ids - '' || l_to_sr_email_id || ''</BR> '';',
'    END IF;',
'    l_body := l_body_header || l_body_detail || chr(10) || l_body_footer;',
'    APEX_MAIL.SEND(p_to        => l_to_email,',
'                   p_from      => ''no-reply@whitecap.net'',',
'                   p_body      => l_body,',
'                   p_body_html => l_body,',
'                   p_subj      => l_subject,',
'                   p_replyto   => NULL);',
'  END IF;',
'  --------------------------------------------------',
'  -- End Salesrep Email',
'  --------------------------------------------------',
'  COMMIT;',
'EXCEPTION',
'  WHEN OTHERS THEN',
'    NULL;',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(4188830384137478)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(4597513092088179)
,p_process_sequence=>50
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Find User Role'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  l_salesrep_flag   VARCHAR2(1);',
'  l_user_name       VARCHAR2(30);',
'  l_instance        VARCHAR2(50);',
'  l_end_date_active DATE;',
'  l_salesrep_name   VARCHAR2(200);',
'BEGIN',
' IF :P6_INSTANCE IS NULL THEN',
'    BEGIN',
'      SELECT SUBSTR(UPPER(global_name), 1, 7)',
'        INTO l_instance',
'        FROM global_name;',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        l_instance := NULL;',
'    END;',
'  ',
'    :P6_INSTANCE := l_instance;',
'  END IF;',
'  ',
'  IF :P6_USER_ROLE IN (''1'', ''2'') THEN',
'    l_user_name := nvl(v(''APP_USER''), USER);',
'    SELECT apps.xxwc_b2b_so_ib_pkg.is_sales_rep(:P6_CUST_ACCOUNT_ID, l_user_name)',
'      INTO l_salesrep_flag',
'      FROM dual;',
'  ',
'    IF l_salesrep_flag IN (''Y'', ''0'') THEN',
'      :P6_USER_ROLE := ''2'';',
'      IF l_salesrep_flag = ''Y'' THEN',
'        :P6_SALESREP := l_user_name;',
'      END IF;',
'    ',
'    ELSE',
'      :P6_USER_ROLE := ''1'';',
'    END IF;',
'  ',
'  END IF;',
'   ',
'  l_user_name := nvl(v(''APP_USER''), USER);',
'',
'  IF :P6_NTID IS NULL THEN',
'    :P6_NTID := l_user_name;',
'  END IF;',
'',
'  IF :P6_USER_NAME IS NULL THEN',
'    BEGIN',
'      SELECT REPLACE(DESCRIPTION, '','', '''')',
'        INTO :P6_USER_NAME',
'        FROM APPS.FND_USER',
'       WHERE USER_NAME = l_user_name',
'         AND ROWNUM = 1;',
'    EXCEPTION',
'      WHEN OTHERS THEN',
'        NULL;',
'    END;',
'  END IF;',
'',
' ',
' EXCEPTION',
'  WHEN OTHERS THEN',
'    dbms_output.put_line(''Error - '' || SQLERRM);',
'END;'))
);
end;
/
prompt --application/pages/page_00007
begin
wwv_flow_api.create_page(
 p_id=>7
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_tab_set=>'TS1'
,p_name=>'Customer Report'
,p_page_mode=>'NORMAL'
,p_step_title=>'Customer Report'
,p_step_sub_title=>'Customer Report'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20170802122228'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3367943231639452)
,p_plug_name=>'Customer Report'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select PARTY_ID,',
'       PARTY_NAME,',
'       PARTY_NUMBER,',
'       CUSTOMER_ID,',
'       DELIVER_SOA,',
'       DELIVER_ASN,',
'       DELIVER_INVOICE,',
'       START_DATE_ACTIVE,',
'       END_DATE_ACTIVE,',
'       CREATION_DATE,',
'       CREATED_BY,',
'       LAST_UPDATE_DATE,',
'       LAST_UPDATED_BY,',
'       TP_NAME,',
'       DELIVER_POA,',
'       DEFAULT_EMAIL,',
'       STATUS,',
'       NOTIFICATION_EMAIL,',
'       NOTIFY_ACCOUNT_MGR,',
'       SOA_EMAIL,',
'       ASN_EMAIL,',
'       COMMENTS,',
'       APPROVED_DATE,',
'       DELIVER_POD,',
'       POD_EMAIL,',
'       POD_FREQUENCY,',
'       ACCOUNT_NUMBER,',
'       ACCOUNT_NAME,',
'       CUST_ACCOUNT_ID,',
'       POD_LAST_SENT_DATE,',
'       POD_NEXT_SEND_DATE,',
'       ID,',
'       LOCATION,',
'       SITE_USE_ID,',
'       SOA_PRINT_PRICE,',
'       POD_PRINT_PRICE',
'  from XXWC.XXWC_B2B_CONFIG_TBL'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(3368055648639452)
,p_name=>'Customer Report'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'GP050872'
,p_internal_uid=>3368055648639452
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3368462943639456)
,p_db_column_name=>'PARTY_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Party Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3368845622639457)
,p_db_column_name=>'PARTY_NAME'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'Party Name'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3369223156639457)
,p_db_column_name=>'PARTY_NUMBER'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Party Number'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3369638073639457)
,p_db_column_name=>'CUSTOMER_ID'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Customer Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3370016713639457)
,p_db_column_name=>'DELIVER_SOA'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Deliver SOA'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3370492640639457)
,p_db_column_name=>'DELIVER_ASN'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Deliver ASN'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3370866605639458)
,p_db_column_name=>'DELIVER_INVOICE'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'Deliver Invoice'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3371274094639458)
,p_db_column_name=>'START_DATE_ACTIVE'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'Start Date Active'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3371671185639458)
,p_db_column_name=>'END_DATE_ACTIVE'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'End Date Active'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3372065234639458)
,p_db_column_name=>'CREATION_DATE'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Creation Date'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3372415864639458)
,p_db_column_name=>'CREATED_BY'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Created By'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3372805352639459)
,p_db_column_name=>'LAST_UPDATE_DATE'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Last Update Date'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3373291561639459)
,p_db_column_name=>'LAST_UPDATED_BY'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Last Updated By'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3373606171639459)
,p_db_column_name=>'TP_NAME'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'TP Name'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3374034021639459)
,p_db_column_name=>'DELIVER_POA'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Deliver POA'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3374442631639459)
,p_db_column_name=>'DEFAULT_EMAIL'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'Default Email'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3374866019639459)
,p_db_column_name=>'STATUS'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'Status'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3375281550639460)
,p_db_column_name=>'NOTIFICATION_EMAIL'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Notification Email'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3375666379639460)
,p_db_column_name=>'NOTIFY_ACCOUNT_MGR'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Notify Account Mgr'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3376037889639460)
,p_db_column_name=>'SOA_EMAIL'
,p_display_order=>20
,p_column_identifier=>'T'
,p_column_label=>'SOA Email'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3376479693639460)
,p_db_column_name=>'ASN_EMAIL'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'ASN Email'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3376814585639460)
,p_db_column_name=>'COMMENTS'
,p_display_order=>22
,p_column_identifier=>'V'
,p_column_label=>'Comments'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3377299535639461)
,p_db_column_name=>'APPROVED_DATE'
,p_display_order=>23
,p_column_identifier=>'W'
,p_column_label=>'Approved Date'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3377614422639461)
,p_db_column_name=>'DELIVER_POD'
,p_display_order=>24
,p_column_identifier=>'X'
,p_column_label=>'Deliver POD'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3378044337639461)
,p_db_column_name=>'POD_EMAIL'
,p_display_order=>25
,p_column_identifier=>'Y'
,p_column_label=>'POD Email'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3378404357639462)
,p_db_column_name=>'POD_FREQUENCY'
,p_display_order=>26
,p_column_identifier=>'Z'
,p_column_label=>'POD Frequency'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3378857338639462)
,p_db_column_name=>'ACCOUNT_NUMBER'
,p_display_order=>27
,p_column_identifier=>'AA'
,p_column_label=>'Account Number'
,p_column_link=>'f?p=&APP_ID.:6:&SESSION.::&DEBUG.:RP:P6_ACCOUNT_NUMBER,P6_USER_ROLE:#ACCOUNT_NUMBER#,&P7_USER_ROLE.'
,p_column_linktext=>'#ACCOUNT_NUMBER#'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3379252718639462)
,p_db_column_name=>'ACCOUNT_NAME'
,p_display_order=>28
,p_column_identifier=>'AB'
,p_column_label=>'Account Name'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3379665104639463)
,p_db_column_name=>'CUST_ACCOUNT_ID'
,p_display_order=>29
,p_column_identifier=>'AC'
,p_column_label=>'Cust Account Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3380090900639463)
,p_db_column_name=>'POD_LAST_SENT_DATE'
,p_display_order=>30
,p_column_identifier=>'AD'
,p_column_label=>'POD Last Sent Date'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3380449141639463)
,p_db_column_name=>'POD_NEXT_SEND_DATE'
,p_display_order=>31
,p_column_identifier=>'AE'
,p_column_label=>'POD Next Send Date'
,p_column_type=>'DATE'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3380886227639463)
,p_db_column_name=>'ID'
,p_display_order=>32
,p_column_identifier=>'AF'
,p_column_label=>'Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3381220421639463)
,p_db_column_name=>'LOCATION'
,p_display_order=>33
,p_column_identifier=>'AG'
,p_column_label=>'Location'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3381652122639464)
,p_db_column_name=>'SITE_USE_ID'
,p_display_order=>34
,p_column_identifier=>'AH'
,p_column_label=>'Site Use Id'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'RIGHT'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3382032697639464)
,p_db_column_name=>'SOA_PRINT_PRICE'
,p_display_order=>35
,p_column_identifier=>'AI'
,p_column_label=>'SOA Print Price'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3382410959639464)
,p_db_column_name=>'POD_PRINT_PRICE'
,p_display_order=>36
,p_column_identifier=>'AJ'
,p_column_label=>'POD Print Price'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(3382809977640245)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'33829'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>50
,p_report_columns=>'ACCOUNT_NUMBER:SOA_EMAIL:ASN_EMAIL:COMMENTS:POD_EMAIL:POD_FREQUENCY:ACCOUNT_NAME:LOCATION:DEFAULT_EMAIL:NOTIFICATION_EMAIL:NOTIFY_ACCOUNT_MGR:STATUS:TP_NAME:'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3349761784973103)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(3367943231639452)
,p_button_name=>'Cancel'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Cancel'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:RP::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3428529260896801)
,p_name=>'P7_USER_ROLE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(3367943231639452)
,p_item_default=>'1'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3428626583896802)
,p_name=>'P7_CUST_ACCOUNT_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(3367943231639452)
,p_display_as=>'NATIVE_HIDDEN'
,p_display_when_type=>'NEVER'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3428714584896803)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Userrole'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'l_salesrep_flag VARCHAR2(1);',
'BEGIN',
'',
'SELECT apps.xxwc_b2b_so_ib_pkg.is_sales_rep(:P7_CUST_ACCOUNT_ID, nvl(v(''APP_USER''),USER))',
'  INTO l_salesrep_flag',
'  FROM dual;',
'',
'IF l_salesrep_flag IN (''Y'', ''0'') THEN ',
'   :P7_USER_ROLE := ''2'';',
'ELSE',
'   :P7_USER_ROLE := ''1'';',
'END IF;',
'',
'dbms_output.put_line(''l_salesrep_flag - ''||l_salesrep_flag);',
'',
'EXCEPTION',
'WHEN OTHERS THEN',
'dbms_output.put_line(''Error - ''||SQLERRM);',
'END;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/pages/page_00008
begin
wwv_flow_api.create_page(
 p_id=>8
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_tab_set=>'TS1'
,p_name=>'File Load with Directory'
,p_page_mode=>'NORMAL'
,p_step_title=>'File Load with Directory'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20170804233640'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1882640299449201)
,p_plug_name=>'Item Load'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>'<strong>Choose the file to be uploaded and Press the ''CSV File Load'' button to load a .csv file for bulk item load.  The file must be formatted correctly and have the correct file name and extension as follows:  "XXWC_POD_Load.csv"</strong>'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(1898639505327525)
,p_name=>'Table'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY'
,p_source=>'Select ID,Name,Filename,created_by,title,created_on  from wwv_flow_files'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_display_condition_type=>'NEVER'
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(1909771519451710)
,p_query_column_id=>1
,p_column_alias=>'ID'
,p_column_display_sequence=>1
,p_column_heading=>'Id'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(1911720672451730)
,p_query_column_id=>2
,p_column_alias=>'NAME'
,p_column_display_sequence=>2
,p_column_heading=>'Name'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(1911890561451731)
,p_query_column_id=>3
,p_column_alias=>'FILENAME'
,p_column_display_sequence=>3
,p_column_heading=>'Filename'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(1914841010045001)
,p_query_column_id=>4
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>5
,p_column_heading=>'Created by'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(1911967182451732)
,p_query_column_id=>5
,p_column_alias=>'TITLE'
,p_column_display_sequence=>4
,p_column_heading=>'Title'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(1914982706045002)
,p_query_column_id=>6
,p_column_alias=>'CREATED_ON'
,p_column_display_sequence=>6
,p_column_heading=>'Created on'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1955906394116703)
,p_plug_name=>'POD Data Load'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select account_number, ',
'pod_email, ',
'pod_frequency, ',
'deliver_pod, ',
'site_use_id',
' from  XXWC.XXWC_B2B_POD_STAGE_TBL'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width=>8.5
,p_prn_height=>11
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#000000'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(1956011567116704)
,p_max_row_count=>'1000000'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_download_formats=>'CSV:HTML:EMAIL:XLS:PDF:RTF'
,p_owner=>'GP050872'
,p_internal_uid=>1956011567116704
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(1956128711116705)
,p_db_column_name=>'ACCOUNT_NUMBER'
,p_display_order=>10
,p_column_identifier=>'A'
,p_column_label=>'Account number'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(1956275594116706)
,p_db_column_name=>'POD_EMAIL'
,p_display_order=>20
,p_column_identifier=>'B'
,p_column_label=>'Pod email'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(1956314391116707)
,p_db_column_name=>'POD_FREQUENCY'
,p_display_order=>30
,p_column_identifier=>'C'
,p_column_label=>'Pod frequency'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(1956418758116708)
,p_db_column_name=>'DELIVER_POD'
,p_display_order=>40
,p_column_identifier=>'D'
,p_column_label=>'Deliver pod'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(1956571657116709)
,p_db_column_name=>'SITE_USE_ID'
,p_display_order=>50
,p_column_identifier=>'E'
,p_column_label=>'Site use id'
,p_column_type=>'STRING'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(1968066792922386)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'19681'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>50
,p_report_columns=>'ACCOUNT_NUMBER:POD_EMAIL:POD_FREQUENCY:DELIVER_POD:SITE_USE_ID'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(1882841347449203)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(1882640299449201)
,p_button_name=>'CSV_File_Load'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Csv file load'
,p_button_position=>'ABOVE_BOX'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2814916078577201)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(1955906394116703)
,p_button_name=>'Send_Customer_Email'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Send customer email'
,p_button_position=>'ABOVE_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:28:&SESSION.::&DEBUG.:RP::'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(1880531973616201)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(1882640299449201)
,p_button_name=>'Cancel'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Cancel'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:5:&SESSION.::&DEBUG.:RP::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(1955770659116701)
,p_branch_name=>'POD upload'
,p_branch_action=>'f?p=&APP_ID.:8:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(1882841347449203)
,p_branch_sequence=>10
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(1882719774449202)
,p_name=>'P8_FILE_BROWSE_UPLOAD'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1882640299449201)
,p_prompt=>'FILE_BROWSE_UPLOAD'
,p_display_as=>'NATIVE_FILE'
,p_cSize=>30
,p_label_alignment=>'RIGHT-CENTER'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'APEX_APPLICATION_TEMP_FILES'
,p_attribute_09=>'SESSION'
,p_attribute_10=>'N'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(1882903013449204)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'FILE_INSERT'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  p_ITEM_LOAD   VARCHAR2(100);',
'  lvc_batch_num NUMBER := TO_CHAR(SYSDATE, ''MMDDYYYYHHMISS'');',
'  l_count       number;',
'  l_statement   varchar2(255);',
'BEGIN',
'  delete from XXWC.XXWC_B2B_POD_STAGE_TBL;',
'  APPS.XXWC_B2B_CAT_CUST_UPLOAD_PKG.XXWC_B2B_POD_FILE_UPLOAD_PRC(:P8_FILE_BROWSE_UPLOAD,',
'                                                                 ''XXWC_PDH_ITEM_LOAD_DIR'',',
'                                                                 NULL,',
'                                                                 p_ITEM_LOAD);',
'  INSERT INTO XXWC.XXWC_B2B_POD_STAGE_TBL',
'    (ACCOUNT_NUMBER, POD_EMAIL, POD_FREQUENCY, DELIVER_POD, site_use_id)',
'    (SELECT ACCOUNT_NUMBER,',
'            POD_EMAIL,',
'            POD_FREQUENCY,',
'            DELIVER_POD,',
'            replace(job_site, chr(13), '''')',
'       FROM XXWC.XXWC_B2B_POD_LOAD_EXT_TBL);',
'  commit;',
'  for i in (select ACCOUNT_NUMBER,',
'                   POD_EMAIL,',
'                   POD_FREQUENCY,',
'                   DELIVER_POD,',
'                   SITE_USE_ID',
'              from XXWC.XXWC_B2B_POD_STAGE_TBL) loop',
'    begin',
'      select count(1)',
'        into l_count',
'        from XXWC.XXWC_B2B_CONFIG_TBL a',
'       where (site_use_id =',
'             (SELECT DISTINCT hcsu.site_use_id',
'                 FROM apps.hz_cust_site_uses_all  hcsu,',
'                      apps.hz_cust_acct_sites_all hcas,',
'                      apps.hz_party_sites         hps',
'                WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'                  AND hps.party_site_id = hcas.party_site_id',
'                  AND hcas.STATUS = ''A''',
'                  AND hcsu.site_use_code = ''SHIP_TO''',
'                  AND ROWNUM < 100',
'                  and cust_account_id =',
'                      (select hc.cust_account_id',
'                         from apps.hz_Cust_accounts_all hc,',
'                              apps.hz_parties           hp',
'                        where hc.account_number = i.account_number',
'                          and hc.party_id = hp.party_id)',
'                  AND hps.party_id =',
'                      (select hc.party_id',
'                         from apps.hz_Cust_accounts_all hc,',
'                              apps.hz_parties           hp',
'                        where hc.account_number = i.account_number',
'                          and hc.party_id = hp.party_id)',
'                  AND hcsu.location = i.site_use_id) or',
'             trim(site_use_id) is null)',
'         and a.account_number = i.account_number;',
'      l_statement := ''Count match - '' || l_count;',
'    end;',
'    if l_count > 0 then',
'      update XXWC.XXWC_B2B_CONFIG_TBL',
'         set pod_email       = i.pod_email,',
'             last_updated_by = 15986,',
'             soa_print_price = ''N'',',
'             pod_frequency   = i.pod_frequency,',
'             DELIVER_POD     = i.deliver_pod',
'       where account_number = i.account_number',
'         and (site_use_id =',
'             (SELECT DISTINCT hcsu.site_use_id',
'                 FROM apps.hz_cust_site_uses_all  hcsu,',
'                      apps.hz_cust_acct_sites_all hcas,',
'                      apps.hz_party_sites         hps',
'                WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'                  AND hps.party_site_id = hcas.party_site_id',
'                  AND hcas.STATUS = ''A''',
'                  AND hcsu.site_use_code = ''SHIP_TO''',
'                  AND ROWNUM < 100',
'                  and cust_account_id =',
'                      (select hc.cust_account_id',
'                         from apps.hz_Cust_accounts_all hc,',
'                              apps.hz_parties           hp',
'                        where hc.account_number = i.account_number',
'                          and hc.party_id = hp.party_id)',
'                  AND hps.party_id =',
'                      (select hc.party_id',
'                         from apps.hz_Cust_accounts_all hc,',
'                              apps.hz_parties           hp',
'                        where hc.account_number = i.account_number',
'                          and hc.party_id = hp.party_id)',
'                  AND hcsu.location = i.site_use_id) or',
'             trim(site_use_id) is null)',
'         and DELIVER_POD = ''N'';',
'      update XXWC.XXWC_B2B_CONFIG_TBL',
'         set pod_email = pod_email || '','' || i.pod_email',
'       where account_number = i.account_number',
'         and (site_use_id =',
'             (SELECT DISTINCT hcsu.site_use_id',
'                 FROM apps.hz_cust_site_uses_all  hcsu,',
'                      apps.hz_cust_acct_sites_all hcas,',
'                      apps.hz_party_sites         hps',
'                WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'                  AND hps.party_site_id = hcas.party_site_id',
'                  AND hcas.STATUS = ''A''',
'                  AND hcsu.site_use_code = ''SHIP_TO''',
'                  AND ROWNUM < 100',
'                  and cust_account_id =',
'                      (select hc.cust_account_id',
'                         from apps.hz_Cust_accounts_all hc,',
'                              apps.hz_parties           hp',
'                        where hc.account_number = i.account_number',
'                          and hc.party_id = hp.party_id)',
'                  AND hps.party_id =',
'                      (select hc.party_id',
'                         from apps.hz_Cust_accounts_all hc,',
'                              apps.hz_parties           hp',
'                        where hc.account_number = i.account_number',
'                          and hc.party_id = hp.party_id)',
'                  AND hcsu.location = i.site_use_id) or',
'             trim(site_use_id) is null)',
'         and DELIVER_POD = ''Y''',
'         and pod_email != i.pod_email;',
'      commit;',
'    end if;',
'    if l_count = 0 then',
'      INSERT into XXWC.XXWC_B2B_CONFIG_TBL',
'        (ACCOUNT_NUMBER,',
'         POD_EMAIL,',
'         POD_FREQUENCY,',
'         DELIVER_POD,',
'         site_use_id,',
'         cust_account_id,',
'         party_id,',
'         party_number,',
'         party_name,',
'         created_by,',
'         soa_print_price)',
'      VALUES',
'        (i.ACCOUNT_NUMBER,',
'         i.POD_EMAIL,',
'         i.POD_FREQUENCY,',
'         i.DELIVER_POD,',
'         (SELECT DISTINCT hcsu.site_use_id',
'            FROM apps.hz_cust_site_uses_all  hcsu,',
'                 apps.hz_cust_acct_sites_all hcas,',
'                 apps.hz_party_sites         hps',
'           WHERE hcas.cust_acct_site_id = hcsu.cust_acct_site_id',
'             AND hps.party_site_id = hcas.party_site_id',
'             AND hcas.STATUS = ''A''',
'             AND hcsu.site_use_code = ''SHIP_TO''',
'             AND ROWNUM < 100',
'             and cust_account_id =',
'                 (select hc.cust_account_id',
'                    from apps.hz_Cust_accounts_all hc, apps.hz_parties hp',
'                   where hc.account_number = i.account_number',
'                     and hc.party_id = hp.party_id)',
'             AND hps.party_id =',
'                 (select hc.party_id',
'                    from apps.hz_Cust_accounts_all hc, apps.hz_parties hp',
'                   where hc.account_number = i.account_number',
'                     and hc.party_id = hp.party_id)',
'             AND hcsu.location = i.site_use_id),',
'         (select hc.cust_account_id',
'            from apps.hz_Cust_accounts_all hc, apps.hz_parties hp',
'           where hc.account_number = i.account_number',
'             and hc.party_id = hp.party_id),',
'         (select hc.party_id',
'            from apps.hz_Cust_accounts_all hc, apps.hz_parties hp',
'           where hc.account_number = i.account_number',
'             and hc.party_id = hp.party_id),',
'         (select hp.party_number',
'            from apps.hz_Cust_accounts_all hc, apps.hz_parties hp',
'           where hc.account_number = i.account_number',
'             and hc.party_id = hp.party_id),',
'         (select hp.party_name',
'            from apps.hz_Cust_accounts_all hc, apps.hz_parties hp',
'           where hc.account_number = i.account_number',
'             and hc.party_id = hp.party_id),',
'         15986,',
'         ''N'');',
'      commit;',
'    end if;',
'  end loop;',
'END;',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(1882841347449203)
);
end;
/
prompt --application/pages/page_00014
begin
wwv_flow_api.create_page(
 p_id=>14
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_tab_set=>'TS1'
,p_name=>'WC B2B Trading Partner Maintenance Form'
,p_page_mode=>'NORMAL'
,p_step_title=>'WC B2B Trading Partner Maintenance Form'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20150803212810'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(5787311366283360)
,p_name=>'WC B2B Trading Partner Maintenance Form'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"PARTY_ID",',
'"PARTY_ID" PARTY_ID_DISPLAY,',
'"PARTY_NAME",',
'"PARTY_NUMBER",',
'"TP_NAME",',
'"DELIVER_SOA",',
'"DELIVER_ASN",',
'"DELIVER_INVOICE",',
'"DELIVER_POA",',
'"START_DATE_ACTIVE",',
'"END_DATE_ACTIVE",',
'"CREATION_DATE",',
'"CREATED_BY",',
'"LAST_UPDATE_DATE",',
'"LAST_UPDATED_BY"',
'from XXWC.XXWC_B2B_CUST_INFO_TBL',
''))
,p_source_type=>'NATIVE_TABFORM'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_prn_format=>'PDF'
,p_prn_output_show_link=>'Y'
,p_prn_output_link_text=>'Print'
,p_prn_content_disposition=>'ATTACHMENT'
,p_prn_document_header=>'APEX'
,p_prn_units=>'INCHES'
,p_prn_paper_size=>'LETTER'
,p_prn_width_units=>'PERCENTAGE'
,p_prn_width=>11
,p_prn_height=>8.5
,p_prn_orientation=>'HORIZONTAL'
,p_prn_page_header_font_color=>'#000000'
,p_prn_page_header_font_family=>'Helvetica'
,p_prn_page_header_font_weight=>'normal'
,p_prn_page_header_font_size=>'12'
,p_prn_page_footer_font_color=>'#000000'
,p_prn_page_footer_font_family=>'Helvetica'
,p_prn_page_footer_font_weight=>'normal'
,p_prn_page_footer_font_size=>'12'
,p_prn_header_bg_color=>'#9bafde'
,p_prn_header_font_color=>'#ffffff'
,p_prn_header_font_family=>'Helvetica'
,p_prn_header_font_weight=>'normal'
,p_prn_header_font_size=>'10'
,p_prn_body_bg_color=>'#efefef'
,p_prn_body_font_color=>'#000000'
,p_prn_body_font_family=>'Helvetica'
,p_prn_body_font_weight=>'normal'
,p_prn_body_font_size=>'10'
,p_prn_border_width=>.5
,p_prn_page_header_alignment=>'CENTER'
,p_prn_page_footer_alignment=>'CENTER'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5790722412283366)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'Check$01'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5787625015283361)
,p_query_column_id=>2
,p_column_alias=>'PARTY_ID'
,p_column_display_sequence=>2
,p_column_heading=>'Party Id'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5787728753283362)
,p_query_column_id=>3
,p_column_alias=>'PARTY_ID_DISPLAY'
,p_column_display_sequence=>3
,p_column_heading=>'Party Id Display'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5787823068283362)
,p_query_column_id=>4
,p_column_alias=>'PARTY_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Party Name'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'POPUP'
,p_named_lov=>wwv_flow_api.id(4901524425545800)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_name||'' ''||party_number d, party_name r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_report_column_width=>360
,p_column_width=>60
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NAME'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5787912621283362)
,p_query_column_id=>5
,p_column_alias=>'PARTY_NUMBER'
,p_column_display_sequence=>5
,p_column_heading=>'Party Number'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'POPUP'
,p_named_lov=>wwv_flow_api.id(5796908191268319)
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select party_number d, party_number r',
'from   APPS.HZ_PARTIES',
'where party_type = ''ORGANIZATION'' AND status = ''A'''))
,p_lov_show_nulls=>'NO'
,p_report_column_width=>30
,p_column_width=>30
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'PARTY_NUMBER'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(6794431722579710)
,p_query_column_id=>6
,p_column_alias=>'TP_NAME'
,p_column_display_sequence=>16
,p_column_heading=>'Tp Name'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_report_column_width=>30
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5788035885283362)
,p_query_column_id=>7
,p_column_alias=>'DELIVER_SOA'
,p_column_display_sequence=>6
,p_column_heading=>'Deliver Soa'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(4897937324296850)
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'N'
,p_lov_null_value=>'N'
,p_column_width=>12
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_SOA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5788114393283362)
,p_query_column_id=>8
,p_column_alias=>'DELIVER_ASN'
,p_column_display_sequence=>7
,p_column_heading=>'Deliver Asn'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(4897937324296850)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_ASN'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5788229212283362)
,p_query_column_id=>9
,p_column_alias=>'DELIVER_INVOICE'
,p_column_display_sequence=>8
,p_column_heading=>'Deliver Invoice'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(4897937324296850)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_INVOICE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(6840308438092808)
,p_query_column_id=>10
,p_column_alias=>'DELIVER_POA'
,p_column_display_sequence=>9
,p_column_heading=>'Deliver Poa'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_LOV'
,p_named_lov=>wwv_flow_api.id(4897937324296850)
,p_lov_show_nulls=>'NO'
,p_column_width=>12
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'DELIVER_POA'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5788324030283362)
,p_query_column_id=>11
,p_column_alias=>'START_DATE_ACTIVE'
,p_column_display_sequence=>10
,p_column_heading=>'Start Date Active'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'START_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5788406133283362)
,p_query_column_id=>12
,p_column_alias=>'END_DATE_ACTIVE'
,p_column_display_sequence=>11
,p_column_heading=>'End Date Active'
,p_disable_sort_column=>'N'
,p_display_as=>'DATE_PICKER'
,p_column_width=>12
,p_include_in_export=>'Y'
,p_print_col_width=>'9'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'END_DATE_ACTIVE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5788530173283362)
,p_query_column_id=>13
,p_column_alias=>'CREATION_DATE'
,p_column_display_sequence=>12
,p_column_heading=>'Creation Date'
,p_hidden_column=>'Y'
,p_column_width=>7
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATION_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5788624909283362)
,p_query_column_id=>14
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>13
,p_column_heading=>'Created By'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'CREATED_BY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5788733861283362)
,p_query_column_id=>15
,p_column_alias=>'LAST_UPDATE_DATE'
,p_column_display_sequence=>14
,p_column_heading=>'Last Update Date'
,p_hidden_column=>'Y'
,p_column_width=>7
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATE_DATE'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(5788832977283362)
,p_query_column_id=>16
,p_column_alias=>'LAST_UPDATED_BY'
,p_column_display_sequence=>15
,p_column_heading=>'Last Updated By'
,p_hidden_column=>'Y'
,p_column_width=>16
,p_include_in_export=>'N'
,p_print_col_width=>'0'
,p_ref_schema=>'WC_APPS'
,p_ref_table_name=>'XXWC_B2B_CUST_INFO_TBL'
,p_ref_column_name=>'LAST_UPDATED_BY'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(5789235781283364)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(5787311366283360)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:apex.widget.tabular.addRow();'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(5789031053283364)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(5787311366283360)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Submit'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(5788914350283364)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(5787311366283360)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:14:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(5789122100283364)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(5787311366283360)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(5790806700283366)
,p_branch_action=>'f?p=&APP_ID.:14:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(2510707760173705)
,p_tabular_form_region_id=>wwv_flow_api.id(5787311366283360)
,p_validation_name=>'PARTY_NAME is not NULL'
,p_validation_sequence=>30
,p_validation=>'PARTY_NAME'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'PARTY NAME must have a valid value.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(5789031053283364)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'PARTY_NAME'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(2511105591189916)
,p_tabular_form_region_id=>wwv_flow_api.id(5787311366283360)
,p_validation_name=>'PARTY_NUMBER is not null'
,p_validation_sequence=>40
,p_validation=>'PARTY_NUMBER'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'PARTY NUMBER must have a valid value.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(5789031053283364)
,p_exec_cond_for_each_row=>'Y'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'PARTY_NUMBER'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(5790025034283365)
,p_tabular_form_region_id=>wwv_flow_api.id(5787311366283360)
,p_validation_name=>'START_DATE_ACTIVE must be a valid date'
,p_validation_sequence=>80
,p_validation=>'START_DATE_ACTIVE'
,p_validation_type=>'ITEM_IS_DATE'
,p_error_message=>'#COLUMN_HEADER# must be a valid date.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(5789031053283364)
,p_exec_cond_for_each_row=>'N'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'START_DATE_ACTIVE'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(5790213435283366)
,p_tabular_form_region_id=>wwv_flow_api.id(5787311366283360)
,p_validation_name=>'END_DATE_ACTIVE must be a valid date'
,p_validation_sequence=>90
,p_validation=>'END_DATE_ACTIVE'
,p_validation_type=>'ITEM_IS_DATE'
,p_error_message=>'#COLUMN_HEADER# must be a valid date.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(5789031053283364)
,p_exec_cond_for_each_row=>'N'
,p_only_for_changed_rows=>'Y'
,p_associated_column=>'END_DATE_ACTIVE'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(5790325779283366)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(5787311366283360)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_CUST_INFO_TBL'
,p_attribute_03=>'PARTY_ID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(5789031053283364)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(5790505128283366)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(5787311366283360)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_CUST_INFO_TBL'
,p_attribute_03=>'PARTY_ID'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
end;
/
prompt --application/pages/page_00028
begin
wwv_flow_api.create_page(
 p_id=>28
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Customer Emails'
,p_page_mode=>'NORMAL'
,p_step_title=>'Customer Emails'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_page_is_public_y_n=>'N'
,p_protection_level=>'C'
,p_cache_mode=>'NOCACHE'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20170726114854'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2740684949831360)
,p_plug_name=>'Form on XXWC_B2B_POD_CUST_EMAILS_TBL'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617090878658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(5501942193167791)
,p_name=>'Report 1'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select "ROWID", ',
'"EMAIL"',
'from "XXWC"."XXWC_B2B_POD_CUST_EMAILS_TBL" ',
'  ',
''))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'N'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2755223093336425)
,p_query_column_id=>1
,p_column_alias=>'ROWID'
,p_column_display_sequence=>1
,p_column_link=>'f?p=#APP_ID#:28:#APP_SESSION#::::P28_ROWID:#ROWID#'
,p_column_linktext=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(2755674020336425)
,p_query_column_id=>2
,p_column_alias=>'EMAIL'
,p_column_display_sequence=>2
,p_column_heading=>'Email'
,p_heading_alignment=>'LEFT'
,p_ref_schema=>'XXWC'
,p_ref_table_name=>'XXWC_B2B_POD_CUST_EMAILS_TBL'
,p_ref_column_name=>'EMAIL'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2741189354831360)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(2740684949831360)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Apply Changes'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P28_ROWID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2741381478831360)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(2740684949831360)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:8:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2756426144336426)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(5501942193167791)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_redirect_url=>'f?p=&APP_ID.:28:&SESSION.::&DEBUG.:28'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2741083855831360)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(2740684949831360)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Load Emails'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''x''',
'from "XXWC"."XXWC_B2B_POD_CUST_EMAILS_TBL"'))
,p_button_condition_type=>'NOT_EXISTS'
,p_grid_new_grid=>false
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2741295942831360)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(2740684949831360)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'P28_ROWID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2760445661897896)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(5501942193167791)
,p_button_name=>'Email'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Email'
,p_button_position=>'RIGHT_OF_TITLE'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''x''',
'from "XXWC"."XXWC_B2B_POD_CUST_EMAILS_TBL"',
'where flag = ''N'''))
,p_button_condition_type=>'EXISTS'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(2742987635831361)
,p_branch_name=>'Go To Page 27'
,p_branch_action=>'f?p=&APP_ID.:28:&SESSION.::&DEBUG.:28::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2743337713831362)
,p_name=>'P28_ROWID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(2740684949831360)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Rowid'
,p_source=>'ROWID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_protection_level=>'S'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2743780967831362)
,p_name=>'P28_EMAIL'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(2740684949831360)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Email'
,p_source=>'EMAIL'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>60
,p_cMaxlength=>4000
,p_cHeight=>4
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(2508944546573816)
,p_validation_name=>'Email'
,p_validation_sequence=>10
,p_validation=>'P28_EMAIL'
,p_validation_type=>'ITEM_NOT_NULL'
,p_error_message=>'Email field cannot be blank'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(2756426144336426)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2745546854831363)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from XXWC_B2B_POD_CUST_EMAILS_TBL'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_POD_CUST_EMAILS_TBL'
,p_attribute_03=>'P28_ROWID'
,p_attribute_04=>'ROWID'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2745968275831363)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of XXWC_B2B_POD_CUST_EMAILS_TBL'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_B2B_POD_CUST_EMAILS_TBL'
,p_attribute_03=>'P28_ROWID'
,p_attribute_04=>'ROWID'
,p_attribute_11=>'I:U:D'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2746344820831363)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(2741295942831360)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2760054756894762)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Send Email'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'DECLARE',
'  l_id number;',
'  l_body varchar2(1000);',
'  l_from     varchar2(50);',
'  l_email_to varchar2(4000);',
'  l_subject varchar2(100);',
'  v_body_temp   varchar2(10000);',
'  l_body_header VARCHAR2(32767) DEFAULT NULL;',
'  l_body_detail VARCHAR2(32767) DEFAULT NULL;',
'  l_body_footer VARCHAR2(32767) DEFAULT NULL;',
'',
'BEGIN',
'  Begin',
'    select email into l_email_to from XXWC.XXWC_B2B_POD_CUST_EMAILS_TBL;',
'  exception',
'    when others then',
'      l_email_to := null;',
'  end;',
'  l_from := ''PODREQUESTS@WHITECAP.NET'';',
'  l_subject := ''Order Tracking with HD Supply just got easier!'';',
'  l_body        := ''Reports.'' || utl_tcp.crlf;',
'  l_body_header := ''<style type="text/css">.style1 {font-family: Arial, Helvetica, sans-serif;}</style><p class="style1">'';',
'  l_body_detail := ''<BR>'' ||',
'                   ''Proof of Delivery documents will now be emailed to you real-time as soon as your order invoices!'' ||',
'                   '' </BR><BR>To help reconcile bills, multiple order identifiers are provided in the email, including the Invoice Number.'' ||',
'                   '' </BR><BR>Depending on the shipment method, the document will also have the goods receipt signature, or third party tracking number.'' ||',
'                   '' </BR><BR>Below are examples of the email you will receive and the Proof of Delivery document. '' ||',
'                   '' </BR><BR>Visit our website today to learn more about additional Order Status Updates available: '' ||',
'                   '' </BR><BR> <a href="https://www.whitecap.com/shop/wc/order-status-updates">https://www.whitecap.com/shop/wc/order-status-updates</a>'' ||',
'                   '' </BR><BR> <img style="background-color: Transparent;" src="http://wcebsdev.hdsupply.net:7777/i51/pod_email1.gif">'' ||',
'                   '' </BR><BR> <img style="background-color: Transparent;" src="http://wcebsdev.hdsupply.net:7777/i51/pod_email2.gif">'' ||',
'                   '' </BR><BR>To ensure there is no delay in receiving your POD’s, be sure of the following: '' ||',
'                   '' </BR><BR><li>  Your company has WinZip </li>'' ||',
'                   '' </BR><BR><li>  Your system can allow emails from <a href= "mailto:abc@example.com"> no-reply@whitecap.net </a></li>'';',
'',
'  l_body_footer := ''<BR></BR> <BR></BR> <BR> Click <a href="https://www.whitecap.com/shop/wc/order-status-updates">here </a> to make changes to your subscription or to unsubscribe from the POD email service.  </BR>'';',
'  v_body_temp   := l_body_header || l_body_detail || CHR(10) ||',
'                   l_body_footer;',
'  apps.xxcus_misc_pkg.html_email(p_to            => l_email_to,',
'                                 p_from          => ''PODREQUESTS@WHITECAP.NET'',',
'                                 p_text          => l_body,',
'                                 p_subject       => l_subject,',
'                                 p_html          => v_body_temp,',
'                                 p_smtp_hostname => ''mailoutrelay.hdsupply.net'',',
'                                 p_smtp_portnum  => 25);',
'  /* APEX_MAIL.SEND(p_to        => l_email_to,',
'                   p_from      => ''PODREQUESTS@WHITECAP.NET'',',
'                   p_body      => l_body,',
'                   p_body_html => v_body_temp,',
'                   p_subj      => l_subject,',
'                   p_replyto   => NULL);*/',
'update XXWC.XXWC_B2B_POD_CUST_EMAILS_TBL set flag = ''Y''; ',
' commit;',
'',
'END;'))
,p_process_error_message=>'Failed'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(2760445661897896)
,p_process_success_message=>'Success'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2509036274573817)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Apply Changes Flag'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'Begin ',
'update XXWC.XXWC_B2B_POD_CUST_EMAILS_TBL set flag = ''N''; ',
' commit;',
'end;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(2741189354831360)
);
end;
/
prompt --application/pages/page_00040
begin
wwv_flow_api.create_page(
 p_id=>40
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Order Status Update - Metrics'
,p_page_mode=>'NORMAL'
,p_step_title=>'Order Status Update - Metrics'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'B2B Metrics'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'RP058050'
,p_last_upd_yyyymmddhh24miss=>'20161214103550'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2469614861557019)
,p_plug_name=>'Order Status Update - Metrics'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2469810098557020)
,p_plug_name=>'Find Region'
,p_parent_plug_id=>wwv_flow_api.id(2469614861557019)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2470426817557022)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(2469810098557020)
,p_button_name=>'P40_FIND'
,p_button_static_id=>'P40_FIND'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'FIND'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
,p_grid_new_row=>'Y'
,p_grid_new_column=>'Y'
,p_grid_column=>18
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2470609190557022)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(2469810098557020)
,p_button_name=>'P40_CLEAR'
,p_button_static_id=>'P40_CLEAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'CLEAR'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:40:P40_FROM_DATE,P40_TO_DATE:,'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>19
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2470024787557021)
,p_name=>'P40_FROM_DATE'
,p_is_required=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(2469810098557020)
,p_prompt=>'From Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2470205298557021)
,p_name=>'P40_TO_DATE'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(2469810098557020)
,p_prompt=>'To Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2470832721557022)
,p_name=>'P40_REPORT_NAME'
,p_item_sequence=>23
,p_item_plug_id=>wwv_flow_api.id(2469810098557020)
,p_prompt=>'Report Name'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'REPORT NAME'
,p_lov=>'.'||wwv_flow_api.id(2471412814557028)||'.'
,p_lov_display_null=>'YES'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(2471002172557023)
,p_name=>'Find Button'
,p_event_sequence=>10
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(2470426817557022)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(2471319841557027)
,p_event_id=>wwv_flow_api.id(2471002172557023)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if (document.getElementById(''P40_REPORT_NAME'').value == ''41'') {',
'apex.navigation.redirect("f?p=&APP_ID.:41:::NO::P41_FROM_DATE,P41_TO_DATE:"+document.getElementById(''P40_FROM_DATE'').value+","+document.getElementById(''P40_TO_DATE'').value);',
'}',
'',
'if (document.getElementById(''P40_REPORT_NAME'').value == ''42'') {',
'apex.navigation.redirect("f?p=&APP_ID.:42:::NO::P42_FROM_DATE,P42_TO_DATE:"+document.getElementById(''P40_FROM_DATE'').value+","+document.getElementById(''P40_TO_DATE'').value);',
'}',
'',
'if (document.getElementById(''P40_REPORT_NAME'').value == ''43'') {',
'apex.navigation.redirect("f?p=&APP_ID.:43:::NO::P43_FROM_DATE,P43_TO_DATE:"+document.getElementById(''P40_FROM_DATE'').value+","+document.getElementById(''P40_TO_DATE'').value);',
'}',
'',
'if (document.getElementById(''P40_REPORT_NAME'').value == ''44'') {',
'apex.navigation.redirect("f?p=&APP_ID.:44:::NO::P44_FROM_DATE,P44_TO_DATE:"+document.getElementById(''P40_FROM_DATE'').value+","+document.getElementById(''P40_TO_DATE'').value);',
'}',
'',
'if (document.getElementById(''P40_REPORT_NAME'').value == ''45'') {',
'apex.navigation.redirect("f?p=&APP_ID.:45:::NO::P45_FROM_DATE,P45_TO_DATE:"+document.getElementById(''P40_FROM_DATE'').value+","+document.getElementById(''P40_TO_DATE'').value);',
'}'))
,p_stop_execution_on_error=>'Y'
);
end;
/
prompt --application/pages/page_00041
begin
wwv_flow_api.create_page(
 p_id=>41
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Order Status Update - Metrics Summary'
,p_page_mode=>'NORMAL'
,p_step_title=>'Order Status Update - Metrics Summary'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'B2B Metrics Summary'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20160516160511'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2570311453920988)
,p_plug_name=>'Order Status Update - Metrics Summary'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2570513904920989)
,p_plug_name=>'Order Status Update - Metrics Summary'
,p_parent_plug_id=>wwv_flow_api.id(2570311453920988)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>11
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'   SELECT PARTY_NAME,',
'          PARTY_NUMBER,',
'          ACCOUNT_NAME,',
'          ACCOUNT_NUMBER,',
'          B2B_SO_CNT,',
'          B2B_SO_AMT,',
'          XML_PO_CNT,',
'          XML_PO_AMT,',
'          DECODE(deliver_invoice,''Y'',apps.xxwc_b2b_inv_intf_pkg.get_invoice_count(CUST_ACCOUNT_ID, :P41_FROM_DATE , :P41_TO_DATE),0) XML_INV_CNT,',
'          DECODE(deliver_invoice,''Y'',apps.xxwc_b2b_inv_intf_pkg.get_invoice_amt(CUST_ACCOUNT_ID, :P41_FROM_DATE , :P41_TO_DATE),0)   XML_INV_AMT',
'FROM (SELECT PARTY_NAME,',
'          PARTY_NUMBER,',
'          ACCOUNT_NAME,',
'          ACCOUNT_NUMBER,',
'          CUST_ACCOUNT_ID,',
'          deliver_invoice,',
'          COUNT(1) B2B_SO_CNT,',
'          SUM(SALE_AMOUNT) B2B_SO_AMT,',
'          SUM(DECODE(ORDER_SOURCE, 1021, 1,0)) XML_PO_CNT,',
'          SUM(DECODE(ORDER_SOURCE, 1021, SALE_AMOUNT,0)) XML_PO_AMT',
'     FROM APPS.XXWC_B2B_STATS_VW',
'    WHERE TRUNC(CREATION_DATE) BETWEEN :P41_FROM_DATE AND :P41_TO_DATE',
' GROUP BY PARTY_NUMBER,',
'          PARTY_NAME,',
'          ACCOUNT_NUMBER,',
'          ACCOUNT_NAME,',
'          CUST_ACCOUNT_ID,',
'          deliver_invoice',
'          ) ORDER BY PARTY_NAME'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P41_FROM_DATE IS NOT NULL AND :P41_TO_DATE IS NOT NULL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(2570730349920990)
,p_name=>'B2B Metrics Summary'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'TD002849'
,p_internal_uid=>2570730349920990
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2570825462920991)
,p_db_column_name=>'PARTY_NUMBER'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'PARTY NUMBER'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'PARTY_NUMBER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2570914507920991)
,p_db_column_name=>'PARTY_NAME'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'PARTY NAME'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'PARTY_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2571231574920991)
,p_db_column_name=>'ACCOUNT_NUMBER'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'CUSTOMER NUMBER'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ACCOUNT_NUMBER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2571327013920991)
,p_db_column_name=>'ACCOUNT_NAME'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'CUSTOMER NAME'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ACCOUNT_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2571426432920992)
,p_db_column_name=>'B2B_SO_CNT'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'B2B SO COUNT'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'B2B_SO_CNT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2571518672920992)
,p_db_column_name=>'B2B_SO_AMT'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'B2B SO SALES$'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'B2B_SO_AMT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2571800339920992)
,p_db_column_name=>'XML_PO_CNT'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'XML PO COUNT'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'XML_PO_CNT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2571900737920992)
,p_db_column_name=>'XML_PO_AMT'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'XML PO SALES$'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'XML_PO_AMT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2572030123920993)
,p_db_column_name=>'XML_INV_CNT'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'XML INV COUNT'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'XML_INV_CNT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2572105962920993)
,p_db_column_name=>'XML_INV_AMT'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'XML INV SALES$'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'XML_INV_AMT'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(2572219956920993)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'25723'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PARTY_NUMBER:PARTY_NAME:ACCOUNT_NUMBER:ACCOUNT_NAME:B2B_SO_CNT:B2B_SO_AMT:XML_PO_CNT:XML_PO_AMT:XML_INV_CNT:XML_INV_AMT'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2572415632920993)
,p_plug_name=>'Find Region'
,p_parent_plug_id=>wwv_flow_api.id(2570311453920988)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2573024980920995)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(2572415632920993)
,p_button_name=>'P41_FIND'
,p_button_static_id=>'P41_FIND'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'FIND'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
,p_grid_new_row=>'Y'
,p_grid_new_column=>'Y'
,p_grid_column=>18
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2573229149920995)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(2572415632920993)
,p_button_name=>'P41_CLEAR'
,p_button_static_id=>'P41_CLEAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'CLEAR'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:41:&SESSION.::&DEBUG.:41:P41_FROM_DATE,P41_TO_DATE:,'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>19
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2573419287920995)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(2572415632920993)
,p_button_name=>'P41_BACK'
,p_button_static_id=>'P41_BACK'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'< BACK'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>20
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2572609565920994)
,p_name=>'P41_FROM_DATE'
,p_is_required=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(2572415632920993)
,p_prompt=>'From Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2572812301920994)
,p_name=>'P41_TO_DATE'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(2572415632920993)
,p_prompt=>'To Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
end;
/
prompt --application/pages/page_00042
begin
wwv_flow_api.create_page(
 p_id=>42
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'# Of Trading Partners Added to B2B'
,p_page_mode=>'NORMAL'
,p_step_title=>'# Of Trading Partners Added to B2B'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'# Of Trading Partners Added to B2B'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_browser_cache=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20151012132107'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2460305948502152)
,p_plug_name=>'# Of Trading Partners Added to B2B - Header'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2460511668502153)
,p_plug_name=>'# Of Trading Partners Added to B2B'
,p_parent_plug_id=>wwv_flow_api.id(2460305948502152)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>11
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select TO_CHAR (bbc.creation_date, ''MON-YYYY'') PERIOD, NVL(STATUS, ''DRAFT'') STATUS, COUNT(1) COUNT',
'from xxwc.xxwc_b2b_cust_info_tbl bbc',
'WHERE TRUNC(CREATION_DATE) BETWEEN :P42_FROM_DATE AND :P42_TO_DATE',
'group by NVL(STATUS, ''DRAFT''), TO_CHAR (bbc.creation_date, ''MON-YYYY'')'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P42_FROM_DATE IS NOT NULL AND :P42_TO_DATE IS NOT NULL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(2460702349502153)
,p_name=>'B2B Metrics Summary'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'TD002849'
,p_internal_uid=>2460702349502153
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2460804523502154)
,p_db_column_name=>'PERIOD'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'PERIOD'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PERIOD'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2460905029502154)
,p_db_column_name=>'COUNT'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'COUNT'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'COUNT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2461007297502154)
,p_db_column_name=>'STATUS'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'STATUS'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'STATUS'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(2461124946502155)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'24612'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PERIOD:COUNT:STATUS'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2461323538502155)
,p_plug_name=>'Find Region'
,p_parent_plug_id=>wwv_flow_api.id(2460305948502152)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2462129022502157)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(2461323538502155)
,p_button_name=>'P42_FIND'
,p_button_static_id=>'P42_FIND'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'FIND'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
,p_grid_new_row=>'Y'
,p_grid_new_column=>'Y'
,p_grid_column=>18
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2462307095502157)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(2461323538502155)
,p_button_name=>'P42_CLEAR'
,p_button_static_id=>'P42_CLEAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'CLEAR'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:42:&SESSION.::&DEBUG.:42:P42_FROM_DATE,P42_TO_DATE:,'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>19
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2461502750502156)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(2461323538502155)
,p_button_name=>'P42_BACK'
,p_button_static_id=>'P42_BACK'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'< BACK'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>20
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2461731763502156)
,p_name=>'P42_FROM_DATE'
,p_is_required=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(2461323538502155)
,p_prompt=>'From Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2461914938502156)
,p_name=>'P42_TO_DATE'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(2461323538502155)
,p_prompt=>'To Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
end;
/
prompt --application/pages/page_00043
begin
wwv_flow_api.create_page(
 p_id=>43
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Trading Partners Added to B2B'
,p_page_mode=>'NORMAL'
,p_step_title=>'Trading Partners Added to B2B'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Trading Partners Added to B2B'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_browser_cache=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20151012132134'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2464301014504848)
,p_plug_name=>'Trading Partners Added to B2B - Header'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2464520185504849)
,p_plug_name=>'Trading Partners Added to B2B'
,p_parent_plug_id=>wwv_flow_api.id(2464301014504848)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>11
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT hca.account_number customer_number',
'     , hca.account_name customer_name',
'     , hp.party_number ',
'     , REPLACE(hp.party_name, '','' , '' '') party_name',
'     , b2bc.status',
'     , b2bc.creation_date',
'     , b2bc.last_update_date',
' FROM apps.hz_Cust_accounts_all hca',
'    , apps.hz_parties hp',
'    , xxwc.xxwc_b2b_cust_info_tbl b2bc',
'WHERE 1 = 1',
'  AND hca.party_id = hp.party_id',
'  AND b2bc.party_id = hp.party_id',
'  AND TRUNC(b2bc.creation_date) BETWEEN :P43_FROM_DATE AND :P43_TO_DATE'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P43_FROM_DATE IS NOT NULL AND :P43_TO_DATE IS NOT NULL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(2464728162504849)
,p_name=>'B2B Metrics Summary'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'TD002849'
,p_internal_uid=>2464728162504849
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2464824942504849)
,p_db_column_name=>'PARTY_NUMBER'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'PARTY NUMBER'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PARTY_NUMBER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2464929215504849)
,p_db_column_name=>'PARTY_NAME'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'PARTY NAME'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'PARTY_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2465032600504850)
,p_db_column_name=>'CUSTOMER_NUMBER'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'CUSTOMER NUMBER'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CUSTOMER_NUMBER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2465111071504850)
,p_db_column_name=>'CUSTOMER_NAME'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'CUSTOMER NAME'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'CUSTOMER_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2465221790504850)
,p_db_column_name=>'STATUS'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'STATUS'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'STATUS'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2465322763504850)
,p_db_column_name=>'CREATION_DATE'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'CREATION DATE'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_format_mask=>'DD-MON-YYYY HH24:MI:SS'
,p_tz_dependent=>'N'
,p_static_id=>'CREATION_DATE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2465415363504850)
,p_db_column_name=>'LAST_UPDATE_DATE'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'LAST UPDATE DATE'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_format_mask=>'DD-MON-YYYY HH24:MI:SS'
,p_tz_dependent=>'N'
,p_static_id=>'LAST_UPDATE_DATE'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(2465529735504851)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'24656'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PARTY_NUMBER:PARTY_NAME:CUSTOMER_NUMBER:CUSTOMER_NAME:STATUS:CREATION_DATE:LAST_UPDATE_DATE'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2465710088504851)
,p_plug_name=>'Find Region'
,p_parent_plug_id=>wwv_flow_api.id(2464301014504848)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2466307669504853)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(2465710088504851)
,p_button_name=>'P43_FIND'
,p_button_static_id=>'P43_FIND'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'FIND'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
,p_grid_new_row=>'Y'
,p_grid_new_column=>'Y'
,p_grid_column=>18
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2466511685504853)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(2465710088504851)
,p_button_name=>'P43_CLEAR'
,p_button_static_id=>'P43_CLEAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'CLEAR'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:43:&SESSION.::&DEBUG.:43:P43_FROM_DATE,P43_TO_DATE:,'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>19
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2466728490504853)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(2465710088504851)
,p_button_name=>'P43_BACK'
,p_button_static_id=>'P43_BACK'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'< BACK'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>20
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2465931048504852)
,p_name=>'P43_FROM_DATE'
,p_is_required=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(2465710088504851)
,p_prompt=>'From Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2466119438504853)
,p_name=>'P43_TO_DATE'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(2465710088504851)
,p_prompt=>'To Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
end;
/
prompt --application/pages/page_00044
begin
wwv_flow_api.create_page(
 p_id=>44
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Order Status Update - SetUp Metrics'
,p_page_mode=>'NORMAL'
,p_step_title=>'Order Status Update - Setup Metrics'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'B2B SetUp Stats'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_browser_cache=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20170310124046'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3453824492314325)
,p_plug_name=>'Order Status Update - Setup Metrics'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3454018151314335)
,p_plug_name=>'Order Status Update - Setup Metrics'
,p_parent_plug_id=>wwv_flow_api.id(3453824492314325)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>11
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select  b2b.Party_ID, b2b.account_number, b2b.account_name, b2b.LOCATION,        case deliver_soa',
'         when ''Y'' then',
'          ''A''',
'         when ''N'' then',
'          ''-''',
'         when ''D'' then',
'          ''D''',
'       end deliver_soa,',
'       case deliver_asn',
'         when ''Y'' then',
'          ''A''',
'         when ''N'' then',
'          ''-''',
'         when ''D'' then',
'          ''D''',
'       end deliver_asn,',
'       case deliver_pod',
'         when ''Y'' then',
'          ''A''',
'         when ''N'' then',
'          ''-''',
'         when ''D'' then',
'          ''D''',
'       end deliver_pod,',
'       case deliver_invoice',
'         when ''Y'' then',
'          ''A''',
'         when ''N'' then',
'          ''-''',
'         when ''D'' then',
'          ''D''',
'       end deliver_invoice,  mp.organization_code, ',
'  mp.attribute8 District,',
'  mp.attribute9 Region,fu.user_Name, fu.description, b2b.creation_date,',
'  b2b.last_update_date,',
'    ( SELECT description',
'      FROM apps.fnd_user where user_id = b2b.last_updated_by) last_updated_by ',
'from xxwc.xxwc_b2b_config_tbl b2b',
'     , apps.fnd_user fu',
'     , apps.fnd_profile_option_values fpou',
'     , apps.mtl_parameters mp',
'where b2b.created_by = fu.user_id',
'   and fpou.level_value = fu.user_id',
'   and fpou.profile_option_id = 11713 ',
'   and fpou.profile_option_value = mp.organization_id',
'   and (deliver_invoice in (''Y'',''D'') or deliver_pod in (''Y'', ''D'') or deliver_soa in (''Y'',''D'') or deliver_asn in (''Y'',''D''))',
'   and TRUNC(b2b.creation_date) BETWEEN :P44_FROM_DATE AND :P44_TO_DATE',
'order by fu.user_name, b2b.account_name, b2b.LOCATION'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P44_FROM_DATE IS NOT NULL AND :P44_TO_DATE IS NOT NULL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(3454200856314336)
,p_name=>'B2B Metrics Summary'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'TD002849'
,p_internal_uid=>3454200856314336
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3454532199314345)
,p_db_column_name=>'DELIVER_SOA'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Deliver SOA'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'DELIVER_SOA'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3454625581314345)
,p_db_column_name=>'DELIVER_ASN'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Deliver ASN'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'DELIVER_ASN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3454722308314345)
,p_db_column_name=>'DELIVER_INVOICE'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Deliver Invoice'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'DELIVER_INVOICE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3454817040314346)
,p_db_column_name=>'ORGANIZATION_CODE'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'Organization Code'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ORGANIZATION_CODE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3454912493314346)
,p_db_column_name=>'USER_NAME'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'NTID'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'USER_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3455026243314346)
,p_db_column_name=>'DESCRIPTION'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'User'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'DESCRIPTION'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3829905262663099)
,p_db_column_name=>'ACCOUNT_NUMBER'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'Account Number'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ACCOUNT_NUMBER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3830001983663099)
,p_db_column_name=>'ACCOUNT_NAME'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'Account Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'ACCOUNT_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3830115719663099)
,p_db_column_name=>'LOCATION'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'Location'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'LOCATION'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3830208896663099)
,p_db_column_name=>'DELIVER_POD'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'Deliver POD'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DELIVER_POD'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(4760207098973260)
,p_db_column_name=>'CREATION_DATE'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'Creation Date'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'CREATION_DATE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(4760423771973263)
,p_db_column_name=>'LAST_UPDATE_DATE'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'Last Update Date'
,p_allow_pivot=>'N'
,p_column_type=>'DATE'
,p_tz_dependent=>'N'
,p_static_id=>'LAST_UPDATE_DATE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(4761706710186089)
,p_db_column_name=>'DISTRICT'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'District'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'DISTRICT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(4761830061186089)
,p_db_column_name=>'REGION'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'Region'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'REGION'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(4762022403239538)
,p_db_column_name=>'PARTY_ID'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'Party ID'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
,p_tz_dependent=>'N'
,p_static_id=>'PARTY_ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(4762303445331700)
,p_db_column_name=>'LAST_UPDATED_BY'
,p_display_order=>21
,p_column_identifier=>'U'
,p_column_label=>'Last Updated By'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'LAST_UPDATED_BY'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(3455106314314347)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'34552'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PARTY_ID:ACCOUNT_NUMBER:ACCOUNT_NAME:LOCATION:DELIVER_SOA:DELIVER_ASN:DELIVER_POD:DELIVER_INVOICE:ORGANIZATION_CODE:DISTRICT:REGION:USER_NAME:DESCRIPTION:CREATION_DATE:LAST_UPDATE_DATE:LAST_UPDATED_BY'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3455324835314349)
,p_plug_name=>'Find Region'
,p_parent_plug_id=>wwv_flow_api.id(3453824492314325)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3455925246314352)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(3455324835314349)
,p_button_name=>'P44_FIND'
,p_button_static_id=>'P44_FIND'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'FIND'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
,p_grid_new_row=>'Y'
,p_grid_new_column=>'Y'
,p_grid_column=>18
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3456104253314352)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(3455324835314349)
,p_button_name=>'P44_CLEAR'
,p_button_static_id=>'P44_CLEAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'CLEAR'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:44:&SESSION.::&DEBUG.:44:P44_FROM_DATE,P44_TO_DATE:,'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>19
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3456330807314353)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(3455324835314349)
,p_button_name=>'P44_BACK'
,p_button_static_id=>'P44_BACK'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'< BACK'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>20
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3455532193314350)
,p_name=>'P44_FROM_DATE'
,p_is_required=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(3455324835314349)
,p_prompt=>'From Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3455727096314352)
,p_name=>'P44_TO_DATE'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(3455324835314349)
,p_prompt=>'To Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
end;
/
prompt --application/pages/page_00045
begin
wwv_flow_api.create_page(
 p_id=>45
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Order Status Update - POD Setup Metrics'
,p_page_mode=>'NORMAL'
,p_step_title=>'Order Status Update - POD Setup Metrics'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'B2B - POD SetUp Stats'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_browser_cache=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20160502223723'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3458510305321424)
,p_plug_name=>'Order Status Update - POD Setup Metrics'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3458723559321425)
,p_plug_name=>'Order Status Update - POD Setup Metrics'
,p_parent_plug_id=>wwv_flow_api.id(3458510305321424)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>11
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct account_number, account_name, location , deliver_pod, mp.organization_code, fu.user_Name, fu.description --, fpou.*',
'  from xxwc.xxwc_b2b_pod_info_tbl b2b',
'     , apps.fnd_user fu',
'     , apps.fnd_profile_option_values fpou',
'     , apps.mtl_parameters mp',
'where b2b.created_by = fu.user_id',
'   and fpou.level_value = fu.user_id',
'   and fpou.profile_option_id = 11713 ',
'   and fpou.profile_option_value = mp.organization_id',
'   and TRUNC(b2b.creation_date) BETWEEN :P45_FROM_DATE AND :P45_TO_DATE',
'order by fu.user_name   , account_number'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P45_FROM_DATE IS NOT NULL AND :P45_TO_DATE IS NOT NULL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(3458926259321425)
,p_name=>'B2B Metrics Summary'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'TD002849'
,p_internal_uid=>3458926259321425
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3459024431321426)
,p_db_column_name=>'ORGANIZATION_CODE'
,p_display_order=>6
,p_column_identifier=>'F'
,p_column_label=>'ORGANIZATION CODE'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ORGANIZATION_CODE'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3459102962321426)
,p_db_column_name=>'USER_NAME'
,p_display_order=>7
,p_column_identifier=>'G'
,p_column_label=>'NTID'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'USER_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3459201648321426)
,p_db_column_name=>'DESCRIPTION'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'USER'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'DESCRIPTION'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3459326718321426)
,p_db_column_name=>'ACCOUNT_NUMBER'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'ACCOUNT NUMBER'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ACCOUNT_NUMBER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3459428633321427)
,p_db_column_name=>'ACCOUNT_NAME'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'ACCOUNT NAME'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ACCOUNT_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3459511678321427)
,p_db_column_name=>'LOCATION'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'LOCATION'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'LOCATION'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3459628670321427)
,p_db_column_name=>'DELIVER_POD'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'DELIVER POD'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'DELIVER_POD'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(3459722317321427)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'34598'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'ORGANIZATION_CODE:USER_NAME:DESCRIPTION:ACCOUNT_NUMBER:ACCOUNT_NAME:LOCATION:DELIVER_POD'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3459931843321428)
,p_plug_name=>'Find Region'
,p_parent_plug_id=>wwv_flow_api.id(3458510305321424)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3460531434321430)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(3459931843321428)
,p_button_name=>'P45_FIND'
,p_button_static_id=>'P45_FIND'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'FIND'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
,p_grid_new_row=>'Y'
,p_grid_new_column=>'Y'
,p_grid_column=>18
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3460713297321430)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(3459931843321428)
,p_button_name=>'P45_CLEAR'
,p_button_static_id=>'P45_CLEAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'CLEAR'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:45:&SESSION.::&DEBUG.:45:P45_FROM_DATE,P45_TO_DATE:,'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>19
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3460912219321431)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(3459931843321428)
,p_button_name=>'P45_BACK'
,p_button_static_id=>'P45_BACK'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'< BACK'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>20
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3460112605321429)
,p_name=>'P45_FROM_DATE'
,p_is_required=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(3459931843321428)
,p_prompt=>'From Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3460318082321430)
,p_name=>'P45_TO_DATE'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(3459931843321428)
,p_prompt=>'To Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
end;
/
prompt --application/pages/page_00053
begin
wwv_flow_api.create_page(
 p_id=>53
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Login Page'
,p_alias=>'LOGIN_DESKTOP'
,p_page_mode=>'NORMAL'
,p_step_title=>'Login Page'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_api.id(24614592041658531)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'Y'
,p_cache_mode=>'NOCACHE'
,p_last_updated_by=>'ADMIN'
,p_last_upd_yyyymmddhh24miss=>'20150824132233'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2763329058061549)
,p_plug_name=>'Login Page'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617090878658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2763928277061565)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(2763329058061549)
,p_button_name=>'P53_LOGIN'
,p_button_static_id=>'P53_LOGIN'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Login'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_request_source=>'LOGIN'
,p_request_source_type=>'STATIC'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2763503180061558)
,p_name=>'P53_USERNAME'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(2763329058061549)
,p_prompt=>'Username'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>40
,p_cMaxlength=>100
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2763709388061565)
,p_name=>'P53_PASSWORD'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(2763329058061549)
,p_prompt=>'Password'
,p_display_as=>'NATIVE_PASSWORD'
,p_cSize=>40
,p_cMaxlength=>100
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2764330758061570)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Set Username Cookie'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex_authentication.send_login_username_cookie (',
'    p_username => lower(:P53_USERNAME) );'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2764124240061565)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Login'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex_authentication.login(',
'    p_username => :P53_USERNAME,',
'    p_password => :P53_PASSWORD );'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2764725849061571)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'Clear Page(s) Cache'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(2764527884061571)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get Username Cookie'
,p_process_sql_clob=>':P53_USERNAME := apex_authentication.get_login_username_cookie;'
);
end;
/
prompt --application/pages/page_00091
begin
wwv_flow_api.create_page(
 p_id=>91
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Administration'
,p_page_mode=>'NORMAL'
,p_step_title=>'Administration'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Administration'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_api.id(24615000945658539)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20150919094924'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3376817899919022)
,p_plug_name=>'Administration'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>11
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_list_id=>wwv_flow_api.id(3377400104919026)
,p_plug_source_type=>'NATIVE_LIST'
,p_list_template_id=>wwv_flow_api.id(24619086631658557)
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3377125377919025)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24616716460658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(24624313043658631)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(24621899126658564)
,p_plug_query_row_template=>1
);
end;
/
prompt --application/pages/page_00092
begin
wwv_flow_api.create_page(
 p_id=>92
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Update RESPONSIBILITY'
,p_page_mode=>'NORMAL'
,p_step_title=>'Update RESPONSIBILITY'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Update RESPONSIBILITY'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_api.id(24615000945658539)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20150919095240'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3381412966921730)
,p_name=>'Responsibility Journal'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select XXWC_WC_RESP_JOURNAL.JOURNAL_ID as JOURNAL_ID,',
'    XXWC_WC_RESP_JOURNAL.RESP_ID as RESP_ID,',
'    XXWC_WC_RESP_JOURNAL.NOTES as NOTES,',
'    apps.XXWC_MS_MASTER_ADMIN_PKG.get_user_name(XXWC_WC_RESP_JOURNAL.CREATED_BY) as CREATED_BY,',
'    XXWC_WC_RESP_JOURNAL.CREATED_ON as CREATED_ON,',
'    XXWC_WC_RESP_JOURNAL.UPDATED_BY as UPDATED_BY,',
'    XXWC_WC_RESP_JOURNAL.UPDATED_ON as UPDATED_ON ',
' from xxwc.XXWC_MS_RESP_JOURNAL_TBL XXWC_WC_RESP_JOURNAL',
'WHERE APP_ID =:APP_ID'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3381632404921731)
,p_query_column_id=>1
,p_column_alias=>'JOURNAL_ID'
,p_column_display_sequence=>1
,p_column_heading=>'JOURNAL_ID'
,p_default_sort_column_sequence=>1
,p_default_sort_dir=>'desc'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3381713369921731)
,p_query_column_id=>2
,p_column_alias=>'RESP_ID'
,p_column_display_sequence=>2
,p_column_heading=>'RESP_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3381803845921731)
,p_query_column_id=>3
,p_column_alias=>'NOTES'
,p_column_display_sequence=>3
,p_column_heading=>'Notes'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3381919760921731)
,p_query_column_id=>4
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>4
,p_column_heading=>'Changed By'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3382024290921731)
,p_query_column_id=>5
,p_column_alias=>'CREATED_ON'
,p_column_display_sequence=>5
,p_column_heading=>'Date'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3382128566921731)
,p_query_column_id=>6
,p_column_alias=>'UPDATED_BY'
,p_column_display_sequence=>6
,p_column_heading=>'UPDATED_BY'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3382213826921731)
,p_query_column_id=>7
,p_column_alias=>'UPDATED_ON'
,p_column_display_sequence=>7
,p_column_heading=>'UPDATED_ON'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3382322384921731)
,p_name=>'Maintain Responsibility'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"RESPONSIBILITY_ID",',
'"RESPONSIBILITY_ID" RESPONSIBILITY_ID_DISPLAY,',
'trim("RESPONSIBILITY_NAME") RESPONSIBILITY_NAME,',
'"APP_ID"',
'from xxwc.XXWC_MS_RESPONSIBILITY_TBL',
'WHERE APP_ID =:APP_ID',
''))
,p_source_type=>'NATIVE_TABFORM'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3382502418921735)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'&nbsp;'
,p_use_as_row_header=>'N'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3382609429921735)
,p_query_column_id=>2
,p_column_alias=>'RESPONSIBILITY_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Responsibility Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_pk_col_source=>'XXWC.XXWC_MS_RESPONSIBILITY_TRG'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_RESPONSIBILITY_TBL'
,p_ref_column_name=>'RESPONSIBILITY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3382720584921735)
,p_query_column_id=>3
,p_column_alias=>'RESPONSIBILITY_ID_DISPLAY'
,p_column_display_sequence=>2
,p_column_heading=>'Responsibility Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_RESPONSIBILITY_TBL'
,p_ref_column_name=>'RESPONSIBILITY_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3382831333921735)
,p_query_column_id=>4
,p_column_alias=>'RESPONSIBILITY_NAME'
,p_column_display_sequence=>4
,p_column_heading=>'Responsibility Name'
,p_use_as_row_header=>'N'
,p_display_as=>'TEXT'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3382927666921736)
,p_query_column_id=>5
,p_column_alias=>'APP_ID'
,p_column_display_sequence=>5
,p_column_heading=>'App Id'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3384308896921740)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24616716460658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(24624313043658631)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(24621899126658564)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3383015846921736)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(3382322384921731)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:addRow();'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3383227615921736)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(3382322384921731)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Submit'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3383429017921736)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(3382322384921731)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:91:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3383618745921737)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(3382322384921731)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3384731150921741)
,p_branch_action=>'f?p=&APP_ID.:92:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3383826588921737)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(3382322384921731)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_RESPONSIBILITY_TBL'
,p_attribute_03=>'RESPONSIBILITY_ID'
,p_process_error_message=>'Unable to process update.'
,p_process_when_button_id=>wwv_flow_api.id(3383227615921736)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3384006638921738)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(3382322384921731)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_RESPONSIBILITY_TBL'
,p_attribute_03=>'RESPONSIBILITY_ID'
,p_process_error_message=>'Unable to process delete.'
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
end;
/
prompt --application/pages/page_00093
begin
wwv_flow_api.create_page(
 p_id=>93
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Update Role Responsibilities'
,p_page_mode=>'NORMAL'
,p_step_title=>'Update Role Responsibilities'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Update Role Responsibilities'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'NO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_step_template=>wwv_flow_api.id(24615000945658539)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20150919095254'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3387801233924820)
,p_name=>'Roles Responsibilites Journal'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select xxwc.xxwc_wc_roles_RESP_JOURNAL.JOURNAL_ID as JOURNAL_ID,',
'    xxwc.xxwc_wc_roles_RESP_JOURNAL.ROLES_RESP_ID as ROLES_RESP_ID,',
'    xxwc.xxwc_wc_roles_RESP_JOURNAL.NOTES as NOTES,',
'    apps.xxwc_ms_master_admin_pkg.get_user_name(xxwc_wc_roles_RESP_JOURNAL.CREATED_BY) as CREATED_BY,',
'    xxwc.xxwc_wc_roles_RESP_JOURNAL.CREATED_ON as CREATED_ON,',
'    xxwc.xxwc_wc_roles_RESP_JOURNAL.UPDATED_BY as UPDATED_BY,',
'    xxwc.xxwc_wc_roles_RESP_JOURNAL.UPDATED_ON as UPDATED_ON ',
' from XXWC.XXWC_MS_ROLES_RESP_JOURNAL_TBL xxwc_wc_roles_RESP_JOURNAL',
'WHERE APP_ID =:APP_ID',
'',
''))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3388022766924820)
,p_query_column_id=>1
,p_column_alias=>'JOURNAL_ID'
,p_column_display_sequence=>1
,p_column_heading=>'JOURNAL_ID'
,p_default_sort_column_sequence=>1
,p_default_sort_dir=>'desc'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3388110505924820)
,p_query_column_id=>2
,p_column_alias=>'ROLES_RESP_ID'
,p_column_display_sequence=>2
,p_column_heading=>'ROLES_RESP_ID'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3388217706924821)
,p_query_column_id=>3
,p_column_alias=>'NOTES'
,p_column_display_sequence=>3
,p_column_heading=>'Notes'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3388308008924821)
,p_query_column_id=>4
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>4
,p_column_heading=>'Changed By'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3388415651924821)
,p_query_column_id=>5
,p_column_alias=>'CREATED_ON'
,p_column_display_sequence=>5
,p_column_heading=>'Date'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3388530919924821)
,p_query_column_id=>6
,p_column_alias=>'UPDATED_BY'
,p_column_display_sequence=>6
,p_column_heading=>'UPDATED_BY'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3388622169924821)
,p_query_column_id=>7
,p_column_alias=>'UPDATED_ON'
,p_column_display_sequence=>7
,p_column_heading=>'UPDATED_ON'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3388709454924821)
,p_name=>'Role Responsibilities'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'"ROLE_RESP_ID",',
'"ROLE_RESP_ID" ROLE_RESP_ID_DISPLAY,',
'"RESPONSIBILITY_ID",',
'"ROLE_ID",',
'"APP_ID"',
'from xxwc.xxwc_ms_role_resp_tbl',
'WHERE APP_ID =:APP_ID'))
,p_source_type=>'NATIVE_TABFORM'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3388930250924822)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'&nbsp;'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3389002638924823)
,p_query_column_id=>2
,p_column_alias=>'ROLE_RESP_ID'
,p_column_display_sequence=>3
,p_column_heading=>'Role Resp Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_pk_col_source=>'XXWC_MS_ROLE_RESP_TRG'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLE_RESP_TBL'
,p_ref_column_name=>'ROLE_RESP_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3389115116924823)
,p_query_column_id=>3
,p_column_alias=>'ROLE_RESP_ID_DISPLAY'
,p_column_display_sequence=>2
,p_column_heading=>'Role Resp Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLE_RESP_TBL'
,p_ref_column_name=>'ROLE_RESP_ID_DISPLAY'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3389203771924823)
,p_query_column_id=>4
,p_column_alias=>'RESPONSIBILITY_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Responsibility Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select RESPONSIBILITY_NAME display_value, RESPONSIBILITY_ID return_value ',
'from xxwc.XXWC_MS_RESPONSIBILITY_TBL',
'WHERE APP_ID =:APP_ID',
'order by 1'))
,p_lov_show_nulls=>'NO'
,p_column_width=>40
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLE_RESP_TBL'
,p_ref_column_name=>'RESPONSIBILITY_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3389326744924823)
,p_query_column_id=>5
,p_column_alias=>'ROLE_ID'
,p_column_display_sequence=>5
,p_column_heading=>'Role Id'
,p_use_as_row_header=>'N'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select r.ROLE_NAME display_value, r.ROLE_ID return_value ',
'from XXWC.XXWC_MS_ROLES_TBL r, XXWC.XXWC_MS_USER_ROLES_TBL w',
'where r.role_id = w.role_id',
'and w.user_id = :APP_USER',
'and w.owner_flag = ''Y''',
'order by 1'))
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
,p_ref_table_name=>'XXWC_MS_ROLE_RESP_TBL'
,p_ref_column_name=>'ROLE_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3389430412924823)
,p_query_column_id=>6
,p_column_alias=>'APP_ID'
,p_column_display_sequence=>6
,p_column_heading=>'App Id'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3391226444924827)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24616716460658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(24624313043658631)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(24621899126658564)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3389523441924823)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(3388709454924821)
,p_button_name=>'ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Add Row'
,p_button_position=>'BOTTOM'
,p_button_redirect_url=>'javascript:addRow();'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3389703165924823)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(3388709454924821)
,p_button_name=>'SUBMIT'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Submit'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3389902656924824)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(3388709454924821)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:91:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3390123191924824)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(3388709454924821)
,p_button_name=>'MULTI_ROW_DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''MULTI_ROW_DELETE'');'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3391602345924828)
,p_branch_action=>'f?p=&APP_ID.:93:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(3390431690924825)
,p_tabular_form_region_id=>wwv_flow_api.id(3388709454924821)
,p_validation_name=>'RESPONSIBILITY_ID must be numeric'
,p_validation_sequence=>30
,p_validation=>'RESPONSIBILITY_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(3389703165924823)
,p_associated_column=>'RESPONSIBILITY_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(3390609707924826)
,p_tabular_form_region_id=>wwv_flow_api.id(3388709454924821)
,p_validation_name=>'ROLE_ID must be numeric'
,p_validation_sequence=>40
,p_validation=>'ROLE_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_always_execute=>'N'
,p_when_button_pressed=>wwv_flow_api.id(3389703165924823)
,p_associated_column=>'ROLE_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3390723095924826)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(3388709454924821)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_ROLE_RESP_TBL'
,p_attribute_03=>'ROLE_RESP_ID'
,p_process_error_message=>'Unable to process update.'
,p_process_when_button_id=>wwv_flow_api.id(3389703165924823)
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3390904899924826)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(3388709454924821)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_ROLE_RESP_TBL'
,p_attribute_03=>'ROLE_RESP_ID'
,p_process_error_message=>'Unable to process delete.'
,p_process_when_button_id=>wwv_flow_api.id(3390123191924824)
,p_process_when=>'MULTI_ROW_DELETE'
,p_process_when_type=>'REQUEST_EQUALS_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
end;
/
prompt --application/pages/page_00094
begin
wwv_flow_api.create_page(
 p_id=>94
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'Users'
,p_page_mode=>'NORMAL'
,p_step_title=>'Users'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Users'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_step_template=>wwv_flow_api.id(24615000945658539)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20150919103654'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3394606191927467)
,p_plug_name=>'Users'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24618090021658553)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY_3'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'SELECT ',
'   "HDS_USER_PULL_MVW"."EMPLOYEE_ID" "EMPLOYEE_ID", ',
'   "HDS_USER_PULL_MVW"."FIRST_NAME" "FIRST_NAME", ',
'   "HDS_USER_PULL_MVW"."LAST_NAME" "LAST_NAME", ',
'   "HDS_USER_PULL_MVW"."NAME" "NAME", ',
'   "HDS_USER_PULL_MVW"."EMAIL" "EMAIL"',
'FROM ',
'   APPS.XXWC_HDS_USER_PULL_MV "HDS_USER_PULL_MVW"'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(3394803055927468)
,p_name=>'Users'
,p_max_row_count=>'10000'
,p_max_row_count_message=>'This query returns more than #MAX_ROW_COUNT# rows, please filter your data to ensure complete results.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_show_display_row_count=>'Y'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'N'
,p_show_rows_per_page=>'N'
,p_show_pivot=>'N'
,p_show_notify=>'Y'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML:EMAIL'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MR024211'
,p_internal_uid=>3394803055927468
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3394920788927469)
,p_db_column_name=>'EMPLOYEE_ID'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'Employee Id'
,p_column_link=>'f?p=&APP_ID.:95:&SESSION.::&DEBUG.::P95_EMPLOYEE_ID:#EMPLOYEE_ID#'
,p_column_linktext=>'#EMPLOYEE_ID#'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_tz_dependent=>'N'
,p_static_id=>'EMPLOYEE_ID'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3395021932927469)
,p_db_column_name=>'FIRST_NAME'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'First Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'FIRST_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3395111893927470)
,p_db_column_name=>'LAST_NAME'
,p_display_order=>3
,p_column_identifier=>'C'
,p_column_label=>'Last Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'LAST_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3395231659927470)
,p_db_column_name=>'NAME'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'Name'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(3395312808927470)
,p_db_column_name=>'EMAIL'
,p_display_order=>5
,p_column_identifier=>'E'
,p_column_label=>'Email'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_tz_dependent=>'N'
,p_static_id=>'EMAIL'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(3395402740927470)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'33955'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'EMPLOYEE_ID:FIRST_NAME:LAST_NAME:NAME:EMAIL'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3395731355927472)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24616716460658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(24624313043658631)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(24621899126658564)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3396022157927473)
,p_button_sequence=>10
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_redirect_url=>'f?p=&APP_ID.:60:&SESSION.::&DEBUG.:60'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_computation(
 p_id=>wwv_flow_api.id(3396332058927473)
,p_computation_sequence=>1
,p_computation_item=>'P95_EMPLOYEE_ID'
,p_computation_type=>'STATIC_ASSIGNMENT'
,p_compute_when=>'CREATE'
,p_compute_when_type=>'REQUEST_EQUALS_CONDITION'
);
end;
/
prompt --application/pages/page_00095
begin
wwv_flow_api.create_page(
 p_id=>95
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'User Roles'
,p_page_mode=>'NORMAL'
,p_step_title=>'User Roles'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'User Roles'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';',
'var htmldb_ch_message=''"OK_TO_GET_NEXT_PREV_PK_VALUE"'';'))
,p_step_template=>wwv_flow_api.id(24615000945658539)
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'GP050872'
,p_last_upd_yyyymmddhh24miss=>'20150919104007'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3398827227930540)
,p_name=>'User Roles Journal'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>25
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select xxwc_wc_user_roles_JOURNAL.JOURNAL_ID as JOURNAL_ID,',
'    xxwc_wc_user_roles_JOURNAL.USER_ROLES_ID as USER_ROLES_ID,',
'    xxwc_wc_user_roles_JOURNAL.NOTES as NOTES,',
'apps.xxwc_ms_master_admin_pkg.get_user_name(xxwc_wc_user_roles_JOURNAL.CREATED_BY) as CREATED_BY,',
'      xxwc_wc_user_roles_JOURNAL.CREATED_ON as CREATED_ON,',
'    xxwc_wc_user_roles_JOURNAL.UPDATED_BY as UPDATED_BY,',
'    xxwc_wc_user_roles_JOURNAL.UPDATED_ON as UPDATED_ON ',
' from XXWC.XXWC_MS_USER_ROLES_JOURNAL_TBL xxwc_wc_user_roles_JOURNAL',
'where user_id = :P95_EMPLOYEE_ID'))
,p_source_type=>'NATIVE_SQL_REPORT'
,p_ajax_enabled=>'Y'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>' - '
,p_query_break_cols=>'0'
,p_query_no_data_found=>'no data found'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3399026184930541)
,p_query_column_id=>1
,p_column_alias=>'JOURNAL_ID'
,p_column_display_sequence=>1
,p_column_heading=>'JOURNAL_ID'
,p_use_as_row_header=>'N'
,p_default_sort_column_sequence=>1
,p_default_sort_dir=>'desc'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3399117924930541)
,p_query_column_id=>2
,p_column_alias=>'USER_ROLES_ID'
,p_column_display_sequence=>2
,p_column_heading=>'USER_ROLES_ID'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3399203025930541)
,p_query_column_id=>3
,p_column_alias=>'NOTES'
,p_column_display_sequence=>3
,p_column_heading=>'Notes'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3399300141930541)
,p_query_column_id=>4
,p_column_alias=>'CREATED_BY'
,p_column_display_sequence=>4
,p_column_heading=>'Changed By'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3399423020930541)
,p_query_column_id=>5
,p_column_alias=>'CREATED_ON'
,p_column_display_sequence=>5
,p_column_heading=>'Date'
,p_use_as_row_header=>'N'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3399515161930541)
,p_query_column_id=>6
,p_column_alias=>'UPDATED_BY'
,p_column_display_sequence=>6
,p_column_heading=>'UPDATED_BY'
,p_use_as_row_header=>'N'
,p_hidden_column=>'Y'
,p_lov_show_nulls=>'NO'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3399611082930541)
,p_query_column_id=>7
,p_column_alias=>'UPDATED_ON'
,p_column_display_sequence=>7
,p_column_heading=>'UPDATED_ON'
,p_hidden_column=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3399705763930542)
,p_plug_name=>'Employees'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_column=>1
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(3402314955930548)
,p_name=>'Employee Roles'
,p_template=>wwv_flow_api.id(24617998492658552)
,p_display_sequence=>15
,p_include_in_reg_disp_sel_yn=>'N'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_new_grid_row=>false
,p_new_grid_column=>false
,p_display_column=>1
,p_display_point=>'BODY_3'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ',
'u.USER_ROLE_ID,',
'u.USER_ID,',
'u.ROLE_ID',
'from xxwc.xxwc_ms_user_roles_tbl u',
'where u.USER_ID = :P95_EMPLOYEE_ID'))
,p_source_type=>'NATIVE_TABFORM'
,p_display_when_condition=>'P95_EMPLOYEE_ID'
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_ajax_enabled=>'N'
,p_fixed_header=>'NONE'
,p_query_row_template=>wwv_flow_api.id(24620892458658561)
,p_query_num_rows=>10
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'(null)'
,p_query_break_cols=>'0'
,p_query_no_data_found=>'No data found.'
,p_query_num_rows_type=>'ROW_RANGES_IN_SELECT_LIST'
,p_query_row_count_max=>500
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_break_type_flag=>'DEFAULT_BREAK_FORMATTING'
,p_csv_output=>'N'
,p_query_asc_image=>'apex/builder/dup.gif'
,p_query_asc_image_attr=>'width="16" height="16" alt="" '
,p_query_desc_image=>'apex/builder/ddown.gif'
,p_query_desc_image_attr=>'width="16" height="16" alt="" '
,p_plug_query_strip_html=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3402503062930548)
,p_query_column_id=>1
,p_column_alias=>'CHECK$01'
,p_column_display_sequence=>1
,p_column_heading=>'&nbsp;'
,p_use_as_row_header=>'N'
,p_display_as=>'CHECKBOX'
,p_derived_column=>'Y'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3402617880930549)
,p_query_column_id=>2
,p_column_alias=>'USER_ROLE_ID'
,p_column_display_sequence=>2
,p_column_heading=>'User Role Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_pk_col_source_type=>'T'
,p_pk_col_source=>'xxwc.xxwc_ms_user_roles_trg'
,p_include_in_export=>'Y'
,p_ref_table_name=>'xxwc_ms_user_roles_trg'
,p_ref_column_name=>'USER_ROLE_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3402727548930549)
,p_query_column_id=>3
,p_column_alias=>'USER_ID'
,p_column_display_sequence=>3
,p_column_heading=>'User Id'
,p_use_as_row_header=>'N'
,p_heading_alignment=>'LEFT'
,p_disable_sort_column=>'N'
,p_hidden_column=>'Y'
,p_display_as=>'HIDDEN'
,p_lov_show_nulls=>'NO'
,p_column_width=>16
,p_column_default=>'P95_EMPLOYEE_ID'
,p_column_default_type=>'ITEM'
,p_lov_display_extra=>'YES'
,p_include_in_export=>'Y'
,p_ref_table_name=>'xxwc_ms_user_roles_tbl'
,p_ref_column_name=>'USER_ID'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(3402811078930549)
,p_query_column_id=>4
,p_column_alias=>'ROLE_ID'
,p_column_display_sequence=>4
,p_column_heading=>'Role Id'
,p_use_as_row_header=>'N'
,p_display_as=>'SELECT_LIST_FROM_QUERY'
,p_inline_lov=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select r.ROLE_NAME display_value, r.ROLE_ID return_value ',
'from xxwc.xxwc_ms_roles_tbl r, XXWC.XXWC_MS_USER_ROLES_TBL w',
'where r.role_id = w.role_id',
'and w.user_id = :APP_USER',
'and w.owner_flag = ''Y''',
'order by 1'))
,p_lov_show_nulls=>'NO'
,p_lov_null_text=>'-Select Role-'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3404113800930551)
,p_plug_name=>'Verbaige'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(3404631865930553)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24616716460658552)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(24624313043658631)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(24621899126658564)
,p_plug_query_row_template=>1
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3402915069930549)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(3402314955930548)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Apply Changes'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P95_EMPLOYEE_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3403114138930549)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(3402314955930548)
,p_button_name=>'APPLY_CHANGES_ADD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Add Row'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_redirect_url=>'javascript:addRow();'
,p_button_execute_validations=>'N'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3399909545930542)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(3399705763930542)
,p_button_name=>'GET_PREVIOUS_EMPLOYEE_ID'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'&lt;'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_redirect_url=>'javascript:htmldb_goSubmit(''GET_PREVIOUS_EMPLOYEE_ID'')'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
,p_button_comment=>'This button is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3400116689930542)
,p_button_sequence=>60
,p_button_plug_id=>wwv_flow_api.id(3399705763930542)
,p_button_name=>'GET_NEXT_EMPLOYEE_ID'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'&gt;'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_redirect_url=>'javascript:htmldb_goSubmit(''GET_NEXT_EMPLOYEE_ID'')'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
,p_button_comment=>'This button is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3400313421930543)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(3399705763930542)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:94:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3400510491930543)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(3399705763930542)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P95_EMPLOYEE_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_grid_new_grid=>false
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3400729862930543)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(3399705763930542)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition_type=>'NEVER'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(3403313222930549)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(3402314955930548)
,p_button_name=>'APPLY_CHANGES_MRD'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(24616088703658545)
,p_button_image_alt=>'Delete Checked'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''APPLY_CHANGES_MRD'');'
,p_button_execute_validations=>'N'
,p_button_condition=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select 1 ',
'from xxwc.xxwc_ms_user_roles_tbl',
'where "USER_ID" = :P95_EMPLOYEE_ID'))
,p_button_condition_type=>'EXISTS'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3405807989930555)
,p_branch_action=>'f?p=&APP_ID.:95:&SESSION.::&DEBUG.::P95_EMPLOYEE_ID:&P95_EMPLOYEE_ID_NEXT.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(3140417174056903)
,p_branch_sequence=>10
,p_branch_condition_type=>'NEVER'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'This button is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3406006553930556)
,p_branch_action=>'f?p=&APP_ID.:95:&SESSION.::&DEBUG.::P95_EMPLOYEE_ID:&P95_EMPLOYEE_ID_PREV.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(15794596225525385)
,p_branch_sequence=>20
,p_branch_condition_type=>'NEVER'
,p_save_state_before_branch_yn=>'Y'
,p_branch_comment=>'This button is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3406228151930556)
,p_branch_action=>'f?p=&APP_ID.:95:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(3402915069930549)
,p_branch_sequence=>30
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(3406402653930559)
,p_branch_action=>'f?p=&APP_ID.:95:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>40
,p_save_state_before_branch_yn=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3400901655930544)
,p_name=>'P95_EMPLOYEE_ID'
,p_item_sequence=>1
,p_item_plug_id=>wwv_flow_api.id(3399705763930542)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Employee Id'
,p_pre_element_text=>'<STRONG>'
,p_post_element_text=>'</STRONG>'
,p_source=>'EMPLOYEE_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3401115709930544)
,p_name=>'P95_FIRST_NAME'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(3399705763930542)
,p_use_cache_before_default=>'NO'
,p_prompt=>'First Name'
,p_source=>'FIRST_NAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>32
,p_cMaxlength=>50
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3401306441930545)
,p_name=>'P95_LAST_NAME'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(3399705763930542)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Last Name'
,p_source=>'LAST_NAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>32
,p_cMaxlength=>50
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621595965658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3401522078930545)
,p_name=>'P95_NAME'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(3399705763930542)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Employee Name'
,p_pre_element_text=>'<strong>'
,p_post_element_text=>'</strong>'
,p_source=>'NAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>32
,p_cMaxlength=>150
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3401727005930546)
,p_name=>'P95_EMAIL'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(3399705763930542)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Email'
,p_source=>'EMAIL'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>32
,p_cMaxlength=>250
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(24621401683658562)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3401919798930546)
,p_name=>'P95_EMPLOYEE_ID_NEXT'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(3399705763930542)
,p_prompt=>'P95_EMPLOYEE_ID_NEXT'
,p_display_as=>'NATIVE_HIDDEN'
,p_cSize=>30
,p_cMaxlength=>2000
,p_cHeight=>1
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_item_comment=>'This item is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3402129190930546)
,p_name=>'P95_EMPLOYEE_ID_PREV'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(3399705763930542)
,p_prompt=>'P95_EMPLOYEE_ID_PREV'
,p_display_as=>'NATIVE_HIDDEN'
,p_colspan=>1
,p_rowspan=>1
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_item_comment=>'This item is needed for Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(3404303293930551)
,p_name=>'P95_LABEL'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(3404113800930551)
,p_prompt=>'<font color= "red"><b>Please do not delete any role from user unless you are sure about it. As it can impact other applications.</b></font>'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621516577658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(3403611524930550)
,p_tabular_form_region_id=>wwv_flow_api.id(3402314955930548)
,p_validation_name=>'ROLE_ID must be numeric'
,p_validation_sequence=>30
,p_validation=>'ROLE_ID'
,p_validation_type=>'ITEM_IS_NUMERIC'
,p_error_message=>'#COLUMN_HEADER# must be numeric.'
,p_always_execute=>'N'
,p_validation_condition=>':request like (''SAVE'') or :request like ''GET_NEXT%'' or :request like ''GET_PREV%'''
,p_validation_condition_type=>'PLSQL_EXPRESSION'
,p_associated_column=>'ROLE_ID'
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3404913098930554)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from HDS_USER_PULL_MVW'
,p_attribute_01=>'APPS'
,p_attribute_02=>'XXWC_HDS_USER_PULL_MV'
,p_attribute_03=>'P95_EMPLOYEE_ID'
,p_attribute_04=>'EMPLOYEE_ID'
,p_process_error_message=>'Unable to fetch row.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3405114754930554)
,p_process_sequence=>20
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_PAGINATION'
,p_process_name=>'Get Next or Previous Primary Key Value'
,p_attribute_01=>'APPS'
,p_attribute_02=>'XXWC_HDS_USER_PULL_MV'
,p_attribute_03=>'P95_EMPLOYEE_ID'
,p_attribute_04=>'EMPLOYEE_ID'
,p_attribute_09=>'P95_EMPLOYEE_ID_NEXT'
,p_attribute_10=>'P95_EMPLOYEE_ID_PREV'
,p_process_error_message=>'Unable to run Get Next or Previous Primary Key Value process.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3405300165930555)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of HDS_USER_PULL_MVW'
,p_attribute_01=>'APPS'
,p_attribute_02=>'XXWC_HDS_USER_PULL_MV'
,p_attribute_03=>'P95_EMPLOYEE_ID'
,p_attribute_04=>'EMPLOYEE_ID'
,p_attribute_11=>'I:U:D'
,p_process_error_message=>'Unable to process row of table HDS_USER_PULL_MV.'
,p_process_when_type=>'NEVER'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3403715055930550)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(3402314955930548)
,p_process_type=>'NATIVE_TABFORM_UPDATE'
,p_process_name=>'ApplyMRU'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_USER_ROLES_TBL'
,p_attribute_03=>'USER_ROLE_ID'
,p_process_error_message=>'Unable to process update.'
,p_process_when=>':request like (''SAVE'') or :request like ''GET_NEXT%'' or :request like ''GET_PREV%'''
,p_process_when_type=>'PLSQL_EXPRESSION'
,p_process_success_message=>'#MRU_COUNT# row(s) updated, #MRI_COUNT# row(s) inserted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3403912267930551)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(3402314955930548)
,p_process_type=>'NATIVE_TABFORM_DELETE'
,p_process_name=>'ApplyMRD'
,p_attribute_01=>'XXWC'
,p_attribute_02=>'XXWC_MS_USER_ROLES_TBL'
,p_attribute_03=>'USER_ROLE_ID'
,p_process_error_message=>'Unable to process delete.'
,p_process_when_button_id=>wwv_flow_api.id(3403313222930549)
,p_process_when=>'APPLY_CHANGES_MRD'
,p_process_when_type=>'REQUEST_IN_CONDITION'
,p_process_success_message=>'#MRD_COUNT# row(s) deleted.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(3405516636930555)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_FOR_PAGES'
,p_attribute_04=>'60'
,p_process_when_button_id=>wwv_flow_api.id(3400729862930543)
,p_process_when_type=>'NEVER'
);
end;
/
prompt --application/pages/page_00099
begin
wwv_flow_api.create_page(
 p_id=>99
,p_user_interface_id=>wwv_flow_api.id(9587179846250856)
,p_name=>'B2B Metrics Summary'
,p_page_mode=>'NORMAL'
,p_step_title=>'B2B Metrics Summary'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'B2B Metrics Summary'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_page_template_options=>'#DEFAULT#'
,p_dialog_chained=>'Y'
,p_overwrite_navigation_list=>'N'
,p_nav_list_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'N'
,p_cache_mode=>'NOCACHE'
,p_cache_timeout_seconds=>21600
,p_help_text=>'No help is available for this page.'
,p_last_updated_by=>'TD002849'
,p_last_upd_yyyymmddhh24miss=>'20151105205856'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2565713857906372)
,p_plug_name=>'B2B Metrics Summary - Header'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617998492658552)
,p_plug_display_sequence=>1
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY_3'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2565921245906384)
,p_plug_name=>'B2B Metrics Summary'
,p_parent_plug_id=>wwv_flow_api.id(2565713857906372)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_display_sequence=>11
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'   SELECT PARTY_NAME,',
'          PARTY_NUMBER,',
'          ACCOUNT_NAME,',
'          ACCOUNT_NUMBER,',
'          COUNT(1) B2B_SO_CNT,',
'          SUM(SALE_AMOUNT) B2B_SO_AMT,',
'          SUM(SOA_DELIVERED) SOA_COUNT,',
'          SUM(SALE_AMOUNT) B2B_SOA_AMT,',
'          SUM(ASN_DELIVERED) ASN_COUNT,',
'          SUM(ASN_DELIVERED_AMT) B2B_ASN_AMT,',
'          SUM(DECODE(ORDER_SOURCE, 1021, 1,0)) XML_PO_CNT,',
'          SUM(DECODE(ORDER_SOURCE, 1021, SALE_AMOUNT,0)) XML_PO_AMT,',
'          SUM(DECODE(ORDER_SOURCE, 1021, INVOICE_COUNT,0)) XML_INV_CNT,',
'          SUM(DECODE(ORDER_SOURCE, 1021, INVOICE_AMOUNT,0)) XML_INV_AMT',
'     FROM APPS.XXWC_B2B_STATS_VW',
'    WHERE TRUNC(CREATION_DATE) BETWEEN :P99_FROM_DATE AND :P99_TO_DATE',
' GROUP BY PARTY_NUMBER,',
'          PARTY_NAME,',
'          ACCOUNT_NUMBER,',
'          ACCOUNT_NAME'))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_row_template=>1
,p_plug_query_show_nulls_as=>' - '
,p_plug_display_condition_type=>'EXISTS'
,p_plug_display_when_condition=>'SELECT ''1'' FROM DUAL WHERE :P99_FROM_DATE IS NOT NULL AND :P99_TO_DATE IS NOT NULL'
,p_pagination_display_position=>'BOTTOM_RIGHT'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(2566124262906386)
,p_name=>'B2B Metrics Summary'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_fixed_header=>'NONE'
,p_show_detail_link=>'N'
,p_show_pivot=>'N'
,p_show_calendar=>'N'
,p_download_formats=>'CSV'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'TD002849'
,p_internal_uid=>2566124262906386
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2566203647906391)
,p_db_column_name=>'PARTY_NUMBER'
,p_display_order=>1
,p_column_identifier=>'A'
,p_column_label=>'PARTY NUMBER'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'PARTY_NUMBER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2566300230906394)
,p_db_column_name=>'PARTY_NAME'
,p_display_order=>2
,p_column_identifier=>'B'
,p_column_label=>'PARTY NAME'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'PARTY_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2566403056906394)
,p_db_column_name=>'SOA_COUNT'
,p_display_order=>8
,p_column_identifier=>'H'
,p_column_label=>'SOA COUNT'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'SOA_COUNT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2566522283906394)
,p_db_column_name=>'ASN_COUNT'
,p_display_order=>9
,p_column_identifier=>'I'
,p_column_label=>'ASN COUNT'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ASN_COUNT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2566630106906395)
,p_db_column_name=>'ACCOUNT_NUMBER'
,p_display_order=>10
,p_column_identifier=>'J'
,p_column_label=>'CUSTOMER NUMBER'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ACCOUNT_NUMBER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2566726038906395)
,p_db_column_name=>'ACCOUNT_NAME'
,p_display_order=>11
,p_column_identifier=>'K'
,p_column_label=>'CUSTOMER NAME'
,p_allow_pivot=>'N'
,p_column_type=>'STRING'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'ACCOUNT_NAME'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2566811629906395)
,p_db_column_name=>'B2B_SO_CNT'
,p_display_order=>12
,p_column_identifier=>'L'
,p_column_label=>'B2B SO COUNT'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'B2B_SO_CNT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2566929513906395)
,p_db_column_name=>'B2B_SO_AMT'
,p_display_order=>13
,p_column_identifier=>'M'
,p_column_label=>'B2B SO SALES$'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'B2B_SO_AMT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2567007856906397)
,p_db_column_name=>'B2B_SOA_AMT'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'SOA SALES$'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'B2B_SOA_AMT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2567124817906397)
,p_db_column_name=>'B2B_ASN_AMT'
,p_display_order=>15
,p_column_identifier=>'O'
,p_column_label=>'ASN SALES$'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'B2B_ASN_AMT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2567232166906397)
,p_db_column_name=>'XML_PO_CNT'
,p_display_order=>16
,p_column_identifier=>'P'
,p_column_label=>'XML PO COUNT'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'XML_PO_CNT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2567316698906397)
,p_db_column_name=>'XML_PO_AMT'
,p_display_order=>17
,p_column_identifier=>'Q'
,p_column_label=>'XML PO SALES$'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'XML_PO_AMT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2567422477906397)
,p_db_column_name=>'XML_INV_CNT'
,p_display_order=>18
,p_column_identifier=>'R'
,p_column_label=>'XML INV COUNT'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'XML_INV_CNT'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(2567530264906397)
,p_db_column_name=>'XML_INV_AMT'
,p_display_order=>19
,p_column_identifier=>'S'
,p_column_label=>'XML INV SALES$'
,p_allow_pivot=>'N'
,p_column_type=>'NUMBER'
,p_heading_alignment=>'LEFT'
,p_tz_dependent=>'N'
,p_static_id=>'XML_INV_AMT'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(2567621710906398)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'25677'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>15
,p_report_columns=>'PARTY_NUMBER:PARTY_NAME:ACCOUNT_NUMBER:ACCOUNT_NAME:B2B_SO_CNT:B2B_SO_AMT:SOA_COUNT:B2B_SOA_AMT:ASN_COUNT:B2B_ASN_AMT:XML_PO_CNT:XML_PO_AMT:XML_INV_CNT:XML_INV_AMT:'
,p_flashback_enabled=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(2567806531906400)
,p_plug_name=>'Find Region'
,p_parent_plug_id=>wwv_flow_api.id(2565713857906372)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(24617707682658552)
,p_plug_display_sequence=>5
,p_include_in_reg_disp_sel_yn=>'N'
,p_plug_display_point=>'BODY'
,p_plug_query_row_template=>1
,p_plug_query_headings_type=>'QUERY_COLUMNS'
,p_plug_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_plug_query_show_nulls_as=>' - '
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2568410581906404)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(2567806531906400)
,p_button_name=>'P99_FIND'
,p_button_static_id=>'P99_FIND'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'FIND'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
,p_grid_new_row=>'Y'
,p_grid_new_column=>'Y'
,p_grid_column=>18
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2568630555906404)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(2567806531906400)
,p_button_name=>'P99_CLEAR'
,p_button_static_id=>'P99_CLEAR'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'CLEAR'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:99:&SESSION.::&DEBUG.:99:P99_FROM_DATE,P99_TO_DATE:,'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>19
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(2568821201906405)
,p_button_sequence=>50
,p_button_plug_id=>wwv_flow_api.id(2567806531906400)
,p_button_name=>'P99_BACK'
,p_button_static_id=>'P99_BACK'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(229841743278055)
,p_button_image_alt=>'< BACK'
,p_button_position=>'BODY'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:40:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
,p_grid_new_row=>'N'
,p_grid_new_column=>'Y'
,p_grid_column=>20
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2568026233906401)
,p_name=>'P99_FROM_DATE'
,p_is_required=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(2567806531906400)
,p_prompt=>'From Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(2568225475906403)
,p_name=>'P99_TO_DATE'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(2567806531906400)
,p_prompt=>'To Date'
,p_display_as=>'NATIVE_DATE_PICKER'
,p_cSize=>30
,p_cMaxlength=>4000
,p_cHeight=>1
,p_label_alignment=>'RIGHT'
,p_field_alignment=>'LEFT-CENTER'
,p_field_template=>wwv_flow_api.id(24621686709658564)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
);
end;
/
prompt --application/deployment/definition
begin
wwv_flow_api.create_install(
 p_id=>wwv_flow_api.id(22777689140130220)
,p_welcome_message=>'This application installer will guide you through the process of creating your database objects and seed data.'
,p_configuration_message=>'You can configure the following attributes of your application.'
,p_build_options_message=>'You can choose to include the following build options.'
,p_validation_message=>'The following validations will be performed to ensure your system is compatible with this application.'
,p_install_message=>'Please confirm that you would like to install this application''s supporting objects.'
,p_install_success_message=>'Your application''s supporting objects have been installed.'
,p_install_failure_message=>'Installation of database objects and seed data has failed.'
,p_upgrade_message=>'The application installer has detected that this application''s supporting objects were previously installed.  This wizard will guide you through the process of upgrading these supporting objects.'
,p_upgrade_confirm_message=>'Please confirm that you would like to install this application''s supporting objects.'
,p_upgrade_success_message=>'Your application''s supporting objects have been installed.'
,p_upgrade_failure_message=>'Installation of database objects and seed data has failed.'
,p_deinstall_success_message=>'Deinstallation complete.'
,p_required_free_kb=>100
,p_required_sys_privs=>'CREATE PROCEDURE:CREATE SEQUENCE:CREATE TABLE:CREATE TRIGGER'
,p_required_names_available=>'TAXEXEMPT_PKG:TAXEXEMP_UPLOAD:TAXEXEMP_UPLOAD_SEQ:BI_TAXEXEMP_UPLOAD'
);
end;
/
prompt --application/deployment/install
begin
wwv_flow_api.create_install_script(
 p_id=>wwv_flow_api.id(22778096468236450)
,p_install_id=>wwv_flow_api.id(22777689140130220)
,p_name=>'install'
,p_sequence=>10
,p_script_type=>'INSTALL'
,p_script_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'CREATE TABLE "WC_APPS"."TAXEXEMP_UPLOAD" ',
'   (    "ID" NUMBER, ',
'    "CUSTOMER_NAME" VARCHAR2(255), ',
'    "SITE_NUM" VARCHAR2(50), ',
'    "STATE" VARCHAR2(30), ',
'    "COUNTRY" VARCHAR2(30), ',
'    "REASON_CODE" VARCHAR2(30), ',
'    "CERTIFICATE_TYPE" VARCHAR2(30), ',
'    "FROM_DATE" DATE, ',
'    "TO_DATE" DATE, ',
'    "TAX_EXEMPTION_TYPE" VARCHAR2(255), ',
'    "UPDATED_BY" VARCHAR2(200), ',
'    "UPDATED_DATE" DATE, ',
'    "ERROR_MESSAGE" VARCHAR2(1000), ',
'    "PROCESSED_FLAG" VARCHAR2(1), ',
'    "CUST_ACCOUNT_ID" NUMBER, ',
'     CONSTRAINT "TAXEXEMP_UPLOAD_PK" PRIMARY KEY ("SITE_NUM", "CUST_ACCOUNT_ID");',
'       ',
'       /',
'       ',
'       CREATE UNIQUE INDEX "WC_APPS"."TAXEXEMP_UPLOAD_PK" ON "WC_APPS"."TAXEXEMP_UPLOAD" ("SITE_NUM", "CUST_ACCOUNT_ID") ;',
'       /',
'       ',
'       ',
'       -- Create sequence ',
'create sequence TAXEXEMP_UPLOAD_SEQ',
'minvalue 1',
'maxvalue 9999999999999999999999999999',
'start with 1',
'increment by 1',
'       cache 20;',
'       /',
'       ',
'       ',
'CREATE OR REPLACE PACKAGE "TAXEXEMPT_PKG" IS',
'',
'  -- -----------------------------------------------------------------------------',
'  -- |----------------------------< taxexemp_pkg >----------------------------|',
'  -- -----------------------------------------------------------------------------',
'  --',
'  -- {Start Of Comments}',
'  --',
'  -- Description:',
'  --   This is the main procedure for the APEX app Upload documents',
'  --',
'  --   VERSION DATE          AUTHOR(S)       DESCRIPTION',
'  -- ------- -----------   --------------- ---------------------------------',
'  -- 1.0     06-SEP-2012     Manny           Created this procedure.',
'',
'',
'',
'',
'  PROCEDURE File_upload(p_app_user VARCHAR2 ,p_status OUT NOCOPY VARCHAR2) ;',
'   PROCEDURE Update_TAX_exempt(p_app_user VARCHAR2) ; ',
'',
'  ',
'END;',
'/',
'CREATE OR REPLACE PACKAGE BODY "TAXEXEMPT_PKG" IS',
'',
'  -- -----------------------------------------------------------------------------',
'  -- |----------------------------< TAXEXEMPT_PKG >--------------------------------|',
'  -- -----------------------------------------------------------------------------',
'  --',
'  --   VERSION DATE          AUTHOR(S)       DESCRIPTION',
'  -- ------- -----------   --------------- ---------------------------------',
'  -- 1.0     07-SEP-2012   Manny         Created this package.',
'',
'',
'',
'--',
'',
'',
'',
'  PROCEDURE File_upload(p_app_user VARCHAR2, p_status OUT NOCOPY VARCHAR2) IS',
'  ',
'    -- -----------------------------------------------------------------------------',
'    -- ',
'    -- All Rights Reserved',
'    -- Description',
'    -- Uploads data from a PIPE delimited spreadsheet.',
'    --',
'    -- Modification History:',
'    --',
'    -- When         Who        Did what',
'    -- -----------  --------   -----------------------------------------------------',
'    -- 07-SEP-2012  Manny      Created this procedure.',
'    ---------------------------------------------------------------------------------',
'',
'    --Initialize Local Variables',
'  ',
'  ',
'    v_blob_data  BLOB;',
'    v_name       VARCHAR2(90);',
'    v_blob_len   NUMBER;',
'    v_position   NUMBER;',
'    v_raw_chunk  RAW(10000);',
'    v_char       CHAR(1);',
'    c_chunk_len  NUMBER := 1;',
'    v_line       VARCHAR2(32767) := NULL;',
'    v_data_array wwv_flow_global.vc_arr2;',
'  ',
'  ',
'  ',
'    --error handling',
'    l_err_program VARCHAR2(75) := ''taxexemp_pkg.file_upload'';',
'    l_err_email   VARCHAR2(200) := ''HDSOracleDevelopers@hdsupply.com'';',
'    l_sec         VARCHAR2(4000);',
'  ',
'  BEGIN',
'    l_sec := ''starting'';',
'  ',
'   -- EXECUTE IMMEDIATE ''truncate table taxexemp_upload'';',
'  ',
'    -- Read data from wwv_flow_files</span>',
'    SELECT blob_content',
'          ,NAME',
'      INTO v_blob_data',
'          ,v_name',
'      FROM wwv_flow_files',
'     WHERE last_updated = (SELECT MAX(last_updated)',
'                             FROM wwv_flow_files',
'                            WHERE updated_by = p_app_user)',
'       AND id = (SELECT MAX(id)',
'                   FROM apex_application_files',
'                  WHERE updated_by = p_app_user);',
'  ',
'    v_blob_len := dbms_lob.getlength(v_blob_data);',
'    v_position := 1;',
'  ',
'    -- Read and convert binary to char</span>',
'    l_sec := ''in while loop'';',
'    WHILE (v_position <= v_blob_len)',
'    LOOP',
'    ',
'      v_raw_chunk := dbms_lob.substr(v_blob_data, c_chunk_len, v_position);',
'      v_char      := chr(hds_wc_apps_misc_pkg.hex_to_decimal(rawtohex(v_raw_chunk)));',
'      v_line      := v_line || v_char;',
'      v_position  := v_position + c_chunk_len;',
'      -- When a whole line is retrieved </span>',
'      IF v_char = chr(10) THEN',
'        -- Convert comma to : to use wwv_flow_utilities </span>',
'        v_line := REPLACE(v_line, ''|'', '':'');',
'        -- Convert each column separated by : into array of data </span>',
'        v_data_array := apex_util.string_to_table(v_line);',
'        -- Insert data into target table </span>',
'        EXECUTE IMMEDIATE ''insert into taxexemp_upload ',
'        (',
'        customer_name,',
'        site_num,',
'        state,',
'        country,',
'        reason_code,',
'        certificate_type,',
'        from_date,',
'        to_date,',
'        cust_account_id,',
'        tax_exemption_type',
'        )',
'                values (:1,:2,:3,:4,:5,:6,:7,:8,:9,regexp_replace(:10, ''''([[:cntrl:]])|(^\t)'''' ))''',
'          USING v_data_array(1), v_data_array(2), v_data_array(3), v_data_array(4), v_data_array(5), v_data_array(6), v_data_array(7), v_data_array(8), v_data_array(9),v_data_array(10);',
'        -- Clear out',
'        v_line := NULL;',
'        COMMIT;',
'      ',
'      END IF;',
'    ',
'    END LOOP;',
'  ',
'    DELETE FROM apex_application_files',
'     WHERE NAME = v_name;',
'',
'      p_status :=''Upload was Sucessful!'';  ',
'  EXCEPTION',
'',
'    WHEN OTHERS THEN',
'      p_status :=''You have duplicates or other issues with this upload'';',
'',
'      hds_wc_apps_misc_pkg.error_api(p_program => l_err_program,',
'                                       p_sec => l_sec||v_data_array(1)||'',''|| v_data_array(2)||'',''||v_data_array(3)||'',''||v_data_array(4)|| '',''||v_data_array(5)||'',''||v_data_array(6)||'',''||v_data_array(7)||'',''||v_data_array(8)||'',''||v_data_array(9)||'
||''',''||v_data_array(10), p_errmsg => SQLERRM,',
'                                       p_email => l_err_email);',
'',
'',
'  END ;',
'',
'',
'',
'  PROCEDURE Update_TAX_exempt(p_app_user VARCHAR2) IS  ',
'  ',
'    -- -----------------------------------------------------------------------------',
'    -- ',
'    -- All Rights Reserved',
'    -- Description',
'    -- Calls package on Oracle to update the cust_all_sites table',
'    --',
'    -- Modification History:',
'    --',
'    -- When         Who        Did what',
'    -- -----------  --------   -----------------------------------------------------',
'    -- 07-SEP-2012  Manny      Created this procedure.',
'    ---------------------------------------------------------------------------------',
'',
'    --Initialize Local Variables',
'    x_errmsg     VARCHAR2(1000);',
'    x_process    VARCHAR2(1);',
'  ',
'    --error handling',
'    l_err_program VARCHAR2(75) := ''taxexemp_pkg.file_upload'';',
'    l_err_email   VARCHAR2(200) := ''HDSOracleDevelopers@hdsupply.com'';',
'    l_sec         VARCHAR2(4000);',
'  ',
'  BEGIN',
'',
'  FOR c_tax IN (SELECT * ',
'                FROM taxexemp_upload ',
'                WHERE processed_flag=''N'') LOOP',
'                ',
'  apps.xxcus_apx_taxexempt@ebizprd_lnk(p_cust_account_id => c_tax.cust_account_id,',
'                              p_sitenum => c_tax.site_num,',
'                              p_state => c_tax.state,',
'                              p_taxtype => c_tax.tax_exemption_type,',
'                              x_errmsg => x_errmsg,',
'                              x_process => x_process);',
'',
'                ',
'  UPDATE taxexemp_upload',
'  SET updated_by =p_app_user , ',
'      updated_date = SYSDATE,',
'      error_message = x_errmsg,',
'      processed_flag = x_process',
'  WHERE id = c_tax.id;',
'  ',
'/*  x_errmsg := NULL;',
'  x_process:= NULL;*/',
'  ',
'  END LOOP;',
'',
'',
'',
'  ',
'  EXCEPTION',
'    WHEN OTHERS THEN',
'      hds_wc_apps_misc_pkg.error_api(p_program => l_err_program,',
'                                       p_sec => l_sec,',
'                                       p_errmsg => SQLERRM,',
'                                       p_email => l_err_email);',
'',
'END;',
'',
'END ;',
'/',
'       ',
'       ',
''))
);
end;
/
prompt --application/deployment/checks
begin
null;
end;
/
prompt --application/deployment/buildoptions
begin
null;
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
