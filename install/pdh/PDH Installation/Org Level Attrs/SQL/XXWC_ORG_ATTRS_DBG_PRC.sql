   /**************************************************************************
    File Name:XXWC_ORG_ATTRS_DBG_PRC
    PROGRAM TYPE: Procedure 
    PURPOSE: Captures debug information on Org attributes page
    HISTORY
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     07-Aug-2012    Santhosh Louis    Initial creation of the procedure
   **************************************************************************/ 
   
CREATE OR REPLACE PROCEDURE APPS.XXWC_ORG_ATTRS_DBG_PRC
(param_name VARCHAR2,param_value VARCHAR2) 
IS
PRAGMA AUTONOMOUS_TRANSACTION;
BEGIN
 INSERT INTO XXWC.XXWC_ORG_ATTRS_DBG VALUES (XXWC_ORG_ATTRS_DBG_S.NEXTVAL,param_name,param_value,CURRENT_TIMESTAMP);
 COMMIT;
END;

/
