create or replace
PACKAGE BODY APPS.XXWC_EGO_ITEM_ORG_ATTRS AS 

   /**************************************************************************
    File Name:XXWC_EGO_ITEM_ORG_ATTRS
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Updates Item org attributes
    HISTORY
    -- Description   : Called from the trigger XXWC Org Attributes page
    -- Dependencies Tables        : custom table XXWC_ORG_ATTRS_DBG
    -- Dependencies Views         : None
    -- Dependencies Sequences     : XXWC_ORG_ATTRS_DBG_S
    -- Dependencies Procedures    : XXWC_ORG_ATTRS_DBG_PKG
    ================================================================
           Last Update Date : 12/12/12
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     23/11/12    Santhosh Louis   Initial creation of the package
						   
   **************************************************************************/
  
PROCEDURE SYNC_ORG_ATTRIBUTES
                              (P_INVENTORY_ITEM_ID       NUMBER
                            ,P_ORGANIZATION_ID          NUMBER
                            ,P_USER_ID                  NUMBER
                            ,P_SEGMENT1                 VARCHAR2                                
                            ,P_SOURCE_TYPE              VARCHAR2
                            ,P_SOURCE_ORG               VARCHAR2    
                            ,P_SRC_RULE_ID              NUMBER                                    
                            ,P_PREPROCESSING_LEAD_TIME  NUMBER   
                            ,P_PRCESSING_LEAD_TIME      NUMBER  
                            ,P_FIXED_LOT_MULTIPLIER     NUMBER   
                            ,P_SALES_VELOCITY_CLASS     VARCHAR2   
                            ,P_PLANNING_METHOD          VARCHAR2  
                            ,P_MINMAX_MINIMUM           NUMBER       
                            ,P_MINMAX_MAXIMUM           NUMBER 
                            ,P_PURCHASE_FLAG            VARCHAR2
                            ,P_RESERVE_STOCK            VARCHAR2
                            ,P_DEFAULT_BUYER            NUMBER
                            ,P_PREV_PURCHASE_FLAG       VARCHAR2
                            ,P_PREV_SV_CLASS            VARCHAR2   
                            ,P_PREV_SRC_RULE_ID         NUMBER                               
                            ,l_WARNINGS          OUT NOCOPY  VARCHAR2                            
                            ,L_RETURN_STATUS     OUT NOCOPY  VARCHAR2
                            ,l_ERROR_MSG         OUT NOCOPY  VARCHAR2
                            )
						
AS
/*--------------
Declarations
----------------*/
l_user_id                   number;
L_RESP_ID                   NUMBER;
L_APPL_ID                   NUMBER;
l_sec_grp_id                number :=0;



   l_item_table          EGO_Item_PUB.Item_Tbl_Type;
   x_item_table          EGO_Item_PUB.Item_Tbl_Type;
   x_Inventory_Item_Id   mtl_system_items_b.inventory_item_id%TYPE;
   x_Organization_Id     mtl_system_items_b.organization_id%TYPE;
   X_RETURN_STATUS       VARCHAR2(1);
   x_msg_count           NUMBER(10);
   X_MSG_DATA            VARCHAR2(1000);
   X_MESSAGE_LIST        ERROR_HANDLER.ERROR_TBL_TYPE;
   X_ERRORCODE         NUMBER;
   
   l_sv_Cat_Set_id 		 NUMBER;
   l_sv_category_id      NUMBER;
   l_sv_prev_cat_id		 NUMBER;
   
   l_pf_Cat_Set_id 		 NUMBER;
   l_pf_category_id 	 NUMBER;
   L_PF_PREV_CAT_ID		 NUMBER;   
    
    L_SOURCE_ORG_CLASS VARCHAR2(100):=NULL;   
    L_WARNING_MSG VARCHAR2(100):= NULL;
    L_SRC_ORG_ID  NUMBER;
    L_ERRO_DESC            VARCHAR2(4000);
    l_sr_error_Desc         VARCHAR2(4000);    
    
   SR_RETURN_STATUS       VARCHAR2(1):='S'; -- in case sourcing rule is not updated still the update to categories should work so hard coded to S
   SR_MSG_COUNT           NUMBER(10);
   SR_MSG_DATA            VARCHAR2(1000);
   
  L_ASSIGNMENT_ID   NUMBER ;
  L_ASSIGNMENT_SET_ID NUMBER;
  L_ASSIGNMENT_TYPE NUMBER;
  L_SOURCING_RULE_TYPE NUMBER;
  
/*--------------
Record type declarations for sourcing rule
----------------*/
l_assignment_set_rec mrp_src_assignment_pub.assignment_set_rec_type;
l_assignment_set_val_rec mrp_src_assignment_pub.assignment_set_val_rec_type;
l_assignment_tbl mrp_src_assignment_pub.assignment_tbl_type;
l_assignment_val_tbl mrp_src_assignment_pub.assignment_val_tbl_type;
o_assignment_set_rec mrp_src_assignment_pub.assignment_set_rec_type;
o_assignment_set_val_rec mrp_src_assignment_pub.assignment_set_val_rec_type;
O_ASSIGNMENT_TBL MRP_SRC_ASSIGNMENT_PUB.ASSIGNMENT_TBL_TYPE;
o_assignment_val_tbl mrp_src_assignment_pub.assignment_val_tbl_type;

BEGIN

L_WARNINGS:=NULL;
-- warnings are checked .. these will not stop the transaction being saved.
-- it will alert the user the warnings on the page
IF P_SOURCE_ORG is not null then 

    BEGIN
      SELECT SEGMENT1 into l_source_org_class
      from MTL_ITEM_CATEGORIES_V
      where INVENTORY_ITEM_ID=P_INVENTORY_ITEM_ID
      AND CATEGORY_SET_NAME='Sales Velocity'
      AND ORGANIZATION_ID=P_SOURCE_ORG;
    EXCEPTION WHEN OTHERS THEN
    l_source_org_class:=Null;
    END;
    
    IF L_SOURCE_ORG_CLASS IS NULL OR L_SOURCE_ORG_CLASS ='N' OR L_SOURCE_ORG_CLASS ='Z' THEN
    L_WARNINGS:=L_WARNINGS||' Source Org does not stock.';
    END IF;
    
END IF;
IF P_SOURCE_TYPE IS NOT NULL AND P_SOURCE_TYPE='1' THEN 
    IF P_SOURCE_ORG IS NULL THEN
      L_WARNINGS:=L_WARNINGS||'Source Org required.';
    END IF;
END IF;
/*
Commented as per Tom , no warnings and error messages on Planning method changes
IF P_RESERVE_STOCK IS NOT NULL AND P_RESERVE_STOCK>0 THEN 
    IF P_PLANNING_METHOD ='2' OR P_PLANNING_METHOD ='6' THEN
        L_WARNINGS:=L_WARNINGS||'Check Planning Method.';
    end if;
END IF;

IF P_PLANNING_METHOD IS NOT NULL AND P_PLANNING_METHOD='6' THEN 
    IF P_SALES_VELOCITY_CLASS ='8' OR P_SALES_VELOCITY_CLASS ='9'  OR P_SALES_VELOCITY_CLASS='B' THEN
        L_WARNINGS:=L_WARNINGS||'Check Planning Method.';
    end if;
end if;
*/


 
 /* this code not required 
 
SELECT 	responsibility_id 
  INTO	l_resp_id
  FROM 	APPS.FND_RESPONSIBILITY_VL
 WHERE 	responsibility_name ='Product Information Management Data Librarian';
 
SELECT 	user_id 
  INTO	l_user_id
  FROM 	APPS.FND_USER
 WHERE 	USER_NAME ='TJACKSON';
 
  SELECT user_id
   ,responsibility_id
   ,responsibility_application_id
   ,security_group_id
 INTO l_user_id
     ,l_resp_id
     ,l_appl_id
     ,l_sec_grp_id
 FROM apps.fnd_user_resp_groups
 WHERE user_id = l_USER_ID
 AND RESPONSIBILITY_ID = L_RESP_ID;
 */
 
 IF P_USER_ID IS NOT NULL THEN 
 L_USER_ID:=P_USER_ID;
 END IF;
 

 
 -- Code will not run into exception till PIM module is installed on EBS
 -- double check adding exception
      BEGIN
         SELECT   responsibility_id, application_id
           INTO   l_resp_id, l_appl_id
           FROM   fnd_responsibility
          WHERE   responsibility_key = 'EGO_PIM_DATA_LIBRARIAN';
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
       
      END;
  

                             XXWC_ORG_ATTRS_DBG_PRC(  'P_INVENTORY_ITEM_ID =>',P_INVENTORY_ITEM_ID   );  
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_ORGANIZATION_ID =>',P_ORGANIZATION_ID        );
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_USER_ID =>',P_USER_ID         );       
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_SEGMENT1 =>',P_SEGMENT1        );                                
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_SOURCE_TYPE =>',P_SOURCE_TYPE     );      
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_SOURCE_ORG =>',P_SOURCE_ORG        );     
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_PREPROCESSING_LEAD_TIME =>',P_PREPROCESSING_LEAD_TIME);
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_PRCESSING_LEAD_TIME =>',P_PRCESSING_LEAD_TIME  );  
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_FIXED_LOT_MULTIPLIER =>',P_FIXED_LOT_MULTIPLIER  ); 
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_SALES_VELOCITY_CLASS =>',P_SALES_VELOCITY_CLASS  ); 
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_PLANNING_METHOD =>',P_PLANNING_METHOD   );     
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_MINMAX_MINIMUM =>',P_MINMAX_MINIMUM   );      
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_MINMAX_MAXIMUM =>',P_MINMAX_MAXIMUM  );       
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_PURCHASE_FLAG =>',P_PURCHASE_FLAG  );        
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_RESERVE_STOCK =>',P_RESERVE_STOCK  );        
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_DEFAULT_BUYER =>',P_DEFAULT_BUYER  );        
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_PREV_PURCHASE_FLAG =>',P_PREV_PURCHASE_FLAG     );
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_PREV_SV_CLASS =>',P_PREV_SV_CLASS    );      
                             XXWC_ORG_ATTRS_DBG_PRC(  'L_WARNINGS =>',L_WARNINGS    );  
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_PREV_SRC_RULE_ID =>',P_PREV_SRC_RULE_ID);
                             XXWC_ORG_ATTRS_DBG_PRC(  'P_SRC_RULE_ID =>',P_SRC_RULE_ID  ); 
                             
      --IF apps.FND_GLOBAl.user_id <> l_user_id THEN //for now set apps_initialize always
        apps.fnd_global.apps_initialize (l_user_id
                                   ,l_resp_id
                                   ,l_appl_id
                                   ,l_sec_grp_id);
      --END IF;             
      
    --Transaction type  Update for Item and the org
   L_ITEM_TABLE(1).TRANSACTION_TYPE            := 'UPDATE'; 
   l_item_table(1).inventory_item_id           := P_INVENTORY_ITEM_ID;
   L_ITEM_TABLE(1).ORGANIZATION_ID             := P_ORGANIZATION_ID;
   
   --set parameters to update 
   L_ITEM_TABLE(1).SOURCE_TYPE                  := P_SOURCE_TYPE;
   L_ITEM_TABLE(1).SOURCE_ORGANIZATION_ID       := P_SOURCE_ORG;
   L_ITEM_TABLE(1).PREPROCESSING_LEAD_TIME      := P_PREPROCESSING_LEAD_TIME;
   L_ITEM_TABLE(1).POSTPROCESSING_LEAD_TIME     := P_PRCESSING_LEAD_TIME    ;
   L_ITEM_TABLE(1).FIXED_LOT_MULTIPLIER         := P_FIXED_LOT_MULTIPLIER  ; 
  -- L_ITEM_TABLE(1). := P_SALES_VELOCITY_CLASS;   
   L_ITEM_TABLE(1).INVENTORY_PLANNING_CODE      := P_PLANNING_METHOD;        
   L_ITEM_TABLE(1).MIN_MINMAX_QUANTITY          := P_MINMAX_MINIMUM  ;       
   L_ITEM_TABLE(1).MAX_MINMAX_QUANTITY          := P_MINMAX_MAXIMUM  ;       
  -- L_ITEM_TABLE(1). := P_PURCHASE_FLAG;          
   L_ITEM_TABLE(1).attribute21 := P_RESERVE_STOCK ;         
   L_ITEM_TABLE(1).BUYER_ID                     := P_DEFAULT_BUYER ;          
  -- L_ITEM_TABLE(1). := P_AMU ;         

   DBMS_OUTPUT.PUT_LINE('=====================================');
   DBMS_OUTPUT.PUT_LINE('Calling EGO_ITEM_PUB.Process_Items API');

   EGO_ITEM_PUB.Process_Items(
         p_api_version           => 1.0
         ,p_init_msg_list         => FND_API.g_TRUE
         ,P_COMMIT                => FND_API.G_TRUE
         ,p_Item_Tbl              => l_item_table 
         ,x_Item_Tbl              => x_item_table
         ,x_return_status         => x_return_status
         ,x_msg_count             => x_msg_count);

   DBMS_OUTPUT.PUT_LINE('==================================');
   DBMS_OUTPUT.PUT_LINE('Return Status ==>'||X_RETURN_STATUS);
                             XXWC_ORG_ATTRS_DBG_PRC( 'End of Item API Call','' );       
   IF (X_RETURN_STATUS = FND_API.G_RET_STS_SUCCESS) THEN
   NULL;
    /* -- Commenting code since the call will always reference single item
      FOR i IN 1..x_item_table.COUNT LOOP
         DBMS_OUTPUT.PUT_LINE('Inventory Item Id :'||to_char(x_item_table(i).Inventory_Item_Id));
         DBMS_OUTPUT.PUT_LINE('Organization Id   :'||to_char(x_item_table(i).Organization_Id));
      END LOOP;
      */
   ELSE
      DBMS_OUTPUT.PUT_LINE('Error Messages :');
      Error_Handler.GET_MESSAGE_LIST(x_message_list=>x_message_list);
      FOR I IN 1..X_MESSAGE_LIST.COUNT LOOP
          l_erro_Desc:= l_erro_Desc|| x_message_list(i).message_text;
         DBMS_OUTPUT.PUT_LINE(x_message_list(i).message_text);
      END LOOP;
   END IF;
   commit;
   DBMS_OUTPUT.PUT_LINE('==================================');
   L_RETURN_STATUS  := X_RETURN_STATUS;
   L_ERROR_MSG      := L_ERRO_DESC;
   XXWC_ORG_ATTRS_DBG_PRC( L_RETURN_STATUS,L_ERROR_MSG );            
   
 /*------------------------------------------------
 If item attribute changes have completed successfully go to update sourcing rule and other categories   
 ------------------------------------------------ */
   IF (X_RETURN_STATUS='S') THEN
    --Check to see if the sourcing rule has been updated.
    --only if updated run the code to update sourcing rule
    IF P_SRC_RULE_ID IS NOT NULL THEN

        
          IF  P_SRC_RULE_ID <> nvl(P_PREV_SRC_RULE_ID,-1) THEN

              IF P_PREV_SRC_RULE_ID IS NULL THEN 
                    L_ASSIGNMENT_ID:=NULL;
              ELSE
               BEGIN
                    SELECT ASSIGNMENT_ID,ASSIGNMENT_SET_ID,ASSIGNMENT_TYPE,SOURCING_RULE_TYPE
                    INTO l_assignment_id,l_ASSIGNMENT_SET_ID,l_ASSIGNMENT_TYPE,l_SOURCING_RULE_TYPE
                    FROM MRP_SR_ASSIGNMENTS 
                    WHERE INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID
                    AND   ORGANIZATION_ID   =P_ORGANIZATION_ID
                    AND   SOURCING_RULE_ID  =P_PREV_SRC_RULE_ID;
                EXCEPTION WHEN OTHERS THEN
                    L_ASSIGNMENT_ID:=NULL;
                    L_ASSIGNMENT_SET_ID:=NULL;
                    L_ASSIGNMENT_TYPE:=NULL;
                    L_SOURCING_RULE_TYPE:=NULL;
                    SR_RETURN_STATUS :='E';
                    L_SR_ERROR_DESC := 'Error While updating Sourcing Rule';
                     XXWC_ORG_ATTRS_DBG_PRC( 'Inside sourcing rule exception','' );    
                END;
              END IF;  
 /*------------------------------------------------
  Sourcing rule update and create are allowed 
  Deletion of sourcing rule not allowed
  Sourcing rule update type hard code to item org
 ------------------------------------------------ */              
              IF L_ASSIGNMENT_ID IS NOT NULL THEN 
               XXWC_ORG_ATTRS_DBG_PRC( 'inside  IF L_ASSIGNMENT_ID IS NOT NULL ','' ); 
                  L_ASSIGNMENT_TBL (1).ASSIGNMENT_ID := L_ASSIGNMENT_ID;    
                  L_ASSIGNMENT_TBL (1).OPERATION := 'UPDATE';
              ELSE
                  L_ASSIGNMENT_TBL (1).OPERATION := 'CREATE';
              END IF;
              
                l_assignment_tbl (1).assignment_set_id := 1;
                L_ASSIGNMENT_TBL (1).ASSIGNMENT_TYPE := 6;
                l_assignment_tbl (1).organization_id := P_ORGANIZATION_ID;
                L_ASSIGNMENT_TBL (1).INVENTORY_ITEM_ID := P_INVENTORY_ITEM_ID;
                l_assignment_tbl (1).sourcing_rule_id := P_SRC_RULE_ID;
                L_ASSIGNMENT_TBL (1).SOURCING_RULE_TYPE := 1;
                
                MRP_SRC_ASSIGNMENT_PUB.PROCESS_ASSIGNMENT
                (   P_API_VERSION_NUMBER      => 1.0,
                    p_init_msg_list           => fnd_api.g_false,
                    P_RETURN_VALUES           => FND_API.G_FALSE,
                    P_COMMIT                  => FND_API.G_FALSE,
                    X_RETURN_STATUS           => SR_RETURN_STATUS,
                    X_MSG_COUNT               => SR_MSG_COUNT,
                    X_MSG_DATA                => SR_MSG_DATA,
                    p_assignment_set_rec      => l_assignment_set_rec,
                    P_ASSIGNMENT_SET_VAL_REC  => L_ASSIGNMENT_SET_VAL_REC,
                    p_assignment_tbl          => l_assignment_tbl,
                    P_ASSIGNMENT_VAL_TBL      => L_ASSIGNMENT_VAL_TBL,
                    x_assignment_set_rec      => o_assignment_set_rec,
                    X_ASSIGNMENT_SET_VAL_REC  => O_ASSIGNMENT_SET_VAL_REC,
                    x_assignment_tbl          => o_assignment_tbl,
                    x_assignment_val_tbl      => o_assignment_val_tbl
                );
                
                    IF SR_RETURN_STATUS='S' THEN
                      L_RETURN_STATUS  := SR_RETURN_STATUS;
                      XXWC_ORG_ATTRS_DBG_PRC( 'Sourcing Rule update completed succesfully','' );    
                    ELSE
                         L_RETURN_STATUS  := SR_RETURN_STATUS;
                            IF SR_MSG_COUNT > 0 THEN
                                XXWC_ORG_ATTRS_DBG_PRC( 'Sourcing Rule completed with error','' );                      
                                FOR l_index IN 1 .. SR_MSG_COUNT
                                LOOP
                                L_SR_ERROR_DESC := L_SR_ERROR_DESC || FND_MSG_PUB.GET (P_MSG_INDEX   => L_INDEX,
                                                                                      P_ENCODED     => FND_API.G_FALSE);                                 
                                END LOOP;
                                 L_ERROR_MSG      := L_SR_ERROR_DESC;   
                            END IF;   
                    END IF;
                    
              END IF;
              
          END IF;
          

       

  XXWC_ORG_ATTRS_DBG_PRC( SR_RETURN_STATUS,L_SR_ERROR_DESC );         
   
 /*------------------------------------------------
    If Sourcing rule updated successfull proceed to category assignment
 ------------------------------------------------ */   
    IF SR_RETURN_STATUS='S' THEN
    
          SELECT CATEGORY_SET_ID 
            INTO L_SV_CAT_SET_ID
            FROM  MTL_CATEGORY_SETS_TL MCST
           WHERE MCST.CATEGORY_SET_NAME='Sales Velocity';
          
          SELECT CATEGORY_SET_ID 
            INTO L_PF_CAT_SET_ID
            FROM MTL_CATEGORY_SETS_TL MCST
           WHERE MCST.CATEGORY_SET_NAME='Purchase Flag'; 
        
 /*------------------------------------------------
   Update categories accordingly..if made null ddelete them
   -- if changed from null to a value create
   -- if changed from one to another value update
 ------------------------------------------------ */           
        IF P_PREV_PURCHASE_FLAG is not NULL THEN 
          
            SELECT 	MCb.category_id into l_pf_prev_cat_id
            FROM 	MTL_CATEGORY_SETS_B mcsb,mtl_categories_B mcb
             WHERE 	MCSB.CATEGORY_SET_ID=l_pf_cat_Set_id
             AND 	MCB.STRUCTURE_ID=MCSB.STRUCTURE_ID
             AND 	mcb.segment1=P_PREV_PURCHASE_FLAG;
            
            IF P_PURCHASE_FLAG IS NOT NULL THEN
            
                SELECT 	MCb.category_id into l_pf_category_id
                FROM 	MTL_CATEGORY_SETS_B mcsb,mtl_categories_B mcb
                 WHERE 	MCSB.CATEGORY_SET_ID=l_pf_cat_Set_id
                 AND 	MCB.STRUCTURE_ID=MCSB.STRUCTURE_ID
                 AND 	mcb.segment1=P_PURCHASE_FLAG;
                 
                IF (P_PREV_PURCHASE_FLAG!=P_PURCHASE_FLAG) THEN
                             XXWC_ORG_ATTRS_DBG_PRC( 'update Purchase Flag category ','' );  
                -- update category_assognment , if there was already a category assigned for that sales velocity then update
                 INV_ITEM_CATEGORY_PUB.UPDATE_CATEGORY_ASSIGNMENT
                      (  P_API_VERSION              => 1.0,  
                       P_INIT_MSG_LIST            => FND_API.G_FALSE,  
                       P_COMMIT                   => FND_API.G_FALSE,
                       P_CATEGORY_ID              => l_pf_category_id,
                       P_OLD_CATEGORY_ID          => l_pf_prev_cat_id,  
                       P_CATEGORY_SET_ID          => l_pf_cat_Set_id,  
                       P_INVENTORY_ITEM_ID        => P_INVENTORY_ITEM_ID	,  
                       P_ORGANIZATION_ID          => P_ORGANIZATION_ID,
                       X_RETURN_STATUS            => X_RETURN_STATUS,  
                       X_ERRORCODE                => X_ERRORCODE,  
                       X_MSG_COUNT                => X_MSG_COUNT,  
                       X_MSG_DATA                 => X_MSG_DATA);
                       
                    IF X_RETURN_STATUS='S' THEN
                       L_ERRO_DESC:= L_ERRO_DESC|| ' Purchase Flag Category Update successful.';
                    ELSE
                      IF X_MSG_COUNT>0 THEN
                        FOR I in 1 .. X_MSG_COUNT LOOP
                        L_ERRO_DESC:= L_ERRO_DESC|| FND_MSG_PUB.GET (P_MSG_INDEX=> I,P_ENCODED=>FND_API.G_FALSE); 
                        END LOOP;
                      END IF;
                    END IF;
                
                END IF;
            ELSE
            XXWC_ORG_ATTRS_DBG_PRC( 'DELETE Purchase Flag category ','' );  
                INV_ITEM_CATEGORY_PUB.DELETE_CATEGORY_ASSIGNMENT
                      (  p_api_version        => 1.0, 
                         p_init_msg_list      => FND_API.G_TRUE, 
                         p_commit             => FND_API.G_FALSE, 
                         x_return_status      => X_RETURN_STATUS, 
                         x_errorcode          => X_ERRORCODE, 
                         x_msg_count          => X_MSG_COUNT, 
                         x_msg_data           => X_MSG_DATA, 
                         p_category_id        => l_pf_prev_cat_id, 
                         p_category_set_id    => l_pf_cat_Set_id, 
                         p_inventory_item_id  => P_INVENTORY_ITEM_ID, 
                         P_ORGANIZATION_ID    => P_ORGANIZATION_ID);
                         
                    IF X_RETURN_STATUS='S' THEN
                       L_ERRO_DESC:= L_ERRO_DESC|| ' Purchase Flag Category Delete successful.';
                    ELSE
                      IF X_MSG_COUNT>0 THEN
                        FOR I IN 1 .. X_MSG_COUNT LOOP
                        L_ERRO_DESC:= L_ERRO_DESC|| FND_MSG_PUB.GET (P_MSG_INDEX=> I,P_ENCODED=>FND_API.G_FALSE); 
                        END LOOP;
                      END IF;
                    END IF;

            END IF;
            
              
          ELSE
            -- code for create category assignment
            -- no previous sales velocity category assigned to item for this org so update
              IF P_PURCHASE_FLAG is not null then 
                    SELECT 	MCb.category_id into l_pf_category_id
                    FROM 	MTL_CATEGORY_SETS_B mcsb,mtl_categories_B mcb
                     WHERE 	MCSB.CATEGORY_SET_ID=l_pf_cat_Set_id
                     AND 	MCB.STRUCTURE_ID=MCSB.STRUCTURE_ID
                     AND 	MCB.SEGMENT1=P_PURCHASE_FLAG;
                 XXWC_ORG_ATTRS_DBG_PRC( 'CREATE Purchase Flag category ','' );  
                  INV_ITEM_CATEGORY_PUB.CREATE_CATEGORY_ASSIGNMENT
                      (  p_api_version        => 1.0, 
                         p_init_msg_list      => FND_API.G_TRUE, 
                         p_commit             => FND_API.G_FALSE, 
                         x_return_status      => X_RETURN_STATUS, 
                         x_errorcode          => X_ERRORCODE, 
                         x_msg_count          => X_MSG_COUNT, 
                         x_msg_data           => X_MSG_DATA, 
                         p_category_id        => l_pf_category_id, 
                         p_category_set_id    => l_pf_cat_Set_id, 
                         p_inventory_item_id  => P_INVENTORY_ITEM_ID, 
                         P_ORGANIZATION_ID    => P_ORGANIZATION_ID);
      
                    IF X_RETURN_STATUS='S' THEN
                       L_ERRO_DESC:= L_ERRO_DESC|| ' Purchase Flag Category Create successful.';
                    ELSE
                      IF X_MSG_COUNT>0 THEN
                        FOR I in 1 .. X_MSG_COUNT LOOP
                        L_ERRO_DESC:= L_ERRO_DESC|| FND_MSG_PUB.GET (P_MSG_INDEX=> I,P_ENCODED=>FND_API.G_FALSE); 
                        END LOOP;
                      END IF;
                    END IF;
                    
                    
                end if;
                
      END IF;
      
         IF P_PREV_SV_CLASS is not NULL THEN 
          
            SELECT 	MCb.category_id into l_sv_prev_cat_id
            FROM 	MTL_CATEGORY_SETS_B mcsb,mtl_categories_B mcb
             WHERE 	MCSB.CATEGORY_SET_ID=l_sv_cat_Set_id
             AND 	MCB.STRUCTURE_ID=MCSB.STRUCTURE_ID
             AND 	mcb.segment1=P_PREV_SV_CLASS;
            
            IF P_SALES_VELOCITY_CLASS IS NOT NULL THEN
            
                SELECT 	MCb.category_id into l_sv_category_id
                FROM 	MTL_CATEGORY_SETS_B mcsb,mtl_categories_B mcb
                 WHERE 	MCSB.CATEGORY_SET_ID=l_sv_cat_Set_id
                 AND 	MCB.STRUCTURE_ID=MCSB.STRUCTURE_ID
                 AND 	mcb.segment1=P_SALES_VELOCITY_CLASS;
                 
                IF (P_PREV_SV_CLASS!=P_SALES_VELOCITY_CLASS) THEN
                   XXWC_ORG_ATTRS_DBG_PRC( 'update sales velocity category ','' );  
                -- update category_assognment , if there was already a category assigned for that sales velocity then update
                 INV_ITEM_CATEGORY_PUB.UPDATE_CATEGORY_ASSIGNMENT
                      (  P_API_VERSION              => 1.0,  
                       P_INIT_MSG_LIST            => FND_API.G_FALSE,  
                       P_COMMIT                   => FND_API.G_FALSE,
                       P_CATEGORY_ID              => l_sv_category_id,
                       P_OLD_CATEGORY_ID          => l_sv_prev_cat_id,  
                       P_CATEGORY_SET_ID          => l_sv_cat_Set_id,  
                       P_INVENTORY_ITEM_ID        => P_INVENTORY_ITEM_ID	,  
                       P_ORGANIZATION_ID          => P_ORGANIZATION_ID,
                       X_RETURN_STATUS            => X_RETURN_STATUS,  
                       X_ERRORCODE                => X_ERRORCODE,  
                       X_MSG_COUNT                => X_MSG_COUNT,  
                       X_MSG_DATA                 => X_MSG_DATA);
                
                    IF X_RETURN_STATUS='S' THEN
                       L_ERRO_DESC:= L_ERRO_DESC|| ' Classification Category Update successful.';
                    ELSE
                      IF X_MSG_COUNT>0 THEN
                        FOR I in 1 .. X_MSG_COUNT LOOP
                        L_ERRO_DESC:= L_ERRO_DESC|| FND_MSG_PUB.GET (P_MSG_INDEX=> I,P_ENCODED=>FND_API.G_FALSE); 
                        END LOOP;
                      END IF;
                    END IF;

                END IF;
            ELSE
                       XXWC_ORG_ATTRS_DBG_PRC( 'DELETE sales velocity category ','' );  
                INV_ITEM_CATEGORY_PUB.DELETE_CATEGORY_ASSIGNMENT
                      (  p_api_version        => 1.0, 
                         p_init_msg_list      => FND_API.G_TRUE, 
                         p_commit             => FND_API.G_FALSE, 
                         x_return_status      => X_RETURN_STATUS, 
                         x_errorcode          => X_ERRORCODE, 
                         x_msg_count          => X_MSG_COUNT, 
                         x_msg_data           => X_MSG_DATA, 
                         p_category_id        => l_sv_prev_cat_id, 
                         p_category_set_id    => l_sv_cat_Set_id, 
                         p_inventory_item_id  => P_INVENTORY_ITEM_ID, 
                         P_ORGANIZATION_ID    => P_ORGANIZATION_ID);
                         
                    IF X_RETURN_STATUS='S' THEN
                       L_ERRO_DESC:= L_ERRO_DESC|| ' Classification Category Delete successful.';
                    ELSE
                      IF X_MSG_COUNT>0 THEN
                        FOR I in 1 .. X_MSG_COUNT LOOP
                        L_ERRO_DESC:= L_ERRO_DESC|| FND_MSG_PUB.GET (P_MSG_INDEX=> I,P_ENCODED=>FND_API.G_FALSE); 
                        END LOOP;
                      END IF;
                    END IF;
                    
            END IF;
            

            
            
          ELSE
            -- code for create category assignment
            -- no previous sales velocity category assigned to item for this org so update
                  IF P_SALES_VELOCITY_CLASS is not null then 
                      SELECT 	MCb.category_id into l_sv_category_id
                      FROM 	MTL_CATEGORY_SETS_B mcsb,mtl_categories_B mcb
                       WHERE 	MCSB.CATEGORY_SET_ID=l_sv_cat_Set_id
                       AND 	MCB.STRUCTURE_ID=MCSB.STRUCTURE_ID
                       AND 	MCB.SEGMENT1=P_SALES_VELOCITY_CLASS;
                                 XXWC_ORG_ATTRS_DBG_PRC( 'CREATE sales velocity category ','' );  
                      INV_ITEM_CATEGORY_PUB.CREATE_CATEGORY_ASSIGNMENT
                      (  p_api_version        => 1.0, 
                         p_init_msg_list      => FND_API.G_TRUE, 
                         p_commit             => FND_API.G_FALSE, 
                         x_return_status      => X_RETURN_STATUS, 
                         x_errorcode          => X_ERRORCODE, 
                         x_msg_count          => X_MSG_COUNT, 
                         x_msg_data           => X_MSG_DATA, 
                         p_category_id        => l_sv_category_id, 
                         p_category_set_id    => l_sv_cat_Set_id, 
                         p_inventory_item_id  => P_INVENTORY_ITEM_ID, 
                         P_ORGANIZATION_ID    => P_ORGANIZATION_ID);
                         
                    IF X_RETURN_STATUS='S' THEN
                       L_ERRO_DESC:= L_ERRO_DESC|| 'Classification Category Create successful.';
                    ELSE
                      IF X_MSG_COUNT>0 THEN
                        FOR I in 1 .. X_MSG_COUNT LOOP
                        L_ERRO_DESC:= L_ERRO_DESC|| FND_MSG_PUB.GET (P_MSG_INDEX=> I,P_ENCODED=>FND_API.G_FALSE); 
                        END LOOP;
                      END IF;
                    END IF;
                    
                  END IF;
          END IF;
          XXWC_ORG_ATTRS_DBG_PRC('Status= >'||X_RETURN_STATUS ,'L_ERRO_DESC =>'||L_ERRO_DESC);
 /*------------------------------------------------
    Set the out params based on the info captured
 ------------------------------------------------ */   
          L_RETURN_STATUS := X_RETURN_STATUS;
          L_ERROR_MSG := L_ERRO_DESC;
          
          
      END IF; -- end for sr_return_Status='S'
    END IF;  --end of if x_retun_Status='S'
   COMMIT;


END SYNC_ORG_ATTRIBUTES;


END XXWC_EGO_ITEM_ORG_ATTRS;