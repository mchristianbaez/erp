create or replace
PACKAGE APPS.XXWC_EGO_ITEM_ORG_ATTRS AS 
   /**************************************************************************
    File Name:XXWC_EGO_ITEM_ORG_ATTRS
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Updates Item org attributes
    HISTORY
    -- Description   : Called from the trigger XXWC Org Attributes page
    -- Dependencies Tables        : custom table XXWC_ORG_ATTRS_DBG
    -- Dependencies Views         : None
    -- Dependencies Sequences     : XXWC_ORG_ATTRS_DBG_S
    -- Dependencies Procedures    : XXWC_ORG_ATTRS_DBG_PKG
    ================================================================
           Last Update Date : 12/12/12
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     23/11/12    Santhosh Louis   Initial creation of the package
						   
   **************************************************************************/
  /* TODO enter package declarations (types, exceptions, methods etc) here */ 
  
PROCEDURE SYNC_ORG_ATTRIBUTES(P_INVENTORY_ITEM_ID       NUMBER
                            ,P_ORGANIZATION_ID          NUMBER
                            ,P_USER_ID                  NUMBER
                            ,P_SEGMENT1                 VARCHAR2                                
                            ,P_SOURCE_TYPE              VARCHAR2
                            ,P_SOURCE_ORG               VARCHAR2       
                            ,P_SRC_RULE_ID              NUMBER                            
                            ,P_PREPROCESSING_LEAD_TIME  NUMBER   
                            ,P_PRCESSING_LEAD_TIME      NUMBER  
                            ,P_FIXED_LOT_MULTIPLIER     NUMBER   
                            ,P_SALES_VELOCITY_CLASS     VARCHAR2   
                            ,P_PLANNING_METHOD          VARCHAR2  
                            ,P_MINMAX_MINIMUM           NUMBER       
                            ,P_MINMAX_MAXIMUM           NUMBER 
                            ,P_PURCHASE_FLAG            VARCHAR2
                            ,P_RESERVE_STOCK            VARCHAR2
                            ,P_DEFAULT_BUYER            NUMBER
                            ,P_PREV_PURCHASE_FLAG       VARCHAR2
                            ,P_PREV_SV_CLASS            VARCHAR2  
                            ,P_PREV_SRC_RULE_ID         NUMBER                               
                            ,l_WARNINGS          OUT NOCOPY  VARCHAR2                              
                            ,L_RETURN_STATUS     OUT NOCOPY  VARCHAR2
                            ,l_ERROR_MSG         OUT NOCOPY  VARCHAR2
                            );

      

END XXWC_EGO_ITEM_ORG_ATTRS;

/