CREATE OR REPLACE PACKAGE APPS."XXWC_EGO_WC_METADATA_PKG"
IS
   /********************************************************************************
   File Name: XXWC_EGO_WC_METADATA_PKG
   PROGRAM TYPE: PL/SQL Package spec and body
   PURPOSE: Procedures and functions for creating the loads to WC Metadata
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the package
    ********************************************************************************/
   /********************************************************************************
   PROGRAM TYPE: FUNCTION
   NAME: AG_NAME_FUNC
   PURPOSE: Function to provide abbreviated attribute group internal name
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/    
   FUNCTION ag_name_func (p_ag_name varchar2)
      RETURN varchar2;
   /********************************************************************************
   PROGRAM TYPE: FUNCTION
   NAME: VS_NAME_FUNC
   PURPOSE: Function to provide abbreviated value set internal name
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/       
   FUNCTION vs_name_func (p_vs_name varchar2)
      RETURN varchar2;

   /********************************************************************************
   PROGRAM TYPE: FUNCTION
   NAME: ATTR_NAME_FUNC
   PURPOSE: Function to provide abbreviated attribute internal name
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/
   FUNCTION attr_name_func (p_attr_name varchar2)
      RETURN varchar2;

   /********************************************************************************
   PROGRAM TYPE: PROCEDURE
   NAME: CREATE_METADATA
   PURPOSE: Procedure to create metadata
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
    ********************************************************************************/      

   PROCEDURE create_metadata (errbuf OUT VARCHAR2, retcode OUT NUMBER);
   
   /********************************************************************************
   PROGRAM TYPE: PROCEDURE
   NAME: CONVERSION
   PURPOSE: Procedure to convert the item attributes
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
    ********************************************************************************/    
   PROCEDURE conversion (errbuf OUT VARCHAR2, retcode OUT NUMBER);
END XXWC_EGO_WC_METADATA_PKG;
/
sho err
