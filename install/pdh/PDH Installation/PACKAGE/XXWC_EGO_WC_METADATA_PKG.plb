/* Formatted on 11-1-2012 11:50:33 AM (QP5 v5.114.809.3010) */
set serveroutput on;

CREATE OR REPLACE PACKAGE BODY APPS."XXWC_EGO_WC_METADATA_PKG"
AS
   /**************************************************************************
    File Name:XXWC_EGO_WC_METADATA_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Upload Metadata
    HISTORY
    -- Description   : Called for creating teh metadata in PIM
    --
    --    Arguments
    --      errbuf  - error message
    --      retcode - error code
    --
    -- Dependencies Tables        : custom table XXWC.XXWC_EGO_MANAGED_ATTR_DEF
    --                              custom table XXWC.XXWC_EGO_ICCSTRUCTURE
    -- Dependencies Views         : None
    -- Dependencies Sequences     : None
    -- Dependencies Procedures    : None
    -- Dependencies Functions     : None
    -- Dependencies Packages      : FND_PROFILE
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ================================================================
           Last Update Date : 06/06/2010
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     09/23/2012    Rajiv Rathod    Initial creation of the package
   **************************************************************************/
   /********************************************************************************
   PROGRAM TYPE: FUNCTION
   NAME: AG_NAME_FUNC
   PURPOSE: Function to provide abbreviated attribute group internal name
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/       
   FUNCTION ag_name_func (p_ag_name varchar2)
      RETURN varchar2
   IS
      l_ag_name      varchar2 (100);
      l_location     number := 0;
      l_new_string   varchar2 (100);
      l_return       varchar2 (100) := 'XXWC_';
   BEGIN
      l_ag_name := UPPER (p_ag_name);
      l_new_string := l_ag_name;
      DBMS_OUTPUT.put_line ('Hello');

      IF (LENGTH ('XXWC_' || p_ag_name || '_AG') > 30)
      THEN
         WHILE (l_location != -1)
         LOOP
            DBMS_OUTPUT.put_line (
               l_new_string || '-' || l_location || '-' || l_return
            );
            l_return := l_return || SUBSTR (l_new_string, 1, 1) || '_';

            SELECT   INSTR (l_new_string, ' ') + 1 INTO l_location FROM DUAL;

            SELECT   SUBSTR (l_new_string, l_location)
              INTO   l_new_string
              FROM   DUAL;

            IF (l_location = 1)
            THEN
               l_location := -1;
            END IF;
         END LOOP;

         l_return :=
            REPLACE (
               REPLACE (
                  REPLACE (
                     REPLACE (
                        REPLACE (REPLACE (l_return, ' ', '_'), '/', '_'),
                        '&',
                        '_'
                     ),
                     '-',
                     '_'
                  ),
                  ',',
                  '_'
               ),
               '''',
               ''
            )
            || 'AG';
         RETURN l_return;
      ELSE
         RETURN UPPER('XXWC_'
                      || REPLACE (
                            REPLACE (
                               REPLACE (
                                  REPLACE (
                                     REPLACE (REPLACE (p_ag_name, ' ', '_'),
                                              '/',
                                              '_'),
                                     '&',
                                     '_'
                                  ),
                                  '-',
                                  '_'
                               ),
                               ',',
                               '_'
                            ),
                            '''',
                            ''
                         )
                      || '_AG');
      END IF;

      RETURN 'XXWC_'
             || REPLACE (
                   REPLACE (
                      REPLACE (
                         REPLACE (
                            REPLACE (REPLACE (l_return, ' ', '_'), '/', '_'),
                            '&',
                            '_'
                         ),
                         '-',
                         '_'
                      ),
                      ',',
                      '_'
                   ),
                   '''',
                   ''
                )
             || '_AG';
   END ag_name_func;
   /********************************************************************************
   PROGRAM TYPE: FUNCTION
   NAME: VS_NAME_FUNC
   PURPOSE: Function to provide abbreviated value set internal name
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/    

   FUNCTION vs_name_func (p_vs_name varchar2)
      RETURN varchar2
   IS
      l_vs_name      varchar2 (100);
      l_location     number := 0;
      l_new_string   varchar2 (100);
      l_return       varchar2 (100) := 'XXWC_';
   BEGIN
      l_vs_name := p_vs_name;
      l_new_string := l_vs_name;
      DBMS_OUTPUT.put_line ('Hello');

      IF (LENGTH ('XXWC_' || p_vs_name || '_VS') > 40)
      THEN
         WHILE (l_location != -1)
         LOOP
            DBMS_OUTPUT.put_line (
               l_new_string || '-' || l_location || '-' || l_return
            );
            l_return := l_return || SUBSTR (l_new_string, 1, 1) || '_';

            SELECT   INSTR (l_new_string, ' ') + 1 INTO l_location FROM DUAL;

            SELECT   SUBSTR (l_new_string, l_location)
              INTO   l_new_string
              FROM   DUAL;

            IF (l_location = 1)
            THEN
               l_location := -1;
            END IF;
         END LOOP;

         l_return := REPLACE (l_return, ' ', '_') || 'VS';
         RETURN l_return;
      ELSE
         RETURN UPPER ('XXWC_' || REPLACE (p_vs_name, ' ', '_') || '_VS');
      END IF;

      RETURN 'XXWC_' || REPLACE (l_return, ' ', '_') || '_VS';
   END vs_name_func;
   
   /********************************************************************************
   PROGRAM TYPE: FUNCTION
   NAME: ATTR_NAME_FUNC
   PURPOSE: Function to provide abbreviated attribute internal name
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the function
    ********************************************************************************/   

   FUNCTION attr_name_func (p_attr_name varchar2)
      RETURN varchar2
   IS
      l_attr_name    varchar2 (100);
      l_location     number := 0;
      l_new_string   varchar2 (100);
      l_return       varchar2 (100) := 'XXWC_';
   BEGIN
      l_attr_name := UPPER (p_attr_name);
      l_new_string := l_attr_name;
      DBMS_OUTPUT.put_line ('Hello');

      IF (LENGTH ('XXWC_' || p_attr_name || '_ATTR') > 30)
      THEN
         WHILE (l_location != -1)
         LOOP
            DBMS_OUTPUT.put_line (
               l_new_string || '-' || l_location || '-' || l_return
            );
            l_return := l_return || SUBSTR (l_new_string, 1, 1) || '_';

            SELECT   INSTR (l_new_string, ' ') + 1 INTO l_location FROM DUAL;

            SELECT   SUBSTR (l_new_string, l_location)
              INTO   l_new_string
              FROM   DUAL;

            IF (l_location = 1)
            THEN
               l_location := -1;
            END IF;
         END LOOP;

         l_return :=
            REPLACE (
               REPLACE (
                  REPLACE (
                     REPLACE (
                        REPLACE (REPLACE (l_return, ' ', '_'), '/', '_'),
                        '&',
                        '_'
                     ),
                     '-',
                     '_'
                  ),
                  ',',
                  '_'
               ),
               '''',
               ''
            )
            || 'ATTR';
         RETURN l_return;
      ELSE
         RETURN UPPER('XXWC_'
                      || REPLACE (
                            REPLACE (
                               REPLACE (
                                  REPLACE (
                                     REPLACE (
                                        REPLACE (p_attr_name, ' ', '_'),
                                        '/',
                                        '_'
                                     ),
                                     '&',
                                     '_'
                                  ),
                                  '-',
                                  '_'
                               ),
                               ',',
                               '_'
                            ),
                            '''',
                            ''
                         )
                      || '_ATTR');
      END IF;

      RETURN 'XXWC_'
             || REPLACE (
                   REPLACE (
                      REPLACE (
                         REPLACE (
                            REPLACE (REPLACE (l_return, ' ', '_'), '/', '_'),
                            '&',
                            '_'
                         ),
                         '-',
                         '_'
                      ),
                      ',',
                      '_'
                   ),
                   '''',
                   ''
                )
             || '_ATTR';
   END attr_name_func;
   
   /********************************************************************************
   PROGRAM TYPE: PROCEDURE
   NAME: CREATE_METADATA
   PURPOSE: Procedure to create metadata
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
    ********************************************************************************/     

   PROCEDURE create_metadata (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   AS
      --declaring variables
      l_user_id         NUMBER;
      l_attr_seq        number := 0;
      l_dc_number_seq   number := 0;
      l_dc_char_seq     number := 0;
      l_dc_long_seq     number := 0;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Beginning of Program');

      --apps initialize
      --block to get the user
      BEGIN
         SELECT   user_id
           INTO   l_user_id
           FROM   fnd_user
          WHERE   user_name = 'XXWC_INT_SUPPLYCHAIN';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'Unable to find user XXWC_INT_SUPPLYCHAIN ' || SQLERRM);
      END;

      fnd_global.apps_initialize (user_id        => l_user_id,
                                  resp_id        => 50296,
                                  resp_appl_id   => 431);

      -- Function to get the correct name for AG internal name

      UPDATE   XXWC.XXWC_EGO_ICCSTRUCTURE
         SET   PARENT_NAME = 'WHITE CAP MST'
       WHERE   parent_name IS NULL;

      UPDATE   XXWC.XXWC_EGO_ICCSTRUCTURE b
         SET   b.leaf = 'X'
       WHERE   NOT EXISTS
                  (SELECT   1
                     FROM   XXWC.XXWC_EGO_ICCSTRUCTURE a
                    WHERE   UPPER (TRIM (b.child_name)) =
                               UPPER (TRIM (a.parent_name)));

      COMMIT;



      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.MTL_ITEM_CAT_GRPS_INTERFACE';

      -- to read data from custom table for ICC and load interface table
      -- Inserting teh top node
      /*    INSERT INTO mtl_item_cat_grps_interface (item_catalog_group_id,
                                                                                                                                             item_catalog_name,
                                                   inactive_date,
                                                   summary_flag,
                                                   enabled_flag,
                                                   start_date_active,
                                                   end_date_active,
                                                   description,
                                                   segment1,
                                                   parent_catalog_group_name,
                                                   item_creation_allowed_flag,
                                                   set_process_id,
                                                   transaction_id,
                                                   process_status,
                                                   transaction_type,
                                                   request_id,
                                                   program_application_id,
                                                   program_id,
                                                   program_update_date,
                                                   created_by,
                                                   creation_date,
                                                   last_updated_by,
                                                   last_update_date,
                                                   last_update_login)
            VALUES   (NULL,
                      'WHITE CAP MST',
                      NULL,
                      'N',
                      'Y',
                      SYSDATE,
                      NULL,
                      'WHITE CAP MST',
                      'WHITE CAP MST',
                      NULL,
                      'N',
                      1,
                      mtl_system_items_interface_s.NEXTVAL,
                      1,
                      'CREATE',
                      NULL,
                      431,
                      NULL,
                      SYSDATE,
                      fnd_profile.VALUE ('USER_ID'),
                      SYSDATE,
                      fnd_profile.VALUE ('USER_ID'),
                      SYSDATE,
                      fnd_profile.VALUE ('LOGIN_ID')); */
      --Inserting teh child node
      FOR c_icc IN (SELECT   leaf,
                             child_key,
                             child_name,
                             parent_key,
                             parent_name
                      FROM   XXWC.XXWC_EGO_ICCSTRUCTURE)
      LOOP
         BEGIN
            INSERT INTO mtl_item_cat_grps_interface (
                                                        item_catalog_group_id,
                                                        item_catalog_name,
                                                        inactive_date,
                                                        summary_flag,
                                                        enabled_flag,
                                                        start_date_active,
                                                        end_date_active,
                                                        description,
                                                        segment1,
                                                        parent_catalog_group_name,
                                                        item_creation_allowed_flag,
                                                        set_process_id,
                                                        transaction_id,
                                                        process_status,
                                                        transaction_type,
                                                        request_id,
                                                        program_application_id,
                                                        program_id,
                                                        program_update_date,
                                                        created_by,
                                                        creation_date,
                                                        last_updated_by,
                                                        last_update_date,
                                                        last_update_login
                       )
              VALUES   (
                           NULL,
                           TRIM (UPPER (SUBSTR (c_icc.child_name, 1, 40))),
                           NULL,
                           'N',
                           'Y',
                           SYSDATE,
                           NULL,
                           TRIM (c_icc.child_name),
                           TRIM (UPPER (SUBSTR (c_icc.child_name, 1, 40))),
                           NVL (
                              TRIM (
                                 UPPER (SUBSTR (c_icc.parent_name, 1, 40))
                              ),
                              'WHITE CAP MST'
                           ),
                           DECODE (c_icc.leaf, 'X', 'Y', 'N'),
                           1,
                           mtl_system_items_interface_s.NEXTVAL,
                           1,
                           'CREATE',
                           NULL,
                           431,
                           NULL,
                           SYSDATE,
                           fnd_profile.VALUE ('USER_ID'),
                           SYSDATE,
                           fnd_profile.VALUE ('USER_ID'),
                           SYSDATE,
                           fnd_profile.VALUE ('LOGIN_ID')
                       );
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the ICC interface table for ICC '
                  || c_icc.child_name
                  || ' error: '
                  || SQLERRM
               );
         END;
      END LOOP;

      COMMIT;

      -- To load value sets
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_FLEX_VALUE_SET_INTF';

      EXECUTE IMMEDIATE 'truncate table ego.EGO_FLEX_VALUE_INTF';

      EXECUTE IMMEDIATE 'truncate table ego.EGO_FLEX_VALUE_TL_INTF';

      FOR c_vs
      IN (SELECT   DISTINCT
                   (TRIM(UPPER(   SUBSTR (ICC_NAME, 1, 40)
                               || '-'
                               || ATTRIBUTE_LABEL)))
                      VALUE_SET_NAME
            FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
           WHERE   attribute_data_type = 'Value Set')
      LOOP
         BEGIN
            INSERT INTO EGO_FLEX_VALUE_SET_INTF (VALUE_SET_NAME,
                                                 VALUE_SET_ID,
                                                 DESCRIPTION,
                                                 VERSION_DESCRIPTION,
                                                 FORMAT_TYPE,
                                                 LONGLIST_FLAG,
                                                 VALIDATION_TYPE,
                                                 PARENT_VALUE_SET_NAME,
                                                 VERSION_SEQ_ID,
                                                 START_ACTIVE_DATE,
                                                 END_ACTIVE_DATE,
                                                 MAXIMUM_SIZE,
                                                 MINIMUM_VALUE,
                                                 MAXIMUM_VALUE,
                                                 VALUE_COLUMN_NAME,
                                                 VALUE_COLUMN_TYPE,
                                                 VALUE_COLUMN_SIZE,
                                                 ID_COLUMN_NAME,
                                                 ID_COLUMN_SIZE,
                                                 ID_COLUMN_TYPE,
                                                 MEANING_COLUMN_NAME,
                                                 MEANING_COLUMN_SIZE,
                                                 MEANING_COLUMN_TYPE,
                                                 TABLE_APPLICATION_ID,
                                                 APPLICATION_TABLE_NAME,
                                                 ADDITIONAL_WHERE_CLAUSE,
                                                 TRANSACTION_TYPE,
                                                 TRANSACTION_ID,
                                                 PROCESS_STATUS,
                                                 SET_PROCESS_ID,
                                                 REQUEST_ID,
                                                 PROGRAM_APPLICATION_ID,
                                                 PROGRAM_ID,
                                                 PROGRAM_UPDATE_DATE,
                                                 LAST_UPDATE_DATE,
                                                 LAST_UPDATED_BY,
                                                 CREATION_DATE,
                                                 CREATED_BY,
                                                 LAST_UPDATE_LOGIN)
              VALUES   (vs_name_func (c_vs.VALUE_SET_NAME),
                        NULL,
                        c_vs.VALUE_SET_NAME,
                        NULL,
                        'C',
                        'X',
                        'I',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        '150',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        'CREATE',
                        mtl_system_items_interface_s.NEXTVAL,
                        1,
                        1,
                        NULL,
                        431,
                        NULL,
                        NULL,
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        fnd_profile.VALUE ('LOGIN_ID'));
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the value set name for  '
                  || c_vs.VALUE_SET_NAME
                  || ' error: '
                  || SQLERRM
               );
         END;
      END LOOP;

      --to load value set name
      FOR c_vsv
      IN (SELECT   DISTINCT
                   vs_name_func(TRIM(UPPER(   SUBSTR (ICC_NAME, 1, 40)
                                           || '-'
                                           || ATTRIBUTE_LABEL)))
                      VALUE_SET_NAME,
                   ATTRIBUTE_VALUE
            FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
           WHERE   attribute_data_type = 'Value Set')
      LOOP
         BEGIN
            INSERT INTO EGO_FLEX_VALUE_INTF (VALUE_SET_NAME,
                                             VALUE_SET_ID,
                                             FLEX_VALUE,
                                             FLEX_VALUE_ID,
                                             VERSION_SEQ_ID,
                                             DISP_SEQUENCE,
                                             START_ACTIVE_DATE,
                                             END_ACTIVE_DATE,
                                             ENABLED_FLAG,
                                             TRANSACTION_TYPE,
                                             TRANSACTION_ID,
                                             PROCESS_STATUS,
                                             SET_PROCESS_ID,
                                             REQUEST_ID,
                                             PROGRAM_APPLICATION_ID,
                                             PROGRAM_ID,
                                             PROGRAM_UPDATE_DATE,
                                             LAST_UPDATE_DATE,
                                             LAST_UPDATED_BY,
                                             CREATION_DATE,
                                             CREATED_BY,
                                             LAST_UPDATE_LOGIN)
              VALUES   (c_vsv.VALUE_SET_NAME,
                        NULL,
                        c_vsv.ATTRIBUTE_VALUE,
                        NULL,
                        NULL,
                        mtl_system_items_interface_s.NEXTVAL,
                        NULL,
                        NULL,
                        'Y',
                        'CREATE',
                        mtl_system_items_interface_s.NEXTVAL,
                        1,
                        1,
                        NULL,
                        431,
                        NULL,
                        NULL,
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        fnd_profile.VALUE ('LOGIN_ID'));

            INSERT INTO EGO_FLEX_VALUE_TL_INTF (VALUE_SET_NAME,
                                                VALUE_SET_ID,
                                                FLEX_VALUE,
                                                FLEX_VALUE_ID,
                                                VERSION_SEQ_ID,
                                                LANGUAGE,
                                                DESCRIPTION,
                                                SOURCE_LANG,
                                                FLEX_VALUE_MEANING,
                                                TRANSACTION_TYPE,
                                                TRANSACTION_ID,
                                                PROCESS_STATUS,
                                                SET_PROCESS_ID,
                                                REQUEST_ID,
                                                PROGRAM_APPLICATION_ID,
                                                PROGRAM_ID,
                                                PROGRAM_UPDATE_DATE,
                                                LAST_UPDATE_DATE,
                                                LAST_UPDATED_BY,
                                                CREATION_DATE,
                                                CREATED_BY,
                                                LAST_UPDATE_LOGIN)
              VALUES   (c_vsv.VALUE_SET_NAME,
                        NULL,
                        c_vsv.ATTRIBUTE_VALUE,
                        NULL,
                        NULL,
                        'US',
                        c_vsv.ATTRIBUTE_VALUE,
                        'US',
                        c_vsv.ATTRIBUTE_VALUE,
                        'CREATE',
                        mtl_system_items_interface_s.NEXTVAL,
                        1,
                        1,
                        NULL,
                        431,
                        NULL,
                        NULL,
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        fnd_profile.VALUE ('LOGIN_ID'));
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the values in value set  '
                  || c_vsv.ATTRIBUTE_VALUE
                  || ' error: '
                  || SQLERRM
               );
         END;
      END LOOP;

      -- To load Attribute groups
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GROUPS_INTERFACE';

      EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GROUPS_DL_INTERFACE';

      FOR c_ag
      IN (SELECT   item_catalog_name Attr_group
            FROM   mtl_item_cat_grps_interface
           WHERE   description IN (SELECT   icc_name
                                     FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                                    WHERE   multirow = 'No')
          UNION
          SELECT   'WHITE CAP MST' FROM DUAL)
      LOOP
         BEGIN
            INSERT INTO EGO_ATTR_GROUPS_INTERFACE (SET_PROCESS_ID,
                                                   ATTR_GROUP_ID,
                                                   APPLICATION_ID,
                                                   ATTR_GROUP_TYPE,
                                                   ATTR_GROUP_NAME,
                                                   ATTR_GROUP_DISP_NAME,
                                                   DESCRIPTION,
                                                   MULTI_ROW,
                                                   VARIANT,
                                                   NUM_OF_ROWS,
                                                   NUM_OF_COLS,
                                                   OWNING_PARTY_ID,
                                                   OWNING_PARTY,
                                                   TRANSACTION_ID,
                                                   PROCESS_STATUS,
                                                   TRANSACTION_TYPE,
                                                   REQUEST_ID,
                                                   PROGRAM_APPLICATION_ID,
                                                   PROGRAM_ID,
                                                   PROGRAM_UPDATE_DATE,
                                                   CREATED_BY,
                                                   CREATION_DATE,
                                                   LAST_UPDATED_BY,
                                                   LAST_UPDATE_DATE,
                                                   LAST_UPDATE_LOGIN)
              VALUES   (1,
                        NULL,
                        431,
                        'EGO_ITEMMGMT_GROUP',
                        ag_name_func (c_ag.Attr_group),
                        c_ag.Attr_group,
                        c_ag.Attr_group,
                        'N',
                        'N',
                        NULL,
                        2,
                        -100,
                        NULL,
                        mtl_system_items_interface_s.NEXTVAL,
                        1,
                        'CREATE',
                        NULL,
                        431,
                        NULL,
                        NULL,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('LOGIN_ID'));

            INSERT INTO EGO_ATTR_GROUPS_DL_INTERFACE (
                                                         SET_PROCESS_ID,
                                                         ATTR_GROUP_ID,
                                                         APPLICATION_ID,
                                                         ATTR_GROUP_TYPE,
                                                         ATTR_GROUP_NAME,
                                                         DATA_LEVEL_ID,
                                                         DATA_LEVEL_NAME,
                                                         DEFAULTING,
                                                         DEFAULTING_NAME,
                                                         VIEW_PRIVILEGE_ID,
                                                         VIEW_PRIVILEGE_NAME,
                                                         USER_VIEW_PRIV_NAME,
                                                         EDIT_PRIVILEGE_ID,
                                                         EDIT_PRIVILEGE_NAME,
                                                         USER_EDIT_PRIV_NAME,
                                                         PRE_BUSINESS_EVENT_FLAG,
                                                         BUSINESS_EVENT_FLAG,
                                                         TRANSACTION_ID,
                                                         PROCESS_STATUS,
                                                         TRANSACTION_TYPE,
                                                         REQUEST_ID,
                                                         PROGRAM_APPLICATION_ID,
                                                         PROGRAM_ID,
                                                         PROGRAM_UPDATE_DATE,
                                                         CREATED_BY,
                                                         CREATION_DATE,
                                                         LAST_UPDATED_BY,
                                                         LAST_UPDATE_DATE,
                                                         LAST_UPDATE_LOGIN
                       )
              VALUES   (1,
                        NULL,
                        431,
                        'EGO_ITEMMGMT_GROUP',
                        ag_name_func (c_ag.Attr_group),
                        NULL,
                        'ITEM_LEVEL',
                        'D',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        mtl_system_items_interface_s.NEXTVAL,
                        1,
                        'CREATE',
                        NULL,
                        431,
                        NULL,
                        NULL,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('LOGIN_ID'));

            NULL;
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the Attribute Group name for  '
                  || c_ag.Attr_group
                  || ' error: '
                  || SQLERRM
               );
         END;
      END LOOP;

      --to insert multirow features and benefits attribute group
      BEGIN
         INSERT INTO EGO_ATTR_GROUPS_INTERFACE (
                                                   SET_PROCESS_ID,
                                                   ATTR_GROUP_ID,
                                                   APPLICATION_ID,
                                                   ATTR_GROUP_TYPE,
                                                   ATTR_GROUP_NAME,
                                                   ATTR_GROUP_DISP_NAME,
                                                   DESCRIPTION,
                                                   MULTI_ROW,
                                                   VARIANT,
                                                   NUM_OF_ROWS,
                                                   NUM_OF_COLS,
                                                   OWNING_PARTY_ID,
                                                   OWNING_PARTY,
                                                   TRANSACTION_ID,
                                                   PROCESS_STATUS,
                                                   TRANSACTION_TYPE,
                                                   REQUEST_ID,
                                                   PROGRAM_APPLICATION_ID,
                                                   PROGRAM_ID,
                                                   PROGRAM_UPDATE_DATE,
                                                   CREATED_BY,
                                                   CREATION_DATE,
                                                   LAST_UPDATED_BY,
                                                   LAST_UPDATE_DATE,
                                                   LAST_UPDATE_LOGIN
                    )
           VALUES   (
                        1,
                        NULL,
                        431,
                        'EGO_ITEMMGMT_GROUP',
                        ag_name_func (
                           (SELECT   attribute_label
                              FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                             WHERE   multirow = 'Yes' AND ROWNUM = 1)
                        ),
                        (SELECT   attribute_label
                           FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                          WHERE   multirow = 'Yes' AND ROWNUM = 1),
                        (SELECT   attribute_label
                           FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                          WHERE   multirow = 'Yes' AND ROWNUM = 1),
                        'Y',
                        'N',
                        NULL,
                        2,
                        -100,
                        NULL,
                        mtl_system_items_interface_s.NEXTVAL,
                        1,
                        'CREATE',
                        NULL,
                        431,
                        NULL,
                        NULL,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('LOGIN_ID')
                    );

         INSERT INTO EGO_ATTR_GROUPS_DL_INTERFACE (
                                                      SET_PROCESS_ID,
                                                      ATTR_GROUP_ID,
                                                      APPLICATION_ID,
                                                      ATTR_GROUP_TYPE,
                                                      ATTR_GROUP_NAME,
                                                      DATA_LEVEL_ID,
                                                      DATA_LEVEL_NAME,
                                                      DEFAULTING,
                                                      DEFAULTING_NAME,
                                                      VIEW_PRIVILEGE_ID,
                                                      VIEW_PRIVILEGE_NAME,
                                                      USER_VIEW_PRIV_NAME,
                                                      EDIT_PRIVILEGE_ID,
                                                      EDIT_PRIVILEGE_NAME,
                                                      USER_EDIT_PRIV_NAME,
                                                      PRE_BUSINESS_EVENT_FLAG,
                                                      BUSINESS_EVENT_FLAG,
                                                      TRANSACTION_ID,
                                                      PROCESS_STATUS,
                                                      TRANSACTION_TYPE,
                                                      REQUEST_ID,
                                                      PROGRAM_APPLICATION_ID,
                                                      PROGRAM_ID,
                                                      PROGRAM_UPDATE_DATE,
                                                      CREATED_BY,
                                                      CREATION_DATE,
                                                      LAST_UPDATED_BY,
                                                      LAST_UPDATE_DATE,
                                                      LAST_UPDATE_LOGIN
                    )
           VALUES   (
                        1,
                        NULL,
                        431,
                        'EGO_ITEMMGMT_GROUP',
                        ag_name_func (
                           ( (SELECT   attribute_label
                                FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                               WHERE   multirow = 'Yes' AND ROWNUM = 1))
                        ),
                        NULL,
                        'ITEM_LEVEL',
                        'D',
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        NULL,
                        mtl_system_items_interface_s.NEXTVAL,
                        1,
                        'CREATE',
                        NULL,
                        431,
                        NULL,
                        NULL,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('LOGIN_ID')
                    );

         NULL;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Error while inserting the Attribute Group name for  '
               || 'Features and benifets'
               || ' error: '
               || SQLERRM
            );
      END;

      --to load attributes in attribute group
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GROUP_COLS_INTF';

      FOR c_aga
      IN (SELECT   DISTINCT
                   ag_name_func (SUBSTR (UPPER (icc_name), 1, 40)) Attr_group
            FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
           WHERE   UPPER (SUBSTR (icc_name, 1, 40)) IN
                         (SELECT   item_catalog_name
                            FROM   mtl_item_cat_grps_interface
                          UNION
                          SELECT   'WHITE CAP MST' FROM DUAL)
          UNION
          SELECT   ag_name_func ( (SELECT   attribute_label
                                     FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                                    WHERE   multirow = 'Yes' AND ROWNUM = 1))
                      Attr_group
            FROM   DUAL)
      LOOP
         FOR c_aga1
         IN (SELECT   DISTINCT icc_key,
                               ICC_NAME,
                               ATTRIBUTE_LABEL,
                               --ATTRIBUTE_VALUE,
                               ATTRIBUTE_UOM,
                               ATTRIBUTE_DATA_TYPE,
                               wcs_publish,
                               multirow
               FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
              WHERE   ag_name_func (SUBSTR (UPPER (icc_name), 1, 40)) =
                         c_aga.Attr_group
                      AND multirow = 'No'
             UNION
             SELECT   DISTINCT icc_key,
                               ICC_NAME,
                               ATTRIBUTE_LABEL,
                               --ATTRIBUTE_VALUE,
                               ATTRIBUTE_UOM,
                               ATTRIBUTE_DATA_TYPE,
                               wcs_publish,
                               multirow
               FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF a
              WHERE   ag_name_func( (SELECT   b.attribute_label
                                       FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF b
                                      WHERE   b.multirow = 'Yes'
                                              AND ROWNUM = 1
                                              AND b.attribute_label =
                                                    a.attribute_label)) =
                         c_aga.Attr_group)
         LOOP
            BEGIN
               l_attr_seq := l_attr_seq + 10;

               IF (c_aga1.ATTRIBUTE_DATA_TYPE = 'Numeric')
               THEN
                  l_dc_number_seq := l_dc_number_seq + 1;
               END IF;

               IF (c_aga1.ATTRIBUTE_DATA_TYPE = 'Long')
               THEN
                  l_dc_long_seq := l_dc_long_seq + 1;
               END IF;

               IF (c_aga1.ATTRIBUTE_DATA_TYPE = 'String'
                   OR c_aga1.ATTRIBUTE_DATA_TYPE = 'Value Set')
               THEN
                  l_dc_char_seq := l_dc_char_seq + 1;
               END IF;

               INSERT INTO EGO_ATTR_GROUP_COLS_INTF (
                                                        SET_PROCESS_ID,
                                                        ATTR_ID,
                                                        APPLICATION_ID,
                                                        ATTR_GROUP_TYPE,
                                                        ATTR_GROUP_ID,
                                                        ATTR_GROUP_NAME,
                                                        INTERNAL_NAME,
                                                        DISPLAY_NAME,
                                                        DESCRIPTION,
                                                        SEQUENCE,
                                                        APPLICATION_COLUMN_NAME,
                                                        DATA_TYPE,
                                                        SEARCH_FLAG,
                                                        UNIQUE_KEY_FLAG,
                                                        INFO_1,
                                                        UOM_CLASS,
                                                        CONTROL_LEVEL,
                                                        VIEW_IN_HIERARCHY_CODE,
                                                        EDIT_IN_HIERARCHY_CODE,
                                                        CUSTOMIZATION_LEVEL,
                                                        READ_ONLY_FLAG,
                                                        ENABLED_FLAG,
                                                        REQUIRED_FLAG,
                                                        SECURITY_ENABLED_FLAG,
                                                        DISPLAY_CODE,
                                                        DEFAULT_VALUE,
                                                        ATTRIBUTE_CODE,
                                                        MAXIMUM_DESCRIPTION_LEN,
                                                        CONCATENATION_DESCRIPTION_LEN,
                                                        FLEX_VALUE_SET_ID,
                                                        FLEX_VALUE_SET_NAME,
                                                        TRANSACTION_ID,
                                                        PROCESS_STATUS,
                                                        TRANSACTION_TYPE,
                                                        REQUEST_ID,
                                                        PROGRAM_APPLICATION_ID,
                                                        PROGRAM_ID,
                                                        PROGRAM_UPDATE_DATE,
                                                        CREATED_BY,
                                                        CREATION_DATE,
                                                        LAST_UPDATED_BY,
                                                        LAST_UPDATE_DATE,
                                                        LAST_UPDATE_LOGIN
                          )
                 VALUES   (
                              1,
                              NULL,
                              431,
                              'EGO_ITEMMGMT_GROUP',
                              NULL,
                              c_aga.Attr_group,
                              UPPER(REPLACE (
                                       attr_name_func (
                                          c_aga1.ATTRIBUTE_LABEL
                                       ),
                                       '/',
                                       '_'
                                    )),
                              c_aga1.ATTRIBUTE_LABEL,
                              c_aga1.ATTRIBUTE_UOM
                              || DECODE (c_aga1.wcs_publish,
                                         'Yes', ' [WCS]',
                                         NULL),
                              l_attr_seq,
                              DECODE (c_aga1.ATTRIBUTE_DATA_TYPE,
                                      'Numeric',
                                      'N_EXT_ATTR' || l_dc_number_seq,
                                      'Long',
                                      'TL_EXT_ATTR' || l_dc_long_seq,
                                      'C_EXT_ATTR' || l_dc_char_seq),
                              DECODE (c_aga1.ATTRIBUTE_DATA_TYPE,
                                      'Numeric', 'N',
                                      'Long', 'A',
                                      'C'),
                              'N',
                              DECODE (c_aga1.multirow, 'Yes', 'Y', 'N'),
                              NULL,
                              NULL,
                              1,
                              NULL,
                              NULL,
                              'A',
                              'N',
                              'Y',
                              'N',
                              NULL,
                              'T',
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              NULL,
                              DECODE (
                                 c_aga1.ATTRIBUTE_DATA_TYPE,
                                 'Value Set',
                                 vs_name_func(TRIM(UPPER(SUBSTR (
                                                            c_aga1.ICC_NAME,
                                                            1,
                                                            40
                                                         )
                                                         || '-'
                                                         || c_aga1.ATTRIBUTE_LABEL)))
                              ),
                              mtl_system_items_interface_s.NEXTVAL,
                              1,
                              'CREATE',
                              NULL,
                              431,
                              NULL,
                              NULL,
                              fnd_profile.VALUE ('USER_ID'),
                              SYSDATE,
                              fnd_profile.VALUE ('USER_ID'),
                              SYSDATE,
                              fnd_profile.VALUE ('LOGIN_ID')
                          );
            EXCEPTION
               WHEN OTHERS
               THEN
                  fnd_file.put_line (
                     fnd_file.LOG,
                        'Error while inserting the Attribute  name for  '
                     || c_aga1.ATTRIBUTE_LABEL
                     || ' error: '
                     || SQLERRM
                  );
            END;
         END LOOP;

         l_attr_seq := 0;
         l_dc_number_seq := 0;
         l_dc_char_seq := 0;
         l_dc_long_seq := 0;
      END LOOP;

      -- to populate AG ICC association
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_ATTR_GRPS_ASSOC_INTERFACE';

      FOR c_ag_icc
      IN (SELECT   ATTR_GROUP_NAME ag, DESCRIPTION icc
            FROM   EGO_ATTR_GROUPS_INTERFACE
           WHERE   MULTI_ROW = 'N'
          UNION
          SELECT   ag_name_func ( (SELECT   attribute_label
                                     FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                                    WHERE   multirow = 'Yes' AND ROWNUM = 1))
                      ag, 'WHITE CAP MST' icc
            FROM   DUAL)
      LOOP
         BEGIN
            INSERT INTO EGO_ATTR_GRPS_ASSOC_INTERFACE (
                                                          ITEM_CATALOG_GROUP_ID,
                                                          ITEM_CATALOG_NAME,
                                                          DATA_LEVEL,
                                                          DATA_LEVEL_ID,
                                                          ATTR_GROUP_NAME,
                                                          ATTR_GROUP_ID,
                                                          SET_PROCESS_ID,
                                                          TRANSACTION_ID,
                                                          PROCESS_STATUS,
                                                          TRANSACTION_TYPE,
                                                          REQUEST_ID,
                                                          PROGRAM_APPLICATION_ID,
                                                          PROGRAM_ID,
                                                          PROGRAM_UPDATE_DATE,
                                                          CREATED_BY,
                                                          CREATION_DATE,
                                                          LAST_UPDATED_BY,
                                                          LAST_UPDATE_DATE,
                                                          LAST_UPDATE_LOGIN,
                                                          ASSOCIATION_ID
                       )
              VALUES   (NULL,
                        c_ag_icc.icc,
                        'ITEM_LEVEL',
                        NULL,
                        c_ag_icc.ag,
                        NULL,
                        1,
                        mtl_system_items_interface_s.NEXTVAL,
                        1,
                        'CREATE',
                        NULL,
                        431,
                        NULL,
                        NULL,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('LOGIN_ID'),
                        NULL);
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Error while inserting the Attribute group association for  name for  '
                  || c_ag_icc.ag
                  || ' error: '
                  || SQLERRM
               );
         END;
      END LOOP;

      -- to create the ICC Pages
      --truncating the interface table before metadata import
      EXECUTE IMMEDIATE 'truncate table ego.EGO_PAGES_INTERFACE';

      EXECUTE IMMEDIATE 'truncate table ego.EGO_PAGE_ENTRIES_INTERFACE';

      FOR c_icc_page
      IN (SELECT   item_catalog_name icc, attr_group_name ag
            FROM   EGO_ATTR_GRPS_ASSOC_INTERFACE
           WHERE   attr_group_name NOT IN
                         (SELECT   ag_name_func( (SELECT   attribute_label
                                                    FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                                                   WHERE   multirow = 'Yes'
                                                           AND ROWNUM = 1))
                            FROM   DUAL))
      LOOP
         BEGIN
            INSERT INTO EGO_PAGES_INTERFACE (
                                                SET_PROCESS_ID,
                                                PAGE_ID,
                                                DISPLAY_NAME,
                                                INTERNAL_NAME,
                                                DESCRIPTION,
                                                CLASSIFICATION_CODE,
                                                CLASSIFICATION_NAME,
                                                DATA_LEVEL,
                                                SEQUENCE,
                                                TRANSACTION_ID,
                                                PROCESS_STATUS,
                                                TRANSACTION_TYPE,
                                                REQUEST_ID,
                                                PROGRAM_APPLICATION_ID,
                                                PROGRAM_ID,
                                                PROGRAM_UPDATE_DATE,
                                                CREATED_BY,
                                                CREATION_DATE,
                                                LAST_UPDATED_BY,
                                                LAST_UPDATE_DATE,
                                                LAST_UPDATE_LOGIN
                       )
              VALUES   (
                           1,
                           NULL,
                           c_icc_page.icc,
                           'XXWC_'
                           || REPLACE (REPLACE (c_icc_page.icc, ' ', '_'),
                                       '/',
                                       '_')
                           || '_PG',
                           c_icc_page.icc,
                           NULL,
                           c_icc_page.icc,
                           'ITEM_LEVEL',
                           100,
                           mtl_system_items_interface_s.NEXTVAL,
                           1,
                           'CREATE',
                           NULL,
                           431,
                           NULL,
                           NULL,
                           fnd_profile.VALUE ('USER_ID'),
                           SYSDATE,
                           fnd_profile.VALUE ('USER_ID'),
                           SYSDATE,
                           fnd_profile.VALUE ('LOGIN_ID')
                       );

            INSERT INTO EGO_PAGE_ENTRIES_INTERFACE (
                                                       SET_PROCESS_ID,
                                                       PAGE_ID,
                                                       INTERNAL_NAME,
                                                       OLD_ASSOCIATION_ID,
                                                       NEW_ASSOCIATION_ID,
                                                       OLD_ATTR_GROUP_ID,
                                                       NEW_ATTR_GROUP_ID,
                                                       OLD_ATTR_GROUP_NAME,
                                                       NEW_ATTR_GROUP_NAME,
                                                       SEQUENCE,
                                                       CLASSIFICATION_CODE,
                                                       CLASSIFICATION_NAME,
                                                       TRANSACTION_ID,
                                                       PROCESS_STATUS,
                                                       TRANSACTION_TYPE,
                                                       REQUEST_ID,
                                                       PROGRAM_APPLICATION_ID,
                                                       PROGRAM_ID,
                                                       PROGRAM_UPDATE_DATE,
                                                       CREATION_DATE,
                                                       CREATED_BY,
                                                       LAST_UPDATED_BY,
                                                       LAST_UPDATE_DATE,
                                                       LAST_UPDATE_LOGIN
                       )
              VALUES   (
                           1,
                           NULL,
                           'XXWC_'
                           || REPLACE (REPLACE (c_icc_page.icc, ' ', '_'),
                                       '/',
                                       '_')
                           || '_PG',
                           NULL,
                           NULL,
                           NULL,
                           NULL,
                           c_icc_page.ag,
                           NULL,
                           100,
                           NULL,
                           c_icc_page.icc,
                           mtl_system_items_interface_s.NEXTVAL,
                           1,
                           'CREATE',
                           NULL,
                           431,
                           NULL,
                           NULL,
                           SYSDATE,
                           fnd_profile.VALUE ('USER_ID'),
                           fnd_profile.VALUE ('USER_ID'),
                           SYSDATE,
                           fnd_profile.VALUE ('LOGIN_ID')
                       );
         EXCEPTION
            WHEN OTHERS
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                     'Error while inserting the Item Pages for ICC '
                  || c_icc_page.icc
                  || ' error: '
                  || SQLERRM
               );
         END;
      END LOOP;

      --to insert multirow ag
      INSERT INTO EGO_PAGE_ENTRIES_INTERFACE (SET_PROCESS_ID,
                                              PAGE_ID,
                                              INTERNAL_NAME,
                                              OLD_ASSOCIATION_ID,
                                              NEW_ASSOCIATION_ID,
                                              OLD_ATTR_GROUP_ID,
                                              NEW_ATTR_GROUP_ID,
                                              OLD_ATTR_GROUP_NAME,
                                              NEW_ATTR_GROUP_NAME,
                                              SEQUENCE,
                                              CLASSIFICATION_CODE,
                                              CLASSIFICATION_NAME,
                                              TRANSACTION_ID,
                                              PROCESS_STATUS,
                                              TRANSACTION_TYPE,
                                              REQUEST_ID,
                                              PROGRAM_APPLICATION_ID,
                                              PROGRAM_ID,
                                              PROGRAM_UPDATE_DATE,
                                              CREATION_DATE,
                                              CREATED_BY,
                                              LAST_UPDATED_BY,
                                              LAST_UPDATE_DATE,
                                              LAST_UPDATE_LOGIN)
        VALUES   (1,
                  NULL,
                  'XXWC_WHITE_CAP_MST_PG',
                  NULL,
                  NULL,
                  NULL,
                  NULL,
                  ag_name_func ( (SELECT   attribute_label
                                    FROM   XXWC.XXWC_EGO_MANAGED_ATTR_DEF
                                   WHERE   multirow = 'Yes' AND ROWNUM = 1)),
                  NULL,
                  200,
                  NULL,
                  'WHITE CAP MST',
                  mtl_system_items_interface_s.NEXTVAL,
                  1,
                  'CREATE',
                  NULL,
                  431,
                  NULL,
                  NULL,
                  SYSDATE,
                  fnd_profile.VALUE ('USER_ID'),
                  fnd_profile.VALUE ('USER_ID'),
                  SYSDATE,
                  fnd_profile.VALUE ('LOGIN_ID'));

      COMMIT;
      fnd_file.put_line (fnd_file.LOG, 'End of Program');
   END create_metadata;
   
   /********************************************************************************
   PROGRAM TYPE: PROCEDURE
   NAME: CONVERSION
   PURPOSE: Procedure to convert the item attributes
   HISTORY
   ===============================================================================
   VERSION DATE          AUTHOR(S)       DESCRIPTION
   ------- -----------   --------------- -----------------------------------------
   1.0     09/23/2012    Rajiv Rathod    Initial creation of the procedure
    ********************************************************************************/      

   PROCEDURE conversion (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   AS
      --declaring variables
      l_user_id              NUMBER;
      l_attr_seq             number := 0;
      l_dc_number_seq        number := 0;
      l_dc_char_seq          number := 0;
      l_dc_long_seq          number := 0;
      l_category_rec         INV_ITEM_CATEGORY_PUB.CATEGORY_REC_TYPE;
      l_return_status        VARCHAR2 (80);
      l_error_code           NUMBER;
      l_msg_count            NUMBER;
      l_msg_data             VARCHAR2 (80);
      l_out_category_id      NUMBER;
      l_category_set_id      NUMBER;
      l_category_id          NUMBER;
      l_structure_id         number;
      l_parent_category_id   number;
      l_request_wait         BOOLEAN;
      l_chr_phase            varchar2 (100);
      l_chr_status           varchar2 (100);
      l_chr_dev_phase        varchar2 (100);
      l_chr_dev_status       varchar2 (100);
      l_chr_msg              varchar2 (1000);
      l_num_request_id       number;
      l_chr_errbuff          VARCHAR2 (10000);
      l_num_retcode          NUMBER;
      i                      NUMBER;
      j                      NUMBER;
      l_row_identifier       NUMBER := 0;
   BEGIN
      fnd_file.put_line (fnd_file.LOG, 'Beginning of Program');

      --apps initialize
      --block to get the user
      BEGIN
         SELECT   user_id
           INTO   l_user_id
           FROM   fnd_user
          WHERE   user_name = 'XXWC_INT_SUPPLYCHAIN';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'Unable to find user XXWC_INT_SUPPLYCHAIN ' || SQLERRM);
      END;

      fnd_global.apps_initialize (user_id        => l_user_id,
                                  resp_id        => 50296,
                                  resp_appl_id   => 431);

      BEGIN
         SELECT   f.ID_FLEX_NUM
           INTO   l_category_rec.structure_id
           FROM   FND_ID_FLEX_STRUCTURES f
          WHERE   f.ID_FLEX_STRUCTURE_CODE = 'XXWC_WEB_HIERARCHY';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'structure XXWC.XXWC_EGO_WEB_HIERARCHY not found ');
      END;

      fnd_file.put_line (fnd_file.LOG,
                         'Begin of category conversion program');

      FOR i IN (SELECT   TRIM (child) child FROM XXWC.XXWC_EGO_WEB_HIERARCHY)
      LOOP
         l_category_rec.segment1 := UPPER (i.child);
         l_category_rec.description := i.child;

         l_category_rec.WEB_STATUS := 'N';

         l_category_rec.SUPPLIER_ENABLED_FLAG := 'Y';



         INV_ITEM_CATEGORY_PUB.Create_Category (
            p_api_version     => 1.0,
            p_init_msg_list   => FND_API.G_FALSE,
            p_commit          => FND_API.G_TRUE,
            x_return_status   => l_return_status,
            x_errorcode       => l_error_code,
            x_msg_count       => l_msg_count,
            x_msg_data        => l_msg_data,
            p_category_rec    => l_category_rec,
            x_category_id     => l_out_category_id
         );
         COMMIT;

         IF l_return_status = fnd_api.g_ret_sts_success
         THEN
            NULL;
         ELSE
            fnd_file.put_line (
               fnd_file.LOG,
                  'Creation of Item Category Failed for: '
               || l_category_rec.segment1
               || ' with the error :'
               || l_error_code
            );
         END IF;
      END LOOP;

      BEGIN
         SELECT   mcs_tl.CATEGORY_SET_ID
           INTO   l_category_set_id
           FROM   mtl_category_sets_tl mcs_tl
          WHERE   mcs_tl.CATEGORY_SET_NAME = 'WC Web Hierarchy';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'category set WC Web Hierarchy not found ');
      END;
      BEGIN
         SELECT   f.ID_FLEX_NUM
           INTO   l_structure_id
           FROM   FND_ID_FLEX_STRUCTURES f
          WHERE   f.ID_FLEX_STRUCTURE_CODE = 'XXWC_WEB_HIERARCHY';
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                               'structure XXWC.XXWC_EGO_WEB_HIERARCHY not found ');
      END;

      FOR i
      IN (SELECT   TRIM (child) child, TRIM (parent) parent
            FROM   XXWC.XXWC_EGO_WEB_HIERARCHY)
      LOOP
         BEGIN
            l_category_id := NULL;
            l_parent_category_id := NULL;

            SELECT   mcb.CATEGORY_ID
              INTO   l_category_id
              FROM   mtl_categories_b mcb
             WHERE   mcb.SEGMENT1 = UPPER (i.child)
                     AND mcb.STRUCTURE_ID = l_structure_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;

         BEGIN
            SELECT   mcb.CATEGORY_ID
              INTO   l_parent_category_id
              FROM   mtl_categories_b mcb
             WHERE   mcb.SEGMENT1 = UPPER (i.parent)
                     AND mcb.STRUCTURE_ID = l_structure_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               NULL;
         END;



         INV_ITEM_CATEGORY_PUB.Create_Valid_Category (
            p_api_version          => 1.0,
            p_init_msg_list        => FND_API.G_FALSE,
            p_commit               => FND_API.G_TRUE,
            x_return_status        => l_return_status,
            x_errorcode            => l_error_code,
            x_msg_count            => l_msg_count,
            x_msg_data             => l_msg_data,
            p_category_set_id      => l_category_set_id,
            p_category_id          => l_category_id,
            p_parent_category_id   => l_parent_category_id
         );

         IF l_return_status = fnd_api.g_ret_sts_success
         THEN
            COMMIT;
            NULL;
         ELSE
            fnd_file.put_line (
               fnd_file.LOG,
               'Create Valid Category Failed with the error :'
               || l_error_code
            );
         END IF;
      END LOOP;

      COMMIT;
      fnd_file.put_line (fnd_file.LOG, 'End of category assignment program');

      fnd_file.put_line (fnd_file.LOG,
                         'Begin of icc adn web hierarchy association');

      FOR i IN (SELECT   ITEM_NUMBER,
                         ICC,
                         WEB_HIERARCHY,
                         MANUF_PART_NO
                  FROM   XXWC.XXWC_EGO_ICC_SKU_ASSO)
      LOOP
         INSERT INTO mtl_system_items_interface (item_number,
                                                 set_process_id,
                                                 created_by,
                                                 creation_date,
                                                 last_update_date,
                                                 last_updated_by,
                                                 transaction_type,
                                                 process_flag,
                                                 organization_id,
                                                 ITEM_CATALOG_GROUP_NAME)
           VALUES   (TRIM (i.item_number),
                     1,
                     FND_PROFILE.VALUE ('USER_ID'),
                     SYSDATE,
                     SYSDATE,
                     FND_PROFILE.VALUE ('USER_ID'),
                     'UPDATE',
                     1,
                     222,
                     TRIM (UPPER (SUBSTR (i.icc, 1, 40))));

         INSERT INTO mtl_item_categories_interface (item_number,
                                                    set_process_id,
                                                    created_by,
                                                    creation_date,
                                                    last_update_date,
                                                    last_updated_by,
                                                    transaction_type,
                                                    process_flag,
                                                    organization_id,
                                                    category_set_name,
                                                    category_name,
                                                    transaction_id)
           VALUES   (TRIM (i.item_number),
                     1,
                     FND_PROFILE.VALUE ('USER_ID'),
                     SYSDATE,
                     SYSDATE,
                     FND_PROFILE.VALUE ('USER_ID'),
                     'CREATE',
                     1,
                     222,
                     'WC Web Hierarchy',
                     UPPER (TRIM (i.WEB_HIERARCHY)),
                     mtl_system_items_interface_s.NEXTVAL);
      END LOOP;

      COMMIT;


      l_num_request_id :=
         apps.fnd_request.submit_request ('INV',
                                          'INCOIN',
                                          NULL,
                                          SYSDATE,
                                          FALSE,
                                          222,
                                          '1',
                                          '1',
                                          '1',
                                          '2',
                                          1 --process set                                                             ,
                                           ,
                                          '2');
      COMMIT;

      l_request_wait :=
         fnd_concurrent.wait_for_request (l_num_request_id,
                                          30,          -- wait 30 sec interval
                                          9999,               -- max wait time
                                          l_chr_phase,
                                          l_chr_status,
                                          l_chr_dev_phase,
                                          l_chr_dev_status,
                                          l_chr_msg);
      fnd_file.put_line (fnd_file.LOG,
                         'End of icc and web hierarchy association');

      fnd_file.put_line (fnd_file.LOG,
                         'Begin of TOP level attribute data value upload');

      UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_VALUES a
         SET   process_flag = 'N',
               Error_message = 'Item does not exist in PIM'
       WHERE   NOT EXISTS (SELECT   1
                             FROM   mtl_system_items_b
                            WHERE   segment1 = a.item_number);



      UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_VALUES a
         SET   process_flag = 'N',
               Error_message = Error_message || ' Item does not exist in ICC'
       WHERE   NOT EXISTS
                  (SELECT   1
                     FROM   mtl_system_items_b b, mtl_item_catalog_groups_b c
                    WHERE   b.segment1 = a.item_number
                            AND b.item_catalog_group_id =
                                  c.item_catalog_group_id
                            AND ROWNUM = 1);

      UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_VALUES
         SET   attribute_group = XXWC_EGO_WC_METADATA_PKG.ag_name_func (icc),
               ATTRIBUTE =
                  XXWC_EGO_WC_METADATA_PKG.attr_name_func (attribute_lable);

      UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_VALUES
         SET   data_type =
                  (SELECT   data_type_code
                     FROM   ego_attrs_v
                    WHERE   attr_group_name || attr_name =
                               attribute_group || ATTRIBUTE);

      COMMIT;

      FOR j IN (SELECT   DISTINCT item_number item_number
                  FROM   XXWC.XXWC_EGO_ATTRIBUTE_VALUES
                 WHERE   process_flag IS NULL)
      LOOP
      l_row_identifier := l_row_identifier +1;
         FOR i
         IN (  SELECT   DISTINCT item_number,
                                 icc,
                                 attribute_lable,
                                 attribute_value,
                                 data_type,
                                 attribute_group,
                                 ATTRIBUTE
                 FROM   XXWC.XXWC_EGO_ATTRIBUTE_VALUES a
                WHERE   a.item_number = j.item_number AND process_flag IS NULL
             ORDER BY   1, 2)
         LOOP
            BEGIN
               INSERT INTO ego.ego_itm_usr_attr_intrfc (
                                                           process_status,
                                                           data_set_id,
                                                           item_number,
                                                           row_identifier,
                                                           attr_group_int_name,
                                                           attr_int_name,
                                                           attr_value_str,
                                                           attr_value_num,
                                                           attr_disp_value,
                                                           transaction_type,
                                                           organization_id,
                                                           created_by,
                                                           creation_date,
                                                           last_updated_by,
                                                           last_update_date,
                                                           last_update_login,
                                                           attr_group_type,
                                                           interface_table_unique_id
                          )
                 VALUES   (
                              1,
                              1,
                              TRIM (i.item_number),
                              l_row_identifier,
                              i.attribute_group,
                              i.ATTRIBUTE,
                              TRIM(DECODE (i.data_type,
                                           'C', i.attribute_value,
                                           'A', i.attribute_value,
                                           NULL)),
                              TRIM(DECODE (i.data_type,
                                           'N', i.attribute_value,
                                           NULL)),
                              NULL,
                              'SYNC',
                              222,
                              fnd_profile.VALUE ('USER_ID'),
                              SYSDATE,
                              fnd_profile.VALUE ('USER_ID'),
                              SYSDATE,
                              fnd_profile.VALUE ('LOGIN_ID'),
                              'EGO_ITEMMGMT_GROUP',
                              mtl_system_items_interface_s.NEXTVAL
                          );
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line(   'Item Number '
                                       || j.item_number
                                       || i.attribute_value
                                       || SQLERRM);
            END;

            COMMIT;
         END LOOP;

         l_chr_errbuff := NULL;
         l_num_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
            errbuf                           => l_chr_errbuff,
            retcode                          => l_num_retcode,
            p_data_set_id                    => 1,
            p_debug_level                    => 0,
            p_purge_successful_lines         => apps.fnd_api.g_false,
            p_initialize_error_handler       => apps.fnd_api.g_true,
            p_validate_only                  => apps.fnd_api.g_false,
            p_ignore_security_for_validate   => apps.fnd_api.g_false
         );
         COMMIT;

         IF (l_chr_errbuff IS NOT NULL)
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Item Number '
               || j.item_number
               || ' error  message '
               || l_chr_errbuff
            );
         END IF;
      END LOOP;

      fnd_file.put_line (fnd_file.LOG,
                         'end of TOP level attribute data value upload');
      fnd_file.put_line (fnd_file.LOG,
                         'Begin of item level attribute data value upload');

      UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES a
         SET   process_flag = 'N',
               Error_message = 'Item does not exist in PIM'
       WHERE   NOT EXISTS (SELECT   1
                             FROM   mtl_system_items_b
                            WHERE   segment1 = a.item_number);



      UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES a
         SET   process_flag = 'N',
               Error_message = Error_message || ' Item does not exist in ICC provided in Data file'
       WHERE   NOT EXISTS
                  (SELECT   1
                     FROM   mtl_system_items_b b, mtl_item_catalog_groups_b c
                    WHERE   b.segment1 = a.item_number
                            AND b.item_catalog_group_id =
                                  c.item_catalog_group_id
                            AND UPPER (icc) = c.segment1
                            AND ROWNUM = 1);

      UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES
         SET   attribute_group = XXWC_EGO_WC_METADATA_PKG.ag_name_func (icc),
               ATTRIBUTE =
                  XXWC_EGO_WC_METADATA_PKG.attr_name_func (attribute_lable);

      UPDATE   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES
         SET   data_type =
                  (SELECT   data_type_code
                     FROM   ego_attrs_v
                    WHERE   attr_group_name || attr_name =
                               attribute_group || ATTRIBUTE);

      COMMIT;

      FOR j IN (SELECT   DISTINCT item_number item_number
                  FROM   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES
                 WHERE   process_flag IS NULL)
      LOOP
      l_row_identifier := l_row_identifier +1;
         FOR i
         IN (  SELECT   DISTINCT item_number,
                                 icc,
                                 attribute_lable,
                                 attribute_value,
                                 data_type,
                                 attribute_group,
                                 ATTRIBUTE
                 FROM   XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES a
                WHERE   a.item_number = j.item_number AND process_flag IS NULL
             ORDER BY   1, 2)
         LOOP
            BEGIN
               INSERT INTO ego.ego_itm_usr_attr_intrfc (
                                                           process_status,
                                                           data_set_id,
                                                           item_number,
                                                           row_identifier,
                                                           attr_group_int_name,
                                                           attr_int_name,
                                                           attr_value_str,
                                                           attr_value_num,
                                                           attr_disp_value,
                                                           transaction_type,
                                                           organization_id,
                                                           created_by,
                                                           creation_date,
                                                           last_updated_by,
                                                           last_update_date,
                                                           last_update_login,
                                                           attr_group_type,
                                                           interface_table_unique_id
                          )
                 VALUES   (
                              1,
                              2,
                              TRIM (i.item_number),
                              l_row_identifier,
                              i.attribute_group,
                              i.ATTRIBUTE,
                              TRIM(DECODE (i.data_type,
                                           'C', i.attribute_value,
                                           'A', i.attribute_value,
                                           NULL)),
                              TRIM(DECODE (i.data_type,
                                           'N', i.attribute_value,
                                           NULL)),
                              NULL,
                              'SYNC',
                              222,
                              FND_PROFILE.VALUE ('USER_ID'),
                              SYSDATE,
                              FND_PROFILE.VALUE ('USER_ID'),
                              SYSDATE,
                              fnd_profile.VALUE ('LOGIN_ID'),
                              'EGO_ITEMMGMT_GROUP',
                              mtl_system_items_interface_s.NEXTVAL
                          );
            EXCEPTION
               WHEN OTHERS
               THEN
                  NULL;
            END;

            COMMIT;
         END LOOP;

         l_chr_errbuff := NULL;
         l_num_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
            errbuf                           => l_chr_errbuff,
            retcode                          => l_num_retcode,
            p_data_set_id                    => 2,
            p_debug_level                    => 0,
            p_purge_successful_lines         => apps.fnd_api.g_false,
            p_initialize_error_handler       => apps.fnd_api.g_true,
            p_validate_only                  => apps.fnd_api.g_false,
            p_ignore_security_for_validate   => apps.fnd_api.g_false
         );
         COMMIT;
      END LOOP;

      fnd_file.put_line (fnd_file.LOG,
                         'end of Item level attribute data value upload');
      fnd_file.put_line (fnd_file.LOG, 'Begin of features and benifets');

      UPDATE   XXWC.XXWC_EGO_FEATURES_BENEFIT a
         SET   process_flag = 'N',
               Error_message = 'Item does not exist in PIM'
       WHERE   NOT EXISTS (SELECT   1
                             FROM   mtl_system_items_b
                            WHERE   segment1 = a.item_number);



      UPDATE   XXWC.XXWC_EGO_FEATURES_BENEFIT a
         SET   process_flag = 'N',
               Error_message = Error_message || ' Item does not exist in ICC'
       WHERE   NOT EXISTS
                  (SELECT   1
                     FROM   mtl_system_items_b b, mtl_item_catalog_groups_b c
                    WHERE   b.segment1 = a.item_number
                            AND b.item_catalog_group_id =
                                  c.item_catalog_group_id
                            AND b.organization_id = 222
                            AND ROWNUM = 1);

      COMMIT;

      FOR j IN (SELECT   DISTINCT item_number item_number
                  FROM   XXWC.XXWC_EGO_FEATURES_BENEFIT
                 WHERE   process_flag IS NULL)
      LOOP
         FOR i
         IN (  SELECT  distinct  item_number, sequence_no, trim(BENEFITS) BENEFITS
                 FROM   XXWC.XXWC_EGO_FEATURES_BENEFIT a
                WHERE   a.item_number = j.item_number AND process_flag IS NULL
             ORDER BY   1, 2)
         LOOP
            INSERT INTO ego.ego_itm_usr_attr_intrfc (
                                                        process_status,
                                                        data_set_id,
                                                        item_number,
                                                        row_identifier,
                                                        attr_group_int_name,
                                                        attr_int_name,
                                                        attr_value_str,
                                                        attr_disp_value,
                                                        transaction_type,
                                                        organization_id,
                                                        created_by,
                                                        creation_date,
                                                        last_updated_by,
                                                        last_update_date,
                                                        last_update_login,
                                                        attr_group_type,
                                                        interface_table_unique_id
                       )
              VALUES   (1,
                        3,
                        TRIM (i.item_number),
                        mtl_system_items_interface_s.NEXTVAL,
                        'XXWC_FEATURES_BENEFITS_AG',
                        'XXWC_FEATURES_BENEFITS_ATTR',
                        TRIM (SUBSTR (i.BENEFITS, 1, 1000)),
                        NULL,
                        'SYNC',
                        222,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('USER_ID'),
                        SYSDATE,
                        fnd_profile.VALUE ('LOGIN_ID'),
                        'EGO_ITEMMGMT_GROUP',
                        mtl_system_items_interface_s.NEXTVAL);

            COMMIT;
         END LOOP;

         l_chr_errbuff := NULL;
         l_num_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
            errbuf                           => l_chr_errbuff,
            retcode                          => l_num_retcode,
            p_data_set_id                    => 3,
            p_debug_level                    => 0,
            p_purge_successful_lines         => apps.fnd_api.g_false,
            p_initialize_error_handler       => apps.fnd_api.g_true,
            p_validate_only                  => apps.fnd_api.g_false,
            p_ignore_security_for_validate   => apps.fnd_api.g_false
         );
         COMMIT;

         IF (l_chr_errbuff IS NOT NULL)
         THEN
            fnd_file.put_line (
               fnd_file.LOG,
                  'Item Number '
               || j.item_number
               || ' error  message '
               || l_chr_errbuff
            );
         END IF;
      END LOOP;

      FOR i IN (SELECT   *
                  FROM   ego_itm_usr_attr_intrfc
                 WHERE   data_set_id = 3 AND process_status = 3)
      LOOP
         UPDATE   ego.ego_itm_usr_attr_intrfc
            SET   process_status = 1
          WHERE       data_set_id = 3
                  AND process_status = 3
                  AND item_number = i.item_number
                  AND attr_int_name = i.attr_int_name
                  AND row_identifier = i.row_identifier;

         COMMIT;
         l_chr_errbuff := NULL;
         l_num_retcode := NULL;
         apps.ego_item_user_attrs_cp_pub.process_item_user_attrs_data (
            errbuf                           => l_chr_errbuff,
            retcode                          => l_num_retcode,
            p_data_set_id                    => 3,
            p_debug_level                    => 0,
            p_purge_successful_lines         => apps.fnd_api.g_false,
            p_initialize_error_handler       => apps.fnd_api.g_true,
            p_validate_only                  => apps.fnd_api.g_false,
            p_ignore_security_for_validate   => apps.fnd_api.g_false
         );
         COMMIT;
      END LOOP;

      fnd_file.put_line (fnd_file.LOG, 'End of features and BENEFITS');
   END conversion;
END XXWC_EGO_WC_METADATA_PKG;
/
sho err