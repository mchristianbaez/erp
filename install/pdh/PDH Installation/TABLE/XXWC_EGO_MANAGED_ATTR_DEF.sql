DROP TABLE XXWC.XXWC_EGO_MANAGED_ATTR_DEF CASCADE CONSTRAINTS;

CREATE TABLE XXWC.XXWC_EGO_MANAGED_ATTR_DEF
(
  ICC_KEY              VARCHAR2(100 BYTE),
  ICC_NAME             VARCHAR2(1000 BYTE),
  ATTRIBUTE_LABEL      VARCHAR2(1000 BYTE),
  ATTRIBUTE_VALUE      VARCHAR2(1000 BYTE),
  ATTRIBUTE_UOM        VARCHAR2(100 BYTE),
  ATTRIBUTE_DATA_TYPE  VARCHAR2(100 BYTE),
  WCS_PUBLISH          varchar2(10),
  Multirow             varchar2(10)
)
/