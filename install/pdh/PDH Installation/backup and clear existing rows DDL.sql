--Backuping the existing data tables
-- These are custom tables and we need to take back up of these and create lok up table 


CREATE TABLE XXWC.XXWC_EGO_ICCSTRUCTURE_BKP AS SELECT * FROM 		XXWC_EGO_ICCSTRUCTURE;
CREATE TABLE XXWC.XXWC_EGO_ICC_SKU_ASSO_BKP AS SELECT * FROM 		XXWC_EGO_ICC_SKU_ASSO;
CREATE TABLE XXWC.XXWC_EGO_ATTRIBUTE_VALUES_BKP as SELECT * FROM 	XXWC_EGO_ATTRIBUTE_VALUES;
CREATE TABLE XXWC.XXWC_EGO_FEATURES_BENEFIT_BKP as SELECT * FROM 	XXWC_EGO_FEATURES_BENEFIT;
CREATE TABLE XXWC.XXWC_EGO_WEB_HIERARCHY_BKP as SELECT * FROM 		XXWC_EGO_WEB_HIERARCHY;
create table XXWC.XXWC_EGO_MANAGED_ATTR_DEF_BKP as select * from 	XXWC_EGO_MANAGED_ATTR_DEF;
create table XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VA_BKP as select * from 	XXWC_EGO_ATTRIBUTE_ITEM_VALUES;

-- Truncate the tables once backed up
TRUNCATE TABLE XXWC.XXWC_EGO_ICCSTRUCTURE;
TRUNCATE TABLE XXWC.XXWC_EGO_ICC_SKU_ASSO;
TRUNCATE TABLE XXWC.XXWC_EGO_ATTRIBUTE_VALUES;
TRUNCATE TABLE XXWC.XXWC_EGO_FEATURES_BENEFIT;
TRUNCATE TABLE XXWC.XXWC_EGO_WEB_HIERARCHY;
TRUNCATE TABLE XXWC.XXWC_EGO_MANAGED_ATTR_DEF;
TRUNCATE TABLE XXWC.XXWC_EGO_ATTRIBUTE_ITEM_VALUES;		  
		  
--create this look up table to see the existing rows 
CREATE TABLE xxwc.xxwc_ego_attr_lookup_tbl
AS
   SELECT a.segment1
         ,a.item_catalog_group_id
         ,c.attr_group_id
         ,c.attr_group_name
         ,c.attr_group_disp_name
         ,d.attr_id
         ,d.attr_group_type
         ,d.description
         ,d.attr_name
         ,d.attr_display_name
         ,d.database_column
         ,d.data_type_code
         ,DECODE (data_type_code, 'A', TO_NUMBER (REPLACE (database_column, 'TL_EXT_ATTR', '')), TO_NUMBER (SUBSTR (database_column, 11))) db_col_num
         ,d.sequence
     FROM APPS.mtl_item_catalog_groups_b a
         ,APPS.ego_obj_ag_assocs_b b
         ,APPS.ego_attr_groups_v c
         ,APPS.ego_attrs_v d
    WHERE a.item_catalog_group_id = b.classification_code AND b.attr_group_id = c.attr_group_id AND c.attr_group_name = d.attr_group_name AND a.segment1 != 'WHITE CAP MST';