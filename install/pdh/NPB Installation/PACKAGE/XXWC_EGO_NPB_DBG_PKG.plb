CREATE OR REPLACE PACKAGE BODY APPS.XXWC_EGO_NPB_DBG_PKG IS
   /**************************************************************************
    File Name:XXWC_EGO_NPB_DBG_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Captures debug information on the NPB form code
    HISTORY
    -- Description   : Called from the trigger XXWC_ENG_NIR_TRG

    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     07-Aug-2012    Santhosh Louis    Initial creation of the package
   **************************************************************************/ 
PROCEDURE DEBUG_PROC
	(
	param_name VARCHAR2,
	param_value VARCHAR2 
	)
   IS
   pragma autonomous_transaction;
BEGIN
	if p_profile_option_value is null then
	    SELECT  FND_PROFILE.VALUE('XXWC_EGO_NPB_DEBUG_PROFILE')
	    INTO    p_profile_option_value
	    FROM    dual;
	end if;

	IF p_profile_option_value ='Y' THEN
		 INSERT INTO XXWC_EGO_NPB_DEBUG 
		 (
		 ID,
		 PARAMETER_NAME  ,
		 PARAMETER_VALUE ,
		 CREATION_DATE 
		 )
		 VALUES 
		 (
		 XXWC_EGO_NPB_DEBUG_S.NEXTVAL,
		 param_name,
		 param_value,
		 CURRENT_TIMESTAMP
		 );
		 COMMIT;
	END IF;

END DEBUG_PROC; -- Procedure
end;
/
