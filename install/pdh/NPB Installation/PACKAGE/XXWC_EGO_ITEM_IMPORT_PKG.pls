set serveroutput on;
create or replace PACKAGE APPS.XXWC_EGO_ITEM_IMPORT_PKG
AS
   /**************************************************************************
    File Name:XXWC_EGO_ITEM_IMPORT_PKG
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Create NPR and trigger Change Order
    HISTORY
    -- Description   : Called from the trigger XXWC_ENG_NIR_TRG
    --
    --
    -- Dependencies Tables        : custom table XXWC_EGO_NPB_DEBUG
    -- Dependencies Views         : None
    -- Dependencies Sequences     : XXWC_EGO_NPB_DEBUG_S
    -- Dependencies Procedures    : XXWC_EGO_NPB_DBG_PKG
    -- Dependencies Functions     : None
    -- Dependencies Packages      : None
    -- Dependencies Types         : None
    -- Dependencies Database Links: None
    ================================================================
           Last Update Date : 01/11/2012
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     07-Aug-2012    Rajiv Rathod,    Initial creation of the package
						   Santhosh Louis
   **************************************************************************/
	FUNCTION process_event (p_subscription_guid IN RAW
							,P_EVENT IN OUT NOCOPY WF_EVENT_T
			)RETURN VARCHAR2;

	FUNCTION check_web_item (p_item_id IN number)
			RETURN NUMBER;
		  
	FUNCTION check_number (p_value IN VARCHAR2)
			RETURN VARCHAR2;      
  /*
   PROCEDURE CREATE_RELATED_ITEMS (P_ITEM_ID               IN  NUMBER,
								  P_ORG_ID                IN  NUMBER,
								  P_RELATED_ITEM_ID       IN  NUMBER,
								  P_RELATIONSHIP_TYPE_ID  IN  NUMBER,
								  p_user_id               IN  NUMBER);
								  
   PROCEDURE CREATE_CROSS_REFERENCE (P_TRANSACTION_TYPE        in  varchar2,
								P_ITEM_ID                 in  number,
								p_org_id                  IN  number,
								P_CROSS_REF_TYPE          IN  varchar2,
								P_CROSS_REF_VALUE         IN  varchar2,
								p_org_independent_flag    IN  varchar2);

    PROCEDURE CREATE_ATTACHMENTS(P_DESCRIPTION      in  varchar2,
                                P_FILE_NAME       IN  varchar2,
                                P_DOC_TEXT        IN  varchar2,
                                P_ITEM_ID         IN  varchar2,
                                P_ORG_ID          IN  VARCHAR2,
                                P_REVISION_ID     in  varchar2,
                                P_ATTACHED_DOC_ID OUT NUMBER
                                );									
   
   --Code commented was used for internal testing
   */
   
	PROCEDURE INITIATE_WORKFLOW(l_item_Type   IN  varchar2,
								l_item_key    IN  varchar2,
								l_body_text   IN  varchar2,
								l_sender_id     IN  varchar2
								);   
   procedure CHECK_ITEM_DUP (P_IN_NUM_CHANGE_ID in number,
								p_STATUS_CODE in VARCHAR2);             
   PROCEDURE IMPORT_ITEM_PROC (P_IN_NUM_CHANGE_ID IN NUMBER);   
   
   END XXWC_EGO_ITEM_IMPORT_PKG;
   /