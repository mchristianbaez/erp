
	SET DEFINE ~
  
  --DROP TABLE XXWC.XXWC_EGO_NPB_DEBUG;
  --DROP SYNONYM APPS.XXWC_EGO_NPB_DEBUG ;
  --DROP SEQUENCE XXWC.XXWC_EGO_NPB_DEBUG_S;
  --DROP SYNONYM APPS.XXWC_EGO_NPB_DEBUG_S;
  --drop package xxwc_ego_npb_debug_pkg;
  --drop package xxwc_item_import_pkg;
  --drop trigger XXWC_EGO_NIR_TRG;
  --drop trigger XXWC_EGO_CHECK_ITEM_TRG;
	/**************************************************************************
    File Name:XXWC_DEBUG.sql
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Create NPR and trigger Change Order
    HISTORY
    -- Description   : Called from the trigger XXWC_ITEM_IMPORT_PKG
    --File has scripts to create custom table XXWC_DEBUG
						  custome sequence XXWC_DEBUG_S
						  custom procedure XXWC_DEBUG_PROC

    ================================================================
           Last Update Date : 01/11/2012
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     07-Aug-2012    Santhosh Louis    Initial creation of the package
   **************************************************************************/ 
	CREATE TABLE XXWC.XXWC_EGO_NPB_DEBUG
	  (
		ID              NUMBER,
		PARAMETER_NAME  VARCHAR2(200 BYTE),
		PARAMETER_VALUE VARCHAR2(4000 BYTE),
		CREATION_DATE 	TIMESTAMP (6)
	  );
	  
	CREATE SEQUENCE XXWC.XXWC_EGO_NPB_DEBUG_S START WITH 1 INCREMENT BY 1;

	CREATE SYNONYM APPS.XXWC_EGO_NPB_DEBUG FOR XXWC.XXWC_EGO_NPB_DEBUG;
  
	CREATE SYNONYM APPS.XXWC_EGO_NPB_DEBUG_S FOR XXWC.XXWC_EGO_NPB_DEBUG_S;
  

	/
	Exit;