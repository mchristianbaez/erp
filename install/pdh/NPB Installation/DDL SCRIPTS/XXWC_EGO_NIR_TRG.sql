--------------------------------------------------------
--  DDL for Trigger XXWC_EGO_NIR_TRG
--------------------------------------------------------
	SET DEFINE ~
	/**************************************************************************
    File Name:XXWC_EGO_NIR_TRG.sql
    PROGRAM TYPE: PL/SQL Package spec and body
    PURPOSE: Invokes the Item Import Package on approval of NPR
    HISTORY
    -- Description   : Triggers the item import on approval of workflow of change request

    ================================================================
           Last Update Date : 01/11/2012
    ================================================================
    ================================================================
    VERSION DATE          AUTHOR(S)       DESCRIPTION
    ------- -----------   --------------- ---------------------------------
    1.0     07-Aug-2012    Rajiv Rathod    Initial creation of the trigger
   **************************************************************************/ 
   
CREATE OR REPLACE TRIGGER "APPS"."XXWC_EGO_NIR_TRG" 
   AFTER UPDATE
   ON ENG_LIFECYCLE_STATUSES
   REFERENCING OLD AS OLD NEW AS NEW
   FOR EACH ROW
    WHEN ( (NEW.status_code = 11 OR NEW.status_code = 6)
         AND NEW.completion_date IS NOT NULL) BEGIN
   XXWC_EGO_ITEM_IMPORT_PKG.IMPORT_ITEM_PROC(:NEW.ENTITY_ID1);
EXCEPTION
   WHEN OTHERS
   THEN
      NULL;
END;
/
ALTER TRIGGER "APPS"."XXWC_EGO_NIR_TRG" ENABLE;
