/*
Add HDS Receivables Inquiry - WC to everyone�s role who already has an OM responsibility CI #347 
  
  Task ID: 20120710-01440  Created: 7/10/2012 6:49:21 AM    
 
   Task Name 
 
   Requested By Linka, Marty   Change Requester 
   Description @SC Add HDS Receivables Inquiry - WC Responsibility to everyone�s role who already has an OM responsibility

Pros: Can be done quickly with minimal testing
Cons: Users will see both HDS Receivables Inquiry - WC and HDS Customer Balance Inquiry - WC in their menu and must choose which to use for different things


1. Identify all Roles that currently have an HDS%Order Mgmt%WC responsibility

2. Add HDS Receivables Inquiry - WC to each Role that does not already have it 
 
Author: Andre Rivas

*/

DECLARE
  l_err_msg             VARCHAR2(3000);
  l_user_id             NUMBER;
  l_responsibility_id   NUMBER;
  l_resp_application_id NUMBER;
  p_user_name           VARCHAR2(50);
  p_responsibility_name VARCHAR2(50);
  l_result              NUMBER;
BEGIN

dbms_output.put_line(' ==> Starting');

  -- Deriving Ids from variables
  BEGIN
    SELECT user_id
      INTO l_user_id
      FROM fnd_user
     WHERE user_name = upper('10011289')
       AND SYSDATE BETWEEN start_date AND nvl(end_date, trunc(SYSDATE) + 1);
  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
      --RAISE program_error;
    WHEN OTHERS THEN
      l_err_msg := 'Error deriving user_id for UserName - ' || p_user_name;
      --RAISE program_error;
  END;

  BEGIN
    SELECT responsibility_id
          ,application_id
      INTO l_responsibility_id
          ,l_resp_application_id
      FROM fnd_responsibility_vl
     WHERE responsibility_name = 'XXWC_IT_SECURITY_ADMINISTRATOR'
       AND SYSDATE BETWEEN start_date AND nvl(end_date, trunc(SYSDATE) + 1);
  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'Responsibility ' || p_responsibility_name ||
                   ' not defined in Oracle';
      --RAISE program_error;
    WHEN OTHERS THEN
      l_err_msg := 'Error deriving Responsibility_id for ' ||
                   p_responsibility_name;
      --RAISE program_error;
  END;

  -- Apps Initialize
  fnd_global.apps_initialize(l_user_id, l_responsibility_id,
                             l_resp_application_id);

   
  
  -- Add an expiration date to revoke.
  -- wf_local_synch.propagateuserrole(p_user_name => '10011289',
  --                                p_role_name => 'UMX|XXWC_ROLE_ACCOUNTS_PAYABLE_SR_MGR',
  --                                p_start_date => '07-APR-2012',
  --                               p_expiration_date => '08-APR-2012');
  



  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MANAGER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MANAGER_AVG_MPO',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_BIN_MTN_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_MPO',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_NO_PCS',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ADMIN COORDINATOR_FIELD',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_AREA_DIRECTOR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ASSISTANT_BRANCH_MANAGER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_BRANCH_MANAGER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_BRANCH_MANAGER_SPC_FAB',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_BUILD_OUT_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER_INV_ADJ_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_INV_ADJ_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_INV_ADJ_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_NO_PCS_BIN_MTN',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CLERK_RECEPTIONIST',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_BIN_MTN',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_BIN_MTN_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_BIN_MTN_POENTRY_CASHIER_SOURCING_RPT',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_CYCLE_COUNT_BIN_MTN_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_BIN_MTN_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_BIN_MTN_POENTRY_CASHIER_SOURCING_RPT',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_INV_ADJ_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_BIN_MTN',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_BIN_MTN_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_INV_ADJ',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_INV_ADJ_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_INV_ADJ_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_INV_ADJ_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_NO_PCS',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_NO_PCS_INV_ADJ_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_MGR_INV_ADJ_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_MGR_MPO_INV_ADJ_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CREDIT_ASSOCIATE_COLLECTIONS',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CREDIT_MGR_CASH_APP',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CREDIT_MGR_CREDIT_&_COLLECTIONS',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_CUSTOMER_SERVICE_SUPERVISOR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DISPATCHER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DISPATCHER_INV_ADJ_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_CMM_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_CMM_SPC_FAB_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_SPC_FAB_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_SALES_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DIVISIONAL_SALES_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DRIVER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DRIVER_INV_ADJ_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_DRIVER_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_ASSOCIATE',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_ASSOCIATE_CYCLE_COUNT',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_ASSOCIATE_INV_ADJ',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_MGR_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ESTIMATOR_DETAILER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ESTIMATOR_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_INV_ADJ_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_MGR_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_MGR_SPC_FAB_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_INV_ADJ_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_NO_PCS_MPO_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_INV_ADJ_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_INV_ADJ_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MGR_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MGR_MPO_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_SPC_FAB_BIN_MTN_POENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INVENTORY_CONTROL_ASSOCIATE',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INVENTORY_CONTROL_ASSOCIATE_POENTRY_OEENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INVENTORY_CONTROL_ASSOCIATE_REC_OEENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INVENTORY_CONTROL_SR_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_INV_SPEC_SSC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_SOURCING_AND_SUPPLY_CHAIN_LEAD',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_TECH_CONFIGURATOR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_TECH_LEAD',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_MECHANIC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_MECHANIC_INV_ADJ_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_MGR_WAREHOUSE_INV_ADJ_REC_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_MGR_WAREHOUSE_MPO',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_NATIONAL_ACCOUNTS_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_OFFICE_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_OFFICE_MGR_SALES_ADMIN_BIN_MAINT_REC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_OFFICE_MGR_SALES_ADMIN_INV_ADJ',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_OPERATIONS SPECIALIST',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_OPERATIONS_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PRICING_ANALYST_SENIOR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PRODUCT_SALES_SPECIALIST',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PRODUCT_SALES_SPECIALIST_INV_ADJ_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PRODUCT_SALES_SPECIALIST_NO_PCS_MPO',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_DIRECTOR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_RECEIVING_ASSOCIATE_BIN_MAINT_OEENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_OPERATIONS_DIRECTOR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_OPERATIONS_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_OPERATIONS_MGR_CMM',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_SALES_DIRECTOR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_RENTAL_COORDINATOR_POENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REPAIRS_RETURNS_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SALES_SUPPORT_ASSOCIATE_SSC',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SALES_SUPPORT_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SOURCING_ANALYST',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SOURCING_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SUPERVISOR_WAREHOUSE_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_TERRITORY_SALES_ASSOCIATE',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_TERRITORY_SALES_ASSOCIATE_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_TERRITORY_SALES_ASSOCIATE_NO_MPO',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_TRAINING_LEAD',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_BIN_MAINT_CYCLE_COUNT_OEENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_BIN_MAINT_OEENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_BIN_MAINT_OEENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_CYCLE_COUNT',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_INV_ADJ_OEENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_OEENTRY',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');


   -- An example of how to remove a relationship:
   /*
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MANAGER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MANAGER_AVG_MPO', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_BIN_MTN_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_MPO', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_NO_PCS', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ADMIN COORDINATOR_FIELD', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_AREA_DIRECTOR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ASSISTANT_BRANCH_MANAGER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BRANCH_MANAGER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BRANCH_MANAGER_SPC_FAB', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BUILD_OUT_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_INV_ADJ_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_INV_ADJ_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_NO_PCS_BIN_MTN', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CLERK_RECEPTIONIST', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_BIN_MTN', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_BIN_MTN_POENTRY_CASHIER_SOURCING_RPT', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_CYCLE_COUNT_BIN_MTN_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_BIN_MTN_POENTRY_CASHIER_SOURCING_RPT', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_BIN_MTN', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_INV_ADJ', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_NO_PCS', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_NO_PCS_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_MGR_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_MGR_MPO_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CREDIT_ASSOCIATE_COLLECTIONS', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CREDIT_MGR_CASH_APP', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CREDIT_MGR_CREDIT_&_COLLECTIONS', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CUSTOMER_SERVICE_SUPERVISOR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISPATCHER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISPATCHER_INV_ADJ_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_CMM_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_CMM_SPC_FAB_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_SPC_FAB_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_SALES_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DIVISIONAL_SALES_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DRIVER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DRIVER_INV_ADJ_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DRIVER_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_ASSOCIATE', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_ASSOCIATE_CYCLE_COUNT', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_ASSOCIATE_INV_ADJ', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_MGR_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ESTIMATOR_DETAILER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ESTIMATOR_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_INV_ADJ_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_MGR_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_MGR_SPC_FAB_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_NO_PCS_MPO_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_INV_ADJ_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MGR_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MGR_MPO_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_SPC_FAB_BIN_MTN_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INVENTORY_CONTROL_ASSOCIATE', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INVENTORY_CONTROL_ASSOCIATE_POENTRY_OEENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INVENTORY_CONTROL_ASSOCIATE_REC_OEENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INVENTORY_CONTROL_SR_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INV_SPEC_SSC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_SOURCING_AND_SUPPLY_CHAIN_LEAD', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_TECH_CONFIGURATOR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_TECH_LEAD', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_MECHANIC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_MECHANIC_INV_ADJ_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_MGR_WAREHOUSE_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_MGR_WAREHOUSE_MPO', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_NATIONAL_ACCOUNTS_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_OFFICE_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_OFFICE_MGR_SALES_ADMIN_BIN_MAINT_REC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_OFFICE_MGR_SALES_ADMIN_INV_ADJ', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_OPERATIONS SPECIALIST', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_OPERATIONS_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PRICING_ANALYST_SENIOR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PRODUCT_SALES_SPECIALIST', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PRODUCT_SALES_SPECIALIST_INV_ADJ_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PRODUCT_SALES_SPECIALIST_NO_PCS_MPO', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_DIRECTOR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_RECEIVING_ASSOCIATE_BIN_MAINT_OEENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_OPERATIONS_DIRECTOR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_OPERATIONS_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_OPERATIONS_MGR_CMM', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_SALES_DIRECTOR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_RENTAL_COORDINATOR_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REPAIRS_RETURNS_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SALES_SUPPORT_ASSOCIATE_SSC', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SALES_SUPPORT_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SOURCING_ANALYST', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SOURCING_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SUPERVISOR_WAREHOUSE_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_TERRITORY_SALES_ASSOCIATE', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_TERRITORY_SALES_ASSOCIATE_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_TERRITORY_SALES_ASSOCIATE_NO_MPO', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_TRAINING_LEAD', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_BIN_MAINT_CYCLE_COUNT_OEENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_BIN_MAINT_OEENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_BIN_MAINT_OEENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_CYCLE_COUNT', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_INV_ADJ_OEENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_OEENTRY', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);

  
EXCEPTION 
WHEN OTHERS THEN 
if (sqlcode=-20002) then NULL ;
else 
RAISE; 
end if; 

*/

  commit;
END;
