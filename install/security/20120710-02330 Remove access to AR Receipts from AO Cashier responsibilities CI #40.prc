DECLARE
  l_err_msg             VARCHAR2(3000);
  l_user_id             NUMBER;
  l_responsibility_id   NUMBER;
  l_resp_application_id NUMBER;
  p_user_name           VARCHAR2(50);
  p_responsibility_name VARCHAR2(50);
  l_result              NUMBER;
BEGIN
  -- Deriving Ids from variables
  BEGIN
    SELECT user_id
      INTO l_user_id
      FROM fnd_user
     WHERE user_name = upper('10011985')
       AND SYSDATE BETWEEN start_date AND nvl(end_date, trunc(SYSDATE) + 1);
  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
      RAISE program_error;
    WHEN OTHERS THEN
      l_err_msg := 'Error deriving user_id for UserName - ' || p_user_name;
      RAISE program_error;
  END;

  BEGIN
    SELECT responsibility_id
          ,application_id
      INTO l_responsibility_id
          ,l_resp_application_id
      FROM fnd_responsibility_vl
     WHERE responsibility_key = 'XXWC_IT_SECURITY_ADMINISTRATOR'
       AND SYSDATE BETWEEN start_date AND nvl(end_date, trunc(SYSDATE) + 1);
  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'Responsibility ' || p_responsibility_name ||
                   ' not defined in Oracle';
      RAISE program_error;
    WHEN OTHERS THEN
      l_err_msg := 'Error deriving Responsibility_id for ' ||
                   p_responsibility_name;
      RAISE program_error;
  END;

  -- Apps Initialize
  fnd_global.apps_initialize(l_user_id, l_responsibility_id,
                             l_resp_application_id);

  
  /* Don't use this
EXCEPTION 
WHEN OTHERS THEN 
if (sqlcode=-20002) then NULL; 
else 
RAISE; 
end if; */

------------------ ADD YOUR CODE HERE

   -- An example of how to Add a relationship:
  /*
  l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MANAGER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
  */
  
   -- An example of how to remove a relationship:
   /*
    l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MANAGER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
   
  */
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ACCOUNT_MGR_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_ASSISTANT_BRANCH_MANAGER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BRANCH_MANAGER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BRANCH_MANAGER_SPC_FAB', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_INV_ADJ_POENTRY', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_INV_ADJ_REC', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_REC', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_CASHIER_NO_PCS_BIN_MTN', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_BIN_MTN_POENTRY_CASHIER_SOURCING_RPT', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_BIN_MTN_POENTRY_CASHIER_SOURCING_RPT', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_MPO_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_ASSOCIATE_PCS_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_LEAD_NO_PCS_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_MGR_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_COUNTER_SALES_MGR_MPO_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_CMM_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_CMM_SPC_FAB_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_DISTRICT_MGR_SPC_FAB_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_MGR', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_EQUIPMENT_RENTAL_MGR_REC', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_MGR_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_FABRICATION_MGR_SPC_FAB_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_ACCOUNT_MGR_NO_PCS_MPO_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_BIN_MTN_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_BIN_MTN_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MPO_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MGR_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_MGR_MPO_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_BIN_MTN_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INSIDE_SALES_PCS_SPC_FAB_BIN_MTN_POENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_INVENTORY_CONTROL_ASSOCIATE_POENTRY_OEENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_MGR_WAREHOUSE_INV_ADJ_REC_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_OFFICE_MGR', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_RECEIVING_ASSOCIATE_BIN_MAINT_CYCLE_COUNT_OEENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_OPERATIONS_DIRECTOR', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_OPERATIONS_MGR', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR_INV_ADJ_OEENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER_MPO_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SUPERVISOR_WAREHOUSE_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_TERRITORY_SALES_ASSOCIATE_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);
  l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_WAREHOUSE ASSOCIATE_BIN_MAINT_OEENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_AO_CASHIER|STANDARD', p_defer_mode => FALSE);

  commit;
END;
