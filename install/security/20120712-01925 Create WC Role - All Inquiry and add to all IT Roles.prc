/*
Create WC Role - All Inquiry and add to all IT Roles 
 
  
  Task ID: 20120712-01925  
 

 
   Requested By Rivas, Andre A   Change Requester 

 
Author: Andre Rivas

*/

DECLARE
  l_err_msg             VARCHAR2(3000);
  l_user_id             NUMBER;
  l_responsibility_id   NUMBER;
  l_resp_application_id NUMBER;
  p_user_name           VARCHAR2(50);
  p_responsibility_name VARCHAR2(50);
  l_result              NUMBER;
BEGIN

dbms_output.put_line(' ==> Starting');

  -- Deriving Ids from variables
  BEGIN
    SELECT user_id
      INTO l_user_id
      FROM fnd_user
     WHERE user_name = upper('10011289')
       AND SYSDATE BETWEEN start_date AND nvl(end_date, trunc(SYSDATE) + 1);
  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
      --RAISE program_error;
    WHEN OTHERS THEN
      l_err_msg := 'Error deriving user_id for UserName - ' || p_user_name;
      --RAISE program_error;
  END;

  BEGIN
    SELECT responsibility_id
          ,application_id
      INTO l_responsibility_id
          ,l_resp_application_id
      FROM fnd_responsibility_vl
     WHERE responsibility_name = 'XXWC_IT_SECURITY_ADMINISTRATOR'
       AND SYSDATE BETWEEN start_date AND nvl(end_date, trunc(SYSDATE) + 1);
  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'Responsibility ' || p_responsibility_name ||
                   ' not defined in Oracle';
      --RAISE program_error;
    WHEN OTHERS THEN
      l_err_msg := 'Error deriving Responsibility_id for ' ||
                   p_responsibility_name;
      --RAISE program_error;
  END;

  -- Apps Initialize
  fnd_global.apps_initialize(l_user_id, l_responsibility_id,
                             l_resp_application_id);

    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_DEVELOPER',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_FINANCE_CONFIGURATOR',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_FINANCE_LEAD',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_HELP_DESK_ANALYST',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_HELP_DESK_ANALYST_SECURITY',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_REPORTING_ANALYST',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_REPORTING_LEAD',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_SALES_AND_FULFILLMENT_CONFIGURATOR',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_SALES_AND_FULFILLMENT_LEAD',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_SOURCING_AND_SUPPLY_CHAIN_CONFIGURATOR',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_SOURCING_AND_SUPPLY_CHAIN_LEAD',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_TECH_CONFIGURATOR',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');
    l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_IT_TECH_LEAD',p_super_name => 'UMX|XXWC_ROLE_ALL_INQUIRY',p_defermode => FALSE,p_enabled => 'Y');

      


  commit;
END;
