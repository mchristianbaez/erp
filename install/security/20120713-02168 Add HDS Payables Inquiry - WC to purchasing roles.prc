/*
Add HDS Payables Inquiry - WC to purchasing roles 
 
 
 
  
  Task ID: 20120713-02168  Created: 7/13/2012 1:09:22 PM    
 
   Task Name 
 
   Requested By Morris, Randall   Change Requester 
   Description Add HDS Payables Inquiry - WC to the following responsibilities. Also add HDS Receivables Inquiry - WC because some have it and some do not.

WC Role - Buyer
WC Role - Buyer (Inv Adj, Cashier)
WC Role - Purchasing Associate
WC Role - Purchasing Director (Sourcing)
WC Role - Purchasing Sr Mgr
WC Role - Regional Purchasing Mgr
WC Role - Regional Purchasing Mgr (Inv Adj, OEEntry,Cashier)
WC Role - Senior Buyer
WC Role - Senior Buyer MPO (Inv Adj, Cashier)
 
 


 
Author: Andre Rivas

*/

DECLARE
  l_err_msg             VARCHAR2(3000);
  l_user_id             NUMBER;
  l_responsibility_id   NUMBER;
  l_resp_application_id NUMBER;
  p_user_name           VARCHAR2(50);
  p_responsibility_name VARCHAR2(50);
  l_result              NUMBER;
BEGIN

dbms_output.put_line(' ==> Starting');

  -- Deriving Ids from variables
  BEGIN
    SELECT user_id
      INTO l_user_id
      FROM fnd_user
     WHERE user_name = upper('10011289')
       AND SYSDATE BETWEEN start_date AND nvl(end_date, trunc(SYSDATE) + 1);
  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'UserName ' || p_user_name || ' not defined in Oracle';
      --RAISE program_error;
    WHEN OTHERS THEN
      l_err_msg := 'Error deriving user_id for UserName - ' || p_user_name;
      --RAISE program_error;
  END;

  BEGIN
    SELECT responsibility_id
          ,application_id
      INTO l_responsibility_id
          ,l_resp_application_id
      FROM fnd_responsibility_vl
     WHERE responsibility_name = 'XXWC_IT_SECURITY_ADMINISTRATOR'
       AND SYSDATE BETWEEN start_date AND nvl(end_date, trunc(SYSDATE) + 1);
  EXCEPTION
    WHEN no_data_found THEN
      l_err_msg := 'Responsibility ' || p_responsibility_name ||
                   ' not defined in Oracle';
      --RAISE program_error;
    WHEN OTHERS THEN
      l_err_msg := 'Error deriving Responsibility_id for ' ||
                   p_responsibility_name;
      --RAISE program_error;
  END;

  -- Apps Initialize
  fnd_global.apps_initialize(l_user_id, l_responsibility_id,
                             l_resp_application_id);

l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_ASSOCIATE', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_DIRECTOR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_SR_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR_INV_ADJ_OEENTRY_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER_MPO_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD', p_defer_mode => FALSE);



l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER', p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_ASSOCIATE', p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_DIRECTOR', p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_SR_MGR', p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR', p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR_INV_ADJ_OEENTRY_CASHIER', p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER', p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD', p_defer_mode => FALSE);
l_result := wf_role_hierarchy.expirerelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER_MPO_INV_ADJ_CASHIER', p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD', p_defer_mode => FALSE);

l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER_INV_ADJ_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_ASSOCIATE',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_DIRECTOR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_SR_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR_INV_ADJ_OEENTRY_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER_MPO_INV_ADJ_CASHIER',p_super_name => 'FND_RESP|AR|XXWC_RECEIVABLES_INQUIRY_WC|STANDARD',p_defermode => FALSE,p_enabled => 'Y');



l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER',p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_BUYER_INV_ADJ_CASHIER',p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_ASSOCIATE',p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_DIRECTOR',p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_PURCHASING_SR_MGR',p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR',p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_REGIONAL_PURCHASING_MGR_INV_ADJ_OEENTRY_CASHIER',p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER',p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD',p_defermode => FALSE,p_enabled => 'Y');
l_result := wf_role_hierarchy.addrelationship(p_sub_name => 'UMX|XXWC_ROLE_SENIOR_BUYER_MPO_INV_ADJ_CASHIER',p_super_name => 'FND_RESP|SQLAP|XXWC_PAYABLES_INQUIRY|STANDARD',p_defermode => FALSE,p_enabled => 'Y');


EXCEPTION 
WHEN OTHERS THEN 
if (sqlcode=-20002) then NULL ;
else 
RAISE; 
end if; 



  commit;
END;
